#!/bin/bash

# -- This script builds OpenCN components inside a docker container. It re-uses
# -- 'agency/build.sh' script.


SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

docker run --rm -it -v $SCRIPTPATH/..:/home/reds/opencn opencn/build-env /home/reds/opencn/agency/build.sh $@
