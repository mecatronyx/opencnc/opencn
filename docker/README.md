This folder contains a `Dockerfile` made to ease the compilation of the whole
OpenCN framework.


## Build the image

To build the Docker image, simple execute the following command (it has to be
done only once):

```bash
cd <OPENCN_HOME>
docker build -t opencn/build-env docker/
```

## Usage

Once the image has been build, simply use the scripts presents in `<OPENCN_HOME>/docker`.


## Advanced mode
The *advanced* mode lets a user to connect itself to the container through a `ssh`
connection. It can be used to debug OpenCN Docker image, for example.

1. Start the container

```bash
cd <OPENCN_HOME>
docker run --rm -it -d -v $PWD:/home/reds/opencn --name opencn-env opencn/build-env
```

2. Now it is possible to *connect* to the container using ssh

```bash
ssh reds@172.17.0.X
```

The following can be used to retrieve the IP address of the machine:

```bash
docker inspect opencn-env | grep IPAddres
```

3. Stopping the container

```bash
docker container stop opencn-env
```

### Tips

* Inside the container, a `sudo apt update` has to be performed before installing
any new packets.
* New installed packets are temporary installed.


