
[![pipeline status](https://gitlab.com/mecatronyx/opencnc/opencn/badges/master/pipeline.svg)](https://gitlab.com/mecatronyx/opencnc/opencn/-/commits/master) 

Welcome to OpenCN Framework
***************************

Retrieve the source code:

```shell
	$ git clone https://gitlab.com/mecatronyx/opencnc/opencn.git opencn
	$ git submodule init
	$ git submodule update
```

Project related links:

* [OpenCN documentation](https://mecatronyx.gitlab.io/opencnc/opencn/)
* [OpenCN web site](https://opencn.heig-vd.ch/)
* [OpenCN discussion forum](https://discourse.heig-vd.ch/c/opencn-framework/)
* [OpenCN Youtube channel](https://www.youtube.com/channel/UC8FQCu_fKYfK7QRDN0j_dBw)

A Virtualbox VM can be downloaded [here](https://reds-data.heig-vd.ch/OpenCN/OpenCN_VM_v3.ova)
* usr: opencn
* pwd: opencn

