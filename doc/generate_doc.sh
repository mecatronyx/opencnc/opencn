#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

usage()
{
    echo "Usage: $0 [OPTIONS] "
    echo ""
    echo "Where OPTIONS are:"
    echo "  -c    clean generated documentation"
    echo "  -f    format, supported: html (default), pdf"
    echo ""
    exit 1
}

# by default - generatate 'html' documentation
format="html"

while getopts "cf:" o; do
    case "$o" in
        c)
            clean=y
            ;;
        f)
            format=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

cd $SCRIPTPATH

if [ "$clean" = "y" ]; then
	echo "== Clean generated documentation"
	make clean
else
	echo "== Documentation generation (format: $format)"
	case "$format" in
		"html")
			make html
			;;
		"pdf")
			make latexpdf
			;;
		*)
			echo "'$format' is not supported"
			;;
	esac
fi

cd -
