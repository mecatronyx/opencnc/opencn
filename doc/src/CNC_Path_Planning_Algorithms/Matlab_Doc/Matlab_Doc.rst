.. _Matlab_documentation:

====================
Matlab documentation
====================

.. note::
  This section provide only a quick overview of the whole matlab code base developed for OpenCN. The code base is available on `gitlab <https://gitlab.com/mecatronyx/opencnc/opencn-matlab>`_.

Getting Starting
----------------

The installation procedure and a basic example are provided in the `README.md <https://gitlab.com/mecatronyx/opencnc/opencn-matlab/-/blob/master/README.md>`_.

Main functions 
--------------

======================= =========================================================================================================================
  Function Name         Information
======================= =========================================================================================================================
FeedoptDefaultConfig    Load the default values for the different parameters (see *Configuration parameters*)
InitFeedoptPlan         Create the context structure which contains different elements which are transmitted between the different algorithms
FeedoptPlan             Finite state machine (FSM), manager of the different steps in the algorithm.
DestroyContext          Free the memory allocated outside the Matlab work space (mainly the different queue). Only used in the matlab editor.
======================= =========================================================================================================================

Queue
-----

Several queues are used to pipe the data between the different processes. A specific class in C++ is implemented to allocate and free the memory used by the diffent queues.

The function `DestroyContext` needs to be called at the end of your tests to free the memory allocated for the queues.

The different queues are created in the function `InitFeedoptPlan` and each queue is stored in the context structure in Matlab.

================= ============ ============================================================================== ==============================================================
  Queue Name       Next Queue   Information                                                                      Documentation
================= ============ ============================================================================== ==============================================================
q_gcode           q_compress    Contains the ``CurvStruct`` created after the interpretation of the GCode     :ref:`G Code Interpreter RS274 <G_Code_Interpreter>`
q_compress        q_smooth      Contains the ``CurvStruct`` after the compression of the small line segments  :ref:`CompressCurvStructs <CompressCurvStructs>`
q_splines         *None*        Contains a pointer to the different structure of spline                       :ref:`Spline <Spline>`
q_smooth          q_split       Contains the ``CurvStruct`` after the smooth step                             :ref:`SmoothCurvStructs <SmoothCurvStructs>`
q_split           q_opt         Contains the ``CurvStruct`` after the Splitting step                          :ref:`SplitCurvStructs <SplitCurvStructs>`
q_opt             *None*        Contains the ``CurvStruct`` after the feedrate optimization                   :ref:`Feedrate planning <Feedrate_Planning>`
================= ============ ============================================================================== ==============================================================

Configuration parameters
------------------------
.. note::
  These parameters are also available in the C/C++ generated code used in the feedopt hal component.

.. warning::
  Some of the parameters have been mainly used and tested on the matlab environment only. 
  Several parameters reflect some heuristic in our approach and then require some time to be tuned to achieve good results.


General (cfg)
~~~~~~~~~~~~~

.. warning::
  The following parameters can lead to unstable behaviors at run time. They're lacking some robustness and more improvement in necessary.
  However, sometimes they are quite handy so they are left available for well skilled user. Normal user should not change these parameters :

  * SplitSpecialSpline
  * UseDynamicBreakpoints
  * DynamicBreakpointsDistance

+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| Parameter                          | Default Value                                    | Description                                                                                                              |
+====================================+==================================================+==========================================================================================================================+
| ENABLE_PRINT_MSG                   |  TRUE                                            | Enable the print of Debug / Info msg in the terminal.                                                                    |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| ReleaseMemoryOfTheQueues           |  TRUE                                            | The queue are freed once they are not required for the next computation.                                                 |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| SplitSpecialSpline                 |  FALSE                                           | Enable a specific split for the interpolation BSpline. The spline are split at each node instead of using their length   |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| source                             |  ""                                              | Path to the G Code filename                                                                                              |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| maskTot                            | [ 1,  1,  1,  0,  1,  1 ]                        | [ X, Y, Z, A, B, C ] Boolean mask of the active axes of the machine.                                                     |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| kin_params                         | [ ... ]                                          | Kinematics parameter. Example XYZBC_tt : { offM, offT, offP, offB, offC }                                                |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| kin_type                           | 'XYZBC_tt'                                       | Kinematics type. For now only the micro5-like kinematics is supported.                                                   |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| dt                                 | 1e-2                                             | Time step used for the discretization                                                                                    |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| DefaultZeroStopCount               | 1                                                | Number of time step passed for a stop                                                                                    |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| NBreak                             | 10                                               | Number of break points (degree of freedom of the BSpline)                                                                |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| NDiscr                             | 20                                               | Number of discretization points (number of points used in the evaluation of the constraints)                             |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| NHorz                              | 10                                               | Horizon length of the curve windows (a.k.a. Look ahead used during the optimization).                                    |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| UseLinearBreakpoints               | TRUE                                             | Use a linear sampling of the node along the BSpline used for the optimization. If not sinusodial version is used.        |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| UseDynamicBreakpoints              | FALSE                                            | Use a variable number of breakpoints for different length. (Legacy code not tested)                                      |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| DynamicBreakpointsDistance         | 0.1                                              | Only actif when UseDynamicBreakpoints is set to true (Legacy code not tested).                                           |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| LeeSplineDegree                    | 4                                                | B Spline order (degree + 1) used in the Lee algorithm. (Note the name of the parameter has not well be chosen)           |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| SplineDegree                       | 3                                                | B Spline order (degree + 1) used in the optimisation. (Note the name of the parameter has not well be chosen)            |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| CutOff                             | 1E-1                                             | Length used by the transition algorithm to cut the curves                                                                |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| LSplit                             | 3                                                | Length used to cut the curves at a given length                                                                          |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| LSplitZero                         | 1E-1                                             | Length used to cut the curves for a Zero start / end                                                                     |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| LThresholdMax                      | 3                                                | Maximum length used in the compressing algorithm                                                                         |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| LThresholdMin                      | 5E-1                                             | Minimum length used in the compressing algorithm. (Should be at least g.t. 3 * CutOff)                                   |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| vmax                               | [ 500, 500, 500, 0,  40, 50 ]                    | Maximum speed along each axis { [mm], [Rad] } / s.                                                                       |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| amax                               | [ 20000, 20000, 20000, 0,  200, 1000 ]           | Maximum acceleration along each axis { [mm], [Rad] } / s^2.                                                              |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| jmax                               | [ 1500000, 1500000, 1500000, 0, 10000, 50000 ]   | Maximum jerk along each axis { [mm], [Rad] } / s^3.                                                                      |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| v_0                                | 0                                                | Boundaries conditions used by the optimization (Do not modify)                                                           |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| v_1                                | 0                                                | Boundaries conditions used by the optimization (Do not modify)                                                           |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| at_0                               | 0                                                | Boundaries conditions used by the optimization (Do not modify)                                                           |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| at_1                               | 0                                                | Boundaries conditions used by the optimization (Do not modify)                                                           |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| coeffD                             | 0                                                | Scaling factor used to adjust cartesian value w.r.t to rotational ones.                                                  |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| GaussLegendreX                     | [ ... ]                                          | Gauss Legendre X (This parameter is computed automatically)                                                              |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| GaussLegendreN                     | [ ... ]                                          | Gauss Legendre N (This parameter is computed automatically)                                                              |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| GaussLegendreW                     | [ ... ]                                          | Gauss Legendre W (This parameter is computed automatically)                                                              |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| maskCart                           | [ ... ]                                          | Boolean mask of the cartesian active axes of the machine (This parameter is computed based on maskTot)                   |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| maskRot                            | [ ... ]                                          | Boolean mask of the rotational active axes of the machine (This parameter is computed based on maskTot)                  |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| indCart                            | [ ... ]                                          | Index of active cartesian axes (This parameter is computed based on maskTot)                                             |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| indRot                             | [ ... ]                                          | Index of active rotational axes (This parameter is computed based on maskTot)                                            |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| NumberOfAxis                       | 0                                                | The number of active axes of the machine (This parameter is computed based on maskTot)                                   |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| NCart                              | 0                                                | The number of active cartesian axes (This parameter is computed based on maskTot)                                        |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| NRot                               | 0                                                | The number of active rotational axes (This parameter is computed based on maskTot)                                       |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+
| D                                  | 0                                                | The scaling matrix used to adjust cartesian value w.r.t to rotational ones (This parameter is computed based on maskTot) |
+------------------------------------+--------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------+


Optimization (cfg.opt)
~~~~~~~~~~~~~~~~~~~~~~

.. warning::
  Some parameters are used for debugging purposes in Matlab. They have not been tested at runtime on a target :
  
  * USE_LINPROG
  * FIND_REASON_INFEASIBILITY

.. warning::
  The following parameters can lead to unstable behaviors at runtime. They lack some robustness, and more improvement is necessary. However, sometimes they can be quite handy, 
  so they are made available for well-skilled users. Normal users should not change these parameters :
  
  * USE_LENGTH_SCALING
  * FORCE_POSITIV_COEFFS

+----------------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter                  | Default value | Description                                                                                                                                                                                                                                                        |
+============================+===============+====================================================================================================================================================================================================================================================================+
| ACC_RAMP_OVER_WINDOWS      | 0.999         | Small ramp on the acceleration constraints. This parameters is used to avoid numerical issue arising during the recending horizon optimization. A value of 1.0 disable its effect. A value close to 0.0 will decrease the optimality of the solution. : [0.0, 1.0] |
+----------------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| FIND_REASON_INFEASIBILITY  | TRUE          | Enable additional optimisation to test which constraints leads to a failure during the optimiation.                                                                                                                                                                |
+----------------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| FORCE_POSITIV_COEFFS       | TRUE          | Ensure the positivity of the coefficients during and after the optimization. The robustness of the resampling step is improved. However, some constraints migth be locally violated.                                                                               |
+----------------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| SLACK_PENALTY              | 10000         | Slack penalty. The higher its value, the more the depassement of jerk constraints will be penalised. A higher value leads to a relaxation of the jerk constraints only when it is absolutly necessary while preserving the validity of the other ones.             |
+----------------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Type                       | LP            | Optimization technique used. Only the linear optimization (LP) is available for now.                                                                                                                                                                               |
+----------------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| USE_LINPROG                | FALSE         | TRUE : Use the linprog solver of Matlab (Interrior Point Method). FALSE : Use the embedded version  COINOR (simplex).                                                                                                                                              |
+----------------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| USE_JERK_CONSTRAINTS       | TRUE          | Enable the use of jerk constraints. TRUE means the second optimiation step will be performed with jerk constraints.                                                                                                                                                |
+----------------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| USE_LENGTH_SCALING         | FALSE         | Enable the use of rescaling of the BSpline coefficients used in the optimization. It might help improving the numerical stability and thus the optimality of the solution. The scaling is based on the length of each curves.                                      |
+----------------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| VEL_RAMP_OVER_WINDOWS      | 0.999         | Small ramp on the velocity constraints. This parameters is used to avoid numerical issue arising during the recending horizon optimization. A value of 1.0 disable its effect. A value close to 0.0 will decrease the optimality of the solution. : [0.0, 1.0]     |
+----------------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+


Cusp (cfg.Cusp)
~~~~~~~~~~~~~~~

+---------------+---------------+-----------------------------------------------------------------------------------------------------------------+
| Parameter     | Default Value | Description                                                                                                     |
+===============+===============+=================================================================================================================+
| CuspThreshold | 10°           | Heuristic value used as a threshold. This is an angle between two consecutive vectors of a given curve.         |
+---------------+---------------+-----------------------------------------------------------------------------------------------------------------+
| Skip          | FALSE         | Enable the skipping of the cusp step.                                                                           |
+---------------+---------------+-----------------------------------------------------------------------------------------------------------------+

Compressing (cfg.Compressing)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+-------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter         | Default Value | Description                                                                                                                                                                                                                   |
+===================+===============+===============================================================================================================================================================================================================================+
| ColTolCosLee      | cosd(10)      | Heuristic value used as a threshold. This is cosinus of an angle (indicator of the colinearity of the vectors) between two consecutive vectors of a given curve. This parameter is used a criteria in the creation of batches.|
+-------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Skip              | FALSE         | Enable the skipping of the Compressing step.                                                                                                                                                                                  |
+-------------------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Smoothing (cfg.Smoothing)
~~~~~~~~~~~~~~~~~~~~~~~~~

+-----------------+---------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter       | Default Value | Description                                                                                                                                                                                                                |
+=================+===============+============================================================================================================================================================================================================================+
| ColTolCosSmooth | cosd(10)      | Heuristic value used as a threshold. This is cosinus of an angle (indicator of the colinearity of the vectors) between two consecutive vectors of a given curve. This parameter is used to check the G1 condition.         |
+-----------------+---------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ColTolSmooth    | 1E-5          | Heuristic value used as a threshold. This parameter is used to check the C0 condition.                                                                                                                                     |
+-----------------+---------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Skip            | FALSE         | Enable the skipping of the Smoothing step. An exact stop will be forced between two consecutive curves that are not G2.                                                                                                    |
+-----------------+---------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+


Code Generation 
---------------

Matlab provides specific tools for C / C++ code generation. An explanation of these tools are used in the project is provided in :ref:`Coder <coder>`.

Code Generation Test and Debugging (Developer)
------------------------------------------------

In the current release of the opencn, there are two test-apps related to feedrate planning and ngc interpretation:

How to Debug and Test the NGC Interpreter?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Folder**: ``agency/usr/rs274ngc/standalone_app``

The application is a standalone version of the component feedopt (without the feedrate planning algorithm). It performs the following steps:

- Open and read the ngc file
- Interpret all the commands present in the file
- Exit once the whole file has been parsed

During the process, some warning messages are outputted in the terminal. Additional print messages can be added to improve debugging and the development of new features.

To compile the application, refer to the help of ``build.sh``. By default, the ngc used for the test is ``test.ngc``.

How to Debug and Test the Generated Code from Matlab Coder?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Folder**: ``agency/usr/target/components/feedopt/matlab_app``

The application is a standalone version of the component feedopt (with the feedrate planning algorithm). It performs the following steps:

- Parse the entire ngc file using the interpreter
- Perform all the steps of the feedrate planning algorithm
- The optimal trajectory is then discretized during the resampling
- The result is written in a CSV file and can directly be analyzed in Matlab

To compile the application, refer to the help of ``build.sh``. By default, the ngc used for the test is ``test.ngc``. The application code can be modified to perform only some part of the whole computation chain of the feedrate planning.

Notice that both application codes can be run in debug mode in vscode. It is easier and quicker to first develop and test a new feature and then try it on the target.

Acknowledgements
----------------
=================== =============================================== ========================================================================================================================================================================================================================================
Matlab function(s)    Functionality                                 Citation
=================== =============================================== ========================================================================================================================================================================================================================================
lgwt.m                Legendre-Gauss Quadrature Weights and Nodes   Greg von Winckel (2022). `Legendre-Gauss Quadrature Weights and Nodes <https://www.mathworks.com/matlabcentral/fileexchange/4540-legendre-gauss-quadrature-weights-and-nodes>`_, MATLAB Central File Exchange. Retrieved February 11, 2022. 
=================== =============================================== ========================================================================================================================================================================================================================================
