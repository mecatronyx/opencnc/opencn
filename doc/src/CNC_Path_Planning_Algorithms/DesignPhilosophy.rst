.. _DesignPhilosophy:

.. note::

  A quick presentation of the algorithms used in the first release is available in videos ( `Software architecture <https://www.youtube.com/watch?v=U7JCOAdtCOc>`_, 
  `Trajectory optimization <https://www.youtube.com/watch?v=nUdvrO6lsX8>`_ )


Path planning concerns the generation of time optimal setpoint values subject to constraints 
regarding speed, acceleration, jerk, and geometrical tolerances w.r.t. the programmed path defined by a G code file (RS274).


- The path planning of OpenCN is based on **subsequent**, i.e. separate optimization of :ref:`geometry <Geometrical_Operations>` and :ref:`feedrate <Feedrate_Planning>`.
  Simultaneous optimization of geometry and feedrate would have been much more complicated and would have led to nonconvex NLP problems with less solver reliability.
- A special emphasis is put on the utilization of analytical solutions, convex formulations of optimization 
  problems, and proven numerical algorithms for standard mathematical problems allowing a high reliability of the entire computation chain.
- Numerization takes place only at the very end of the computation chain. Curve pieces are represented analytically. This allows exact calculation of higher derivatives.
  
The following figure depicts the global computation chain of the path planning which is explained in detail in the following chapters.

.. _fig-overview:

.. figure:: /pictures/Overview_PathPlanning.png
   :align: center
   :scale: 60 %
