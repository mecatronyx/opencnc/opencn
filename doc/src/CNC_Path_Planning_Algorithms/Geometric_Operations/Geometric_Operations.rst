
.. _Geometrical_Operations:

####################
Geometric Operations
####################

The interpretation of G code (RS274) yields a list of parametric curve pieces called ``CurvStructs`` and described by 
:math:`{\bf r}_k(u_k), \; k=1, \ldots, N`, 
where each curve abscissa :math:`u_k` is normalized :math:`0 \le u_k \le 1`.
There is no discretization at this point, each ``CurvStruct`` is an analytical representation of a curve piece. 
We call the derivatives of :math:`{\bf r}` with respect to :math:`u` the `parametric derivatives`.
The curve abscissa :math:`u` depends on time :math:`t`; this dependency :math:`u(t)` is treated in :ref:`Feedrate_Planning`. 
Position :math:`{\bf r}(u)`
and parametric derivatives :math:`{\bf r'}(u), {\bf r''}(u), {\bf r'''}(u)` can be 
evaluated at arbitrary values of parameter :math:`u` using the function ``EvalCurvStruct``.
Values of :math:`u` outside the closed interval :math:`[0, 1]` are disallowed.

Geometric :math:`G^2` continuity
--------------------------------

In order to avoid discontinuities in the normal acceleration when passing from one curve piece to the next,
curvature :math:`\kappa` must be continuous and the parametric derivatives must be colinear at the end of one curve 
piece and the beginning of the next one. This is commonly called geometric :math:`G^2` continuity.

.. figure:: /pictures/Geometric_Continuity.png
   :name: _fig-GeometricContinuity
   :align: center
   :scale: 80 %

   Geometric Continuity

As shown in figure :numref:`_fig-GeometricContinuity` the curve pieces defined by G code are only :math:`G^0` or in the best case :math:`G^1`.
This explains the need for having smooth :math:`G^2` transitions achieved by :ref:`SmoothCurvStructs`.

.. _G_Code_Interpreter:

G code interpreter RS274
------------------------

The G code interpreter RS274 is a LinuxCNC legacy written in C++.
The ``*.ngc`` G code file is parsed, and a ``CurvStruct`` is generated in a callback for each parsed G code command.

We recommend G codes with a maximum number of digits for high resolution. 
The unit being in millimetres, we recommend 6 digits after the decimal point leading to a theoretical 1 nanometer resolution.

A special attention is payed to circular arcs where the G code includes the coordinates of the center point.
This over parametrization leads to a potential discontinuity due to roundoff errors.
In order to avoid this problem, the function ``CorrectArcCenter`` recalculates the radius and the center point coordinates before creating 
a ``Helix`` ``CurvStruct``.

Available ``CurvStruct`` types
------------------------------

Four different types of ``CurvStructs`` are implemented:

- ``Line``
- ``Helix``  
- ``TransP5``
- ``Spline`` 

A common data for each ``CurvStruct`` type is the geometric start point :math:`P_0` and end point :math:`P_1`.
Otherwise the encoding of geometric information differs between the four ``CurvStruct`` types. 

Each ``CurvStruct`` has its own maximum feedrate inherited from the G code.
This information is **only** used in the feedrate planning.

Another information stored in the ``CurvStruct`` is the zero speed mode ``ZSpdMode``.
There are four possibilities: ``NN`` no zero speed at both ends of the curve piece, 
``ZN`` zero speed only at the start point, 
``NZ`` zero speed only at the end point, 
and ``ZZ`` zero speed at both start and end points of the curve piece. This information comes first during the cusp detection in ``CheckCurvStructs``.
In case of a detected cusp, no ``TransP5`` transition curve is inserted, and the feedrate planning makes a full stop at the cusp.
  
Line
^^^^
A line segment corresponds to a ``G01`` command in the G code. 
The function ``ConstrLineStruct`` constructs this type of ``CurvStruct``.
The end points :math:`P_0` and :math:`P_1` in :math:`R^3` determine the line segment, and the corresponding curve piece is parametrized by 
:math:`{\bf r}(u) = u P_1 + (1-u) P_0`. This is realized by the function ``EvalLine``. 

Helix
^^^^^
A special case of a helix is an arc of circle in the plane corresponding to either a ``G02`` or ``G03`` command in the G code.
The helix is parametrized by the start point :math:`P_0`, the end point :math:`P_1`, the unit vector :math:`e_{\mbox{vec}}` 
about which the rotation takes place, the total angle of rotation :math:`\theta` and the pitch of the helix.
The function ``ConstrHelixStruct`` constructs the ``CurvStruct`` ``Helix``.
Evaluation of position and parametric derivatives is realized by the function ``EvalHelix``.


TransP5
^^^^^^^

``TransP5`` is a degree 5 polynomial used for :math:`G^2` smoothing of transitions between curve pieces.
It is calculated by ``G2_Hermite_Interpolation`` proposed by :cite:t:`Herzog2019`. 
The function ``ConstrTransP5Struct`` constructs the ``CurvStruct`` with a type ``TransP5``.
Evaluation of position and parametric derivatives is realized by the function ``EvalTransP5``.


.. _Spline:

Spline
^^^^^^

The function ``ConstrBSplineStruct`` constructs the ``CurvStruct`` with a type ``Spline``.
Evaluation of position and parametric derivatives is realized by the function ``EvalBSpline``. 
The control points of the spline are stored externally, not inside the ``CurvStruct`` with the use of a pointer named ``sp_index``.
This pointer access memory allocated outside the Matlab Workspace and thus needs to be freed at the end of the program execution. 

CheckCurvStructs
----------------

The entire list of ``CurvStructs`` is checked for cusps, and the result is stored in the ``ZSpdMode`` fields for later usage.
A cusp is detected if the angle between tangents of adjacent curve pieces is less than ``CuspThreshold``. 
In the case of five axis machine kinematics, the detection is performed in the piece frame (WCS). 

.. _CompressCurvStructs:

CompressCurvStructs
-------------------

CAM (including PostPro) programs often generate G code with a lot of tiny G01 line segments.
Obviously in this case there is an underlying smooth curve with a lack of information due to discretization.
It would be very inefficient to insert a ``TransP5`` transition between many tiny line segments.
For this reason, this situation is treated by compressing the line segments into a B-Spline of order define by the parameter ``SplineDegree``
using the knot distribution proposed by :cite:t:`Lee1989`.
By doing so, the lack of information due to CAM discretization is partially recovered.
This function aims to detect the situation where a huge amount of tiny line segments is detected in the G-code. 
The groups of tiny line segments are created based on the following criterion: 

- The individual length of each segment is smaller than ``LThreshold``
- Two consecutive curves are collinear w.r.t the given margin ``Compressing.ColTolCos`` 
- No cusp is detected between two line segments

The Compressing feature can globally be turned off using parameter ``Compressing.Skip``.

.. note::
   To suite the requirements of five axes machining, some heuristice has been added to the compressing (reinterpolation with BSpline of the small line segments).
   The resulting implementation is not optimal and still required some improvement to reach the standard of modern CNC.

.. _SmoothCurvStructs:

SmoothCurvStructs
-----------------

The geometrical discontinuities in the programmed G code path must be removed by rounding, sometimes called blending, in order to prevent productivity 
decrease and a potential excitation of machine vibrations. 
Therefore, a part of the abutted curves on both sides 
of the discontinuity must be cut out corresponding to parameter ``CutOff``, and the missing parts must be completed by a transition curve with 
geometrical :math:`G^2` continuity at the points where the transition curve rejoins the programmed path.
This procedure is called `optimal` :math:`G^2` `Hermite interpolation`.
Following :cite:t:`Herzog2019`, the optimal transition curve leads to quintic polynomials. 
For the determination of ``TransP5`` coefficients the `positive` roots of a degree 9 polynomial must be determined.
This computation is coded in ``G2_Hermite_Interpolation``.

The calculation of a ``TransP5`` transition fails under certain conditions, e.g. transitions between collinear line segments are not possible and not useful.
As shown in :cite:t:`Herzog2019`, a cusp-free transition may also fail if parameter ``CutOff`` is too large.

The shortening of the abutted curve pieces is a trivial reparametrization in case of ``Line`` and ``Helix``.
However, in case of a ``Spline`` the calculation is more difficult.
The determination of a segment length :math:`\int_{u_1}^{u_2} |{\bf r'}(u)| du` has no analytical solution.
For this reason, the length is approximated by numerical Gauss-Legendre integration in ``SplineLengthApproxGL_bounds``.
The cut-off needs to know which value of curve abscissa :math:`u` corresponds to which given ``CutOff`` length.
This is achieved via a bisexion algorithm iteration coded in ``SplineLengthFindU_up`` and ``SplineLengthFindU_down``.

.. note::
   In the current implementation, a failure during the computation of the transition leads to a forced zero stop.

.. _SplitCurvStructs:

SplitCurvStructs
----------------
The length of curve pieces as described by the G code can vary between many order of magnitudes, e.g. between 1 micron and 100 millimetres.
For different reasons, the feedrate planning cannot cope with this heterogeneity.
In order to homogenize the curve lengths, too long curve pieces are split up into smaller pieces.
In some sort, the ``SplitCurvStructs`` is the inverse operation of ``CompressCurvStructs``.
It is not necessary that each curve piece has exactly the same length.

Reparametrization of the smaller curve pieces is easy, except for ``Spline``.
For this reason, two additional parameters a and b are introduced defining an affine function :math:`\tilde{u} = a u + b`.
Splitting a long ``Spline`` into smaller pieces permits to multiple ``CurvStructs`` to share the same control points stored externally.
The only parameters which vary are the coefficients a and b.
