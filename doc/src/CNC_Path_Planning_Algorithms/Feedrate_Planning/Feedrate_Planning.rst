
.. _Feedrate_Planning:

#################
Feedrate Planning
#################

Formulation of the Feedrate Optimization Problem
------------------------------------------------

The time derivatives :math:`\frac{d^n}{dt^n} \, {\bf r}(u(t))` can be calculated by applying chain and multiplication rule :

.. math::
   :label: eq_derivatives
   :nowrap:

    \begin{eqnarray*}
    \dot{\bf r}     & = &   {\bf r}' \: \dot{u}\\
    \ddot{\bf r}    & = &   {\bf r}'' \: \dot{u}^2 \; + \; {\bf r}' \: \ddot{u} \\
    \dddot{\bf r}   & = &   {\bf r}''' \: \dot{u}^3 \; + \; 3 \, {\bf r}'' \: \dot{u} \, \ddot{u} \; + \; {\bf r}' \, \dddot{u}.
    \end{eqnarray*}
 


Here, the prime :math:`'` denotes the derivative w.r.t. :math:`u` and the dot :math:`\dot{}` corresponds to a derivative w.r.t. time.
The quantities :math:`\dot{u}`, :math:`\ddot{u}` are sometimes called `pseudo` velocity respectively `pseudo` acceleration.


It already was observed by :cite:t:`Verscheure2009` that by applying a nonlinear change in variable :math:`q(u) := \dot{u}^2, \ddot{u}= \frac{1}{2} q'`, 
acceleration :math:`\ddot{\bf r}` becomes a linear function of the new unknown :math:`q(u)`, and the time optimal control problem `without` jerk constraint becomes convex.

.. math::
   :label: eq_contraints
   :nowrap:

    \begin{eqnarray*}
    q(u)  \: & < &   \:v_{\max}^2 \, / \, ||{\bf r'}(u)||^2  \\
    {\bf -a_{\max}}  \: & < &  \: q(u) \, {\bf r''}(u) + \frac{1}{2} q'(u) \, {\bf r'}(u)   \:  <  \: {\bf a_{\max}}, 
    \end{eqnarray*}

where the latter inequality is understood componentwise.
:cite:t:`Erkorkmaz2017` showed that the resulting problem `without jerk constraint` can be cast as a linear program (LP), 
observing that minimizing the travel time :math:`T` is equivalent to maximizing :math:`\int_0^1{q(u) \, du}` which is linear in the decision variables. 
For this purpose, the unknown function :math:`q(u)` is approximated by a B-spline of degree :math:`d` and a given knot vector.
The new unknowns, also called decision variables are the coefficients :math:`{\bf x}` of the B-spline.
More precisely, for the optimisation over one horizon, as explained in :ref:`RecedingHorizon`, the decision variables regrouped in vector :math:`{\bf x}`
correspond to the concatenation of all spline coefficients.

It is advantageous for various reasons to formulate the feedrate problem as a `convex` optimization problem.
However the jerk constraints are intrinsically non-convex due to a square root :math:`\sqrt{q}` appearing in the formulas, refer to :cite:t:`Erkorkmaz2017`.
Two possible ways of handling the non-convex jerk constraints are described below. 

Handling of the jerk constraints in the actual version of OpenCN
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As proposed by :cite:t:`Erkorkmaz2017`, the feedrate planning is solved in a first step `without` jerk constraints.
This amounts to solving a linear program (LP).
Then, the term :math:`\sqrt{q(u)}` is replaced by a precomputed upper bound :math:`\sqrt{q^{\star}(u)}` being the solution
of the feedrate planning in the first step, i.e. without jerk constraint. 
Then in a second step, a new LP is formulated adding the jerk constraints.

This approach corresponds to a conservative approximation of the jerk constraint compatible with the LP setting, and taking the standard form :

.. math::
   \mbox{minimize}_{\bf \, x} \; \;  {\bf c}^T \, {\bf x} \quad \mbox{subject to} \; \left\{ \begin{array}{l} 
   {\bf A \, x} \, <  \, {\bf b} \\
   {\bf A_{\mbox{\scriptsize eq}}} \, {\bf x} \, =  \, {\bf b_{\mbox{\scriptsize eq}}}.  
   \end{array} \right. 
   :label: eq_LP

OpenCN uses the Simplex solver CLP (COIN-OR) for LP solving.
The downside of this approach is the fact that two consecutive LP problems must be called, 
the latter being much more expensive since the jerk constraints are added.
Another disadvantage is that the used Simplex solver does not allow a `warm start` utilizing the solution of a former horizon.
In the actual implementation of OpenCN, the feedrate planning is by far the most computationally expensive part.


Handling of the jerk constraints in future versions of OpenCN 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It is not possible to cast the jerk constraints into a convex optimization problem.
Therefore, it must be treated by some sort of approximation.
One way of treating jerk constraints consists of adding a regularization term into the objective funtion :math:`J` to be `minimized`:

.. math::
   J = - \int_0^1{q(u) \, du} \; + \; \gamma \cdot \int_0^1{{{q}''(u)}^2 \, du}.
   :label: eq_QP_loss
  
Here, :math:`\gamma` is a regularization parameter to be choosen.
The problem above can be transformed to a standard quadratic program (QP):

.. math::
   \mbox{minimize}_{\bf \, x} \; \;  \frac{1}{2} {\bf x^T \, P \, x} \; + {\bf c^T \, x} \quad \mbox{subject to} \quad {\bf l \; \le \; A \, x \; \le \; u}.
   :label: eq_QP

QP optimization problems are very well understood, and many real-time solvers exist (e.g. qpOASES, FORCES, OSQP :cite:t:`Stellato2020`), 
since model predictive control (MPC) also relies on solving QP's in real-time.
This new feedrate planning approach is under investigation.

  
.. _recedingHorizon:

Receding Horizon
----------------

A G-code is typically composed of a very high number of curve pieces, and it is not possible to compute the optimal feedrate planning
of the entire trajectory in a single shot.
Therefore, the feedrate planning is subdivided in small chunks where only ``NHorz`` curve pieces are optimized simultaneously.
Each curve piece has its own parametrization :math:`{\bf r}_k(u_k), \; k=1, \ldots, N`, and the parametric derivatives at the end
of one piece and at the beginning of the next piece might disagree.
Equality constraints must be introduced in the optimization problem in order to assure the continuity of velocity and tangential acceleration
at the border of each piece. These continuity conditions in the receding horizon procedure are illustrated by the red bullets in figure :numref:`_fig-Receding_Horizon`.  

.. figure:: /pictures/Receding_Horizon.png
   :name: _fig-Receding_Horizon
   :align: center
   :scale: 60 %

   Feedrate Planning with Receding Horizon



At the very first curve piece, initial velocity :math:`v_{0,0}` and initial tangential acceleration :math:`a_{t0,0}` are zero.
Final velocity and final tangential acceleration at the end of the horizon are always assumed to be zero.
This means that we do as if it is already the end of the trajectory, which is obviously not the case.
This conservative assumption will have very little effect on the optimal solution of the first curve piece inside the horizon.
Therefore, at iteration :math:`k`, only the solution :math:`q_k(u)` of the first piece inside the horizon is retained.
The solution of the remaining pieces may be used as an initial guess to warm start the next solver call at iteration :math:`k+1`.
Now the horizon is shifted as shown by figure :numref:`_fig-Receding_Horizon` for the case of ``NHorz = 5``.
The new left hand boundary conditions :math:`v_{0,k+1}` and tangential acceleration :math:`a_{t0,k+1}` are heritated from the retained
solution of the iteration before.

.. _Feedrate_Planning/zero_speed_condition:

Handling of Zero Speed Condition
--------------------------------

Zero speed typically occurs at the very beginning and the very end of the trajectory but it can also happen somewhere in between.
In case of a detected cusp or when the G61 exact path command is used in the G-code the transition, ``TransP5`` is not inserted 
between the adjacent curve pieces. This way, the information of a desired stop in the trajectory is preserved.

Additionally, a start at zero speed (ZN) is a singularity for the optimization which must be treated separately. 
Detailled information about this specificity has been discuted in the `Forum <https://discourse.heig-vd.ch/t/feedrate-planning-at-zero-speed/260/2>`_. 
The retained approach to this problem is based on the imposition of a constant pseudo jerk :math:`j_{ps}` during the short pieces 
of curve around standstill. 

.. math::
   :label: eq_psJerk
   :nowrap:

   \begin{eqnarray*}
   &u         & =  \frac{1}{6} j_{ps} \: t^3 \\
   &\dot{u}       & =   \frac{1}{3} j_{ps} \: t^2 \\
   &\ddot{u}      & =  j_{ps} \: t
   \end{eqnarray*}

To ensure the continuity of the curve (c.f. figure :numref:`_fig-Receding_Horizon`), the receiding horizon approach required the 
knowledge of desired speed and tangential acceleration at both the start and the stop of each curve processed over the horizon 
window. A special care is thus required in presence of zero speed segments (ZN, NZ). The speed and acceleration are computed based 
on a pseudo jerk (see Eq. :eq:`eq_psJerk` and Eq. :eq:`eq_derivatives`). Finally, the resulting speed and acceleration are imposed as equality constraints in the 
optimization problem. 

.. note::
   Notice that sometimes the resulting optimization problem fails due to infeasibility of the formulated problem. 
   Work around based on the relaxation of some constraints and additional heuristic have been implemented leading on a better success rate during the optimization.

Resampling and Generation of Setpoint Values
--------------------------------------------

Denoting the sampling period by :math:`\Delta t`, :math:`u_k := u(t_k)` and :math:`u_{k+1} := u(t_k + \Delta t)`, 
the truncated Taylor expansion of :math:`u(t)` yields 

.. math::  
   u_{k+1} = u_k + \sqrt{q(u_k)} \: \Delta t \, + \, \frac{1}{4} q'(u_k) \: \Delta t^2.
   :label: eq_taylor



If :math:`u_{k+1} > 1`, a transition to the next curve piece must take place. 
To this end, we first calculate the elapsed time :math:`T_r` from the previous point :math:`u_k` to the end point :math:`u = 1`.
If :math:`q(u)` is approximated by a piecewise linear function, analytic integration gives :

.. math::
   T_r = \frac{2 \, (1 - u_k)}{\sqrt{q(1)} + \sqrt{q(u_k)}}.
   :label: eq_Ttransition


The sampling of the next curve piece must be started with a shortened value :math:`\Delta t_0 = \Delta t \, - \, T_r` 
using Eq. :eq:`eq_taylor`.

.. note::
   In the current implementation of the algorithm, the numerical resolution of the resampling integral has been modified. 
   Hence, some modifications has been added to deal with numerical issues arissing from floating points arithmetic.
