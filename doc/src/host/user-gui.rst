.. _user_gui:

########
User-Gui
########

This application is used to control the Micro3 (M3) milling machine available at
HEIG-VD.

.. warning::

	Under construction

************
Installation
************

The following packages needes to be install to build the *usr-gui* application:

.. code-block:: shell

	$ sudo apt install coinor-libclp1 coinor-clp coinor-libclp-dev libgsl23 \
	  libgsl-dev  qt5-default net-tools


***********
Application
***********

The application is  built in ``<OPENCN-ROOT>/agency/usr/build-host/bin/`` folder.

.. code-block:: shell

	Usage: ./user-gui [options]
	OpenCN GUI

	Options:
	  -h, --help         Displays this help.
	  -i, --ip <ip>      target's IP address
  -p, --port <port>  target's IP port

When no options are set, the GUI asks for an IP:port in a dedicated Window.
