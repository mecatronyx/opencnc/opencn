.. _tooledit:

########
Tooledit
########

This Tcl/Tk application is used to create and edit tool tables files. First you must install the 
following packages:

:code:`sudo apt install tcl bwidget`

The tooledit launch script is located in "build-host/bin".
To edit a tool table use:

:code:`./tooledit.sh <path to tool table file>`

To create a new tool table use:

:code:`./tooledit.sh -n <path to tool table file>`

This Tcl applet and the tool tables were imported from LinuxCNC more information can be found:

* `Tool table  (version 2.4.x and later) <http://wiki.linuxcnc.org/cgi-bin/wiki.pl?ToolTable>`_
* `Tool edit <http://linuxcnc.org/docs/html/gui/tooledit.html>`_

