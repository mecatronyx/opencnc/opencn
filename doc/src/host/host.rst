.. _host:

###########
OpenCN Host
###########

.. toctree::
   :maxdepth: 5
   :hidden:

   user-gui
   tooledit

OpenCN framework offers the possibility to control an OpenCN device, called **Target**
with application running on a remote PC, called **Host**.

Existing OpenCN *Remote Apps*:

* :ref:`user-gui  <user_gui>`
* :ref:`tooledit <tooledit>`

Here is the command to build the *host* apps:

.. code-block:: shell

	$ cd <OPENCN-HOME>/agency/usr
	$ ./build-host.sh [-c]

The results can be found:

* Binaries: ``<OPENCN-HOME>/agency/usr/build-host/bin``
* Python module: ``<OPENCN-HOME>/agency/usr/build-host/python``

