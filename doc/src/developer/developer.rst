.. _developer:

Developer Documentation
#########################

.. toctree::
    :maxdepth: 6
    :hidden:
   
    code_map
    drawio
    logging
    debugging
    dev_flow
    misc
    ci
    target_root_filesystem
    

| This section is dedicated to anyone aiming to help developement in OpenCN.
| The different section below contain help for debugging components, developement flow, tips etc : 
 
* :ref:`Coding Map <code_map>`
* :ref:`Drawio <drawio>`
* :ref:`Logging help <logging>`
* :ref:`Various debugging help, tool & tips <debugging>`
* :ref:`Developpement flow <dev_flow>`
* :ref:`Miscellaneous  <misc>`
* :ref:`Continuous Integration <ci>`
* :ref:`Target Root Filesystem <target_rootfs>`
