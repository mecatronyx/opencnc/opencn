
.. _drawio:

Drawio
#################

| DrawIO is the prefered way to present scheme and diagram in OpenCN as it's free & open-source.
| The file is saved as ``.drawio`` and is automatically converted to ``.svg`` by the *sphinxcontrib.drawio* plugin
| This section present some usage, tips & tricks.

***************************
Include a DrawIO in the doc
***************************

Here are the steps to include a drawIO image to the documentaion :

1. Go to `drawIO app <https://app.diagrams.net/>`_
2. Make your scheme/diagram
3. Download as ``.drawio``
4. Place it in a proper location under ``<Repo>/doc/src/....``
5. Integrate it in the wished ``.rst`` with : 

.. code-block::

    .. drawio-figure:: <relative_path>/myDrawIO.drawio
        :page-name: <your-well-named-page>
        :alt: <A nice description>
        :align: center

*********************************
Proper text color in exported SVG
*********************************

Sometimes, when giving colors to text item, they do not properly export in ``.svg``.
Here is a a quick step by step to ensure proper color display in .svg :

1. Select the text
2. On the right side, select this style : 

.. figure:: _pictures/drawio_textCol_1.png
    :alt: <A nice description>
    :align: center

3. Select your color using this tool : 

.. figure:: _pictures/drawio_textCol_2.png
    :align: center


4. Export, integrate, enjoy
