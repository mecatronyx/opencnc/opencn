.. _code_map:

########
Code Map
########

This section present the different location of where to find what about OpenCN source code

********************
Tree
********************

This is an overview of the OpenCN file repository.

.. code-block:: shell

    Repo
    ├── agency
    │   ├── avz                 # Agency VirtualiZer, an hypervisor. It is only used on arm and arm64 during boot.
    │   └── filesystem          # Script for image mount of opencn
    │   └── linux               # Linux kernel
    │   │   ├── ...
    │   │   ├── opencn          # OpenCN specific addition : components, HAL, lib, etc.. for kernel side
    │   │   └── ...
    │   └── rootfs              # Root filesystem, configuration for target, HAL config file, etc..
    │   └── target              # its files for target
    │   └── usr                 # Userspace side for target, Host(GUI) & server interface
    │   │   ├── common          # Target<->Host communication configuration
    │   │   ├── host            # Host GUI
    │   │   │   ├── user_gui    # 3 axis milling machine GUI
    │   │   │   ├── user_m5_gui # 5 axis milling machine GUI
    │   │   │   └── ...
    │   │   ├── matlab          # Path planner (feedopt) submodule for trajectory optimization
    │   │   ├── rs274ngc        # G-code interpreter
    │   │   ├── target          # Userspaceside side of target, OpenCN components, tools like halcmd, ethercat, etc..
    │   │   └── ...
    ├── bsp                     # Board support package (bsp) for different targets
    ├── buildroot               # buildroot sources
    ├── doc                     # OpenCN sphinx documentation
    │   ├── build               # HTML & PDF generation of documentation
    │   ├── src                 # Source file for documentation
    │   └── ...
    ├── docker                  # Docker for compilation & deploy
    ├── PostPro                 # Configuration file for Computer-Aided Manufacturing (CAM)
    ├── qemu                    # Qemu sources
    ├── scripts                 # Scripts used in the framework
    └── u-boot                  # u-boot source tree

********************
Source documentation
********************

| The source file documentation is stored under ``<Repo>/doc/src/``.
| The generated HTML documentation : ``<Repo>/doc/build/html/``.

****************************************
Host / GUI / Target<->Host interface
****************************************

There are 2 differents GUI available : 

| Micro3 milling machine : ``<Repo>/agency/usr/host/user_gui/``
| Micro5 milling machine : ``<Repo>/agency/usr/host/user_m5_gui/``

The communication interface configuration file is common to all GUI, and can be found under : 

``<Repo>/agency/usr/common/opencn_interface/opencn_interface.capnp``

**********
Components
**********

OpenCN components pocess *userspace* code that will call *kernel* / *RT* code.
Some component in userspace only load the *kernel* side and do nothing more on the *userspace* side. 
While other have workers implement to do specific task on *userspace* side like feedopt (a pathplanner component)
that will calculate the optimized path on *userspace* side before sending it to the *RT* part through a buffer.

| Userspace component location : ``<Repo>/agency/usr/target/components/<component_folder>/<component_name>.cpp``
| Kernel/RT component location : ``<Repo>/agency/linux/opencn/components/<component_folder>/<component_name>.c``

Please note that usually, userspace side have ``.cpp`` extension, - even tough you can write it in plain C if you like - 
while the kernel/RT side use ``.c`` extension.

*************************
Validation / Testing file
*************************

Testing and validation files for components or any OpenCN sould be put there : 

``<Repo>/agency/rootfs/board/common/rootfs_overlay/etc/opencn/validation/....``

********************
Rootfs / Config file
********************

You can change/add files of the rootfs of the target in the following directories : 

| Common files to all target : ``<Repo>/agency/rootfs/board/common/rootfs_overlay/``
| Target specific files : ``<Repo>/agency/rootfs/board/<TARGET>/rootfs_overlay/`` where <TARGET> is, see :ref:`environment`.

For example, the configuration files of the Micro5 milling is stored in : ``<Repo>/agency/rootfs/board/<TARGET>/rootfs_overlay/root/micro5/``