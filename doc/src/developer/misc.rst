
.. _misc:

#############
Miscellaneous
#############

**********************
External documentation
**********************

* :download:`OpenCN framework specification <../pdf/OpenCN_AMP_Specifications_v2019_6.pdf>`

*********************
Sphinx external links
*********************

* `sphinx web site <https://www.sphinx-doc.org/en/master/>`_
* `Sphinx documentation style guide <https://documentation-style-guide-sphinx.readthedocs.io/en/latest/style-guide.html>`_
* `Documenting Your Project Using Sphinx <https://pythonhosted.org/an_example_pypi_project/sphinx.html>`_

**********************
General external links
**********************

* `LinuxCNC <http://linuxcnc.org/>`_
