.. _logging:

##################
Logging facilities
##################

Logging messages from the RT domain can lead to unexpected latencies and cause troubles. For this reason,
the standard ``printk()`` or ``lprintk()`` functions are re-rerouted to a specific frontend driver called ``vuart``
which interacts with a non-RT backend counterpart.

On x86 platform, the logging infrastructure is slightly different since no (avz) hypervisor is running and is
replaced by an emulated version. The mechanism is known as ``vlog``.

================
x86 VLOG Logging
================

Vlog is activated by means of an ``OPENCN_IOCTL_LOGFILE_ON`` ioctl commandperformed in the logfile application (see ``usr/core/logfile.c``).

The activation is done during a  ``S50opencn`` init script as soon as the rootfs has been mounted by the init application.
The logging can be disabled by invoking the ``logfile`` application with the ``-d`` argument.

As soon as the vlog is activated, all invocations to printk() or lprintk() leads to sending over the vlog frontend/backend which 
writes to a temporary file (``/var/log/opencn.log``). At the shutdown of vlog, the file is moved to ``root/opencn.log`` and stays persistent.

======================
Standard vuart logging
======================

The ``vuart`` logging is based on a frontend driver which is running in the RT domain (CPU #1) and a backend driver
running in the non-RT domain (CPU #0). It is fully similar to *vlog* under x86 configuration.

The same functionalities as for x86 are available; it is possible to configure the environment to get the output on screen
or in a file.

The logging (message outputs) are enabled by default.

Additionnally, the following functions are available in ``soo/include/soo/dev/vuart.h``:

Enabling the messages issued from *printk()/lprintk()* functions:

.. c:function:: void rt_log_enable(void);

Disabling the logs:

.. c:function:: void rt_log_disable(void);

Check if the logs are enabled or not: 

.. c:function:: bool rt_log_enabled(void);

============================================
Enabling/disabling RT logging from the shell
============================================

The ``logfile`` utility allows the user to enable/disable RT logging from the shell directly. Usage of this command is:

*To enable* the logging in the ``root/opencn.log`` file

.. code-block:: shell

   logfile -e

*To disable* the logging (flush the output buffer and close the file)

.. code-block:: shell

   logfile -d

.. note::

   When the RT domain messages logged into a file, it is possible to display
   it, at the same time, on the screen with the following commmand: 

   .. code-block:: shell

	  tail -f /var/log/opencn.log

