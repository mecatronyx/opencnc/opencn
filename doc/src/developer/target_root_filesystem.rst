.. _target_rootfs:

######################
Target root filesystem
######################

| We use `buildroot <https://buildroot.org/>`_ to make the root filesystem (rootfs).
| You can see  the sections below on how to interact with the rootfs and linux configuration/package.

================
Adding package
================

If you wish to add package & command to the target, follow these steps : 

* Go to ``<OpenCN-Repo>/agency/rootfs``
* Type ``make menuconfig``. You will then enter buildroot configuration tool.
* Select the package you wish to add.
* Save the file on the default path under ``<OpenCN-Repo>/agency/rootfs/.config``
* The default configuration for the target is stored under ``<OpenCN-Repo>/agency/rootfs/configs``. 
  We will modify them to add the package we just added.
* Do a diff (with `meld <https://meldmerge.org/>`_ for example) between ``<OpenCN-Repo>/agency/rootfs/.config`` and ``<OpenCN-Repo>/agency/rootfs/configs/<target_config_file>`` for all the supported target.
* Set the proper options (normally all of them).
* Save
* It's a good idea to clean the rootfs before rebuilding it, it does avoid some strange behavior . Run ``<OpenCN-Repo>/agency/build.sh -cr`` to do so.
* Run ``<OpenCN-Repo>/agency/build.sh -r`` to build the rootfs. This may take from ~30minutes up to 1 hours depending on your PC.
* Deploy with ``<OpenCN-Repo>/agency/deploy.sh -ru <your_device>``
* Test
* Enjoy

======================
Adding files to target
======================

| If you wish to add/modify common files to the target, you can add them under ``<OpenCN-Repo>/agency/rootfs/board/common/rootfs_overlay/<more_path_for_files>``
| The target specific files can be added/modified under ``<OpenCN-Repo>/agency/rootfs/<target>/common/rootfs_overlay/<more_path_for_files>``

=============================
Configuration change of linux
=============================

If you wish to change configuration of the linux, follow these steps : 

1. Go to ``<OpenCN-Repo>/agency/linux``
2. Type ``make menuconfig``. You will then enter the configuration tool.
3. Select the package you wish to add.
4. Save the file on the default path under ``<OpenCN-Repo>/agency/linux/.config``
5. Run ``make``. **WARNING** : Do not run ``<OpenCN-Repo>/agency/build.sh`` script afterwards, this will erase the last ``.config`` file.
   Please see point **8** for permanently changing the configuration.
6. Deploy using ``<OpenCN-Repo>/agency/deploy.sh -b <device>`` with ``device`` being your USB/SD card.
7. Test
8. If your happy with the configuration, you can permanently change this by doing a meld between your 
   ``.config`` file and ``<OpenCN-Repo>/agency/linux/arch/<ARCH>/configs/<config_file>`` where ``<ARCH>`` can be *arm64* or *x86*