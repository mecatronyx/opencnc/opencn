.. _ci:

######################
Continuous Integration
######################

=====================
Automatic runner
=====================

Here is a quick overview of the *CI/CD* deployed in the OpenCN repo. It uses three specific runners which we deployed on a private VM.

It has three stages containing one job each.

******************
build_docker stage
******************
This stage will build the docker image used by the rest of the pipeline. It uses a DinD image to be able to use Docker.  
It only has one job, *build_docker*, which is ran:

 * only on **master**
 * only on *Dockerfile* changes

***********
build stage
***********
This stage builds **Qemu**.

It only has one job, *qemu-build* which is can be ran manually on every branches.


************
deploy stage
************
This stage builds this doc and publishes it.

It only has one job, *pages* which is is ran only on master branch.

=====================
Continuous Validation
=====================

************************
Validation Script
************************

| This section is dedicated to the testing of the different OpenCN system.
| As of today, a very few system has dedicated testing script. Also, none of the script is automatically tested by the runner.
| For now, theses scripts is used to manually test things.
| Feel free to add any validation script under ``<OpenCN-Repo>/agency/rootfs/board/common/rootfs_overlay/etc/opencn/validation/``.
| These scipts will be added to the target when building the rootfs with ``<OpenCN-Repo>/agency/build.sh -r``.
| You need to add **at least** a *README* to explain how the testing script work, their features, limitation, etc...