.. _debugging:

##########################
Debugging facilities
##########################

*******************************************
Recover file from Virtual Machine
*******************************************

.. _recover_file_vm:

There is two available option to recover file : 

* SSH
* Script

========================================
Recover through SCP (Virtual & Physical)
========================================

You will find the step under :ref:`Environment <recover_file_scp>` section.

=====================================
Recover through Script (Virtual only)
=====================================

.. warning:: 
    This method only work for virtual machine.

| A script is available under ``<opencn-repo>/agency/filesystem/recover_file.sh``.
| This script will try to recover a file under ``/root/``.

Before using it, make sure to sync the data and to shut it down using the following command :

.. code-block:: shell

    # Force an immediate write to all cached data to disk
    sync

    #When using a VM :
    shutdown

Then you can use the script, simply follow the command : 

.. code-block:: shell

    cd <opencn-repo>/agency/filesystem/

    # Run the script
    ./recover_file.sh

    # Enter the filename to recover relative to /root
    my_file.log

    #Check that the file is recovered
    ll | grep my_file.log

*******************************************
Segfault - Sanitizing address in components
*******************************************

| In case of a segmentation fault during the modification/development of a component, you case sanitize the address.  
| Basically, the compiler will try to detect memory addresses that are invalid, and will help you to find where the problem come from.

.. warning::
    This procedure is only working for code on the userspace part of OpenCN !

Follow the given steps : 

* Go to <OpenCN-Repo>/agency/usr/target/components/<YourComponent>
* Edit the CMakeList.txt such as the 'set_target_properties' is :

.. code-block::

    set_target_properties(<YourComponent> PROPERTIES COMPILE_FLAGS "${_components_cflags} -fsanitize=address -static-libasan")
    set_target_properties(<YourComponent> PROPERTIES LINK_FLAGS "${_components_ldflags} -fsanitize=address -static-libasan")

* Then run the build script in debugging mode : 

.. code-block:: shell

    #WARNING : Here we run the build script under 'usr' which only build the userspace
    #           part of OpenCN, not to be confused with the usual one under 'agency' folder.
    cd <OpenCN-Repo>/agency/usr/
    ./build.sh -d

* And deploy the userspace part : 

.. code-block:: shell

    cd <OpenCN-Repo>/agency/
    ./deploy.sh -u


*******************************************
Float printing in RT/kernel space
*******************************************

.. warning::
    Using lenghty or several ``opencn_printf`` function will result in RT jitter spike.
    Please see issue https://gitlab.com/mecatronyx/opencnc/opencn/-/issues/197

| A function called ``opencn_printf`` is available after adding ``#include <opencn/ctypes/strings.h>`` in your C file.
| The usage is the same as the `printf <https://cplusplus.com/reference/cstdio/printf/>`_ function in C :

.. code-block:: C

    opencn_printf("My float is : %5.2f\n",my_float);
