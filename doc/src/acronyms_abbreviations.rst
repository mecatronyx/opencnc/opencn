==========================
Acronyms and abbreviations
==========================

========        =============================       ============================================================================================================================================
Acronyms        Signification                       Note
========        =============================       ============================================================================================================================================
AMP             Asymmetric Multiprocessing          /
CSP             Cyclic Synchronous Position         Axis control mode (CIA402) - Each EtherCAT frame, a target position is sent to the axis in this mode
CSV             Cyclic Synchronous Velocity         Axis control mode (CIA402) - Each EtherCAT frame, a target velocity is sent to the axis in this mode
CST             Cyclic Synchronous Torque           Axis control mode (CIA402) - Each EtherCAT frame, a target torque is sent to the axis in this mode
FIFO            First In First Out                  FIFO is an accounting method in which assets purchased or acquired first are disposed of first
HAL             Hardware Abstraction Layer          Heritage from LinuxCNC, the HAL manage the loading of OpenCN components/features and can link them together
HPET            High Precision Event Timer          /
LCCT            /                                   OpenCN component - Former orchestrator of OpenCN, to be replaced by OCNO in the futur
LCEC            LinuxCnc Ethercat Compononent       OpenCN component - Heritage from LinuxCNC. Manage the EtherCAT communication by using `IGH master <https://etherlab.org/en/ethercat/>`_
MUX             MUltipleXeur                        OpenCN component - Provide a configurable multiplexer in the HAL
OCNO            OpenCN Orchestrator                 OpenCN component - Act as an orchestrator between the different components to provide functionality CN such as Jogging / Homing / Machining
PLC             Programmable Logic Controller       PLC provide flexibility in your environnement by providing machine-specific task, as well as user-customizable RT running code
PPM / PP        Profile Position Mode               Axis control mode (CIA402) - A target position is sent to the drive wich manage itself the acceleration/deceleration curve of the axis
PVM / PV        Profile Velocity Mode               Axis control mode (CIA402) - A target velocity is sent to the drive wich manage itself the acceleration/deceleration curve of the axis
PWM             Pulse-Width Modulation              `Wiki <https://en.wikipedia.org/wiki/Pulse-width_modulation>`_ - Used by OpenCN-Companion
RT              Real Time                           Capabilty of a PC to perform time-critical task. We usualy differentiate hard-RT (OpenCN) & soft-RT
TSD             Triamec Servo Drive                 Drive from Triamec that provide a varity of functionality. supported in OpenCN
VM              Virtual Machine                     | OpenCN can run in x86-qemu of virt64(arm64) VM. 
                                                    | A VM is a computer system that is created by software on one computer to copy the operations performed by a separate computer
========        =============================       ============================================================================================================================================
