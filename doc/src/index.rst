
.. image:: pictures/heigvd.png
   :align: left
   :width: 80px
   :height: 60px

.. image:: pictures/mecatronyx.png
   :align: right
   :width: 200px
   :height: 60px

.. image:: pictures/OpenCN.png
   :align: center

.. toctree::
   :maxdepth: 5
   :numbered:
   :hidden:

   acronyms_abbreviations
   introduction
   architecture/architecture
   environment/environment
   components/components
   CNC_Path_Planning_Algorithms/CNC_Path_Planning_Algorithms
   rs274_interpreter/rs274
   user_guide
   host/host
   communication/communication
   companion
   developer/developer
  
\
\
\

Welcome to OpenCN
=================

For any information and discussions related to the SOO framework, please
visit our main discussion forum at https://discourse.heig-vd.ch/c/opencn

OpenCN is an open, flexible and powerful solution for low-cost compact system control with
embedded path planning algorithms and hard realtime control.

OpenCN Concepts and Architecture
================================

- :ref:`Introduction to OpenCN <introduction>`
- :ref:`Architecture and principles <architecture>`

Environment & targets
=====================

- :ref:`Environment <environment>`


OpenCN Components
=================

- :ref:`Main components of the framework <components>`

CNC Path Planning algorithms
============================

- :ref:`CNC Path Planning Algorithms <CNC_Path_Planning_Algorithms>`

G-Code intrepreter
==================

- :ref:`RS-274 Intrepreter <rs274>`

Setup and Environment
=====================

- :ref:`User guide <user_guide>`

OpenCN Host
===========

- :ref:`host / remote applications<host>`

OpenCN Communication Interfaces
===============================

- :ref:`Communication interfaces<communication>`


Developer documentation
=======================

- :ref:`Developer documentation index <developer>`
- :ref:`Development flow <dev_flow>`
- :ref:`Miscellaneous <misc>`
- :ref:`Continius Integration/Deployment <ci>`
- :ref:`Logging facilities <logging>`
- :ref:`Debugging facilities <debugging>`


Other project and information
=============================

- :ref:`OpenCN Companion <companion>`


