.. _rs274:

###########################
G-Code interpreter (RS274)
###########################

OpenCN implements a custom version of LinuxCNC RS-274 g-code interpreter. Its purpose is to read 
and interpret G-Code commands from a ".ngc" file.

Configuration of the interpreter
--------------------------------

.. warning:: 
    **The following implementation of the interpreter configuration will be changed in 
    the near future.**

The interpreter must be configured using an ".ini" file. The interpreter will open the configuration
file base on the path contained in a environment variable called "INI_FILE_NAME". To create such
variable you must use the following command:

:code:`export INI_FILE_NAME <path to the ".ini" file>`

.. note:: This variable will be accessible only by the current session.

To make a system-wide environment variable you need to add it to "/etc/environment"::
 
    INI_FILE_NAME="<path to the '.ini' file>"

To check if the variable was correctly defined you can use the following command:

:code:`echo $INI_FILE_NAME`

The configuration file is divided in sections. Each section has a specific name and specific variables.
The following list shows the supported sections and their respective variables:

- [TRAJ]: Additional settings for the motion controller

+----------------+---------------------------------------------------------------------+
| Parameter name | Description                                                         |
+================+=====================================================================+
| COORDINATES    | Supported axes (X Y Z A B C U V W). By default XYZ machine is used. |
+----------------+---------------------------------------------------------------------+

.. warning::
    The supported axes are only used as valid entry points for the G-Code interpreter. It is independant of the feedrate planning algorithm used in feedopt. 
    This means the interpreter can read the supported axes while ``feedopt`` hal component only supports the available kinematics.

- [EMCIO]: Settings for I/O interface

+--------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter name     | Description                                                                                                                                                                                                  |
+====================+==============================================================================================================================================================================================================+
| TOOL_TABLE         | Path to the tool table. Under "/root/tool.tbl" for the target.                                                                                                                                               |
+--------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| RANDOM_TOOLCHANGER | Only possible value is 1. This is for machines that cannot place the tool back into the pocket it came from. For example, machines that exchange the tool in the active pocket with the tool in the spindle. |
+--------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| DEFAULT_FEED_G0    | Value used as feed rate for the move outside the raw material                                                                                                                                                |
+--------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

.. warning:::
    Since the openCN's interpreter is based on the LinuxCNC ones, some of the functionalities provided in the Initfile remain without any warranty (`LinuxCNC Initfile documentation <https://linuxcnc.org/docs/2.6/html/config/ini_config.html#sub:EMCIO-Section>`_)


Here is an example configuration file::

    [TRAJ]
    COORDINATES = X Y Z B C

    [EMCIO]
    TOOL_TABLE = /home/hugo/Documents/E2C/opencn-178/agency/usr/matlab/common/tool.tbl
    RANDOM_TOOLCHANGER = 1
    DEFAULT_FEED_G0 = 5000.0

This file defines a 5-axis machine and a tool table file located at the same location as the 
configuration file itself. The tool table can be modified using :ref:`Tooledit <tooledit>` application.

.. include:: gcode.rst
