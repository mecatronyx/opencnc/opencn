.. _environment:

###########
Environment
###########

*******
Targets
*******

*OpenCN* framework supports emulated and real targets for arm64 and x86 architectures.
4 targets are supported:

* x86 platform, called *x86*
* Raspberry PI 4, called *rpi4_64*
* Emulated x86 platform, called *x86-qemu*
* Emulated ARM64 platform, called *virt64*

********
Material
********

This paragraph provides information on the needed material for each real platforms.

x86 target
==========

* A Intel CPU with at least 4 cores
* An ``e1000e`` supported Ethernet controller used for *EtherCAT* communications
* A second Ethernet controller
* At least one USB free port, after connecting a keyboard and mouse.
* USB thumb drive (at least 200 MB at the time of writing)

Raspberry PI
============

* A *Raspberry PI 4* board
* A SD card
* (optional) An USB to serial cable to interact with the console of the OpenCN target.
* `ETHERNET DUAL <https://www.sg-electronic-systems.com/ecommerce/ethernet-shield/18-ethernet-dual-v11-the-ethernet-is-based-on-enc28j60-spi-controller-all-functionalities-are-full-integrated-in-standard-linux-ker.html>`_. It is used to communicate with the Host PC.

Raspberry Compute Module 4
==========================
 
* A *Raspberry Compute Module 4* module
* A  *Raspberry Compute Module 4* main board with 2 ethernet interfaces (one should be the CM4 original ETH).
* A SD card
* (optional) An USB to serial cable to interact with the console of the OpenCN target.

The following base board have been tested:

* `Dual Gigabit Ethernet Base Board <https://www.waveshare.com/cm4-dual-eth-base.htm>`_.
* `Mini Dual Gigabit Ethernet Base Board  <https://www.waveshare.com/cm4-dual-eth-mini.htm>`_.
* `CM4 NVME NAS BOX <https://www.waveshare.com/wiki/CM4-NVME-NAS-BOX>`_.

.. note::

	The `CM4 NVME NAS BOX <https://www.waveshare.com/wiki/CM4-NVME-NAS-BOX>`_ provides
	2 locations for SSD nvme drives. Here are information on how to deal with this
	drives

*************
Configuration
*************

Target IP addresses
===================

On all platforms, the Ethernet interface are configured with a static IP address.
The address is:

* **eth0** : ``192.168.53.15``
* **eth1** : ``192.168.53.16`` (Raspberry only)


A DHCP server is also running  on the platform. It provides an IP address to the
Host PC.


ssh
===

A ``ssh`` server is present on each supported platforms. It then is possible to
connect to a *OpenCN* target with it. The password is ``root``

**Example**:

.. code-block:: shell

	$ ssh root@192.168.53.15

.. warning::

	The results of the ``halcmd show`` is only displayed on the main console. It
	is not showed on ``ssh`` terminals.

.. _recover_file_scp:

It is also possible to use ``scp`` (OpenSSH secure file copy) to copy files from/to
an *OpenCN* platform. The ``scp`` command format is following:

.. code-block:: shell

	$ scp <SRC_FILE> <TARGET_FOLDER>

The format, of a remote destination, is the following: ``root@<IP_ADDRESS>:<PATH_ON_TARGET>``

The next command shows how to use ``scp`` to copy ``micro3.hal`` file from an
OpenCN target to a local directory of the HOST PC, the default password is ``root``.

.. code-block:: shell

	$ scp root@192.168.53.15:/root/micro3.hal .

| A ``avahi`` (zero conf server) is also available. It simplifies connection to an
	*OpenCN* target: instead of using an IP address, it is possible to use the target
	hostname + ``.local`` (``opencn.local`` in our case).

It means that is this possible to connect to an *OpenCN* target by using ``opencn.local``
instead of ``192.168.53.15``. Examples:

.. code-block:: shell

	$ ping opencn.local
	$ ssh root@opencn.local

.. warning::

	``avahi`` only works if the *OpenCN* target and the HOST PC are connected on
	the same LAN.

-------------------------
Avoid host identification
-------------------------

| Everytime you will rebuild the target, you will get an error because the remote host has changed it's identification.
| To avoid identification checking, you can create a config file on your host/GUI PC under ``~/.ssh/config`` that contain : 

.. code-block:: shell

	Host 192.168.53.15
		StrictHostKeyChecking no
		UserKnownHostsFile=/dev/null


sshfs
=====

It is possible to ``mount`` a folder of the target platform on the HOST PC using
``sshfs``.

It is, for example, possible to mount the ``root`` folder on the HOST PC to be
able to edit an HAL file.

The ``sshfs`` command use the same format as ``scp``. The next commands shows how
to mount the ``root`` folder to the ``fs`` folder present on the current directory
of the HOST PC.

The command ``umount`` can be used to stop (umount) the folder sharing,

.. code-block:: shell

	$ sshfs root@192.168.53.15:/root fs
	$ umount fs

.. _env_ssd_drive:

*********
SSD drive
*********

When a lots of data have to be logged, it can be better to store them on a SSD drive
instead of the SD Card. SSD drive are much faster (and bigger).

A SSD drive can be connected on a USB port, with an adaptation or using a CM4 box
like the `CM4 NVME NAS BOX <https://www.waveshare.com/wiki/CM4-NVME-NAS-BOX>`_.

Usage
=====

Here are the commands to use to make the drive mounted and ready to use.

* Check the disk name

.. code-block:: shell

	$ lsblk

the drive name is similar to ``nvme0nX`` where X is the disk number.

* Partition creation

.. code-block:: shell

	$ (echo n; echo p; echo 1; echo ; echo ; echo w) | fdisk /dev/nvme0nX

or

.. code-block:: shell

	$ sudo parted /dev/nvme0nX --script mklabel msdos mkpart primary ext4 0% 100%

* Format the partition as ext4.

.. code-block:: shell

	$ mkfs.ext4 /dev/nvme0nXp1

$ Mount the partition on the OpenCN target.

.. code-block:: shell

	$ mkdir <MOUNT POINT>
	$ mount /dev/nvme0nXp1 <MOUNT POINT>

The possible ``<MOUNT POINT>``, often used in *Linux* environment: ``/mnt/data``
