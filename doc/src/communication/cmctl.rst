.. _cmctl:

####################
Component Controller
####################

The *Component Controller*, called ``cmctl``, provides an communication interface
between a **Target** and an **Host**. It is based on `Cap'n Proto <https://capnproto.org/>`_
framework.

Cap'n Proto is data interchange format and capability-based RPC system. Think JSON,
except binary.


``Cmctl`` provides a C/C++ and a python interfaces. The :numref:`_fig-component_controller`
shows the different parts of a component.

.. figure:: /pictures/component_controller.png
   :name: _fig-component_controller
   :alt: Components controller
   :align: center

   Components controller

It based on the client/server RPC model (Remote Procedure Call): The ``cmctr_client``
sends a command to the ``cmctr_server``. The ``cmctr_client`` computes the command
and send back the answer.  The :ref:`RPC flow <rpc_flow>` figure shows RPC
communication flow between a client and a server.

.. _rpc_flow:
.. uml::
   :align: center
   :caption: RPC flow

   client -> server : cmd 1
   activate server
   return answer 1

   client -> server : cmd 2
   activate server
   return answer 2

Server
======

The server is `cmctl-server` available in the OpenCN target. This
server has to be started before starting any `CMCtl-client`.

In OpenCN target:

.. code::

	./cmctl-server &


Client
======

The client is a RPC client provided by *Cap'n Proto* library. The following code
show how to to retrieve this client.

.. code-block:: cpp

	#include <capnp/ez-rpc.h>

	capnp::EzRpcClient *client = new capnp::EzRpcClient(ip, port);
	if (!client)
		printf("ERROR - Connection failed\n");

Pins interfaces
===============

C++
---

The control of the PINs can be handle by two sets of classes: classes representing
PINs and a class for PINs transactions, which can perform multiple get/set PINs in
one network transaction.

The PIN related classes can directly set/get a PIN value. It also contains an
internal/local value (called local value in this document) which can be read/write
at anytime without performing a network transaction. This local value is used later
on with the PINs transactions. We can therefore distinguish the local value of the
PINs and their remote value. The local value is not known from the target board
until a transaction is performed. The local and remote values can be different
and a transaction would synchronize them. This local value it is used with the
PINs transactions.

A PINs transactions is used to perform multiple get/set PIN values in one network
transaction. The overhead introduces by the network protocol is reduced overall
if several actions on PINs are sent in one transaction compared to sending a network
transaction per action. Its usage is the following:

#. Add PINs to the PINs transactions with the wanted transaction (GET or SET). The
   transaction can be different for each "transaction".
#. Execute the transactions. PINs transactions use the current local value for the
   SET transaction or update this local value with the value got from the PIN GET.

Some comments:

* It is possible to create multiple sets of transactions
* An transactions can be executed at anytime
* It is possible to read/write PIN local value at anytime.

The following example shows a basic example on how to use PIN related classes.

.. code-block:: cpp

	// Create an instance of the 'capnp' client (@todo replace it with a CMClient)
	capnp::EzRpcClient*client = new capnp::EzRpcClient(ip, port);

	// Create some PINs
	CMPinI32 s32In{"loopback.0.s32_in.1", client};
 	CMPinI32 s32Out{"loopback.0.s32_out.1", client};

 	// Create a PIN transaction
 	CMPinTransactions transactions{client};

 	// Set single PIN Value
 	s32In.set(3450);

 	// Read single PIN Value
 	auto aVariable = s32Out.get();

 	// Perform a setPin to multiple PINs in one transaction
 	// 1. Add the PINs to the 'PinActions'
 	transactions.add(s32In, CMPinTransactionType::SET);
 	transactions.add(s32Out, CMPinTransactionType::GET);

 	// 2. Set PINs values - this value stays local
 	s32In = -256;

 	// 3. Execute
 	CMPinTransactionType.execute();

 	// 4. Get the value after 'transaction' execution
	auto aVariable = s32Out

PINs Mode
^^^^^^^^^

In some application, the use of PIN transactions is not necessary. It can even make
the code less readable.

In this case, the PIN classes offers two modes to get/set PINs values:

* :mod:`AccessMode::VALUE` mode: It is the one presented above. It uses :func:`get`/:func:`set`
  methods.
* :mod:`AccessMode::PIN` mode: In this mode, it is possible to use the PIN object directly
  as with :mod:`int` variable. for example:

.. code-block:: cpp

	s32In = 32; // It performs :mod:`PIN_VALUE`, it updates the s32IN PIN (network transaction)
	aVar = S32out; // Read the S32out PIN  (network transaction

API
^^^

The PIN classes are the following.

* :ref:`CMPinI32 <cmpini32>`
* :ref:`CMPinU32 <cmpinu32>`
* :ref:`CMPinBit <cmpinbit>`
* :ref:`CMPinFloat <cmpinfloat>`

They all are a subclass of the :ref:`CMPin<cmpin>` and all behaves the same way.
This class should not be used directly.

.. _cmpin:

CMPIn
"""""

The  is the :class:`CMPin` class is the parent class for all PIN classes. It is not
 expected to use it directly.

 .. cpp:enum-class:: AccessMode

 	Define the PIN value access mode

 	* :mod:`LOCAL`: The local PIN 'value' is used to set or get - not network transaction
 	* :mod:`PIN`: The current PINs of the 'target' is set or get - perform a network transaction

.. cpp:function:: std::string name()

	Return the PIN name

.. cpp:function:: hal_type_t type()

	Return the PIN type, supported values are:

	* HAL_TYPE_UNSPECIFIED = -1
	* HAL_BIT = 1
	* HAL_FLOAT = 2
	* HAL_S32 = 3
	* HAL_U32 = 4

.. cpp:function:: int error()

	return the latest communication error. The errors codes are:

	* CMCTL_SUCCESS = 0,
	* CMCTL_PIN_NOT_EXIST,
	* CMCTL_PIN_TYPE_ERROR,
	* CMCTL_CONNECTION_FAILED,
	* CMCTL_NO_CONNECTION,

.. cpp:function:: void setMode(AccessMode mode)

	Set the PIN value access mode

.. cpp:function:: AccessMode& mode()

	Return the current PIN value access mode

.. _cmpini32:


.. cpp:class:: CMPinI32 : CMPin

	.. cpp:function:: CMPinI32(std::string name, capnp::EzRpcClient* client)


	.. cpp:function:: void set(int32_t value)
	.. cpp:function:: int32_t get()
	.. cpp:function:: operator int32_t()
	.. cpp:function:: CMPinI32& operator=(int32_t value)

.. _cmpinu32:

.. cpp:class:: CMPinU32 : CMPin

	.. cpp:function:: CMPinU32(std::string name, capnp::EzRpcClient* client)

	.. cpp:function:: void set(uint32_t value)

		Set the PIN to value through a network transcation. The local value is
		also updated

	.. cpp:function:: uint32_t get()

		Get current PIN value through a network transcation. The local value is
		also updated

	.. cpp:function:: operator uint32_t()

		Depending on the PIN value mode, it returns the internal value (no network
		transaction) or the current PIN value (network transaction). In the second
		case, the internal value is also updated.

	.. cpp:function:: CMPinI32& operator=(uint32_t value)

		Depending on the PIN value mode, it sets the internal value (no network
		transaction) or the current PIN value (network transaction). In the second
		case, the internal value is also updated.

.. _cmpinbit:

.. cpp:class:: CMPinBit : CMPin

	.. cpp:function:: CMPinBit(std::string name, capnp::EzRpcClient* client)

	.. cpp:function:: void set(bool value)

		Set the PIN to value through a network transcation. The local value is
		also updated

	.. cpp:function:: bool get()

		Get current PIN value through a network transcation. The local value is
		also updated

	.. cpp:function:: operator bool()

		Depending on the PIN value mode, it returns the internal value (no network
		transaction) or the current PIN value (network transaction). In the second
		case, the internal value is also updated.

	.. cpp:function:: CMPinI32& operator=(bool value)

		Depending on the PIN value mode, it sets the internal value (no network
		transaction) or the current PIN value (network transaction). In the second
		case, the internal value is also updated.

.. _cmpinfloat:

.. cpp:class:: CMPinFloat : CMPin

	.. cpp:function:: CMPinFloat(std::string name, capnp::EzRpcClient* client)

	.. cpp:function:: void set(double value)

		Set the PIN to value through a network transcation. The local value is
		also updated

	.. cpp:function:: double get()

		Get current PIN value through a network transcation. The local value is
		also updated

	.. cpp:function:: operator double()

		Depending on the PIN value mode, it returns the internal value (no network
		transaction) or the current PIN value (network transaction). In the second
		case, the internal value is also updated.

	.. cpp:function:: CMPinI32& operator=(double value)

		Depending on the PIN value mode, it sets the internal value (no network
		transaction) or the current PIN value (network transaction). In the second
		case, the internal value is also updated.

CMPinTransactions
"""""""""""""""""

.. cpp:class:: CMPinTransactions

	CMPinTransactions object are used to combine multiple PIN get/set in one network
	transaction.


	.. cpp:function:: CMPinTransactions(capnp::EzRpcClient* client)

		CMPinTransactions constructor. It only needs the capnp client as parameter

	.. cpp:function:: void clear()

		Clear the transactions. It removes all PINs previously add to the transaction
		with the :func:`add` method.

	.. cpp:function:: void add(CMPin *pin, CMPinTransactionType transaction)

		Add a PIN to the transaction to a CMPinTransaction object. A PIN can only be
		added once to an action. It the PIN is already present in the object, the second
		call has no effect.

	.. cpp:function:: void remove(CMPin *pin)

		Remove a PIN of the CMPinTransactions object.

	.. cpp:function:: void execute()

		execute the transaction

	.. cpp:function:: size_t size()

		return the current size of the transaction, ie the number of PINs present in the
		transaction


Python
------

The Python interface behavior is similar to the C++ interface.

.. code-block:: python

	import opencn

	client = opencn.EzRpcClient("192.168.53.15", 7002)
	pin = opencn.CMPinI32("loopback.0.s32_in.1", client)

	pin.get()
	pin.set()

	pin.value = 123
	a = pin.value

	transactions = opencn.CMPinTransactions(client)
	transactions.add(pin, 1)

	pin.setMode(1)

	# Access PIN value (network transaction)
	pin.value = 123
	a = pin.value

.. _note:

	The Python interface is a bind of the C++ code. The binding is realized with
	`pybind11 <https://github.com/pybind/pybind11>`_ library.

API
^^^

The :ref:`opencn <opencn_module>` provides the classes to communicate with the
``cmctl``.

.. _opencn_module:

.. py:module:: opencn

.. py:class:: EzRpcClient(ip, port)

	Captain Proto RPC client

..	py:class:: CMPin

	Main PINs class. It is expected to create object from this class directly.


	.. py:method:: name

		Return PIN name as displayed with ``halcmd show pin`` command

	.. py:method:: error

		Return error code of the last network transaction

	.. py:method:: setMode(AccessMode mode)

		Set The PIN value access mode

	.. py:method:: mode

		Return the PIN value access mode

.. py:class:: CMPinI32(ip, port)

	.. py:method:: set(value)

		Set the PIN to value through a network transcation. The local value is
		also updated

	.. py:method:: get

		Get current PIN value through a network transcation. The local value is
		also updated

	.. py:method:: __getattr__

		Depending on the PIN value mode, it returns the internal value (no network
		transaction) or the current PIN value (network transaction). In the second
		case, the internal value is also updated.

		It is called with the :mod:`value` attribute. An error is raised if another
		attribute is used.

	.. py:method:: __setattr__(value)

		Depending on the PIN value mode, it sets the internal value (no network
		transaction) or the current PIN value (network transaction). In the second
		case, the internal value is also updated.

		It is called with the :mod:`value` attribute. No action is performed when
		it is called with another attribute.

.. py:class:: CMPinU32(ip, port)

	.. py:method:: set(value)

		Set the PIN to value through a network transcation. The local value is
		also updated

	.. py:method:: get

		Get current PIN value through a network transcation. The local value is
		also updated

	.. py:method:: __getattr__

		Depending on the PIN value mode, it returns the internal value (no network
		transaction) or the current PIN value (network transaction). In the second
		case, the internal value is also updated.

	.. py:method:: __setattr__

		Depending on the PIN value mode, it sets the internal value (no network
		transaction) or the current PIN value (network transaction). In the second
		case, the internal value is also updated.

		It is called with the :mod:`value` attribute. No action is performed when
		it is called with another attribute.

.. py:class:: CMPinBit(ip, port)

	.. py:method:: set(value)

		Set the PIN to value through a network transcation. The local value is
		also updated

	.. py:method:: get

		Get current PIN value through a network transcation. The local value is
		also updated

	.. py:method:: __getattr__

		Depending on the PIN value mode, it returns the internal value (no network
		transaction) or the current PIN value (network transaction). In the second
		case, the internal value is also updated.

		It is called with the :mod:`value` attribute. An error is raised if another
		attribute is used.

	.. py:method:: __setattr__

		Depending on the PIN value mode, it sets the internal value (no network
		transaction) or the current PIN value (network transaction). In the second
		case, the internal value is also updated.

		It is called with the :mod:`value` attribute. No action is performed when
		it is called with another attribute.

.. py:class:: CMPinFloat(ip, port)

	.. py:method:: set(value)

		Set the PIN to value through a network transcation. The local value is
		also updated

	.. py:method:: get

		Get current PIN value through a network transcation. The local value is
		also updated

	.. py:method:: __getattr__

		Depending on the PIN value mode, it returns the internal value (no network
		transaction) or the current PIN value (network transaction). In the second
		case, the internal value is also updated.

		It is called with the :mod:`value` attribute. An error is raised if another
		attribute is used.

	.. py:method:: __setattr__

		Depending on the PIN value mode, it sets the internal value (no network
		transaction) or the current PIN value (network transaction). In the second
		case, the internal value is also updated.

		It is called with the :mod:`value` attribute. No action is performed when
		it is called with another attribute.

.. py:class:: CMPinTransactions(client)

	CMPinTransactions objects are used to combine multiple PIN get/set in one network
	transaction.

	.. py:method:: add(pin, transaction)

		Add a PIN to the transaction to a CMPinTransaction object. A PIN can only be
		added once to an action. It the PIN is already present in the object, the second
		call has no effect.

	.. py:method:: remove

		Remove a PIN of the CMPinTransactions object.

	.. py:method:: clear

		Clear the transactions. It removes all PINs previously add to the transaction
		with the :mod:`add` method.

	.. py:method:: size

		return the current size of the transaction, ie the number of PINs present in the
		transaction

	.. py:method:: execute

		Execute the transaction
