.. _tcp_lib:

#############
TCP libraries
#############

The library runs only on the user side and can be compiled for both target and
host.

The TCP libraries allow to exchange messages between a server and a client
using the TCP/IP communication protocol. A client can connect to the server and
send it a request. Once the server has received a message it used registered
callback functions to transmit the message to a handler.

Messages are transmitted to the handler in a string format. The TCP server and
client are agnostic of the content of the messages and only perform the
transportation. It is the handler which will parse the messages and dispatch the
information further to the correct component.

A client request may or may not require a response from the server.
The handler which knows the content of the message indicates to the server if a
response to the request is requiered to be sent and provide the response
message.

.. _tcp_lib_fig_flow:
.. figure:: /pictures/tcp_lib.png
    :name: _fig-tcp_lib
    :alt: TCP libraries communication flow
    :align: center

    TCP libraries communication flow

At :numref:`tcp_lib_fig_flow`, the flow of a message exchange is shown.

#. The handler on the client side requests the TCP client to send a message to
   the server. The handler indicates if the request requires a response. In this
   case, the dotted path would be used for the response.
#. The request is transmitted over TCP to the server.
#. The server uses a callback function to transfer the message to its
   handler. The handler of the client and the handler of the server may not be
   the same.
#. If a response is expected, the handler provides it to the server.
#. The server sends the response to the client. Then it closes the socket
   previously opened with the TCP connection. If no response is expected, the
   server directly closes the socket.
#. The client would then transfer the response to its handler.

TCP server library
==================

The TCP server library code can be found in
``<OPENCN-HOME>/agency/usr/common/tcp_server/``. It can be build for both
host and target.

The TCP server is a class that can be instantiated. It takes as arguments:

* A port number: The server waits on this port number on any IP address for
  a client connection.
* A callback function: This function is called when the server received a
  client request. The function indicates in return if the request requires
  a response, and if it does, it provides the response in a return parameter.
  The server sends this response over TCP using the same socket as the one
  previously opened with the TCP connection.
* A void pointer: This pointer is always provided back to the callback
  function. It is usualy used by the caller function to provide a pointer on
  itself (*this*) such that when the callback function is invoked the object
  associated with it can be retrieved.

The documentation for the server API is available in the tcp_server.hpp header
file.

TCP client library
==================

The TCP client library code can be found in
``<OPENCN-HOME>/agency/usr/common/tcp_client/``. It can be build for both
host and target.

TCP client is a class that can be instantiated. It takes as arguments:

* The server port number.
* The server IP address.

With these parameters it can then be used to send messages to this server.
When the handler asks for a message to be sent, it must indicate if a
response is expected for the message. In this case, the client read the socket
after sending the message and the response is returned to the handler.
Otherwise the client does not wait on receiving a response.

The documentation for the client API is available in the tcp_client.hpp header
file.

TCP library example
===================

Here is a simple example that used the TCP server and client to communicate
using localhost IP address and port 7001. The client sends a simple
"hello world" message. When the TCP server received the message it calls the
handler registered function which prints it. The handler provides a "ACK"
response to the server which sends it to the client. The handler receives the
response from the client and prints it.

.. code-block:: cpp

    #include <iostream>
    #include <string>
    #include <thread>
    #include "tcp_client.hpp"
    #include "tcp_server.hpp"

    /* Run the server start() on a thread as this function does not return */
    void serverFn(TcpServer *server)
    {
        server->start();
    }

    /* Callback function calls when the server receives a message */
    void serverReceiveMessage(std::string message, bool &sync_response,
        std::string &response, void *data)
    {
        std::cout << "Server received: " << message << std::endl;
        /* Indicate that a response needs to be sent to the client */
        sync_response = true;
        /* Response to be sent to the client*/
        response = "ACK";
    }

    int main(int argc, char* argv[]) {
        /* Create a server listening on 7001 port */
        TcpServer server(7001, serverReceiveMessage, nullptr);
        /* Create a localhost client */
        TcpClient client("127.0.0.1", 7001);

        std::string message("Hello world");
        std::string response;

        /* Start the server */
        std::thread serverThread(serverFn, &server);
        /* Send a message to the server, providing a pointer for the response */
        client.sendMessage(message, true, &response);

        /* Prints the response received */
        std::cout << "Client receives response: " << response << std::endl;
        /* Properly shutdown the server */

        server.stop();

        /* Waiting for the server to stop */
        serverThread.join();

        return 0;
    }

