.. _communication:

###############################
OpenCN Communication Interfaces
###############################

.. toctree::
   :maxdepth: 5
   :hidden:

   cmctl
   tcp_lib

OpenCN framework can be used with an OpenCN device, called **Target**
and a remote application running on a PC, called **Host**.

The different *Communication Interfaces* provided by the framework are:

* :ref:`Component Controller <cmctl>`
* :ref:`TCP libraries  <tcp_lib>`
