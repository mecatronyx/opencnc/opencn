.. _user_guide:

##########
User guide
##########

OpenCN environment can be used in tree *modes*:

*  **Native** mode: The compilation of the different components (kernel, rootfs,
   ...) and deployment are done directly on the host machine.
*  **Docker** mode: The framework provides a docker environment which provides all
   the tools needed by this environment.
*  **Windows10+WSL2** mode: Using a Windows 10 OS running WSL2 for a Ubuntu20.04LTS VM used for the compilation, deployement, GUI, etc...

.. warning::

	OpenCN framework is expected to be used in a Linux host. It is used/tested
	with Ubuntu (20.04LTS) and Debian (buster)


Below are the commands to retrieve the source code:


.. code-block:: shell

	$ git clone https://gitlab.com/mecatronyx/opencnc/opencn.git opencn
	$ cd opencn
	$ git submodule init
	$ git submodule update

.. _native mode:

*************
*Native* mode
*************

Tools installation
==================

This section all the

* Add i386 architecture

.. code-block:: shell

	$ sudo dpkg --add-architecture i386

* Required packages

.. code-block:: shell

	$ sudo apt update & sudo apt upgrade
	$ sudo apt install bc bison bridge-utils ca-certificates  capnproto cpio \
	  cmake device-tree-compiler default-jdk dosfstools dvipng elfutils fakeroot file flex \
	  gcc git graphviz g++ latexmk lib32z1-dev libc6:i386 qtbase5-dev qtdeclarative5-dev libcapstone3 libepoxy0 \
	  libgbm1 libgtk2.0-dev libjpeg62 libssl-dev libstdc++6:i386 libusb-1.0-0 make nano \
	  openssh-server patch pkg-config python3 python3-pip python3-cryptography python3-pyelftools \
	  rsync sudo tex-gyre texlive-latex-recommended texlive-latex-extra texlive-fonts-recommended \
	  wget unzip u-boot-tools xz-utils zlib1g:i386

* Cap'N'Proto package

.. code-block:: shell

	$ curl -O https://capnproto.org/capnproto-c++-0.7.0.tar.gz
	$ tar zxf capnproto-c++-0.7.0.tar.gz
	$ cd capnproto-c++-0.7.0
	$ ./configure
	$ make -j6 check
	$ sudo make install
	$ cd ..
	$ rm -rf capnproto-c++-0.7.0
	$ rm capnproto-c++-0.7.0.tar.gz 


* Sphinx package

The documentation is created using Sphinx and the following packages need to be installed :

.. code-block:: shell

	$ pip3 install sphinx sphinx_rtd_theme sphinxcontrib-bibtex sphinxcontrib-drawio sphinxcontrib-plantuml sphinx-last-updated-by-git


* drawio

.. code-block:: shell

	sudo apt-get install --no-install-recommends libgtk-3-0 libnotify4 libsecret-1-0 libxss1 xvfb
	wget -q https://github.com/jgraph/drawio-desktop/releases/download/v20.6.2/drawio-amd64-20.6.2.deb
	sudo dpkg -i drawio-amd64-20.6.2.deb
	rm drawio-amd64-20.6.2.deb

* Ninja package

QEMU 5.2.0 requires python ninja package to be installed:

.. code-block:: shell

        $ pip3 install ninja

* Add the ARM toolchain

For the 64-bit version (virt & RPi4), the AArch-64 (ARM 64-bit) toolchain can be installed with the following commands:

.. code-block:: shell

   $ sudo mkdir -p /opt/toolchains && cd /opt/toolchains
   # Download and extract arm-none-linux-gnueabihf toolchain (gcc v10.2).
   $ sudo wget https://developer.arm.com/-/media/Files/downloads/gnu-a/10.2-2020.11/binrel/gcc-arm-10.2-2020.11-x86_64-aarch64-none-linux-gnu.tar.xz
   $ sudo tar xf gcc-arm-10.2-2020.11-x86_64-aarch64-none-linux-gnu.tar.xz
   $ sudo rm gcc-arm-10.2-2020.11-x86_64-aarch64-none-linux-gnu.tar.xz
   $ sudo mv gcc-arm-10.2-2020.11-x86_64-aarch64-none-linux-gnu aarch64-none-linux-gnu_10.2
   $ echo 'export PATH="${PATH}:/opt/toolchains/aarch64-none-linux-gnu_10.2/bin"' | sudo tee -a /etc/profile.d/02-toolchains.sh

.. note::

	After the installation of a new toolchain, a "user log-out/log-in" is needed for
	the changes to be applied

Compilation
===========

Target platforms
----------------

The file ``build.conf`` in ``agency/`` contains the ``PLATFORM`` to select the target platform.

Possible platforms and types are:

+------------+-------------------------------------+
| Name       | Platform                            |
+============+=====================================+
| *virt64*   | QEMU/virt 64-bit platform           |
+------------+-------------------------------------+
| *x86-qemu* | QEMU/x86 (64-bit) platform          |
+------------+-------------------------------------+
| *rpi4_64*  | Raspberry Pi 4 in 64-bit mode       |
+------------+-------------------------------------+
| *x86*      | x86-based PC (64-bit) platform      |
+------------+-------------------------------------+

The framework provides a generic build scripts, ``<OPENCN_HOME>/agency/build.sh``.

It provides options to build each OpenCN components independently; ``-k`` for the
kernel, ``-r`` for the rootfs and ``-u`` for the user application; or all in one
call (``-a``). The target selection is done the ``-t`` option. It only has to be
call once. The ``-b`` option build the dependencies, like U-Boot for ARM targets
or qemu for the emulation ones.

.. code-block:: shell

	Usage: ./build.sh [OPTIONS]

	Where OPTIONS are:
	  -a    build all components
	  -b    build u-boot and/or qemu depending on the target
	  -c    clean the selected components
	  -k    build linux kernel
	  -r    build rootfs (secondary)
	  -u    build user apps
	  -t    select the wanted target (rpi4_64, virt64, x86, x86-qemu)
	        It has to be done at least once (creation of build.conf file)

.. note::

	This script only focus on the build of target components (Components running
	in the selected target). It does not propose an option to build the host
	application, like the ``user_gui``.

Examples
--------

* Select rpi4 target: ``./build.sh -t rpi4_64``
* Clean the kernel build: ``./build.sh -kc``

Deployment
==========

As for the compilation, the framework provides a generic build scripts, ``<OPENCN_HOME>/agency/deploy.sh``.

Before to perform the deployment, the SD card or USB key has to be formated. It
can be done with the ``-f`` option.

As for the ``build.sh`` script, ``deploy.sh`` provides options to deploy each OpenCN
components independently; ``-b`` for the boot elements (U-Boot, kernel, ...), ``-r``
for the rootfs and ``-u`` for the user application; or all in one call (``-a``).

.. code-block:: shell

	Usage: ./deploy.sh [OPTIONS] [DEVICE]

	Where OPTIONS are:
	  -a    Deploy all (no SD card formating)
	  -b    Deploy boot (kernel, U-boot, etc.)
	  -f    format the SD card / USB key
	  -r    Deploy rootfs (secondary)
	  -u    Deploy usr apps
	At least one OPTION should be set

	DEVICE is the device name of the SD Card or USB key (ex: sdb or mmcblk0 or other...)

.. note::

	The target is the one selected by the -t option of the ``build.sh`` script.


Copy on internal disk (x86 only)
--------------------------------

This procedure copies the installation files from the USB stick to the internal
disk.

* Boot on your OpenCN USB device
* Run ``./install.sh [partition to install]``
* Reboot
* Remove USB device
* Set grub to use the correct partition
	* On grub print, press "e" to edit the launch command
	* Add `root=[partition to install]` at the end of the last line, so it does look like :
	  ``linux /boot/bzImage earlyprintk=vga,keep root=[partition to install]``
	* Press F10 to launch the boot

.. warning::

	 This changed is not saved

To make this change permanent:

* Boot OpenCN from the internal disk
* Modify ``/boot/grub/grub.cfg`` file
	* add ``root=[partition to install]`` at the end of the last line. It looks like :

.. code-block:: shell

	" menuentry "OpenCn .... bootstrap" {

	set gfxpayload=keep

	linux /boot/bzImage earlyprintk=vga,keep root=[partition to install]

	}

* Enter the following command to update grub

.. code-block:: shell

	 grub-install --target=i386-pc --root-directory=/ /dev/sda

* Reboot

Emulation
=========

To start the qemu emulation for the `virt64` or `x86-quemu` targets, is simply
done by running ``<OPENCN_HOME>/agency/st`` script.

.. code-block:: shell

	$ cd <OPENCN_HOME>/agency
	$ ./st

.. warning::

   For the ``x86-qemu`` configuration, QEMU relies on *kvm* by default. It means that
   no other VM monitor (like VirtualBox for example) can run at the same time.
   To disable kvm, edit the ``st`` script, remove option ``--enable-kvm`` and change
   the cpu to ``Nehalem``.


Documentation
=============

The documentation can be generated using the script ``<OPENCN_HOME>/doc/generate_doc.sh``.
The script can be used without option to generate the files or with the option
-c to clean them.

.. code-block:: shell

    Usage: ./generate_doc.sh [OPTIONS]

    Where OPTIONS are:
    	-c    clean generated documentation
	-f    format, supported: html (default), pdf

The generated html files are located in ``<OPENCN_HOME>/doc/build/html/`` and the pdf ones in ``<OPENCN_HOME>/doc/build/latex/``.


.. _Docker mode:

*************
*Docker* mode
*************

OpenCN framework provides a docker environment which provides all the needed tools.
The main advantages of a container solution are:

* Simplification of the deployment of the framework: No need to install a bunch
  of packets on the development PC. Only docker is needed
* Avoids compatibility issues with tool version, after an update for example
* End-User can use any Linux distribution

| First, we need to properly install docker, following `this guide <https://docs.docker.com/engine/install/ubuntu/>`_
| To avoid using the ``sudo`` command in docker, be sure to follow the `post-installation guide <https://docs.docker.com/engine/install/linux-postinstall/>`_
| Now we can build the OpenCN image :

.. code-block:: shell

	$ cd <OPENCN_HOME>
	$ docker build -t opencn/build-env docker/

Usage
=====

In the ``<OPENCN_HOME>/docker`` folder, there is scripts to build (``<OPENCN_HOME>/docker/build.sh``),
deploy (``<OPENCN_HOME>/docker/deploy.sh``), launch (``<OPENCN_HOME>/docker/st.sh``)
the emulation or generate the documentation (``<OPENCCN_HOME>/docker/generate_doc.sh``).
There usage are exactly the same the one in *Native* mode.

.. warning::
	
	The build of host(GUI) is currently not added in the docker.


**********************
Windows10 + WSL2 mode
**********************


General setup
=============================

| If you still want to use Windows as a primary OS, you can use WSL2 (Windows Subsystem for Linux) package.
| Assuming you are already using a Windows 10 install, here is the general step to follow : 

On Windows : 

* Install WSL2 on windows with Ubuntu 20.04LTS
* Install VcXsrv, this will be used later to display the GUI directly from WSL2.

On Linux : 

* Follow guideline of `docker mode`_ or `native mode`_.
* To run the GUI you will need additional package using XLaunch, run ``sudo apt install libxcb-xinerama0 libxcb-cursor0``
* To build & run the GUI, follow the guideline specific for :ref:`GUI compilation  <host>`

| One last step is needed to being able to acces an external storage ( USB or SDCARD).
| For this, you can follow `this guideline <https://github.com/jovton/USB-Storage-on-WSL2>`_.
| WARNING : However you will need a few tweaks for Ubuntu 20.04LTS : 

* For the installation package, Instead  of using pahole, install : ``sudo apt install dwarves``.
* When rebuilding WSL2 kernel, under "make menuconfig" remove anything "DEBUG_INFO_BTF" related.

Automate the launch
=============================

Now that the setup is working, we can automate the launch of the different services :

On Windows : 

* Go to ``%AppData%\Microsoft\Windows\Start Menu\Programs\Startup``
* Add a shortcut with the command ``"C:\Program Files\VcXsrv\vcxsrv.exe" :0 -multiwindow -clipboard -wgl`` (Or wherever you installed vcxsrv).
* TODO : iSCSI application (not supported yet)

On Linux : 

.. code-block:: shell

	$ sudo nano /etc/wsl.conf

And add the following lines :

.. code-block:: shell

	[boot]
	#Configure system to add external drive as /dev/sd*
	#WARNING : ISCI need to be properly configured, up and running on Windows
	systemd=true
	command="sudo modprobe -v libiscsi; sudo modprobe -v scsi_transport_iscsi; sudo modprobe -v iscsi_tcp; sudo modprobe -v libiscsi_tcp; sudo /etc/init.d/open-iscsi start; export WSLHOSTIP=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'); sudo iscsiadm -m discovery -t st -p $WSLHOSTIP; sudo iscsiadm -m node -T "iqn.1991-05.com.microsoft:target1" -p "$WSLHOSTIP:3260" -l"  
