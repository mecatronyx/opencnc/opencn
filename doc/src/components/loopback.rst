.. _loopback:

========
Loopback
========

The loopback HAL component has been designed for debug purpose. It contains
pairs of HAL pins. A pair of pins is composed of one input pin and one output
pins of the same type. The purpose of this component is to copy any data
written on an input pin to its paired output pin. The function that copies
the data from input to output pins is exported to be run in a real time thread.
The number of pins is provided dynamically as a string argument of the program.
Each char of the string describes the type of a pair of pins:

* 'f' -> float
* 's' -> 32-bit signed integer
* 'u' -> 32-bit unsigned integer
* 'b' -> bit

For instance, to load a loopback component with 2 pairs of float pins and
one pair of 32-bit signed pins, the configuration can be specified as follow:
**cfg=ffs**

.. note::

   The maximum number of pairs of pins allowed for this component is 25.

.. note::

   It is possible to generated up to 8 loopback components, however this feature
   has not been tested though.


Example
-------

This section provides an example of use of the loopback component using
the HAL command to load the component, add its real time function to a
thread and modify input pins.

Load a loopback component with one pair of float pins, one pair of 32-bit
signed pins, one pair of 32-bit unsigned pins and one pair of bit pins:

:code:`halcmd load loopback cfg=fsub`

Load a thread component:

:code:`halcmd load threads name1=loopback_thread period1=20000`

Add the real time function of the loopback component to the thread:

:code:`halcmd addf loopback.0 loopback_thread`

Show pins of the loopback component:

:code:`halcmd show pin loopback`

This command should show this information::

    [   68.064071] Component Pins:
    [   68.064397] Owner    Type  Dir         Value  Name
    [   68.064913]     14     bit   IN          FALSE  loopback.0.b_in.3
    [   68.065753]     14     bit   OUT         FALSE  loopback.0.b_out.3
    [   68.065753]
    [   68.066683]     14     float IN              0  loopback.0.f_in.0
    [   68.067594]     14     float OUT             0  loopback.0.f_out.0
    [   68.067594]
    [   68.068423]     14     s32   IN              0  loopback.0.s32_in.1
    [   68.069284]     14     s32   OUT             0  loopback.0.s32_out.1
    [   68.069284]
    [   68.070128]     14     s32   OUT            94  loopback.0.time
    [   68.070128]
    [   68.070938]     14     u32   IN     0x00000000  loopback.0.u32_in.2
    [   68.071753]     14     u32   OUT    0x00000000  loopback.0.u32_out.2
    [   68.071753]
    [   68.072620]     16     s32   OUT            94  loopback_thread.time

Start the HAL

:code:`halcmd start`

Modify the float input pin:

:code:`halcmd setp loopback.0.f_in.0 12.3`

Show pins of the loopback component

:code:`halcmd show pin loopback`

The value written to loopback.0.f_in.0 has been copied to loopback.0.f_out.0::

    [  137.240362]     14     float IN           12.3  loopback.0.f_in.0
    [  137.241265]     14     float OUT          12.3  loopback.0.f_out.0