.. _lcct:

====
LCCT
====

LCCT stands for Linux Control Toolkit and acts as an orchestrator between the different components, especially
to drive a M3 manufacturing machine.

The lcct component is not part of LinuxCNC originally.

