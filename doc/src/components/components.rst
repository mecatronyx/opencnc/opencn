.. _components:

#################
OpenCN Components
#################

.. toctree::
   :maxdepth: 5
   :hidden:
   
   ocno/ocno
   lcec/lcec
   feedopt/feedopt
   simple_pg
   plc/plc
   spindle
   mux
   sampler
   streamer
   trivkins
   kinematic/kinematic
   loopback
   threads
   lcct
    
OpenCN supports the following components:

* :ref:`OpenCN Orchestrator (OCNO) <ocno>`
* :ref:`LinuxCNC EtherCAT (LCEC) <lcec>`
* :ref:`Planning Optimizer (feedopt) <feedopt>`
* :ref:`Simple Profile Generator (simple_pg) <simple_pg>`
* :ref:`Machine Logic (plc) <plc>`
* :ref:`Spindle <spindle>`
* :ref:`Multiplexer (mux) <mux>`
* :ref:`Sampler <sampler>`
* :ref:`Streamer <streamer>`
* :ref:`Trivial kinematics (trivkins) <trivkins>`
* :ref:`Kinematic <kinematic>`
* :ref:`Loopback <loopback>`
* :ref:`Threads <threads>`
* :ref:`LinuxCNC Control Toolkit (LCCT) <lcct>`

A component is composed in three parts:

* **user space**: The main task of the user-space part is to parse the component
  parameters.
* **kernel**: It is in the kernel that the components PINs and  HAL functions
  (function exported by the component) are defined.
* **kernel real-time**: The HAL functions are RT functions.

The :numref:`_fig-component_arch` shows the different parts of a component.


.. figure:: /pictures/opencn_component_architecture.png
   :name: _fig-component_arch
   :alt: Components architecture
   :align: center
   :scale: 70 %

   Components architecture


