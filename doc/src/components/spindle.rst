.. _spindle:

=========
Spindle
=========

| The spindle component is responsible for managing a single spindle. 
| It controls the spindle's state, acceleration, and deceleration, while also offering options for managing temperature,
  cooling, and automatic tool changer (ATC) tool mounting.

| You can load a spindle component using the following command : 

.. code-block:: shell

	halcmd load spindle [--cfg=<CONFIG>] [--name=<NAME>] [--cooling] [--temperature]

Where the parameters are:


#. (optional) **cfg**: Select the wanted spindle type (default: ``std``). The supported types are:

    * std : Standard spindle
    * atc : Spindle has a Automatic Tool Changer. Provide 2 locking + 1 cone cleaning pin
#. (optional) **name**: Specify a name for the ``spindle.<NAME>``. If not specified, the default name will be the loaded spindle instance number, starting from ``0``, then ``1``, etc...
#. (optional) **cooling** : spindle has a cooling, provide a cooling pin
#. (optional) **temperature** : spindle has a temperature sensor, provide temperature monitoring pin

*****************
Features
*****************

----------------------------
Multiple parallel spindle
----------------------------

| The spindle component can be loaded up to eight times to manage up to eight different physical spindles.
| Each spindle operates independently and can be controlled asynchronously.

----------------------
Cooling
----------------------

| If your spindle has cooling capability, you can use this option.
| Depending if you also have the temperature option, the ``cooling`` pin will behave differently.
| If you *do not* have the temperature option & the spindle is enabled, the ``cooling`` pin is enabled.
| If you loaded the spindle with the temperature option, the cooling pin will be active/inactive to try to reach ``temp.target``. 

----------------------
Temperature management
----------------------

| The temperature option provide monitoring of the spindle temperature.
| If the spindle temperature is below ``temp.min`` parameter, the spindle is locked to a maximum velocity of ``temp.max-vel-min-temp``.
| If the spindle temperature is between min and max parameter, behavior is normal.
| If the spindle temperature is higher than ``temp.max`` parameter, the spindle shutdown and force the cooling (if present).

| The temperature option interact with the cooling ``option``. 
| If both are present, the spindle component will activate/desactivate the ``cooling.enable`` pin
  to try to reach the target temperature ``temp.target``  pin within the ``temp-target-tol`` parameter.

.. drawio-figure:: _pictures/spindle.drawio
    :page-name: temperature-target
    :alt: Behavior of the cooling pin
    :align: center

------------------
Speed override
------------------

| You can override the spindle ``cmd-velocity`` by modifying the ``cmd-override`` pin (Default value = 0[%]).
| For example, a value of `-10.0` would make the spindle turn 10% slower.
| This can become handy during machining when encoutering chatter, modifying the spindle velocity can help reduce this effect.

.. warning::

    To use the cmd-override pin, ensure that the velocity.tolerance parameter is set. 
    If the cmd-override exceeds the velocity.tolerance value, the spindle will momentarily indicate velocity-reached = false.
    This will cause OCNO to stop operation, assuming the spindle is struggling to maintain its speed (if OCNO is linked).
    To prevent this, ensure that changes to cmd-override values are smaller than the velocity.tolerance parameter.

------------------
ATC configuration
------------------

| In the ATC configuration, the spindle will only turn if both ``atc-sens-0`` and ``atc-sens-1`` pin are at ``1``.
| The componenet will also provide a ``cmd-free-tool`` pin to lock/unlock the tool in the spindle.
| When the tool is free, the ``cone-cleaning`` pin is active, false otherwise.

----------------------------
Regulation Acceleration Type
----------------------------

| There are two acceleration profiles available, Linear (regulation-type=0 / default) or a 3rd degree polynome (regulation-type=1) as shown in the plot below.
| The regulation with a polynome3 help to avoid current spike at the start/end of an acceleration/deceleration, it also limit the jerk during this phase.


.. figure:: _pictures/spinde_regulation_acceleration_type.png
    :alt: Regulation Acceleration Type available on the Spindle.
    :align: center

    Regulation Acceleration Type available on the Spindle.

**********
Parameters
**********

The following table describes the parameters of the spindle component available in all configurations.
For proper functionning of the spindle, some parameter has **mandatory** usage and need to be set properly for the spindle to work.

=================================       ================    ================    ============================    ====================================================================================================
Parameter name                          Type                Direction           Usage                           Description
=================================       ================    ================    ============================    ====================================================================================================
spindle.<name>.regulation.accel              HAL_FLOAT               HAL_RW          Mandatory                  Acceleration parameter of your spindle in [turn/s²]
spindle.<name>.regulation.type               HAL_U32                 HAL_RW          Optionnal                  Regulation type for the acceleration/deceleration cuve (0 = Linear(default) or 1=Poly3)
spindle.<name>.velocity.min                  HAL_FLOAT               HAL_RW          Mandatory                  Minimum required velocity in [turn/s].
spindle.<name>.velocity.max                  HAL_FLOAT               HAL_RW          Mandatory                  Maximum possible velocity in [turn/s]
spindle.<name>.velocity.tolerance            HAL_FLOAT               HAL_RW          Optionnal                  Tolerance in [%] over the spindle velocity for the status of ``velocity-reached`` pin
=================================       ================    ================    ============================    ====================================================================================================

-------------------------------------
Mode specific parameter : Temperature
-------------------------------------

The following table describe the parameter for the spindle component added when the temperature option is set 

=================================       ================    ================    ============================    ====================================================================================================
Parameter name                          Type                Direction           Usage                           Description
=================================       ================    ================    ============================    ====================================================================================================
spindle.0.temp.target                   HAL_FLOAT               HAL_RW          Optionnal                       The target temperature the spindle will try to reach through the ``cooling`` pin. (Default = 0[°C])
                                                                                                                Cooling option need to be present. 
spindle.0.temp.target-tol               HAL_FLOAT               HAL_RW          Optionnal                       Tolerance in [°C] over the control of the target temperature. 
spindle.0.temp.min                      HAL_FLOAT               HAL_RW          Mandatory                       Minimum acceptable spindle temperature to reach ``max-velocity``.
spindle.0.temp.max-vel-min-temp         HAL_FLOAT               HAL_RW          Mandatory                       Maximum velocity possible when the spindle is under the ``min-temp``.
spindle.0.temp.max                      HAL_FLOAT               HAL_RW          Mandatory                       Maximum acceptable temperature in [°C]. Spindle will shutdown if spindle temperature is higher.
=================================       ================    ================    ============================    ====================================================================================================

****
PINs
****

The following table describe the PINs for the spindle component available for all configurations

=================================     ================    ================      =====================================================================================================================
Pin name                                Type                Direction             Description
=================================     ================    ================      =====================================================================================================================
spindle.0.cmd-velocity                  HAL_FLOAT           HAL_IN              Set the wanted spindle velocity in [turn/s].
spindle.0.cmd-enable                    HAL_BIT             HAL_IN              Enable or disable the spindle rotation.
spindle.0.cmd-fault-reset               HAL_BIT             HAL_IN              Reset the fault of the spindle.
spindle.0.cmd-override                  HAL_FLOAT           HAL_IN              Apply an override over the ``cmd-velocity`` in [%].
spindle.0.target-velocity               HAL_FLOAT           HAL_OUT             Target velocity (usually connected to a drive).
spindle.0.current-velocity              HAL_FLOAT           HAL_IN              Current speed of the spindle.
spindle.0.set-mode-csv                  HAL_BIT             HAL_OUT             To the drive, set the spindle in Cyclic Synchronous Velocity (CSV) mode.
spindle.0.set-mode-inactive             HAL_BIT             HAL_OUT             To the drive, set the spindle in inactive mode.
spindle.0.in-mode-csv                   HAL_BIT             HAL_IN              From the drive, spindle is in CSV mode.
spindle.0.in-mode-inactive              HAL_BIT             HAL_IN              From the drive, spindle is in inactive mode.
spindle.0.fault-reset                   HAL_BIT             HAL_OUT             To the drive, attempt to reset the fault on the spindle.
spindle.0.in-fault                      HAL_BIT             HAL_IN              From the drive, spindle is in fault.
spindle.0.can-turn                      HAL_BIT             HAL_OUT             Spindle is capable to start motion, regarding it's configuration and temperature.
spindle.0.machine-active                HAL_BIT             HAL_IN              The machine is active (for example milling), avoid tool unlocking  on ATC config by mistake.
spindle.0.velocity-reached              HAL_BIT             HAL_OUT             Spindle velocity reached its speed, in the ``velocity.tolerance`` tolerance. see :ref:`spindle_velocity_reached_pin`
spindle.0.air-protection                HAL_BIT             HAL_OUT             Protect spindle of debris entering, active when spindle is enabled.
=================================     ================    ================      =====================================================================================================================

------------------------------------
Mode specific pins : Temperature
------------------------------------

The following table describe the PINs for the spindle component added when the temperature option is set 

=================================     ================    ================      =================================================================================================================
Pin name                                 Type                Direction             Description
=================================     ================    ================      =================================================================================================================
spindle.0.temp.actual                   HAL_FLOAT           HAL_IN              From the temperature sensor, indicating the spindle temperature in [°C]
=================================     ================    ================      =================================================================================================================

------------------------------------
Mode specific pins : Cooling
------------------------------------

The following table describe the PINs for the spindle component added when the cooling option is set 

=================================     ================    ================      =================================================================================================================
Pin name                                 Type                Direction             Description
=================================     ================    ================      =================================================================================================================
spindle.0.cooling.enable                   HAL_BIT             HAL_OUT          Activate the cooling of the spindle.
                                                                                If the spindle has a temperature sensor, and it is in over-temperature,
                                                                                the cooling is forced.
=================================     ================    ================      =================================================================================================================

-------------------------------------
Mode specific pins : ATC type
-------------------------------------

The following table describe the PINs for the spindle component added when the ATC configuration is set 

=================================     ================    ================      =================================================================================================================
Pin name                                 Type                Direction             Description
=================================     ================    ================      =================================================================================================================
spindle.0.atc.cmd-free-tool             HAL_BIT             HAL_IN              Ask the spindle component to free the tool 
spindle.0.atc.free-tool                 HAL_BIT             HAL_OUT             To your ATC control, actually free the tool
spindle.0.atc.clean-cone                HAL_BIT             HAL_OUT             Activate cone cleaning when ``cmd-free-tool`` is active
spindle.0.atc.sens-0                    HAL_BIT             HAL_IN              ATC sensor n°1 
spindle.0.atc.sens-1                    HAL_BIT             HAL_IN              ATC sensor n°2
=================================     ================    ================      =================================================================================================================

------------------------------------
Debug specific pins
------------------------------------

If the constant ``DEBUG_SPINDLE`` is set in ``spindle.c`` file, the component export more pins : 

=================================     ================    ================      =================================================================================================================
Pin name                                 Type                Direction             Description
=================================     ================    ================      =================================================================================================================
spindle.0.d-state-elec                  HAL_U32             HAL_OUT             Spindle FSM electrical status, regarding ``Spindle_elec_state_t``
spindle.0.d-state-speed                 HAL_U32             HAL_OUT             Spindle FSM speed status, regarding ``Spindle_speed_state_t``
=================================     ================    ================      =================================================================================================================


*****
Usage
*****

When using an already setup spindle components, you can interact with the 4 following pin : 

* ``spindle.0.cmd-velocity``
* ``spindle.0.cmd-enable``
* ``spindle.0.cmd-fault-reset``
* ``spindle.0.cmd-override``

The commande below provide an example usage/setup of the spindle : 

.. code-block:: shell

    # Load the component
    halcmd load spindle --cfg=std --name=main

    # Setup mandatory parameter
    spindle.main.regulation.accel 1000
    spindle.main.velocity.min 500
    spindle.main.velocity.max 4500
    spindle.main.velocity.tolerance 30

    #Link the spindle to your drive, see section "Pin Interface"
    halcmd net <signal-name> spindle.main.<......> lcec.0.<Your_drive>.<....>

    #Always remember to link the update function, otherwise nothing will happen
    halcmd addf spindle.main lcec_thread.0 #Using a thread for the spindle other than lcec is strongly discouraged on a physical CN.

    # Start the HAL
    hacmd start

    #Then you can start your spindle with : 
    halcmd setp spindle.main.cmd-velocity 500 # Set the target speed rotation at 60 turn/s for example (need to be higher than spindle.main.velocity.min and lower than spindle.main.velocity.max)
    halcmd setp spindle.main.cmd-enable 1 # Enslave the spindle and start the rotation

.. note::
    The spindle can be linked to OCNO - for example - for automatic management of the spindle speed and state

.. warning::
    If the spindle is in transition between 2 speed, and a new velocity is requested, it will reach the first ordered speed before then transitioning to the second command.
    Put in another way, if the spindle is transitionning in speed to a requested value, it will first end it's transition, before transitionning to a new command.
    Put in another way, when the spindle is accelerating/decelerating, it will not interrupt it's transition if a new speed is requested.



.. _spindle_velocity_reached_pin:

***********************************
Velocity-reached pin specificity
***********************************

| If you add/modify components, and you need to check the ``velocity-reached`` pin, beware that
    you need to wait at least **one** RT cycle after changing one of the ``spindle.<name>.cmd-XXXXXX`` pin.
| Otherwise, the spindle's component will not have the time to update the status of ``velocity-reached`` pin.

***********************
Timing Diagram
***********************
--------------------
Simplified
--------------------

This is a simplified timing diagram that explain a possible use-case for the control of a spindle through the spindle's component :

#. Spindle is properly loaded, awaiting command. Nothing is happening.
#. User set a wished velocity for the spindle to reach. Spindle is still disabled. Nothing is happening.
#. User ask a rotation of the spindle : Spindle is put in CSV mode, the spindle's component is managing the acceleration of the spindle to the requested velocity.
#. User do nothing during this timeframe. Spindle as reached it's requested velocity, spindle's component is maintaining the speed of the spindle. 
#. User ask a higher value of rotation speed, spindle's component is accelerating to the given speed.
#. Same as state 4 : User did nothing during this timeframe. Spindle as reached it's requested velocity, spindle's component is maintaining the speed of the spindle. 
#. User ask to stop the rotation of the spindle with ``cmd-enable``, spindle's component is managing the deceleration.
#. Spindle's component is still decelerating the spindle.
#. Since ``cmd-enable`` is at ``0`` and the spindle's component finished the deceleration (thus current-velocity = 0) =>spindle's component is setting the spindle in inactive mode.
#. User set the ``cmd-target-velocity`` to ``0``. Nothing is happening

.. drawio-figure:: _pictures/spindle.drawio
    :page-name: cmd-vel-simplified
    :alt: Simplified timing diagram for the spindle
    :align: center
    
    Simplified timing diagram for the spindle

--------------------
Detailed
--------------------

This timing diagram aim to explain in details the management of the spindle : 

.. note::
    | For simplicity sake, the management of  the spindle controlling mode (Inactive or Cyclic Synchronous Velocity - CSV)  is not represented in this diagram.
    | For information, the spindle is put in CSV mode if ``cmd-enable`` is ``true``, inactive mode otherwise.

.. drawio-figure:: _pictures/spindle.drawio
    :page-name: cmd-vel-complete
    :alt: Complete timing diagram of the spindle
    :align: center

    Complete timing diagram of the spindle

********************
Testing - CI/CD
********************

| A complete set of file is provided to test the spindle component in VM (x86-qemu or virt64), see :ref:`environment`.
| If you wish to modify the spindle component, it's strongly adviced to test your modification using thoses script.
| Obviously, you will need to add the test script for any new features you may add.
| Repo path : ``<OpenCN-Repo>/agency/rootfs/board/common/rootfs_overlay/etc/opencn/validation/components/spindle/``
| On the target, you can find the files under ``/etc/opencn/validation/components/spindle/``