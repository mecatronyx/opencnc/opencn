.. _trivkins:

========
Trivkins
========

The *trivkins* component exports the following three  functions for a trivial
kinematics: :func:kinematicsType, :func:kinematicsForward and :func:kinematicsInverse()

Usage
-----

The trivkins has two parameters:

* Type: type of kinematic used. Supported values:
   * **‘B’** or **‘b’**: Both, the forward and inverse kinematics are defined.
   * **‘F’** or **‘f’**: Forward, only the forward kinematics exists.
   * **‘I’** or **‘I’**: Inverse only the inverse kinematics exists.
   * **‘1’**:  Identity. joints and world coordinates are the same, as for s
     lideway  machines (XYZ milling machines).
* Coordinates: the coordinates used by the machine (standard coordinate name,
  [xyzabcuvw]). For example, for a XYZ milling machine, the coordinates parameter
  is XYZ

Invocation:

.. code-block:: shell

   halcmd load trivkins type=<TYPE> coordinates=<COORDINATES>

