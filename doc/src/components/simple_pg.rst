.. _simple_pg:

####################################
Simple Profile Generator (simple pg)
####################################

The Simple PG component is a simple profile generator. It generates a smooth C^1
transition (cosinus based motion computation ) between a given initial position
x0 and a given final position x1. The shape of the transition is the following:

.. figure:: _pictures/simple_pg_shape.png
	:name: _fig-Simple PG shape
	:alt: Simple PG shape
	:align: center

	Simple PG shape

It can be load with the following command:

.. code-block:: shell

	./halcmd load simple_pg joint_nr=<JOINT NR>

Where the parameters are:

* ``joint_nr``: number of joints.


*****************
HAL PINS & params
*****************

**Global PINs**

	===================  =======  =========  ================================
	Name                 Type     Direction  Description
	===================  =======  =========  ================================
	simple_pg.enable     HAL_BIT  HAL_IN     Enable the Simple PG
	simple_pg.is_enable  HAL_BIT  HAL_OUT    Indicates is the Simple PG is
	                                         enabled or not
	===================  =======  =========  ================================

**Per joint PINs**

	=================================  =========  =========  ===========================
	Name                               Type       Direction  Description
	=================================  =========  =========  ===========================
	simple_pg.jointN.cmd-pos           HAL_FLOAT  HAL_IN     Final target position
	simple_pg.jointN.current-position  HAL_FLOAT  HAL_IN     Current joint position
	simple_pg.jointN.relative-pos      HAL_BIT    HAL_IN     The target position is
	                                                         relative to the current
	                                                         position
	simple_pg.jointN.stop              HAL_BIT    HAL_IN     Stop PG processing on this
	                                                         joint
	simple_pg.jointN.target-position   HAL_FLOAT  HAL_OUT    Set point computed by the
	                                                         PG
	simple_pg.jointN.is_running        HAL_BIT    HAL_OUT    Indicates if the PG process
	                                                         is active on this joint
	=================================  =========  =========  ===========================

**Per joint PARAMs**

	==================================== 	=========  =========  	==============================================================
	Name                                 	Type       Direction  	Description
	==================================== 	=========  =========  	==============================================================
	simple_pg.jointN.speed               	HAL_FLOAT  HAL_RW     	Target speed, in unit/min (Default = 300unit/min)
	==================================== 	=========  =========  	==============================================================
