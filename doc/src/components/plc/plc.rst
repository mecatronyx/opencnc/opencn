.. _plc:

###################
Machine Logic (PLC)
###################

.. toctree::
   :maxdepth: 5
   :hidden:

   plc_micro5

The *Machine Logic* (PLC) component contains specific code to support different
features like the logic for a machine type or algorithms for performances
evaluation.
It allow the user to write RT running code for specific use case and customization
of a machine behavior

It can be load

.. code-block:: shell

	halcmd plc cfg=<PLC CONFIG>

Where the ``<PLC CONFIG>`` is one of the supported ``plc``, see :ref:`here<plc configs>`)

.. note::

	It is possible to load multiple different PLC.

The following *Machine Logic* are supported:

.. _plc configs:

* :ref:`Micro5 milling machine (cfg = micro5) <plc micro5>`

****************
New PLC creation
****************

The creation of a new ``PLC`` is quite easy:

The ``PLC`` functionality (PINs, Params, function) should be implemented in a new
C file in ``agency/linux/opencn/components/plc/`` folder (don't forget to update
the ``Makefile`` present in this folder accordingly).

Once the new ``plc`` functionality is implemented, it can be integrated by following
these steps:

* Add an entry in PLC_CFG enumeration (``linux/opencn/include/opencn/uapi/plc.h``
  file)
* user space: Inserted the new ``plc``  in the ``supported_cfg`` array (``usr/target/include/components/plc/plc.c``
  file).
* In the file with the new ``plc`` functionally, add an ``plc_<PLC_NAME>_init``
  function. This function fills a variable of ``plc_func_param_t`` type and
  call ``plc_init()`` function. The following example shows the ``micro5``
  ``plc`` init function.

.. code-block:: C

	int plc_micro5_init(const char *name)
	{
		int ret;

		plc_func_param_t params;

		params.name       = name;
		params.comp_id    = comp_id;
		params.data       = micro5_data;
		params.data_size  = sizeof(plc_micro5_data_t);
		params.func       = plc_micro5_func;
		params.hal_pins   = micro5_pins;
		params.hal_params = NULL;

		ret = plc_init(&params);

		return ret;
	}

* In the ``plc.c`` file:
	* Add the init function prototype in the section at the beginning of the ``plc.c``
	  file
	* Add a new condition in the switch case to call the new ``plc`` init function
	  in the :func:`plc_app_main` function.
	* path: ``agency/linux/opencn/components/plc/``
