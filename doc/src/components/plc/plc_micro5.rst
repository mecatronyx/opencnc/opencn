.. _plc micro5:

######
Micro5
######

.. warning::

	Under construction

The Micro5 PLC provide feature and safety for the operation of the Micro5 Machine at the HEIG-VD.
These safety and feature includes :

* Emergency stop handling
* Axis error handling
* Sealing of rotary axis
* Palette release & locking
* Z axis break release/locking.
