.. warning::
    This description is not uptodate. The last implementation for the 5 axes did modify some parts of the coder whole chain. 

Queue management
^^^^^^^^^^^^^^^^


Overview
""""""""

The first one is a custom ``CurvStruct`` queue, implemented using a C++
``std::vector`` because Matlab’s dynamic resizing is way too slow. This
wrapper combines the Mex API method for in-Matlab use and
``coder.ceval`` calls for native generation. All of this is furthermore
encapsulated in a queue class. All of the matlab functions are in
``c_planner_v5/queue_foo.m``. The class definition is in
``queue_coder.m``.

We will go over the implementation of the ``queue_set`` and
``queue_get`` functions.

.. code-block:: matlab

   % File: queue_set.m
   function queue_set(ptr, index, value)
   if coder.target('matlab')
       queue_set_mex(ptr, index, value);
   else
       coder.ceval('c_queue_set', uint64(ptr), uint32(index), value);
   end
   end

   % File: queue_get.m
   function value = queue_get(ptr, value_type, index)
   if coder.target('matlab')
       value = queue_get_mex(ptr, index);
   else
       value = value_type;
       coder.ceval('c_queue_get', uint64(ptr), uint32(index), coder.ref(value));
   end
   end


Mex
"""

This should be familiar now. Notice how there is no
``coder.target('mex')`` as we never generate code when mexing, it is
directly built using the Mex API.

.. code-block:: cpp

   // File: queue.h
   // In this file we define the C++ queue structure
   #pragma once

   #include <vector>
   #include <mex.hpp>
   #include <mexAdapter.hpp>

   struct Queue {
       // Use a vector of the Array mex datatype
       std::vector<matlab::data::Array> elems;
   };


   // File: queue_set.cpp
   #include "queue.h"

   using matlab::mex::ArgumentList;
   using namespace matlab::data;

   class MexFunction : public matlab::mex::Function
   {
   public:
       void operator()(ArgumentList outputs, ArgumentList inputs)
       {
           if (inputs.size() != 3) {
               return;
           }

           uint64_t qptr = inputs[0][0];
           Queue *q = reinterpret_cast<Queue *>(qptr);

           // convert matlab 1-base index to 0-base index
           uint64_t index = static_cast<uint64_t>(inputs[1][0]) - 1;

           if (index >= q->elems.size()) {
               std::shared_ptr<matlab::engine::MATLABEngine> matlabPtr = getEngine();
               matlab::data::ArrayFactory factory;
               matlabPtr->feval(u"error", 0,
                                std::vector<matlab::data::Array>(
                                    {factory.createScalar("queue_set: index(%d) out of range (%d)"), 
                                     factory.createScalar(index),
                                     factory.createScalar(q->elems.size())}));
           }
           // generic way to access the element at index 'index'
           // even on non-indexable datastructures like a list
           auto it = q->elems.begin();
           std::advance(it, index);
           *it = inputs[2];
       }
   };


Native C/C++ code
""""""""""""""""""

One new thing to take note of in ``queue_get`` is ``coder.ref(value)``.
This guarantees that the argument is passed as a pointer in the call to
the function. We can see this in the excerpt of the generated code.
Since the queue is encapsulated in a class, this generates class members
and optimizes away the call to the function ``queue_get`` itself.

.. code-block:: cpp

   // File: excerpt from generated.cpp
   void queue_coder::get(int b_index, CurvStruct *value) const
   {
       *value = this->value_type;
       c_queue_get(this->ptr, static_cast<unsigned int>(b_index), value);
   }

Since native code generation makes direct calls to some functions, we
need to implement these as well. They can be found in
``src/functions.cpp``.

.. code-block:: cpp

   // File: excerpt from src/functions.cpp
   #include "generated.h"
   using CurveVector = std::vector<ocn::CurvStruct>;
   void c_queue_get(ocn::uint64m_T ptr, int32_t index, ocn::CurvStruct *value)
   {
       auto q = reinterpret_cast<CurveVector*>(ptr.chunks[0]);
       *value = q->at(index - 1);
   }

   void c_queue_set(ocn::uint64m_T ptr, int32_t index, ocn::CurvStruct value)
   {
       auto q = reinterpret_cast<CurveVector*>(ptr.chunks[0]);
       q->at(index - 1) = value;
   }

Notice how the functions use ``ocn::CurvStruct`` as argument types and
include ``generated.h``. These do not exist before generating. But this
is ok because generation occurs first and then the build happens.

The functions are mexed using ``mex_queue.m``. Code generation for the
native environment is all done in a single call to ``codegen`` in
``generate_c.m``.

If you look at it, you can notice that there is not a single queue
function present in the entry points. This is because it is *not* used
*directly* by the native code.
