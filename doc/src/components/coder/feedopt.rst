
.. _feedopt_coder:

.. warning::
    This description is not uptodate. The last implementation for the 5 axes did modify some parts of the coder whole chain. 

*******
Feedopt
*******

.. toctree::
   :maxdepth: 5
   :hidden:
   
   coder
   
- :ref:`Coder used in conjunction with Matlab <coder>`

The position values are generated with a trajectory planner implemented
in Matlab and then converted into ``C++`` code using the Matlab
**coder**. This trajectory planner, called **Feedopt** (Feedrate
Optimizer), accepts G-code (RS274) as input, using the interpreter from
LinuxCNC (release 2.7), called **RS274**.

.. uml::

    skinparam defaultFontName hack
    package Kernel {
        object lcct
        object "feedopt" as feedoptK
        object lcec
    }

    package Userspace {
        object "feedopt" as feedoptU
        object server
        object RS274
    }
     
    package Remote {
        object GUI
        object client 
    }
    
    lcct -- feedoptK: pins
    lcct -- lcec: pins
    feedoptU -- RS274
    GUI -- client
    server -- client: capnp
    server -- feedoptU: pins
    server -- lcct: pins
    feedoptK -- feedoptU: pins + sharedbuf

Several steps are required to go from g-code to position values and CNC
motion: 

- Create ``CurvStruct``\ s from g-code, which are matlab structures that describe geometric primitives (Line, Helix), and push them to a queue 
-  Analyze the queue and make geometric optimization on the path 
- Optimize the path into a minimal-time, jerk-limited trajectory using b-splines for the velocity profile 
- Resample the resulting trajectory segments at 10Khz in the user-side and send the resulting position values to **lcct** through the kernel-side feedopt 
- When certain conditions are met, lcct sends feedopt positions to **lcec**, the EtherCAT master


The goal when using Matlab is to have the same codebase for easy
algorithm testing and then just transcripts it to native C/C++ code. External
libraries, source code, or even Matlab functions themselves can be
compiled into shared libraries that Matlab can call as normal functions.
This process is called **Mex**-ing. Some issues arise from these steps:

- Matlab has no B-spline implementation and thus we use the GSL (GNU
  Scientific Library) one. This means that we need to write Matlab
  functions that can selectively use a Mex library that calls GSL
  functions or direct calls to GSL when using native code. 
- Matlab has
  poor dynamic resing of arrays. The performance is unacceptable and thus
  custom queues are used 
- RS274 from LinuxCNC is used, which is a big C++
  codebase, and it needs to be compiled into a Mex library as well 
- All of the code needs to eventualy be callable from pure C++

