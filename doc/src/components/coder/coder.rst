
.. _coder:

Matlab coder introduction
=========================
  
.. warning::
    This description is not uptodate. The last implementation for the 5 axes did modify some parts of the coder whole chain. 


Overview
--------

The matlab coder is a tool for generating C/C++ code from matlab
functions as well as for making Mex libraries from them. It can be
configured in several modes, depending on the generation target. The
modes in use in OpenCN are: 

- Embedded Library: Generates code that 100%
  compatible with standard C/C++ without any matlab runtime dependencies,
  therefore suitable for an embedded environment. Here it is a library
  because it will only be called from other code, not a standlone
  executable. 
- Mex (MATLAB executable generation): Generates code for creating libraries that can be
  used by Matlab as normal functions, either for speeding up execution of
  existing scripts or bringing in external libraries. It also allows
  directly compiling C++ files without any generation when using the Mex
  headers and API.

.. note::
    All documentation up to date on Matlab Coder can be found, after login,
    on `Mathworks site <https://www.mathworks.com/help/pdf_doc/coder/index.html>`_.


Coder Quick Start for C/C++ code generation
-------------------------------------------

- Within a Matlab script, we first have to create a coder configuration object:
  
  .. code-block:: matlab

        % This prepares Matlab Coder to generate static standalone C/C++ code.
        % This also creates a coder.EmbeddedCodeConfig object without Embedded Coder toolbox needed.
        cfg = coder.config('lib', 'ecoder', true);

- | Then, we should change the configuration juste created, according to the needs to our application.
  | Here is example with only some config parameters set in OpenCN:

  .. code-block:: matlab

        cfg.FilePartitionMethod = 'MapMFileToCFile';
        cfg.InlineBetweenUserFunctions = 'Speed';
        cfg.SupportNonFinite = false;
        cfg.CustomHeaderCode = '#include "functions.h"';
        % etc, etc ...

  More configuration options are explained in an example file :file:`generate_c.m`, in this document below
  (see :ref:`Example<coder_generate_c_example>`).

  | All options used for C++ code generation for OpenCN are detailed in comments of the :file:`generate_c.m` script.
  | This script has been used to produce files for all targets (arm 32 and 64 bits, and x86 64 bits).
  | This file is located in ``<OPENCN_HOME>/agency/usr/matlab/common``.
  | Here is a direct link to this file on
    `Gitlab <https://gitlab.com/mecatronyx/opencnc/opencn-matlab/-/blob/7-some-matlab-coder-inlining-configurations-may-make-disappear-class-methods-signatures-in-the/common/generate_c.m>`__.

- | Finally, :code:`codegen` command must be invoked, in order to generate code, here is an example.
  | Functions passed to :code:`codegen` as arguments are called coder 'entry-points'.
  
  .. code-block:: matlab
    
        % coder.OutputType('function2') returns an object that specifies a type of function2().
        % function2() must be an entry-point.

        % coder.typeof(0, [1, 1024], [0, 1]) tells to Matlab Coder that
        % the 2nd argument of function1() is a line vector of doubles
        % with the number of columns, that may be up to 1024.

        codegen('-config', cfg,'-d', output_directory, '-o', 'output_file_name_c_code', ...
            'function1', '-args', {coder.OutputType('function2'), coder.typeof(0, [1, 1024], [0, 1])}, ...
            'function2', '-args', 0);


.. note::

  - C++ files have been already generated, and they are a part of opencn-matlab submodule.
  - Don't forget to run :code:`git gubmodule update --init` (see :ref:`User Guide<user_guide>`)
  - Matlab is not needed to run OpenCN.
  - One needs to execute :file:`generate_c.m` script only when adding/changing Matlab code, for generate new C++ code.
  - C++ files, actually present in the repo, were geneated with MATLAB Coder 5.3 version and MATLAB R2021b Update 2 version.



Coder Quick Start for mexing matlab functions
---------------------------------------------

- Within a Matlab script, we first have to create a coder configuration object:
  
  .. code-block:: matlab

        % This prepares Matlab Coder to generate files executable by Matlab (MEX files)
        cfg = coder.config('mex');

- | Then, as above, we should change the configuration juste created, according to the needs to our application.
  | Here is example with only some config parameters set in OpenCN:
  
  .. code-block:: matlab
    
        cfg.GenCodeOnly = false;
        cfg.FilePartitionMethod = 'SingleFile';
        cfg.PreserveArrayDimensions = true;
        cfg.GenerateReport = false;
        cfg.TargetLang = 'C++';
        cfg.CppNamespace = 'ocn';
        % etc, etc
    
  | There is an example file :file:`generate_mex.m`, in this document below, (see :ref:`Example<coder_generate_mex_example>`).
  | All options used for mexing Matlab OpenCN files are detailed in comments of the :file:`generate_mex.m` script.
  | This script have been used to produce files for all targets (arm 32 and 64 bits, and x86 64 bits).
  | This file is located in ``<OPENCN_HOME>/agency/usr/matlab/common``.
  | Here is a direct link to this file on
    `Gitlab <https://gitlab.com/mecatronyx/opencnc/opencn-matlab/-/blob/7-some-matlab-coder-inlining-configurations-may-make-disappear-class-methods-signatures-in-the/common/generate_mex.m>`__.

- | Finally, invoke codegen with Matlab entry-point functions, that you need to mex,
    with the config object (cfg), created as explained above, hereafter is an example.
  | Functions passed to :code:`codegen` as arguments are called coder 'entry-points'.

  .. code-block:: matlab
        
        % Returns an object that specifies a type of function2().
        % function2() must be an entry-point.
        function2_type = coder.OutputType('function2');

        % In this example function1() first argument is a column vertor of fixed size 3.
        % The second argument is the output of function2().

        % In this example function2() 1st argument is a matrix of int with both dimensions of variable size.
        % The 2nd argument is a character string that can be up to 2000 characters long.

        codegen('-config', cfg,'-d', output_directory, '-o', 'output_file_name_mex', ...
            'function1', '-args', {[0, 0, 0]', function2_type}, ...
            'function2', '-args', {coder.typeof(int32(0), [Inf, Inf], [1, 1]), coder.typeof(' ', [1, 2000], [0, 1])});

.. note::

    One can also mex C/C++ functions by simply passing a file name to Matlab ``mex`` function, as presented here :

    .. code-block:: matlab

        mex path/to/source/file/example.cpp -output output_file_name_mex

    | :file:`example.cpp` is a wrapper to be implemented, using the MEX API.
    | Detailed explanations are given in the section :ref:`Using MEX API<coder_using_mex_api>`.
    | Here is a direct link to such directly mexed cpp OpenCN files on
      `Gitlab <https://gitlab.com/mecatronyx/opencnc/opencn-matlab/-/tree/master/common/mex/queue>`_.


Briefly about code generation for Matlab classes
------------------------------------------------

- | In order to generate C++ classes from Matlab classes, one needs to create
    an object of this class in a Matlab function and pass this function
    as an entry-point to codegen as an argument, in code generation script.
  | These include all MATLAB classes such as value classes, handle classes, and system objects.

.. warning::

    | Class Methods signatures may be inlined when using the default inlining Matlab Coder configuration.
    | This makes them impossible to be directly called from an external C++ code.
    | A way to preserve theses signatures is to set the inlining configuration to 'Readability':

    .. code-block:: matlab

        cfg.InlineBetweenUserFunctions = 'Readability';

    | Another way is to add the :code:`coder.inline('never')` directive in the definition of all
      methods in their Matlab classes, that are called from outside of the generated code,
      so that the global inlining configuration can be kept 'Speed' (default value).
    | This method has beed adopted for code generation in OpenCN.

- | About code generation for classes, see `Mathworks documentation <https://www.mathworks.com/help/coder/ug/generate-c-classes-from-matlab-classes.html>`__.
  | About Coder limitations with classes, see the following `section <https://www.mathworks.com/help/coder/ug/generate-c-classes-from-matlab-classes.html#mw_c6a4d2cc-1651-4722-9469-b551891a505f>`__.

Code generation from Matlab functions with calls to external C/C++ functions
----------------------------------------------------------------------------

- These commands are most used in OpenCN when there are C code calls on :code:`codegen()` entry-points functions call paths.

 .. code-block:: matlab

    % File: ExampleMFunction.m (called by an entry-point function)

    % That coder needs to know, where c_function_name() is implemented.
    coder.updateBuildInfo('addIncludePaths','$(START_DIR)\codegen\lib');
    coder.updateBuildInfo('addSourceFiles','file_name.cpp');

    % This includes the header line ' "#include "file_name.h" ' in the generated C/C++ file.
    % In this way, c_function_name() definition can be found at build time.
    coder.cinclude('file_name.h');

    % Add link flags to the precompiled library
    coder.updateBuildInfo('addLinkFlags', '-lthelibrary');

    % coder.ceval() calls c_function_name(), located in 'file_name.cpp', with specified arguments.
    % 'ptr' and 'index' must be defined within this Matlab script/function or passed as arguments.
    % 'value' is the output variable of c_function_name(). Its type must be known before calling coder.ceval().
    % coder.ref(value) specifies a pointer (reference) to 'value', so that the output variable can be set in the generated code.
    value = uint32(0);
    coder.ceval('c_function_name', uint64(ptr), uint32(index), coder.ref(value)));
    

Limitations
-----------

Unfortunately, there are a lot of limitations when using this tool that
have to accounted for. Here are some of them:

- Return arguments always have to be assigned on all call paths

- | In Matlab, after assigning ``var = 0`` or ``var = 0.0``, ``var`` has a 'double' type.
  | In order to generate a variable of int type, we need to cast explicitly: ``var=int32(0)``.
  
- No handle classes or classes *containing* handle classes can be used as arguments to entry-point functions.
  
- When creating new structures, the fields cannot re-use the ones of the structure that is being created:
 
  .. code-block:: matlab
     
    function S = CreateStruct()
        S.a = 42;
        S.b = S.a + 10; % This line is not accepted by the coder
    end
  
  I would say this is bad practice with complicated structures anyway, it is
  much easier to navigate source code when classes clearly define a list of 
  properties, their dimensions, types and default values.
  

Let’s do two examples of using this tool before going to how OpenCN
libraries work.

.. _coder_ext_lib_example:

Calling external libraries from Matlab
======================================      

Imagine we have a library that we want to use in Matlab but we either
don’t have access to its source code, or it is too complex anyway and
not at all worth it re-implementing it. This library has a class
``ExampleClass`` with a method ``getValue`` returning an int.


Using code generation
---------------------

When using code generation, an intermediate C++ file is required for
wrapping the library since we cannot handle classes directly. We can
then call C++ functions using
``coder.ceval('functionName', arg1, arg2)``.

.. uml::

    skinparam defaultFontName hack
    left to right direction
    object "Matlab Script" as matlabScript
    object Wrapper
    object ExampleClass
    matlabScript-->Wrapper: coder.ceval
    Wrapper-->ExampleClass

We need a way to store a reference to the class instance in Matlab.
Since a pointer type does not exist, a ``uint64`` will be used instead.
An ``CreateExampleClass`` function will be responsible of creating this
instance through the wrapper and then returing the ``uint64``-cast
pointer.

**Matlab-side functions**:

.. code-block:: matlab

   % File: CreateExampleClass.m
   function handle = CreateExampleClass()
   if coder.target('mex')
       % If we are generating code for Mex

       % This line, even if the value is unusued, declares
       % the type of 'handle' because Matlab has no way of knowing it
       % purely from coder.ceval
       handle = uint64(0);

       % insert a call to initLibrary into the generated code
       handle = coder.ceval('createExampleClass');

       % Add the wrapper file to the compilation
       coder.updateBuildInfo('addSourceFiles','wrapper.cpp');

       % -------
       % Add link flags to the precompiled library
       coder.updateBuildInfo('addLinkFlags', '-lthelibrary');
       % OR
       % Add library source
       coder.updateBuildInfo('addSourceFiles', 'library_source.cpp');
       % -------

       % Include the wrapper header file
       % This will be inserted at the beginning of the generated code
       coder.cinclude('wrapper.h');
   else
       % If not, just call the mexed library
       handle = CreateExampleClass_mex()
   end
   end

   % File: ExampleClassGetValue.m
   function value = ExampleClassGetValue(handle)
   if coder.target('mex')
       % Insert call to getValue into generated code
       value = int32(0);

       % Insert a call to getValue with 'handle' as argument
       value = coder.ceval('getValue', handle);

       coder.updateBuildInfo('addSourceFiles','wrapper.cpp');
       coder.updateBuildInfo('addSourceFiles', 'library_source.cpp');
       coder.cinclude('wrapper.h');
   else
       value = ExampleClassGetValue_mex(handle);
   end
   end

**Wrapper functions**:

.. code-block:: cpp

   // File: wrapper.h
   #include <cstdint>
   uint64_t createExampleClass();
   int getValue(uint64_t handle);

   // File: wrapper.cpp
   #include "library_header.h"
   uint64_t createExampleClass() {
       return reinterpret_cast<uint64_t>(new ExampleClass);
   }

   int getValue(uint64_t handle) {
       return reinterpret_cast<ExampleClass*>(handle)->getValue();
   }

.. _coder_generate_mex_example:

Now we have to configure code generation.

.. code-block:: matlab

   % File: generate_mex.m
   cfg = coder.config('mex');
   % We want the library to be built as well
   cfg.GenCodeOnly = false;

   % Generate C++
   cfg.TargetLang = 'C++';

   % Call code generation with this configuration for CreateExampleClass
   codegen('-config', cfg, ...
       ...% Set the entry-point function
       'CreateExampleClass',...
       % Set the resulting file name
       '-o', 'CreateExampleClass_mex'
   )

   % Call code generation with this configuration for ExampleClassGetValue
   codegen('-config', cfg, ...
       ...% Set the entry-point function with an argument example
       'ExampleClassGetValue', '-args', uint64(0),...
       ...% Set the resulting file name
       '-o', 'CreateExampleClass_mex'
   )

All that is left to do is to run ``generate_mex.m``. The library can
then be used this way from Matlab:

.. code-block:: matlab

   % File: TestExampleClass.m
   handle = CreateExampleClass;
   value = ExampleClassGetValue(handle);

To destroy the class instance, another function in the same spirit that
handles destruction should be created.


.. _coder_using_mex_api:

Using Mex API
-------------

When using the Mex API, no code generation takes place, we write the
wrapper as well as the Matlab function itself. Using the same
``ExampleClass`` from before, the C++ code would look like this:

.. code-block:: cpp

   // File: example_class_create.cpp
   #include "library_header.h"

   using matlab::mex::ArgumentList;

   // Matlab expects a class named 'MexFunction' that extends
   // matlab::mex::Function
   class MexFunction : public matlab::mex::Function
   {
   public:
       // It has to implement the call operator, ()
       void operator()(ArgumentList outputs, ArgumentList inputs)
       {
           // Object for creating Matlab-understood types
           matlab::data::ArrayFactory factory;

           // Create a new instance of the ExampleClass
           ExampleClass* instance = new ExampleClass;

           // And then create a generic 'scalar' with createScalar,
           // returning it to Matlab in the first output argument
           outputs[0] = factory.createScalar(
               reinterpret_cast<uint64_t>(instance)
           );
       }
   };

   // File: example_class_getvalue.cpp
   #include "library_header.h"

   using matlab::mex::ArgumentList;
   class MexFunction : public matlab::mex::Function
   {
   public:
       void operator()(ArgumentList outputs, ArgumentList inputs)
       {
           // Check if the number of inputs is correct
           if (inputs.size() != 1) {
               // Get a pointer to the Matlab runtime
               std::shared_ptr<matlab::engine::MATLABEngine> matlabPtr = getEngine();

               // And call the 'error' function, with an error id and 
               // a list of format arguments (printf-like)
               matlabPtr->feval(u"error", 0,
                   std::vector<matlab::data::Array>({
                       factory.createScalar("Wrong number of inputs: %d"),
                       factory.createScalar(inputs.size())}));

           }

           matlab::data::ArrayFactory factory;

           // Get the handle as in uint64_t from the generic inputs
           // (Probably implemented with a conversion operator)
           uint64_t handle = inputs[0][0];

           // Convert it to our class instance
           ExampleClass* instance = reinterpret_cast<ExampleClass*>(handle);

           // As before, return a value
           outputs[0] = factory.createScalar(
               instance->getValue()
           );
       }
   };

These functions can now be built using the ``mex`` command.

.. code-block:: matlab

   % File: build_mex.m
   mex example_class_create.cpp -output ExampleClassCreate_mex
   mex example_class_getvalue.cpp -output ExampleClassGetValue_mex

These functions can be called directly, or we can create a Matlab class
for convenience.

.. code-block:: matlab

   % File: ExampleClass.m
   classdef ExampleClass

   properties (SetAccess=private)
       % uint64 scalar modifyable only from within the class
       handle(1,1) uint64
   end

   methods
       function obj = ExampleClass()
           obj.handle = ExampleClassCreate_mex()
       end

       function value = get(obj) {
           value = ExampleClassGetValue_mex(obj.handle);
       }
   end
   end

   % File: TestExampleClass.m
   exampleClass = ExampleClass;
   value = exampleClass.get;

Calling external libraries in generated code
--------------------------------------------

The final goal of OpenCN is for code to be executed in a native,
embedded C++ environment, so these functions have to work seamlessly for
native code generation as well.

``ExampleClass`` requires little change from the mex codegen example.

.. code-block:: matlab

   % File: CreateExampleClass.m
   function handle = CreateExampleClass()
   if coder.target('mex')
       % ...
   elseif coder.target('rtw')
       % If we are generating code for embedded

       handle = uint64(0);

       % insert a call to createExampleClass into the generated code
       handle = coder.ceval('createExampleClass');

       % Include the wrapper header file
       % This will be inserted at the beginning of the generated code
       coder.cinclude('wrapper.h');
   else
       % If not, just call the mexed library
       handle = CreateExampleClass_mex()
   end
   end

   % File: ExampleClassGetValue.m
   function value = ExampleClassGetValue(handle)
   if coder.target('mex')
       % ...
   elseif coder.target('rtw')
       % If generating for embedded
       value = int32(0);

       % Insert a call to getValue with 'handle' as argument
       value = coder.ceval('getValue', handle);
       coder.cinclude('wrapper.h');
   else
       value = ExampleClassGetValue_mex(handle);
   end
   end

Notice that there are no source files or library links added with
updateBuildInfo; this is the job of the native build system.

.. _coder_generate_c_example:

Let’s configure code generation. More parameters than needed for this
example are introduced here.

.. code-block:: matlab

   % File: generate_c.m
   % This time we want an embedded library, without Matlab dependencies
   cfg = coder.config('lib', 'ecoder', true);

   % Some code formatting, we need to read it sometimes
   cfg.IndentSize = 4;
   cfg.ColumnLimit = 100;

   % Variables will have the same name in the generated code as in Matlab
   % This is for easier debugging
   cfg.PreserveVariableNames = 'All';

   % This time we _only_ want code generation, without compilation by Matlab
   cfg.GenCodeOnly = true;

   % Generate all code into a single source file, easier to integrate into
   % the native build system
   cfg.FilePartitionMethod = 'SingleFile';

   % Choose language and standard
   cfg.TargetLang = 'C++';
   cfg.TargetLangStandard = 'C++11 (ISO)';

   % Choose a target device architecture
   cfg.HardwareImplementation.TargetHWDeviceType = 'Intel->x86-64 (Linux 64)';
   cfg.HardwareImplementation.ProdHWDeviceType = 'Intel->x86-64 (Linux 64)';

   % Enable Intel SSE extensions
   cfg.CodeReplacementLibrary = 'Intel SSE (Linux)';

   % Put all generated code into the namespace 'ocn', this reduces the
   % potential for name conflicts and is overall cleaner
   cfg.CppNamespace = 'ocn';

   % We do not need to generate an example, nor a Makefile
   cfg.GenerateExampleMain = 'DoNotGenerate';
   cfg.GenerateMakefile = false;

   % Allow variables to change their size, this makes it much more flexible
   % when matrices need to change their size depending on some parameters
   cfg.EnableVariableSizing = true;

   codegen('-config', cfg,...
       ...% Set output directory
       '-d', 'generate_example', ...
       ...% Add first function to generation
       'ExampleClassCreate',...
       ...% Add second function with example value
       'ExampleClassGetValue', '-args', uint64(0)
   );

The first function in the codegen list will define the generated
filename. This means that after running ``generate_c.m``,
``generate_example`` will contain ``ExampleClassCreate.cpp`` and
``ExampleClassCreate.h``.

The coder will generate all the needed auxiliary functions, but only the
ones specified in the codegen list will be directly callable.

An example native code using this would look like this

.. code-block:: cpp

   // File: TestExampleClass.cpp
   #include "generate_example/ExampleClassCreate.h"
   int main() {
       uint64_t handle = ocn::ExampleClassCreate();
       int value = ocn::ExampleClassGetValue(handle);
       return 0;
   }

Now we just need to build it, using CMake for example.

.. code-block:: cmake

   # File: CMakeLists.txt
   project(example)
   add_executable(example 
       TestExampleClass.cpp
       library_source.cpp
       generate_example/ExampleClassCreate.cpp
   )

This may seem like a complicated way to just call ``new ExampleClass``
and then using it directly, but remember, the entry point functions may
be very complicated and call wrapped libraries themselves. They also
have to work in both the Mex-ed environement and the native one, while
conserving the same Matlab-level interface for easy testing and
integration.


Wrappers in OpenCN
------------------

After this introduction to how code generation works, we can now look at
the different wrappers that exist in OpenCN, how they are implemented,
and introduce some more details that are needed to make them work.

.. include:: /components/coder/queue.rst
.. include:: /components/coder/bspline.rst
.. include:: /components/coder/rs274.rst
.. include:: /components/coder/clp.rst

