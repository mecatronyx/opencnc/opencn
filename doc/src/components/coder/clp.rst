
Lib CLP (Simplex solver)
^^^^^^^^^^^^^^^^^^^^^^^^
The external library COIN-OR is used to solve the linear programming optimization problem.
The solver is called in Matlab with the function ``c_simplex.m``. 
The specific wrapper for the c++ files to the mex is ``cpp_simplex.cpp``. 
The Doxygen documentation for COIN-OR is available `here <https://coin-or.github.io/CoinUtils/Doxygen/>`_.