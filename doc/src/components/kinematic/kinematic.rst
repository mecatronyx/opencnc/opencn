.. _kinematic:

=========
Kinematic
=========

.. toctree::
   :maxdepth: 5
   :hidden:

   kinematic_xyzbc

The support of the kinematic, in *OpenCN* framework, is divided in two parts:

* **Basic**: Computation of the world (piece / tasks space) / joint (joint space) coordinates (``forward`` and ``inverse``
  functions)
* **Advanced**: Optimization (``jacobian`` functions)

The ``kinematic`` component only provides ``forward`` and ``inverse`` functions.
The advanced kinematic support is directly implemented in specific components like
``feedopt``.

.. warning::
	A specific kinematic should be selected specifically for each Hal component. 
	The inifile should be changed with care depending on the axes to used. 

This component can be loaded like this:

.. code-block:: shell

	halcmd load kinematic type=<TYPE> params=<FILE WITH PARAMS> [-p]

where

* (optional) type: The type of kinematic. If it is omitted, the trivial kinematic
  is selected
* (optional) params: A file with the wanted parameters. If it is omitted, all
  parameters are set to 0.
* (optional) -p: if set, HAL PINs and the HAL function are created.


OpenCN supports the following kinematic types:

* Trivial
* Core XY
* :ref:`XYZBC <kinematic_xyzbc>`

********
HAL PINs
********

The PINs are only available is the `-p` parameter is passed when the component is
loaded.

	=====================  =========  =========  =====================
	Name                   Type       Direction  Description
	=====================  =========  =========  =====================
	kinematic.world-in-x   HAL_FLOAT  HAL_IN     World input, axis X
	kinematic.world-in-y   HAL_FLOAT  HAL_IN     World input, axis Y
	kinematic.world-in-z   HAL_FLOAT  HAL_IN     World input, axis Z
	kinematic.world-in-a   HAL_FLOAT  HAL_IN     World input, axis A
	kinematic.world-in-b   HAL_FLOAT  HAL_IN     World input, axis B
	kinematic.world-in-c   HAL_FLOAT  HAL_IN     World input, axis C
	kinematic.world-in-u   HAL_FLOAT  HAL_IN     World input, axis U
	kinematic.world-in-v   HAL_FLOAT  HAL_IN     World input, axis V
	kinematic.world-in-w   HAL_FLOAT  HAL_IN     World input, axis W
	kinematic.world-out-x  HAL_FLOAT  HAL_OUT    World output, axis X
	kinematic.world-out-y  HAL_FLOAT  HAL_OUT    World output, axis Y
	kinematic.world-out-z  HAL_FLOAT  HAL_OUT    World output, axis Z
	kinematic.world-out-a  HAL_FLOAT  HAL_OUT    World output, axis A
	kinematic.world-out-b  HAL_FLOAT  HAL_OUT    World output, axis B
	kinematic.world-out-c  HAL_FLOAT  HAL_OUT    World output, axis C
	kinematic.world-out-u  HAL_FLOAT  HAL_OUT    World output, axis U
	kinematic.world-out-v  HAL_FLOAT  HAL_OUT    World output, axis V
	kinematic.world-out-w  HAL_FLOAT  HAL_OUT    World output, axis W
	kinematic.joint-out-x  HAL_FLOAT  HAL_OUT    Joint output, axis X
	kinematic.joint-out-y  HAL_FLOAT  HAL_OUT    Joint output, axis Y
	kinematic.joint-out-z  HAL_FLOAT  HAL_OUT    Joint output, axis Z
	kinematic.joint-out-a  HAL_FLOAT  HAL_OUT    Joint output, axis A
	kinematic.joint-out-b  HAL_FLOAT  HAL_OUT    Joint output, axis B
	kinematic.joint-out-c  HAL_FLOAT  HAL_OUT    Joint output, axis C
	kinematic.joint-out-u  HAL_FLOAT  HAL_OUT    Joint output, axis U
	kinematic.joint-out-v  HAL_FLOAT  HAL_OUT    Joint output, axis V
	kinematic.joint-out-w  HAL_FLOAT  HAL_OUT    Joint output, axis W
	kinematic.joint-in-x   HAL_FLOAT  HAL_IN     Joint input, axis X
	kinematic.joint-in-y   HAL_FLOAT  HAL_IN     Joint input, axis Y
	kinematic.joint-in-z   HAL_FLOAT  HAL_IN     Joint input, axis Z
	kinematic.joint-in-a   HAL_FLOAT  HAL_IN     Joint input, axis A
	kinematic.joint-in-b   HAL_FLOAT  HAL_IN     Joint input, axis B
	kinematic.joint-in-c   HAL_FLOAT  HAL_IN     Joint input, axis C
	kinematic.joint-in-u   HAL_FLOAT  HAL_IN     Joint input, axis U
	kinematic.joint-in-v   HAL_FLOAT  HAL_IN     Joint input, axis V
	kinematic.joint-in-w   HAL_FLOAT  HAL_IN     Joint input, axis W
	=====================  =========  =========  =====================

The relationship between these PINs sets are the following:

* Forward kinematic: Joint-in --> World-out
* Inverse kinematic: World-in --> Joint-out

**********
HAL Params
**********


	=====================  =========  =========  =====================
	Name                   Type       Direction  Description
	=====================  =========  =========  =====================
	kinematic.type         HAL_U32    HAL_RO     Selected kinematic
	kinematic.param0 to    HAL_FLOAT  HAL_RW     Parameters value
	kinematic.param31
	=====================  =========  =========  =====================

***************
Parameters file
***************

The parameters are defined in a file, the parameter file. This file uses the *ini*
format define by Microsoft.
It has one section called ``[Parameters]`` and one line per parameter. Only the
required parameter has to be added. The other parameters are set to 0.
The parameters format is the following, where N can be 0 to 31: ``paramN = value``
It is possible to add comments with `;`  or `#` characters.
Below is an example of a parameter ini file:

.. note::

	OpenCN as based his definition of the kinematic parameter over `ISG-kernel work
	<https://www.isg-stuttgart.de/fileadmin/kernel/kernel-html/en-GB/index.html?p=292629387>`_.
	Please note that most of the transformation is not implemented in OpenCN for now.

.. code-block:: ini

	[Parameters]

	; tool length
	param0 = 34.21

	# parameter 5
	param5 = 23.123

In matlab, the parameter are set using the following function:

.. code-block:: C

	int kin_set_params( double *p )

It takes, as argument, an array of doubles. The size of the array is expected to
be 32 (one per parameter).

**************
Implementation
**************

Basic kinematic
===============

The support of the basic kinematic functions is implemented directly in C and
located in the kernel.

Matlab environment
------------------

Matlab has to access inverse and forward transformation implemented in the basic
kinematic interface. In order to avoid code duplication, these functionalities are
included in the advanced kinematic interface (see details below). This kinematic
interface is *mexed* (compiled C/C++ files callable from Matlab) so they can be
used directly from Matlab code. The additional functionalities required in Matlab
development flow are to set and to get the kinematic type, its parameter
values and the parameters of the tool.

OpenCN environment
------------------

AS the basic kinematic is provided as an ``HAL`` component, it provides HALPINs
and function (see description above). It also provides an APIs (standard functions)
which can be called by any kernel code, RT or not-RT (see below for the details on
this API).
The basic kinematic function can also be called from the ``user-space`` via a
``kinematic`` library. This library call the kernel functions via an ``IOCTL``
The functions prototype are the same as the one in the kernel.

The following picture shows an overview of the OpenCN kinematic support in OpenCN
environment:

.. figure:: pictures/kinematic.png
  :name: _fig-kinematic
  :alt: Kinematic
  :align: center

  Kinematic architecture overview


Files organization
^^^^^^^^^^^^^^^^^^

* Path: ``<OPENCN HOME>/agency/linux/opencn/components/kinematic``
* Files
	* ``kinematic_priv.h``: - The different kinematics header
	* ``kinematic.c``:- Main/default kinematic file
	* ``kinematic_<Type>.c``: - Specific kinematic file. In case more the specific
	  kinematic is implemented in more than one file, these files should be placed
	  in a folder named kinematic_<Type>
	* ``kinematic_hal.c`` - HAL specific functions. It is only used in OpenCN flow
	  (not in Matlab environment)
	* In uapi include folder, ``kinematic.h`` - It contains the definitions to be
	  used in the components or codes (kernel only) which need kinematic computation.

Interface / API
^^^^^^^^^^^^^^^

.. code-block:: C

	typedef enum {
	    KIN_TYPE_TRIVIAL = 0,
	    KIN_TYPE_COREXY,
	    KIN_TYPE_XYZBC,
	} kin_type_t;

	// == Functions to be used by Matlab only ==
	int kin_set_type( kin_type_t type );
	kin_type_t kin_get_type( void );
	int kin_set_params( const double *p );
	int kin_get_params( double *p );

	// == Functions to use in Matlab and OpenCN ==

	// kinematic forward
	void kin_forward( double *world, const double *joint );

	// kinematic inverse
	void kin_inverse( const double *world, double *joint );

The :func:`kin_forward` and :func:`kin_inverse()`  functions are generic functions
which are linked with the wanted kinematic when the component is loaded (OpenCN flow)
or  by a call to :func:`kin_set_type` function (Matlab flow).
