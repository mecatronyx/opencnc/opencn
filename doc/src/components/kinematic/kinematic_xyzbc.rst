.. _kinematic_xyzbc:

===============
kinematic XYZBC
===============

.. note::
	The kinematic names is not well chosen. Only 5 axes (X Y Z B C) with TABLE/TABLE topologie is available.
	The following bloc has not being well tested. Use it without any warranty. 


.. |pic1| image:: pictures/kinematic_xyzbc_tt.png
	:scale: 100%
	
.. |pic2| image:: pictures/kinematic_xyzbc_tt_with_offset.png
	:scale: 100%

+---------------------------------------------------+---------------------------------------------------+
| |pic1|                                            | |pic2|                                            |
+===================================================+===================================================+
| Fig.1 Kinematics XYZBC TABLE/TABLE without offset | Fig.2 Kinematics XYZBC TABLE/TABLE with offset    |
+---------------------------------------------------+---------------------------------------------------+

.. note::
	The tool offset parameters have to be defined using the machine coordinate system. Once the G code is read, the tool offset is added to the curve path.
	Thus, the functions `set_tool_length` or `set_tool_offset` need to add a negative sign to compensate this addition.


List of the parameters used with this kinematic:

.. note::
	The folowing table present the parameters used in E2C for machining with the micro5 machine and thus the parametrisation is based on HSMWorks postprocessor.

+-----------+------------------------+---------------------+---------------------+
| Parameter | Description            | Values Micro5 (XSR) | Values Micro5 (HGS) |
+===========+========================+=====================+=====================+
| param0    | Machine X offset       | 0                   | 0                   |
+-----------+------------------------+---------------------+---------------------+
| param1    | Machine Y offset       | 0                   | 0                   |
+-----------+------------------------+---------------------+---------------------+
| param2    | Machine Z offset       | 0                   | 0                   |
+-----------+------------------------+---------------------+---------------------+
| param3    | Tool X offset          | 0                   | 0                   |
+-----------+------------------------+---------------------+---------------------+
| param4    | Tool Y offset          | 0                   | 0                   |
+-----------+------------------------+---------------------+---------------------+
| param5    | Tool Z offset          | 0                   | 0                   |
+-----------+------------------------+---------------------+---------------------+
| param6    | Piece X offset         | 0.07155             | 0                   |
+-----------+------------------------+---------------------+---------------------+
| param7    | Piece Y offset         | -0.1645             | 0                   |
+-----------+------------------------+---------------------+---------------------+
| param8    | Piece Z offset         | 1.6                 | -68.802             |
+-----------+------------------------+---------------------+---------------------+
| param9    | B axis Offset X offset | 0                   | -60.8               |
+-----------+------------------------+---------------------+---------------------+
| param10   | B axis Offset Y offset | 0                   | -6.6                |
+-----------+------------------------+---------------------+---------------------+
| param11   | B axis Offset Z offset | 0                   | 0                   |
+-----------+------------------------+---------------------+---------------------+
| param12   | C axis Offset X offset | 0                   | 0                   |
+-----------+------------------------+---------------------+---------------------+
| param13   | C axis Offset Y offset | 0                   | 0                   |
+-----------+------------------------+---------------------+---------------------+
| param14   | C axis Offset Z offset | 0                   | 0                   |
+-----------+------------------------+---------------------+---------------------+
