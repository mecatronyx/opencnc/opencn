.. _epos4:

###########
Maxon epos4
###########

The EPOS4 drive is a product offered by Maxon. It is part of Maxon's EPOS (Easy
Positioning System) family of controllers, and it is designed for controlling
various types of electric motors, including brushed and brushless DC motors, and
stepper motors. It also provides digital and analogic I/O.

It supports the following operating modes:

* Profile Position Mode (PPM),
* Profile Velocity Mode (PVM)
* Homing Mode (HMM)
* Cyclic Synchronous Position Mode (CSP)
* Cyclic Synchronous Velocity Mode (CSV)
* Cyclic Synchronous Torque Mode (CST)

The current implementation does not suppot the digital I/O (Connector X7) and
the analog I/O (Connector X8).

*************
Configuration
*************

Here are the information to define an epos4 slave in a *EtherCAT* topology XML file:

* salve type: EPOS4
* Distributed clock configuration (DC):
    * Assign Active: 0x300
    * Sync0 event: 1x - Use same frequency as EtherCAT
* Support for the init commands (SDO)

Here is an example of *EtherCAT* topology XML file with one epo4 drive as slave:

.. code-block:: xml

    <masters>
        <master idx="0" appTimePeriod="1000000">
            <slave idx="0" type="EPOS4" name="<SLAVE NAME>">
                <initCmds filename="<PATH TO INIT CMDs FILE>"/>
                <dcConf assignActivate="300" sync0Cycle="*1" sync0Shift="0"/>
            </slave>
        </master>
    </masters>

*****
Modes
*****

The ``set-mode-<MODE>`` PINs are used to set the drive in the wanted mode and the
``in-mode-<MODE>`` PINs indicate the actual drive mode. <MODE> can be one of ``inactive``,
``hm``, ``ppm``, ``pvm``, ``csp``, ``csv`` or ``cst``.

Once the drive is in one specific mode, the wanted target can be set with the
``set-target`` PIN. It does not concern the inactive and homing modes.

The actual drive *values* can be read from ``actual-position``, ``actual-velocity`` and
``actual-torque`` PINs.

The ``set-target`` and ``actual-`` PINs can be scaled using the ``pos-scale``,
``velocity-scale`` and ``torque-scale`` HAL parameters.

A quick-stop can be requested at any time by rising the ``quick-stop`` PIN. The
drive falls in quick-stop state and clear the PIN.

.. note::

    It is possible to switch for any mode to any another mode when the drive is idle.

.. warning::

    The drive is always idle in csp, csv & cst modes. In these modes the drives
    is controlled by the control device. It is expected that this control device
    knows what it is doing

Fault
=====

The ``in-fault`` PIN indicates if the drive is in fault. The error code can be read
from ``error-code`` PIN. The table below gives the information about the significance
of this value:

===  ====================================
Bit  Description
===  ====================================
 7   Motion error
 6   reserved (always 0)
 5   Device profile-specific
 4   communication error
 3   Temperature error
 2   Voltage error
 1   Current error
 0   Generic error
===  ====================================

The fault can be clear with ``fault-reset`` PIN.

Homing mode
===========

In the Homing mode, the procedure can be started and stopped with the ``hm.start``
and ``hm.stop`` PINs. The ``homed`` PIN indicates if the drive is homed or not.

Profile Position Mode
=====================

To start the movement in PPM, the ``ppm.start`` PIN should be set and it can be
stopped with ``ppm.stop`` PINs.

This mode has 3 parameters to configure it behavior:

* ``ppm.endless-movement``: when set, the movement will never ends.
* ``ppm.immediate-change``: When a movement is ongoing and a start to a new position
  is requested (with ``ppm.start`` PIN), it indicates if the actual procedure complete
  before starting the next positioning or abort the current procedure and start
  immediately the new procedure.
* ``ppm.relative-pos``: indicate if the target position is relative (= 1) or
  absolute (= 0)

Modulo
======

For the positioning modes (PPM & CSP), the drive can be configured to treat the
target position using a modulo. This allows the actual position to vary between 0
and the specified modulo value.

To set the modulo value, use the ``modulo`` HAL parameter to configure the desired
value.

****
PINs
****

.. note::

    The <prefix> is ``lcec.0.<SLAVE NAME>`` where ``<SLAVE NAME>`` correspond to
    the ``<SLAVE NAME>`` in the EtherCAT topology XML file

=============================  =========  =========  ====================================
Name                           Type       Direction  Description
=============================  =========  =========  ====================================
<prefix>.set-mode-inactive     HAL_BIT    HAL_IN     Set the drive in Inactive mode
<prefix>.set-mode-hm           HAL_BIT    HAL_IN     Set the drive in Homing mode
<prefix>.set-mode-ppm          HAL_BIT    HAL_IN     Set the drive in PPM mode
<prefix>.set-mode-pvm          HAL_BIT    HAL_IN     Set the drive in PVM mode
<prefix>.set-mode-csp          HAL_BIT    HAL_IN     Set the drive in CSP mode
<prefix>.set-mode-csv          HAL_BIT    HAL_IN     Set the drive in CSV mode
<prefix>.set-mode-cst          HAL_BIT    HAL_IN     Set the drive in CST mode
<prefix>.in-mode-inactive      HAL_BIT    HAL_OUT    Drive is in Inactive mode
<prefix>.in-mode-hm            HAL_BIT    HAL_OUT    Drive is in Homing mode
<prefix>.in-mode-ppm           HAL_BIT    HAL_OUT    Drive is in PPM mode
<prefix>.in-mode-pvm           HAL_BIT    HAL_OUT    Drive is in PVM mode
<prefix>.in-mode-csp           HAL_BIT    HAL_OUT    Drive is in CSP mode
<prefix>.in-mode-csv           HAL_BIT    HAL_OUT    Drive is in CSV mode
<prefix>.in-mode-cst           HAL_BIT    HAL_OUT    Drive is in CST mode
<prefix>.quick-stop            HAL_BIT    HAL_IN     Active Quick-Stop
<prefix>.set-target            HAL_FLOAT  HAL_IN     Set target position/velocity or
                                                     torque, depending on the actual
                                                     drive mode
<prefix>.actual-position       HAL_FLOAT  HAL_OUT    Actual drive position, in [inc] with
                                                     ``pos-scale`` param set to 1.0
<prefix>.actual-velocity       HAL_FLOAT  HAL_OUT    Actual drive velocity, in [rpm] with
                                                     ``velocity-scale`` param set to 1.0
<prefix>.actual-torque         HAL_FLOAT  HAL_OUT    Actual drive torque, in [uNm] with
                                                     ``torque-scale`` param set to 1.0
<prefix>.hm.start              HAL_BIT    HAL_IN     Start homing procedure, if the drive
                                                     is in homing mode.
<prefix>.hm.stop               HAL_BIT    HAL_IN     Stop homing procedure, if the drive
                                                     is in homing mode.
<prefix>.homed                 HAL_BIT    HAL_OUT    True if the axis is homed
<prefix>.in-fault              HAL_BIT    HAL_OUT    The drive is in Fault
<prefix>.fault-reset           HAL_BIT    HAL_IN     Reset faults
<prefix>.error-code            HAL_BIT    HAL_OUT    Last returned code
<prefix>.ppm.start             HAL_BIT    HAL_IN     Start PMM procedure
<prefix>.ppm.stop              HAL_BIT    HAL_IN     Stop PPM procedure
<prefix>.idle                  HAL_BIT    HAL_IN     The drive is idle. It is possible
                                                     to change the mode of operation
=============================  =========  =========  ====================================

**************
HAL parameters
**************

=============================  =========  =========  ===========================================
Name                           Type       Direction  Description
=============================  =========  =========  ===========================================
<prefix>.pos-scale             HAL_FLOAT   HAL_RW    Scale parameter for position target
<prefix>.velocity-scale        HAL_FLOAT   HAL_RW    Scale parameter for velocity target
<prefix>.torque-scale          HAL_FLOAT   HAL_RW    Scale parameter for torque target
<prefix>.modulo                HAL_FLOAT   HAL_RW    Target position modulo, only in
                                                     PPM and CSP mode.
<prefix>.ppm.immediate-change  HAL_BIT     HAL_RW    In PPM mode, immediately apply position
                                                     change
<prefix>.ppm.relative-pos      HAL_BIT     HAL_RW    In PPM mode, true: target position is
                                                     relative, absolute otherwise
<prefix>.ppm.endless-movement  HAL_BIT     HAL_RW    Set the PPM mode for an endless movement
=============================  =========  =========  ===========================================

******************
Usage example
******************
A simple architecture is provided in the directory ``epos4/epos4.hal``.
This architecture is composed as the following figure:

.. image:: ../_pictures/epos4_usage_example.drawio.png
    :width: 50%
    :align: center

It is composed of:

* A ``lcec`` component to communicate with the epos4 througt the EtherCAT bus
* A ``simple position generator`` component to generate a simple position
  trajectory
* A ``mux`` component to select the target position between simple position
  generator and an external component

The external pin of the ``mux`` component is used to control the modes Profile Position Mode (PPM) and Profile Velocity Mode (PVM).

To initialize the HAL components, you can use the command:

.. code-block:: bash

    halcmd -f epos4/epos4.hal

.. warning::
    Don't use the command twice, it will create duplicate components and crash the RaspBerry Pi.

***************
Validation test
***************

A validation test is provided in the directory ``epos4/validation``.

This test works with the basic architecture described above.

You can run the test with the following command:

.. code-block:: bash

    cd epos4/validation
    ./validation.sh

To run the test, you need to initialize all the HAL components. You can do it with the .hal file provided or with the option ``-init`` of the validation test.

For more information about the validation test, please refer to the README file in the validation folder.

.. warning::

    The validation test is using values (for the position, velocity and torque scale, the speed parameters of the simple position generator, etc.)
    that are adapted to the motor used in the test. If you want to use the validation test with another motor, you need to adapt the values.



