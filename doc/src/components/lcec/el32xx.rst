.. _el32xx:

###############
Beckhoff EL3202
###############

The EL3202 EtherCAT slave is a 2-channel analog input to read temperature from
PT100 sensor.

The returned value is in [°C]. In debug mode, the returned temperature is set to
22.3 °C.

.. note::

	The current implementation only supports the read-back of the temperature value.

*********
PINs list
*********

================= ========= =========
Name              Type      Direction
================= ========= =========
chanX.temperature HAL_FLOAT HAL_OUT
================= ========= =========


The official documentation can be found  `here <https://download.beckhoff.com/download/Document/io/ethercat-terminals/EL32xxen.pdf>`_
