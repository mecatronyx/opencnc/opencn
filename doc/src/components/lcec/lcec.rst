.. _lcec:

================================
LinuxCNC Ethercat Control (LCEC)
================================

The *lcec* component provides an interface between the EtherCAT master and OpenCN
components. It is a part of the project `linuxcnc-ethercat <https://github.com/sittner/linuxcnc-ethercat>`_
made by Sascha Ittner.

Overview
--------

lcec enables EtherCAT Distributed Clock feature (DC) in mode B (a slave is master
of the clock).

It does not export any function. Instead, it creates HAL threads, one per master.
These threads:

* Implement the EtherCAT DC in mode B
* Provide HAL thread features: adding/removing functions, start/stop thread execution
* The thread period depends of :mod:appTimePeriod master parameter sets in the
  *EtherCAT* configuration file (see :ref:`etherCAt configuration file`)
* Are named :mod:lcec_thread.N where N is the master index in the EtherCAT bus
  topology.

.. note::

 The current version of lcec only supports 1 master in the EtherCAT topology.

.. _etherCAt configuration file:

*EtherCAT* Configuration file
-----------------------------

The *EtherCAT* configuration file is a XML file that describes the *EtherCAT* bus
topology. The supported tags are:

* **masters**: top level tag
* **master**: master description
* **slave**: slave description
* **dcConf**: Distributed clock description
* **initCmds**: initial commands sent to initialize a slave (CoE or IDN commands)

The following paragraphs provide details about the parameters supported by each
tag.

Master tag parameters
^^^^^^^^^^^^^^^^^^^^^

* **idx**: Index of the master in EtherCAT topology
* **appTimePeriod**: Period of the EtherCAT cycle, in ns
* **name**: (optional) Name of the master. If not set, the master name is set to
  the ‘idx’ value


Salve tag parameters
^^^^^^^^^^^^^^^^^^^^

* **type**: Type of the slave
* **idx**: Index of the slave in EtherCAT topology
* **name**: (optional) name of the slave. If not set, the slave name is set to the
  ‘idx’ value

dcConfig tag parameters
^^^^^^^^^^^^^^^^^^^^^^^
* **assignActivate**: AssignActivate  word. It is vendor-specific and can be taken
  from the XML device description file.
* **sync0Cycle**: (optional) SYNC0 cycle time, in ns
* **sync0Shift**: (optional) SYNC0 shift time, in ns
* **sync1Cycle**: (optional) SYNC1 cycle time, in ns
* **sync1Shift**: (optional) SYNC1 shift time, in ns

initCmds tag parameters
^^^^^^^^^^^^^^^^^^^^^^^

* **filename**: name of the file with the CoE or SoE commands

EtherCAT configuration file example
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: xml

   <masters>
      <master idx="0" appTimePeriod="100000">
         <slave idx="0" type="EK1100"/>
         <slave idx="1" type="EL1252"/>
         <slave idx="2" type="EL2252">
            <dcConf assignActivate="300" sync0Cycle="*1" sync0Shift="50000"/>
         </slave>
      </master>
   </masters>

lcec PINs
---------

``lcec`` define the following PINs for the *EtherCAT* master and each slaves will
also have the PINs defined below.

**master PINs**

    ================= ======= =========   ==================================================================================
    Name              Type    Direction   Description
    ================= ======= =========   ==================================================================================
    slaves-responding HAL_U32 HAL_OUT     
    state-init        HAL_BIT HAL_OUT     
    state-preop       HAL_BIT HAL_OUT     
    state-safeop      HAL_BIT HAL_OUT     
    state-op          HAL_BIT HAL_OUT     
    link-up           HAL_BIT HAL_OUT     
    all-op            HAL_BIT HAL_OUT     
    process-time      HAL_U32 HAL_OUT     Duration to process every HAL function attached to lcec in [ns] 
    loop-time         HAL_U32 HAL_OUT     Duration of the complete lcec Real Time (RT) thread in [ns]. 
                                          That is, process of attached HAL function (``process-time`` pin), 
                                          read and send of EtherCAT frame, and sleep time until next loop.
    current-time      HAL_U32 HAL_OUT     Current time at the start of the lcec Real Time (RT) Thread in [ns]
    ================= ======= =========   ==================================================================================

**Default slaves PINs**

    ============ ======= =========
    Name         Type    direction
    ============ ======= =========
    online       HAL_BIT HAL_OUT
    operational  HAL_BIT HAL_OUT
    state-init   HAL_BIT HAL_OUT
    state-preop  HAL_BIT HAL_OUT
    state-safeop HAL_BIT HAL_OUT
    state-op     HAL_BIT HAL_OUT
    ============ ======= =========


Debug mode
----------

The lcec debug mode provides a standalone mode: In this mode, lcec does not interface
with the *EtherCAT* master and so there are no EtherCAT activities at all. It only
creates HAL pins and parameters and call the init(),read() and write() functions
of the slaves.

It means that it is possible to test/debug the system without *EtherCAT* interface
(no slaves needed). The behavior of the pins/parameters exported are slave-dependent.

Usage
-----

the lcec component takes 2 parameters when loaded:

* **Cfg**: XML file EtherCAT providing the topology of the bus. The content of this
  file is described in :ref:`etherCAt configuration file` paragraph.
* **Debug** (-d): enable the debug mode.

Example:

.. code-block:: shell

   $ halcmd load lcec cfg=<ETHECAT_TOPOLOGY> [-d]

Supported slaves
----------------

.. toctree::
   :maxdepth: 0
   :hidden:

   tsd80e
   epos4
   cteu
   el32xx

* :ref:`Beckhoff Digital Inputs<Beckhoff Digital Inputs>`
* :ref:`Beckhoff Digital Ouptus<Beckhoff Digital Outputs>`
* :ref:`Beckhoff Analog Inputs<Beckhoff Analog Inputs>`
* :ref:`Beckhoff AX5X00`
* :ref:`Beckhoff EL7411`
* :ref:`Beckhoff el3202<el32xx>`
* :ref:`Triamec Tsd80e<tsd80e>`
* :ref:`Maxon epos4<epos4>`
* :ref:`Festo cteu<cteu>`
* :ref:`OpenCN Companion<lcec companion>`

.. _Beckhoff Digital Inputs:

Beckhoff Inputs EL1xxx
^^^^^^^^^^^^^^^^^^^^^^

Beckhoff proposes a set of different Digital Inputs EtherCAT slaves. LCEC supports
the following one. They all behave the same way. The only difference is the
number of inputs.

========== ============ ========== ============
Slave      Input number Slave      Input number
========== ============ ========== ============
**EL1002**   2          **EL1094**   4
**EL1004**   4          **EL1098**   8
**EL1008**   8          **EL1104**   4
**EL1012**   2          **EL1114**   4
**EL1014**   4          **EL1124**   4
**EL1018**   8          **EL1134**   4
**EL1024**   4          **EL1144**   4
**EL1034**   4          **EL1808**   8
**EL1084**   4          **EL1809**   16
**EL1088**   8          **EL1819**   16
========== ============ ========== ============

PINs list
"""""""""

========= ======= =========
Name      Type    direction
========= ======= =========
din-X     HAL_BIT HAL_OUT
din-X-not HAL_BIT HAL_OUT
========= ======= =========

PINs deubg list
"""""""""""""""

These PINs are only available when the ``lcec`` is loaded in ``debug`` mode. It
is used to set a value to the Input PINs.

=========== ======= =========
Name        Type    direction
=========== ======= =========
debug_din-X HAL_BIT HAL_IN
=========== ======= =========

.. _Beckhoff Digital Outputs:

Beckhoff Outputs EL2xxx
^^^^^^^^^^^^^^^^^^^^^^^

Beckhoff proposes a set of different Digital Ouputs EtherCAT slaves. LCEC supports
the following one. They all behave the same way. The only difference is the
number of inputs.

========== ============== ========== ==============
Slave      Outputs number Slave      Outputs number
========== ============== ========== ==============
**EL2002**  2             **EL2088**  8
**EL2004**  4             **EL2124**  4
**EL2008**  8             **EL2612**  2
**EL2022**  2             **EL2622**  2
**EL2024**  4             **EL2634**  4
**EL2032**  2             **EL2798**  8
**EL2034**  4             **EL2808**  8
**EL2042**  2             **EL2809**  16
**EP2028**  8             **EP2008**  8
**EL2084**  4             **EP2809**  16
========== ============== ========== ==============


PINs list
"""""""""

========= ======= =========
Name      Type    direction
========= ======= =========
dout-X    HAL_BIT HAL_IN
========= ======= =========

PARAMs list
"""""""""""

============== ======= ======
Name           Type    mode
============== ======= ======
dout-%d-invert HAL_BIT HAL_RW
============== ======= ======

.. _Beckhoff Analog Inputs:

Beckhoff EL3702
^^^^^^^^^^^^^^^

Beckhoff EL3702 is a 2-channel analog input terminal with oversampling.
This terminal is used to sample analog voltage in the range of +/- 10[V].
It stores values in 16 bits integer signed format.
The values are acquired and stored internally in a slave buffer at the rate that can be higher than the EtherCAT frame rate, i.e. oversampling.
There is 1 buffer per channel, and 2 channels per slave.
The oversampling ratio is defined using the ``dcConf`` tag and Sync0/Sync1 entries in the EtherCAT configuration xml file (explained below).

OS (oversampling) ratio is set to 10[-] using the method described above.
In order to change the OS factor, one needs to change the xml file.
For the moment, one ALSO needs to adapt the corresponding C code, i. e. lcec_el3702.c and lcec_el3702.h

PINs list
"""""""""
%s.%s.%s.ain-%d-CycleCount PIN contains a cycle counter for each channel that is incremented with each EtherCAT cycle.
With the OS factor set to 10, a record consists of a 16-bit CycleCounter (overflowing) and 10 16-bit samples per channel.
The CycleCounter in the higher-level control system can be used to ensure data reliability.

%s.%s.%s.ain-%d-X-Volt are output HAL PINs where the sampled analog values are stored.

For each channel:

"ain" in PINs names stands for "analog inputs".
The 1st "%s" is replaced with the lcec module name (EL3702) at PINs creation.
The 2nd "%s" is replaced with the name of the EtherCAT master.
The 3rd "%s" is replaced with the EtherCAT slave name.

The last "%d" in PINs names is replaced with the channel number.

============================  =========   =========
Name                          Type        Direction
============================  =========   =========
%s.%s.%s.ain-%d-CycleCount    HAL_U32     HAL_OUT
%s.%s.%s.ain-%d-0-Volt        HAL_FLOAT   HAL_OUT
%s.%s.%s.ain-%d-1-Volt        HAL_FLOAT   HAL_OUT
%s.%s.%s.ain-%d-2-Volt        HAL_FLOAT   HAL_OUT
%s.%s.%s.ain-%d-3-Volt        HAL_FLOAT   HAL_OUT
%s.%s.%s.ain-%d-4-Volt        HAL_FLOAT   HAL_OUT
%s.%s.%s.ain-%d-5-Volt        HAL_FLOAT   HAL_OUT
%s.%s.%s.ain-%d-6-Volt        HAL_FLOAT   HAL_OUT
%s.%s.%s.ain-%d-7-Volt        HAL_FLOAT   HAL_OUT
%s.%s.%s.ain-%d-8-Volt        HAL_FLOAT   HAL_OUT
%s.%s.%s.ain-%d-9-Volt        HAL_FLOAT   HAL_OUT
============================  =========   =========

In addition, for each channel, only in DEBUG mode, for each channel:

%s.%s.%s.ain-%d-X-Volt-simu-input PINs are used to simulate signal acquisition.
Their values are copied directly on the %s.%s.%s.ain-%d-X-Volt output PINs, as soon as the main thread is started.

=================================   =========   =========
Name                                Type        Direction
=================================   =========   =========
%s.%s.%s.ain-%d-0-Volt-simu-input   HAL_FLOAT   HAL_IN
%s.%s.%s.ain-%d-1-Volt-simu-input   HAL_FLOAT   HAL_IN
%s.%s.%s.ain-%d-2-Volt-simu-input   HAL_FLOAT   HAL_IN
%s.%s.%s.ain-%d-3-Volt-simu-input   HAL_FLOAT   HAL_IN
%s.%s.%s.ain-%d-4-Volt-simu-input   HAL_FLOAT   HAL_IN
%s.%s.%s.ain-%d-5-Volt-simu-input   HAL_FLOAT   HAL_IN
%s.%s.%s.ain-%d-6-Volt-simu-input   HAL_FLOAT   HAL_IN
%s.%s.%s.ain-%d-7-Volt-simu-input   HAL_FLOAT   HAL_IN
%s.%s.%s.ain-%d-8-Volt-simu-input   HAL_FLOAT   HAL_IN
%s.%s.%s.ain-%d-9-Volt-simu-input   HAL_FLOAT   HAL_IN
=================================   =========   =========

Slave definition
""""""""""""""""

Hereafter is an example of EtherCAT bus topology with 3 slaves, stored in the EtherCAT topology XML file:

EK1100: bus coupler, needed to physically interface analog input terminals with the master.
EL3702: two analog input slaves.

* ``dcConf`` tag: configuration of the *Distributed Clock*
* ``appTimePeriod`` entry: The *EtherCAT* cycle time is set to 1[ms] (1000000[ns]).
* ``assignActivate`` entry: set to 0x730 (fixed value).
* ``sync0Cycle`` entry: 100000[ns]. In order to set OS (oversampling) factor to 10, one needs to set ``sync0Cycle`` value to 1/10 of the ``appTimePeriod`` tag value.
  For another oversampling rate, ``sync0Cycle`` has to be changed, according to a desired OS factor.
  The minimum ``sync0Cycle`` value is 1/100 of the ``appTimePeriod``, that corresponds to OS = 100, maximum OS factor possible.

.. note::

    With the current C code implementation, ``sync0Cycle`` entry value has to be 1/10 of the ``appTimePeriod``.
    For another oversampling rate, source C code has to be updated, in addition to the xml file.

* ``sync1Cycle`` entry: 900000[ns]. This value is obtained with this formula: ``appTimePeriod - sync0Cycle``.
* ``sync0Shift`` and ``sync1Shift`` entries should be kept zero.

``EtherCAT period/Sync0 Cycle`` sets the oversampling ratio (OS).
It must be an integer positive value. OS max is 100, according to Bekhoff documentation.

In this example, EtherCAT frequency is set to 1[kHz]. Sampling frequency is set to 10[kHz].

.. code-block:: xml

    <masters>
        <master idx="0" appTimePeriod="1000000">
            <slave idx="0" type="EK1100"/>
            <slave idx="1" type="EL3702" name="AIN1">
                <dcConf assignActivate="730" sync0Cycle="100000" sync0Shift="0" sync1Cycle="900000" sync1Shift="0"/>
            </slave>
            <slave idx="2" type="EL3702" name="AIN2">
                <dcConf assignActivate="730" sync0Cycle="100000" sync0Shift="0" sync1Cycle="900000" sync1Shift="0"/>
            </slave>
        </master>
    </masters>


.. _Beckhoff AX5X00:

Beckhoff AX5X00
^^^^^^^^^^^^^^^

Beckhof AX500 are servo drive with support for one (AX5100) or two (AX5200) axes.

PINs list
"""""""""
======================= ========= =========
Name                    Type      direction
======================= ========= =========
axisX.enable            HAL_BIT   HAL_IN
axisX.enabled           HAL_BIT   HAL_OUT
axisX.fault             HAL_BIT   HAL_OUT
axisX.target-position   HAL_FLOAT HAL_IN
axisX.status            HAL_U32   HAL_OUT
axisX.position-feedback HAL_FLOAT HAL_OUT
======================= ========= =========

PARAMs list
"""""""""""

======================= ========= =======
Name                    Type      mode
======================= ========= =======
axisX.scale             HAL_FLOAT HAL_RW
axisX.pos-resolution    HAL_U32   HAL_RO
======================= ========= =======



Slave definition
""""""""""""""""

The AX5X00 slave definition, in the EtherCAT topology XML, is the following:

* ``slave`` tag: set the ``type``  attribute to one of the 2 options:
    *  ``AX5100`` for an 1 axis
    * ``AX5200`` for 2 axes
* ``initCmds`` tag: SDO startup file with the definition of the IDN. It depends
  on the setup, motor and slave connected to the drive.
* ``dcConf`` tag: configuration of the *Distributed Clock*

The following code snippet is an example of a EtherCAT topology XML with one AX5200
drive connected to the master.

.. code-block:: xml

    <masters>
        <master idx="0" appTimePeriod="1000000">
            <slave idx="0" type="AX5200" name="AX5203">
                <initCmds filename="ax5200_startup.xml"/>
                <dcConf assignActivate="730" sync0Cycle="250000" sync0Shift="5000" sync1Cycle="750000" sync1Shift="0"/>
            </slave>
        </master>
    </masters>


EtherCAT cycle time
"""""""""""""""""""

The EtherCAT cycle time has to be set:

* EtherCAT topology XML file, the ``appTimePeriod`` attribute of the ``master``
  tag. Example:

.. code::

   <master idx="0" appTimePeriod="1000000">

* In the SoE startup file, the following IDN also has to be set with the same
  period.

    * IDN `S-0-0001` Control unit cycle time (TNcyc)
    * IDN `S-0-0002` Communication cycle time (tScyc)

The following code snippet shows the definition of the IDN `S-0-0001` for a period
of 1000 us.

.. code-block:: xml

    <InitCmd>
        <Transition>PS</Transition>
        <Comment><![CDATA[Tncyc - NC cycle time]]></Comment>
        <Timeout>0</Timeout>
        <OpCode>3</OpCode>
        <DriveNo>0</DriveNo>
        <IDN>1</IDN>
        <Elements>64</Elements>
        <Attribute>0</Attribute>
        <Data>e803</Data>
    </InitCmd>

Distributed clock configuration
"""""""""""""""""""""""""""""""

**Assign Activate**: 0x730

**Sync0**

    The `Sync0` cycle time may only be configured with 62.5 us, 125 us or 250 us,
    otherwise the CPU generates the error code F409.

**Sync1**

    The `Sync1` signals are parameterized according to the EtherCAT cycle time.
    This cycle time is always a multiple of `Sync0`. If a signal fails to materialize,
    the CPU also generates the error code F414, and the connected axes are stopped
    with the `"EStop ramp"`.


This :ref:`code snippet<ax5000_dc_snippet>` shows a DC configuration.
Explanation:

* The *EtherCAT* cycle time is set to 1000 us
* Assign AC is set to 0x730
* Sync0 cycle: 250 us
* Sync1 cycle: 750 us This value is obtained with this formula: ``(EtherCAT cycle time)
  - (sync0 time)``. In this particular case ``1000 us - 250 us = 750 us``

.. _ax5000_dc_snippet:

.. code-block:: xml

    <dcConf assignActivate="730" sync0Cycle="250000" sync0Shift="5000" sync1Cycle="750000" sync1Shift="0"/>

The XML files below are examples for ``AX5100`` or an ``AX5200`` drive set in
``position control`` mode with an EtherCAT frequency of 1 KHz.

* AX5100

    * EtherCAT topology: ax5100_ec_test.xml
    * IDN configuration: ax5100_startup.xml

* AX5200

    * EtherCAT topology: ax5200_ec_test.xml
    * IDN configuration: ax5200_startup.xml


.. _Beckhoff EL7411:

Beckhoff EL7411
^^^^^^^^^^^^^^^

The Beckhoff EL7411 is a servomotor drive with an integrated resolver interface.

.. note::

    It is a new terminal and the official documentation is not complete

PINs list
"""""""""
================== ========= =========
Name               Type      direction
================== ========= =========
enable             HAL_BIT   HAL_IN
target-position    HAL_FLOAT HAL_IN
enabled            HAL_BIT   HAL_OUT
status-ready       HAL_BIT   HAL_OUT
status-switched-on HAL_BIT   HAL_OUT
status-operation   HAL_BIT   HAL_OUT
status-fault       HAL_BIT   HAL_OUT
status-disabled    HAL_BIT   HAL_OUT
status-warning     HAL_BIT   HAL_OUT
position-feedback  HAL_S32   HAL_OUT
================== ========= =========

PARAMs list
"""""""""""

======================= ========= =======
Name                    Type      mode
======================= ========= =======
scale                   HAL_FLOAT HAL_RW
======================= ========= =======


Distributed clock configuration
"""""""""""""""""""""""""""""""

The official documentation does not mention the DC configuration.

The following configuration is working. It was extracted for experimentation done
with TwinCAT.

.. code-block:: xml

    <dcConf assignActivate="730" sync0Cycle="62500" sync0Shift="5000" sync1Cycle="937500" sync1Shift="0"/>


.. _lcec companion:

OpenCN companion
^^^^^^^^^^^^^^^^

OpenCN companion is *simple* *EtherCAT* slave. The doc of this slave can be found
`here <companion.html>`_.

In OpenCN, the support of the OpenCN Companion EtherCAT slave is part of the lcec
component. The following code shows an example of an EtherCAT topology file with
one Companion slave.

.. code-block:: xml

   <masters>
      <master idx="0" appTimePeriod="1000000">
         <slave idx="0" type="COMPANION" name="COMP">
            <dcConf assignActivate="300" sync0Cycle="*1" sync0Shift="500000"/>
         </slave>
      </master>
   </masters>

The companion export the following PINs:

================= ========= =========
Name              Type      direction
================= ========= =========
in-0                bit     OUT
in-1                bit     OUT
in-2                bit     OUT
in-3                bit     OUT
out-0               bit     IN
out-1               bit     IN
out-2               bit     IN
out-3               bit     IN
pwm-duty-cycle-0    u32     IN
pwm-duty-cycle-1    u32     IN
================= ========= =========

New slave integration
---------------------

This paragraph gives an overview on how to integrate a new type of slave. The slave
behavior is implemented in the kernel.

1. Add the new type in ``lcec_slave_type_t`` enum defined in ``<OPENCN_HOME>/include/opencn/uapi/lcec_conf.h``
   file.
   Example:  ``lcecSlaveTypeEL12522``.

2. Add   the   new slave type in the ``slaveTypes[]`` array in ``usr/components/lcec/lcec_xml_parser.c``
   file. The  first  parameter  is  the  slave  name  (a  string,  this  string  is  then  used  in  the  EtherCAT configuration XML file) and the second parameter is the value added in previous
   step.
   Example: ``{ "EL1252", lcecSlaveTypeEL1252 }``

3. In the kernel
   a. Create the slave source files:

      * ``my_slave.h``: VID, PID, PDO number and the prototype of the init function
      * ``my_slave.c``: implementation of the feature behavior: initialization read
        and write functions. The PDOs and Sync managers should also be defined here.

   b. In ``lcec_conf.c`` file, ``types[]`` array: add the new slave definitions
      ``{lcecSlaveTypeEL1252,     LCEC_EL1252_VID,     LCEC_EL1252_PID,     LCEC_EL1252_PDOS,
      lcec_el1252_init}``

SDO read-back
^^^^^^^^^^^^^

*lcec* provides a mechanism to read-back SDO values. It can used to retrieve SDO
value sets from the ``InitCmds`` XML file.

To perform a SDO read-back, a SDO transaction should be added in the SDO transaction
pending list. The following function can be used to perform this task:

.. code-block:: c

    void lcec_sdo_add_trans(lcec_sdo_trans_t *trans);

A SDO transaction elements are:

    * **slave**: Salve which asks for the transaction
    * **index**: SDO index
    * **subindex**: SDO subindex
    * **data**: Pointer to store the transaction result
    * **data_size**: size of the SDO element
    * **ec_min_state**: minimum EtherCAT state in which the slave has to be to perform
      the SDO read. Valid state are: ``EC_AL_STATE_INIT``, ``EC_AL_STATE_PREOP``,
      ``EC_AL_STATE_SAFEOP``, ``EC_AL_STATE_OP``
    * **read_done**: true if the transaction has completed

.. note::

    The SDO transaction takes time to complete. It is to the requester to check
    if its transaction has completed or not.

* **Example A:** Transaction creation:

.. code-block:: c

    lcec_sdo_trans_t sdo_trans;

    sdo_trans.slave = slave;
    sdo_trans.index = 0x6092;
    sdo_trans.subindex = 1;
    sdo_trans.data = kmalloc(4, GFP_ATOMIC);
    sdo_trans.data_size = 4;
    sdo_trans.ec_min_state = EC_AL_STATE_PREOP;

    lcec_sdo_add_trans(sdo_trans);

* **Example B:** Checking transaction completion>

.. code-block:: c

    if (sdo_trans->read_done)
        ...

Tips & Good Practice
^^^^^^^^^^^^^^^^^^^^

Here are some tips for the integration of a new EtherCAT slave.

* Be sure that you are using the latest stable firmware.
* Use TwinCAT at first to setup/test your slave/drive.
    * Switching slave state one by one, from INIT, PRE-OP, SAFE-OPup to OP, sometimes
      help to prompt more errors and debug info on TwinCAT
* When developing a new driver's slave, everything can run perfectly when alone
  on the EtherCAT topology.
  When coding (possibly PDO mapping, :func:`memset` function) is mishandled, strange
  behaviors can happen and render subsequent slave inoperable on the EhterCAT topology.
  It is a good practice to test your slave along with other and to check that everything
  is running as expected before merging. In that case, depending on configuration
  we saw different behaviors:

    * slave going into OP state and refusing any command
    * slave watchdog timeout, falling into PreOP+Error state

* A driver's slave implementation can be partial by only using some features of
  the slave as long as it is properly documented, but we still encourages a full
  implementation ;).

Beware of the behavior of the drive to properly handle your code, some things to follow for CiA402 drive profile (and maybe other) : 

* Check the mode of operation display (0x6061) **before** checking the status word (0x6041).
  The bits of the status word is dependant on the mode of operation of the drive. 
  For example on a TSD80, in mode of operation (6) *homing* the bit 12 tell the drive is homed. 
  Whereas in mode operation (8) *CSP*, the bit 12 tell us the drive is ready to follow *CSP* command
* For the Cyclic Synchronus Position *CSP* mode, always write the target-position as the current-position of the drive **before** 
  changing the mode of operation (0x6060) to *CSP* mode. This avoid jump in the motor position, overcurrent, *clack* noise or even falling in fault.

