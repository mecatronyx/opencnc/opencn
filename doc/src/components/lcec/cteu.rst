.. _cteu:

##########
Festo CTEU
##########

The fieldbus modules for valve terminals with Festo-specific interface and high
degree of protection IP65/67 can be used in many applications and offer a multitude
of functions.

To be able to use this slave with OpenCN, the fix configuration has to be programed
in the SII. The programming can be done using TwinCAT.

* ETSI file: ``Festo CTEU-EtherCAT Fix - BITARR8 - 20151019.xml``
* Doc: ``CTEU-EC Config fix-mdp_EN.pdf``

.. note::

	In the current implementation, the number of Input and Outputs PDOs is hardcoded
	in the source file. It can be changed by setting ``LCEC_CTEU_IN_PDOS`` and
	``LCEC_CTEU_OUT_PDOS`` macros to the wanted number.

*********
PINs list
*********

================= ======== =========
Name              Type     Direction
================= ======== =========
inN.bit-M         HAL_BIT  HAL_OUT
outN.bit-M        HAL_BIT  HAL_OUT
================= ======== =========

where
* N is the PDOs N. It depends on the number of Input or Output PDOs are defined
* M is the bit number (0 to 7)
