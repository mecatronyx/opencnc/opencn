.. _tsd80e:

##############
Triamec TSD80e
##############

.. warning::

	Under construction


******************
Startup parameters
******************

Here are some of the *startup parameters* (or *init_commands*) supported the TSD80e
drive. The reader can find :ref:`below<tsd80_initcmds>` an example of TSD80e startup
parameter file used with a micro3 milling machine.


.. note::

	The index in the following table is for the axis0. The index for the axis1 =
	axis0 offset + 0x800


=============  ======  =========  ====================================================
Name           Index   sub-index  Description
=============  ======  =========  ====================================================
Feed           0x6092   01        To change the default scaling, use this parameter.
                                  Its units are inc/mm or inc/degree. The default
                                  value is 10'000. Formulas:

                                  * resolution [mm] = (1 / Feed)
                                  * range [mm] = resolution * 2^31

                                  To get for example 1nm resolution,
                                  choose the value 1'000'000 (1mm = 1'000'000nm). Be
                                  aware that this reduces the maximum possible position
                                  to 2^31*1nm = 2.1m (The EtherCAT data size is 32-bit).
Homing offset  0x23ee   00        Offset to the homing position
=============  ======  =========  ====================================================

.. note::

	The easiest way to generate a startup setting file is to use TwinCAT.

	**NB**: It is needed to set the *end of line** of the generated file to ``UNIX``
	(using ``does2unix`` tool for example).

**Example**

.. _tsd80_initcmds:

.. code-block:: xml

	<?xml version="1.0" encoding="ISO-8859-1"?>
	<EtherCATMailbox>
		<CoE>
			<InitCmds>
				<InitCmd>
					<Transition>PS</Transition>
					<Ccs>1</Ccs>
					<Comment><!Feed parameter, value: 0x2000000></Comment>
					<Timeout>0</Timeout>
					<Index>0x6092</Index>
					<SubIndex>01</SubIndex>
					<Data>00000002</Data>
		    	</InitCmd>
				<InitCmd>
					<Transition>PS</Transition>
					<Comment><!Feed parameter, value: 0x2000000></Comment>
					<Ccs>1</Ccs>
					<Timeout>0</Timeout>
					<Index>0x6892</Index>
					<SubIndex>01</SubIndex>
					<Data>00000002</Data>
		    	</InitCmd>
			</InitCmds>
		</CoE>
	</EtherCATMailbox>

