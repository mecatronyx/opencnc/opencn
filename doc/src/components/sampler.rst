.. _sampler:

=======
Sampler
=======

| The sampler is used to sample HAL data from pins and store it in a file or in a
  shared memory segment. It is possible to sample float, signed32, unsigned32 and boolean pin.
| The commande to load the sampler is the following : 

.. code-block:: shell

	halcmd load sampler --cfg=<string> [--depth=<int>] [--name=<string>] [--number=<int>] [--tag] [--memory] [--timeusrloop] [--path=<ABS_PATH>]


Where the parameters are:

* **cfg** : Defines the set of HAL pins that sampler exports and later samples data
  from. The sampler exports one pin for each character in string.
  Legal characters are:

  * f/F : (HAL_FLOAT)
  * b/B : (HAL_BIT)
  * s/S : (HAL_S32)
  * u/U : (HAL_U32)
* (optional) **depth** : Sets the depth of the ring buffer between to RT and userspace side,
  before transmitting it to the file or the shared memory segment. Increase it in case of lost frame (i.e *overruns* pin is higher than zero).
* (optional) **name**: Set a name for the sampler instance, exported pins will be 'sampler.<name>'. 
  If the name is not specified, the sampler will increment the instance number at each load, starting at 0.
* (optional) **number** : The number of sample that will be recorded. If *number* is unspecified, the sampler run as long as the *enable* pin is true.
* (optional) **tag**: Add the sample numbers at the start of the log files. This number
  is incremented by 1 at every new sample. The sample numbers is put back at 0 every time the sampler is re-enabled.
  If you see missing value (e.g sample n° is 536->537->569->570), you have lost frame, increase ``<depth>`` parameter.
* (optional) **memory** : The sampler will fill a message queue instead of a file. See section `Memory Mode`_.
* (optional) **timeusrloop** : Used for debug, time taken by the userspace loop to write data to file.
  Value is written at the last column of the log file.
* (optional) **path** : Specify an *absolute* path where the output logfile is stored (need to be an ext4 partition). 
  By default, the path is ``/root/``. The path **must exist** before loading the sampler.
  Be sure to format your parition in ext4 with ``sudo mkfs.ext4 /dev/sdaX``

The result file is stored by default in : ``/root/log_sample_<name>.csv``

*****************
HAL PINS
*****************

-------------------
General pins
-------------------
  =======================    =======  =========   ============================================================
  Name                       Type     Direction     Description
  =======================    =======  =========   ============================================================
  sampler.<name>.enable      HAL_BIT  HAL_IN      Enable/Disable the sampling
  sampler.<name>.overruns    HAL_S32  HAL_IO      | Number of sample / Lost frame between the RT and usespace
                                                  | The value is reset when a ``new-file`` is requested. 
                                                  | See `Troubleshooting lost sample / overruns`_.
  sampler.<name>.pin.%d      /        HAL_IN      Linked pin that will be sampled, 
                                                  pin type is defined depending on <cfg> parameter
  =======================    =======  =========   ============================================================

-------------------
General parameters
-------------------
  ========================    =========   =========   ===================================================================
  Name                        Type        Direction   Description
  ========================    =========   =========   ===================================================================
  sampler.<name>.loss-warn    HAL_FLOAT   HAL_RW      Time between warning for potential lost frames in [s] (Default=3s).
                                                      Set a negative number to desactivate.
  ========================    =========   =========   ===================================================================

-------------------
File mode pins
-------------------

  =======================    =======  =========   =============================================================
  Name                       Type     Direction     Description
  =======================    =======  =========   =============================================================
  sampler.<name>.new-file    HAL_BIT  HAL_IN      Overwrite ``/root/log_sample_<name>.csv`` with an empty file
                                                  and write the header. Fall back to ``0`` once file is created
  =======================    =======  =========   =============================================================

*****************
Usage
*****************

.. warning::
  This example expect the user to have some basic knowledge over the HAL system and is only meant to show 
  a basic example of the sampler usage.

Here is an example to record 3 float and 1 boolean from different component :

.. code-block:: shell

  # Load the sampler to record 3 float and 1 boolean
  halcmd load sampler --cfg=fffb --tag 

  # Link the pin to record
  halcmd net Your_signal_Name_1     sampler.0.pin.0 Your_Float_pin
  halcmd net Your_signal_Name_2     sampler.0.pin.1 Your_Other_Float_pin
  halcmd net Your_signal_Name_3     sampler.0.pin.2 Your_Another_Float_pin
  halcmd net Your_signal_Name_4     sampler.0.pin.3 Yet_Another_pin

  # IMPORTANT : Link the sampler to a thread

  # Start the sampling
  halcmd setp sampler.0.enable 1

  # Stop the sampling
  halcmd setp sampler.0.enable 0

  #Check if we had any overruns (=lost pins / 0=ok)
  halcmd show pin sampler.0.overruns


| To recover the file when running on a virtual machine (x86-qemu or virt64), please look at :ref:`Recover file in VM <recover_file_vm>`.
| The sampler automatically create a log file with the header. Regarding the header, the sampler try for 1 seconds to recover
  the signal name to put in the header, if not possible, the channel name is ``N/A``. If more pin as been linked to the sampler
  the user can ask for a new file with ``sampler.<name>.new-file``, and the sampler will try again to recover the signal name.
| If the sampler is enabled, ``sampler.<name>.enable`` at 1, and a ``sampler.<name>.new-file`` is asked, the sampler will create
  a new file and the sampling will continue. If the tag option is enable, the sample number will not be reset.

****************
Features
****************

----------------
Custom Header
----------------

If a file ``/root/header.txt`` exist, the sampler will add it to the header of the log file ``/root/log_sampler_<name>.csv``

.. warning:: 

  The custom header is common for all the sampler instance.

*************
Limitations
*************
The sampler has some limitations : 

* Number of recordable pin : 60 pins can be recorded, *SAMPLER_MAX_PINS* define this limit.
* Number of instance : A maximum of number of 8 sampler instance can be loaded in one HAL config, *SAMPLER_MAX_INSTANCE* define this limit.
* Header : At the load of the sampler, it will try for *SAMPLER_SIGNAL_TIMEOUT* seconds (default=1s) to retrieve the name of the signal to put it in the header of the file.
  In the case of manual loading/linking, this delay is too short, the user can ask for a *new-file* and the sampler will try again to retrieve the signal name to write in the header.

*****************
Functionnement
*****************
| The real time function of the sampler is periodically sampling the HAL pins and
  writes the data in a ring buffer structure placed in a memory segment shared between
  the user space and the kernel space.  
| The user space can obtain pin values by reading the ring buffer entries. The program
  running in the user spaces can write these values into a file or into a
  memory slot depending on the argument of the component.  

*****************
Memory Mode
*****************

.. _sampler_memory_mode:

The sampler uses the msgsnd() system call to place the pins values into
a memory queue. If the queue is full, the message is dropped.
A process that needs to read the messages can get the shared memory
segment ID using msgget() and the key of the shared memory segment that
is provided in sampler.h.
To pop a message from the queue, the process should use msgrcv() function.
The pins are placed in the message inorder.

An example ``test_sampler_memory.cpp`` file can be found under ``<opencn-repo>/agency/usr/target/component/sampler/example``

*****************************************
Troubleshooting lost sample / overruns
*****************************************

When sampling numerous pins (>30pins), at high framerate (10kHz), a loss of frame may occur.
Different factor can impact the loss : 

* The number of **total** sampled pin
* The sampling speed
* The current load on the userspace side *(CPU 2 & 3)*
* Buffer size ``depth`` parameter
* Writing speed of the device

You can try different fix for this : 
 
1. If possible, decrease number of sampled pins, 
2. If possible, decrease sampling speed.
3. Increase ``depth`` size. *(Tested up to 4096)*
4. Use a SSD/HDD with better bandwidth
5. Use a separated SSD/HDD from the "OS side", be sure to format your device in ext4 with ``sudo mkfs.ext4 /dev/sdaX`` (for example). Then mount it at boot in ``start_opencn.sh`` (for example), and specify the *absolute* ``path`` option to the sampler. See :ref:`this <env_ssd_drive>` for more information.