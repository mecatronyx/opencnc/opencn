.. _ocno:

==========================
OpenCN Orchestrator (OCNO)
==========================

.. toctree::
   :maxdepth: 5
   :hidden:

   core
   homing
   jogging
   machining


*OCNO* is the *OpenCN Orchestrator*. It manages the different states of a machine
by *managing* the different components around it.

*OCNO* defines the current state of a machine. It is divided into different working modes:

* Inactive mode
* :ref:`Homing mode <ocno homing>`
* :ref:`Jogging mode <ocno jogging>`
* :ref:`Machining mode <ocno machining>`


It also offers an interface to load external modes for specific cases.

.. note::

	External modes / interfaces are not supported in current version.

The command to load it is the following:

.. code-block:: shell

	halcmd load ocno axes=<AXES>

Where the parameters are:

* axes: a string with the axes to export. Allowed characters are [x,y,z,a,b,c,u,v,w].
  A specific character can be set only once. For example, ``axes=xyzz`` is not allowed,
  whereas ``axes=xyzuvw`` is correct.
