.. _ocno machining:

==============
Machining mode
==============

The *Machining* mode is the mode where the machine gets its data from a G-Code.
*OCNO* gets points from a path planning component (``feedopt`` in the current release).
Machining process can be stopped or paused at any time.

.. warning::

  The current implementation does not properly support a recovery of the machining
  after a pause


The *Machining* mode provides the following features:

* Being able to set a return position after milling is done (``ocno.machining.axisN.clearing-pos``
  PIN)
* Wait an amount before launching machining (``ocno.machining.start-delay`` PIN)
* Warn before machining start (``ocno.machining.will-run`` pin)
* Tell when it is machining or not (``ocno.machining.running`` PIN)
* Safety by managing the ``underrun``. An ``underrun`` occurs when the *Sample
  Queue* of the *Path Planer* is empty (no more samples are available). This can
  occur at any moment while machining mode is in run state. The system will send
  a quick stop before going back to an idle state.

.. warning::

  The quickstop is not properly supported in the current release

.. drawio-figure:: pictures/ocno.drawio
   :format: jpg
   :page-name: ocno-machining-fsm
   :align: center

   *OCNO* Machining Finite State machine

**States description**

* **Init**: Send the command to the “device” to enter CSP mode
* **Wait Mode**: Wait for the “device” to be entered CSP mode
* **Wait Cmd Operation**: It waits for the  Path-Planning component (feedopt in
  current version) to be ready and for the user to trigger a “start”. When both
  conditions are filled, the system is set to move to the clearing position. Also,
  if the spindle rotation is required, the spindle is enabled and the system will
  go to “wait spindle reach state”. If not, directly jump to go “clearing position”
  state
* **Wait Spindle Reach Speed** : Wait for the spindle to reach the required RPM.
  The minimum speed is defined by the chosen spindle rotation speed threshold.
* **Go Clearing Position** : Get the clearing position (user-defined, usually far
  from workpiece) for an axis.
* **Run Clearing Position**: Go to the clearing position of the given axis. This
  phase helps to avoid tool breakage.
* **Go Start Pos**: Start process to go to machining start position, one axis at
  a time
* **Run Start Pos**: Process the movement to go to the machining start position,
  for axis N. Once completed, ask to Start for the next axis (go to Ask Start Pos
  state). In this state, the spindle RPM will be checked at every sample. When the
  RPM is too low, a quick stop will be sent and the process will stop and go back
  to idle mode.
* **Wait Cmd Machining**: Wait for a Machining start command
* **Wait Start Delay**: Wait a delay, user configurable, before starting the machining
* **Run**: process machining, check each cycle that the spindle RPM is at least at
  XX% of the required speed (configurable).
* **Go Final Position**: Initiate to move axis N (on axis at a time) to the final
  position
* **Run Final Position**: Process the move of axis N to the final position (one
  axis at a time). Once completed, start to move the next axis to its final position.
  If all axes are in their final position, the machining process is completed. The
  process is done and the system goes back to “Wait Spindle Zero Speed” state.
  * **Wait Spindle Zero Speed**: Wait until the spindle has stopped.

*************************
HAL Machining global PINs
*************************

===================================  =========  =========  ===============================================
Name                                 Type       Direction  Description
===================================  =========  =========  ===============================================
ocno.machining.start                 HAL_BIT    HAL_IN     Start machining
ocno.machining.pause                 HAL_BIT    HAL_IN     Pause machining execution
ocno.machining.resampling-pause      HAL_BIT    HAL_OUT    if a pause is triggered, the resampling also
                                                           needs to be paused.
ocno.machining.reset                 HAL_BIT    HAL_IN     Reset machining execution
ocno.machining.operation-ready       HAL_BIT    HAL_IN     Inform if system is ready to start machining
                                                           (Path Planning component)
ocno.machining.will-run              HAL_BIT    HAL_OUT    Informs that the machining will start
                                                           `ocno.machining.start-delay` seconds after the
                                                           actual machine motion. It can be used in
                                                           different way to tell other component the
                                                           machining will soon start
ocno.machining.running               HAL_BIT    HAL_OUT    the machining process is running
ocno.machining.get-one-sample        HAL_BIT    HAL_OUT    Ask only one sample to the path planner
                                                           component. used to go to the starting position
ocno.machining.read-samples          HAL_BIT    HAL_OUT    The FSM is ready to read samples and to move
                                                           axes
ocno.machining.start-delay           HAL_U32    HAL_IN     Delay between a ‘start’ command and the
                                                           starting of the machining processing in [ms].
                                                           Set at 0ms by default
ocno.machining.finished              HAL_BIT    HAL_IN     Last sample is sent, machining is over
ocno.machining.underrun              HAL_BIT    HAL_IN     Information about the underrun. Put at true if
                                                           the path planner was not able to send target
                                                           position when it was required
ocno.machining.spindle-speed         HAL_FLOAT  HAL_IN     Contains the current spindle rotation speed
                                                           value
ocno.machining.target-spindle-speed  HAL_FLOAT  HAL_IN     Spindle target speed value required
===================================  =========  =========  ===============================================

***************************
HAL Machining PINs per axis
***************************

=================================  =========  =========  ============================================
Name                               Type       Direction  Description
=================================  =========  =========  ============================================
ocno.machining.axisN.cmd-pos       HAL_FLOAT  HAL_IN     Axis command/target position
ocno.machining.axisN.clearing-pos  HAL_FLOAT  HAL_IN     Position to move the axis once the machining
                                                         has completed
=================================  =========  =========  ============================================
