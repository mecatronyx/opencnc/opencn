.. _ocno homing:

===========
Homing mode
===========

.. note::

	In current version, *OCNO* only supports homing performed by the drive. *OCNO*
	just sends a command to the drive to start the homing process.

The homing is performed one axis at a time. It is done in the axis order as set
when *OCNO* is loaded (axes parameter).

The Homing mode also provides the possibility to move the axes to a specific position
once the homing is done. It is done by setting the ``ocno.homing.axisN.move-offset``
to the wanted absolute position.

A homing processing can be started with ``ocno.homing.start`` PIN. At any moment,
the homing process can be stopped with the ``ocno.homing.stop`` PIN. This stops all
the axes and set the homing FSM in *IDLE* state. The machine remains in *Homing* mode.

The following figure shows the *OCNO* Homing FSM.


.. drawio-figure:: pictures/ocno.drawio
   :format: png
   :page-name: ocno-homing-fsm
   :align: center

   *OCNO* Homing Finite State machine

**States description**

* **Init**: Set the mode of the axis to "homing". The machine will be set to inactive.
* **Wait Mode**: Check that the “devices” are in homing mode. The FSM will be in this
  state until the axes are in homing mode.
* **Wait Cmd**: Wait the user command to start homing processing. This state allows the
  user to set any offset before starting the homing. When the user requires a start,
  the machine will be set to running mode.
* **Axis Start**: Start homing on axis N. The axes will be processed one by one.
* **Axis Started**: Check that the homing process has started by checking the ``ocno.homing.homed``
  PIN. It is expected to be 0 during homing process.
* **Axis homed**: Stay in this state until the homing on the axis is done. When the
  axis is homed, there are two possibilities :

   * Offset of the pin=0, (previously required by the user): we jump to the state
     “home next axis”.
   * Offset of the pin!=0 : first we change the mux to ensure we’re sending the
     positions from PG. Then we set the axis in CSP mode, then go to “wait csp mode”
     state.

* **Wait CSP mode**: Before moving to the required offset, all axes need to be in CSP
  mode, this state will loop until this requirement is filled.
* **Go offset pos**: Start the current axis motion to the "offset position"
* **Run offset pos**: Loop until the axis is at the required offset. Then the next axis
  can be processed.
* **Home next axis**: This state checks if there are more axes to be processed or not,
  there is two possible output :

   * All axes processed: Homing is over we go back to the “init” state. This will reset
     the previous setup from the user and wait for a new command.
   * More axes to process: Process the next axis. Jump back to the state “Axis start”.


**********************
HAL Homing global PINs
**********************

======================  =========  =========  ==========================================
Name                    Type       Direction  Description
======================  =========  =========  ==========================================
ocno.homing.start       HAL_BIT    HAL_IN     Start homing processing
ocno.homing.stop        HAL_BIT    HAL_IN     Stop homing processing
ocno.homing.processing  HAL_BIT    HAL_OUT    Information about the homing process state
======================  =========  =========  ==========================================

************************
HAL Homing PINs per axis
************************

=============================  =========  =========  ==========================================
Name                           Type       Direction  Description
=============================  =========  =========  ==========================================
ocno.homing.axisN.start        HAL_BIT    HAL_OUT    Send cmd to start homing process to axis N
ocno.homing.axisN.stop         HAL_BIT    HAL_OUT    Send cmd to stop homing process to axis N
ocno.homing.axisN.homed        HAL_BIT    HAL_IN     Inform if the axis is homed or not
ocno.homing.axisN.move-offset  HAL_FLOAT  HAL_IN     Axis position after homing done
                                                     NB: the axis position will be equal to this
                                                     value
=============================  =========  =========  ==========================================


