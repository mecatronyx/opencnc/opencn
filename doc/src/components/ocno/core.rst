.. _ocno core:

=========
OCNO Core
=========

OCNO core is the part of OCNO responsible of changing the different mode of the
machine (jog-home-machining..). It also provide generic function to handle the
machine like the axis state, position, the current mode, etc...

*********************************
Management of the different modes
*********************************

Only one mode can be selected at a time (``ocno.set-machine-<MODE>`` PINs). If
multiple modes are selected, the machine remains in its current mode and an error
message is printed in the console.

The change of the machine mode can only be done when the machine state is *IDLE*.
A machine is in *IDLE* state when it waits for a command. If a new mode is selected
when the machine is not in *IDLE* state, it remains in its current mode and an error
message is printed in the console. The machine state can be read in ``ocno.machine-state``
PIN.

If the system is not homed, it is only possible to enter in *Homing* and *Jogging*
modes. Any other modes required the machine to be homed to work.

The modes commands (``ocno.set-machine-<MODE>`` PINs) are reset by OCNO. In the
same logic, the commands sent to a drive have to be rested by the component receiving
these commands.

It is possible to stop machine processing at any time by setting ``ocno.stop_operation``
PIN.

******************
Machine dimensions
******************

*OCNO* provides the ``ocno.axisN.limit-min`` and ``ocno.axisN.limit-max`` HAL
parameters.

A cross of a soft limit is threaded like a machine fault:

* Machine is set in *Inactive* mode
* The ``ocno.machine-in-fault`` PIN is set

 A specific message is also printed in the console.

 Setting these parameters to 0 disable limits checks.

*******
Spindle
*******

*OCNO* can request to enable the spindle by setting the ``ocno.spindle.enable``
PIN. This PIN informs the system when to activate the spindle. It also gives the
requested spindle speed PIN through the ``ocno.spindle.speed`` PIN.

Before enabling the ``ocno.spindle.enable`` PIN, *OCNO* does two checks.

First it checks that a request of the spindle is done by reading ``ocno.spindle.request``
PIN. If no spindle request, the spindle is not activated.

Then it checks that the spindle can be safely enabled by reading ``ocno.spindle.can-turn``
PIN. This PIN is expected to be connected to a specific PLC which performs safety
check to ensure the hardware can respond to the commands. If it is not the case,
the system will enter in inactive mode and activate the error pin.

.. note::

	*OCNO* does only manage ONE spindle.

****************
Fault management
****************

When a fault is detected (``ocno.axisN.in_fault`` PINs), the machine is immediately
set in *Inactive* mode.

The ``ocno.machine-in-fault`` PIN can be used to read the machine fault status.
The fault can be resetted by setting the ``ocno.fault-reset`` PIN. It will send
the fault reset command to all axes. This PIN is reset to 0 by *OCNO*.

Quick stop :

..
    Some errors can trigger a quick stop. The quick stop prevents the hardware from
    being damaged. Quick stop commands will be sent and the machine will stop in a
    smooth way that will minimize the damage on the spindle before going back to an
    idle state. Currently, the quick stop only affects the spindle, but it should also
    affect the axis to improve the safety.

There is a generic error state that can be triggered by any mode. This error is
used to leave the current mode and to go back to the inactive mode.
If any axis enters an error state, the system will trigger the error state and come
back to inactive mode.

***************
HAL global PINs
***************

**Modes management**

==========================  =========  =========  ========================================
Name                        Type       Direction  Description
==========================  =========  =========  ========================================
ocno.set-machine-inactive   HAL_BIT    HAL_IN     Set the machine in inactive mode
ocno.set-machine-homing     HAL_BIT    HAL_IN     Set the machine in homing mode
ocno.set-machine-jog        HAL_BIT    HAL_IN     Set the machine in jogging mode
ocno.set-machine-machining  HAL_BIT    HAL_IN     Set the machine in machining mode
ocno.in-machine-inactive    HAL_BIT    HAL_OUT    If set, the machine is in inactive mode
ocno.in-machine-homing      HAL_BIT    HAL_OUT    If set, the machine is in homing mode
ocno.in-machine-jog         HAL_BIT    HAL_OUT    If set, the machine is in jogging mode
ocno.in-machine-machining   HAL_BIT    HAL_OUT    If set, the machine is in machining mode
ocno.stop-operation         HAL_BIT    HAL_IN     Stop current processing
ocno.machine-state          HAL_U32    HAL_OUT    Machine state -
                                                  * 0: idle
                                                  * 1: Running
ocno.machine-homed          HAL_BIT    HAL_OUT    Machine ``homed`` state -
                                                  * 0: not ``homed``
                                                  * 1: ``homed``
==========================  =========  =========  ========================================

**Fault management**

=====================  =========  =========  =========================================
Name                   Type       Direction  Description
=====================  =========  =========  =========================================
ocno.fault-reset       HAL_BIT    HAL_IN     General fault reset. Send the fault-reset
                                             command to all axis (drive)
ocno.machine-in-fault  HAL_BIT    HAL_OUT    The machine is in fault. It means that at
                                             least one axis is in fault.
=====================  =========  =========  =========================================

**Position control**

=================  =========  =========  ===========================================
Name               Type       Direction  Description
=================  =========  =========  ===========================================
ocno.datapath-sel  HAL_U32    HAL_OUT    Selection of the datapath. In current
                                         implementation, 2 data-paths are supported:
                                         direct or  Simple-PG.
=================  =========  =========  ===========================================

****************
HAL spindle PINs
****************

=============================   =========  =========  ===========================================
Name                            Type       Direction  Description
=============================   =========  =========  ===========================================
ocno.spindle.enable             HAL_BIT    HAL_OUT    Enables spindle to rotate
ocno.spindle.request            HAL_BIT    HAL_IN     User request to enable the spindle rotation
ocno.spindle.can-turn           HAL_BIT    HAL_IN     The spindle is ready to rotate
ocno.spindle.speed              HAL_FLOAT  HAL_OUT    Target RPM for the spindle to reach
ocno.spindle.fault-reset        HAL_BIT    HAL_OUT    Reset the error on the drive
ocno.spindle.velocity-reached   HAL_BIT    HAL_IN     Spindle has reached its velocity
=============================   =========  =========  ===========================================

*****************
HAL PINs per axis
*****************

============================  =========  =========  ===========================================
Name                          Type       Direction  Description
============================  =========  =========  ===========================================
ocno.axisN.set-mode-inactive  HAL_BIT    HAL_OUT    Request to set axis in inactive mode
ocno.axisN.set-mode-homing    HAL_BIT    HAL_OUT    Request to set axis in homing mode
ocno.axisN.set-mode-csp       HAL_BIT    HAL_OUT    Request to set axis in csp mode
ocno.axisN.in-mode-inactive   HAL_BIT    HAL_IN     If set, the axis is in inactive mode
ocno.axisN.in-mode-homing     HAL_BIT    HAL_IN     If set, the axis is in homing mode
ocno.axisN.in-mode-csp        HAL_BIT    HAL_IN     If set, the axis is in csp mode
ocno.axisN.current-joint-pos  HAL_FLOAT  HAL_IN     Current axis position
ocno.axisN.target-pos         HAL_FLOAT  HAL_OUT    Axis target position
ocno.axisN.position-offset    HAL_FLOAT  HAL_IN     Add a position offset to the axis N.
ocno.axisN.in-fault           HAL_BIT    HAL_IN     The axis N (drive) is in fault
ocno.axisN.fault-reset        HAL_BIT    HAL_OUT    Rested axis N (drive) faults
ocno.axisN.quick-stop         HAL_BIT    HAL_OUT    Something went wrong and required a quick
                                                    stop for the axis.
============================  =========  =========  ===========================================

**************
HAL parameters
**************

============================  =========  =========  ===========================================
Name                          Type       Direction  Description
============================  =========  =========  ===========================================
ocno.axisN.limit-min          HAL_FLOAT  HAL_RW     Minimum limit for the axis N.
                                                    Default = 0 (no limits)
ocno.axisN.limit-max          HAL_FLOAT  HAL_RW     Maximum limit for the axis N
                                                    Default = 0 (no limits)
============================  =========  =========  ===========================================
