.. _ocno jogging:

============
Jogging mode
============

The *Jogging* mode lets the axes/joints to be in free run. It sends the target
position to a *Profile Generator* component like the Simple-PG one. This mode does
not compute the command points.

This mode supports:

* Relative or absolute move
* Perform a jogging on multiple axes at the same time

The Jogging mode can be used with the spindle rotating or with an idle spindle.
The user can change this mode at any moment. The user can also change the spindle
velocity at any moment. The system will change the velocity in any state. If a
command was triggered, this running command will end before the velocity is modified.

The machine does not have to be homed to enter in *Jogging* mode. The behavior is
 different depending on the homing state :

* Not homed : relative jog only
* Homed : absolute jog or relative jog

The jogging process is the following:

1. Configure the axes to move by setting its target position (``ocno.jog.axisN.cmd-pos``)
   and set it active (``ocno.jog.axisN.active``). The ``ocno.jog.axisN.active`` PIN
   is automatically rested.
2. Start the motion (``ocno.jog.start``).
3. The ``ocno.jog.axisN.is_running`` PINs informs about the processing state,
   running or not.

The following figure shows the *OCNO* Jogging FSM.

.. drawio-figure:: pictures/ocno.drawio
   :format: jpg
   :page-name: ocno-jogging-fsm
   :align: center

   *OCNO* Jogging Finite State machine

*************
Usage Example
*************

The follow example shows how to use the *Jogging* mode of ``ocno``. It assumes that
``ocno`` has be loaded for a 5 axis milling machine, with a command similar to this
one:

..  code-block:: shell

    halcmd load ocno axes=xyzbc

It performs a simultaneous move of X, B and C joint, the others joints remaining
stationary.
The axis X does a move -10 relative to it current position, B moves to -45 (absolute)
and C moves to 180 (absolute).

.. Note::

    Each axis moves independently, their motion ends at different times.

.. Note::

    The units of the moved distances depends on the machine/drive configuration.

.. Note::

    The motion speed  of a joint is a parameter of the profile generator not
    ocno-jogging.

..  code-block:: shell

    # Select the axis to move
    halcmd ocno.jog.axisX.active 1
    halcmd ocno.jog.axisB.active 1
    halcmd ocno.jog.axisC.active 1

    # Set X as relative motion on X and B, C in  absolute motion.
    halcmd ocno.jog.axisX.relative-pos 1
    halcmd ocno.jog.axisC.relative-pos 0
    halcmd ocno.jog.axisB.relative-pos 0

    # Set the target positions
    halcmd ocno.jog.axisX.cmd-pos -10
    halcmd ocno.jog.axisB.cmd-pos -45
    halcmd ocno.jog.axisC.cmd-pos 180

    # Start the motion
    halcmd ocno.jog.start 1

    # Stop the motion
    halcmd ocno.jog.stop 1


The *Jogging* status can be checked by ``ocno.jog.running`` or  ``ocno.jog.axisN.is_running``
to get the status per axis.

**States description**

* **Init** : Set the mode of the axis to CSP mode. Also enable simple PG and
  update the position pin information. Ensure that the datapath of the mux is using
  Simple-PG target-position"
* **Wait Mode** : Check that the “devices” are in csp mode. When all the devices are
  in csp mode:

  * Spindle rotation required: Set the spindle speed and go to “wait spindle reach
    speed” state
  * Spindle rotation not required: jump to wait cmd state.

* **Wait spindle reach speed** : Wait for the spindle to reach the required RPM. The
  minimum speed is defined by the chosen spindle rotation speed threshold. If the
  user sets a new speed rotation value while the spindle is reaching it, the current
  value is overwritten to reach the new specified value.
* **Wait Cmd** : Wait the user command to start jog processing. If the spindle is
  enabled and the user changes the spindle speed rotation, not any command can be
  sent until the new spindle speed is not reached. in this mode, the user can set
  up the position of the axes, the offsets and what axis are selected to move.
  When a start is triggered, simple PG is enabled, for every selected axis:

    * Relative pos: set the targeted position with offset
    * Absolute pos:

		  * Axis not homed: nothing is set and go back to wait command
		  * Axis homed: set the targeted position with offset

	Finally, set the machine to running mode

* **Run** : Jog processing on-going. Waiting for all the axis to reach the targeted
  position. When it is done, set the machine to idle and go back to wait cmd state.

***********************
HAL Jogging global PINs
***********************

=============================  =========  =========  ============================================
Name                           Type       Direction  Description
=============================  =========  =========  ============================================
ocno.jog.start                 HAL_BIT    HAL_IN     Start jogging
ocno.jog.stop                  HAL_BIT    HAL_IN     Stop jogging
ocno.jog.enable                HAL_BIT    HAL_OUT    Used to enable or disable simple PG
ocno.jog.running               HAL_BIT    HAL_OUT    Machining is jogging
ocno.jog.target-spindle-speed  HAL_FLOAT  HAL_IN     Contains the spindle RPM aimed by the system
=============================  =========  =========  ============================================

*************************
HAL Jogging PINs per axis
*************************

===========================  =========  =========  =============================================
Name                         Type       Direction  Description
===========================  =========  =========  =============================================
ocno.jog.axisN.relative-pos  HAL_BIT    HAL_IN     If set to True, performs a relative-motion
                                                   according to 'cmd-pos', elsewhere performs
                                                   an absolute-motion
ocno.jog.axisN.active        HAL_BIT    HAL_IN     Select the axis for the next jog motion group
ocno.jog.axisN.cmd-pos       HAL_FLOAT  HAL_IN     Target position for axis N
ocno.jog.axisN.is_running    HAL_BIT    HAL_OUT    Jogging is ongoing on axis N
===========================  =========  =========  =============================================
