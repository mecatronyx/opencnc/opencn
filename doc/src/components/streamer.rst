.. _streamer:

========
Streamer
========

The streamer component reads a file containing some numerical values corresponding to a certain type (int, float, etc.).

Cfg property
------------

The number and format of values contained in the file read by the sampler is defined in the cfg property passed as 
argument to the application. Valid types are:
 
•  f,F   Floating point value
•  b,B   Bit
•  u,U   32-bit unsigned
•  s,S   32-bit signed

The number of letter in the cfg property corresponds to the number of value (separated by a white space at minimum). 
For example, ``cfg="ff"`` means two float values per line.

Depth property
--------------

The depth value is the number of values that can be placed in the FIFO associated to the stream.
