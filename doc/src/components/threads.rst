.. _threads:

=======
Threads
=======

The *threads* component is currently able to manage up to three independent
periodic threads (or tasks). Depending on the arguments passed to the application,
the threads are initialized during the component loading.

Each thread can have a different period.

Functions are then attached (exported) to these threads.