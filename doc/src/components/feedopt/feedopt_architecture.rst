.. _feedopt_architecture:

********************
Feedopt Architecture
********************

The following diagram introduces the different parts of the feedrate planning
algorithm. Both development and testing have been performed on MATLAB, while the
actual code on the machine is written in C/C++. To do so, a transcription of matlab
code into C/C++ has been realized using the code generator plugin named MATLAB Coder.

To prevent misunderstanding of which part is native C++ or MATLAB code, two colors
have been used in the figure below.

.. figure:: pictures/feedopt_overview.png
   :name: _feedopt_overview
   :align: center

   *Feedopt* Overview

The proposed algorithm used for the feedrate planning is composed of the following
operations :

#. Initialization : ( state : **Fopt_Init** )
	* Input : Structure of configuration which contains all the algorithms’ parameters.
	* Output : The context structure
	* Goal : Create the context structure and do the initialization.
	* Comments : The context structure is a key element in the algorithm pipeline.
	  This structure is read and written all along through the different
	  steps of the algorithm. Its subfield cfg contains all the configuration
	  parameters used in the algorithms.
#. Reading GCode : ( state : **Fopt_GCode** )
	* Input : GCode file + variable of environment which contains the path to the
	  table of tools used during the machining operations
	* Output : Queue ( custom class based on std::vector ) named q_gcode.
	* Goal : Read the Gcode passed as input and then create structures ( CurvStruct )
	  representing geometric curves with other information used in the next
	  operations.
	* Comments : All the supported G commands are listed in this table. The interpreter
	  is composed of several files written natively in C++ and others
	  which have been generated from Matlab. It is based on the open source
	  Gcode interpreter of LinuxCNC.
#. CheckCusp : ( state : **Fopt_Check** )
	* Input : The queue of curvStruct q_gcode
	* Output : The queue of curvStruct q_gcode ( The number of elements in the
	  queues does not change )
	* Goal : Try to identify a cusp ( a.k.a. A situation where an exact stop of
	  the tool is required : e.g. drilling )
	* Comments : The algorithm works on a batch of two successive curves. When the
	  detection of a cusp occurs a zero stop label is added to the curves, thus the
	  new queue is not required.
#. Compressing : ( state : **Fopt_Compress** )
	* Input : The queue of curvStruct q_gcode and the empty queue of the spline q_spline
	  ( read and write )
	* Output : The queue of curvStruct q_compress the resulting queue of the spline
	  ( read and write )
	* Goal : Aggregate small linear segments in a batch and then perform a spline
	  interpolation based on first and last points of each segment.
	* Comments : The algorithm first creates a batch of curvStruct and then performs
	  the interpolation on this batch. One spline is created for each
	  batch of curvStructs. The queue q_spline is used to store the geometric
	  information of the curve. Note that one spline curve may contain
	  a large set of data points compared to the other type of curves.
	  For this reason, a dummy curve named spline is stored in q_compress
	  while the actual real spline is stored in q_spline. Many dummy
	  splines can point to the same real spline, it makes sense for the
	  operation of splitting.
#. Smoothing : ( state : **Fopt_Smooth** )
	* Input : The queue of curvStruct q_compress + q_spline ( read only )
	* Output : The queue of curvStruct q_smooth + q_spline ( read only )
	* Goal : Ensure the curve to be smooth between each segment. A smooth transition
	  is added when possible and an exact stop is imposed in the other case.
	* Comments : The computations involve two following curves at the time.

#. Splitting : ( state : **Fopt_Split** )
	* Input :  The queue of curvStruct q_smooth + q_spline ( read only )
	* Output : The queue of curvStruct q_split + q_spline ( read only )
	* Goal : Ensure the length of each segment to stay close to a given value. The
	  curves are cut to reduce their size.
	* Comments : The computation is done one curve at the time. The number of resulting
	  curves may depend on the parameter of the splitting.
#. Optimization : ( state : **Fopt_Opt** )
	* Input : The queue of curvStruct q_split + q_spline ( read only )
	* Output : The queue of curvStruct q_opt + q_spline ( read only )
	* Goal : Solve the numerical optimization problem. The result of the optimization
	  is the vector containing the weights for the basis functions used to
	  represent the speed profile.
	* Comments : This operation requires at least two calls to the external solver.
	  The call of the second optimization can be only performed after
	  the first one ended. The optimization may lack robustness and might
	  not work depending on the parameters used during the previous operation.
#. Resampling : ( Not a state of the FSM )
	* Input : One optimized curvStruct ( a.k.a. one curve contained in q_opt ) +
	  q_spline ( read only )
	* Output : The resulting commands for the drives ( array of the size of the
	  number of drive )
	* Goal : The commands for the drives are computed through the discretization
	  of the speed profile obtained with the optimization.
	* Comments : This rate at which the drive commands are sent needs to match the
	  discretization time used during the resampling. This part has once
	  been performed in kernel space.
#. Kinematics : ( Not a state of the FSM )
	* Input : type of kinematics + kinematics parameters
	* Output : A library with the kinematics functions
	* Goal : Compute the kinematics transformation
	* Comments : The kinematics interface is coded in native C. The advanced kinematics
	  functions in C are generated from the Matlab.
