.. _feedopt:

=======
Feedopt
=======

.. toctree::
   :maxdepth: 5
   :hidden:

   feedopt_architecture


The component *Feedopt*, contraction of Feedrate and optimization, is responsible
for the path planning. Path planning concerns the generation of time optimal setpoint
values subject to constraints regarding speed, acceleration, jerk, and geometrical
tolerances w.r.t. the programmed path defined by a G-code file (RS274).

The approach used in the feedrate planning is composed of two stages : A suite of
mathematical operations without Real-Time (RT) constraints followed by the discretization
of the commands called resampling step. The second stage consists of sending the
commands to the motors’ drives at a fixed frequency.

Setpoints values are called a sample in this paragraph. It corresponds to a vector
of N dimensions where N is the number of axes.

This chapter describes the HAL part of *feedopt*. The other part of feedopt are
documented on the following chapters:

* :ref:`Algorithms <CNC_Path_Planning_Algorithms>`
* :ref:`Architecture <feedopt_architecture>`

Feedop can be load with the following call

.. code-block:: shell

    halcmd load feedopt --axes=<AXES>

Where:

* **axes**: A string with the machine axes. For example, for the *micro5* machine,
  axes parameter is ``--axes=xyzbc``.

.. note::

   The axes parameter is a standard Linux command line tool parameter. It can be
   also be used  without the '=' sign  (``--axes xyzbc``).
   The short form is ``-a``

.. note::

   On current implementation, *feedopt* only supports up to 6 axes (x, y, z, a,
   b & c).

The usage of Feedopt is quite simple: First send the wanted *G-Code* file to the
OpenCN target and then start the operations. The G-Code source path has to be
``/root/gcode.ngc``.

*Feedopt* performs 2 types of operations: samples generation (*Sample Generation*
process) and samples read and makes them available to the samples PINs (*Read Sample*
process).

The *Sample Generation* consists of the following phases: G-Code file reading,
geometric operations, feedrate planning, optimization and resampling. The explanation
of each phase is explained in the chapter `Architecture <feedopt_architecture>`.
It can be started by setting the ``feedopt.gen.start`` PIN. It is possible to pause
sample generation processing with the ``feedopt.resampling.pause`` PIN.

The *Read Sample* process consists of fetching samples from the sample buffer
periodically and to make it available to the sample PINs. It can be started by
setting the ``feedopt.read.start`` PIN. It is also possible to just fetch one sample
by setting ``feedopt.read.single`` PIN when the *Read Sample** process has not been
started. The PIN feedop.read.stop stops the Read Sample process.

The samples are fetched at the thread period on which Feedopt HAL function has been
added. If the buffer is empty (no sample available), Feedopt raises the
``feedopt.sample.underrun`` PIN. In this case the *Read Sample* process is stopped.

The ``feedopt.gen.active`` and ``feedopt.read.active`` PINs indicate if the related
process is active or not.

Feedopt operations can be reseted by setting ``feedopt.reset`` PIN. Both processes,
*Sample Generation* and *Read Sample*, are reseted.

It is possible to change some of the feedrate planning and geometric operations
parameters by updating the corresponding HAL parameter. The :ref:`table <feedopt_params>`
provides the full list of the available parameters.

*****************
Feedrate Override
*****************

The bottleneck of the approach stays in the way the communication is implemented
between the stage for sample generation and the one for reading and sending the
commands to the motors’ drive. A queue of samples is used for this communication.
This queue is populated with the time optimal solutions resulting from the mathematical
optimization whose processing time highly increases with respect to a choice of
large parameters (NDiscr, NHorz, Nbreak) and to the complexity of the geometry of
the curve. On the other hand, the elements of the queue are popped out at a fixed
frequency in a HAL thread.

Therefore, it is possible that an underrun of the queue occurs during the machining
time. An elegant solution to tackle this problem is to reduce the feed of the tool.
An override of the optimal feedrate is thus realized based on a low-pass filter to
ensure a smooth transition.

There is 2 types of overrides to decrease the real feedrate :

* Manual override: The user can define the override pourcentage.
  Its value can be set using ``feedopt.resampling.manual-override`` PIN
* Automatic: an automatic override percentage is applied when the resampling process
  is about to consume all the optimized curves present in the queue. The current
  value of this auto override can be read from feedopt.resampling.auto-override PARAM.
  This option cannot be disabled, in order to preserve the robustness of the machining process.

The automatic and manual override factors are multiplied together to get the applied
override value. This combination is bounded between 0 and 1.

It is quite relevant to notice that the current implementation lacks reactivity.
Actually, the manual override becomes only perceptible or active once all the previous
samples in the current queue have been sent to the motor.

Feedopt operations can be reseted by ``setting feedopt.reset PIN``. Both processes,
*Sample Generation* and *Read Sample*, are stopped and moved back to their initial
state.

*****************
HAL PINS & PARAMs
*****************

PINs

   ==================================  =========  ========= =============================================
   Name                                Type       Direction Description
   ==================================  =========  ========= =============================================
   feedopt.spindle-target-speed        HAL_FLOAT  HAL_OUT   Target speed for the spindle
   feedopt.read.active                 HAL_BIT    HAL_OUT   Read Sample process is active
   feedopt.gen.active                  HAL_BIT    HAL_IN    Sample Generation process is active
   feedopt.read.single                 HAL_BIT    HAL_IN    Read one sample when feedopt, *Read Sample*
                                                            process, is inactive
   feedopt.gen.start                   HAL_BIT    HAL_IN    Start *Sample Generation* process
   feedopt.sampling-period-ns          HAL_U32    HAL_IN    Sampling period in [ ns ]
   feedopt.ready                       HAL_BIT    HAL_OUT   Inform that feedopt ready
   feedopt.finished                    HAL_BIT    HAL_OUT   All the samples have been read / computed
   feedopt.read.start                  HAL_BIT    HAL_IN    Start *Read Sample* process
   feedopt.reset                       HAL_BIT    HAL_IN    Reset feedopt operations. After a reset,
                                                            Feedopt is ready to restart operations
   feedopt.optimising.count            HAL_S32    HAL_IN    Number of curves (curvstruct) after the
   feedopt.optimising.progress         HAL_S32    HAL_IN    Number of curvstruct which has been optimized
   feedopt.resampling.progress         HAL_S32    HAL_OUT   Number of curves which has been resampled
                                                            optimization process
   feedopt.sample.number               HAL_U32    HAL_OUT   Current number of samples (samples present
                                                            in the sample queue)
   feedopt.resampling.pause            HAL_BIT    HAL_IN    Pause resampling
   feedopt.resampling.gcodeline        HAL_S32    HAL_OUT   Current G-code line
   feedopt.resampling.manual-override  HAL_FLOAT  HAL_IN    Manual override factor ( 1: means no
                                                            override )
   feedopt.sample.underrun             HAL_BIT    HAL_OUT   The sample buffer is empty during *Sample
                                                            Read* process
   ==================================  =========  ========= =============================================

PINs per axis

   ====================  =========  =========  ==========================
   Name                  Type       Direction  Description
   ====================  =========  =========  ==========================
   feedopt.axisN.sample  HAL_FLOAT  HAL_OUT    Position sample for axis N
   ====================  =========  =========  ==========================

.. _feedopt_params:

PARAMs

   ================================  =========    =========    ============================================================================================
   Name                              Type         Direction    Description
   ================================  =========    =========    ============================================================================================
   feedopt.nhorz                     HAL_U32       HAL_RW      Number of curve pieces being simultaneously
                                                               optimized regarding feedrate in the receding
                                                               horizon (look-ahead)
   feedopt.ndiscr                    HAL_U32       HAL_RW      Number of grid points for the discretization
                                                               of acceleration constraints
   feedopt.nbreak                    HAL_U32       HAL_RW      Number of break points for the scalar B-Spline
                                                               q(u)
   feedopt.lsplit                    HAL_FLOAT     HAL_RW      Maximum length of each CurvStruct used for
                                                               Splitting, in mm
   feedopt.cut_off                   HAL_FLOAT     HAL_RW      CutOff length for transitions between curve
                                                               pieces, in mm
   feedopt.resampling.auto-override  HAL_FLOAT     HAL_RO      Current automatic override factor (1: means no
                                                               override)
   feedopt.split-special-spline        HAL_BIT     HAL_RW      Activate on interpolated 5 axis motion or when G01 has short length. 
                                                               Help to reduce the slowdown on curve. 
                                                               This option make the queue longer and the target may run out of ressource depending on the
                                                               G-Code length.
   feedopt.debug-print                 HAL_BIT     HAL_RW      Prompt on target the current state of the path
                                                               planning. This option may slowdown a 
                                                               bit the calculation.
   ================================  =========    =========    ============================================================================================

Detail information on the algorithm and its param can be found:

* :ref:`Geometric parameters <Geometrical_Operations>`
* :ref:`Feed Rate Planing parameters  <Feedrate_Planning>`
* :ref:`Algorithms parameters  <Matlab_documentation>`

PARAMs per axis

   ==================  =========  =========  ===============================
   Name                Type       Direction  Description
   ==================  =========  =========  ===============================
   feedopt.axisN.vmax  HAL_FLOAT  HAL_RW     Maximum Velocity for axis N
   feedopt.axisN.amax  HAL_FLOAT  HAL_RW     Maximum acceleration for axis N
   feedopt.axisN.jmax  HAL_FLOAT  HAL_RW     Maximum jerk for axis N
   ==================  =========  =========  ===============================

