.. _architecture:

###################
OpenCN Architecture
###################

.. toctree::
   :maxdepth: 5
   :hidden:
   
   hal
   
A key feature of the OpenCN kernel organization is its Asymmetric Multi-Processing (AMP) approach.
OpenCN requires to run on four CPUs (quad-core) and each core (CPU) has a dedicated function.

For example, ``CPU #0`` is for the general purpose activities such as the shell, utilities, possibly a GUI, etc.
While ``CPU #1`` is reserved for strict (hard) realtime activities **without any** interactions with Linux scheduling
or other deferred activities (namely softirqs).
``CPU #2`` and ``CPU #3`` are reserved for (Linux scheduled) SMP multi-task activities such as optimization 
(mathematical) algorithms.
 

.. figure:: /pictures/opencn_architecture_v2021_1.png
   :align: center
   :scale: 70 %

   General architecture of OpenCN(-AMP)

The ``AVZ`` hypervisor is a very small component under the Linux kernel and enables efficient interactions
between RT and non-RT CPUs. 

OpenCN borrows the concept of **components** from **LinuxCNC**, enabling
the communication between them using **pins**. Connecting two or more
pins creates a **signal**.


- :ref:`Hardware Abstraction Layer (HAL) <hal>`


