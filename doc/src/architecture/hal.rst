
.. _hal:


################################
Hardware Abstraction Layer (HAL)
################################


HAL concept comes from *linuxCNC* project. This documentation is also derivated from
`linuxCNC documentation <http://linuxcnc.org/docs/html/hal/intro.html>`_.

HAL stands for *Hardware Abstraction Layer*. At the highest level, it is simply a
way to allow a number of *building blocks* (called components) to be loaded and
interconnected to assemble a complex system.

HAL is based on the same principles that are used to design hardware circuits and
systems, so it is useful to examine those principles first.

Any system (including a CNC machine), consists of interconnected components. For
the CNC machine, those components might be the drives, motors, encoders, etc...
The machine builder must select, mount and wire these pieces together to make a
complete system.

************
Startup
************

By default, a HAL configuration is launched at startup by ``/etc/profile.d/start_opencn.sh`` on the target.

************
HAL Concepts
************

This section defines key HAL terms

**Component**

    A HAL component is a piece of software with well-defined inputs, outputs, and
    behavior, that can be loaded and interconnected as needed.

**Pin**

	Pins are the inputs and the outputs of the *components*.

**Parameter**

	Parameters are used to configure *components* or . They can be seen as trim pots

**Signal**

	Signals are the equivalent of a wire. They are used to connect pins together

**Type**

	A pin can be of one of the following types:

        * bit - a single TRUE/FALSE or ON/OFF value
        * float - a 64 bit floating point value, with approximately 53 bits of resolution
          and over 1000 bits of dynamic range.
        * u32 - a 32 bit unsigned integer, legal values are 0 to 4,294,967,295
        * s32 - a 32 bit signed integer, legal values are -2,147,483,647 to +2,147,483,647

**Function**

	Components export or more functions. These functions perform components specific
	actions. Functions should be added to a thread to be executed.

**Thread**

    A thread is a list of functions that runs at specific intervals. When a thread
    is first created, it has a specific time interval (period), but no functions.
    Functions can be added to the thread, and will be executed in order every time
    the thread runs.

***********
halcmd tool
***********

``halcmd`` is used to manipulate the HAL from the command line. ``halcmd`` can
optionally read commands from a file, allowing complex HAL configurations to be
setup with a single command.

Usage
=====

.. code-block:: shell

	halcmd [OPTIONS] [COMMAND [ARG]]

Options
=======

-f [file]

	Take the inputs from the file ``file``. If ``file`` is not specified, take the
	inputs from ``stdin``.

-h [command]

	Display a help message and exit. Display extended help on command if specified


Commands
========

Commands tell ``halcmd`` what to do. Normally ``halcmd`` reads a single command
from the command line and executes it. If the ``-f`` option is used, ``halcmd``
reads each line of the file as a new command.

.. note::

	``halcmd`` supports command completion. the completion of the parameters, when
	a component is about to be loaded, is not supported.

**load MODULE_NAME**

	 Load a HAL module called ``MODULE_NAME``.

**setp name value**

	Set the value of parameter or pin name to value. It fails in the following
	cases:

	* name does not exist as a pin or parameter,
	* parameter if read only
	* PIN is an output
	* PIN is attached to a signal,
	* Value is not a legal value. Legal values depend on the type of the PIN or
	  parameter.

	If a PIN and a parameter both have the same name, the command affects the
	parameter.

**getp name**

	Get the value of parameter or pin name. It fails if the name does not exist
	as a PIN or parameter.
	If a PIN and a parameter both have the same name, the command affects the
	parameter.


**sets name value**

	Set the value of signal ``name`` to value. Fails if ``name`` does not exist
	or if it already has a writer.


**gets name**

	Get the value of signal ``name``. Fail if ``name`` does not exist.


**net signame pin1 [arrow] pin2**

	Create ``signname`` which connects ``pin1`` with ``pin2``. ``arrow`` can be
	``=>``, ``<=``, ``<=>`` or omitted. ``halcmd`` ignores arrows, but they can be
	useful in command files to document the direction of data flow. Arrows should
	not be used on the command line since the shell might try to interpret them.

**start**

	Start execution of realtime threads. Each thread periodically calls all of
	the functions that were added to it with the ``addf`` command, in the order
	in which they were added.

**stop**

	Stop execution of realtime threads. The threads will no longer call their
	functions.

**show [item]**

	Print HAL items to stdout in human readable format. Item can be one of
	``comp`` (components), ``pin``, ``sig`` (signals), ``param`` (parameters),
	``funct`` (functions), ``thread``, or ``alias``. The item ``all`` can be used
	to show all the preceding types. If item is omitted, show will print everything.

**addf FUNC_NAME THREAD_NAME**

	Add function ``FUNC_NAME`` to thread ``THREAD_NAME``. ``FUNC_NAME`` will run
	after any functions that were previously added to the thread.


**list [type]**

	Prints the names of HAL items of the specified type. ``type`` is ``comp``,
	``pin``, ``sig``, ``param``, ``funct``, or ``thread``.
	Names are printed on a single line, space separated.


Examples
========

Here are some examples on how to use the ``halcmd`` tool.

* Module loading:
	 ``halcmd load <MODULE_NAME> [PARAMS]``
* Thread creation:
	``halcmd load threads name1=<THREAD_NAME> period1=<period>``
* Add function to a thread:
	``halcmd addf <FUNC_NAME> <THREAD_NAME>``
* Start / stop hal execution:
	``halcmd start``

	``halcmd stop``
* Set PIN value:
	``halcmd setp <PIN NAME> VALUE``
* Read PIN value:
	``halcmd show pin``


It is possible to create a script with HAL commands and to call with the ``halcmd``
script. By convention, a HAL script use ``hal`` as extension.

* call a HAL script:
	``halcmd -f <HAL_SCRIPT>``

* HAL script example :

.. code-block:: shell

	load loopback cfg=fsub
	load threads name1=loopback_thread period1=20000
	addf loopback.0 loopback_thread
	setp loopback.0.f_in.0 12.3
	show pin loopback






