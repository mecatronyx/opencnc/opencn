/*
 * Copyright (C) 2014-2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef UAPI_SAMPLER_H
#define UAPI_SAMPLER_H

#include <opencn/uapi/hal.h>

#define SAMPLER_DEV_NAME	"/dev/opencn/sampler/0"
#define SAMPLER_DEV_MAJOR	103

/* Sampler component */
#define SAMPLER_DEV_MINOR	0

#define SAMPLER_COMP_NAME "sampler"
#define SAMPLER_DEFAULT_NAME "0"
#define SAMPLER_DEF_LOGPATH "/root/"			/* Default path for logfile */
#define SAMPLER_MAX_INSTANCE		8			/* Maximum number of sampler instance */
#define SAMPLER_MAX_PINS    60  				/* Maximum number of recordable pin / Increase stack size if increased */
#define SAMPLER_NAME_MAX_LENGTH HAL_NAME_LEN-16 /* "sampler.<name>.pin.999" is 16 char so HAL_NAME_LEN-16=31  */
#define SAMPLER_DEFAULT_DEPTH "1000"			/* Default depth of the ring buffer between userspace and RT */
#define SAMPER_WARN_TIME 3.0					/* Time between warning (in seconds) between potential lost frame */
#define SAMPLER_PATH_MAX 256					/* Maximum length for path of logfile */

/*
 * IOCTL codes
 */
#define SAMPLER_IOCTL_CONNECT		_IOW(0x05000000, 0, char)
#define SAMPLER_IOCTL_DISCONNECT	_IOW(0x05000000, 1, char)
#define SAMPLER_IOCTL_INSTANCE_NR  	_IOW(0x05000000, 2, char)

typedef struct {
	int depth; 								/* Depth of ring buffer */
    char cfg[SAMPLER_MAX_PINS + 1]; 		/* Pin type that is sampled */
    bool memory; 							/* Memory mode */
    char name[SAMPLER_NAME_MAX_LENGTH + 1]; /* Name of the sampler.<name> */
	char path[SAMPLER_PATH_MAX];			/* Path where output logfile is stored */ 

	int instance_nr;						/* Instance number */
	long channel;							/* HAL channel */
	
	bool tag;								/* Put sample number in first column */
	long int nb_samples; 					/* Number of sample that will be read, -1 means forever*/
	int nb_pins;							/* Number of pin that will be recorded */
	bool timeusrloop;						/* Do we time the user writer loop */
} sampler_connect_args_t;

typedef struct {
	hal_type_t type;
	union {
	double f;
	uint32_t u;
	int32_t s;
	bool b;
	};
} sampler_value_t;

/* Sample registered*/
typedef struct {
	unsigned long n_tag;
	int n_pins;
	sampler_value_t pins[SAMPLER_MAX_PINS];
} sampler_sample_t;

/* Ring buffer of sampler */
typedef struct {
	int head, tail;
	sampler_sample_t *sampler_data;
	int capacity;
	volatile int size;
} samp_rg_t;


typedef struct {
    int instance_nr;
} sampler_instance_nr_args_t;


/**
 * @brief Pop available sample of ring buffer
 * @param samp_rg_t* : The sampler ring buffer
 * @param sampler_sample_t* : Value of the samples
 * @return Number of availabe sample in <value>
 */
int samp_rg_pop(samp_rg_t *rg, sampler_sample_t *value);


#endif /* UAPI_SAMPLER_H */
