/*
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 */

#ifndef UAPI_MUX_H
#define UAPI_MUX_H

#ifndef __KERNEL__
#include <af.h>
#endif

#include <opencn/uapi/hal.h>

#define MUX_COMP_NAME  "mux"
#define MUX_DEV_NAME   (OPENCN_DEV_PATH MUX_COMP_NAME)

/*
 * IOCTL codes
 */
#define MUX_IOCTL_CONNECT      _IOW(0x05000000, 0, char)
#define MUX_IOCTL_DISCONNECT   _IOW(0x05000000, 1, char)
#define MUX_IOCTL_INSTANCE_NR  _IOW(0x05000000, 2, char)

/* Default 'mux' name - it will converted to the interger (mux number) */
#define MUX_DEFAULT_NAME    "default"

/* Default (minimum) vector sizes */
#define MUX_DEFAULT_VECTOR_NR   2
#define MUX_DEFAULT_VECTOR_SIZE 1

/* MUX name size - remove 4 char: 'mux' + '.' */
#define MUX_NAME_SIZE_MAX   (HAL_NAME_LEN - 4)

typedef struct {
    char name[MUX_NAME_SIZE_MAX];
    int type;
    int vector_nr;
    int vector_size;
} mux_connect_args_t;


typedef struct {
    int instance_nr;
} mux_instance_nr_args_t;

#endif /* UAPI_MUX_H */