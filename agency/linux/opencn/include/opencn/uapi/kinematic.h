/*
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 */

#ifndef UAPI_KINEMATIC_H
#define UAPI_KINEMATIC_H

#include <opencn/uapi/hal.h>

#define KIN_COMP_NAME  "kinematic"

#ifndef __KERNEL__
#include <af.h>
#define KIN_DEV_NAME   (OPENCN_DEV_PATH KIN_COMP_NAME)
#endif


#define KIN_PARAM_NR  32

/* Current version only support up to 6 joints */
#define KIN_MAX_JOINTS 6

/*
 * IOCTL codes
 */
#define KIN_IOCTL_CONNECT      _IOW(0x05000000, 0, char)
#define KIN_IOCTL_DISCONNECT   _IOW(0x05000000, 1, char)
#define KIN_IOCTL_FORWARD      _IOW(0x05000000, 2, char)
#define KIN_IOCTL_INVERSE      _IOW(0x05000000, 3, char)

enum {
    KIN_TYPE_TRIVIAL = 0,
    KIN_TYPE_COREXY,
    KIN_TYPE_XYZBC,
    KIN_TYPE_END,
    KIN_TYPE_NO_INITIALIZED,
};

typedef struct {
    unsigned type_id;
    char type_name[HAL_NAME_LEN];
} kin_type_t;


/* IOCTL connect parameters
*/
typedef struct {
    int type;
    double params[KIN_PARAM_NR];
    unsigned enable_pin;
} kin_connect_args_t;


/* IOCTL 'forward' & 'inverse' args, used in the libkin
*/
typedef struct {
    double joint[KIN_MAX_JOINTS];
    double world[KIN_MAX_JOINTS];
} kin_lib_args_t;

#ifdef __cplusplus
extern "C" {
#endif

/* == Functions to be used by Matlab only == */
int kin_set_type(int type);

int kin_get_type(void);

int kin_set_params(double *params);
int kin_get_params(double *params);

/* == Functions to use in Matlab and OpenCN == */

/* kinematic forward */
void kin_forward(double *world, const double *joint);

/* kinematic inverse */
void kin_inverse(const double *world, double *joint);

#ifdef __cplusplus
} // extern C
#endif


#endif /* UAPI_KINEMATIC_H */
