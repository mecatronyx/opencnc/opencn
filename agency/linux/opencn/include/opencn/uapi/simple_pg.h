/*
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 */

#ifndef UAPI_SIMPLE_PG_H
#define UAPI_SIMPLE_PG_H

#include <opencn/uapi/hal.h>

#define SIMPLE_PG_COMP_NAME  "simple_pg"

#ifndef __KERNEL__
#include <af.h>
#define SIMPLE_PG_DEV_NAME   (OPENCN_DEV_PATH SIMPLE_PG_COMP_NAME)
#endif

/*
 * IOCTL codes
 */
#define SIMPLE_PG_IOCTL_CONNECT      _IOW(0x05000000, 0, char)
#define SIMPLE_PG_IOCTL_DISCONNECT   _IOW(0x05000000, 1, char)

 /* IOCTL connect parameters
*/
typedef struct {
    int joint_nr;
} simple_pg_connect_args_t;

#endif /* UAPI_SIMPLE_PG_H */