/*
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 */

#ifndef UAPI_PLC_H
#define UAPI_PLC_H

#ifndef __KERNEL__
#include <af.h>
#endif

#include <opencn/uapi/hal.h>

#define PLC_COMP_NAME  "plc"
#define PLC_DEV_NAME   (OPENCN_DEV_PATH PLC_COMP_NAME)

/*
 * IOCTL codes
 */
#define PLC_IOCTL_CONNECT      _IOW(0x05000000, 0, char)
#define PLC_IOCTL_DISCONNECT   _IOW(0x05000000, 1, char)

enum {
    PLC_CFG_MICRO3 = 0,
    PLC_CFG_MICRO5,
    PLC_CFG_SPINDLE,
    PLC_CFG_COREXY,
    PLC_CFG_DEMONSTRATOR,
    PLC_CFG_OCNO_DBG,
    PLC_CFG_END,
};


typedef struct {
    unsigned cfg_id;
    char cfg_name[HAL_NAME_LEN];
} plc_cfg_t;


typedef struct {
    char name[HAL_NAME_LEN];
    int  id;
} plc_connect_args_t;



#endif /* UAPI_PLC_H */
