/*
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2023 Xavier Soltner <xavier.soltner@heig-vd.ch>
 *
 */

#ifndef UAPI_SPINDLE_H
#define UAPI_SPINDLE_H

#ifndef __KERNEL__
#include <af.h>
#endif

#include <opencn/uapi/hal.h>

#define SPINDLE_COMP_NAME  "spindle"
#define SPINDLE_DEV_NAME   (OPENCN_DEV_PATH SPINDLE_COMP_NAME)


#define SPINDLE_DEV_MAJOR	110
#define SPINDLE_DEV_MINOR	0
#define SPINDLE_CFG_MAX_LENGTH 100

/*
 * IOCTL codes
 */
#define SPINDLE_IOCTL_CONNECT		_IOW(0x05000000, 0, char)
#define SPINDLE_IOCTL_DISCONNECT	_IOW(0x05000000, 1, char)
#define SPINDLE_IOCTL_INSTANCE_NR  	_IOW(0x05000000, 2, char)

#define SPINDLE_NAME_MAX_LENGTH HAL_NAME_LEN-27     /* "spindle..velocity.tolerance" is the longest pin/param length of 27 character*/
#define SPINDLE_ZERO_SPEED_THRESH 10.0              /* in [turn/min] */
#define SPINDLE_VEL_TOL 5.0                         /* Default velocity tolerance in [%]*/
#define SPINDLE_TEMP_TOL 0.1                        /* Default tolerance of temperature regulation in [°C]*/

/* Spindle Temperature */
enum {
    SPINDLE_TEMP_BELOW_MIN,
    SPINDLE_TEMP_OK,
    SPINDLE_TEMP_OVERHEAT,
};


/* Configuration */
enum {
    SPINDLE_CFG_STD,
    SPINDLE_CFG_ATC,
    SPINDLE_CFG_UNKNOWN
};

/* Regulation type */
enum {
    SPINDLE_REGULATION_LINEAR,
    SPINDLE_REGULATON_POLY3,
    SPINDLE_REGULATION_UNKNOWN,
};

/* Spindle speed state */
typedef enum {
    SPINDLE_SPEED_NOT,
    SPINDLE_SPEED_SLOW,
    SPINDLE_SPEED_NOMINAL,
}Spindle_speed_state_t;

/* Spindle State */
typedef enum {
    SPINDLE_STATE_INACTIVE,
    SPINDLE_STATE_ENABLED,
    SPINDLE_STATE_ACTIVE,
    SPINDLE_STATE_TRANSITION,
    SPINDLE_STATE_FAULT,
    SPINDLE_STATE_FAULT_RESET_WAIT,
}Spindle_elec_state_t;

/* Transition flags*/
enum {
    SPINDLE_TRANS_NOMINAL,
    SPINDLE_TRANS_STOP,
};

typedef struct {
    uint16_t cfg_id;                        /* Configuration type */
    char name[SPINDLE_NAME_MAX_LENGTH + 1]; /* Name of the spindle.<name> */
    bool has_temperature;                   /* Spindle has a temperature monitoring*/
    bool has_cooling;                       /* Spindle has a cooling */
    int instance_nr;                        /* Instance number */
} spindle_connect_args_t;


typedef struct {
    int instance_nr;
} spindle_instance_nr_args_t;

#endif /* UAPI_SPINDLE_H */
