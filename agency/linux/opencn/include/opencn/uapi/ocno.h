/*
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 */

#ifndef UAPI_OCNO_H
#define UAPI_OCNO_H

#ifndef __KERNEL__
#include <af.h>
#endif

#include <opencn/uapi/hal.h>

#define OCNO_COMP_NAME  "ocno"
#define OCNO_DEV_NAME   (OPENCN_DEV_PATH OCNO_COMP_NAME)

/*
 * IOCTL codes
 */
#define OCNO_IOCTL_CONNECT      _IOW(0x05000000, 0, char)
#define OCNO_IOCTL_DISCONNECT   _IOW(0x05000000, 1, char)

typedef struct {
    char axes[HAL_NAME_LEN];
} ocno_connect_args_t;

#endif /* UAPI_OCNO_H */
