/********************************************************************
 *  Copyright (C) 2019 Peter Lichard <peter.lichard@heig-vd.ch>
 *  Copyright (C) 2019 Jean-Pierre Miceli Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

#ifndef FEEDOPT_PRIV_H
#define FEEDOPT_PRIV_H

#include <opencn/uapi/feedopt.h>

typedef enum {
	FEEDOPT_STATE_INACTIVE,
	FEEDOPT_STATE_RUNNING,
} FEEDOPT_STATE;

typedef struct {
	int head, tail;
	feedopt_sample_t *feedopt_data;
	int capacity;
	volatile int size;
} fopt_rg_t;

typedef struct {
	/* Axis PIN */
	hal_float_t *sample_pin;

	/* Axis PARAMs */
	hal_float_t vmax;
	hal_float_t amax;
	hal_float_t jmax;

} feedopt_axis_t;


/* this structure contains the HAL shared memory feedopt_data for the component
 */
typedef struct {
	/* PINs */
	hal_float_t *const spindle_speed_out;
	hal_bit_t *const read_active, *const read_single, *const gen_active;
	hal_bit_t *const ready_out, *const finished_out, *const underrun_out;
	hal_bit_t *const gen_start, *const reset_pin;
	hal_u32_t *const sample_number;
	hal_float_t *const manual_override;
	hal_bit_t *const read_start;
	hal_bit_t *const read_stop;
	hal_u32_t *const sampling_period_ns;

	hal_s32_t *const us_optimising_count;
	hal_s32_t *const us_optimising_progress;
	hal_s32_t *const rt_resampling_progress;
	hal_bit_t *const us_resampling_pause;
	hal_s32_t *const current_gcode_line;

	/* PARAMs */
	hal_u32_t   nhorz;
	hal_u32_t   ndiscr;
	hal_u32_t   nbreak;
	hal_float_t lsplit;
	hal_float_t cut_off;
	hal_float_t auto_override;
	hal_bit_t split_special_spline;

	/* Axis PINs & PARAMs */
	feedopt_axis_t * hal_axes;

	/* Specific print */
	hal_bit_t debug_print;

} feedopt_hal_t;

typedef struct {
	/* PINs & PARAMs */
	feedopt_hal_t * hal_data;

	/* Axis infos */
	char     axes[HAL_NAME_LEN];
	unsigned axis_nr;

	unsigned reset;
} feedopt_data_t;

void feedopt_reset(feedopt_hal_t *fopt_hal);

#endif /* FEEDOPT_PRIV_H */


