// SPDX-License-Identifier: GPL-2.0-only

/*
 * Copyright (c) 2019-2020 Peter Lichard (peter.lichard@heig-vd.ch)
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 */

/**
 * This file, 'feedopt.c', is the realtime part of feedopt.
 */

#include <linux/ctype.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/uaccess.h>

#ifdef CONFIG_ARM
#include <asm/neon.h>
#endif

#ifdef CONFIG_X86
#include <asm/fpu/api.h>
#endif

#include <opencn/hal/hal.h>			/* HAL public API decls */
#include <opencn/rtapi/rtapi.h>		/* RTAPI realtime OS API */
#include <opencn/rtapi/rtapi_app.h> /* RTAPI realtime module decls */

#include <opencn/components/feedopt.h>

#include <opencn/ctypes/strings.h>

#include <opencn/strtox.h>

#include <opencn/rtapi/rtapi_errno.h>
#include <opencn/rtapi/rtapi_math.h>
#include <opencn/rtapi/rtapi_string.h>

static void fopt_rg_alloc(fopt_rg_t *rg, int capacity)
{
	rg->head = 0;
	rg->tail = 0;
	rg->capacity = capacity;
	rg->size = 0;
	rg->feedopt_data = kmalloc(sizeof(feedopt_sample_t) * capacity, GFP_ATOMIC);
}

static void fopt_rg_free(fopt_rg_t *rg)
{
	rg->capacity = 0;
	rg->size = 0;
	rg->tail = 0;
	rg->head = 0;
	kfree(rg->feedopt_data);
}

static int fopt_rg_push(fopt_rg_t *rg, feedopt_sample_t value)
{
	if (rg->size < rg->capacity) {
		rg->feedopt_data[rg->head++] = value;
		__sync_fetch_and_add(&rg->size, 1);
		if (rg->head == rg->capacity)
			rg->head = 0;
		return 1;
	}
	return 0;
}


static void fopt_rg_clear(fopt_rg_t *rg)
{
	rg->size = 0;
	rg->head = 0;
	rg->tail = 0;
}

/***********************************************************************
 *                STRUCTURES AND GLOBAL VARIABLES                       *
 ************************************************************************/

/* other globals */
static int comp_id; /* component ID */

#ifdef OPENCN_FORCE_UNDERRUN
static int debug_count = 0;
#endif

fopt_rg_t samples_queue;
feedopt_sample_t current_sample;

static const hal_pindesc_t feedopt_axis_pins[] = {

	{ HAL_FLOAT, HAL_OUT,  offsetof(feedopt_axis_t, sample_pin), "%s.axis%c.sample" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t feedopt_pins[] = {
	{ HAL_FLOAT, HAL_OUT, offsetof(feedopt_hal_t, spindle_speed_out),     "%s.spindle-target-speed" },
	{ HAL_BIT,   HAL_OUT, offsetof(feedopt_hal_t, read_active),           "%s.read.active" },
	{ HAL_BIT,   HAL_OUT, offsetof(feedopt_hal_t, gen_active),            "%s.gen.active" },
	{ HAL_BIT,   HAL_IN,  offsetof(feedopt_hal_t, read_single),           "%s.read.single" },
	{ HAL_BIT,   HAL_IN,  offsetof(feedopt_hal_t, reset_pin),             "%s.reset" },
	{ HAL_BIT,   HAL_IN,  offsetof(feedopt_hal_t, gen_start),              "%s.gen.start" },
	{ HAL_U32,   HAL_OUT, offsetof(feedopt_hal_t, sample_number),          "%s.sample.number" },
	{ HAL_U32,   HAL_IN,  offsetof(feedopt_hal_t, sampling_period_ns),     "%s.sampling-period-ns" },
	{ HAL_BIT,   HAL_OUT, offsetof(feedopt_hal_t, ready_out),              "%s.ready" },
	{ HAL_BIT,   HAL_OUT, offsetof(feedopt_hal_t, finished_out),           "%s.finished" },
	{ HAL_BIT,   HAL_OUT, offsetof(feedopt_hal_t, underrun_out),           "%s.sample.underrun" },
	{ HAL_BIT,   HAL_IN,  offsetof(feedopt_hal_t, read_start),             "%s.read.start" },
	{ HAL_BIT,   HAL_IN,  offsetof(feedopt_hal_t, read_stop),              "%s.read.stop" },
	{ HAL_BIT,   HAL_IN,  offsetof(feedopt_hal_t, us_resampling_pause),    "%s.resampling.pause" },
	{ HAL_FLOAT, HAL_IN,  offsetof(feedopt_hal_t, manual_override),        "%s.resampling.manual-override" },
	{ HAL_S32,   HAL_IN,  offsetof(feedopt_hal_t, us_optimising_progress), "%s.optimising.progress" },
	{ HAL_S32,   HAL_IN,  offsetof(feedopt_hal_t, us_optimising_count),    "%s.optimising.count" },
	{ HAL_S32,   HAL_OUT, offsetof(feedopt_hal_t, rt_resampling_progress), "%s.resampling.progress" },
	{ HAL_S32,   HAL_OUT, offsetof(feedopt_hal_t, current_gcode_line),     "%s.resampling.gcodeline" },

    { HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t feedopt_axis_params[] = {
	/* Feedrate Planning  */
	{ HAL_FLOAT, HAL_RW, offsetof(feedopt_axis_t, vmax), "%s.axis%c.vmax" },
	{ HAL_FLOAT, HAL_RW, offsetof(feedopt_axis_t, amax), "%s.axis%c.amax" },
	{ HAL_FLOAT, HAL_RW, offsetof(feedopt_axis_t, jmax), "%s.axis%c.jmax" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t feedopt_params[] = {
	/* Feedrate Planning  */
	{ HAL_U32, HAL_RW, offsetof(feedopt_hal_t, nhorz), "%s.nhorz" },
	{ HAL_U32, HAL_RW, offsetof(feedopt_hal_t, ndiscr), "%s.ndiscr" },
	{ HAL_U32, HAL_RW, offsetof(feedopt_hal_t, nbreak), "%s.nbreak" },

	/* Geometric operations */
	{ HAL_FLOAT, HAL_RW, offsetof(feedopt_hal_t, lsplit), "%s.lsplit" },
	{ HAL_FLOAT, HAL_RW, offsetof(feedopt_hal_t, cut_off), "%s.cut_off" },
	{ HAL_BIT, HAL_RW, offsetof(feedopt_hal_t, split_special_spline), "%s.split-special-spline" },

	/* Resampling */
	{ HAL_FLOAT, HAL_RO, offsetof(feedopt_hal_t, auto_override),  "%s.resampling.auto-override" },

	/* Specific print */
	{ HAL_BIT, HAL_RW, offsetof(feedopt_hal_t, debug_print), "%s.debug-print" },
	

    { HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

FEEDOPT_STATE state = FEEDOPT_STATE_INACTIVE;

/***********************************************************************
 *                  LOCAL FUNCTION DECLARATIONS                         *
 ************************************************************************/

static int feedopt_init(feedopt_connect_args_t *args, void **private_data);
static void feedopt_update(void *arg, long period);

extern void feedopt_update_fp(feedopt_data_t *data, FEEDOPT_STATE state, long period);

/***********************************************************************
 *                       INIT AND EXIT CODE                             *
 ************************************************************************/

static int feedopt_app_main(feedopt_connect_args_t *args, void **private_data)
{
	int retval;

	comp_id = hal_init(__core_hal_user, "feedopt");
	if (comp_id < 0) {
		RTAPI_PRINT_MSG(RTAPI_MSG_ERR, "FEEDOPT: ERROR: hal_init() failed\n");
		return -EINVAL;
	}

	retval = feedopt_init(args, private_data);
	if (retval < 0)
		goto fail;

	hal_ready(__core_hal_user, comp_id);

	return 0;

fail:

	hal_exit(__core_hal_user, comp_id);

	return retval;
}


/***********************************************************************
 *            REALTIME COUNTER COUNTING AND UPDATE FUNCTIONS            *
 ************************************************************************/

void feedopt_reset(feedopt_hal_t *fopt_hal)
{
#ifdef OPENCN_FORCE_UNDERRUN
    debug_count = 0;
#endif
	*fopt_hal->read_active = 0;
	*fopt_hal->rt_resampling_progress = 0;
	fopt_rg_clear(&samples_queue);
	state = FEEDOPT_STATE_INACTIVE;
}

static void feedopt_update(void *arg, long period)
{
	feedopt_data_t *data = (feedopt_data_t *)arg;

	*data->hal_data->sampling_period_ns = period;

#ifdef CONFIG_ARM
	kernel_neon_begin();
#endif

	feedopt_update_fp(data, state, period);

#ifdef CONFIG_ARM
	kernel_neon_end();
#endif
}


static int feedopt_create_pins(feedopt_data_t *data, int comp_id)
{
	feedopt_hal_t  *hal_data = data->hal_data;
	feedopt_axis_t *axis;
	int ret;
	int i;

	/* Export PINs */
	ret = hal_pin_newf_list(__core_hal_user, comp_id, hal_data, feedopt_pins, FEEDOPT_COMP_NAME);

	/* Export PARAMs */
	ret = hal_param_newf_list(__core_hal_user, hal_data, comp_id, feedopt_params, FEEDOPT_COMP_NAME);

	for (i = 0; i < data->axis_nr; i++) {
		axis = &hal_data->hal_axes[i];

		/* Export axis PINs */
		ret = hal_pin_newf_list(__core_hal_user, comp_id, axis, feedopt_axis_pins, FEEDOPT_COMP_NAME, data->axes[i]);

		/* Export axis PARAMs */
		ret = hal_param_newf_list(__core_hal_user, axis, comp_id, feedopt_axis_params, FEEDOPT_COMP_NAME, data->axes[i]);
		if (ret != 0)
			printk("[%s] ERROR: Creation of the axis PARAMs failed\n", FEEDOPT_COMP_NAME);

	}

	/* Set PINs default values */
	*hal_data->manual_override = 1.0;

	return ret;
}

static int feedopt_init(feedopt_connect_args_t *args, void **private_data)
{
	int ret;
	int usefp = 1;
	feedopt_data_t *data;
	feedopt_hal_t  *hal;
	feedopt_axis_t *axes;

	data = kmalloc(sizeof(feedopt_data_t), GFP_ATOMIC);
	if (!data)
		printk("[%s] ERROR: memory allocation failed\n", FEEDOPT_COMP_NAME);

	/* Store axis info */
	data->axis_nr = strlen(args->axes);
	strcpy(data->axes, args->axes);

	/* Memory allocation for PINs & PARAMs */
	hal = hal_malloc(__core_hal_user, sizeof(feedopt_hal_t));
	if (!hal) {
		printk("[%s] Error: memory allocation for the PINs failed\n", FEEDOPT_COMP_NAME);
		BUG();
	}

	axes = hal_malloc(__core_hal_user, sizeof(feedopt_axis_t) * data->axis_nr);
	if (!axes) {
		printk("[%s] Error: memory allocation for the axis PINs failed\n", FEEDOPT_COMP_NAME);
		BUG();
	}

	data->hal_data = hal;
	data->hal_data->hal_axes = axes;

	feedopt_create_pins(data, comp_id);

	/* Export function */
	ret = hal_export_funct(__core_hal_user, "feedopt.update", feedopt_update, data, usefp, 0, comp_id);
	if (ret != 0) {
		RTAPI_PRINT_MSG(RTAPI_MSG_ERR, "FEEDOPT: ERROR: function export failed\n");
		return ret;
	}

	*private_data = (void *)data;

	fopt_rg_alloc(&samples_queue, FEEDOPT_RT_QUEUE_SIZE);

	printk("[FEEDOPT]: Initialized\n");

	return 0;
}

/* opencn - This part of code comes from the user space counterpart. */
#if 1 /* Why to create a second component ?? */
static int feedopt_user_init(feedopt_connect_args_t *args, int major, int minor) {
	hal_user_t *hal_user;
	char comp_name[HAL_NAME_LEN + 1];

	hal_user = find_hal_user_by_dev(major, minor);
	if (!hal_user) {
		hal_user = (hal_user_t *) kzalloc(sizeof(hal_user_t), GFP_ATOMIC);
		if (!hal_user)
			BUG();

		/* Get the current related PID. */
		hal_user->pid = current->pid;
		hal_user->major = major;
		hal_user->minor = minor;
		hal_user->channel = args->channel;

		add_hal_user(hal_user);
	}

	snprintf(comp_name, sizeof(comp_name), "halfeedopt%d", hal_user->pid);
	hal_user->comp_id = hal_init(hal_user, comp_name);

	hal_ready(hal_user, hal_user->comp_id);

	return 0;
}
#endif

int feedopt_open(struct inode *inode, struct file *file)
{
	return 0;
}

int feedopt_release(struct inode *inode, struct file *filp)
{
	return 0;
}

/*
 * Read a stream feedopt_update and returns the feedopt_update number.
 */
ssize_t feedopt_read(struct file *filp, char __user *buf, size_t len, loff_t *off)
{
	return 0;
}

ssize_t feedopt_write(struct file *filp, const char __user *buf, size_t len, loff_t *off)
{
	feedopt_sample_t p_curv = *(feedopt_sample_t *)buf;

#ifdef OPENCN_FORCE_UNDERRUN
	if (++debug_count > 100) {
		debug_count = 200;
		schedule();
		return PushStatus_TryAgain;
	}
#endif

	if (!fopt_rg_push(&samples_queue, p_curv)) {
		return PushStatus_TryAgain;
	}

	return PushStatus_Success;
}

long feedopt_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int rc = 0, major, minor;
	hal_user_t *hal_user;
	feedopt_data_t *data;
	feedopt_hal_t *fopt_hal;

	major = imajor(filp->f_path.dentry->d_inode);
	minor = iminor(filp->f_path.dentry->d_inode);

	switch (cmd) {
	case FEEDOPT_IOCTL_CONNECT:
		BUG_ON(minor + 1 > 1);

/* Pure kernel side init */
#warning Check if already present (initialized) ...
		rc = feedopt_app_main((feedopt_connect_args_t *)arg, &filp->private_data);
		BUG_ON(rc);

#if 1 /* Why to create a second component ?? */
		rc = feedopt_user_init((feedopt_connect_args_t *) arg, major, minor);
		BUG_ON(rc);
#endif
		break;

	case FEEDOPT_IOCTL_DISCONNECT:

		hal_user = find_hal_user_by_dev(major, minor);
		BUG_ON(hal_user == NULL);

		fopt_rg_free(&samples_queue);
		hal_exit(hal_user, hal_user->comp_id);

		hal_exit(__core_hal_user, comp_id);

		break;

	case FEEDOPT_IOCTL_RESET:

		data = (feedopt_data_t *)filp->private_data;
		fopt_hal = data->hal_data;
		/*
		 * when a reset is issued, the system could be in the slowdown phase, so
		 * we wait until it stops and reports that RT is inactive
		 */
		data->reset = 1;
		while (*fopt_hal->read_active)
			schedule();
		feedopt_reset(fopt_hal);

	break;

	}

	return 0;
}

struct file_operations feedopt_fops = {
	.owner = THIS_MODULE,
	.open = feedopt_open,
	.release = feedopt_release,
	.unlocked_ioctl = feedopt_ioctl,
	.read = feedopt_read,
	.write = feedopt_write,
};

int feedopt_comp_init(void)
{
	int rc;

	printk("OpenCN: feedopt subsystem initialization.\n");

	/* Registering device */
	rc = register_chrdev(FEEDOPT_DEV_MAJOR, FEEDOPT_DEV_NAME, &feedopt_fops);
	if (rc < 0) {
		printk("Cannot obtain the major number %d\n", FEEDOPT_DEV_MAJOR);
		return rc;
	}

	return 0;
}

late_initcall(feedopt_comp_init)
