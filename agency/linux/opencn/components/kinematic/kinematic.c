/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: kinematic.c
 *
 *
 * kinematic component - generic functions.
 *
 ********************************************************************/

#include <linux/printk.h>

#include <opencn/uapi/kinematic.h>
#include "kinematic_priv.h"

static kin_func_forward_t kin_forward_raw = NULL;
static kin_func_inverse_t kin_inverse_raw = NULL;



typedef struct {
    int type;
    double params[KIN_PARAM_NR];
} kin_data_t;

static kin_data_t data = { KIN_TYPE_NO_INITIALIZED, { 0 } };

int kin_set_type(int type)
{
	int ret = 0;

	switch (type) {

	case KIN_TYPE_TRIVIAL:
		kin_forward_raw = kin_forward_trivial;
		kin_inverse_raw = kin_inverse_trivial;
		break;
	case KIN_TYPE_COREXY:
		kin_forward_raw = kin_forward_corexy;
		kin_inverse_raw = kin_inverse_corexy;
		break;
	case KIN_TYPE_XYZBC:
		kin_forward_raw = kin_forward_xyzbc;
		kin_inverse_raw = kin_inverse_xyzbc;
		break;
	default:
		printk("[%s] ERROR - kinematic '%d' not supported\n", KIN_COMP_NAME, type);
		ret = -1;
	}

	data.type = type;

	return ret;
}

int kin_get_type(void)
{
	return data.type;
}

int kin_set_params(double *params)
{
	int i;

	for (i = 0; i < KIN_PARAM_NR; i++)
		data.params[i] = params[i];

#ifdef OPENCN
	kin_update_hal_params(params);
#endif

	return 0;
}

int kin_get_params( double *params )
{
	int i;

	for (i = 0; i < KIN_PARAM_NR; i++)
		params[i] = data.params[i];

	return 0;
}

/* == Functions to use in Matlab and OpenCN == */

/* kinematic forward */
void kin_forward(double *world, const double *joint)
{
	if (kin_forward_raw)
		kin_forward_raw(world, joint);
	else
		printk("[%s] ERROR - kinematic not initialized !\n", KIN_COMP_NAME);
}


/* kinematic inverse */
void kin_inverse(const double *world, double *joint)
{
	if (kin_inverse_raw)
		kin_inverse_raw(world, joint);
	else
		printk("[%s] ERROR - kinematic not initialized !\n", KIN_COMP_NAME);

}
