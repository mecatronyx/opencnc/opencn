/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: kinematic_hal.c
 *
 *
 * kinematic component - HAL support.
 *
 ********************************************************************/

#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>

#include <opencn/hal/hal.h>
#include <opencn/uapi/kinematic.h>


typedef struct {
	/* PINs */
	hal_float_t *world_inverse_pin[KIN_MAX_JOINTS];
	hal_float_t *world_forward_pin[KIN_MAX_JOINTS];
	hal_float_t *joint_inverse_pin[KIN_MAX_JOINTS];
	hal_float_t *joint_forward_pin[KIN_MAX_JOINTS];

	/* PARAMs */
	hal_u32_t   type;
	hal_float_t params[KIN_PARAM_NR];
} kin_hal_data_t;

kin_hal_data_t *hal_data = NULL;

static const hal_pindesc_t kin_pins[] = {
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, world_inverse_pin[0]), "%s.world-in-x" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, world_inverse_pin[1]), "%s.world-in-y" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, world_inverse_pin[2]), "%s.world-in-z" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, world_inverse_pin[3]), "%s.world-in-a" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, world_inverse_pin[4]), "%s.world-in-b" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, world_inverse_pin[5]), "%s.world-in-c" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, world_inverse_pin[6]), "%s.world-in-u" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, world_inverse_pin[7]), "%s.world-in-v" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, world_inverse_pin[8]), "%s.world-in-w" },

	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, world_forward_pin[0]), "%s.world-out-x" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, world_forward_pin[1]), "%s.world-out-y" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, world_forward_pin[2]), "%s.world-out-z" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, world_forward_pin[3]), "%s.world-out-a" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, world_forward_pin[4]), "%s.world-out-b" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, world_forward_pin[5]), "%s.world-out-c" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, world_forward_pin[6]), "%s.world-out-u" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, world_forward_pin[7]), "%s.world-out-v" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, world_forward_pin[8]), "%s.world-out-w" },

	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, joint_inverse_pin[0]), "%s.joint-out-x" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, joint_inverse_pin[1]), "%s.joint-out-y" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, joint_inverse_pin[2]), "%s.joint-out-z" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, joint_inverse_pin[3]), "%s.joint-out-a" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, joint_inverse_pin[4]), "%s.joint-out-b" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, joint_inverse_pin[5]), "%s.joint-out-c" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, joint_inverse_pin[6]), "%s.joint-out-u" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, joint_inverse_pin[7]), "%s.joint-out-v" },
	{ HAL_FLOAT, HAL_OUT, offsetof(kin_hal_data_t, joint_inverse_pin[8]), "%s.joint-out-w" },

	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, joint_forward_pin[0]), "%s.joint-in-x" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, joint_forward_pin[1]), "%s.joint-in-y" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, joint_forward_pin[2]), "%s.joint-in-z" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, joint_forward_pin[3]), "%s.joint-in-a" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, joint_forward_pin[4]), "%s.joint-in-b" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, joint_forward_pin[5]), "%s.joint-in-c" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, joint_forward_pin[6]), "%s.joint-in-u" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, joint_forward_pin[7]), "%s.joint-in-v" },
	{ HAL_FLOAT, HAL_IN, offsetof(kin_hal_data_t, joint_forward_pin[8]), "%s.joint-in-w" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

/************************************************************************
 *                       PERIODIC RT FUNCTIONS                          *
 ************************************************************************/


static void kin_func(void *args, long period)
{
	int i;
	double world[KIN_MAX_JOINTS] = { 0 };
	double joint[KIN_MAX_JOINTS] = { 0 };
	kin_hal_data_t *data = (kin_hal_data_t *)args;

	/* World to join computation */
	for (i = 0; i < KIN_MAX_JOINTS; i++)
		world[i] = *data->world_inverse_pin[i];

	kin_inverse(world, joint);

	for (i = 0; i < KIN_MAX_JOINTS; i++)
		*data->joint_inverse_pin[i] = joint[i];

	/* Joint to world computation */
	for (i = 0; i < KIN_MAX_JOINTS; i++)
		joint[i] = *data->joint_forward_pin[i];

	kin_forward(world, joint);

	for (i = 0; i < KIN_MAX_JOINTS; i++)
		*data->world_forward_pin[i] = world[i];
}

/************************************************************************
 *                       INIT AND EXIT CODE                             *
 ************************************************************************/

static void kin_create_hal_params(kin_hal_data_t *hal_data, int comp_id)
{
	int i;
	int ret;

	ret = hal_param_u32_newf(__core_hal_user, HAL_RO, &hal_data->type, comp_id, "%s.type", KIN_COMP_NAME);
	if (ret) {
		printk("[%s] Error - Failed to create 'type' PIN\n", KIN_COMP_NAME);
		BUG();
	}

	for (i = 0; i < KIN_MAX_JOINTS; i++) {
		ret = hal_param_float_newf(__core_hal_user, HAL_RO, &hal_data->params[i], comp_id, "%s.param%d", KIN_COMP_NAME, i);

		if (ret) {
			printk("[%s] Error - Failed to create 'param' PINs\n", KIN_COMP_NAME);
			BUG();
		}
	}
}

static void kin_set_params_hal(kin_hal_data_t *hal_data, double *params)
{
	int i;

	for (i = 0; i < KIN_PARAM_NR; i++)
		hal_data->params[i] = params[i];
}

void kin_update_hal_params(double *params)
{
	kin_set_params_hal(hal_data, params);
}

static int kin_app_main(kin_connect_args_t *args)
{
	int ret = 0;
	int comp_id;
	char name[HAL_NAME_LEN];

	comp_id = hal_init(__core_hal_user, KIN_COMP_NAME);
	if (comp_id < 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "[%s] Error: hal_init() failed\n", KIN_COMP_NAME);
		return -EINVAL;
	}

	hal_data = hal_malloc(__core_hal_user, sizeof(kin_hal_data_t));
	if (!hal_data) {
		printk("[%s] Error: 'hal_data' allocation failed\n", KIN_COMP_NAME);
		ret = -EIO;
		goto fail;
	}

	if (args->enable_pin) {

		/* PINs creation */
		ret = hal_pin_newf_list(__core_hal_user, comp_id, hal_data, kin_pins, KIN_COMP_NAME);
		if (ret != 0) {
			printk("[%s] PINs creation failed\n", KIN_COMP_NAME);
			goto fail;
		}

		/* PARAMS creation */
		kin_create_hal_params(hal_data, comp_id);

		/* Export function */
		sprintf(name, "%s_func", KIN_COMP_NAME);

		ret = hal_export_funct(__core_hal_user, name, kin_func, hal_data, 1, 0, comp_id);
		if (ret != 0) {
			printk("[%s] Error: function export failed\n", KIN_COMP_NAME);
			goto fail;
		}
	}

	/* link the wanted kinematic */
	ret = kin_set_type(args->type);
	hal_data->type = args->type;

	/*  Set the parameter values */
	ret = kin_set_params(args->params);
	kin_set_params_hal(hal_data, args->params);

	hal_ready(__core_hal_user, comp_id);

	return 0;

fail:
	hal_exit(__core_hal_user, comp_id);

	return ret;

}

/************************************************************************
 *                Char Device & file operation definitions              *
 ************************************************************************/

static int kin_open(struct inode *inode, struct file *file)
{
	return 0;
}

static int kin_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static long kin_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int rc = 0;
	double world[KIN_MAX_JOINTS];
	double joint[KIN_MAX_JOINTS];

	switch (cmd) {
	case KIN_IOCTL_CONNECT:

		rc = kin_app_main((kin_connect_args_t *)arg);
		if (rc)
			printk("%s: failed to initialize...\n", __func__);

		break;

	case KIN_IOCTL_DISCONNECT:
		break;

	case KIN_IOCTL_FORWARD:

		memcpy(joint, ((kin_lib_args_t *)arg)->joint, sizeof(joint));

		kin_forward(world, joint);

		memcpy(((kin_lib_args_t *)arg)->world, world, sizeof(world));

		break;

	case KIN_IOCTL_INVERSE:

		memcpy(world, ((kin_lib_args_t *)arg)->world, sizeof(world));

		kin_inverse(world, joint);

		memcpy(((kin_lib_args_t *)arg)->joint, joint, sizeof(joint));

		break;
	}

	return rc;
}

struct file_operations kin_fops = {
	.owner = THIS_MODULE,
	.open = kin_open,
	.release = kin_release,
	.unlocked_ioctl = kin_ioctl,
};

int kin_comp_init(void)
{
	printk("OpenCN: %s subsystem initialization.\n", KIN_COMP_NAME);

	hal_create_interface(KIN_COMP_NAME, &kin_fops);

	return 0;
}

late_initcall(kin_comp_init)
