/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: kinematic_priv.h
 *
 ********************************************************************/

typedef void (*kin_func_forward_t)(double *world, const double *joint);
typedef void (*kin_func_inverse_t)(const double *world, double *joint);

void kin_update_hal_params(double *params);

void kin_forward_trivial(double *world, const double *joint);
void kin_inverse_trivial(const double *world, double *joint);


void kin_forward_corexy(double *world, const  double *joint);
void kin_inverse_corexy(const double *world, double *joint);

void kin_forward_xyzbc(double *world, const  double *joint);
void kin_inverse_xyzbc(const double *world, double *joint);

