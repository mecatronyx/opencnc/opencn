/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: kinematic_corexy.c
 *
 *
 * kinematic component - Core XY kinematic.
 *
 ********************************************************************/

#include <opencn/uapi/kinematic.h>

/* Core XY kinematic forward */
void kin_forward_corexy(double *world, const double *joint)
{
	int i;

	world[0] = 0.5 * (joint[0] + joint[1]);
	world[1] = 0.5 * (joint[0] - joint[1]);

	for (i = 2; i < KIN_MAX_JOINTS; i++)
		world[i] = joint[i];
}

/* Core XY kinematic inverse */
void kin_inverse_corexy(const double *world, double *joint)
{
	int i;

	joint[0] = world[0] + world[1];
	joint[1] = world[0] - world[1];

	for (i = 2; i < KIN_MAX_JOINTS; i++)
		 joint[i] = world[i];
}
