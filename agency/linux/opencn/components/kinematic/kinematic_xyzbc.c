/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: kinematic_xyzbc.c
 *
 *
 * kinematic component - XYZBC, 5 axes kinematic.
 *
 ********************************************************************/

#include <opencn/rtapi/rtapi_math.h>

#include <opencn/uapi/kinematic.h>


/*
 * function piece = kin_xyzbc_tt_forward(in1,in2)
 *
 * KIN_XYZBC_TT_FORWARD
 *     PIECE = KIN_XYZBC_TT_FORWARD(IN1,IN2)
 *
 * Arguments    : const double in1[5]
 *                double in2[5][3]
 *                double piece[5]
 * Return Type  : void
 */
void kin_xyzbc_tt_forward_matlab(const double in1[5], double in2[5][3], double piece[5])
{
    double t10;
    double t11;
    double t2;
    double t3;
    double t4;
    double t5;
    double t9;
    /*     This function was generated by the Symbolic Math Toolbox version 9.1. */
    /*     09-Oct-2023 10:15:19 */
    /* Forward Kinematics :  */
    /* INPUTS :  */
    /* 	r_j    (5x1) : pose vector( joint space ) */
    /* 	p      (3x5) : parameters */
    /* OUTPUTS :  */
    /* 	r_t    (5x1) : pose vector ( piece frame ) */
    /* 'kin_xyzbc_tt_forward:15' b = in1(4,:); */
    /* 'kin_xyzbc_tt_forward:16' c = in1(5,:); */
    /* 'kin_xyzbc_tt_forward:17' offB1 = in2(10); */
    /* 'kin_xyzbc_tt_forward:18' offB2 = in2(11); */
    /* 'kin_xyzbc_tt_forward:19' offC1 = in2(13); */
    /* 'kin_xyzbc_tt_forward:20' offB3 = in2(12); */
    /* 'kin_xyzbc_tt_forward:21' offC2 = in2(14); */
    /* 'kin_xyzbc_tt_forward:22' offC3 = in2(15); */
    /* 'kin_xyzbc_tt_forward:23' offM1 = in2(1); */
    /* 'kin_xyzbc_tt_forward:24' offM2 = in2(2); */
    /* 'kin_xyzbc_tt_forward:25' offM3 = in2(3); */
    /* 'kin_xyzbc_tt_forward:26' offP1 = in2(7); */
    /* 'kin_xyzbc_tt_forward:27' offP2 = in2(8); */
    /* 'kin_xyzbc_tt_forward:28' offP3 = in2(9); */
    /* 'kin_xyzbc_tt_forward:29' offT1 = in2(4); */
    /* 'kin_xyzbc_tt_forward:30' offT2 = in2(5); */
    /* 'kin_xyzbc_tt_forward:31' offT3 = in2(6); */
    /* 'kin_xyzbc_tt_forward:32' x = in1(1,:); */
    /* 'kin_xyzbc_tt_forward:33' y = in1(2,:); */
    /* 'kin_xyzbc_tt_forward:34' z = in1(3,:); */
    /* 'kin_xyzbc_tt_forward:35' t2 = cos(b); */
    t2 = cos(in1[3]);
    /* 'kin_xyzbc_tt_forward:36' t3 = cos(c); */
    t3 = cos(in1[4]);
    /* 'kin_xyzbc_tt_forward:37' t4 = sin(b); */
    t4 = sin(in1[3]);
    /* 'kin_xyzbc_tt_forward:38' t5 = sin(c); */
    t5 = sin(in1[4]);
    /* 'kin_xyzbc_tt_forward:39' t6 = -offT1; */
    /* 'kin_xyzbc_tt_forward:40' t7 = -offT2; */
    /* 'kin_xyzbc_tt_forward:41' t8 = -offT3; */
    /* 'kin_xyzbc_tt_forward:42' t9 = offB1+offC1+offM1+t6+x; */
    t9 = ((((&in2[0][0])[9] + (&in2[0][0])[12]) + (&in2[0][0])[0]) + -(&in2[0][0])[3]) + in1[0];
    /* 'kin_xyzbc_tt_forward:43' t10 = offB2+offC2+offM2+t7+y; */
    t10 = ((((&in2[0][0])[10] + (&in2[0][0])[13]) + (&in2[0][0])[1]) + -(&in2[0][0])[4]) + in1[1];
    /* 'kin_xyzbc_tt_forward:44' t11 = offB3+offC3+offM3+t8+z; */
    t11 = ((((&in2[0][0])[11] + (&in2[0][0])[14]) + (&in2[0][0])[2]) + -(&in2[0][0])[5]) + in1[2];
    /* 'kin_xyzbc_tt_forward:45' piece =
     * [-offC1-offP1-offB1.*t3-offB2.*t5+t5.*t10+t2.*t3.*t9-t3.*t4.*t11;-offC2-offP2-offB2.*t3+offB1.*t5+t3.*t10-t2.*t5.*t9+t4.*t5.*t11;-offB3-offC3-offP3+t2.*t11+t4.*t9;b;c];
     */
    piece[0] =
        (((((-(&in2[0][0])[12] - (&in2[0][0])[6]) - (&in2[0][0])[9] * t3) - (&in2[0][0])[10] * t5) +
          t5 * t10) +
         t2 * t3 * t9) -
        t3 * t4 * t11;
    piece[1] =
        (((((-(&in2[0][0])[13] - (&in2[0][0])[7]) - (&in2[0][0])[10] * t3) + (&in2[0][0])[9] * t5) +
          t3 * t10) -
         t2 * t5 * t9) +
        t4 * t5 * t11;
    piece[2] = (((-(&in2[0][0])[11] - (&in2[0][0])[14]) - (&in2[0][0])[8]) + t2 * t11) + t4 * t9;
    piece[3] = in1[3];
    piece[4] = in1[4];
}

/*
 * function joint = kin_xyzbc_tt_inverse(in1,in2)
 *
 * KIN_XYZBC_TT_INVERSE
 *     JOINT = KIN_XYZBC_TT_INVERSE(IN1,IN2)
 *
 * Arguments    : const double in1[5]
 *                double in2[5][3]
 *                double joint[5]
 * Return Type  : void
 */
void kin_xyzbc_tt_inverse_matlab(const double in1[5], double in2[5][3], double joint[5])
{
    double t2;
    double t3;
    double t4;
    double t5;
    /*     This function was generated by the Symbolic Math Toolbox version 9.1. */
    /*     09-Oct-2023 10:15:19 */
    /* Inverse Kinematics :  */
    /* INPUTS :  */
    /* 	r_t    (5x1) : pose vector ( piece frame ) */
    /* 	p      (3x5) : parameters */
    /* OUTPUTS :  */
    /* 	r_j    (5x1) : pose vector( joint space ) */
    /* 'kin_xyzbc_tt_inverse:15' b = in1(4,:); */
    /* 'kin_xyzbc_tt_inverse:16' c = in1(5,:); */
    /* 'kin_xyzbc_tt_inverse:17' offB1 = in2(10); */
    /* 'kin_xyzbc_tt_inverse:18' offC1 = in2(13); */
    /* 'kin_xyzbc_tt_inverse:19' offB3 = in2(12); */
    /* 'kin_xyzbc_tt_inverse:20' offC2 = in2(14); */
    /* 'kin_xyzbc_tt_inverse:21' offC3 = in2(15); */
    /* 'kin_xyzbc_tt_inverse:22' offM1 = in2(1); */
    /* 'kin_xyzbc_tt_inverse:23' offM2 = in2(2); */
    /* 'kin_xyzbc_tt_inverse:24' offM3 = in2(3); */
    /* 'kin_xyzbc_tt_inverse:25' offP1 = in2(7); */
    /* 'kin_xyzbc_tt_inverse:26' offP2 = in2(8); */
    /* 'kin_xyzbc_tt_inverse:27' offP3 = in2(9); */
    /* 'kin_xyzbc_tt_inverse:28' offT1 = in2(4); */
    /* 'kin_xyzbc_tt_inverse:29' offT2 = in2(5); */
    /* 'kin_xyzbc_tt_inverse:30' offT3 = in2(6); */
    /* 'kin_xyzbc_tt_inverse:31' x = in1(1,:); */
    /* 'kin_xyzbc_tt_inverse:32' y = in1(2,:); */
    /* 'kin_xyzbc_tt_inverse:33' z = in1(3,:); */
    /* 'kin_xyzbc_tt_inverse:34' t2 = cos(b); */
    t2 = cos(in1[3]);
    /* 'kin_xyzbc_tt_inverse:35' t3 = cos(c); */
    t3 = cos(in1[4]);
    /* 'kin_xyzbc_tt_inverse:36' t4 = sin(b); */
    t4 = sin(in1[3]);
    /* 'kin_xyzbc_tt_inverse:37' t5 = sin(c); */
    t5 = sin(in1[4]);
    /* 'kin_xyzbc_tt_inverse:38' joint =
     * [-offB1-offC1-offM1+offT1+offB1.*t2+offB3.*t4+offC3.*t4+offP3.*t4+t4.*z+offC1.*t2.*t3-offC2.*t2.*t5+offP1.*t2.*t3-offP2.*t2.*t5+t2.*t3.*x-t2.*t5.*y;-offC2-offM2+offT2+offC2.*t3+offC1.*t5+offP2.*t3+offP1.*t5+t5.*x+t3.*y;-offB3-offC3-offM3+offT3-offB1.*t4+offB3.*t2+offC3.*t2+offP3.*t2+t2.*z-offC1.*t3.*t4+offC2.*t4.*t5-offP1.*t3.*t4+offP2.*t4.*t5-t3.*t4.*x+t4.*t5.*y;b;c];
     */
    joint[0] =
        (((((((((((((-(&in2[0][0])[9] - (&in2[0][0])[12]) - (&in2[0][0])[0]) + (&in2[0][0])[3]) +
                  (&in2[0][0])[9] * t2) +
                 (&in2[0][0])[11] * t4) +
                (&in2[0][0])[14] * t4) +
               (&in2[0][0])[8] * t4) +
              t4 * in1[2]) +
             (&in2[0][0])[12] * t2 * t3) -
            (&in2[0][0])[13] * t2 * t5) +
           (&in2[0][0])[6] * t2 * t3) -
          (&in2[0][0])[7] * t2 * t5) +
         t2 * t3 * in1[0]) -
        t2 * t5 * in1[1];
    joint[1] =
        (((((((-(&in2[0][0])[13] - (&in2[0][0])[1]) + (&in2[0][0])[4]) + (&in2[0][0])[13] * t3) +
            (&in2[0][0])[12] * t5) +
           (&in2[0][0])[7] * t3) +
          (&in2[0][0])[6] * t5) +
         t5 * in1[0]) +
        t3 * in1[1];
    joint[2] =
        (((((((((((((-(&in2[0][0])[11] - (&in2[0][0])[14]) - (&in2[0][0])[2]) + (&in2[0][0])[5]) -
                  (&in2[0][0])[9] * t4) +
                 (&in2[0][0])[11] * t2) +
                (&in2[0][0])[14] * t2) +
               (&in2[0][0])[8] * t2) +
              t2 * in1[2]) -
             (&in2[0][0])[12] * t3 * t4) +
            (&in2[0][0])[13] * t4 * t5) -
           (&in2[0][0])[6] * t3 * t4) +
          (&in2[0][0])[7] * t4 * t5) -
         t3 * t4 * in1[0]) +
        t4 * t5 * in1[1];
    joint[3] = in1[3];
    joint[4] = in1[4];
}


static void kin_xyzbc_set_params(double in[5][3], double *params)
{
    /* Machine */
    in[0][0] = params[0];       /*machine   offset x*/
    in[0][1] = params[1];       /*machine   offset y*/
    in[0][2] = params[2];       /*machine   offset z*/
    /* Tool */
    in[1][0] = params[3];       /*tool      offset x*/
    in[1][1] = params[4];       /*tool      offset y*/
    in[1][2] = params[5];       /*tool      offset z*/
    /* Piece */
    in[2][0] = params[6];       /*piece     offset x*/
    in[2][1] = params[7];       /*piece     offset y*/
    in[2][2] = params[8];       /*piece     offset z*/
    /* B */
    in[3][0] = params[9];       /*B axis    offset x*/
    in[3][1] = params[10];      /*B axis    offset y*/
    in[3][2] = params[11];      /*B axis    offset z*/
    /* C */
    in[4][0] = params[12];      /*C axis    offset x*/
    in[4][1] = params[13];      /*C axis    offset y*/
    in[4][2] = params[14];      /*C axis    offset z*/
}


/* xyzbc kinematic forward */
void kin_forward_xyzbc(double *world, const double *joint)
{
    double params[KIN_PARAM_NR];
    double joint_deg[5];
    double in2[5][3];
    int i;

    kin_get_params(params);

    kin_xyzbc_set_params(in2, params);

    /* convert angles to radian */
    for (i = 0; i < 5; i++ )
        if (i > 2)
            joint_deg[i] = (joint[i] * M_PI) /  180.0;
        else
            joint_deg[i] = joint[i];

    kin_xyzbc_tt_forward_matlab(joint_deg, in2, world);

    /* convert angles back to degree */
    for (i = 3; i < 5; i++ )
        world[i] = (world[i] * 180.0) / M_PI;
}

/* xyzbc kinematic inverse */
void kin_inverse_xyzbc(const double *world, double *joint)
{
    double params[KIN_PARAM_NR];
    double world_deg[5];
    double in2[5][3];
    int i;

    kin_get_params(params);

    kin_xyzbc_set_params(in2, params);

    /* convert angles to radian */
    for (i = 0; i < 5; i++ )
        if (i > 2)
            world_deg[i] = (world[i] * M_PI) /  180.0;
        else
            world_deg[i] = world[i];

    kin_xyzbc_tt_inverse_matlab(world_deg, in2, joint);

    /* convert angles back to degree */
    for (i = 3; i < 5; i++ )
        joint[i] = (joint[i] * 180.0) / M_PI;
}
