/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: kinematic_trivial.c
 *
 *
 * kinematic component - Trivial kinematic.
 *
 ********************************************************************/

#include <opencn/uapi/kinematic.h>

/* Trivial kinematic forward */
void kin_forward_trivial(double *world, const  double *joint)
{
	int i;

	for (i = 0; i < KIN_MAX_JOINTS; i++)
		world[i] = joint[i];

}

/* Trivial kinematic inverse */
void kin_inverse_trivial(const double *world, double *joint)
{
	int i;

	for (i = 0; i < KIN_MAX_JOINTS; i++)
		joint[i] = world[i];
}


