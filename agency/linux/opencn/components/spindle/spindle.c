/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2023 by Xavier Soltner <xavier.soltner@heig-vd.ch>
 *
 * file: spindle.c
 *
 *
 * It is the kernel part of the "spindle" OpenCN HAL component. 
 * The spindle component is in charge of managing ONE spindle. 
 * This component manage the state and acceleration/deceleration of the spindle. 
 * It also provide option to manage the spindle temperature, cooling, and ATC tool mounting. 
 *
 ********************************************************************/

#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <opencn/ctypes/strings.h> /* opencn_printf */

#include <opencn/hal/hal.h>
#include <opencn/rtapi/rtapi_math.h>
#include <opencn/uapi/spindle.h>

/* globals */
static int spindle_instance_nr = 0;

/* Add spin_debug pin */
#if 0
#define DEBUG_SPINDLE
#endif

typedef struct {
    /* Pins */
    hal_float_t *actual_pin;

    /* Params */
    hal_float_t target_param;
    hal_float_t target_tol_param;
    hal_float_t min_param;
    hal_float_t max_param;
    hal_float_t max_vel_min_temp_param;

    /* Temperature state */
    int state;
} spindle_temperature_t;

typedef struct {
    /* Pins */
    hal_bit_t *enable_pin;
} spindle_cooling_t;

typedef struct {
    /* Pins */
    hal_bit_t *cmd_free_tool_pin;
    hal_bit_t *free_tool_pin;
    hal_bit_t *clean_cone_pin;
    hal_bit_t *sens_0_pin;
    hal_bit_t *sens_1_pin;
} spindle_atc_t;

typedef struct{
    /* Linear coefficient */
    double a,b;

    double v0;      /* start velocity in [turn/s]*/ 
    double v1;      /* end velocity in [turn/s]*/ 

    double t0;      /* = absolute start time of motion in [s]*/ 
    double t1;      /* = absolute end time of acceleration / deceleration in [s]*/ 

    double T;       /* Total time for the acceleration / deceleration phase in [s] */

    double accel;   /* acceleration parameter of the spindle in [turn/s²]*/ 
}interp_spindle_lin_t;

typedef struct {
    /* Polynomial coefficient */
    double a,b,c,d;

    /* Time & Velocity data */
    double v0;       /* start velocity in [turn/s] */
    double v1;       /* end velocity in [turn/s] */

    double t0;       /* = absolute start time of motion in [s] */
    double t1;       /* = absolute end time of acceleration / deceleration in [s] */
    double T;        /* Total time for the acceleration / deceleration phase in [s] */

    /* Acceleration for transition profile */
    double accel;    /* acceleration parameter of the spindle in [turn/s²] */

}interp_spindle_poly3_t;

typedef struct {
    /* List of possible transition type */
    interp_spindle_lin_t lin;
    interp_spindle_poly3_t poly3;

    /* Regulation type for given transition */
    int reg_type;

    /* General information given by transition children */
    double T;       /* Total time for the acceleration / deceleration phase in [s] */
    double t1;      /* absolute end time of acceleration / deceleration in [s] */

    /* Is the motion done ? */
    bool is_done;
}interp_trans_t;

typedef struct{
    hal_u32_t *state_elec_pin;
    hal_u32_t *state_speed_pin;
}spindle_debug_t;


typedef struct {
    /* List of spindle component loaded */
	struct list_head list;
    int comp_id;
    
    /* General pins */
    hal_float_t *cmd_velocity_pin;
    hal_bit_t *cmd_enable_pin;
    hal_bit_t *cmd_fault_reset_pin;
    hal_float_t *cmd_override_pin;
    hal_float_t *target_velocity_pin;
    hal_float_t *current_velocity_pin;
    hal_bit_t *set_mode_csv_pin;
    hal_bit_t *set_mode_inactive_pin;
    hal_bit_t *in_mode_csv_pin;
    hal_bit_t *in_mode_inactive_pin;
    hal_bit_t *fault_reset_pin;
    hal_bit_t *in_fault_pin;
    hal_bit_t *machine_active_pin;
    hal_bit_t *can_turn_pin;
    hal_bit_t *velocity_reached_pin;
    hal_bit_t *air_protection_pin;

    /* General parameters */
    hal_s32_t regulation_type_param;
    hal_float_t acceleration_param;
    hal_float_t velocity_min_param;
    hal_float_t velocity_max_param;
    hal_float_t velocity_tolerance_param;

    /* State */
    Spindle_elec_state_t state_elec;
    Spindle_speed_state_t state_speed;

    /* Timing */
    double dt;          /* time between each cycle in [s] */
    double cur_time;    /* absolute current local time of the component instance in [s] */
    
    /* History */
    double prev_act_veloctiy;
    double prev_cmd_override;
    double prev_cmd_velocity;
    double prev_cmpt_vel;
    bool is_accel_from_zero;
    bool is_vel_reached_override;

    /* Computed velocity */
    double cmpt_vel;

    /* Transition Profile */
    interp_trans_t interp;

    /* Settings */
    uint16_t cfg_id;
    char name[SPINDLE_NAME_MAX_LENGTH + 1];
    bool has_temperature;
    bool has_cooling;
    int instance_nr;

    /* Specific pins & param mods */
    spindle_temperature_t   *temperature;
    spindle_cooling_t       *cooling;
    spindle_atc_t           *atc;

    /* Spindle debug pin */
    spindle_debug_t *spin_debug;
} spindle_data_t;

LIST_HEAD(spindles);

/***********************************************************************
 *              GENERAL PINS & PARAMS
 * *********************************************************************/
static const hal_pindesc_t spindle_pins[] = {

    { HAL_FLOAT, HAL_IN,   offsetof(spindle_data_t, cmd_velocity_pin),      "%s.%s.cmd-velocity"},
    { HAL_BIT, HAL_IN,     offsetof(spindle_data_t, cmd_enable_pin),        "%s.%s.cmd-enable"},
    { HAL_BIT, HAL_IN,     offsetof(spindle_data_t, cmd_fault_reset_pin),   "%s.%s.cmd-fault-reset"},
    { HAL_FLOAT, HAL_IN,   offsetof(spindle_data_t, cmd_override_pin),      "%s.%s.cmd-override"},
    { HAL_FLOAT, HAL_OUT,  offsetof(spindle_data_t, target_velocity_pin),   "%s.%s.target-velocity"},
    { HAL_FLOAT, HAL_IN,   offsetof(spindle_data_t, current_velocity_pin),  "%s.%s.current-velocity"},
    { HAL_BIT, HAL_OUT,    offsetof(spindle_data_t, set_mode_csv_pin),      "%s.%s.set-mode-csv"},
    { HAL_BIT, HAL_OUT,    offsetof(spindle_data_t, set_mode_inactive_pin), "%s.%s.set-mode-inactive"},
    { HAL_BIT, HAL_IN,     offsetof(spindle_data_t, in_mode_csv_pin),       "%s.%s.in-mode-csv"},
    { HAL_BIT, HAL_IN,     offsetof(spindle_data_t, in_mode_inactive_pin),  "%s.%s.in-mode-inactive"},
    { HAL_BIT, HAL_OUT,    offsetof(spindle_data_t, fault_reset_pin),       "%s.%s.fault-reset"},
    { HAL_BIT, HAL_IN,     offsetof(spindle_data_t, in_fault_pin),          "%s.%s.in-fault"},
    { HAL_BIT, HAL_IN,     offsetof(spindle_data_t, machine_active_pin),    "%s.%s.machine-active"},
    { HAL_BIT, HAL_OUT,    offsetof(spindle_data_t, can_turn_pin),          "%s.%s.can-turn"},
    { HAL_BIT, HAL_OUT,    offsetof(spindle_data_t, velocity_reached_pin),  "%s.%s.velocity-reached"},
    { HAL_BIT, HAL_OUT,    offsetof(spindle_data_t, air_protection_pin),    "%s.%s.air-protection"},
    
    { HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t spindle_param[] = {

    { HAL_U32, HAL_RW,      offsetof(spindle_data_t, regulation_type_param),    "%s.%s.regulation.type"},
    { HAL_FLOAT, HAL_RW,    offsetof(spindle_data_t, acceleration_param),       "%s.%s.regulation.accel"},
    { HAL_FLOAT, HAL_RW,    offsetof(spindle_data_t, velocity_min_param),       "%s.%s.velocity.min"},
    { HAL_FLOAT, HAL_RW,    offsetof(spindle_data_t, velocity_max_param),       "%s.%s.velocity.max"},
    { HAL_FLOAT, HAL_RW,    offsetof(spindle_data_t, velocity_tolerance_param), "%s.%s.velocity.tolerance"},

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t spindle_debug_pins[] = {

    { HAL_U32,  HAL_OUT,    offsetof(spindle_debug_t, state_elec_pin),   "%s.%s.d-state-elec"},
    { HAL_U32,  HAL_OUT,    offsetof(spindle_debug_t, state_speed_pin),   "%s.%s.d-state-speed"},
    
    { HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

/***********************************************************************
 *              MODE SPECIFIC PINS & PARAMS
 * *********************************************************************/

static const hal_pindesc_t spindle_temperature_pins[] = {

    { HAL_FLOAT, HAL_IN,   offsetof(spindle_temperature_t, actual_pin),      "%s.%s.temp.actual"},
    
    { HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t spindle_temperature_param[] = {

    { HAL_FLOAT, HAL_RW,    offsetof(spindle_temperature_t, target_param),              "%s.%s.temp.target"},
    { HAL_FLOAT, HAL_RW,    offsetof(spindle_temperature_t, target_tol_param),          "%s.%s.temp.target-tol"},
    { HAL_FLOAT, HAL_RW,    offsetof(spindle_temperature_t, min_param),                 "%s.%s.temp.min"},
    { HAL_FLOAT, HAL_RW,    offsetof(spindle_temperature_t, max_param),                 "%s.%s.temp.max"},
    { HAL_FLOAT, HAL_RW,    offsetof(spindle_temperature_t, max_vel_min_temp_param),    "%s.%s.temp.max-vel-min-temp"},

    { HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t spindle_cooling_pins[] = {

    { HAL_BIT, HAL_OUT,   offsetof(spindle_cooling_t, enable_pin),      "%s.%s.cooling.enable"},
    
    { HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t spindle_atc_pins[] = {

    { HAL_BIT, HAL_IN,  offsetof(spindle_atc_t, cmd_free_tool_pin), "%s.%s.atc.cmd-free-tool"},
    { HAL_BIT, HAL_OUT, offsetof(spindle_atc_t, free_tool_pin),     "%s.%s.atc.free-tool"},
    { HAL_BIT, HAL_OUT, offsetof(spindle_atc_t, clean_cone_pin),    "%s.%s.atc.clean-cone"},
    { HAL_BIT, HAL_IN,  offsetof(spindle_atc_t, sens_0_pin),        "%s.%s.atc.sens-0"},
    { HAL_BIT, HAL_IN,  offsetof(spindle_atc_t, sens_1_pin),        "%s.%s.atc.sens-1"},
 
    { HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

/************************************************************************
 *                                                                      *
 *                       DEBUG FUNC                                     *
 *                                                                      *
 ************************************************************************/
#ifdef DEBUG_SPINDLE

static void spindle_trans_print(spindle_data_t *data)
{
/*
 * Print is deactivated because it lead to RT unstability,
 * Please see issue https://gitlab.com/mecatronyx/opencnc/opencn/-/issues/197
 * 
 */

    //opencn_printf("[%s.%s] Regulation=%d\n",SPINDLE_COMP_NAME, data->name, data->interp.reg_type);
    //opencn_printf("[%s.%s] cur_time=%8.2f\n",SPINDLE_COMP_NAME, data->name, data->cur_time);

    switch (data->interp.reg_type){
    case SPINDLE_REGULATION_LINEAR:
        //opencn_printf("[%s.%s] a=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.lin.a);
        //opencn_printf("[%s.%s] b=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.lin.b);
        //opencn_printf("[%s.%s] v0=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.lin.v0);
        //opencn_printf("[%s.%s] v1=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.lin.v1);
        //opencn_printf("[%s.%s] t0=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.lin.t0);
        //opencn_printf("[%s.%s] t1=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.lin.t1);
        //opencn_printf("[%s.%s] T=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.lin.T);
        //opencn_printf("[%s.%s] accel=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.lin.accel);
        break;
    case SPINDLE_REGULATON_POLY3:
        //opencn_printf("[%s.%s] a=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.poly3.a);
        //opencn_printf("[%s.%s] b=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.poly3.b);
        //opencn_printf("[%s.%s] c=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.poly3.c);
        //opencn_printf("[%s.%s] d=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.poly3.d);
        //opencn_printf("[%s.%s] v0=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.poly3.v0);
        //opencn_printf("[%s.%s] v1=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.poly3.v1);
        //opencn_printf("[%s.%s] t0=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.poly3.t0);
        //opencn_printf("[%s.%s] t1=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.poly3.t1);
        //opencn_printf("[%s.%s] T=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.poly3.T);
        //opencn_printf("[%s.%s] accel=%5.2f\n",SPINDLE_COMP_NAME, data->name, data->interp.poly3.accel);
        break;

    default:
        rtapi_print_msg(RTAPI_MSG_ERR,"[%s.%s] %d is unkown regulation\n",SPINDLE_COMP_NAME, data->name, data->regulation_type_param);
        break;
    }
}
#endif


/***********************************************************************
 *                                                                     *
 *                       TRANSITION PROFILE                            *
 *                                                                     *
 ************************************************************************/
static void spindle_trans_init_lin(spindle_data_t *data, int flag)
{
    interp_spindle_lin_t ip = data->interp.lin;
    double cur_time = data->cur_time;

    /* Get transition flag */
    if (flag == SPINDLE_TRANS_STOP){
        ip.v1 = 0.0;
    } else {
        ip.v1 = data->cmpt_vel;
    }   
    /* Get parameter */
    ip.v0 = *data->current_velocity_pin;
    ip.accel = data->acceleration_param;

    /* Calculate timing */
    ip.T = fabs(ip.v1 - ip.v0) / ip.accel;
    ip.t0 = cur_time;
    ip.t1 = ip.t0 + ip.T;

    /* This is solution of a linear transition with constraint of p1(t) at 0 and T*/
    ip.a = (ip.v1 - ip.v0) / (ip.t1 - ip.t0);
    ip.b = ip.v0;

    /* Store transition */
    data->interp.lin = ip;
    data->interp.T = ip.T;
    data->interp.t1 = ip.t1;

    return;
}

static double spindle_trans_get_lin(spindle_data_t *data)
{
    interp_spindle_lin_t ip = data->interp.lin;
    double cur_time = data->cur_time;

    /* Get elapsed time */
    double elps_time = (cur_time - ip.t0);

    /* Set if the motion is done */
    if ( ip.t1 < cur_time){
        data->interp.is_done = true;
    }    

    /* Store */
    data->interp.lin = ip;

    /* Return linear value at current time */
    if ( ip.t1 < cur_time){
        return ip.v1;
    } else {
        return ip.a * elps_time + ip.b;
    }
}

static void spindle_trans_init_poly3(spindle_data_t *data, int flag)
{
    interp_spindle_poly3_t ip = data->interp.poly3;
    double cur_time = data->cur_time;

    /* Get transition flag */
    if (flag == SPINDLE_TRANS_STOP){
        ip.v1 = 0.0;
    } else {
        ip.v1 = data->cmpt_vel;
    }
    /* Get parameter */
    ip.v0 = *data->current_velocity_pin;
    ip.accel = data->acceleration_param;
    
    /* Calculate timing */
    ip.T = fabs(ip.v1 - ip.v0) / ip.accel;
    ip.t0 = cur_time;
    ip.t1 = ip.t0 + ip.T;
    
    /* This is solution of a polynome3 with  contraint of p3(t) and p3'(t) at 0 and T*/
    ip.a = -(2.0 * (ip.v1 - ip.v0)) / (ip.T * ip.T * ip.T);
    ip.b = (3.0*(ip.v1 - ip.v0)) / (ip.T * ip.T);
    ip.c = 0.0;
    ip.d = ip.v0;

    /* Store transition */
    data->interp.poly3 = ip;
    data->interp.T = ip.T;
    data->interp.t1 = ip.t1;

    return;
}

static double spindle_trans_get_poly3(spindle_data_t *data)
{
    interp_spindle_poly3_t ip = data->interp.poly3;
    double cur_time = data->cur_time;

    /* Get elapsed time */
    double elps_time = (cur_time - ip.t0);

    /* Set if the motion is done */
    if ( ip.t1 < cur_time )
        data->interp.is_done = true;

    /* Store */
    data->interp.poly3 = ip;

    /* Return poly3 value at current time */
    if ( ip.t1 < cur_time){
        return ip.v1;
    } else {
        return ip.a * elps_time *elps_time * elps_time + ip.b * elps_time *elps_time + ip.c * elps_time + ip.d;
    }
    
}

static void spindle_trans_init(spindle_data_t *data, int flag)
{
    /* Init */
    data->interp.reg_type = data->regulation_type_param;
    data->interp.is_done = false;

    /* Init profile specific */
    switch (data->interp.reg_type){
    case SPINDLE_REGULATION_LINEAR:
        spindle_trans_init_lin(data,flag);
        break;
    case SPINDLE_REGULATON_POLY3:
        spindle_trans_init_poly3(data,flag);
        break;
    case SPINDLE_REGULATION_UNKNOWN:
        /* Nothing to do - wait user change of regulation_type after warning */
        break;
    default:
        data->interp.reg_type = SPINDLE_REGULATION_UNKNOWN;
        data->interp.is_done = true;
        rtapi_print_msg(RTAPI_MSG_ERR,"[%s.%s] %d is unkown regulation\n",SPINDLE_COMP_NAME, data->name, data->regulation_type_param);
        break;
    }

#ifdef DEBUG_SPINDLE
    spindle_trans_print(data);
#endif
}

static double spindle_trans_get(spindle_data_t *data)
{
    double ret;

    switch (data->interp.reg_type){
    case SPINDLE_REGULATION_LINEAR:
        ret = spindle_trans_get_lin(data);
        break;
    case SPINDLE_REGULATON_POLY3:
        ret = spindle_trans_get_poly3(data);
        break;
    case SPINDLE_REGULATION_UNKNOWN:
        /* Return previous computed speed */
        ret = data->prev_cmpt_vel;
        break;
    default:
        data->interp.reg_type = SPINDLE_REGULATION_UNKNOWN;
        data->interp.is_done = true;
        ret = 0.0;
        rtapi_print_msg(RTAPI_MSG_ERR,"[%s.%s] %d is unkown regulation\n",SPINDLE_COMP_NAME, data->name, data->regulation_type_param);
        break;
    }

    return ret;
}



/************************************************************************
 *                                                                      *
 *                       CONFIGRUATION SPECIFIC FUNC                    *
 *                                                                      *
 ************************************************************************/

static void spindle_cfg_atc(spindle_data_t *data)
{
    /* Tool release / lock */
    if (*data->atc->cmd_free_tool_pin){
        /* Status check */
        if (*data->machine_active_pin){
            rtapi_print_msg(RTAPI_MSG_WARN,"[%s.%s] Machine is active, tool can't be unlocked !\n",SPINDLE_COMP_NAME,data->name);
            *data->atc->cmd_free_tool_pin = 0;
        } else if (*data->in_mode_csv_pin){
            rtapi_print_msg(RTAPI_MSG_WARN,"[%s.%s] Spindle is enabled, tool can't be unlocked !\n",SPINDLE_COMP_NAME,data->name);
            *data->atc->cmd_free_tool_pin = 0;
        } else {
            *data->atc->free_tool_pin = 1;
        }
    } else {
        *data->atc->free_tool_pin = 0;
    }

    /* Cone Cleaning */
    if (*data->atc->free_tool_pin == 1)
        *data->atc->clean_cone_pin = 1;
    else
        *data->atc->clean_cone_pin = 0;
    

    return;
}

static void spindle_can_turn(spindle_data_t *data)
{
    Spindle_speed_state_t ss = SPINDLE_SPEED_NOMINAL;

    /* Check Spindle Temperature - can override spindle config */
    if (data->has_temperature){
        if (data->temperature->state == SPINDLE_TEMP_OVERHEAT){
            ss = SPINDLE_SPEED_NOT;
        } else if (data->temperature->state == SPINDLE_TEMP_BELOW_MIN){
            ss = SPINDLE_SPEED_SLOW;
        } else {
            ss = SPINDLE_SPEED_NOMINAL;
        }
    }

    /* Check configuration */
    switch (data->cfg_id) {
    case SPINDLE_CFG_STD:
        if ( data->has_temperature){
            /* Keep state decided by temperature */
            ss = ss;
        } else {
            ss = SPINDLE_SPEED_NOMINAL;
        }
        break;
    case SPINDLE_CFG_ATC:
        if ( data->has_temperature ){
            if ( *data->atc->sens_0_pin && *data->atc->sens_1_pin && data->temperature->state != SPINDLE_TEMP_OVERHEAT){
                /* Keep state decided by temperature */
                ss = ss;
            } else {
                //rtapi_print_msg(RTAPI_MSG_ERR,"[%s.%s] ATC Spindle can't turn\n",SPINDLE_COMP_NAME,data->name);
                ss = SPINDLE_SPEED_NOT;
                data->state_speed = ss;
            }
        } else {
            if ( *data->atc->sens_0_pin && *data->atc->sens_1_pin ){
                ss = SPINDLE_SPEED_NOMINAL;
            } else {
                //rtapi_print_msg(RTAPI_MSG_ERR,"[%s.%s] ATC Spindle can't turn\n",SPINDLE_COMP_NAME,data->name);
                ss = SPINDLE_SPEED_NOT;
                data->state_speed = ss;
            }
        }
        break;
    }

    /* Store */
    data->state_speed = ss;
    /* Spindle can turn ? */
    switch (ss){
    case SPINDLE_SPEED_NOT : 
        *data->can_turn_pin = 0;
        break;
    case SPINDLE_SPEED_SLOW : 
        *data->can_turn_pin = 1;
        break;
    case SPINDLE_SPEED_NOMINAL : 
        *data->can_turn_pin = 1;
        break;
    }    
}

/***********************************************************************
 *                       OPTION SPECIFIC FUNC                          *
 ************************************************************************/

static void spindle_opt_cooling(spindle_data_t *data)
{
    /* Check if spindle has cooling option */
    if ( ! data->has_cooling){
        return;
    }
    
    /* Force cooling if overtemperature */
    if (data->has_temperature){
        if (data->temperature->state == SPINDLE_TEMP_OVERHEAT){
            *data->cooling->enable_pin = 1;
            return;
        }
    }

    /* If temperature option is present, we regulate the cooling pin */
    if ( data->has_temperature && *data->in_mode_csv_pin){
        /* Trigger cooling start */
        if( data->temperature->target_param + data->temperature->target_tol_param < *data->temperature->actual_pin){
            *data->cooling->enable_pin = 1;
        }
        
        /* Trigger cooling stop  */
        if (*data->cooling->enable_pin == 1 &&
            data->temperature->target_param - data->temperature->target_tol_param > *data->temperature->actual_pin){
            *data->cooling->enable_pin = 0;
        }
    } else {
        /* Only cooling option is present */
        /* Cooling active if spindle is enabled & temperature is not present */
        if (*data->in_mode_csv_pin)
            *data->cooling->enable_pin = 1;
        else
            *data->cooling->enable_pin = 0;
    }
}

static void spindle_opt_temperature(spindle_data_t *data)
{
    /* Check if spindle has temperature */
    if ( !data->has_temperature){
        data->temperature->state = SPINDLE_TEMP_OK;
        return;
    }
    
    /* Set temperature state */
    if (*data->temperature->actual_pin > data->temperature->max_param){
        printk_once("[%s.%s] Spindle overtemperature !\n",SPINDLE_COMP_NAME,data->name);
        data->temperature->state = SPINDLE_TEMP_OVERHEAT;
    } else if (*data->temperature->actual_pin < data->temperature->min_param){
        data->temperature->state = SPINDLE_TEMP_BELOW_MIN;
    } else {
        data->temperature->state = SPINDLE_TEMP_OK;
    }
}

/***********************************************************************
 *                       OTHER FUNC                                    *
 ************************************************************************/
static void spindle_manage_speed(spindle_data_t *data)
{
    /* Get applied velocity */
    data->cmpt_vel = *data->cmd_velocity_pin + ((*data->cmd_velocity_pin) * (*data->cmd_override_pin) / 100.0 );

    /* Check underspeed */
    if ( data->cmpt_vel < data->velocity_min_param && data->cmpt_vel != 0.0 ){
        /* Restore previous command */
        data->cmpt_vel = data->prev_cmpt_vel;
        *data->cmd_velocity_pin = data->prev_cmd_velocity;
        *data->cmd_override_pin = data->prev_cmd_override;

        rtapi_print_msg(RTAPI_MSG_ERR,"[%s.%s] Speed too low, cancel command\n",SPINDLE_COMP_NAME,data->name);
    }
    /* Check overspeed */
    if ( data->cmpt_vel > data->velocity_max_param ){
        /* Restore previous command */
        data->cmpt_vel = data->prev_cmpt_vel;
        *data->cmd_velocity_pin = data->prev_cmd_velocity;
        *data->cmd_override_pin = data->prev_cmd_override;

        rtapi_print_msg(RTAPI_MSG_ERR,"[%s.%s] Speed too high, cancel command\n",SPINDLE_COMP_NAME,data->name);
    }

    /* FSM speed */
    switch (data->state_speed){
    case SPINDLE_SPEED_NOT:
        *data->cmd_enable_pin = 0;
        data->state_elec = SPINDLE_STATE_INACTIVE;
        break;
    case SPINDLE_SPEED_SLOW:
        /* Check max speed under low temperature */
        if( data->cmpt_vel > data->temperature->max_vel_min_temp_param){
            rtapi_print_msg(RTAPI_MSG_ERR,"[%s.%s] Temperature too low for given speed, cancel command\n",SPINDLE_COMP_NAME,data->name);
            data->cmpt_vel = data->prev_cmpt_vel;
            *data->cmd_velocity_pin = data->prev_cmd_velocity;
            *data->cmd_override_pin = data->prev_cmd_override;
        } else {
            /* Asked velocity is ok for low temperature */
            if ( data->cmpt_vel != data->prev_cmpt_vel ){
                /* Store history */
                data->prev_cmpt_vel = data->cmpt_vel;
                data->prev_cmd_velocity = *data->cmd_velocity_pin;
                data->prev_cmd_override = *data->cmd_override_pin;
                /* Calculate transition */
                spindle_trans_init(data,SPINDLE_TRANS_NOMINAL);
                data->state_elec = SPINDLE_STATE_TRANSITION;
                rtapi_print_msg(RTAPI_MSG_INFO,"[%s.%s] New Velocity\n",SPINDLE_COMP_NAME,data->name);
            }
        }
        break;
    case SPINDLE_SPEED_NOMINAL:
        if ( data->cmpt_vel != data->prev_cmpt_vel ){
                /* Store history */
                data->prev_cmpt_vel = data->cmpt_vel;
                data->prev_cmd_velocity = *data->cmd_velocity_pin;
                data->prev_cmd_override = *data->cmd_override_pin;
                /* Calculate transition */
                spindle_trans_init(data,SPINDLE_TRANS_NOMINAL);
                data->state_elec = SPINDLE_STATE_TRANSITION;
                rtapi_print_msg(RTAPI_MSG_INFO,"[%s.%s] New Velocity\n",SPINDLE_COMP_NAME,data->name);
            }
        break;
    default:
        break;
    }

}

static bool spindle_is_zero_speed(spindle_data_t *data)
{
    if (*data->current_velocity_pin < SPINDLE_ZERO_SPEED_THRESH)
        return true;
    else
        return false;
}

static bool spindle_is_decelerating(spindle_data_t *data)
{
    if ((*data->current_velocity_pin - data->prev_act_veloctiy) < 0.0 )
        return true;
    else
        return false;
}


/***********************************************************************
 *                       FSM FUNC                                      *
 ************************************************************************/

static void spindle_fsm_inactive(spindle_data_t *data)
{
    /* Spindle enabling/disabling */
    if ( *data->cmd_enable_pin ){
        /* Manage speed state */
        switch (data->state_speed){
        case SPINDLE_SPEED_NOT:
            rtapi_print_msg(RTAPI_MSG_ERR,"[%s.%s] Spindle can't turn\n",SPINDLE_COMP_NAME,data->name);
            *data->cmd_enable_pin = 0;
            break;
        case SPINDLE_SPEED_SLOW:
        case SPINDLE_SPEED_NOMINAL:
            *data->set_mode_csv_pin = 1;
            data->state_elec = SPINDLE_STATE_ENABLED;
            break;
        default:
            break;
        }
    } else {
        /* Reset history */
        data->prev_cmpt_vel = 0.0;
        data->prev_cmd_override = 0.0;
        data->prev_cmd_velocity = 0.0;
        
        *data->set_mode_inactive_pin = 1;
    }
}

static void spindle_fsm_enabled(spindle_data_t *data)
{
    /* Set flag for zero speed */
    data->is_accel_from_zero = true;

    /* Spindle disabling */
    if ( *data->cmd_enable_pin == 0 || data->temperature->state == SPINDLE_TEMP_OVERHEAT){
        *data->set_mode_inactive_pin = 1;
        data->state_elec = SPINDLE_STATE_INACTIVE;
        return;
    }

    /* Wait for spindle to be in CSV */
    if( *data->in_mode_csv_pin == 0 )
        return;

    /* Manage speed state */
    spindle_manage_speed(data);
}

static void spindle_fsm_active(spindle_data_t *data)
{   
    /* Stop spindle ? */
    if ((*data->cmd_enable_pin == 0 || data->temperature->state == SPINDLE_TEMP_OVERHEAT)){
        /* Is spindle already at a stop ? */
        if ( spindle_is_zero_speed(data)) {
             data->state_elec = SPINDLE_STATE_ENABLED;
        } else if (spindle_is_decelerating(data)){
            /* Wait for spindle to decelerate 
             * As of 07.11.24 we only deal with high-end hardware spindle
             * If we use lower-cost encoder with unprecise reading of speed
             * this function COULD sometimes return 'false' 
             * even-tough the spindle is decelerating.
             * To correct, we could implement a mean value reading of the last X sample
             * to avoid this kind of error     
             */

            return;
        } else{
            rtapi_print_msg(RTAPI_MSG_INFO,"[%s.%s] Stopping spindle\n",SPINDLE_COMP_NAME,data->name);
            spindle_trans_init(data,SPINDLE_TRANS_STOP);
            data->state_elec = SPINDLE_STATE_TRANSITION;
        }

        return;
    }

    /* Manage speed state */
    spindle_manage_speed(data);

}

static void spindle_fsm_transition(spindle_data_t *data)
{
    double tar_vel;
    
    tar_vel = spindle_trans_get(data); 

    /* Applying csp */
    *data->target_velocity_pin = tar_vel;

    /* End transition ? */
    if (data->interp.is_done){
        rtapi_print_msg(RTAPI_MSG_INFO,"[%s.%s] Interp is done !\n",SPINDLE_COMP_NAME,data->name);
        /* Stop spindle rotation ? */
        if (*data->current_velocity_pin == 0)
            data->state_elec = SPINDLE_STATE_ENABLED;
        else
            data->state_elec = SPINDLE_STATE_ACTIVE;  
    }
}

static void spindle_fsm_fault(spindle_data_t *data)
{
    /* Reset all pin */
    *data->cmd_enable_pin = 0;
    *data->cmd_velocity_pin = 0;
    *data->cmd_override_pin = 0;
    *data->set_mode_csv_pin = 0;
    *data->set_mode_inactive_pin = 0;
    *data->can_turn_pin = 0;
    *data->velocity_reached_pin = 0;
    *data->target_velocity_pin = 0;
    data->prev_cmd_velocity = 0;
    data->prev_cmd_override = 0;
    data->prev_cmpt_vel = 0;

    /* Fault Reset */
    if (*data->cmd_fault_reset_pin){
        *data->fault_reset_pin = 1;
        *data->cmd_fault_reset_pin = 0;

        /* Wait state */
        data->state_elec = SPINDLE_STATE_FAULT_RESET_WAIT;
    }
}

static void spindle_fsm_fault_reset_wait(spindle_data_t *data)
{
    /* Wait for spindle to clear fault */
    if (*data->in_fault_pin == 0){
        /* Reset state */
        data->state_elec = SPINDLE_STATE_INACTIVE;
    }
}

/***********************************************************************
 *                       REALTIME FUNC                                 *
 ************************************************************************/

static void spindle_func(void *arg, long period)
{
    spindle_data_t *data = (spindle_data_t *)arg;

    /* Updating current time */
    data->dt = period / 1e9;
    data->cur_time = data->cur_time + data->dt;

    /* Fault state can override FSM state_elec */
    if (*data->in_fault_pin && data->state_elec != SPINDLE_STATE_FAULT_RESET_WAIT){
        data->state_elec = SPINDLE_STATE_FAULT;
    }
    
    /* Get speed state */
    spindle_can_turn(data);
    
    /* Manage option */
    spindle_opt_cooling(data);
    spindle_opt_temperature(data);

    /* Manage Confguration */
    switch (data->cfg_id){
    case SPINDLE_CFG_STD:
        /* Nothing to do */
        break;
    case SPINDLE_CFG_ATC:
        spindle_cfg_atc(data);
        break;
    case SPINDLE_CFG_UNKNOWN:
        /* Nothing to do */
        break;
    }

    /* Manage state */
    switch (data->state_elec){
    case SPINDLE_STATE_INACTIVE:
        spindle_fsm_inactive(data);
        break;
    case SPINDLE_STATE_ENABLED:
        spindle_fsm_enabled(data);
        break;
    case SPINDLE_STATE_ACTIVE:
        spindle_fsm_active(data);
        break;
    case SPINDLE_STATE_TRANSITION:
        spindle_fsm_transition(data);
        break;
    case SPINDLE_STATE_FAULT:
        spindle_fsm_fault(data);
        break;
    case SPINDLE_STATE_FAULT_RESET_WAIT:
        spindle_fsm_fault_reset_wait(data);
        break;
    default:
        rtapi_print_msg(RTAPI_MSG_ERR,"[%s.%s] Unknown state !\n",SPINDLE_COMP_NAME,data->name);
        break;
    }

    /* Air protection on if spindle on */
    if (*data->in_mode_csv_pin)
        *data->air_protection_pin = 1;
    else
        *data->air_protection_pin = 0;

    /* Spindle can't turn if in fault */
    if ( *data->in_fault_pin ){
        *data->can_turn_pin = 0;
    }

    /* Specific flag for acceleration from zero speed */
    if ( data->interp.t1 - data->interp.T / 2.0 > data->cur_time && data->is_accel_from_zero == true ){
        data->is_vel_reached_override = true;
    } else {
        data->is_vel_reached_override = false;
        data->is_accel_from_zero = false;
    }

    /* Set if spindle has reached its speed */
    if ( *data->in_fault_pin ){
        /* Not reached if in fault */
        *data->velocity_reached_pin = 0;
    } else if ( *data->in_mode_inactive_pin == 1 && *data->cmd_enable_pin == 0) {
        /* Reached if not asked to turn & inactive */
        *data->velocity_reached_pin = 1;
    } else if ( *data->in_mode_csv_pin == 0) {
        /* Not reached if not in csv */
        *data->velocity_reached_pin = 0;
    } else if ( *data->cmd_velocity_pin == 0.0 && !spindle_is_zero_speed(data)) {
        /* Not reached if asked to stop until zero speed */
        *data->velocity_reached_pin = 0;
    } else if ( data->is_vel_reached_override == true ){
        /* Not reached if accelerating from zero speed */
        *data->velocity_reached_pin = 0;
    } else if ( *data->current_velocity_pin >= data->cmpt_vel - data->cmpt_vel * (data->velocity_tolerance_param / 100.0) - SPINDLE_ZERO_SPEED_THRESH &&
                *data->current_velocity_pin <= data->cmpt_vel + data->cmpt_vel * (data->velocity_tolerance_param / 100.0) + SPINDLE_ZERO_SPEED_THRESH) {
        /* Spindle is in tolerance limit */
        *data->velocity_reached_pin = 1;
    } else {
        *data->velocity_reached_pin = 0;
    }

    /* Clear Fault pin */
    *data->cmd_fault_reset_pin = 0; 

    /* Update history */
    data->prev_act_veloctiy = *data->current_velocity_pin;

    /* Set debug pin */
#ifdef DEBUG_SPINDLE
    *data->spin_debug->state_elec_pin = data->state_elec;
    *data->spin_debug->state_speed_pin = data->state_speed;
#endif

    return;
}


/***********************************************************************
 *                       HAL INIT AND EXIT CODE                        *
 ************************************************************************/
static int spindle_init(spindle_data_t *data, char *comp_name)
{
    int ret;
    char buf[HAL_NAME_LEN];

    /* Export General pins & params */
	ret = hal_pin_newf_list(__core_hal_user, data->comp_id, data, spindle_pins, SPINDLE_COMP_NAME, data->name);
    if (ret != 0) {
        printk("[%s.%s] General PINs export failed\n", SPINDLE_COMP_NAME, data->name);
        return ret;
    }
    ret = hal_param_newf_list(__core_hal_user, data, data->comp_id, spindle_param, SPINDLE_COMP_NAME, data->name);
    if (ret != 0) {
        printk("[%s.%s] General PARAMs export failed\n", SPINDLE_COMP_NAME, data->name);
        return ret;
    }

    /* Export spin_debug pin */
#ifdef DEBUG_SPINDLE
    ret = hal_pin_newf_list(__core_hal_user, data->comp_id, data->spin_debug, spindle_debug_pins, SPINDLE_COMP_NAME, data->name);
    if (ret != 0) {
        printk("[%s.%s] Debug PINs export failed\n", SPINDLE_COMP_NAME, data->name);
        return ret;
    }
#endif

    /* Alloc mode specific pin & params */
    /* Configuration*/
    switch (data->cfg_id){
    case SPINDLE_CFG_STD:
        /* Nothing to do */
        break;
    case SPINDLE_CFG_ATC:
        ret = hal_pin_newf_list(__core_hal_user, data->comp_id, data->atc, spindle_atc_pins, SPINDLE_COMP_NAME, data->name);
        if (ret != 0) {
            printk("[%s.%s] ATC PINs export failed\n", SPINDLE_COMP_NAME, data->name);
            return ret;
        }
        break;
    default:
        /* Error handled by userspace at load */
        break;
    }
    
    if (data->has_temperature){
        /* Temperature pins */
        ret = hal_pin_newf_list(__core_hal_user, data->comp_id, data->temperature, spindle_temperature_pins, SPINDLE_COMP_NAME, data->name);
        if (ret != 0) {
            printk("[%s.%s] Temperature PINs export failed\n", SPINDLE_COMP_NAME, data->name);
            return ret;
        }
        /* Temperature params */
        ret = hal_param_newf_list(__core_hal_user, data->temperature, data->comp_id, spindle_temperature_param, SPINDLE_COMP_NAME, data->name);
        if (ret != 0) {
            printk("[%s.%s] Temperature PARAMs export failed\n", SPINDLE_COMP_NAME, data->name);
            return ret;
        }
    }

    if (data->has_cooling){
        /* Cooling pins */
        ret = hal_pin_newf_list(__core_hal_user, data->comp_id, data->cooling, spindle_cooling_pins, SPINDLE_COMP_NAME, data->name);
        if (ret != 0) {
            printk("[%s.%s] Cooling PINs export failed\n", SPINDLE_COMP_NAME, data->name);
            return ret;
        }
    }

	/* Export function */
    sprintf(buf, "%s.%s.%s", SPINDLE_COMP_NAME, data->name, "func");
    ret = hal_export_funct(__core_hal_user, buf, spindle_func, data, 1, 0, data->comp_id);
    if (ret != 0) {
        printk("[%s.%s] Error: function export failed\n", SPINDLE_COMP_NAME,data->name);
        return ret;
    }

    return 0;
}

static int spindle_app_main(spindle_connect_args_t *args)
{
    int ret, comp_id;
    char comp_name[HAL_NAME_LEN];
    spindle_data_t *data = NULL;

    sprintf(comp_name, "%s.%s", SPINDLE_COMP_NAME, args->name);
    comp_id = hal_init(__core_hal_user, comp_name);
    if (comp_id < 0) {
        rtapi_print_msg(RTAPI_MSG_ERR, "SPINDLE: ERROR: hal_init() failed\n");
        ret = -EINVAL;
        goto fail;
    }

    /* Alloc */ /* TODO : We alloc everything, eventhough it may not be used */
    data = hal_malloc(__core_hal_user, sizeof(spindle_data_t));
    if (!data) {
        printk("[%s.%s] Error: memory allocation failed on main data\n", SPINDLE_COMP_NAME, args->name);
        BUG();
    }
    memset(data, 0, sizeof(spindle_data_t));

#ifdef DEBUG_SPINDLE
    data->spin_debug = hal_malloc(__core_hal_user, sizeof(spindle_debug_t));
    if (!data->spin_debug) {
        printk("[%s.%s] Error: memory allocation failed on main spin_debug\n", SPINDLE_COMP_NAME, args->name);
        BUG();
    }
    memset(data->spin_debug, 0, sizeof(spindle_debug_t));
#endif

    data->temperature = hal_malloc(__core_hal_user, sizeof(spindle_temperature_t));
    if (!data->temperature) {
        printk("[%s.%s] Error: memory allocation failed on temperature\n", SPINDLE_COMP_NAME, args->name);
        BUG();
    }
    memset(data->temperature, 0, sizeof(spindle_temperature_t));

    data->cooling = hal_malloc(__core_hal_user, sizeof(spindle_cooling_t));
    if (!data->cooling) {
        printk("[%s.%s] Error: memory allocation failed on cooling\n", SPINDLE_COMP_NAME, args->name);
        BUG();
    }
    memset(data->cooling, 0, sizeof(spindle_cooling_t));

    data->atc = hal_malloc(__core_hal_user, sizeof(spindle_atc_t));
    if (!data->atc) {
        printk("[%s.%s] Error: memory allocation failed on atc\n", SPINDLE_COMP_NAME, args->name);
        BUG();
    }
    memset(data->atc, 0, sizeof(spindle_atc_t));

    /* Store parameter on kernel side */
    strcpy(data->name, args->name);
    data->comp_id           = comp_id;
    data->cfg_id            = args->cfg_id;
    data->has_temperature   = args->has_temperature;
    data->has_cooling       = args->has_cooling;
    data->instance_nr       = args->instance_nr;

    /* Init */
    data->state_elec = SPINDLE_STATE_INACTIVE;
    data->state_speed = SPINDLE_SPEED_NOT;
    data->prev_act_veloctiy = 0.0;
    data->cur_time = 0.0;
    data->velocity_tolerance_param = SPINDLE_VEL_TOL;
    if( data->has_temperature)
        data->temperature->target_tol_param = SPINDLE_TEMP_TOL;

    /* Create pins & params */
    ret = spindle_init(data, comp_name);
    if (ret != 0) {
        goto fail;
    }

    /* Store the new 'spindle' in the samples list */
    list_add_tail(&data->list, &spindles);

    hal_ready(__core_hal_user, comp_id);

    /* All went well -> increment the number of 'spindle' instances */
    spindle_instance_nr++;

    return 0;

fail:
    hal_exit(__core_hal_user, comp_id);
    return ret;
}


long spindle_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
    int rc = 0, major, minor;
    hal_user_t *hal_user;
    spindle_instance_nr_args_t data;

    major = imajor(filp->f_path.dentry->d_inode);
    minor = iminor(filp->f_path.dentry->d_inode);

    switch (cmd) {

        case SPINDLE_IOCTL_CONNECT:
            /* Pure kernel side init */
            rc = spindle_app_main((spindle_connect_args_t *) arg);
            if (rc) {
                printk("%s: failed to initialize...\n", __func__);
                goto out;
            }
            break;

        case SPINDLE_IOCTL_DISCONNECT:

            hal_user = find_hal_user_by_dev(major, minor);
            BUG_ON(hal_user == NULL);

            hal_exit(hal_user, hal_user->comp_id);

            break;

        case SPINDLE_IOCTL_INSTANCE_NR :
            data.instance_nr = spindle_instance_nr;
            if (copy_to_user((spindle_instance_nr_args_t *)arg, &data, sizeof(spindle_instance_nr_args_t))){
                return -EACCES;
            }
            break;
        
    }
    out:
    return rc;
}

ssize_t spindle_read(struct file *filp, char __user *_sample, size_t instance_nr, loff_t *off)
{
    return 0;
}

int spindle_open(struct inode *inode, struct file *file)
{
    return 0;
}

int spindle_release(struct inode *inode, struct file *filp)
{
    return 0;
}

struct file_operations spindle_fops = {
    .owner = THIS_MODULE,
    .open = spindle_open,
    .release = spindle_release,
    .unlocked_ioctl = spindle_ioctl,
    .read = spindle_read,
};

int spindle_comp_init(void) {

    printk("OpenCN: spindle subsystem initialization.\n");

    hal_create_interface(SPINDLE_COMP_NAME, &spindle_fops);

    return 0;
}

late_initcall(spindle_comp_init)