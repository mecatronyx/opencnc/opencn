/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2010 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: simple_pg.c
 *
 * It is the kernel part of the "simple_pg" OpenCN HAL component.
 *
 ********************************************************************/

#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>

#include <opencn/rtapi/rtapi_math.h>
#include <opencn/hal/hal.h>
#include <opencn/uapi/simple_pg.h>

/* Minimum Travel time in second */
#define SIMPLE_PG_TRAVEL_TIME_MIN  0.1

typedef struct interp {
	double x0, x1;   /* initial & final position */
	double t0;       /* start time */
	double T;        /* Travel time */
} interp_t;

typedef struct {
	/* PINs */
	hal_float_t *cmd_pos_pin;
	hal_float_t *current_pos_pin;
	hal_bit_t   *relative_pos_pin;
	hal_bit_t   *stop_pin;
	hal_float_t *target_position_pin;
	hal_bit_t   *is_running_pin;

	/* Params */
	hal_float_t speed_param;

	double cmd_pos_prev;
	interp_t interp;

} simple_pg_joint_data_t;

typedef struct {
	/* PINs */
	hal_bit_t   *enable_pin;
	hal_bit_t   *is_enable_pin;

	/* args */
	int joint_nr;

	simple_pg_joint_data_t *joint;

} simple_pg_data_t;

static const hal_pindesc_t simple_pg_joint_pins[] = {
	{ HAL_FLOAT, HAL_IN,  offsetof(simple_pg_joint_data_t, cmd_pos_pin),         "%s.joint%d.cmd-pos" },
	{ HAL_FLOAT, HAL_IN,  offsetof(simple_pg_joint_data_t, current_pos_pin),     "%s.joint%d.current-position" },
	{ HAL_BIT,   HAL_IN,  offsetof(simple_pg_joint_data_t, relative_pos_pin),    "%s.joint%d.relative-pos" },
	{ HAL_BIT,   HAL_IN,  offsetof(simple_pg_joint_data_t, stop_pin),            "%s.joint%d.stop" },
	{ HAL_FLOAT, HAL_OUT, offsetof(simple_pg_joint_data_t, target_position_pin), "%s.joint%d.target-position" },
	{ HAL_BIT,   HAL_OUT, offsetof(simple_pg_joint_data_t, is_running_pin),      "%s.joint%d.is_running" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t simple_pg_joint_params[] = {
	{ HAL_FLOAT, HAL_RW, offsetof(simple_pg_joint_data_t, speed_param), "%s.joint%d.speed" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t simple_pg_pins[] = {
	{ HAL_BIT,   HAL_IN,  offsetof(simple_pg_data_t, enable_pin),    "%s.enable" },
	{ HAL_BIT,   HAL_OUT, offsetof(simple_pg_data_t, is_enable_pin), "%s.is_enable" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static int comp_id; /* component ID */
static double cur_time = 0; /* in seconds */

/************************************************************************
 *                       PERIODIC RT FUNCTIONS                          *
 ************************************************************************/

static void simple_pg_init(interp_t *ip, double x0, double x1, double speed)
{
	ip->x0 = x0;
	ip->x1 = x1;
	ip->t0 = cur_time;
	ip->T = max((fabs(x1 - x0) / speed), SIMPLE_PG_TRAVEL_TIME_MIN);
}

static bool simple_pg_compute(interp_t *ip, double *output)
{
	double t = (cur_time - ip->t0) / ip->T; /* normalized time */

    if (t > 1.0) {
        *output = ip->x1;
        return false;
    } else {
        *output = ip->x0 + (1.0 - cos(t * M_PI)) * 0.5 * (ip->x1 - ip->x0);
        return true;
    }
}

static void simple_pg_func(void *arg, long period)
{
	simple_pg_data_t       *data = (simple_pg_data_t *)arg;
	simple_pg_joint_data_t *joint;
	double setpoint;
	double target_pos;
	double speed;
	double dt = 0;
	int i;
	static bool was_enabled = false;

	dt = period / 1e9;
	cur_time = cur_time + dt;

	if (*data->enable_pin) {
		/* PG is enable */

		for (i = 0; i < data->joint_nr; i++) {
			joint =  &data->joint[i];

			if (!was_enabled) {
				/* Initialization of the cmd and target position */
				*joint->target_position_pin = *joint->current_pos_pin;

				/* Reset the PG command PINs */
				*joint->is_running_pin = 0;
				*joint->stop_pin       = 0;

				joint->cmd_pos_prev = *joint->cmd_pos_pin;

			} else if 	(*joint->cmd_pos_pin != joint->cmd_pos_prev ) {
				/* New command --> Init profile */
				joint->cmd_pos_prev = *joint->cmd_pos_pin;

				/* convert unit/min to unite/sec */
				speed = joint->speed_param / 60.0;

				if (*joint->relative_pos_pin)
					target_pos = *joint->current_pos_pin + *joint->cmd_pos_pin;
				else
					target_pos = *joint->cmd_pos_pin;

				simple_pg_init( &joint->interp, *joint->current_pos_pin, target_pos, speed );

				*joint->is_running_pin = 1;

			} else if (*joint->stop_pin) {
				*joint->is_running_pin = 0;

			} else if (*joint->is_running_pin) {
				/* Compute profile */
				*joint->is_running_pin = simple_pg_compute(&joint->interp, &setpoint);

				if( *joint->is_running_pin )
					*joint->target_position_pin = setpoint;
			}

		/* Reset 'Stop' PIN for each joints */
		*joint->stop_pin = 0;

		was_enabled = true;
		}
	} else {
		/* Not enable */
		was_enabled = false;
	}

	*data->is_enable_pin = *data->enable_pin;
}

/************************************************************************
 *                       INIT AND EXIT CODE                             *
 ************************************************************************/

static int simple_pg_app_main(simple_pg_connect_args_t *arg)
{
	simple_pg_joint_data_t *joint_data;
	simple_pg_data_t *data;
	int ret = 0;
	int i;
	char buf[HAL_NAME_LEN];

	comp_id = hal_init(__core_hal_user, SIMPLE_PG_COMP_NAME);
	if (comp_id < 0) {
		printk("[%s] Error: hal_init() failed\n", SIMPLE_PG_COMP_NAME);
		return -EINVAL;
	}

	data = hal_malloc(__core_hal_user, sizeof(simple_pg_data_t));
	if (!data) {
		printk("[%s] Failed to allocate memory\n", SIMPLE_PG_COMP_NAME);
		goto fail;
	}
	memset(data, 0, sizeof(simple_pg_data_t));

	/* Store args received from usr-space */
	data->joint_nr = arg->joint_nr;

	/* joint memory allocation */
	joint_data = hal_malloc(__core_hal_user, sizeof(simple_pg_joint_data_t) * data->joint_nr);
	if (!joint_data) {
		printk("[%s] Failed to allocate 'joint' memory\n", SIMPLE_PG_COMP_NAME);
		goto fail;
	}
	memset(joint_data, 0, sizeof(simple_pg_joint_data_t) * data->joint_nr);
	data->joint = joint_data;

	/* export global pins */
	ret = hal_pin_newf_list(__core_hal_user, comp_id, data, simple_pg_pins, SIMPLE_PG_COMP_NAME);
	if (ret != 0) {
		printk("[%s] PINs export failed\n", SIMPLE_PG_COMP_NAME);
		goto fail;
	}

	for (i = 0; i < data->joint_nr; i++) {
		joint_data = &data->joint[i];

		/* export joint pins */
		ret = hal_pin_newf_list(__core_hal_user, comp_id, joint_data, simple_pg_joint_pins, SIMPLE_PG_COMP_NAME, i);
		if (ret != 0) {
			printk("[%s] Joint PINs export failed\n", SIMPLE_PG_COMP_NAME);
			goto fail;
		}

		ret = hal_param_newf_list(__core_hal_user, joint_data, comp_id, simple_pg_joint_params, SIMPLE_PG_COMP_NAME, i);
		if (ret != 0) {
			printk("[%s] PARAMs export failed\n", SIMPLE_PG_COMP_NAME);
			goto fail;
		}
		/* Default parameter values */
		joint_data->speed_param = 300;
	}

	/* export function */
	sprintf(buf, "%s.%s", SIMPLE_PG_COMP_NAME, "func");
	ret = hal_export_funct(__core_hal_user, buf, simple_pg_func, data, 1, 0, comp_id);
	if (ret != 0) {
		printk("[%s] Error: function export failed\n", SIMPLE_PG_COMP_NAME);
		goto fail;
	}

	hal_ready(__core_hal_user, comp_id);

	return 0;

fail:
	hal_exit(__core_hal_user, comp_id);

	return ret;
}


/************************************************************************
 *                Char Device & file operation definitions              *
 ************************************************************************/

static int simple_pg_open(struct inode *inode, struct file *file)
{
	return 0;
}

static int simple_pg_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static long simple_pg_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int rc = 0;

	switch (cmd) {
	case SIMPLE_PG_IOCTL_CONNECT:

		rc = simple_pg_app_main((simple_pg_connect_args_t *)arg);
		if (rc) {
			printk("%s: failed to initialize...\n", __func__);
			goto out;
		}

	case SIMPLE_PG_IOCTL_DISCONNECT:
		break;
	}

out:

	return rc;
}

struct file_operations simple_pg_fops = {
	.owner = THIS_MODULE,
	.open = simple_pg_open,
	.release = simple_pg_release,
	.unlocked_ioctl = simple_pg_ioctl,
};

int simple_pg_comp_init(void)
{
	printk("OpenCN: simple_pg subsystem initialization.\n");

	hal_create_interface(SIMPLE_PG_COMP_NAME, &simple_pg_fops);

	return 0;
}

late_initcall(simple_pg_comp_init)
