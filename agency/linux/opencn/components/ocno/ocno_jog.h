/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2010 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: ocno_core.h
 *
 * 'ocno' Jogging mode data & functions
 *
 ********************************************************************/

#ifndef OCNO_JOG_H
#define OCNO_JOG_H

#define OCNO_JOG_MODE_NAME       "jog"

typedef struct {
	hal_bit_t  *relative_pos_pin;
	hal_bit_t  *select_pin;
	hal_float_t *cmd_pos_pin;
	hal_bit_t  *is_running_pin;
	hal_bit_t  *spindle_enable;
} ocno_jog_axis_t;

typedef struct {
	/* Parent */
	void *parent; /* or 'parent' ? */

	/* Jogging PINs */
	hal_bit_t *start_pin;
	hal_bit_t *stop_pin;
	hal_bit_t *enable_pin;
	hal_bit_t *running_pin;
	hal_float_t *target_spindle_speed_pin;

	/*  Per Axis Jogging PINs */
	ocno_jog_axis_t *axis;
} ocno_jog_data_t;

static const hal_pindesc_t ocno_jog_pins[] = {

	{HAL_BIT,   HAL_IN,  offsetof(ocno_jog_data_t, start_pin),   "%s.%s.start"},
	{HAL_BIT,   HAL_IN,  offsetof(ocno_jog_data_t, stop_pin),    "%s.%s.stop"},
	{HAL_BIT,   HAL_OUT, offsetof(ocno_jog_data_t, enable_pin),  "%s.%s.enable"},
	{HAL_BIT,   HAL_OUT, offsetof(ocno_jog_data_t, running_pin), "%s.%s.running"},
	/* Spindle control */
	{HAL_FLOAT, HAL_IN,  offsetof(ocno_jog_data_t, target_spindle_speed_pin), "%s.%s.target-spindle-speed"},

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t ocno_jog_axis_pins[] = {

	{ HAL_BIT,   HAL_IN,  offsetof(ocno_jog_axis_t, relative_pos_pin), "%s.%s.axis%c.relative-pos" },
	{ HAL_BIT,   HAL_IN,  offsetof(ocno_jog_axis_t, select_pin),       "%s.%s.axis%c.select" },
	{ HAL_FLOAT, HAL_IN,  offsetof(ocno_jog_axis_t, cmd_pos_pin),      "%s.%s.axis%c.cmd-pos" },
	{ HAL_BIT,   HAL_IN,  offsetof(ocno_jog_axis_t, is_running_pin),   "%s.%s.axis%c.is_running" },


	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

/* Jogging mode initialization  */
void ocno_jog_init(void *parent, ocno_jog_data_t *data, int comp_id);

/* Jogging fsm execution */
void ocno_jog_fsm_exec(ocno_jog_data_t *data, bool new_mode);


void ocno_jog_enable(ocno_jog_data_t *data);
void ocno_jog_disable(ocno_jog_data_t *data);


#endif /* OCNO_JOG_H */
