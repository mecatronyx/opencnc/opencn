/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: ocno_machining.c
 *
 *
 * Support of the machining mode
 *
 ********************************************************************/

#include <soo/uapi/soo.h>
#include <linux/string.h>
#include <opencn/ctypes/strings.h>
#include <opencn/hal/hal.h>

#include "ocno_priv.h"


#define MSG_MAX_LEN 200
void debug_mac(const char* text, ...)
{
	#ifdef DEBUG_MACH
	#warning Only print when 'text' is changed
	va_list args;
	static char last_text[MSG_MAX_LEN];

	if (text == NULL) 
		return;

	/* Check max message length */
	if (strlen(text) > MSG_MAX_LEN){
		rtapi_print_msg(RTAPI_MSG_ERR, "[ocno/machining] Error: Message longer than %d,no print\n", MSG_MAX_LEN);
		return;
	}

	va_start(args, text);

	/* Check if UNFORMATTED text is different */
	if (strcmp(last_text, text) != 0 ){
		opencn_printf(text,args);
		//Save for next iteration
		strcpy(last_text, text);
	}

	va_end(args);
	#endif
}

enum {
	OCNO_MAC_STATE_INIT = 0,
	OCNO_MAC_STATE_WAIT_MODE,
	OCNO_MAC_STATE_WAIT_CMD_OPERATION,
	OCNO_MAC_STATE_WAIT_PATHPLANNER_SPINDLE_SPEED,
	OCNO_MAC_STATE_WAIT_SPINDLE_REACH_SPEED,
	OCNO_MAC_STATE_GO_CLEARING_POS,
	OCNO_MAC_STATE_RUN_CLEARING_POS,
	OCNO_MAC_STATE_ASK_START_POS,
	OCNO_MAC_STATE_GO_START_POS,
	OCNO_MAC_STATE_RUN_START_POS,
	OCNO_MAC_STATE_WAIT_CMD_MACHINING,
	OCNO_MAC_STATE_WAIT_START_DELAY,
	OCNO_MAC_STATE_RUN,
	OCNO_MAC_STATE_GO_FINAL_POS,
	OCNO_MAC_STATE_RUN_FINAL_POS,
	OCNO_MAC_STATE_WAIT_SPINDLE_ZERO_SPEED,
};


static void ocno_mac_fsm_init(void *args);
static void ocno_mac_fsm_wait_mode(void *args);
static void ocno_mac_fsm_wait_cmd_operation(void *args);
static void ocno_mac_fsm_wait_pathplanner_spindle_speed(void *args);
static void ocno_mac_fsm_wait_spindle_reach_speed(void *args);
static void ocno_mac_fsm_go_clearing_pos(void *args);
static void ocno_mac_fsm_run_clearing_pos(void *args);
static void ocno_mac_fsm_go_start_pos(void * args);
static void ocno_mac_fsm_run_start_pos(void * args);
static void ocno_mac_fsm_wait_cmd_machining(void * args);
static void ocno_mac_fsm_wait_start_delay(void * args);
static void ocno_mac_fsm_run(void * args);
static void ocno_mac_fsm_go_final_pos(void * args);
static void ocno_mac_fsm_run_final_pos(void * args);
static void ocno_mac_fsm_wait_spindle_zero_speed(void * args);


static ocno_state_fn_t ocno_mac_funcs[] = {
	[OCNO_MAC_STATE_INIT]               		= ocno_mac_fsm_init,
	[OCNO_MAC_STATE_WAIT_MODE]          		= ocno_mac_fsm_wait_mode,
	[OCNO_MAC_STATE_WAIT_CMD_OPERATION] 		= ocno_mac_fsm_wait_cmd_operation,
	[OCNO_MAC_STATE_WAIT_PATHPLANNER_SPINDLE_SPEED] = ocno_mac_fsm_wait_pathplanner_spindle_speed,
	[OCNO_MAC_STATE_WAIT_SPINDLE_REACH_SPEED] 	= ocno_mac_fsm_wait_spindle_reach_speed,
	[OCNO_MAC_STATE_GO_CLEARING_POS]    		= ocno_mac_fsm_go_clearing_pos,
	[OCNO_MAC_STATE_RUN_CLEARING_POS]   		= ocno_mac_fsm_run_clearing_pos,
	[OCNO_MAC_STATE_GO_START_POS]       		= ocno_mac_fsm_go_start_pos,
	[OCNO_MAC_STATE_RUN_START_POS]      		= ocno_mac_fsm_run_start_pos,
	[OCNO_MAC_STATE_WAIT_CMD_MACHINING] 		= ocno_mac_fsm_wait_cmd_machining,
	[OCNO_MAC_STATE_WAIT_START_DELAY]   		= ocno_mac_fsm_wait_start_delay,
	[OCNO_MAC_STATE_RUN]                		= ocno_mac_fsm_run,
	[OCNO_MAC_STATE_GO_FINAL_POS]       		= ocno_mac_fsm_go_final_pos,
	[OCNO_MAC_STATE_RUN_FINAL_POS]      		= ocno_mac_fsm_run_final_pos,
	[OCNO_MAC_STATE_WAIT_SPINDLE_ZERO_SPEED] 	= ocno_mac_fsm_wait_spindle_zero_speed,
};


static ocno_fsm_t ocno_mac_fsm = {
	.state = OCNO_MAC_STATE_INIT,
	.funcs = ocno_mac_funcs,
};

static void ocno_mac_reset_pin_value(ocno_mac_data_t  *data)
{
	*data->start_pin             = 0;
	*data->pause_pin             = 0;
	*data->resampling_pause_pin  = 0;
	*data->will_run_pin          = 0;
	*data->running_pin           = 0;
	*data->read_samples_pin      = 0;
	*data->finished_pin          = 0;
}


static void ocno_mac_fsm_init(void *args)
{
	ocno_mac_data_t  *data = (ocno_mac_data_t *)args;
	ocno_core_data_t *core = ocno_get_core_data(data->parent);

	RTAPI_PRINT_MSG(RTAPI_MSG_DBG, "[%s] Mode init\n", OCNO_COMP_NAME);
	/* Reset machining pin */
	ocno_mac_reset_pin_value(data);
#warning we still do something wrong at init here and the motor jump position
	/* PG preparation - set target position to current position */
	ocno_set_all_axes_current_pos(core);

	/* Set the drives in CSP mode */
	ocno_send_csp_cmd(core);

	ocno_set_machine_state(data->parent, OCNO_MACHINE_IDLE);
	ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_WAIT_MODE);
}

static void ocno_mac_fsm_wait_mode(void *args)
{
	ocno_mac_data_t *data = (ocno_mac_data_t *)args;
	ocno_core_data_t *core = ocno_get_core_data(data->parent);

	if (ocno_axis_in_csp(core)) {
		RTAPI_PRINT_MSG(RTAPI_MSG_DBG, "[%s] LEAVING WAIT MODE\n", OCNO_COMP_NAME);
		ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_WAIT_CMD_OPERATION);
	}
}

static void ocno_mac_fsm_wait_cmd_operation(void *args)
{
	ocno_mac_data_t  *data = (ocno_mac_data_t *)args;
	ocno_core_data_t *core = ocno_get_core_data(data->parent);

	ocno_set_machine_state(data->parent, OCNO_MACHINE_IDLE);

	if ((*data->operation_ready_pin) && (*data->start_pin)) {
		RTAPI_PRINT_MSG(RTAPI_MSG_DBG, "[%s] LEAVING WAIT CMD OPERATION\n", OCNO_COMP_NAME);

		*data->start_pin = 0;

		/* Init jog process */
		data->process_axis = 0;

		*data->get_one_sample_pin = 1;
		*data->resampling_pause_pin = 0;

		/* Set all axis to current position */
		ocno_set_all_axes_current_pos(core);

		/* Set the "path planing selector" to PG */
		ocno_datapath_selection(core, OCNO_DATAPATHPATH_PG);

		/* Enable PG */
		ocno_jog_enable(ocno_get_jog_data(data->parent));

		ocno_spindle_set(core, *core->spindle->rotation_request_pin);

		ocno_set_machine_state(data->parent, OCNO_MACHINE_RUNNING);

		if(ocno_spindle_rotation_required(core)) {
			ocno_set_spindle_speed(core, *data->target_spindle_speed_pin);
			ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_WAIT_PATHPLANNER_SPINDLE_SPEED);
			debug_mac("[ocno/machining] Spindle rotation required\n");

		} else {
			ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_GO_CLEARING_POS);
			debug_mac("[ocno/machining] Spindle rotation not required\n");
		}
	}
}

static void ocno_mac_fsm_wait_pathplanner_spindle_speed(void *args)
{
	ocno_mac_data_t  *data = (ocno_mac_data_t *)args;

	debug_mac("[ocno/machining] Waiting for pathplanner to give spindle speed\n");

	if (*data->target_spindle_speed_pin != 0.0) {
		debug_mac("[ocno/machining] Set target spindle speed\n");
		ocno_set_spindle_speed(ocno_get_core_data(data->parent), *data->target_spindle_speed_pin);
		ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_WAIT_SPINDLE_REACH_SPEED);
	}
}

static void ocno_mac_fsm_wait_spindle_reach_speed(void *args)
{
	ocno_mac_data_t  *data = (ocno_mac_data_t *)args;
	ocno_core_data_t *core = ocno_get_core_data(data->parent);

	debug_mac("[ocno/machining] Waiting for spindle to reach speed\n");

	if (is_spindle_speed_reached(core)) {
		debug_mac("[ocno/machining] Target speed is high enough\n");
		ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_GO_CLEARING_POS);
	}
}


static void ocno_mac_fsm_go_clearing_pos(void *args)
{
	ocno_mac_data_t *data = (ocno_mac_data_t *)args;
	ocno_mac_axis_t *axis = &data->axis[data->process_axis];
	ocno_core_data_t *core = ocno_get_core_data(data->parent);

	*data->running_pin = 1;

	ocno_set_axis_abs_target(core, data->process_axis, *axis->clearing_pos_pin);
	ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_RUN_CLEARING_POS);
	debug_mac("[ocno/machining] Leave clearing position at %5.2fmm \n", *axis->clearing_pos_pin);

}


static void ocno_mac_fsm_run_clearing_pos(void *args)
{
	ocno_mac_data_t *data = (ocno_mac_data_t *)args;
	ocno_jog_data_t *jog = ocno_get_jog_data(data->parent);
	ocno_jog_axis_t *jog_axis = &jog->axis[data->process_axis];
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);
	
	if (!*jog_axis->is_running_pin) {
		if (data->process_axis == axes_info->axis_nr - 1) {
			/* All axes are in their start position */

			/* prepare for the next jog process */
			data->process_axis = 0;

			ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_GO_START_POS);
			debug_mac("[ocno/machining] Go to start pos\n");

		} else {
			/* Set next axis to its start position */
			debug_mac("[ocno/machining] Go back to clearing pos axis%d \n", data->process_axis);
			data->process_axis++;			

			ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_GO_CLEARING_POS);
		}
	}
}


static void ocno_mac_fsm_go_start_pos(void * args)
{
	ocno_mac_data_t *data = (ocno_mac_data_t *)args;
	ocno_mac_axis_t *axis = &data->axis[data->process_axis];
	ocno_core_data_t *core = ocno_get_core_data(data->parent);
	double cmd_pos;
	cmd_pos = *axis->cmd_pos_pin;

	ocno_set_axis_abs_target(core, data->process_axis, cmd_pos);
	ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_RUN_START_POS);
}

static void ocno_mac_fsm_run_start_pos(void * args)
{
	ocno_mac_data_t *data = (ocno_mac_data_t *)args;
	ocno_jog_data_t *jog = ocno_get_jog_data(data->parent);
	ocno_jog_axis_t *jog_axis = &jog->axis[data->process_axis];
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);
	debug_mac("[ocno/machining] Run start pos state\n");

	if (! *jog_axis->is_running_pin) {

		if (data->process_axis == axes_info->axis_nr - 1) {
			/* All axes are in their start position */

			//ocno_jog_disable(ocno_get_jog_data(data->parent)); // Temporary fix avoid jump on ocno_mac_fsm_run_final_pos because simple_pg had not enough time to set target-pos

			ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_WAIT_CMD_MACHINING);

		} else {
			/* Set next axis to its start position */
			data->process_axis++;
			ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_GO_START_POS);
		}
	}
}


static void ocno_mac_fsm_wait_cmd_machining(void * args)
{
	ocno_mac_data_t *data = (ocno_mac_data_t *)args;
	ocno_core_data_t *core = ocno_get_core_data(data->parent);

	ocno_set_machine_state(data->parent, OCNO_MACHINE_IDLE);

	if (*data->start_pin) {
		debug_mac("[ocno/machining] Wait command machining\n");

		ocno_set_machine_state(data->parent, OCNO_MACHINE_RUNNING);

		data->start_delay = rtapi_get_time();

		*data->start_pin = 0;

		*data->will_run_pin = 1;

		/* Set the "path planing selector" to Direct / RAW (no PG) */
		ocno_datapath_selection(core, OCNO_DATAPATHPATH_DIRECT);

		ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_WAIT_START_DELAY);
	}
}

static void ocno_mac_fsm_wait_start_delay(void * args)
{
	ocno_mac_data_t *data = (ocno_mac_data_t *)args;
	uint64_t now = rtapi_get_time();
	debug_mac("[ocno/machining] Start delay\n");

	if ( now - data->start_delay >= MILLISECS(*data->start_delay_pin)) { /* Pin in milliseconds unit */

		/* Start the 'read sample' process from path planning */
		*data->read_samples_pin = 1;
		ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_RUN);
	}
}


static void ocno_mac_fsm_run(void * args)
{
	ocno_mac_data_t  *data = (ocno_mac_data_t *)args;
	ocno_mac_axis_t  *axis;
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);
	ocno_core_data_t *core = ocno_get_core_data(data->parent);
	ocno_core_axis_t *core_axis;
	int i;

	debug_mac("[ocno/machining] run\n");

	/* Machine in 'pause' --> don't perform anything */
	if (*data->pause_pin) {
		/* FIX TODO : CHECK */
		*data->resampling_pause_pin = 1;
		return;
	} else {
		*data->resampling_pause_pin = 0;
	}

	if (*data->finished_pin) {

		*data->will_run_pin = 0;

		/* Init jog process */
		data->process_axis = 0;

		ocno_jog_enable(ocno_get_jog_data(data->parent));

		/* Set the "path planing selector" to PG */
		ocno_datapath_selection(core, OCNO_DATAPATHPATH_PG);

		ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_GO_FINAL_POS);

		return;
	}

	if (*data->underrun_pin) {
		/* What to do... */
		debug_mac("[ocno/machining] underrun detected...\n");
		ocno_quick_stop_enable(core);
		ocno_error_enable(core);
	}

	/* Check if spindle RPM is high enough, if not, a quick stop is triggered*/
	if(ocno_spindle_rotation_required(core)) {
		if (!is_spindle_speed_reached(core)) {
			rtapi_print_msg(RTAPI_MSG_ERR,"[ocno/machining] Spindle RPM is too low ! Triggering Quick stop\n");
			ocno_quick_stop_enable(core);
			ocno_error_enable(core);
		}
	}

	for (i = 0; i < axes_info->axis_nr ; i++) {
		axis      = &data->axis[i];
		core_axis = &core->axis[i];

		ocno_set_axis_abs_target(core, i, *axis->cmd_pos_pin);
	}
}


static void ocno_mac_fsm_go_final_pos(void * args)
{
	ocno_mac_data_t *data = (ocno_mac_data_t *)args;
	ocno_mac_axis_t *axis = &data->axis[data->process_axis];
	ocno_core_data_t *core = ocno_get_core_data(data->parent);
	ocno_set_axis_abs_target(core, data->process_axis, *axis->clearing_pos_pin);
	ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_RUN_FINAL_POS);
	
}


static void ocno_mac_fsm_run_final_pos(void * args)
{
	ocno_mac_data_t *data = (ocno_mac_data_t *)args;
	ocno_core_data_t *core = ocno_get_core_data(data->parent);
	ocno_jog_data_t *jog = ocno_get_jog_data(data->parent);
	ocno_jog_axis_t *jog_axis = &jog->axis[data->process_axis];
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);

	debug_mac("[ocno/machining]Run final pos\n");

	if (!*jog_axis->is_running_pin) {

		if (data->process_axis == axes_info->axis_nr - 1) {
			/* All axes are in their start position */
			debug_mac("[ocno/machining] Go to debug, disabling PG\n");
			/* Ask spindle stop */
			debug_mac("[ocno/machining] Spindle set zero speed & disable\n");
			ocno_set_spindle_speed(core, 0.0);
			ocno_spindle_set(core, false);

			ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_WAIT_SPINDLE_ZERO_SPEED);

			ocno_jog_disable(ocno_get_jog_data(data->parent));

		} else {
			/* Set next axis to its start position */
			debug_mac("[ocno/machining] Set next axis to its start position\n");

			data->process_axis++;
			ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_GO_FINAL_POS);
		}
	}
}

static void ocno_mac_fsm_wait_spindle_zero_speed(void * args)
{
	ocno_mac_data_t *data = (ocno_mac_data_t *)args;
	ocno_core_data_t *core = ocno_get_core_data(data->parent);

	if(is_spindle_speed_reached(core)){
		*data->running_pin  = 0;
		ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_WAIT_CMD_OPERATION);
		debug_mac("[ocno/machining] Spindle is at speed zero\n");
	}
}



/* Machining fsm execution */
void ocno_mac_fsm_exec(ocno_mac_data_t *data, bool new_mode)
{

	if (*data->reset_pin) {
		/* Reset Operation */
		ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_GO_FINAL_POS);
	}

	if (new_mode)
		/* new mode -->  initialize the fsm */
		ocno_fsm_change_state(&ocno_mac_fsm, OCNO_MAC_STATE_INIT);

	if (ocno_is_homed(ocno_get_homing_data(data->parent))) {
		/* execute state */
		ocno_fsm_execute(&ocno_mac_fsm, data);
	} else {
		ocno_error_enable(data->parent);
		debug_mac("[ocno/machining] Machining mode error, system is not homed...\n");
	}

	/* Update state */
	#ifdef DEBUG_MACH
	ocno_mac_debug_t *data_d = (ocno_mac_debug_t *)data->debug;
	*data_d->d_state_pin = ocno_mac_fsm.state;
	#endif
}


static void ocno_mac_create_pins(ocno_mac_data_t *data, ocno_axes_info_t *axes_info, int comp_id)
{
	int i;
	int ret;
	ocno_mac_axis_t *axis;

	ret = hal_pin_newf_list(__core_hal_user, comp_id, data, ocno_mac_pins, OCNO_COMP_NAME, OCNO_MACHINING_MODE_NAME);

	for (i =0; i < axes_info->axis_nr; i++) {
		debug_mac("[ocno/machining] Axis in create pins : %d\n", i);
		axis = &data->axis[i];
		ret = hal_pin_newf_list(__core_hal_user, comp_id, axis, ocno_mac_axis_pins, OCNO_COMP_NAME, OCNO_MACHINING_MODE_NAME, axes_info->axes[i]);
	}

	/* Export debug pin */
	#ifdef DEBUG_MACH
	ret = hal_pin_newf_list(__core_hal_user, comp_id, data->debug, ocno_mac_debug_pins, OCNO_COMP_NAME, OCNO_MACHINING_MODE_NAME);
	#endif
}

/* Machining mode initialization  */
void ocno_mac_init(void *parent, ocno_mac_data_t *data, int comp_id)
{
	ocno_mac_axis_t *axis = NULL;
	ocno_axes_info_t *axes_info;
	ocno_data_t *ocno = (ocno_data_t *)parent;

	data = hal_malloc(__core_hal_user, sizeof(ocno_mac_data_t));
	if (!data) {
		printk("[%s/%s] Error: memory allocation failed\n", OCNO_COMP_NAME, OCNO_MACHINING_MODE_NAME);
		BUG();
	}

	data->parent = parent;
	ocno->mac    = data;

	/* Set the axis order reference - used to set offset to correct axis */

	axes_info = ocno_get_axes_info(parent);

	axis = hal_malloc(__core_hal_user, sizeof(ocno_mac_axis_t) * axes_info->axis_nr);
	if (!axis) {
		printk("[%s/%s] Error: PINs per axis data allocation failed\n", OCNO_COMP_NAME, OCNO_MACHINING_MODE_NAME);
		BUG();

	}
	data->axis = axis;

	/* Malloc debug data */
	#ifdef DEBUG_MACH
	data->debug = hal_malloc(__core_hal_user, sizeof(ocno_mac_debug_t));
	if (!data->debug) {
		printk("[%s/%s] Error: memory allocation failed\n", OCNO_COMP_NAME, OCNO_MACHINING_MODE_NAME);
		BUG();
	}
	#endif

	ocno_mac_create_pins(data, axes_info, comp_id);
}
