/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2010 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: ocno_machining.h
 *
 * 'ocno' Machining data & functions
 *
 ********************************************************************/

#ifndef OCNO_MACHINING_H
#define OCNO_MACHINING_H

#include <opencn/uapi/kinematic.h>

#define OCNO_MACHINING_MODE_NAME "machining"

/* Debug Machining FSM */
#if 0
#define DEBUG_MACH
#endif

typedef struct{
	/* Debug pin */
	hal_u32_t *d_state_pin;
} ocno_mac_debug_t;

typedef struct {
	hal_float_t *cmd_pos_pin;
	hal_float_t *clearing_pos_pin;
} ocno_mac_axis_t;

typedef struct {
	/* Parent */
	void *parent;

	/* Machining PINs */
	hal_bit_t *start_pin;
	hal_bit_t *pause_pin;
	hal_bit_t *resampling_pause_pin;
	hal_bit_t *reset_pin;

	hal_bit_t *operation_ready_pin;
	hal_bit_t *will_run_pin;
	hal_bit_t *running_pin;
	hal_bit_t *get_one_sample_pin;
	hal_bit_t *read_samples_pin;

	hal_u32_t *start_delay_pin;

	hal_bit_t *finished_pin;
	hal_bit_t *underrun_pin;

	hal_float_t *spindle_speed_value_pin;
	hal_float_t *target_spindle_speed_pin;

	/*  Per Axis Machining PINs */
	ocno_mac_axis_t *axis;

	/* Processing variables */
	unsigned process_axis;
	uint64_t start_delay;
	
	/* Debug pins */
	#ifdef DEBUG_MACH
	ocno_mac_debug_t *debug;
	#endif
} ocno_mac_data_t;

static const hal_pindesc_t ocno_mac_axis_pins[] = {

	{ HAL_FLOAT, HAL_IN, offsetof(ocno_mac_axis_t, cmd_pos_pin),   "%s.%s.axis%c.cmd-pos" },
	{ HAL_FLOAT, HAL_IN, offsetof(ocno_mac_axis_t, clearing_pos_pin), "%s.%s.axis%c.clearing-pos" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t ocno_mac_pins[] = {

	/* Generic Control */
	{ HAL_BIT, HAL_IN,  offsetof(ocno_mac_data_t, start_pin), "%s.%s.start" },
	{ HAL_BIT, HAL_IN,  offsetof(ocno_mac_data_t, pause_pin), "%s.%s.pause" },
	{ HAL_BIT, HAL_OUT, offsetof(ocno_mac_data_t, resampling_pause_pin), "%s.%s.resampling-pause" },
	{ HAL_BIT, HAL_IN,  offsetof(ocno_mac_data_t, reset_pin), "%s.%s.reset" },

	{ HAL_BIT, HAL_IN,  offsetof(ocno_mac_data_t, operation_ready_pin), "%s.%s.operation-ready" },
	{ HAL_BIT, HAL_OUT, offsetof(ocno_mac_data_t, will_run_pin), "%s.%s.will-run" },
	{ HAL_BIT, HAL_OUT, offsetof(ocno_mac_data_t, running_pin), "%s.%s.running" },
	{ HAL_BIT, HAL_OUT, offsetof(ocno_mac_data_t, get_one_sample_pin), "%s.%s.get-one-sample" },
	{ HAL_BIT, HAL_OUT, offsetof(ocno_mac_data_t, read_samples_pin), "%s.%s.read-samples" },

	{ HAL_U32, HAL_IN, offsetof(ocno_mac_data_t, start_delay_pin), "%s.%s.start-delay" },

	{ HAL_BIT, HAL_IN,  offsetof(ocno_mac_data_t, finished_pin), "%s.%s.finished" },
	{ HAL_BIT, HAL_IN,  offsetof(ocno_mac_data_t, underrun_pin), "%s.%s.underrun" },

	{ HAL_FLOAT, HAL_IN, offsetof(ocno_mac_data_t, target_spindle_speed_pin), "%s.%s.target-spindle-speed" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

/* Debug pin */
static const hal_pindesc_t ocno_mac_debug_pins[] = {

	{ HAL_U32, HAL_OUT,  offsetof(ocno_mac_debug_t, d_state_pin), "%s.%s.deb.state" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

/* Machining mode initialization  */
void ocno_mac_init(void *parent, ocno_mac_data_t *data, int comp_id);

/* Machining fsm execution */
void ocno_mac_fsm_exec(ocno_mac_data_t *data, bool new_mode);

#endif /* OCNO_MACHINING_H */
