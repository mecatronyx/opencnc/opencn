/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2010 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: ocno_priv.h
 *
 * 'ocno' private header file
 *
 ********************************************************************/

#ifndef OCNO_PRIV_H
#define OCNO_PRIV_H

#include <opencn/uapi/hal.h>
#include <opencn/uapi/ocno.h>

#include "ocno_core.h"
#include "ocno_homing.h"
#include "ocno_jog.h"
#include "ocno_machining.h"

/* Current machine state Running or not */
typedef enum {
	OCNO_MACHINE_IDLE = 0,
	OCNO_MACHINE_RUNNING,
} ocno_machine_state_t;


/* FSM state callback format */
typedef void (*ocno_state_fn_t)(void *args);

/* FSM type
 *
 * funcs: Array with state callback functions
 * state: Current FSM state
 */
typedef struct {
	ocno_state_fn_t *funcs;
	int    state;
} ocno_fsm_t;


/* Axis info */
typedef struct {
	char axes[HAL_NAME_LEN];
	unsigned axis_nr;
} ocno_axes_info_t;


/* Ocno main data */
typedef struct {

	/* Modes data */
	ocno_core_data_t   *core;
	ocno_homing_data_t *homing;
	ocno_jog_data_t    *jog;
	ocno_mac_data_t    *mac;

	/* Generic data */
	ocno_axes_info_t     info;
	unsigned             mode;
	ocno_machine_state_t state;
} ocno_data_t;


/* ocno_fsm_change_state - Change FSM state
 */
void ocno_fsm_change_state(ocno_fsm_t *fsm, int new_state);

/* Execute callback function of the current state */
void ocno_fsm_execute(ocno_fsm_t *fsm, void *data);

/* Retrieve info an the axes */
ocno_axes_info_t *ocno_get_axes_info(ocno_data_t *data);


/* Set current working mode */
void ocno_set_mode(ocno_data_t *data, unsigned mode);

/* Retrieve current working mode */
unsigned ocno_get_mode(ocno_data_t *data);

/*  Set the machine state (running or idle) */
void ocno_set_machine_state(ocno_data_t *data, ocno_machine_state_t state);

/*  Retrieve the machine state (running or idle) */
ocno_machine_state_t ocno_get_machine_state(ocno_data_t *data);


ocno_core_data_t   *ocno_get_core_data(ocno_data_t *data);
ocno_homing_data_t *ocno_get_homing_data(ocno_data_t *data);
ocno_jog_data_t    *ocno_get_jog_data(ocno_data_t *data);

#endif /* OCNO_PRIV_H */
