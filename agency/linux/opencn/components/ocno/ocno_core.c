/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: ocno_core.c
 *
 *
 * Support of the core features (not specific to a working mode).
 *
 ********************************************************************/

#include <linux/string.h>
#include <opencn/ctypes/strings.h>


#include <opencn/hal/hal.h>
#include <opencn/uapi/ocno.h>

#include "ocno_priv.h"

/* Return the mode in string format */
static char *ocno_mode_str(unsigned mode)
{
	switch (mode) {
	case OCNO_MODE_INACTIVE:
		return OCNO_INACTIVE_MODE_NAME;
	case OCNO_MODE_HOMING:
		return OCNO_HOMING_MODE_NAME;
	case OCNO_MODE_JOG:
		return OCNO_JOG_MODE_NAME;
	case OCNO_MODE_MACHINING:
		return OCNO_MACHINING_MODE_NAME;
	default:
		return "invalid";
	}
}

/* ocno_position_in_limits - check the 'position' is in the software limits
 * of the machine
 *
 * return true is the position is in the limits or false otherwise
 */
static bool ocno_position_in_limits(ocno_core_axis_t *axis, double position, char axis_name)
{
	if ((axis->limit_min_param != 0) && (position < axis->limit_min_param)) {
		opencn_printf("[%s] Minimum limit reached on axis %c (min: %g, position: %g)\n", OCNO_COMP_NAME, axis_name, axis->limit_min_param, position);
		return false;

	} else if ((axis->limit_max_param != 0) && (position > axis->limit_max_param)) {
		opencn_printf("[%s] Maximum limit reached on axis %c (max: %g, position: %g)\n", OCNO_COMP_NAME, axis_name, axis->limit_max_param, position);
		return false;
	}

	return true;
}

/* Check if an axis is in fault
 *
 * return true if an axis is in fault, false otherwise
*/
static bool ocno_machine_in_fault(ocno_core_data_t *data)
{
	int i;
	ocno_core_axis_t *axis;
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);

	for (i = 0; i < axes_info->axis_nr; i++) {
		axis = &data->axis[i];

		if (*axis->in_fault_pin) {
			*data->machine_in_fault_pin = 1;
			return true;
		}
	}

	*data->machine_in_fault_pin = 0;

	return false;
}

static void ocno_send_reset_cmd(ocno_core_data_t *data)
{
	int i;
	ocno_core_axis_t *axis;
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);

	/* Axis fault */
	for (i = 0; i < axes_info->axis_nr; i++) {
		axis = &data->axis[i];

		*axis->fault_reset_pin = 1;
	}

	/* Spindle fault */
	*data->spindle->fault_reset_pin = 1;

}

/* ocno_send_inactive_cmd - Send commands to set the machine in inactive mode
*/
static void ocno_send_inactive_cmd(ocno_core_data_t *data)
{
	int i;
	ocno_core_axis_t *axis;
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);

	for (i = 0; i < axes_info->axis_nr; i++) {
		axis = &data->axis[i];

		*(axis->set_mode_inactive_pin) = 1;
	}
}


/* ocno_send_homing_cmd - Send commands to set the machine in homing mode
*/
void ocno_send_homing_cmd(ocno_core_data_t *data)
{
	int i;
	ocno_core_axis_t *axis;
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);

	for (i = 0; i < axes_info->axis_nr; i++) {
		axis = &data->axis[i];

		// printk("== debug - axis%d - set homing mode\n", i);
		/* Check if axis is already in homing mode */
		if(!*(axis->in_mode_homing_pin)) {
			*(axis->set_mode_homing_pin) = 1;
		}	
	}
}

/* ocno_axis_in_homing - check if all axis are in homing mode
 *
 * return true if all axis are in homing mode, false otherwise
*/
bool ocno_axis_in_homing(ocno_core_data_t *data)
{
	int i;
	ocno_core_axis_t *axis;
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);

	for (i = 0; i < axes_info->axis_nr; i++) {
		axis = &data->axis[i];

		if (! *axis->in_mode_homing_pin)
			return false;
	}

	return true;
}


void ocno_set_axis_csp_mode(ocno_core_data_t *data, int axis_idx)
{
	ocno_core_axis_t *axis = &data->axis[axis_idx];

	if(!*axis->in_mode_csp_pin) {
		*(axis->set_mode_csp_pin) = 1;
	}
}


/* ocno_send_csp_cmd - Send commands to set the machine in csp mode
*/
void ocno_send_csp_cmd(ocno_core_data_t *data)
{
	int i;
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);

	for (i = 0; i < axes_info->axis_nr; i++) {
		ocno_set_axis_csp_mode(data, i);
	}
}


/* ocno_axis_in_csp - check if all axis are in csp mode
 *
 * return true if all axis are in csp mode, false otherwise
*/
bool ocno_axis_in_csp(ocno_core_data_t *data)
{
	int i;
	ocno_core_axis_t *axis;
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);

	for (i = 0; i < axes_info->axis_nr; i++) {
		axis = &data->axis[i];

		if (! *axis->in_mode_csp_pin)
			return false;
	}

	return true;
}

void ocno_set_inactive_mode(ocno_core_data_t *data)
{
	ocno_send_inactive_cmd(data);
	ocno_set_mode(data->parent, OCNO_MODE_INACTIVE);
}

void ocno_set_axis_rel_target(ocno_core_data_t *data, int axis_idx, double cmd_pos)
{
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);
	ocno_core_axis_t *axis = &data->axis[axis_idx];
	double target_pos = (*axis->current_joint_pos_pin + cmd_pos);

	/* Add the offset to the 'cmd_pos' */
	target_pos += *axis->position_offset_pin;

	if (ocno_position_in_limits(axis, target_pos, axes_info->axes[axis_idx]))
		*axis->target_pos_pin = target_pos;
	else
		ocno_set_inactive_mode(data);
}

void ocno_set_axis_abs_target(ocno_core_data_t *data, int axis_idx, double cmd_pos)
{
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);
	ocno_core_axis_t *axis = &data->axis[axis_idx];
	double target_pos;
	// opencn_printf("[ocno/core] set axis abs target - axis : %d\n", axis_idx);
	// opencn_printf("[ocno/core] set axis abs target - cmd pos : %g\ns", cmd_pos);
	/* Add the offset to the 'cmd_pos' */
	target_pos = cmd_pos + *axis->position_offset_pin;

	if (ocno_position_in_limits(axis, target_pos, axes_info->axes[axis_idx]))
		*axis->target_pos_pin = target_pos;
	else
		ocno_set_inactive_mode(data);
}

void ocno_set_axis_current_pos(ocno_core_data_t *data, int axis_idx)
{
	ocno_core_axis_t *axis = &data->axis[axis_idx];

	*axis->target_pos_pin = *axis->current_joint_pos_pin;
}

void ocno_set_all_axes_current_pos(ocno_core_data_t *data)
{
	int i;
	ocno_core_axis_t *axis;
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);

	for (i = 0; i < axes_info->axis_nr; i++) {
		rtapi_print_msg(RTAPI_MSG_DBG,"[ocno/core] set all axis current pos - axis : %d\n", i);
		axis = &data->axis[i];

		*axis->target_pos_pin = *axis->current_joint_pos_pin;
	}
}

void ocno_spindle_set(ocno_core_data_t *data, bool state)
{
	*data->spindle->enable_pin = state;
}


void ocno_quick_stop_enable(ocno_core_data_t *data)
{
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);
	int i;
	for (i = 0; i < axes_info->axis_nr; i++) {
		*data->axis->quick_stop_pin = 1;
	}
}

void ocno_quick_stop_disable(ocno_core_data_t *data)
{
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);
	int i;
	for (i = 0; i < axes_info->axis_nr; i++) {
		*data->axis->quick_stop_pin = 0;
	}
}

bool ocno_can_spindle_turn(ocno_core_data_t *data)
{
	return *data->spindle->can_turn_pin;
}

bool ocno_spindle_rotation_required(ocno_core_data_t *data)
{
	return *data->spindle->rotation_request_pin;
}

bool ocno_spindle_is_safe(ocno_core_data_t *data)
{
	if (ocno_spindle_rotation_required(data) && !ocno_can_spindle_turn(data)) {
		printk_once("[%s] Spindle enable, but spindle can't turn !\n", OCNO_COMP_NAME);
		ocno_error_enable(data);
		return false;
	}
	return true;
}

void ocno_set_spindle_speed(ocno_core_data_t *data, hal_float_t speed)
{
	*data->spindle->speed_pin = speed;
}

bool is_spindle_speed_reached(ocno_core_data_t *data)
{
	return *data->spindle->velocity_reached_pin;
}

void ocno_datapath_selection(ocno_core_data_t *data, ocno_datapath_t sel)
{
	*data->datapath_sel_pin = sel;
}

void ocno_error_enable(ocno_core_data_t *data)
{
	data->ocno_is_in_error = true;
}

void ocno_error_disable(ocno_core_data_t *data)
{
	data->ocno_is_in_error = false;
}


ocno_datapath_t ocno_datapath_status(ocno_core_data_t *data)
{
	return (ocno_datapath_t)*data->datapath_sel_pin;
}


/* HAL RT function entry point */
void ocno_hal_func(void *args, long period)
{
	unsigned mode = 0;
	bool new_mode = false;
	ocno_data_t *data =  (ocno_data_t *)args;
	ocno_core_data_t *core_data = data->core;
	unsigned cur_mode = ocno_get_mode(data);
	ocno_machine_state_t state = ocno_get_machine_state(data);
	bool homed = ocno_is_homed(data->homing);
	static bool in_fault = false;

	/* Fault management */
	if (ocno_machine_in_fault(core_data)) {
		ocno_error_disable(core_data);
		if (!in_fault) {
			RTAPI_PRINT_MSG(RTAPI_MSG_DBG, "[%s] Machine is in fault !\n", OCNO_COMP_NAME);
			in_fault = true;
			ocno_spindle_set(core_data, false);
			/* TODO Check what to do in this case */
			ocno_send_inactive_cmd(core_data);
			/* Reset current FSM - temp fix, we should manage quickstop */
			switch(cur_mode){
			case OCNO_MODE_HOMING:
				RTAPI_PRINT_MSG(RTAPI_MSG_DBG, "[%s] Send FSM reset OCNO_MODE_HOMING !\n", OCNO_COMP_NAME);
				ocno_homing_fsm_exec(data->homing, true);
				break;
			case OCNO_MODE_JOG:
				RTAPI_PRINT_MSG(RTAPI_MSG_DBG, "[%s] Send FSM reset OCNO_MODE_JOG !\n", OCNO_COMP_NAME);
				ocno_jog_fsm_exec(data->jog, true);
				break;
			case OCNO_MODE_MACHINING:
				RTAPI_PRINT_MSG(RTAPI_MSG_DBG, "[%s] Send FSM reset OCNO_MODE_MACHINING !\n", OCNO_COMP_NAME);
				ocno_mac_fsm_exec(data->mac, true);
				break;
			}
			/* Set machine mode in inactive */
			cur_mode = OCNO_MODE_INACTIVE;
		}

	} else {
		in_fault = false;
	}

	// if (core_data->ocno_is_in_error) {
	// 	cur_mode = OCNO_MODE_INACTIVE;
	// }

	if (*core_data->fault_reset_pin) {
		RTAPI_PRINT_MSG(RTAPI_MSG_DBG, "Resetting drive faults\n");
		ocno_send_reset_cmd(core_data);
		*core_data->fault_reset_pin = 0;
	}

	if (*core_data->stop_operation_pin) {
		*core_data->stop_operation_pin = 0;
		ocno_send_inactive_cmd(core_data);
		cur_mode = OCNO_MODE_INACTIVE;
	}

	/* Check if a new working mode is requested */
	if (!in_fault) {
		mode = (*(core_data->set_mode_inactive_pin)  << OCNO_MODE_INACTIVE_POS) |
			   (*(core_data->set_mode_homing_pin)    << OCNO_MODE_HOMING_POS)   |
			   (*(core_data->set_mode_jog_pin)       << OCNO_MODE_JOG_POS)      |
			   (*(core_data->set_mode_machining_pin) << OCNO_MODE_MACHINING_POS);
	}

	if (mode) {
		/* new mode requested */
		if (mode == cur_mode) {
			RTAPI_PRINT_MSG(RTAPI_MSG_DBG, "[%s] New mode requested - Machine already in '%s' mode\n", OCNO_COMP_NAME, ocno_mode_str(mode));

		} else if (!homed && mode == OCNO_MODE_MACHINING) {
			RTAPI_PRINT_MSG(RTAPI_MSG_DBG, "[%s] Machine has to be homed before going in '%s' mode\n", OCNO_COMP_NAME, ocno_mode_str(mode));

		} else if (state != OCNO_MACHINE_IDLE) {
		  /* Check if it is possible to change mode (machine or FSM in IDLE mode */
		   RTAPI_PRINT_MSG(RTAPI_MSG_DBG, "[%s] New mode requested - Machine is running, cannot change state\n", OCNO_COMP_NAME);

		} else {
			/* It is a new mode */
			new_mode = true;
			cur_mode = mode;
			ocno_spindle_set(core_data, false);
		}
	}

	if (new_mode)
		RTAPI_PRINT_MSG(RTAPI_MSG_DBG, "[%s] Mode set to '%s'\n", OCNO_COMP_NAME, ocno_mode_str(cur_mode));

	/* FSM / state management */
	switch (cur_mode) {
	case OCNO_MODE_INACTIVE:
		if (new_mode)
			ocno_send_inactive_cmd(core_data);
		break;
	case OCNO_MODE_HOMING:
		ocno_homing_fsm_exec(data->homing, new_mode);
		break;
	case OCNO_MODE_JOG:
		ocno_jog_fsm_exec(data->jog, new_mode);
		break;
	case OCNO_MODE_MACHINING:
		ocno_mac_fsm_exec(data->mac, new_mode);
		break;
	default:
		/* Error case --> print error + set 'inactive' mode */
		ocno_send_inactive_cmd(core_data);
		printk("[%s] Error - Only one mode can be set at one time !\n", OCNO_COMP_NAME);
		cur_mode = OCNO_MODE_INACTIVE;
	}

	/* Set machine home status */
	*core_data->machine_homed_pin = homed;

	/* Clear commands */
	*(core_data->set_mode_inactive_pin)  = 0;
	*(core_data->set_mode_homing_pin)    = 0;
	*(core_data->set_mode_jog_pin)       = 0;
	*(core_data->set_mode_machining_pin) = 0;

	/* save current working mode */
	ocno_set_mode(data, cur_mode);

	*(core_data->in_mode_inactive_pin)  = cur_mode & OCNO_MODE_INACTIVE;
	*(core_data->in_mode_homing_pin)    = cur_mode & OCNO_MODE_HOMING;
	*(core_data->in_mode_jog_pin)       = cur_mode & OCNO_MODE_JOG;
	*(core_data->in_mode_machining_pin) = cur_mode & OCNO_MODE_MACHINING;
}

/************************************************************************
 *                     OCNO CORE INITIALIZATION                         *
 ************************************************************************/

static int ocno_core_create_pins(ocno_core_data_t *data, int comp_id)
{
	int i;
	int ret;
	ocno_core_axis_t *axis;
	ocno_axes_info_t  *axes_info = ocno_get_axes_info(data->parent);

	ret = hal_pin_newf_list(__core_hal_user, comp_id, data, ocno_core_pins, OCNO_COMP_NAME);
	BUG_ON(ret != 0);

	ret = hal_pin_newf_list(__core_hal_user, comp_id, data->spindle, ocno_spindle_pins, OCNO_COMP_NAME);
	BUG_ON(ret != 0);

	for (i =0; i < axes_info->axis_nr; i++) {
		axis = &data->axis[i];

		ret = hal_pin_newf_list(__core_hal_user, comp_id, axis, ocno_core_axis_pins, OCNO_COMP_NAME, axes_info->axes[i]);
		BUG_ON(ret != 0);

		ret = hal_param_newf_list(__core_hal_user, axis, comp_id, ocno_core_axis_params, OCNO_COMP_NAME, axes_info->axes[i]);
		BUG_ON(ret != 0);
	}

	return 0;
}


void ocno_core_init(void *parent, ocno_core_data_t *data, int comp_id)
{
	ocno_core_axis_t *axis = NULL;
	ocno_axes_info_t *axes_info;
	ocno_data_t *ocno = (ocno_data_t *)parent;

	data = hal_malloc(__core_hal_user, sizeof(ocno_core_data_t));
	if (!data) {
		printk("[%s/%s] Error: memory allocation failed\n", OCNO_COMP_NAME, OCNO_CORE_NAME);
		BUG();
	}

	data->spindle = hal_malloc(__core_hal_user, sizeof(ocno_spindle_data_t));
	if (!data->spindle) {
		printk("[%s/%s] Error: memory allocation failed\n", OCNO_COMP_NAME, OCNO_CORE_NAME);
		BUG();
	}

	data->parent = parent;
	ocno->core   = data;

	axes_info = ocno_get_axes_info(parent);
	axis = hal_malloc(__core_hal_user, sizeof(ocno_core_axis_t) * axes_info->axis_nr);
	if (!axis) {
		printk("[%s/%s] Error: PINs per axis data allocation failed\n", OCNO_COMP_NAME, OCNO_CORE_NAME);
		BUG();

	}

	data->axis = axis;

	ocno_core_create_pins(data, comp_id);

	data->ocno_is_in_error = 0;

	/* When ocno is loaded, the fault reset is triggered to begin in a good state */
	//*data->fault_reset_pin = 1;
	ocno_spindle_set(ocno->core, false);
	ocno_send_inactive_cmd(ocno->core);
	*ocno->core->fault_reset_pin = 1;

	/* By default the machine is in 'inactive' mode */
	ocno_set_mode(data->parent, OCNO_MODE_INACTIVE);
	ocno_set_machine_state(data->parent, OCNO_MACHINE_IDLE);
	ocno_send_inactive_cmd(data);
}
