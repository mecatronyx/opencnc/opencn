/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: ocno_jog.c
 *
 *
 * Support of the jogging mode to move axis/joint. Also support spindle
 * rotation request
 *
 ********************************************************************/

#include <opencn/hal/hal.h>
#include <linux/string.h>
#include <opencn/ctypes/strings.h>
#include "ocno_priv.h"

/* Debug Jog FSM */
#if 0
#define DEBUG_JOG
#endif

#define MSG_MAX_LEN 200
void debug_jog(const char* text, ...)
{
	#ifdef DEBUG_JOG
	#warning Only print when 'text' is changed
	va_list args;
	static char last_text[MSG_MAX_LEN];

	if (text == NULL) 
		return;

	/* Check max message length */
	if (strlen(text) > MSG_MAX_LEN){
		rtapi_print_msg(RTAPI_MSG_ERR, "[ocno/jog] Error: Message longer than %d,no print\n", MSG_MAX_LEN);
		return;
	}

	va_start(args, text);

	/* Check if UNFORMATTED text is different */
	if (strcmp(last_text, text) != 0 ){
		opencn_printf(text,args);
		//Save for next iteration
		strcpy(last_text, text);
	}

	va_end(args);
	#endif
}


enum {
	OCNO_JOG_STATE_INIT = 0,
	OCNO_JOG_STATE_WAIT_MODE,
	OCNO_JOG_WAIT_SPINDLE_REACH_SPEED,
	OCNO_JOG_STATE_WAIT_CMD,
	OCNO_JOG_STATE_RUN,
};


static void ocno_jog_fsm_init(void *args);
static void ocno_jog_fsm_wait_mode(void *args);
static void ocno_jog_fsm_wait_spindle_reach_speed_mode(void *args);
static void ocno_jog_fsm_wait_cmd(void *args);
static void ocno_jog_fsm_run(void *args);


static ocno_state_fn_t ocno_jog_funcs[] = {
	[OCNO_JOG_STATE_INIT]      = ocno_jog_fsm_init,
	[OCNO_JOG_STATE_WAIT_MODE] = ocno_jog_fsm_wait_mode,
	[OCNO_JOG_WAIT_SPINDLE_REACH_SPEED] = ocno_jog_fsm_wait_spindle_reach_speed_mode,
	[OCNO_JOG_STATE_WAIT_CMD]  = ocno_jog_fsm_wait_cmd,
	[OCNO_JOG_STATE_RUN]       = ocno_jog_fsm_run,
};

static ocno_fsm_t ocno_jog_fsm = {
	.state = OCNO_JOG_STATE_INIT,
	.funcs = ocno_jog_funcs,
};


static void ocno_jog_reset_pin_value(ocno_jog_data_t  *data)
{
	ocno_jog_axis_t  *axis;
	ocno_axes_info_t *axes_info = ocno_get_axes_info(data->parent);
	int i;

	*data->start_pin = 0;
	*data->stop_pin  = 0;
	*data->running_pin = 0;

	for (i = 0; i < axes_info->axis_nr; i++) {
		axis = &data->axis[i];

		*axis->relative_pos_pin = 0;
		*axis->select_pin       = 0;
		*axis->is_running_pin   = 0;
	}
}


static void ocno_jog_fsm_init(void *args)
{
	ocno_jog_data_t  *data = (ocno_jog_data_t *)args;
	ocno_core_data_t *core = ocno_get_core_data(data->parent);

	/* Set the pins at a specific state */
	ocno_jog_reset_pin_value(data);

	/* PG preparation - set target position to current position */
	ocno_set_all_axes_current_pos(core);

	/* Enable PG */
	ocno_jog_enable(data);

	/* Set the drives in CSP mode */
	ocno_send_csp_cmd(core);

	/* Datapath selection */
	ocno_datapath_selection(core, OCNO_DATAPATHPATH_PG);

	ocno_spindle_set(core, *core->spindle->rotation_request_pin);
	ocno_set_machine_state(data->parent, OCNO_MACHINE_IDLE);

	ocno_fsm_change_state(&ocno_jog_fsm, OCNO_JOG_STATE_WAIT_MODE);

	debug_jog("[ocno/jog] Init state\n");
}


static void ocno_jog_fsm_wait_mode(void *args)
{
	ocno_jog_data_t *data = (ocno_jog_data_t *)args;
	ocno_core_data_t *core = ocno_get_core_data(data->parent);

	ocno_set_machine_state(data->parent, OCNO_MACHINE_IDLE);

	debug_jog("[ocno/jog] wait mode state\n");

	if (ocno_axis_in_csp(core)) {
		/* If the spindle is asked to rotate, enter in spindle waiting mode, otherwise
		 * skip this state and directly enter in wait cmd state */
		if(ocno_spindle_rotation_required(core)) {
			ocno_set_spindle_speed(core, *data->target_spindle_speed_pin);
			ocno_fsm_change_state(&ocno_jog_fsm, OCNO_JOG_WAIT_SPINDLE_REACH_SPEED);

			debug_jog("[ocno/jog] axes are in CSP, spindle rotation \n");
		} else {
			ocno_fsm_change_state(&ocno_jog_fsm, OCNO_JOG_STATE_WAIT_CMD);
			debug_jog("[ocno/jog] axes are in CSP\n");
		} 
	}
}


static void ocno_jog_fsm_wait_spindle_reach_speed_mode(void *args)
{
	ocno_jog_data_t *data = (ocno_jog_data_t *)args;
	ocno_core_data_t *core = ocno_get_core_data(data->parent);

	debug_jog("[ocno/jog] wait spindle reach speed mode state\n");

	/* Ask spindle roation to stop ? */
	if(!ocno_spindle_rotation_required(core)){
		ocno_set_spindle_speed(ocno_get_core_data(data->parent), 0.0);
		ocno_fsm_change_state(&ocno_jog_fsm, OCNO_JOG_STATE_WAIT_CMD);
	}

	if (is_spindle_speed_reached(core)) {
		ocno_fsm_change_state(&ocno_jog_fsm, OCNO_JOG_STATE_WAIT_CMD);
		debug_jog("[ocno/jog] spindle reached required speed\n");
	}
}


void ocno_jog_fsm_wait_cmd(void *args)
{
	ocno_jog_data_t *data = (ocno_jog_data_t *)args;
	ocno_core_data_t *core = ocno_get_core_data(data->parent);
	ocno_jog_axis_t *axis;
	ocno_axes_info_t  *axes_info = ocno_get_axes_info(data->parent);
	int i;

	/* Waiting cmd --> Disable PG */
	//ocno_jog_disable(data);
	ocno_spindle_set(core, *core->spindle->rotation_request_pin);

	//debug_jog("[ocno/jog] wait cmd state\n");

	if(ocno_spindle_rotation_required(core)) {
		if (*core->spindle->speed_pin != *data->target_spindle_speed_pin) {
			debug_jog("[ocno/jog] new spindle rotation speed required\n");
			ocno_set_spindle_speed(core, *data->target_spindle_speed_pin);
			ocno_fsm_change_state(&ocno_jog_fsm, OCNO_JOG_WAIT_SPINDLE_REACH_SPEED);
			return;
		}
	}

	if (*data->start_pin) {
		debug_jog("[ocno/jog] start a new command !\n");
		*data->start_pin = 0;
		debug_jog("[ocno/jog] start pin to 0 !\n");
		ocno_set_machine_state(data->parent, OCNO_MACHINE_RUNNING);

		/* Enable PG */
		ocno_jog_enable(data);

		*data->running_pin = 1;

		for (i = 0; i < axes_info->axis_nr; i++) {
			axis = &data->axis[i];

			if (*axis->select_pin) {
				/* enabled axis - set the target position */
				if (*axis->relative_pos_pin) {
					ocno_set_axis_rel_target(core, i, *axis->cmd_pos_pin);
				}
				else {
					/* If absolute pos selected and system not homed, nothing happens*/
					if (ocno_is_homed(ocno_get_homing_data(data->parent))) {
						ocno_set_axis_abs_target(core, i, *axis->cmd_pos_pin);
					} else {
						debug_jog("[ocno/jog] System not homed, impossible to set absolute pos !\n");
						ocno_fsm_change_state(&ocno_jog_fsm, OCNO_JOG_STATE_WAIT_CMD);
						return;
					}
				}

				/* Reset the cmd */
				*axis->select_pin = 0;

			} else {
				ocno_set_axis_current_pos(core, i);
			}
		}

		/* enable jog processing */
		// *data->enable_pin = 1;

		ocno_fsm_change_state(&ocno_jog_fsm, OCNO_JOG_STATE_RUN);
	}
}


void ocno_jog_enable(ocno_jog_data_t *data)
{
	*data->enable_pin = 1;
}


void ocno_jog_disable(ocno_jog_data_t *data)
{
	*data->enable_pin = 0;
}


void ocno_jog_stop(ocno_jog_data_t *data)
{
	// *data->enable_pin = 0;
	ocno_fsm_change_state(&ocno_jog_fsm, OCNO_JOG_STATE_WAIT_CMD);
}


void ocno_jog_fsm_run(void *args)
{
	ocno_jog_data_t *data = (ocno_jog_data_t *)args;
	ocno_jog_axis_t *axis;
	ocno_core_data_t *core = ocno_get_core_data(data->parent);
	ocno_axes_info_t  *axes_info = ocno_get_axes_info(data->parent);
	int i;
	bool running = false;

	debug_jog("[ocno/jog] run state\n");

	if(ocno_spindle_rotation_required(core)) {
		if (!is_spindle_speed_reached(core)) {
			debug_jog("Spindle RPM is too low ! Quick stop required...\n");
			ocno_quick_stop_enable(core);
			ocno_error_enable(core);
			ocno_set_machine_state(data->parent, OCNO_MACHINE_IDLE);
			return;
		}
	}

	for (i = 0; i < axes_info->axis_nr; i++) {
		/* Stop the current goal */
		if (*data->stop_pin) {
			debug_jog("Stop operation required !\n");
			*data->stop_pin = 0;
			running = false;
			ocno_set_machine_state(data->parent, OCNO_MACHINE_IDLE);
			break;
		}

		axis = &data->axis[i];

		if (*axis->is_running_pin) {
			running = true;
			break;
		}
	}

	if (!running) {
		// *data->enable_pin = 0;

		*data->running_pin = 0;

		ocno_set_machine_state(data->parent, OCNO_MACHINE_IDLE);
		ocno_fsm_change_state(&ocno_jog_fsm, OCNO_JOG_STATE_WAIT_CMD);
	}
}


void ocno_jog_fsm_exec(ocno_jog_data_t *data, bool new_mode)
{
	if (new_mode)
		/* new mode -->  initialize the fsm */
		ocno_fsm_change_state(&ocno_jog_fsm, OCNO_JOG_STATE_INIT);

	else if (*data->stop_pin){
		*data->stop_pin = 0;
		ocno_jog_stop(data);
	}


	 /* Raise error if spindle can't turn and spindle rotation enable */
	if (ocno_spindle_is_safe(ocno_get_core_data(data->parent))) {
		/* execute state */
		ocno_fsm_execute(&ocno_jog_fsm, data);
	}
}


static void ocno_jog_create_pins(ocno_jog_data_t *data, ocno_axes_info_t *axes_info, int comp_id)
{
	int i;
	int ret;
	ocno_jog_axis_t *axis;

	ret = hal_pin_newf_list(__core_hal_user, comp_id, data, ocno_jog_pins, OCNO_COMP_NAME, OCNO_JOG_MODE_NAME);

	for (i =0; i < axes_info->axis_nr; i++) {
		axis = &data->axis[i];

		ret = hal_pin_newf_list(__core_hal_user, comp_id, axis, ocno_jog_axis_pins, OCNO_COMP_NAME, OCNO_JOG_MODE_NAME, axes_info->axes[i]);
	}
}


void ocno_jog_init(void *parent, ocno_jog_data_t *data, int comp_id)
{
	ocno_jog_axis_t *axis = NULL;
	ocno_axes_info_t *axes_info;
	ocno_data_t *ocno = (ocno_data_t *)parent;

	data = hal_malloc(__core_hal_user, sizeof(ocno_jog_data_t));
	if (!data) {
		printk("[%s/%s] Error: memory allocation failed\n", OCNO_COMP_NAME, OCNO_JOG_MODE_NAME);
		BUG();
	}

	data->parent = parent;
	ocno->jog = data;

	axes_info = ocno_get_axes_info(parent);

	axis = hal_malloc(__core_hal_user, sizeof(ocno_jog_axis_t) * axes_info->axis_nr);
	if (!axis) {
		printk("[%s/%s] Error: PINs per axis data allocation failed\n", OCNO_COMP_NAME, OCNO_JOG_MODE_NAME);
		BUG();

	}
	data->axis = axis;

	ocno_jog_create_pins(data, axes_info, comp_id);
}
