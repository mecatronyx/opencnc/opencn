/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: ocno_homing.c
 *
 *
 * Support of the homing mode - only support driver self-homing mode
 *
 ********************************************************************/

#include <opencn/hal/hal.h>

#include "ocno_priv.h"


/* DEBUG - TODO : REMOVE */
static int debug = 0;
void debug_print(const char* text) 
{
	if(debug++ >= 3000) {
		printk(text);
		debug = 0; 
	}
}

enum {
	OCNO_HOMING_STATE_INIT = 0,
	OCNO_HOMING_STATE_WAIT_MODE,
	OCNO_HOMING_STATE_WAIT_CMD,
	OCNO_HOMING_STATE_AXIS_START,
	OCNO_HOMING_STATE_AXIS_STARTED,
	OCNO_HOMING_STATE_AXIS_HOMED,
	OCNO_HOMING_STATE_WAIT_CSP_MODE,
	OCNO_HOMING_STATE_AXIS_GO_OFFSET_POS,
	OCNO_HOMING_STATE_AXIS_RUN_OFFSET_POS,
	OCNO_HOMING_STATE_NEXT_AXIS,
};

static void ocno_homing_fsm_init(void *args);
static void ocno_homing_fsm_wait_mode(void *args);
static void ocno_homing_fsm_wait_cmd(void *args);
static void ocno_homing_fsm_home_axis_start(void *args);
static void ocno_homing_fsm_home_axis_started(void *args);
static void ocno_homing_fsm_axis_homed(void *args);
static void ocno_homing_fsm_wait_csp_mode(void *args);
static void ocno_homing_fsm_home_go_offset_pos(void *args);
static void ocno_homing_fsm_home_run_offset_pos(void *args);
static void ocno_homing_fsm_home_next_axis(void *args);

static ocno_state_fn_t ocno_homing_funcs[] = {
	[OCNO_HOMING_STATE_INIT]                = ocno_homing_fsm_init,
	[OCNO_HOMING_STATE_WAIT_MODE]           = ocno_homing_fsm_wait_mode,
	[OCNO_HOMING_STATE_WAIT_CMD]            = ocno_homing_fsm_wait_cmd,
	[OCNO_HOMING_STATE_AXIS_START]          = ocno_homing_fsm_home_axis_start,
	[OCNO_HOMING_STATE_AXIS_STARTED]        = ocno_homing_fsm_home_axis_started,
	[OCNO_HOMING_STATE_AXIS_HOMED]          = ocno_homing_fsm_axis_homed,
	[OCNO_HOMING_STATE_WAIT_CSP_MODE]       = ocno_homing_fsm_wait_csp_mode,
	[OCNO_HOMING_STATE_AXIS_GO_OFFSET_POS]  = ocno_homing_fsm_home_go_offset_pos,
	[OCNO_HOMING_STATE_AXIS_RUN_OFFSET_POS] = ocno_homing_fsm_home_run_offset_pos,
	[OCNO_HOMING_STATE_NEXT_AXIS]           = ocno_homing_fsm_home_next_axis,
};

static ocno_fsm_t ocno_homing_fsm = {
	.state = OCNO_HOMING_STATE_INIT,
	.funcs = ocno_homing_funcs,
};

static void ocno_homing_reset_pin_value(ocno_homing_data_t  *data)
{
	*data->start_pin = 0;
	*data->stop_pin  = 0;
	*data->processing_pin = 0;
}

/* Set the drives in 'homing' mode */
static void ocno_homing_fsm_init(void *args)
{
	ocno_homing_data_t *data = (ocno_homing_data_t *)args;

	ocno_homing_reset_pin_value(data);

	ocno_send_homing_cmd(ocno_get_core_data(data->parent));

	ocno_set_machine_state(data->parent, OCNO_MACHINE_IDLE);
	ocno_fsm_change_state(&ocno_homing_fsm, OCNO_HOMING_STATE_WAIT_MODE);

	printk("[ocno/homing] init\n");
}

static void ocno_homing_fsm_wait_mode(void *args)
{
	ocno_homing_data_t *data = (ocno_homing_data_t *)args;
	debug_print("[ocno/homing] wait_mode\n");
	if (ocno_axis_in_homing(ocno_get_core_data(data->parent))) {
		printk("[ocno/homing] Wait mode -> Wait cmd\n");
		ocno_fsm_change_state(&ocno_homing_fsm, OCNO_HOMING_STATE_WAIT_CMD);
	}
}

static void ocno_homing_fsm_wait_cmd(void *args)
{
	ocno_homing_data_t *data = (ocno_homing_data_t *)args;

	debug_print("[ocno/homing] wait_cmd\n");

	if (*data->start_pin) {
		/* The machine is running/processing ! */

		printk("[ocno/homing] start PIN\n");
		ocno_set_machine_state(data->parent, OCNO_MACHINE_RUNNING);

		*data->start_pin = 0;

		/* Reset the processing axis */
		data->process_axis = 0;

		*data->processing_pin = 1;

		ocno_fsm_change_state(&ocno_homing_fsm, OCNO_HOMING_STATE_AXIS_START);
	}
}

static void ocno_homing_fsm_home_axis_start(void *args)
{
	ocno_homing_data_t *data = (ocno_homing_data_t *)args;
	ocno_homing_axis_t *axis;

	axis = &data->axis[data->process_axis];
	*axis->start_pin = 1;

	printk("[ocno/homing] start axis%d\n", data->process_axis);

	ocno_fsm_change_state(&ocno_homing_fsm, OCNO_HOMING_STATE_AXIS_STARTED);
}


static void ocno_homing_fsm_home_axis_started(void *args)
{
	ocno_homing_data_t *data = (ocno_homing_data_t *)args;
	ocno_homing_axis_t *axis;

	axis = &data->axis[data->process_axis];
	printk("[ocno/homing] home axis started state\n");
	if (!*axis->homed_pin) {
		printk("[ocno/homing] NOT HOMED, axis%d -->  completed\n", data->process_axis);
		ocno_fsm_change_state(&ocno_homing_fsm, OCNO_HOMING_STATE_AXIS_HOMED);
	}
}

static void ocno_homing_fsm_axis_homed(void *args)
{
	ocno_homing_data_t *data = (ocno_homing_data_t *)args;
	ocno_homing_axis_t *axis;
	ocno_core_data_t *core = ocno_get_core_data(data->parent);
	ocno_core_axis_t *core_axis;

	axis = &data->axis[data->process_axis];
	if (*axis->homed_pin) {
		/* Axis homed -> Preparation to move axis to the 'offset' position */
		printk("[ocno/homing] HOMED, axis%d\n", data->process_axis);


		if (*axis->move_offset_pin == 0) {
			/* No offset --> Perform next axis */
			ocno_fsm_change_state(&ocno_homing_fsm, OCNO_HOMING_STATE_NEXT_AXIS);
		} else {
			printk("[ocno/homing] offset on axis%d\n", data->process_axis);
			
			/* PG preparation - set target position to current position */
			// ocno_set_all_axes_current_pos(core);

			/* PG preparation - set target position to current position of current axis */
			core_axis = &core->axis[data->process_axis];
			*core_axis->target_pos_pin = *core_axis->current_joint_pos_pin;

			/* Datapath selection */
			ocno_datapath_selection(core, OCNO_DATAPATHPATH_PG);

			/* Set Axis in csp mode */
			ocno_set_axis_csp_mode(core, data->process_axis);

			/* Axis homed -> Move axis to the 'offset' position */
			ocno_fsm_change_state(&ocno_homing_fsm, OCNO_HOMING_STATE_WAIT_CSP_MODE);
		}
	}
}

static void ocno_homing_fsm_wait_csp_mode(void *args)
{
	ocno_homing_data_t *data = (ocno_homing_data_t *)args;
	ocno_core_data_t   *core = ocno_get_core_data(data->parent);
	ocno_core_axis_t   *core_axis;

	core_axis = &core->axis[data->process_axis];

	if (*core_axis->in_mode_csp_pin) {
		/* Axis in 'csp' mode -> Move axis to the 'offset' position */
		printk("[ocno/homing] CSP, axis%d\n", data->process_axis);
		/* Enable PG */
		ocno_jog_enable(ocno_get_jog_data(data->parent));
		ocno_fsm_change_state(&ocno_homing_fsm, OCNO_HOMING_STATE_AXIS_GO_OFFSET_POS);
	}
}

static void ocno_homing_fsm_home_go_offset_pos(void *args)
{
	ocno_homing_data_t *data = (ocno_homing_data_t *)args;
	ocno_homing_axis_t *axis = &data->axis[data->process_axis];

	ocno_core_data_t *core = ocno_get_core_data(data->parent);

	/* Set Axis in csp mode */
	// ocno_set_axis_csp_mode(core, data->process_axis);

	printk("[ocno/homing] GO offset\n");

	/* set target position to current position */
	//ocno_set_all_axes_current_pos(core);

	/* Datapath selection */
	//ocno_datapath_selection(core, OCNO_DATAPATHPATH_PG);

	/* Enable PG */
	//ocno_jog_enable(ocno_get_jog_data(data->parent));

	/* Set the current axis to the 'offset' Position */
	ocno_set_axis_abs_target(core, data->process_axis, *axis->move_offset_pin);

	ocno_fsm_change_state(&ocno_homing_fsm, OCNO_HOMING_STATE_AXIS_RUN_OFFSET_POS);
}

static void ocno_homing_fsm_home_run_offset_pos(void *args)
{
	ocno_homing_data_t *data = (ocno_homing_data_t *)args;
	ocno_jog_data_t    *jog  = ocno_get_jog_data(data->parent);
	ocno_jog_axis_t    *jog_axis  = &jog->axis[data->process_axis];
	// ocno_axes_info_t  *axes_info = ocno_get_axes_info(data->parent);

	if (! *jog_axis->is_running_pin) {
		printk("[ocno/homing] Homing process COMPLETED\n");

		/* Disable jog */
		ocno_jog_disable(jog);

		ocno_fsm_change_state(&ocno_homing_fsm, OCNO_HOMING_STATE_NEXT_AXIS);
	}
}


static void ocno_homing_fsm_home_next_axis(void *args)
{
	ocno_homing_data_t *data = (ocno_homing_data_t *)args;
	ocno_axes_info_t  *axes_info = ocno_get_axes_info(data->parent);

	if (data->process_axis == axes_info->axis_nr - 1) {
		/* All axes are in their start position */
		ocno_fsm_change_state(&ocno_homing_fsm, OCNO_HOMING_STATE_INIT);

	} else {
		/* Star homing process for next axis */
		data->process_axis++;
		ocno_fsm_change_state(&ocno_homing_fsm, OCNO_HOMING_STATE_AXIS_START);
	}
}


static void ocno_homing_stop(ocno_homing_data_t *data)
{
	int i;
	ocno_homing_axis_t *axis;
	ocno_axes_info_t  *axes_info = ocno_get_axes_info(data->parent);

	for (i = 0; i < axes_info->axis_nr; i++) {
		axis = &data->axis[i];
		*(axis->stop_pin) = 1;
	}

	ocno_set_machine_state(data->parent, OCNO_MACHINE_IDLE);
	ocno_fsm_change_state(&ocno_homing_fsm, OCNO_HOMING_STATE_INIT);
}

/* check if all axes are homed or not
 *
 * return true if all the axis are homed, false otherwise
 */
bool ocno_is_homed(ocno_homing_data_t *data)
{
	int i;
	ocno_homing_axis_t *axis;
	ocno_axes_info_t   *axes_info = ocno_get_axes_info(data->parent);

	for (i = 0; i < axes_info->axis_nr; i++) {
		axis = &data->axis[i];

		if (!*axis->homed_pin)
			return false;
	}

	return true;
}

void ocno_homing_fsm_exec(ocno_homing_data_t *data, bool new_mode)
{
	if (new_mode)
		/* new mode -->  initialize the fsm */
		ocno_fsm_change_state(&ocno_homing_fsm, OCNO_HOMING_STATE_INIT);
	else if (*data->stop_pin) {
		/* Check stop */
		*data->stop_pin = 0;
		ocno_homing_stop(data);
	}

	/* execute state */
	ocno_fsm_execute(&ocno_homing_fsm, data);
}


static void ocno_homing_create_pins(ocno_homing_data_t *data, ocno_axes_info_t *axes_info, int comp_id)
{
	int i;
	int ret;
	ocno_homing_axis_t *axis;

	ret = hal_pin_newf_list(__core_hal_user, comp_id, data, ocno_homing_pins, OCNO_COMP_NAME, OCNO_HOMING_MODE_NAME);

	for (i =0; i < axes_info->axis_nr; i++) {
		axis = &data->axis[i];

		ret = hal_pin_newf_list(__core_hal_user, comp_id, axis, ocno_homing_axis_pins, OCNO_COMP_NAME, OCNO_HOMING_MODE_NAME, axes_info->axes[i]);
	}
}

void ocno_homing_init(void *parent, ocno_homing_data_t *data, int comp_id)
{
	ocno_homing_axis_t *axis = NULL;
	ocno_axes_info_t  *axes_info;
	ocno_data_t *ocno = (ocno_data_t *)parent;

	data = hal_malloc(__core_hal_user, sizeof(ocno_homing_data_t));
	if (!data) {
		printk("[%s/%s] Error: memory allocation failed\n", OCNO_COMP_NAME, OCNO_HOMING_MODE_NAME);
		BUG();
	}

	data->parent = parent;
	ocno->homing = data;

	axes_info = ocno_get_axes_info(parent);

	axis = hal_malloc(__core_hal_user, sizeof(ocno_homing_axis_t) * axes_info->axis_nr);
	if (!axis) {
		printk("[%s/%s] Error: PINs per axis data allocation failed\n", OCNO_COMP_NAME, OCNO_HOMING_MODE_NAME);
		BUG();

	}
	data->axis = axis;

	ocno_homing_create_pins(data, axes_info, comp_id);
}
