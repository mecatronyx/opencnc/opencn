/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: ocno.c
 *
 *
 * It is the kernel part of the "ocno" OpenCN HAL component.
 *
 ********************************************************************/

#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/slab.h>

#include <opencn/hal/hal.h>
#include <opencn/uapi/ocno.h>

#include "ocno_priv.h"


/************************************************************************
 *                       FSM support                                    *
 ************************************************************************/

void ocno_fsm_change_state(ocno_fsm_t *fsm, int new_state)
{
	/* TODO - add some checks */

	fsm->state = new_state;
}

void ocno_fsm_execute(ocno_fsm_t *fsm, void *data)
{
	fsm->funcs[fsm->state](data);
}

/************************************************************************
 *                    MODE & STATE HELPER FUNCTIONS                     *
 ************************************************************************/

static void ocno_set_axes_info(ocno_data_t *data, char *axes, unsigned axis_nr)
{
	strcpy(data->info.axes, axes);
	data->info.axis_nr = axis_nr;
}

ocno_axes_info_t *ocno_get_axes_info(ocno_data_t *data)
{
	return &data->info;
}

void ocno_set_mode(ocno_data_t *data, unsigned mode)
{
	data->mode = mode;
}

unsigned ocno_get_mode(ocno_data_t *data)
{
	return data->mode;
}

void ocno_set_machine_state(ocno_data_t *data, ocno_machine_state_t state)
{
	ocno_core_data_t *core = ocno_get_core_data(data);

	data->state = state;

	*core->machine_state_pin = state;
}

ocno_machine_state_t ocno_get_machine_state(ocno_data_t *data)
{
	return data->state;
}

/************************************************************************
 *                    MODE DATA struc HELPER FUNCTIONS                  *
 ************************************************************************/

ocno_core_data_t *ocno_get_core_data(ocno_data_t *data)
{
	return data->core;
}

ocno_homing_data_t *ocno_get_homing_data(ocno_data_t *data)
{
	return data->homing;
}

ocno_jog_data_t *ocno_get_jog_data(ocno_data_t *data)
{
	return data->jog;
}

/************************************************************************
 *                       INIT AND EXIT CODE                             *
 ************************************************************************/

static int ocno_initialization(char *axes, int comp_id)
{
	ocno_data_t *data;
	unsigned axis_nr;
	char name[HAL_NAME_LEN];
	int ret;

	data = kmalloc(sizeof(ocno_data_t), GFP_KERNEL);
	if (!data)
		printk("[%s] ERROR: memory allocation failed\n", OCNO_COMP_NAME);

	/* Store the info related to axis - received for user-space */
	axis_nr = strlen(axes);
	ocno_set_axes_info(data, axes, axis_nr);

	/* Set Machine in 'inactive' mode (initial value) */
	data->mode = OCNO_MODE_INACTIVE;

	/* Different mode initialization */
	ocno_core_init(data, data->core, comp_id);
	ocno_homing_init(data, data->homing, comp_id);
	ocno_jog_init(data, data->jog, comp_id);
	ocno_mac_init(data, data->mac, comp_id);

	/* Export RT function */
	sprintf(name, "%s.%s", OCNO_COMP_NAME, "func");
	ret = hal_export_funct(__core_hal_user, name, ocno_hal_func, data, 1, 0, comp_id);
	if (ret != 0) {
		printk("[%s] Error: function export failed\n", OCNO_COMP_NAME);
		ret = -1;
	}

	return 0;
}

static int ocno_app_main(ocno_connect_args_t *args)
{
	int ret = 0;
	int comp_id;

	/* Set all Error messages */
	rtapi_set_msg_level(RTAPI_MSG_ERR);

	comp_id = hal_init(__core_hal_user, OCNO_COMP_NAME);
	if (comp_id < 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "[%s] Error: hal_init() failed\n", OCNO_COMP_NAME);
		return -EINVAL;
	}

	ret = ocno_initialization(args->axes, comp_id);
	if (ret != 0) {
		printk("[%s] Error: initialization failed\n", OCNO_COMP_NAME);
		goto fail;
	}

	/* Info HAL that all is ready */
	hal_ready(__core_hal_user, comp_id);

	return 0;

fail:
	hal_exit(__core_hal_user, comp_id);

	return ret;
}

/************************************************************************
 *                Char Device & file operation definitions              *
 ************************************************************************/

static int ocno_open(struct inode *inode, struct file *file)
{
	return 0;
}

static int ocno_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static long ocno_ioctl(struct file *filp, unsigned int cmd, unsigned long args)
{
	int rc = 0;

	switch (cmd) {
	case OCNO_IOCTL_CONNECT:

		printk("[DEBUG] OCNO_IOCTL_CONNECT\n");

		rc = ocno_app_main((ocno_connect_args_t *)args);
		if (rc)
			printk("%s: failed to initialize...\n", __func__);
		break;

	case OCNO_IOCTL_DISCONNECT:
		break;

	}

	return rc;
}

struct file_operations ocno_fops = {
	.owner = THIS_MODULE,
	.open = ocno_open,
	.release = ocno_release,
	.unlocked_ioctl = ocno_ioctl,
};

static int ocno_comp_init(void)
{
	printk("OpenCN: %s subsystem initialization.\n", OCNO_COMP_NAME);

	hal_create_interface(OCNO_COMP_NAME, &ocno_fops);

	return 0;
}

late_initcall(ocno_comp_init)
