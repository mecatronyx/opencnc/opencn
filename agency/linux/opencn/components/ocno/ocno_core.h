/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2010 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: ocno_core.h
 *
 * 'ocno' Core data & functions
 *
 ********************************************************************/

#ifndef OCNO_CORE_H
#define OCNO_CORE_H

#include <opencn/uapi/ocno.h>

/* Mode name */
#define OCNO_INACTIVE_MODE_NAME "inactive"
#define OCNO_CORE_NAME          "core"

/* Modes state macro */
#define OCNO_MODE_INACTIVE_POS   0
#define OCNO_MODE_HOMING_POS     1
#define OCNO_MODE_JOG_POS        2
#define OCNO_MODE_MACHINING_POS  3

#define OCNO_MODE_NO_MODE     0
#define OCNO_MODE_INACTIVE   (1 << OCNO_MODE_INACTIVE_POS)
#define OCNO_MODE_HOMING     (1 << OCNO_MODE_HOMING_POS)
#define OCNO_MODE_JOG        (1 << OCNO_MODE_JOG_POS)
#define OCNO_MODE_MACHINING  (1 << OCNO_MODE_MACHINING_POS)

/* Datapath selection */
typedef enum {
	OCNO_DATAPATHPATH_DIRECT = 0,
	OCNO_DATAPATHPATH_PG,
} ocno_datapath_t;

typedef struct {
	hal_bit_t *set_mode_inactive_pin;
	hal_bit_t *set_mode_homing_pin;
	hal_bit_t *set_mode_csp_pin;
	hal_bit_t *set_mode_csv_pin;
	hal_bit_t *in_mode_inactive_pin;
	hal_bit_t *in_mode_homing_pin;
	hal_bit_t *in_mode_csp_pin;
	hal_bit_t *in_mode_csv_pin;

	/* Position control */
	hal_float_t *current_joint_pos_pin;
	hal_float_t *target_pos_pin;
	hal_float_t *position_offset_pin;

	/* Fault management */
	hal_bit_t *fault_reset_pin;
	hal_bit_t *in_fault_pin;
	hal_bit_t *quick_stop_pin;

	/* Machine limits (Params) */
	hal_float_t limit_min_param;
	hal_float_t limit_max_param;
} ocno_core_axis_t;

typedef struct {
	hal_bit_t *enable_pin;
	hal_bit_t *rotation_request_pin;
	hal_bit_t *can_turn_pin;
	hal_float_t *speed_pin;
	hal_bit_t *fault_reset_pin;
	hal_bit_t *velocity_reached_pin;

} ocno_spindle_data_t;


typedef struct {
	void *parent;

	/* Modes management */
	hal_bit_t *set_mode_inactive_pin;
	hal_bit_t *set_mode_homing_pin;
	hal_bit_t *set_mode_jog_pin;
	hal_bit_t *set_mode_machining_pin;
	hal_bit_t *in_mode_inactive_pin;
	hal_bit_t *in_mode_homing_pin;
	hal_bit_t *in_mode_jog_pin;
	hal_bit_t *in_mode_machining_pin;
	hal_bit_t *stop_operation_pin;
	hal_u32_t *machine_state_pin;

	/* Fault management */
	hal_bit_t *fault_reset_pin;
	hal_bit_t *machine_in_fault_pin;
	hal_bit_t *machine_homed_pin;

	/* Data-path selection for target position of axis */
	hal_u32_t *datapath_sel_pin;

	/* Axis related data */
	ocno_core_axis_t *axis;

	/* Spindle related data */
	ocno_spindle_data_t *spindle;

	/* Generic error state */
	bool ocno_is_in_error;
} ocno_core_data_t;


static const hal_pindesc_t ocno_core_pins[] = {
	/* Modes management */
	{ HAL_BIT, HAL_IN,  offsetof(ocno_core_data_t, set_mode_inactive_pin),  "%s.set-machine-inactive" },
	{ HAL_BIT, HAL_IN,  offsetof(ocno_core_data_t, set_mode_homing_pin),    "%s.set-machine-homing" },
	{ HAL_BIT, HAL_IN,  offsetof(ocno_core_data_t, set_mode_jog_pin),       "%s.set-machine-jog" },
	{ HAL_BIT, HAL_IN,  offsetof(ocno_core_data_t, set_mode_machining_pin), "%s.set-machine-machining" },
	{ HAL_BIT, HAL_OUT, offsetof(ocno_core_data_t, in_mode_inactive_pin),   "%s.in-machine-inactive" },
	{ HAL_BIT, HAL_OUT, offsetof(ocno_core_data_t, in_mode_homing_pin),     "%s.in-machine-homing" },
	{ HAL_BIT, HAL_OUT, offsetof(ocno_core_data_t, in_mode_jog_pin),        "%s.in-machine-jog" },
	{ HAL_BIT, HAL_OUT, offsetof(ocno_core_data_t, in_mode_machining_pin),  "%s.in-machine-machining" },
	{ HAL_BIT, HAL_IN,  offsetof(ocno_core_data_t, stop_operation_pin),     "%s.stop-operation" },
	{ HAL_U32, HAL_OUT, offsetof(ocno_core_data_t, machine_state_pin),      "%s.machine-state" },
        { HAL_BIT, HAL_OUT, offsetof(ocno_core_data_t, machine_homed_pin),      "%s.machine-homed" },

	/* Fault management */
	{ HAL_BIT, HAL_IN,  offsetof(ocno_core_data_t, fault_reset_pin), "%s.fault-reset" },
	{ HAL_BIT, HAL_OUT, offsetof(ocno_core_data_t, machine_in_fault_pin), "%s.machine-in-fault" },

	/* Data-path selection */
	{ HAL_U32, HAL_OUT,  offsetof(ocno_core_data_t, datapath_sel_pin), "%s.datapath-sel" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t ocno_spindle_pins[] = {
	{ HAL_BIT, HAL_OUT, offsetof(ocno_spindle_data_t, enable_pin), "%s.spindle.enable" },
	{ HAL_BIT, HAL_IN, offsetof(ocno_spindle_data_t, rotation_request_pin), "%s.spindle.request" },
	{ HAL_BIT, HAL_IN, offsetof(ocno_spindle_data_t, can_turn_pin), "%s.spindle.can-turn" },
	{ HAL_FLOAT, HAL_OUT, offsetof(ocno_spindle_data_t, speed_pin), "%s.spindle.speed" },
	{ HAL_BIT, HAL_OUT,  offsetof(ocno_spindle_data_t, fault_reset_pin), "%s.spindle.fault-reset" },
	{ HAL_BIT, HAL_IN, offsetof(ocno_spindle_data_t, velocity_reached_pin), "%s.spindle.velocity-reached" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t ocno_core_axis_pins[] = {
	/* Mode management (drive control) */
	{ HAL_BIT, HAL_OUT, offsetof(ocno_core_axis_t, set_mode_inactive_pin), "%s.axis%c.set-mode-inactive" },
	{ HAL_BIT, HAL_OUT, offsetof(ocno_core_axis_t, set_mode_homing_pin),   "%s.axis%c.set-mode-homing" },
	{ HAL_BIT, HAL_OUT, offsetof(ocno_core_axis_t, set_mode_csp_pin),      "%s.axis%c.set-mode-csp" },
	// { HAL_BIT, HAL_OUT, offsetof(ocno_core_axis_t, set_mode_csv_pin),      "%s.axis%c.set-mode-csv" },
	{ HAL_BIT, HAL_IN,  offsetof(ocno_core_axis_t, in_mode_inactive_pin),  "%s.axis%c.in-mode-inactive" },
	{ HAL_BIT, HAL_IN,  offsetof(ocno_core_axis_t, in_mode_homing_pin),    "%s.axis%c.in-mode-homing" },
	{ HAL_BIT, HAL_IN,  offsetof(ocno_core_axis_t, in_mode_csp_pin),       "%s.axis%c.in-mode-csp" },
	// { HAL_BIT, HAL_IN,  offsetof(ocno_core_axis_t, in_mode_csv_pin),       "%s.axis%c.in-mode-csv" },

	/* Position control */
	{ HAL_FLOAT, HAL_IN,  offsetof(ocno_core_axis_t, current_joint_pos_pin), "%s.axis%c.current-joint-pos" },
	{ HAL_FLOAT, HAL_OUT, offsetof(ocno_core_axis_t, target_pos_pin),        "%s.axis%c.target-pos" },
	{ HAL_FLOAT, HAL_IN,  offsetof(ocno_core_axis_t, position_offset_pin),   "%s.axis%c.position-offset" },

	/* Fault management */
	{ HAL_BIT, HAL_OUT, offsetof(ocno_core_axis_t, fault_reset_pin), "%s.axis%c.fault-reset" },
	{ HAL_BIT, HAL_IN,  offsetof(ocno_core_axis_t, in_fault_pin),    "%s.axis%c.in-fault" },
	{ HAL_BIT, HAL_OUT, offsetof(ocno_core_axis_t, quick_stop_pin), "%s.axis%c.quick-stop" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};


static const hal_pindesc_t ocno_core_axis_params[] = {
	/* Machine limits */
	{ HAL_FLOAT, HAL_RW, offsetof(ocno_core_axis_t, limit_min_param), "%s.axis%c.limit-min" },
	{ HAL_FLOAT, HAL_RW, offsetof(ocno_core_axis_t, limit_max_param), "%s.axis%c.limit-max" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};


/* Core initialization */
void ocno_core_init(void *parent, ocno_core_data_t *data, int comp_id);

/* ocno_send_homing_cmd - Send commands to set the machine in homing mode */
void ocno_send_homing_cmd(ocno_core_data_t *data);

/* ocno_axis_in_homing - check if all axis are in homing mode
 *
 * return true if all axis are in homing mode, false otherwise
*/
bool ocno_axis_in_homing(ocno_core_data_t *data);

/* Set axis with index 'axis_idx' in csp mode */
void ocno_set_axis_csp_mode(ocno_core_data_t *data, int axis_idx);

/* ocno_send_csp_cmd - Send commands to set the machine in csp mode */
void ocno_send_csp_cmd(ocno_core_data_t *data);

/* ocno_axis_in_csp - check if all axis are in csp mode
 *
 * return true if all axis are in csp mode, false otherwise
*/
bool ocno_axis_in_csp(ocno_core_data_t *data);

void ocno_set_axis_rel_target(ocno_core_data_t *data, int axis_idx, double cmd_pos);
void ocno_set_axis_abs_target(ocno_core_data_t *data, int axis_idx, double cmd_pos);
void ocno_set_axis_current_pos(ocno_core_data_t *data, int axis_idx);

void ocno_set_all_axes_current_pos(ocno_core_data_t *data);

bool ocno_spindle_status(ocno_core_data_t *data);
bool ocno_spindle_rotation_required(ocno_core_data_t *data);
bool ocno_can_spindle_turn(ocno_core_data_t *data);
bool ocno_spindle_is_safe(ocno_core_data_t *data);
void ocno_set_spindle_speed(ocno_core_data_t *data, hal_float_t speed);
bool is_spindle_speed_reached(ocno_core_data_t *data);

void ocno_quick_stop_enable(ocno_core_data_t *data);
void ocno_quick_stop_disable(ocno_core_data_t *data);

void ocno_datapath_selection(ocno_core_data_t *data, ocno_datapath_t sel);
ocno_datapath_t ocno_datapath_status(ocno_core_data_t *data);

void ocno_spindle_set(ocno_core_data_t *data, bool state);
void ocno_error_enable(ocno_core_data_t *data);
void ocno_error_disable(ocno_core_data_t *data);

void ocno_hal_func(void *args, long period);


#endif /* OCNO_CORE_H */
