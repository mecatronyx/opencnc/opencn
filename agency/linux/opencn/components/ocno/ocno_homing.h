/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2010 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: ocno_priv.h
 *
 * 'ocno' private header file
 *
 ********************************************************************/

#ifndef OCNO_HOMING_H
#define OCNO_HOMING_H

#define OCNO_HOMING_MODE_NAME    "homing"

typedef struct {
	hal_bit_t *start_pin;
	hal_bit_t *stop_pin;
	hal_bit_t *homed_pin;
	hal_float_t *move_offset_pin;
} ocno_homing_axis_t;

typedef struct {
	void *parent;

	/* Homing PINs */
	hal_bit_t *start_pin;
	hal_bit_t *stop_pin;
	hal_bit_t *processing_pin;

	/*  Per Axis Homing PINs */
	ocno_homing_axis_t *axis;

	/* processing axis */
	unsigned process_axis;
} ocno_homing_data_t;


static const hal_pindesc_t ocno_homing_pins[] = {
	{ HAL_BIT, HAL_IN,  offsetof(ocno_homing_data_t, start_pin),      "%s.%s.start" },
	{ HAL_BIT, HAL_IN,  offsetof(ocno_homing_data_t, stop_pin),       "%s.%s.stop" },
	{ HAL_BIT, HAL_OUT, offsetof(ocno_homing_data_t, processing_pin), "%s.%s.processing" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t ocno_homing_axis_pins[] = {
	{ HAL_BIT,   HAL_OUT, offsetof(ocno_homing_axis_t, start_pin),       "%s.%s.axis%c.start" },
	{ HAL_BIT,   HAL_OUT, offsetof(ocno_homing_axis_t, stop_pin),        "%s.%s.axis%c.stop" },
	{ HAL_BIT,   HAL_IN,  offsetof(ocno_homing_axis_t, homed_pin),       "%s.%s.axis%c.homed" },
	{ HAL_FLOAT, HAL_IN,  offsetof(ocno_homing_axis_t, move_offset_pin), "%s.%s.axis%c.move-offset" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

/* == Homing mode functions ================================================ */

// void ocno_homing_init(ocno_homing_data_t *data, int comp_id);
void ocno_homing_init(void *parent, ocno_homing_data_t *data, int comp_id);
void ocno_homing_fsm_exec(ocno_homing_data_t *data, bool new_mode);
bool ocno_is_homed(ocno_homing_data_t *data);

#endif /* OCNO_HOMING_H */
