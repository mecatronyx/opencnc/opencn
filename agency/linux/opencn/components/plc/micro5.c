/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 * Copyright (C) 2022 Xavier Soltner <xavier.soltner@heig-vd.ch>
 *
 * file: micro5.c
 *
 *  micro5 PLC code
 *
 ********************************************************************/

#include <linux/kernel.h>

#include <opencn/hal/hal.h>

#include <opencn/uapi/plc.h>
#include "plc_priv.h"


typedef struct {

	/* Fault */
	hal_bit_t *is_x_in_fault_pin;
	hal_bit_t *is_y_in_fault_pin;
	hal_bit_t *is_z_in_fault_pin;
	hal_bit_t *is_b_in_fault_pin;
	hal_bit_t *is_c_in_fault_pin;
	hal_bit_t *stop_operation_pin;

	hal_bit_t *is_estop_pushed_pin;	
	hal_bit_t *is_milling;
	hal_bit_t *is_z_enabled_pin;
	hal_bit_t *release_break_z_pin;
	hal_bit_t *is_b_enabled_pin;
	hal_bit_t *is_c_enabled_pin;
	hal_bit_t *is_spindle_enabled_pin;
	hal_bit_t *b_c_s_axis_sealing_pin;
	hal_bit_t *cmd_free_palette_pin;
	hal_bit_t *free_palette_pin;
	hal_bit_t *blowing_palette_pin;
} plc_micro5_data_t;


static const hal_pindesc_t micro5_pins[] = {

	/* Get axis error state */
	{ HAL_BIT, HAL_IN,  offsetof(plc_micro5_data_t, is_x_in_fault_pin), "%s.is_x_in_fault" },
	{ HAL_BIT, HAL_IN,  offsetof(plc_micro5_data_t, is_y_in_fault_pin), "%s.is_y_in_fault" },
	{ HAL_BIT, HAL_IN,  offsetof(plc_micro5_data_t, is_z_in_fault_pin), "%s.is_z_in_fault" },
	{ HAL_BIT, HAL_IN,  offsetof(plc_micro5_data_t, is_b_in_fault_pin), "%s.is_b_in_fault" },
	{ HAL_BIT, HAL_IN,  offsetof(plc_micro5_data_t, is_c_in_fault_pin), "%s.is_c_in_fault" },

	/* Emergency Stop (EStop) & security */
	{ HAL_BIT, HAL_IN,  offsetof(plc_micro5_data_t, is_estop_pushed_pin), "%s.is_EStop_pushed" },
	{ HAL_BIT, HAL_IN,  offsetof(plc_micro5_data_t, is_milling), "%s.is_milling" },

	/* To tell other  component to stop softare operation */
	{ HAL_BIT, HAL_OUT,  offsetof(plc_micro5_data_t, stop_operation_pin), "%s.stop_operation" },

	/* Z axis */
	{ HAL_BIT, HAL_IN,  offsetof(plc_micro5_data_t, is_z_enabled_pin), "%s.is_z_enabled" },
	{ HAL_BIT, HAL_OUT, offsetof(plc_micro5_data_t, release_break_z_pin), "%s.release_break_z" },

	/* Rotary axis state */
	{ HAL_BIT, HAL_IN,  offsetof(plc_micro5_data_t, is_b_enabled_pin), "%s.is_b_enabled" },
	{ HAL_BIT, HAL_IN,  offsetof(plc_micro5_data_t, is_c_enabled_pin), "%s.is_c_enabled" },
	{ HAL_BIT, HAL_IN,  offsetof(plc_micro5_data_t, is_spindle_enabled_pin), "%s.is_spindle_enabled" },

	/* axis sealing */
	{ HAL_BIT, HAL_OUT, offsetof(plc_micro5_data_t, b_c_s_axis_sealing_pin), "%s.b_c_s_axis_sealing" },

	/* Palette control */
	{ HAL_BIT, HAL_IN,  offsetof(plc_micro5_data_t, cmd_free_palette_pin), "%s.cmd_free_palette" },
	{ HAL_BIT, HAL_OUT, offsetof(plc_micro5_data_t, free_palette_pin), "%s.free_palette" },
	{ HAL_BIT, HAL_OUT, offsetof(plc_micro5_data_t, blowing_palette_pin), "%s.blowing_palette" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static int comp_id; /* component ID */
static plc_micro5_data_t *micro5_data;
#define PLC_MICRO5_NAME "micro5"

static void plc_micro5_func(void *arg, long period)
{
	plc_micro5_data_t *data = (plc_micro5_data_t *)arg;
	bool is_axis_fault = false;

	/* Quickstop all axis if one is in fault*/
	is_axis_fault |= *data->is_x_in_fault_pin || *data->is_y_in_fault_pin 
				|| *data->is_z_in_fault_pin || *data->is_b_in_fault_pin 
				|| *data->is_c_in_fault_pin;
	
	if (is_axis_fault || *data->is_estop_pushed_pin) {
		*data->stop_operation_pin = 1;
	} else {
		*data->stop_operation_pin = 0;
	}

	/* Break Z axis if axis is disabled*/
	*data->release_break_z_pin = *data->is_z_enabled_pin;

	/* Axis Sealing */
	*data->b_c_s_axis_sealing_pin = *data->is_b_enabled_pin || *data->is_c_enabled_pin || *data->is_spindle_enabled_pin;
	
	/* Freeing the palette */
	if ( *data->cmd_free_palette_pin && *data->is_milling == 0 ) {
		*data->free_palette_pin = true;
		*data->blowing_palette_pin = true;
	} else {
		*data->free_palette_pin = false;
		*data->blowing_palette_pin = false;
	}
	
}

int plc_micro5_init(const char *name)
{
	int ret;

	plc_func_param_t params;

	params.name       = name;
	params.comp_id    = comp_id;
	params.data       = micro5_data;
	params.data_size  = sizeof(plc_micro5_data_t);
	params.func       = plc_micro5_func;
	params.hal_pins   = micro5_pins;
	params.hal_params = NULL;

    ret = plc_init(&params);

	return ret;
}
