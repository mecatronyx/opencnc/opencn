/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2010 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: plc_priv.h
 *
 * It is the kernel part of the "plc" OpenCN HAL component. It exports the
 * functions related to the cfg received for the user-space
 *
 ********************************************************************/

#ifndef UAPI_PLC_PRIV_H
#define UAPI_PLC_PRIV_H

#include <opencn/uapi/plc.h>

typedef void (*plc_func_t)(void *arg, long period);

typedef struct {
    const char          *name;
    int                  comp_id;
    void                *data;
    int                  data_size;
    plc_func_t           func;
    const hal_pindesc_t *hal_pins;
    const hal_pindesc_t *hal_params;
} plc_func_param_t;

int plc_init(plc_func_param_t *params);

#endif /* UAPI_PLC_PRIV_H */
