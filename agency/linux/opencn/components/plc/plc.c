/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: plc.c
 *
 *
 * It is the kernel part of the "plc" OpenCN HAL component. It exports the
 * functions related to the cfg received for the user-space
 *
 ********************************************************************/

#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>

#include <opencn/hal/hal.h>
#include <opencn/uapi/plc.h>
#include "plc_priv.h"

/************************************************************************
 *                       PLC SPECIFIC PROTOTYPE                         *
 ************************************************************************/

int plc_micro5_init(const char *name);
int plc_ocno_dbg_init(const char *name);

/************************************************************************
 *                       INIT AND EXIT CODE                             *
 ************************************************************************/

int plc_init(plc_func_param_t *params)
{
	int ret;
	char comp_name[HAL_NAME_LEN];

	sprintf(comp_name, "%s.%s", PLC_COMP_NAME, params->name);

	params->comp_id = hal_init(__core_hal_user, comp_name);
	if (params->comp_id < 0) {
		printk("[plc/%s] Error: hal_init() failed\n", params->name);
		return -EINVAL;
	}

	params->data = hal_malloc(__core_hal_user, params->data_size);
	if (!params->data) {
		printk("[plc/%s] Failed to allocate memory\n", params->name);
		goto fail;
	}

	/* export PINs */
	if (params->hal_pins) {
		ret = hal_pin_newf_list(__core_hal_user, params->comp_id, params->data, params->hal_pins, comp_name);
		if (ret != 0) {
			printk("[plc/%s] PINs export failed\n", params->name);
			goto fail;
		}
	}

	/* export Params */
	if (params->hal_params) {
		ret = hal_param_newf_list(__core_hal_user, params->data, params->comp_id, params->hal_params, comp_name);
		if (ret != 0) {
			printk("[plc/%s] Params export failed\n", params->name);
			goto fail;
		}
	}

	/* export function */
	ret = hal_export_funct(__core_hal_user, comp_name, params->func, params->data, 1, 0, params->comp_id);
	if (ret != 0) {
		printk("[plc/%s] Error: function export failed\n", params->name);
		goto fail;
	}

	hal_ready(__core_hal_user, params->comp_id);

	return 0;

fail:
	hal_exit(__core_hal_user, params->comp_id);

	return ret;
}

static int plc_app_main(plc_connect_args_t *arg)
{
	int ret = 0;

	printk("[PLC] machine %s, id: %d\n", arg->name, arg->id);

	switch (arg->id) {
	case PLC_CFG_MICRO3:
		printk("[PLC] %s not implemented yet\n", arg->name);
		break;
	case PLC_CFG_MICRO5:
		ret = plc_micro5_init(arg->name);
		break;
	case PLC_CFG_COREXY:
		printk("[PLC] %s not implemented yet\n", arg->name);
		break;
	case PLC_CFG_DEMONSTRATOR:
		printk("[PLC] %s not implemented yet\n", arg->name);
		break;
	case PLC_CFG_OCNO_DBG:
		ret = plc_ocno_dbg_init(arg->name);
		break;
	default:
		printk("[PLC] %s is not a supported config\n", arg->name);
		ret = -1;
	}

	return ret;
}

/************************************************************************
 *                Char Device & file operation definitions              *
 ************************************************************************/

static int plc_open(struct inode *inode, struct file *file)
{
	return 0;
}

static int plc_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static long plc_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int rc = 0;

	switch (cmd) {
	case PLC_IOCTL_CONNECT:

		rc = plc_app_main((plc_connect_args_t *)arg);
		if (rc)
			printk("%s: failed to initialize...\n", __func__);

	case PLC_IOCTL_DISCONNECT:
		break;
	}

	return rc;
}

struct file_operations plc_fops = {
	.owner = THIS_MODULE,
	.open = plc_open,
	.release = plc_release,
	.unlocked_ioctl = plc_ioctl,
};

int plc_comp_init(void)
{
	printk("OpenCN: plc subsystem initialization.\n");

	hal_create_interface(PLC_COMP_NAME, &plc_fops);

	return 0;
}

late_initcall(plc_comp_init)
