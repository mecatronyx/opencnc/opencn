/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: ocno_debug.c
 *
 *  Code use to test / debug 'ocno' component. It simulates lcec/drive behavior
 *  on one axis
 *
 ********************************************************************/

#include <linux/kernel.h>

#include <soo/uapi/soo.h>
#include <soo/uapi/console.h>

#include <opencn/hal/hal.h>
#include <opencn/ctypes/strings.h>

#include <opencn/uapi/plc.h>
#include "plc_priv.h"

#define OCNO_MODE_INACTIVE_POS    0
#define OCNO_MODE_HOMING_POS      1
#define OCNO_MODE_CSP_POS         2

#define OCNO_MODE_NO_MODE     0
#define OCNO_MODE_INACTIVE   (1 << OCNO_MODE_INACTIVE_POS)
#define OCNO_MODE_HOMING     (1 << OCNO_MODE_HOMING_POS)
#define OCNO_MODE_CSP        (1 << OCNO_MODE_CSP_POS)


#define HOMING_STATE_WAIT_CMD     0
#define HOMING_STATE_PROCESSING   1
#define HOMING_STATE_END          2

/* Homing duration, in second */
#define HOMING_DURATION_MIN       5

/* Increment current position, in mm, used in homing & csp */
#define POSITION_STEPS    (0.1)

typedef struct {
	/* PINs */
	hal_bit_t   *set_mode_inactive_pin;
	hal_bit_t   *set_mode_homing_pin;
	hal_bit_t   *set_mode_csp_pin;
	hal_bit_t   *in_mode_inactive_pin;
	hal_bit_t   *in_mode_homing_pin;
	hal_bit_t   *in_mode_csp_pin;
	hal_float_t *target_position_pin;
	hal_float_t *current_position_pin;
	hal_bit_t   *homed_pin;
	hal_bit_t   *start_homing_pin;
	hal_bit_t   *stop_homing_pin;
	hal_bit_t   *csp_running_pin;
	hal_bit_t   *in_fault_pin;
	hal_bit_t   *fault_reset_pin;
	hal_bit_t   *set_fault_pin;

	/* PARAMs */
	hal_u32_t   homing_duration_param;
	hal_float_t pos_steps_param;

	unsigned cur_mode;

	uint64_t homing_start_time;

} plc_ocno_dbg_data_t;

static const hal_pindesc_t ocno_dbg_pins[] = {

	{ HAL_BIT, HAL_IN, offsetof(plc_ocno_dbg_data_t, set_mode_inactive_pin), "%s.set-mode-inactive" },
	{ HAL_BIT, HAL_IN, offsetof(plc_ocno_dbg_data_t, set_mode_homing_pin),   "%s.set-mode-homing" },
	{ HAL_BIT, HAL_IN, offsetof(plc_ocno_dbg_data_t, set_mode_csp_pin),      "%s.set-mode-csp" },

	{ HAL_BIT, HAL_OUT, offsetof(plc_ocno_dbg_data_t, in_mode_inactive_pin), "%s.in-mode-inactive" },
	{ HAL_BIT, HAL_OUT, offsetof(plc_ocno_dbg_data_t, in_mode_homing_pin),   "%s.in-mode-homing" },
	{ HAL_BIT, HAL_OUT, offsetof(plc_ocno_dbg_data_t, in_mode_csp_pin),      "%s.in-mode-csp" },

	{ HAL_FLOAT, HAL_IN,  offsetof(plc_ocno_dbg_data_t, target_position_pin),  "%s.target-position" },
	{ HAL_FLOAT, HAL_OUT, offsetof(plc_ocno_dbg_data_t, current_position_pin), "%s.current-position" },

	{ HAL_BIT, HAL_IN,  offsetof(plc_ocno_dbg_data_t, start_homing_pin), "%s.start-homing" },
	{ HAL_BIT, HAL_IN,  offsetof(plc_ocno_dbg_data_t, stop_homing_pin),  "%s.stop-homing" },
	{ HAL_BIT, HAL_OUT, offsetof(plc_ocno_dbg_data_t, homed_pin),        "%s.homed" },

	{ HAL_BIT, HAL_OUT, offsetof(plc_ocno_dbg_data_t, csp_running_pin),    "%s.csp-is_running" },

	{ HAL_BIT, HAL_OUT, offsetof(plc_ocno_dbg_data_t, in_fault_pin),    "%s.in-fault" },
	{ HAL_BIT, HAL_IN,  offsetof(plc_ocno_dbg_data_t, fault_reset_pin), "%s.fault-reset" },
	{ HAL_BIT, HAL_IN,  offsetof(plc_ocno_dbg_data_t, set_fault_pin),   "%s.set-fault" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const hal_pindesc_t ocno_dbg_params[] = {
	{ HAL_U32,   HAL_RW, offsetof(plc_ocno_dbg_data_t, homing_duration_param), "%s.homing-duration" },
	{ HAL_FLOAT, HAL_RW, offsetof(plc_ocno_dbg_data_t, pos_steps_param),       "%s.position-step" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};


static int comp_id; /* component ID */
static plc_ocno_dbg_data_t *ocno_dbg_data;


static void set_inactive_mode(plc_ocno_dbg_data_t *data)
{
	*data->in_mode_inactive_pin = 1;
	*data->in_mode_homing_pin   = 0;
	*data->in_mode_csp_pin      = 0;

	data->cur_mode =  OCNO_MODE_INACTIVE;
}

static void homing_process(plc_ocno_dbg_data_t *data, bool init)
{
	uint64_t now;
	static int state = 0;
	static int last_state = -1;
	bool inited = false;
	bool completed = false;

	if (init) {
		state = HOMING_STATE_WAIT_CMD;
		inited = true;
	}

	if (inited)
		printk("== DEBUG - Starting HOMING mode\n");


	if (last_state != state) {
		printk("== DEBUG - HOMING state: %d\n", state);
		last_state = state;
	}

	if (*data->stop_homing_pin)
		state = HOMING_STATE_WAIT_CMD;

	switch (state) {
		case HOMING_STATE_WAIT_CMD:

			if (*data->start_homing_pin) {
				*data->homed_pin        = 0;
				*data->start_homing_pin = 0;
				state = HOMING_STATE_PROCESSING;

				data->homing_start_time = rtapi_get_time();

				printk("== DEBUG - starting homing processing \n");

			}

			break;

		case HOMING_STATE_PROCESSING:

			/* Update the current position... */
			*data->current_position_pin += data->pos_steps_param;

			/* Check duration */
			now = rtapi_get_time();
			if ((now - data->homing_start_time) > SECONDS(data->homing_duration_param))
				state = HOMING_STATE_END;

			break;

		case HOMING_STATE_END:
			/* Homing --> move to 0 */
			if (*data->current_position_pin > 0) {
				*data->current_position_pin -= data->pos_steps_param;

				if (*data->current_position_pin < 0)
					completed = true;

			} else {
				*data->current_position_pin += data->pos_steps_param;

				if (*data->current_position_pin > 0)
					completed = true;
			}

			/* Check if the homing has completed */
			if (completed) {

				opencn_printf("== DEBUG - homing end, current pos: %f\n", *data->current_position_pin);

				*data->current_position_pin = 0;
				*data->homed_pin = 1;
				state = HOMING_STATE_WAIT_CMD;
			}
			break;

		default:
			/* Error - not supported homing state ! */
			break;
	}
}

static void csp_process(plc_ocno_dbg_data_t *data)
{
	if (*data->target_position_pin == *data->current_position_pin) {
		*data->csp_running_pin = 0;

	} else {

		*data->csp_running_pin = 1;

		if (*data->target_position_pin > *data->current_position_pin) {

			*data->current_position_pin += POSITION_STEPS;

			if (*data->current_position_pin >= *data->target_position_pin)
				*data->current_position_pin = *data->target_position_pin;

		} else {

			*data->current_position_pin -= POSITION_STEPS;

			if (*data->current_position_pin <= *data->target_position_pin)
				*data->current_position_pin = *data->target_position_pin;

		}
	}
}

static void plc_ocno_dbg_func(void *arg, long period)
{
	plc_ocno_dbg_data_t *data = (plc_ocno_dbg_data_t *)arg;
	unsigned mode = 0;
	bool new_mode = false;

	/* Fault management - the fault is set by user */
	if (*data->set_fault_pin) {
		*data->in_fault_pin  = 1;
		*data->set_fault_pin = 0;

		*data->csp_running_pin = 0;

		set_inactive_mode(data);

		return;
	}

	if (*data->fault_reset_pin) {
		*data->fault_reset_pin = 0;
		*data->in_fault_pin    = 0;
	}

	/* Mode management */
	mode = (*(data->set_mode_inactive_pin) << OCNO_MODE_INACTIVE_POS) |
           (*(data->set_mode_homing_pin)   << OCNO_MODE_HOMING_POS)   |
           (*(data->set_mode_csp_pin)      << OCNO_MODE_CSP_POS);

	/* Check if new mode is requested */
	if (mode) {
#if 0 /* Should env print errors ? */
		if (mode == data->cur_mode) {
			/* ERROR - already in this mode ! */
			printk("")
		} else {
			new_mode = true;
			data->cur_mode = mode;
		}
#else
		new_mode = true;
		data->cur_mode = mode;
#endif
	}

	if (new_mode)
		printk("DEBUG - new mode selected: %d\n", data->cur_mode);


	switch (data->cur_mode) {
	case OCNO_MODE_INACTIVE:
		if (new_mode)
			set_inactive_mode(data);
		break;
	case OCNO_MODE_HOMING:
		homing_process(data, new_mode);
		break;
	case OCNO_MODE_CSP:
		csp_process(data);
		break;

	default:
		/* No supported case */
		break;
	}

	/* Inform on the current state */
	*data->in_mode_inactive_pin = data->cur_mode & OCNO_MODE_INACTIVE;
	*data->in_mode_homing_pin   = data->cur_mode & OCNO_MODE_HOMING;
	*data->in_mode_csp_pin      = data->cur_mode & OCNO_MODE_CSP;

	/* Reset commands PIN */
	*data->set_mode_inactive_pin = 0;
	*data->set_mode_homing_pin   = 0;
	*data->set_mode_csp_pin      = 0;
}

int plc_ocno_dbg_init(const char *name)
{
	int ret;

	plc_func_param_t params;

	params.name       = name;
	params.comp_id    = comp_id;
	params.data       = ocno_dbg_data;
	params.data_size  = sizeof(plc_ocno_dbg_data_t);
	params.func       = plc_ocno_dbg_func;
	params.hal_pins   = ocno_dbg_pins;
	params.hal_params = ocno_dbg_params;

	ret = plc_init(&params);

	/* Set Params default value */
	((plc_ocno_dbg_data_t *)params.data)->homing_duration_param = HOMING_DURATION_MIN;
	((plc_ocno_dbg_data_t *)params.data)->pos_steps_param       = POSITION_STEPS;

	/* Set 'machine' in inactive mode */
	((plc_ocno_dbg_data_t *)params.data)->cur_mode = OCNO_MODE_INACTIVE;

	return ret;
}
