/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2023 by Xavier Soltner <xavier.soltner@heig-vd.ch>
 *
 * file: sampler.c
 *
 * It is the kernel part of the "sampler" OpenCN HAL component.
 * The kernel part init the component and send the value of the pin
 * to the user space part through a ring buffer for the userspace part
 * to process.
 *
 ********************************************************************/

#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/ctype.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>

#include <opencn/rtapi/rtapi.h>            /* RTAPI realtime OS API */
#include <opencn/rtapi/rtapi_app.h>        /* RTAPI realtime module decls */
#include <opencn/hal/hal.h>                /* HAL public API decls */

#include <opencn/components/streamer.h>

#include <opencn/hal/hal.h>
#include <opencn/strtox.h>

#include <opencn/ctypes/strings.h>

#include <opencn/rtapi/rtapi_errno.h>
#include <opencn/rtapi/rtapi_string.h>

#include <opencn/uapi/hal.h>
#include <opencn/uapi/sampler.h>

/***********************************************************************
 *                       DEBUG FUNCTION		                           *
 ************************************************************************/

/* Debug Sampler */
#if 0
#define DEBUG_SAMP
#endif

#define MSG_MAX_LEN 200
void debug_samp(const char* text, ...)
{
	#ifdef DEBUG_SAMP
	#warning Only print when 'text' is changed
	va_list args;
	static char last_text[MSG_MAX_LEN];

	if (text == NULL) 
		return;

	/* Check max message length */
	if (strlen(text) > MSG_MAX_LEN){
		rtapi_print_msg(RTAPI_MSG_ERR, "[%s] Error: Message longer than %d,no print\n", SAMPLER_COMP_NAME, MSG_MAX_LEN);
		return;
	}

	va_start(args, text);

	/* Check if UNFORMATTED text is different */
	if (strcmp(last_text, text) != 0 ){
		opencn_printf(text,args);
		//Save for next iteration
		strcpy(last_text, text);
	}

	va_end(args);
	#endif
}

/***********************************************************************
 *                       RING BUFFER FUNCTION                           *
 ************************************************************************/

void samp_rg_alloc(samp_rg_t *rg, int capacity)
{
	rg->head = 0;
	rg->tail = 0;
	rg->capacity = capacity;
	rg->size = 0;
	rg->sampler_data = kmalloc(sizeof(sampler_sample_t) * capacity, GFP_ATOMIC);
}

void samp_rg_free(samp_rg_t *rg)
{
	rg->capacity = 0;
	rg->size = 0;
	rg->tail = 0;
	rg->head = 0;
	kfree(rg->sampler_data);
}

int samp_rg_push(samp_rg_t *rg, sampler_sample_t value)
{
	if (rg->size < rg->capacity) {
		rg->sampler_data[rg->head++] = value;
		__sync_fetch_and_add(&rg->size, 1);
		if (rg->head == rg->capacity)
			rg->head = 0;
		return 0;
	} else {
		/* Userspace was too slow, lost one frame */
		/* For specific application, we could implement a dynamic+temporary
		 * allocation of memory in the RT side (here) IF the userside is
		 * overrun to avoid losing data and to avoid needing specific hardware 
		 * (SSD external disk for example)
		 */

		return 1;

	}
}

int samp_rg_pop(samp_rg_t *rg, sampler_sample_t *value)
{
	int i;
	int nb_sample;

	nb_sample = rg->size;

	if (nb_sample > 0) {
		for( i = 0; i < nb_sample; i++ ){
			value[i] = rg->sampler_data[rg->tail];
			rg->tail++;
			if (rg->tail == rg->capacity)
				rg->tail = 0;
		}
		
		__sync_fetch_and_add(&rg->size, -rg->size);

		return nb_sample;
	}
	return 0;
}

void samp_rg_clear(samp_rg_t *rg)
{
	rg->size = 0;
	rg->head = 0;
	rg->tail = 0;
}

/***********************************************************************
 *                STRUCTURES AND GLOBAL VARIABLES                       *
 ************************************************************************/

/* Sampler State */
typedef enum {
	SAMPLER_INACTIVE,
	SAMPLER_ENABLED,
	SAMPLER_RESET,
} sampler_state_t;

/* this structure contains the HAL shared memory data for one sampler */
typedef struct {
	struct list_head list;

	/* Timing */
	double dt;			// time between each cycle in [s]
	double cur_time;	// absolute current local time of the component instance in [s]

	/* History */
	double prev_warn_time;			/* Last local time a warning about lost frame was prompt */
	unsigned int prev_overruns_nb;	/* Last number of overruns when prompted */

	/* Input argument stored from userspace */
	char comp_name [HAL_NAME_LEN + 1]; 	/* sampler.<comp_name>*/	
	char cfg [SAMPLER_MAX_PINS]; 		/* pins configuration*/
	int nb_pins;						/* Number of sampled pin */
	int depth;							/* Depth of ring buffer */
	
	int comp_id;						/* HAL component ID */
	int instance_nr;					/* Instance number */
	bool did_enable;					/* Was the sampler enabled*/
	sampler_state_t state;				/* Sampler State */
	unsigned long n_tag;				/* Sample number */
	bool memory;					/* Sampler in memory mode */

	/* Interaction/Status pin */
	hal_bit_t *enable;		/* enable sampling */
	hal_bit_t *new_file;    /* Create a new file */
	hal_s32_t *overruns;	/* number of sample lost */
    
	/* Params */
	hal_float_t loss_warn; 	/* Prompt warning every X seconds on frame loss, if negative, desactivated*/

	/* Sampled pin */
    pin_data_t pins[SAMPLER_MAX_PINS]; /* Data pointer to HAL pin */
} sampler_data_t;

/* other globals */
static int sampler_instance_nr = 0;
samp_rg_t sampler_queue[SAMPLER_MAX_INSTANCE];

LIST_HEAD(samplers);


/***********************************************************************
 *            REALTIME COUNTER COUNTING AND UPDATE FUNCTIONS            *
 ************************************************************************/

static void sample_update(void *arg, long period)
{
	int i = 0;
	sampler_data_t *samp_data = (sampler_data_t *) arg;
	sampler_sample_t data;
	pin_data_t *pptr;

    /* Updating current time */
	samp_data->dt = period / 1e9;
	samp_data->cur_time = samp_data->cur_time + samp_data->dt;

	/* Set sampler state*/
	if(*samp_data->enable){	
		samp_data->state=SAMPLER_ENABLED;
		/* reset tag*/
		if (!samp_data->did_enable){
			samp_data->n_tag = 0;
			samp_data->did_enable = true;
		} else {
			samp_data->n_tag++;
		}
	} else {
		if (samp_data->did_enable) {
			samp_data->state = SAMPLER_RESET;
		}
	}

	/* FSM State */
	switch (samp_data->state){
		case SAMPLER_INACTIVE :
			/* Nothing done */
			break;

		case SAMPLER_ENABLED :
			/* Update pins*/
			data.n_tag = samp_data->n_tag;
			data.n_pins = samp_data->nb_pins;
			pptr = samp_data->pins;
			for ( i = 0; i < samp_data->nb_pins; i++){
				switch (samp_data->cfg[i]){
					case 'b' :
					case 'B' :
						data.pins[i].type = HAL_BIT;
						data.pins[i].b = *pptr->hbit;
						break;
					case 'u' :
					case 'U' :
						data.pins[i].type = HAL_U32;
						data.pins[i].u = *pptr->hu32;
						break;
					case 's' : 
					case 'S' :
						data.pins[i].type = HAL_S32;	
						data.pins[i].s = *pptr->hs32;
						break;
					case 'f' : 
					case 'F' :
						data.pins[i].type = HAL_FLOAT;
						data.pins[i].f = *pptr->hfloat;
						break;
					default : 
						/* Nothing done */
						break;
				}
				/* Update pin pointer */
				pptr++;
			}

			/* Push value */
			if(samp_rg_push(&sampler_queue[samp_data->instance_nr], data) == 1){
				/* Lost frame */
				*samp_data->overruns = *samp_data->overruns + 1;
			}

			break;


		case SAMPLER_RESET :
			/* Clear Ring buffer if emptied by userspace */
			if(sampler_queue[samp_data->instance_nr].size == 0){
				samp_rg_clear(&sampler_queue[samp_data->instance_nr]);
				samp_data->state = SAMPLER_INACTIVE;
				samp_data->did_enable=false;
			}
			
			break;
	}

	/* Prompt warning if overruns & requested by param */
	if (samp_data->loss_warn > 0.0f ){
		if ( samp_data->prev_warn_time + samp_data->loss_warn < samp_data->cur_time &&
			samp_data->prev_overruns_nb != *samp_data->overruns){
			
			printk("[%s.%s] Lost %d frames !\n",SAMPLER_COMP_NAME, samp_data->comp_name, *samp_data->overruns - samp_data->prev_overruns_nb);

			samp_data->prev_warn_time = samp_data->cur_time;
			samp_data->prev_overruns_nb	 = *samp_data->overruns;
		}
	} 

	return;
}

/***********************************************************************
 *                       INIT AND EXIT CODE                             *
 ************************************************************************/
static int init_sampler(sampler_data_t *samp) {
	int retval, usefp, i;
	pin_data_t *pptr;
	char buf[HAL_NAME_LEN + 1];
	
	/* Alloc ring buffer */
	samp_rg_alloc(&sampler_queue[samp->instance_nr], samp->depth);

	/* export "standard" pins and params */
	retval = hal_pin_bit_newf(__core_hal_user, HAL_IN, &(samp->enable), samp->comp_id, "%s.%s.enable", SAMPLER_COMP_NAME, samp->comp_name);
	if (retval != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "SAMPLER: ERROR: 'enable' pin export failed\n");
		return -EIO;
	}
	retval = hal_pin_s32_newf(__core_hal_user, HAL_IO, &(samp->overruns), samp->comp_id, "%s.%s.overruns", SAMPLER_COMP_NAME, samp->comp_name);
	if (retval != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "SAMPLER: ERROR: 'overruns' pin export failed\n");
		return -EIO;
	}
	retval = hal_param_float_newf(__core_hal_user, HAL_RW, &(samp->loss_warn), samp->comp_id, "%s.%s.loss-warn", SAMPLER_COMP_NAME, samp->comp_name);
	if (retval != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "SAMPLER: ERROR: 'loss-warn' parameter export failed\n");
		return -EIO;
	}
	/* Export pin related to file mode */
	if (!samp->memory){
		retval = hal_pin_bit_newf(__core_hal_user, HAL_IN, &(samp->new_file), samp->comp_id, "%s.%s.new-file", SAMPLER_COMP_NAME, samp->comp_name);
		if (retval != 0) {
			rtapi_print_msg(RTAPI_MSG_ERR, "SAMPLER: ERROR: 'new-file' pin export failed\n");
			return -EIO;
		}
	}

	/* Init the standard pins and params */
	*(samp->enable) = 0;
	*(samp->overruns) = 0;
	samp->state = SAMPLER_INACTIVE;
	samp->cur_time = 0.0f;
	samp->loss_warn = SAMPER_WARN_TIME; 
	samp->prev_overruns_nb = 0;
	samp->prev_warn_time = 0.0;

	/* Init file mode pin */
	if(!samp->memory){
		*(samp->new_file) = 1; // 1-> force header writing at init
	}

	/* Export input sampled pin */
	pptr = samp->pins;
	for (i = 0; i < samp->nb_pins; i++){
		rtapi_snprintf(buf, sizeof(buf), "%s.%s.pin.%d", SAMPLER_COMP_NAME, samp->comp_name, i);		
		switch (samp->cfg[i]) {
			case 'b' : 
			case 'B' : 
				retval = hal_pin_new(__core_hal_user, buf, HAL_BIT, HAL_IN, (void **) pptr, samp->comp_id);
				break;
			case 'u' : 
			case 'U' : 
				retval = hal_pin_new(__core_hal_user, buf, HAL_U32, HAL_IN, (void **) pptr, samp->comp_id);
				break;
			case 's' : 
			case 'S' : 
				retval = hal_pin_new(__core_hal_user, buf, HAL_S32, HAL_IN, (void **) pptr, samp->comp_id);
				break;
			case 'f' : 
			case 'F' : 
				retval = hal_pin_new(__core_hal_user, buf, HAL_FLOAT, HAL_IN, (void **) pptr, samp->comp_id);
				usefp = 1;
				break;
			default : 
				rtapi_print_msg(RTAPI_MSG_ERR, "SAMPLER: ERROR samp->cfg[%d]=%c not supported\n",i,samp->cfg[i]);
				break;
		}

		if (retval != 0) {
			rtapi_print_msg(RTAPI_MSG_ERR, "SAMPLER: ERROR: pin '%s' export failed\n", buf);
			return -EIO;
		}
		pptr++;
	}

	/* export update function */
	rtapi_snprintf(buf, sizeof(buf), "sampler.%s", samp->comp_name);
	retval = hal_export_funct(__core_hal_user, buf, sample_update, samp, usefp, 0, samp->comp_id);
	if (retval != 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "SAMPLER: ERROR: function export failed\n");
		return retval;
	}

	return 0;

}

/* opencn - This part of code comes from the user space counterpart. */

static int sampler_user_init(sampler_connect_args_t *args, int major, int minor) {
	hal_user_t *hal_user;
	char comp_name[HAL_NAME_LEN + 1];

	hal_user = find_hal_user_by_dev(major, minor);
	if (!hal_user) {
		hal_user = (hal_user_t *) kmalloc(sizeof(hal_user_t), GFP_ATOMIC);
		if (!hal_user) {
            BUG();
		}
		memset(hal_user, 0, sizeof(hal_user_t));

		/* Get the current related PID. */
		hal_user->pid = current->pid;
		hal_user->major = major;
		hal_user->minor = minor;
		hal_user->channel = args->channel;

		add_hal_user(hal_user);
	}

	snprintf(comp_name, sizeof(comp_name), "HAL_%s.%s", SAMPLER_COMP_NAME, args->name);
	hal_user->comp_id = hal_init(hal_user, comp_name);

	hal_ready(hal_user, hal_user->comp_id);

	return 0;
}

static int sampler_app_main(sampler_connect_args_t *args, int n) {

	int retval, comp_id;
	char comp_name[HAL_NAME_LEN];
	sampler_data_t *samp_data;

	sprintf(comp_name, "%s.%s", SAMPLER_COMP_NAME, args->name);
	comp_id = hal_init(__core_hal_user, comp_name);
	if (comp_id < 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "SAMPLER: ERROR: hal_init() failed\n");
		retval = -EINVAL;
		goto fail;
	}
	samp_data = hal_malloc(__core_hal_user, sizeof(sampler_data_t));

	/* Store parameter on kernel side */
	strcpy(samp_data->comp_name, args->name);
	strcpy(samp_data->cfg, args->cfg);
	samp_data->nb_pins = args->nb_pins;
	samp_data->depth = args->depth;
	samp_data->memory = args->memory;
	samp_data->instance_nr = sampler_instance_nr;
	samp_data->comp_id = comp_id;

	retval = init_sampler(samp_data);


	/* Store the new 'sampler' in the samples list */
	list_add_tail(&samp_data->list, &samplers);

	hal_ready(__core_hal_user, comp_id);

	/* All went well -> increment the number of 'sampler' instances */
	sampler_instance_nr++;

	return 0;

fail:
	hal_exit(__core_hal_user, samp_data->comp_id);
	return retval;
}


long sampler_ioctl(struct file *filp, unsigned int cmd, unsigned long arg) {

	int rc = 0, major, minor;
	hal_user_t *hal_user;
	sampler_instance_nr_args_t data;

	major = imajor(filp->f_path.dentry->d_inode);
	minor = iminor(filp->f_path.dentry->d_inode);

	switch (cmd) {

		case SAMPLER_IOCTL_CONNECT:
			/* Pure kernel side init */
			rc = sampler_app_main((sampler_connect_args_t *) arg, minor);
			if (rc) {
				printk("%s: failed to initialize...\n", __func__);
				goto out;
			}
			/* Initialization for this process instance. */
			rc = sampler_user_init((sampler_connect_args_t *) arg, major, minor);
			break;

		case SAMPLER_IOCTL_DISCONNECT:
		
			samp_rg_free(&sampler_queue[(int)arg]);

			hal_user = find_hal_user_by_dev(major, minor);
			BUG_ON(hal_user == NULL);

			hal_exit(hal_user, hal_user->comp_id);

			break;

		case SAMPLER_IOCTL_INSTANCE_NR :
			data.instance_nr = sampler_instance_nr;
			if (copy_to_user((sampler_instance_nr_args_t *)arg, &data, sizeof(sampler_instance_nr_args_t)))
					return -EACCES;
			break;
		
	}
out:
	return rc;
}

ssize_t sampler_read(struct file *filp, char __user *_sample, size_t instance_nr, loff_t *off) {
	return samp_rg_pop(&sampler_queue[(int)instance_nr],(sampler_sample_t *)_sample);
}

int sampler_open(struct inode *inode, struct file *file) {


	return 0;
}

int sampler_release(struct inode *inode, struct file *filp) {


	return 0;
}

struct file_operations sampler_fops = {
		.owner = THIS_MODULE,
		.open = sampler_open,
		.release = sampler_release,
		.unlocked_ioctl = sampler_ioctl,
		.read = sampler_read,
};

int sampler_comp_init(void) {

	int rc;

	printk("OpenCN: sampler subsystem initialization.\n");

	/* Registering device */
	rc = register_chrdev(SAMPLER_DEV_MAJOR, SAMPLER_DEV_NAME, &sampler_fops);

	if (rc < 0) {
		printk("Cannot obtain the major number %d\n", SAMPLER_DEV_MAJOR);
		return rc;
	}

	return 0;
}

late_initcall(sampler_comp_init)
