/********************************************************************
 *  Copyright (C) 2012 Sascha Ittner <sascha.ittner@modusoft.de>
 *  Copyright (C) 2015 Claudio lorini <claudio.lorini@iit.it>
 *  Copyright (C) 2021 Boris Rozanov <boris.rozanov@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

#ifndef _LCEC_EL3702_H_
#define _LCEC_EL3702_H_

/** \brief Vendor ID */
#define LCEC_EL3702_VID LCEC_BECKHOFF_VID

/** \brief Product Code */
#define LCEC_EL3702_PID 0x0e763052

/** \brief Number of channels */
#define LCEC_EL3702_CHANNELS 2

/** \brief Number of samples per channel */
#define LCEC_EL3702_SAMPLES_PER_CHANNEL 10

/** \brief Number of PDOs */
#define LCEC_EL3702_PDOS 22

/* Voltage full scale */
#define LCEC_EL3702_VOLTAGE_FS 10.0

/* Number full scale */
#define LCEC_EL3702_NUMBER_FS 32767

int lcec_el3702_init(int comp_id, struct lcec_slave *slave, ec_pdo_entry_reg_t *pdo_entry_regs);

#endif /* _LCEC_EL3702_H_ */
