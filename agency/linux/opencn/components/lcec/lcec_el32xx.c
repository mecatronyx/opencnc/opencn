/********************************************************************
 *  Copyright (C) 2021 Jean-Pierre Miceli Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

#include "lcec_priv.h"
#include "lcec_el32xx.h"

/* returned value in debug mode */
#define EL3202_DEBUG_TEMP  ((double)22.3)

/* PDO offset between channels */
#define EL3202_CHAN_OFFSET 0x10

/* Slave:          "EL3202"
 * Vendor ID:       0x00000002
 * Product code:    0x0c823052
 * Revision number: 0x00160000
 */

static ec_pdo_entry_info_t el32xx_pdo_entries[] = {
	{0x6000, 0x01, 1}, /* Underrange */
	{0x6000, 0x02, 1}, /* Overrange */
	{0x6000, 0x03, 2}, /* Limit 1 */
	{0x6000, 0x05, 2}, /* Limit 2 */
	{0x6000, 0x07, 1}, /* Error */
	{0x0000, 0x00, 7}, /* Gap */
	{0x1800, 0x07, 1},
	{0x1800, 0x09, 1},
	{0x6000, 0x11, 16}, /* Value */
	{0x6010, 0x01, 1}, /* Underrange */
	{0x6010, 0x02, 1}, /* Overrange */
	{0x6010, 0x03, 2}, /* Limit 1 */
	{0x6010, 0x05, 2}, /* Limit 2 */
	{0x6010, 0x07, 1}, /* Error */
	{0x0000, 0x00, 7}, /* Gap */
	{0x1801, 0x07, 1},
	{0x1801, 0x09, 1},
	{0x6010, 0x11, 16}, /* Value */
};

ec_pdo_info_t el32xx_pdos[] = {
	{0x1a00, 9, el32xx_pdo_entries + 0}, /* RTD TxPDO-Map Ch.1 */
	{0x1a01, 9, el32xx_pdo_entries + 9}, /* RTD TxPDO-Map Ch.2 */
};

ec_sync_info_t el32xx_syncs[] = {
	{0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
	{1, EC_DIR_INPUT,  0, NULL, EC_WD_DISABLE},
	{2, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
	{3, EC_DIR_INPUT,  2, el32xx_pdos + 0, EC_WD_DISABLE},
	{0xff}
};

typedef struct {
	/* pins */
	hal_float_t   *temperature_pin;

	/* PDOs */
	unsigned int value_pdo;
} lcec_el32xx_chan_t;

typedef struct {
	lcec_el32xx_chan_t *chan;
	unsigned chan_nr;
} lcec_el32xx_data_t;

static const lcec_pindesc_t slave_pins[] = {
	{ HAL_FLOAT, HAL_OUT, offsetof(lcec_el32xx_chan_t, temperature_pin),  "%s.%s.%s.chan%d.temperature" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

void lcec_el32xx_read(struct lcec_slave *slave, long period)
{
	lcec_master_t *master = slave->master;
	uint8_t *pd = master->process_data;
	lcec_el32xx_data_t *hal_data = (lcec_el32xx_data_t *)slave->hal_data;
	lcec_el32xx_chan_t *chan;
	uint16_t temp;
	int i;

	/* wait for slave to be operational */
	if ((!slave->state.operational) && (!master->debug)) {
		return;
	}

	for (i = 0; i < hal_data->chan_nr; i++) {
		chan = &hal_data->chan[i];

		if (master->debug) {
			*(chan->temperature_pin) = EL3202_DEBUG_TEMP;
		} else {
			temp = EC_READ_U16(&pd[chan->value_pdo]);
			*(chan->temperature_pin) = (double)temp / 10;
		}
	}
}

int lcec_el32xx_init(int comp_id, struct lcec_slave *slave, ec_pdo_entry_reg_t *pdo_entry_regs, unsigned chan_nr)
{
	lcec_master_t *master = slave->master;
	lcec_el32xx_data_t *hal_data = NULL;
	lcec_el32xx_chan_t *chan = NULL;
	int debug = master->debug;
	int i;
	int err;

	/* initialize callbacks */
	slave->proc_read  = lcec_el32xx_read;

	/* initialize sync info */
	slave->sync_info = el32xx_syncs;

	/* alloc hal memory */
	if ((hal_data = hal_malloc(__core_hal_user, sizeof(lcec_el32xx_data_t))) == NULL) {
		rtapi_print_msg(RTAPI_MSG_ERR, LCEC_MSG_PFX "hal_malloc() for slave %s.%s failed\n", master->name, slave->name);
		return -EIO;
	}
	memset(hal_data, 0, sizeof(lcec_el32xx_data_t));

	hal_data->chan_nr = chan_nr;
	slave->hal_data = hal_data;

	if ((chan = hal_malloc(__core_hal_user, sizeof(lcec_el32xx_chan_t) * chan_nr)) == NULL) {
		rtapi_print_msg(RTAPI_MSG_ERR, LCEC_MSG_PFX "hal_malloc() for slave %s.%s failed\n", master->name, slave->name);
		return -EIO;
	}
	memset(chan, 0, sizeof(lcec_el32xx_chan_t) * chan_nr);
	hal_data->chan = chan;

	for (i = 0; i < chan_nr; i++) {
		chan = &hal_data->chan[i];

		if (!debug) {
			/* initialize PDO entries     position      vend.id     prod.code   index                              sindx offset            bit pos */
			LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x6000 + (i * EL3202_CHAN_OFFSET), 0x11, &chan->value_pdo,     NULL);
		}

		/* export pins */
		if ((err = lcec_pin_newf_list(chan, slave_pins, lcec_module_name, master->name, slave->name, i)) != 0)
			return err;

	}

	return 0;
}


int lcec_el3202_init(int comp_id, struct lcec_slave *slave, ec_pdo_entry_reg_t *pdo_entry_regs)
{
	return lcec_el32xx_init(comp_id, slave, pdo_entry_regs, LCEC_EL3202_CHAN);
}
