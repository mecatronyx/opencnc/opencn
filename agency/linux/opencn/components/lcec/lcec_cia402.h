/********************************************************************
 *  Copyright (C) 2023 Jean-Pierre Miceli Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

#ifndef _LCEC_CIA402_H_
#define _LCEC_CIA402_H_

/* CiA402 drive profile control word bits */
enum {
    CIA402_CW_SWITCH_ON = 1 << 0,      /* M */
    CIA402_CW_ENABLE_VOLTAGE = 1 << 1, /* M */
    CIA402_CW_QUICK_STOP = 1 << 2,     /* O */
    CIA402_CW_ENABLE_OP = 1 << 3,      /* M */
    CIA402_CW_OP_MODE_BIT_4 = 1 << 4,  /* O */
    CIA402_CW_OP_MODE_BIT_5 = 1 << 5,  /* O */
    CIA402_CW_OP_MODE_BIT_6 = 1 << 6,  /* O */
    CIA402_CW_FAULT_RESET = 1 << 7,    /* M */
    CIA402_CW_HALT = 1 << 8,           /* O */
    CIA402_CW_OP_MODE_BIT_9 = 1 << 9,  /* O */
    CIA402_CW_RESERVED = 1 << 10,      /* O */
    CIA402_CW_MAN_SPEC_11 = 1 << 11,   /* O */
    CIA402_CW_MAN_SPEC_12 = 1 << 12,   /* O */
    CIA402_CW_MAN_SPEC_13 = 1 << 13,   /* O */
    CIA402_CW_MAN_SPEC_14 = 1 << 14,   /* O */
    CIA402_CW_MAN_SPEC_15 = 1 << 15,   /* O */
};

/* CiA402 drive profile status word bits */
enum {
    CIA402_SW_READY_TO_SWITCH_ON = 1 << 0,     /* M */
    CIA402_SW_SWITCH_ON = 1 << 1,              /* M */
    CIA402_SW_OP_ENABLED = 1 << 2,             /* M */
    CIA402_SW_FAULT = 1 << 3,                  /* M */
    CIA402_SW_VOLTAGE_ENABLED = 1 << 4,        /* O */
    CIA402_SW_QUICK_STOP = 1 << 5,             /* O */
    CIA402_SW_SWITCH_ON_DISABLED = 1 << 6,     /* M */
    CIA402_SW_WARNING = 1 << 7,                /* O */
    CIA402_SW_MAN_SPEC_8 = 1 << 8,             /* O */
    CIA402_SW_REMOTE = 1 << 9,                 /* O */
    CIA402_SW_OP_MODE_SPEC_10 = 1 << 10,       /* O */
    CIA402_SW_INTERNAL_LIMIT_ACTIVE = 1 << 11, /* O */
    CIA402_SW_OP_MODE_SPEC_12 = 1 << 12,       /* C, M for csp or csv or cst */
    CIA402_SW_OP_MODE_SPEC_13 = 1 << 13,       /* O */
    CIA402_SW_MAN_SPEC_14 = 1 << 14,           /* O */
    CIA402_SW_MAN_SPEC_15 = 1 << 15            /* O */
};

/* CiA402 drive profile modes of operation */
typedef enum {
    CIA402_OP_MODE_NONE = 0,  /* Custom mode for disabling the drive, not in the standard */
    CIA402_OP_MODE_PP = 1,    /* O: Profile position mode */
    CIA402_OP_MODE_VL = 2,    /* O: Velocity mode */
    CIA402_OP_MODE_PV = 3,    /* O: Profile velocity mode */
    CIA402_OP_MODE_TQ = 4,    /* O: Torque profile mode */
    CIA402_OP_MODE_HM = 6,    /* O: Homing mode */
    CIA402_OP_MODE_IP = 7,    /* O: Interpolated position mode */
    CIA402_OP_MODE_CSP = 8,   /* C: Cyclic synchronous position mode  (CSP or CSV */
                              /* or CST is mandatory) */
    CIA402_OP_MODE_CSV = 9,   /* C: Cyclic synchronous velocity mode */
    CIA402_OP_MODE_CST = 10,  /* C: Cyclic synchronous torque mode */
    CIA402_OP_MODE_CSTCA = 11 /* O: Cyclic synchronous torque mode with commutation angle */
} cia402_op_mode_t;


/* EtherCAT FSM state */
enum {
    CIA402_EC_STATE_SWITCH_FAULT = 0,
    CIA402_EC_STATE_SWITCH_ON_DISABLED,
    CIA402_EC_STATE_READY_TO_SWITCH_ON,
    CIA402_EC_STATE_OPERATION_ENABLED,
    CIA402_EC_STATE_QUICK_STOP_ACTIVE,
};


/* Disable voltage - EtherCAT FSM commands */
#define CIA42_ETH_FSM_CMD_DISABLE_VOLTAGE (0)

/* Shutdown - EtherCAT FSM commands */
#define CIA42_ETH_FSM_CMD_SHUTDOWN  (CIA402_CW_ENABLE_VOLTAGE | \
                                     CIA402_CW_QUICK_STOP)

/* Switch on & Enable Operation - EtherCAT FSM commands */
#define CIA42_ETH_FSM_CMD_SWITCH_ON_ENABLE   (CIA402_CW_SWITCH_ON      |  \
                                              CIA402_CW_ENABLE_VOLTAGE |  \
                                              CIA402_CW_QUICK_STOP     |  \
                                              CIA402_CW_ENABLE_OP)

/* Quick-Stop - EtherCAT FSM commands */
#define CIA42_ETH_FSM_CMD_QUICK_STOP  (CIA402_CW_ENABLE_VOLTAGE)


/* HMM mode states */
enum {
    CIA42_OPER_STATE_HMM_INIT = 0,
    CIA42_OPER_STATE_HMM_STARTING,
    CIA42_OPER_STATE_HMM_WAIT_CMD,
    CIA42_OPER_STATE_HMM_ACTIVE,
};

/* PPM mode states */
enum {
    CIA42_OPER_STATE_PPM_INIT = 0,
    CIA42_OPER_STATE_PPM_WAIT_CMD,
    CIA42_OPER_STATE_PPM_START,
    CIA42_OPER_STATE_PPM_WAIT_ACK,
    CIA42_OPER_STATE_PPM_ACTIVE,
};

/* CSP mode states */
enum {
    CIA42_OPER_STATE_CSP_INIT = 0,
    CIA42_OPER_STATE_CSP_WAIT_CMD,
};

enum {
    CIA402_HM_METHOD_ACTUAL_POSITION = 37,
};

#endif /* _LCEC_CIA402_H_ */
