/********************************************************************
 *  Copyright (C) 2022 Jean-Pierre Miceli Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

#ifndef _LCEC_EL32XX_H_
#define _LCEC_EL32XX_H_

#define LCEC_EL3202_VID LCEC_BECKHOFF_VID

#define LCEC_EL3202_PID 0x0c823052

#define LCEC_EL3202_CHAN 2

#define LCEC_EL3202_PDOS (1 * LCEC_EL3202_CHAN)

int lcec_el3202_init(int comp_id, struct lcec_slave *slave, ec_pdo_entry_reg_t *pdo_entry_regs);

int lcec_el32xx_init(int comp_id, struct lcec_slave *slave, ec_pdo_entry_reg_t *pdo_entry_regs, unsigned chan_nr);

#endif /* _LCEC_EL32XX_H_ */