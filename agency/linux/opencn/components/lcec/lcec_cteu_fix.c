/********************************************************************
 *  Copyright (C) 2021 Jean-Pierre Miceli Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

#include "lcec_priv.h"
#include "lcec_cteu_fix.h"

/* Number of IN & OUT PDOs used in the application */
#warning Number of IN & OUT PDOs should come from XML config file (ref issue #146)
#define LCEC_CTEU_IN_PDOS  0
#define LCEC_CTEU_OUT_PDOS 1

/* Data value 'read' in debug mode */
#define LCEC_CTEU_DATA_IN_DEBUG  0x55

/* Slave:          "CTEU-EtherCAT 16Byte"
 * Vendor ID:       0x0000001d
 * Product code:    0x0008bc8c
 * Revision number: 0x00000001
 */

static ec_pdo_entry_info_t cteu_pdo_entries[] = {
    {0x7000, 0x01, 8}, /* Output 1 */
    {0x7000, 0x02, 8}, /* Output 2 */
    {0x7000, 0x03, 8}, /* Output 3 */
    {0x7000, 0x04, 8}, /* Output 4 */
    {0x7000, 0x05, 8}, /* Output 5 */
    {0x7000, 0x06, 8}, /* Output 6 */
    {0x7000, 0x07, 8}, /* Output 7 */
    {0x7000, 0x08, 8}, /* Output 8 */
    {0x7000, 0x09, 8}, /* Output 9 */
    {0x7000, 0x0a, 8}, /* Output 10 */
    {0x7000, 0x0b, 8}, /* Output 11 */
    {0x7000, 0x0c, 8}, /* Output 12 */
    {0x7000, 0x0d, 8}, /* Output 13 */
    {0x7000, 0x0e, 8}, /* Output 14 */
    {0x7000, 0x0f, 8}, /* Output 15 */
    {0x7000, 0x10, 8}, /* Output 16 */
    {0x6000, 0x01, 8}, /* Input 1 */
    {0x6000, 0x02, 8}, /* Input 2 */
    {0x6000, 0x03, 8}, /* Input 3 */
    {0x6000, 0x04, 8}, /* Input 4 */
    {0x6000, 0x05, 8}, /* Input 5 */
    {0x6000, 0x06, 8}, /* Input 6 */
    {0x6000, 0x07, 8}, /* Input 7 */
    {0x6000, 0x08, 8}, /* Input 8 */
    {0x6000, 0x09, 8}, /* Input 9 */
    {0x6000, 0x0a, 8}, /* Input 10 */
    {0x6000, 0x0b, 8}, /* Input 11 */
    {0x6000, 0x0c, 8}, /* Input 12 */
    {0x6000, 0x0d, 8}, /* Input 13 */
    {0x6000, 0x0e, 8}, /* Input 14 */
    {0x6000, 0x0f, 8}, /* Input 15 */
    {0x6000, 0x10, 8}, /* Input 16 */
};

static ec_pdo_info_t cteu_pdos[] = {
    {0x1600, 16, cteu_pdo_entries + 0}, /* RxPDO */
    {0x1a00, 16, cteu_pdo_entries + 16}, /* TxPDO */
};

static ec_sync_info_t cteu_syncs[] = {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 1, cteu_pdos + 0, EC_WD_ENABLE},
    {3, EC_DIR_INPUT, 1, cteu_pdos + 1, EC_WD_DISABLE},
    {0xff}
};

typedef struct {
	hal_bit_t *bit_pin[8];
} lcec_cteu_pin_t;

typedef struct {
	/* PINs */
	lcec_cteu_pin_t in_pins[LCEC_CTEU_IN_PDOS];
	lcec_cteu_pin_t out_pins[LCEC_CTEU_OUT_PDOS];

	/* PDOs */
	unsigned int in_pdo[LCEC_CTEU_PDOS / 2];
	unsigned int out_pdo[LCEC_CTEU_PDOS / 2];
} lcec_cteu_data_t;

static void lcec_cteu_read(struct lcec_slave *slave, long period)
{
	lcec_master_t *master = slave->master;
	uint8_t *pd = master->process_data;
	lcec_cteu_data_t *hal_data = (lcec_cteu_data_t *)slave->hal_data;
	uint8_t data;
	int i, j;

	for (i = 0; i < LCEC_CTEU_IN_PDOS; i++) {

		if (master->debug)
			data = EC_READ_U8(&pd[hal_data->in_pdo[i]]);
		else
			data = LCEC_CTEU_DATA_IN_DEBUG;

		for (j = 0; j < 8; j++)
			*hal_data->in_pins[i].bit_pin[j] = (data & ~(1 << j)) >> j;
	}
}

static void lcec_cteu_write(struct lcec_slave *slave, long period)
{
	lcec_master_t *master = slave->master;
	uint8_t *pd = master->process_data;
	lcec_cteu_data_t *hal_data = (lcec_cteu_data_t *)slave->hal_data;
	uint8_t data,bit;
	int i, j;

	if (master->debug)
		return;

	for (i = 0; i < LCEC_CTEU_OUT_PDOS; i++) {
		data = 0;

		for (j = 0; j < 8; j++) {
			bit = *hal_data->out_pins[i].bit_pin[j];
			data |= (bit << j);
		}
		EC_WRITE_U8(&pd[hal_data->out_pdo[i]], data);
	}
}

int lcec_cteu_init(int comp_id, struct lcec_slave *slave, ec_pdo_entry_reg_t *pdo_entry_regs)
{
	lcec_master_t *master = slave->master;
	lcec_cteu_data_t *hal_data = NULL;
	int debug = master->debug;
	int err;
	int i, j;

	/* initialize callbacks */
	slave->proc_read = lcec_cteu_read;
	slave->proc_write = lcec_cteu_write;

	/* initialize sync info */
	slave->sync_info = cteu_syncs;

	/* alloc hal memory */
	if ((hal_data = hal_malloc(__core_hal_user, sizeof(lcec_cteu_data_t))) == NULL) {
		rtapi_print_msg(RTAPI_MSG_ERR, LCEC_MSG_PFX "hal_malloc() for slave %s.%s failed\n", master->name, slave->name);
		return -EIO;
	}
	memset(hal_data, 0, sizeof(lcec_cteu_data_t));
	slave->hal_data = hal_data;

	for (i = 0; i < (LCEC_CTEU_PDOS / 2); i++) {

		if (!debug) {
			/* initialize PDO entries     position      vend.id     prod.code   index   sindx      offset                 sbit pos */
			LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x6000, 0x01 + i,  &hal_data->in_pdo[i],  NULL);
			LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x7000, 0x01 + i,  &hal_data->out_pdo[i], NULL);
		}
	}

	/* export pins */
	for (i = 0; i < LCEC_CTEU_IN_PDOS; i++) {
		for (j = 0; j < 8; j++) {
			err = hal_pin_bit_newf(__core_hal_user, HAL_OUT, &(hal_data->in_pins[i].bit_pin[j]), comp_id, "%s.%s.%s.in%d.bit-%d", lcec_module_name, master->name, slave->name, i, j);
			if (err)
    			return err;
    	}
	}

	for (i = 0; i < LCEC_CTEU_OUT_PDOS; i++) {
		for (j = 0; j < 8; j++) {
			err = hal_pin_bit_newf(__core_hal_user, HAL_IN, &(hal_data->out_pins[i].bit_pin[j]), comp_id, "%s.%s.%s.out%d.bit-%d", lcec_module_name, master->name, slave->name, i, j);
			if (err)
    			return err;
    	}
	}

	return 0;
}
