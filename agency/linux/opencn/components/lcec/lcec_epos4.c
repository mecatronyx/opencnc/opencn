/********************************************************************
 *  Copyright (C) 2021 Xavier Soltner <xavier.soltner@heig-vd.ch>
 *  Copyright (C) 2023 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * Description: Add support for Maxon EPOS4 drive. It supports 'ppm', 'pvm', 'hmm',
 *              'csp', 'csv' and 'cst' modes
 *
 ********************************************************************/

#include <linux/slab.h>

#include "lcec_priv.h"
#include "lcec_cia402.h"
#include "lcec_epos4.h"

/* Modes macro - bit position */
#define EPOS4_MODE_INACTIVE_POS  0
#define EPOS4_MODE_HM_POS        1
#define EPOS4_MODE_PP_POS        2
#define EPOS4_MODE_PV_POS        3
#define EPOS4_MODE_CSP_POS       4
#define EPOS4_MODE_CSV_POS       5
#define EPOS4_MODE_CST_POS       6

/* Modes macro */
#define EPOS4_MODE_INACTIVE (1 << EPOS4_MODE_INACTIVE_POS)
#define EPOS4_MODE_HM       (1 << EPOS4_MODE_HM_POS)
#define EPOS4_MODE_PP       (1 << EPOS4_MODE_PP_POS)
#define EPOS4_MODE_PV       (1 << EPOS4_MODE_PV_POS)
#define EPOS4_MODE_CSP      (1 << EPOS4_MODE_CSP_POS)
#define EPOS4_MODE_CSV      (1 << EPOS4_MODE_CSV_POS)
#define EPOS4_MODE_CST      (1 << EPOS4_MODE_CST_POS)

/* Master 0, Slave 0, "EPOS4"
 * Vendor ID:       0x000000fb
 * Product code:    0x64500000
 * Revision number: 0x01700000
 */

static ec_pdo_entry_info_t epos4_rx_pdo_entries[] = {
	{0x6040, 0x00, 16}, /* Control word */
	{0x6060, 0x00,  8}, /* Modes of operation */
	{0x6071, 0x00, 16}, /* Target Torque */
	{0x607A, 0x00, 32}, /* Target Position [Increment] */
	{0x60FF, 0x00, 32}, /* Target Velocity */
};

static ec_pdo_entry_info_t epos4_tx_pdo_entries[] = {
	{0x6041, 0x00, 16},  /* Status Word */
	{0x6061, 0x00,  8},  /* Modes of Operation Display */
	{0x6064, 0x00, 32},  /* Position Actual Value [Increment] */
	{0x606C, 0x00, 32},  /* Velocity Actual Value */
	{0x6077, 0x00, 16},  /* Torque Actual Value */
	{0x603F, 0x00, 16},  /* Error Code */
};

static ec_pdo_info_t epos4_pdos[] = {
    {0x1600, 5, epos4_rx_pdo_entries},
    {0x1a00, 6, epos4_tx_pdo_entries},
};

static ec_sync_info_t lcec_epos4_syncs[] = {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT,  0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 1, epos4_pdos + 0, EC_WD_ENABLE},
    {3, EC_DIR_INPUT,  1, epos4_pdos + 1, EC_WD_DISABLE},
    {0xff}
};

typedef struct {
	/* Rx pdo*/
	unsigned int ctrl_word;
	unsigned int modes_oper;
	unsigned int target_torque;
	unsigned int target_position;
	unsigned int target_velocity;

	/*Tx pdo */
	unsigned int status_word;
	unsigned int modes_oper_display;
	unsigned int position_actual_value;
	unsigned int velocity_actual_value;
	unsigned int torque_actual_value;
	unsigned int error_code;
} lcec_epos4_pdo_t;

typedef struct {
	hal_bit_t *set_mode_inactive;
	hal_bit_t *set_mode_hm;
	hal_bit_t *set_mode_ppm;
	hal_bit_t *set_mode_pvm;
	hal_bit_t *set_mode_csp;
	hal_bit_t *set_mode_csv;
	hal_bit_t *set_mode_cst;
	hal_bit_t *quick_stop;

	hal_bit_t *in_mode_inactive;
	hal_bit_t *in_mode_hm;
	hal_bit_t *in_mode_ppm;
	hal_bit_t *in_mode_pvm;
	hal_bit_t *in_mode_csp;
	hal_bit_t *in_mode_csv;
	hal_bit_t *in_mode_cst;

	hal_float_t *target;
	hal_float_t *actual_position;
	hal_float_t *actual_velocity;
	hal_float_t *actual_torque;

	hal_bit_t *hm_start;
	hal_bit_t *hm_stop;
	hal_bit_t *homed;

	hal_bit_t *fault_reset;
	hal_bit_t *in_fault;
	hal_u32_t *error_code;

	hal_bit_t *ppm_start;
	hal_bit_t *ppm_stop;

	hal_bit_t *idle;
} lcec_epos4_pin_t;

typedef struct {
	hal_float_t pos_scale;
	hal_float_t velocity_scale;
	hal_float_t torque_scale;
	hal_float_t modulo;
	hal_bit_t   ppm_immediate_change;
	hal_bit_t   ppm_relative_pos;
	hal_bit_t   ppm_endless_mov;
} lcec_epos4_param_t;


typedef struct {
	lcec_epos4_pdo_t *pdos;
	lcec_epos4_pin_t *pins;
	lcec_epos4_param_t *params;

	/* internal variables */
	uint16_t status_word;
	uint8_t  modes_oper;
	unsigned ethercat_state;
	bool     idle;

} lcec_epos4_data_t;


static const lcec_pindesc_t epso4_pins[] = {
	/* drive mode commands */
	{HAL_BIT, HAL_IN, offsetof(lcec_epos4_pin_t, set_mode_inactive), "%s.%s.%s.set-mode-inactive"},
	{HAL_BIT, HAL_IN, offsetof(lcec_epos4_pin_t, set_mode_hm),  "%s.%s.%s.set-mode-hm"},
	{HAL_BIT, HAL_IN, offsetof(lcec_epos4_pin_t, set_mode_ppm), "%s.%s.%s.set-mode-ppm"},
	{HAL_BIT, HAL_IN, offsetof(lcec_epos4_pin_t, set_mode_pvm), "%s.%s.%s.set-mode-pvm"},
	{HAL_BIT, HAL_IN, offsetof(lcec_epos4_pin_t, set_mode_csp), "%s.%s.%s.set-mode-csp"},
	{HAL_BIT, HAL_IN, offsetof(lcec_epos4_pin_t, set_mode_csv), "%s.%s.%s.set-mode-csv"},
	{HAL_BIT, HAL_IN, offsetof(lcec_epos4_pin_t, set_mode_cst), "%s.%s.%s.set-mode-cst"},
	{HAL_BIT, HAL_IN, offsetof(lcec_epos4_pin_t, quick_stop),   "%s.%s.%s.quick-stop"},

	/* drive mode indicators */
	{HAL_BIT, HAL_OUT, offsetof(lcec_epos4_pin_t, in_mode_inactive), "%s.%s.%s.in-mode-inactive"},
	{HAL_BIT, HAL_OUT, offsetof(lcec_epos4_pin_t, in_mode_hm),  "%s.%s.%s.in-mode-hm"},
	{HAL_BIT, HAL_OUT, offsetof(lcec_epos4_pin_t, in_mode_ppm), "%s.%s.%s.in-mode-ppm"},
	{HAL_BIT, HAL_OUT, offsetof(lcec_epos4_pin_t, in_mode_pvm), "%s.%s.%s.in-mode-pvm"},
	{HAL_BIT, HAL_OUT, offsetof(lcec_epos4_pin_t, in_mode_csp), "%s.%s.%s.in-mode-csp"},
	{HAL_BIT, HAL_OUT, offsetof(lcec_epos4_pin_t, in_mode_csv), "%s.%s.%s.in-mode-csv"},
	{HAL_BIT, HAL_OUT, offsetof(lcec_epos4_pin_t, in_mode_cst), "%s.%s.%s.in-mode-cst"},

	/* Target cmd & actual values */
	{HAL_FLOAT, HAL_IN,  offsetof(lcec_epos4_pin_t, target), "%s.%s.%s.set-target"},
	{HAL_FLOAT, HAL_OUT, offsetof(lcec_epos4_pin_t, actual_position), "%s.%s.%s.actual-position"},
	{HAL_FLOAT, HAL_OUT, offsetof(lcec_epos4_pin_t, actual_velocity), "%s.%s.%s.actual-velocity"},
	{HAL_FLOAT, HAL_OUT, offsetof(lcec_epos4_pin_t, actual_torque),   "%s.%s.%s.actual-torque"},

	{HAL_BIT, HAL_IN,  offsetof(lcec_epos4_pin_t, hm_start), "%s.%s.%s.hm.start"},
	{HAL_BIT, HAL_IN,  offsetof(lcec_epos4_pin_t, hm_stop),  "%s.%s.%s.hm.stop"},
	{HAL_BIT, HAL_OUT, offsetof(lcec_epos4_pin_t, homed),    "%s.%s.%s.homed"},

	{HAL_BIT, HAL_IN,  offsetof(lcec_epos4_pin_t, fault_reset), "%s.%s.%s.fault-reset"},
	{HAL_BIT, HAL_OUT, offsetof(lcec_epos4_pin_t, in_fault),    "%s.%s.%s.in-fault"},
	{HAL_U32, HAL_OUT, offsetof(lcec_epos4_pin_t, error_code),  "%s.%s.%s.error-code"},

	{HAL_BIT, HAL_IN,  offsetof(lcec_epos4_pin_t, ppm_stop),             "%s.%s.%s.ppm.stop"},
	{HAL_BIT, HAL_IN,  offsetof(lcec_epos4_pin_t, ppm_start),            "%s.%s.%s.ppm.start"},

	{HAL_BIT, HAL_IN,  offsetof(lcec_epos4_pin_t, idle),             "%s.%s.%s.idle"},

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static const lcec_pindesc_t epos4_params[] = {
	{ HAL_FLOAT, HAL_RW, offsetof(lcec_epos4_param_t, pos_scale),      "%s.%s.%s.pos-scale" },
	{ HAL_FLOAT, HAL_RW, offsetof(lcec_epos4_param_t, velocity_scale), "%s.%s.%s.velocity-scale" },
	{ HAL_FLOAT, HAL_RW, offsetof(lcec_epos4_param_t, torque_scale),   "%s.%s.%s.torque-scale" },
	{ HAL_FLOAT, HAL_RW, offsetof(lcec_epos4_param_t, modulo),         "%s.%s.%s.modulo"},

	{HAL_BIT, HAL_RW,  offsetof(lcec_epos4_param_t, ppm_endless_mov),      "%s.%s.%s.ppm.endless-movement"},
	{HAL_BIT, HAL_RW,  offsetof(lcec_epos4_param_t, ppm_immediate_change), "%s.%s.%s.ppm.immediate-change"},
	{HAL_BIT, HAL_RW,  offsetof(lcec_epos4_param_t, ppm_relative_pos),     "%s.%s.%s.ppm.relative-pos"},

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

static double lcec_epos4_compute_modulo(double modulo, double target_pos)
{
	int64_t  pos        = (int64_t)target_pos;
	double   rest       = target_pos - (double)pos;
	double   modulo_pos;

	pos        = pos % (int64_t)modulo;
	modulo_pos = (double)pos + rest;

	return modulo_pos;
}

/* Retrieve the EtherCAT FSM state */
static void lcec_epos4_get_eth_fsm_state(lcec_epos4_data_t *data)
{
	uint16_t sw  = data->status_word;

	if (sw & CIA402_SW_FAULT) {
		/* Drive is in fault state */
		data->ethercat_state = CIA402_EC_STATE_SWITCH_FAULT;
	} else if (sw & CIA402_SW_SWITCH_ON_DISABLED) {
		/* Switch on Disabled state --> send 'shutdown' cmd to go to Ready to
		   Switch on */
		data->ethercat_state = CIA402_EC_STATE_SWITCH_ON_DISABLED;

	} else if (!(sw  & CIA402_SW_QUICK_STOP)) {
		/* Quick stop Active state */
		data->ethercat_state = CIA402_EC_STATE_QUICK_STOP_ACTIVE;
	} else {
		if (sw & CIA402_SW_OP_ENABLED) {
			/* Operation Enable --> operate action in the current mode */
			data->ethercat_state = CIA402_EC_STATE_OPERATION_ENABLED;
		} else {
			/* Ready to switch ON --> wait to enable a working mode */
			data->ethercat_state = CIA402_EC_STATE_READY_TO_SWITCH_ON;
		}
	}
}

/* Retrieve the working mode */
static void lcec_epos4_get_working_mode(lcec_epos4_pin_t *pins, uint16_t sw , uint8_t mode)
{
	/* Disable all 'in-mode' PINs */
	*pins->in_mode_inactive = 0;
	*pins->in_mode_hm  = 0;
	*pins->in_mode_ppm = 0;
	*pins->in_mode_pvm = 0;
	*pins->in_mode_csp = 0;
	*pins->in_mode_csv = 0;
	*pins->in_mode_cst = 0;

	/* Set the current working mode */
	if (sw & CIA402_SW_OP_ENABLED) {

		switch (mode) {
			case CIA402_OP_MODE_PP:
				*pins->in_mode_ppm = 1;
				break;
			case CIA402_OP_MODE_PV:
				*pins->in_mode_pvm = 1;
				break;
			case CIA402_OP_MODE_HM:
				*pins->in_mode_hm = 1;
				break;
			case CIA402_OP_MODE_CSP:
				*pins->in_mode_csp = 1;
				break;
			case CIA402_OP_MODE_CSV:
				*pins->in_mode_csv = 1;
				break;
			case CIA402_OP_MODE_CST:
				*pins->in_mode_cst = 1;
				break;
			default:
				/* Error Case not supported */
				printk("[EPOS4] EROOR - mode '%d' not supported\n", mode);
				break;
		}
	} else {
		*pins->in_mode_inactive = 1;
	}
}

/* operation for 'HMM' mode */
static void lcec_epos4_oper_mode_hmm(lcec_epos4_data_t *data, uint8_t *pd, bool init)
{
	lcec_epos4_pdo_t *pdos = data->pdos;
	lcec_epos4_pin_t *pins = data->pins;
	static int state = CIA42_OPER_STATE_PPM_INIT;
	uint16_t cw;

	if (init)
		state = CIA42_OPER_STATE_HMM_INIT;

	switch (state) {
	case CIA42_OPER_STATE_HMM_INIT:
		/* Reset HM commands PINs */
		*pins->hm_start = 0;
		*pins->hm_stop  = 0;

		data->idle = true;

		state = CIA42_OPER_STATE_HMM_WAIT_CMD;
		break;
	case  CIA42_OPER_STATE_HMM_WAIT_CMD:
		if (*pins->hm_start == 1) {
			cw = (CIA42_ETH_FSM_CMD_SWITCH_ON_ENABLE | CIA402_CW_OP_MODE_BIT_4);
			EC_WRITE_U16(&pd[pdos->ctrl_word], cw);

			*pins->hm_start = 0;
			data->idle = false;
			state = CIA42_OPER_STATE_HMM_STARTING;
		}
		break;
	case  CIA42_OPER_STATE_HMM_STARTING:
		/* Check if Homing has started */
		if (!(data->status_word & CIA402_SW_OP_MODE_SPEC_12))
			state = CIA42_OPER_STATE_HMM_ACTIVE;
		break;
	case  CIA42_OPER_STATE_HMM_ACTIVE:
		if (*pins->hm_stop == 1) {
			cw = (CIA42_ETH_FSM_CMD_SWITCH_ON_ENABLE | CIA402_CW_HALT);
			EC_WRITE_U16(&pd[pdos->ctrl_word], cw);
		}

		if (data->status_word & CIA402_SW_OP_MODE_SPEC_12) {
			/* Homing procedure is completed successfully */
			state = CIA42_OPER_STATE_HMM_INIT;
		}
		break;
	}
}

/* operation for 'PPM' mode */
static void lcec_epos4_oper_mode_ppm(lcec_epos4_data_t *data, uint8_t *pd, bool init)
{
	lcec_epos4_pdo_t   *pdos = data->pdos;
	lcec_epos4_pin_t   *pins = data->pins;
	lcec_epos4_param_t *params = data->params;
	int32_t target;
	double pos_modulo;
	static int state = CIA42_OPER_STATE_PPM_INIT;
	uint16_t cw;

	if (init)
		state = CIA42_OPER_STATE_PPM_INIT;

	switch (state) {
	case CIA42_OPER_STATE_PPM_INIT:
		printk("[EPOS4] PPM MODE - INIT\n");
		data->idle = true;
		*pins->ppm_start = 0;
		*pins->ppm_stop = 0;

		state = CIA42_OPER_STATE_PPM_WAIT_CMD;
		break;

	case CIA42_OPER_STATE_PPM_WAIT_CMD:
		data->idle = true;
		if (*pins->ppm_start == 1) {
			printk("[EPOS4] PPM MODE - New target !\n");
			/* Set new target position */
			if (params->modulo != 0) {
				pos_modulo = lcec_epos4_compute_modulo(params->modulo, *pins->target);
				target = (int32_t)(pos_modulo * params->pos_scale);
			} else {
				target = (int32_t)(*pins->target * params->pos_scale);
			}

			EC_WRITE_S32(&pd[pdos->target_position], target);

			*pins->ppm_start = 0;
			data->idle = false;
			state = CIA42_OPER_STATE_PPM_START;
		}
		break;

	case CIA42_OPER_STATE_PPM_START:
		printk("[EPOS4] PPM MODE - Start movement\n");
		cw = (CIA42_ETH_FSM_CMD_SWITCH_ON_ENABLE | CIA402_CW_OP_MODE_BIT_4);

		if (params->ppm_immediate_change)
			cw |= CIA402_CW_OP_MODE_BIT_5;
		if (params->ppm_relative_pos)
			cw |= CIA402_CW_OP_MODE_BIT_6;
		if (params->ppm_endless_mov)
			cw |= CIA402_CW_MAN_SPEC_15;

		EC_WRITE_U16(&pd[pdos->ctrl_word], cw);
		state = CIA42_OPER_STATE_PPM_WAIT_ACK;
		break;

	case CIA42_OPER_STATE_PPM_WAIT_ACK:
		if (data->status_word & CIA402_SW_OP_MODE_SPEC_12) {
			/* Clear the 'new point' flag */
			cw = CIA42_ETH_FSM_CMD_SWITCH_ON_ENABLE;
			EC_WRITE_U16(&pd[pdos->ctrl_word], CIA42_ETH_FSM_CMD_SWITCH_ON_ENABLE);

			state = CIA42_OPER_STATE_PPM_ACTIVE;
		}
		break;

	case CIA42_OPER_STATE_PPM_ACTIVE:

		if (*pins->ppm_stop) {
			EC_WRITE_U16(&pd[pdos->ctrl_word], CIA42_ETH_FSM_CMD_SWITCH_ON_ENABLE |
				                               CIA402_CW_HALT);
			*pins->ppm_stop = 0;
			/* Procedure stopped --> wait next cmd */
			state = CIA42_OPER_STATE_PPM_WAIT_CMD;

		} else if ((params->ppm_immediate_change) && (*pins->ppm_start == 1)) {
			/* Immediate changes + new position --> send position to the drive */
			state = CIA42_OPER_STATE_PPM_WAIT_CMD;

		} else if (data->status_word & CIA402_SW_OP_MODE_SPEC_10) {
			/* Check if Target Position reached or movement halted */
			state = CIA42_OPER_STATE_PPM_WAIT_CMD;
		}
		break;
	}
}

/* operation for 'PVM' mode */
static void lcec_epos4_oper_mode_pvm(lcec_epos4_data_t *data, uint8_t *pd)
{
	lcec_epos4_pdo_t   *pdos = data->pdos;
	lcec_epos4_pin_t   *pins = data->pins;
	lcec_epos4_param_t *params = data->params;

	/* Set target velocity */
	EC_WRITE_S32(&pd[pdos->target_velocity], (int32_t)(*pins->target * params->velocity_scale));

#if 0 /* Disable stop for now - should it be really implemented ? */
	if (*pins->ppv_stop) {
		EC_WRITE_U16(&pd[pdos->ctrl_word], CIA42_ETH_FSM_CMD_SWITCH_ON_ENABLE |
				                               CIA402_CW_HALT);

		*pins->ppv_stop = 0;
	}
#endif

	/* Idle when the speed is 0 */
	data->idle = (data->status_word & CIA402_SW_OP_MODE_SPEC_12);
}

/* operation for 'CSP' mode */
static void lcec_epos4_oper_mode_csp(lcec_epos4_data_t *data, uint8_t *pd, bool init)
{
	lcec_epos4_pdo_t   *pdos = data->pdos;
	lcec_epos4_pin_t   *pins = data->pins;
	lcec_epos4_param_t *params = data->params;
	double pos_modulo;
	int32_t target;

	static int state = CIA42_OPER_STATE_CSP_INIT;

	if (init)
		state = CIA42_OPER_STATE_CSP_INIT;

	switch (state) {
	case CIA42_OPER_STATE_CSP_INIT:
		printk("[EPOS4] CSP MODE - INIT\n");
		*pins->target = *pins->actual_position;
		state = CIA42_OPER_STATE_CSP_WAIT_CMD;
		break;

	case CIA42_OPER_STATE_CSP_WAIT_CMD:
		if (params->modulo != 0) {
			pos_modulo = lcec_epos4_compute_modulo(params->modulo, *pins->target);
			target = (int32_t)(pos_modulo * params->pos_scale);
		} else {
			target = (int32_t)(*pins->target * params->pos_scale);
		}

		EC_WRITE_S32(&pd[pdos->target_position], target);

		break;
	}
}

/* Set the operation mode */
static void lcec_epos4_set_operation_mode(lcec_epos4_data_t *data, uint8_t *pd, unsigned cmd)
{
	lcec_epos4_pdo_t *pdos = data->pdos;
	lcec_epos4_pin_t   *pins = data->pins;
	lcec_epos4_param_t *params = data->params;
	int32_t target;

	if (cmd) {
		switch (cmd) {
			case EPOS4_MODE_INACTIVE:
				EC_WRITE_U16(&pd[pdos->ctrl_word], CIA42_ETH_FSM_CMD_DISABLE_VOLTAGE);
				return;
			case EPOS4_MODE_HM:
				EC_WRITE_U8(&pd[pdos->modes_oper], CIA402_OP_MODE_HM);
				break;
			case EPOS4_MODE_PP:
				EC_WRITE_U8(&pd[pdos->modes_oper], CIA402_OP_MODE_PP);
				break;
			case EPOS4_MODE_PV:
				EC_WRITE_U8(&pd[pdos->modes_oper], CIA402_OP_MODE_PV);
				break;
			case EPOS4_MODE_CSP:
				/* Update the target position PDO/register with actual position */
				target = (int32_t)(*pins->actual_position * params->pos_scale);
				EC_WRITE_S32(&pd[pdos->target_position], target);

				EC_WRITE_U8(&pd[pdos->modes_oper], CIA402_OP_MODE_CSP);
				break;
			case EPOS4_MODE_CSV:
				/* Reset the target velocity PDO/register */
				EC_WRITE_S32(&pd[pdos->target_velocity], 0);

				EC_WRITE_U8(&pd[pdos->modes_oper], CIA402_OP_MODE_CSV);
				break;
			case EPOS4_MODE_CST:
				/* Reset the target torque PDO/register */
				EC_WRITE_S16(&pd[pdos->target_torque], 0);

				EC_WRITE_U8(&pd[pdos->modes_oper], CIA402_OP_MODE_CST);
				break;
			default:
				printk("[EPOS4] Error - Only one cmd can be set at one time !\n");
				return;
		}
		/* Send 'switch ON & enable' cmd */
		EC_WRITE_U16(&pd[pdos->ctrl_word], CIA42_ETH_FSM_CMD_SWITCH_ON_ENABLE);
	}
}

/* Handle the operation when the EtherCAT FSM is in Enable state */
static void lcec_epos4_oper_enabled_state(lcec_epos4_data_t *data, uint8_t *pd, unsigned cmd, bool init)
{
	lcec_epos4_pdo_t   *pdos = data->pdos;
	lcec_epos4_pin_t   *pins = data->pins;
	lcec_epos4_param_t *params = data->params;

	if (cmd) {
		if (!data->idle) {
			printk("[EPOS4] New mode requested - Drive is not idled, cannot change state\n");
		} else {
			/* Change the operating mode */
			lcec_epos4_set_operation_mode(data, pd, cmd);
		}

	} else {
		/* Execute processing of the current operation mode */
		if (*pins->quick_stop) {
			/* Send 'Quick-Stop' cmd */
			EC_WRITE_U16(&pd[pdos->ctrl_word], CIA42_ETH_FSM_CMD_QUICK_STOP);
		} else {
			switch (data->modes_oper) {
			case CIA402_OP_MODE_PP:
				lcec_epos4_oper_mode_ppm(data, pd, init);
				break;
			case CIA402_OP_MODE_PV:
				lcec_epos4_oper_mode_pvm(data, pd);
				break;
			case CIA402_OP_MODE_HM:
				lcec_epos4_oper_mode_hmm(data, pd, init);
				break;
			case CIA402_OP_MODE_CSP:
				lcec_epos4_oper_mode_csp(data, pd, init);
				break;
			case CIA402_OP_MODE_CSV:
				EC_WRITE_S32(&pd[pdos->target_velocity], (int32_t)(*pins->target * params->velocity_scale));
				break;
			case CIA402_OP_MODE_CST:
				EC_WRITE_S16(&pd[pdos->target_torque], (int16_t)(*pins->target * params->torque_scale));
				break;
			}
		}
	}
}

/* Clear the PINs which should not let active */
static void lcec_epos4_clear_pins(lcec_epos4_pin_t *pins)
{
	*pins->fault_reset = 0;
	*pins->quick_stop  = 0;
}

/* Retrieve the command & reset actions/cmd PINs  */
static unsigned lcec_epos4_get_cmd(lcec_epos4_pin_t *pins)
{
	unsigned cmd;

	cmd = (*pins->set_mode_inactive << EPOS4_MODE_INACTIVE_POS)   |
	       (*pins->set_mode_hm       << EPOS4_MODE_HM_POS)    |
	       (*pins->set_mode_ppm      << EPOS4_MODE_PP_POS)    |
	       (*pins->set_mode_pvm      << EPOS4_MODE_PV_POS)    |
	       (*pins->set_mode_csp      << EPOS4_MODE_CSP_POS)   |
	       (*pins->set_mode_csv      << EPOS4_MODE_CSV_POS)   |
	       (*pins->set_mode_cst      << EPOS4_MODE_CST_POS);

	/* Reset commands */
	*pins->set_mode_inactive = 0;
	*pins->set_mode_hm = 0;
	*pins->set_mode_ppm = 0;
	*pins->set_mode_pvm = 0;
	*pins->set_mode_csp = 0;
	*pins->set_mode_csv = 0;
	*pins->set_mode_cst = 0;

	return cmd;
}

static void lcec_epos4_read(struct lcec_slave *slave, long period)
{
    lcec_master_t *master = slave->master;
	uint8_t *pd = master->process_data;
	lcec_epos4_data_t *data = (lcec_epos4_data_t *)slave->hal_data;
	lcec_epos4_pdo_t *pdos = data->pdos;
	lcec_epos4_pin_t *pins = data->pins;
	lcec_epos4_param_t *params = data->params;
	uint16_t sw;
	uint8_t mode;

	/* wait for slave to be operational */
	if ((!slave->state.operational) && (!master->debug))
		return;

	sw   = EC_READ_U16(&pd[pdos->status_word]);
	mode = EC_READ_U8(&pd[pdos->modes_oper_display]);

	data->status_word = sw;
	data->modes_oper  = mode;

	lcec_epos4_get_eth_fsm_state(data);
	lcec_epos4_get_working_mode(pins, sw, mode);

	if (master->debug) {
		/* 'actual_velocity' and 'actual_torque' not simulated in debug mode */
		*pins->actual_position = *pins->target;
	} else {
		*pins->actual_position = (double)(EC_READ_S32(&pd[pdos->position_actual_value])) / params->pos_scale;
		*pins->actual_velocity = (double)EC_READ_S32(&pd[pdos->velocity_actual_value]) / params->velocity_scale;
		*pins->actual_torque   = (double)EC_READ_S16(&pd[pdos->torque_actual_value]) / params->torque_scale;
		*pins->error_code      = EC_READ_U16(&pd[pdos->error_code]);
	}

	*pins->homed    = sw & CIA402_SW_MAN_SPEC_15;
	*pins->in_fault = sw & CIA402_SW_FAULT;
	*pins->idle     = data->idle;
}


static void lcec_epos4_write(struct lcec_slave *slave, long period)
{
	lcec_master_t *master = slave->master;
	uint8_t *pd = master->process_data;
	lcec_epos4_data_t *data = (lcec_epos4_data_t *)slave->hal_data;
	lcec_epos4_pdo_t *pdos = data->pdos;
	lcec_epos4_pin_t *pins = data->pins;
	uint16_t cw = 0;
	static bool init = false;
	unsigned cmd;

	/* wait for slave to be operational */
	if ((!slave->state.operational) && (!master->debug))
		return;

	cmd = lcec_epos4_get_cmd(pins);

	switch (data->ethercat_state) {
	case CIA402_EC_STATE_SWITCH_FAULT:
		if (*pins->fault_reset == 1)
			EC_WRITE_U16(&pd[pdos->ctrl_word], CIA402_CW_FAULT_RESET);
		break;
	case CIA402_EC_STATE_SWITCH_ON_DISABLED:
		/* EtherCAT in  'Switch on disabled' state --> send 'shutdown' command */
		cw = CIA42_ETH_FSM_CMD_SHUTDOWN;
		EC_WRITE_U16(&pd[pdos->ctrl_word], cw);
		break;
	case CIA402_EC_STATE_READY_TO_SWITCH_ON:
		lcec_epos4_set_operation_mode(data, pd, cmd);
		data->idle = true;
		init = true;
		break;
	case CIA402_EC_STATE_OPERATION_ENABLED:
		lcec_epos4_oper_enabled_state(data, pd, cmd, init);
		init = false;
		break;
	case CIA402_EC_STATE_QUICK_STOP_ACTIVE:
		lcec_epos4_set_operation_mode(data, pd, cmd);
		data->idle = true;
		init = true;
		break;
	}

	lcec_epos4_clear_pins(pins);
}

int lcec_epos4_init(int comp_id, struct lcec_slave *slave, ec_pdo_entry_reg_t *pdo_entry_regs)
{
	lcec_master_t *master = slave->master;
	lcec_epos4_data_t  *hal_data;
	lcec_epos4_pdo_t   *pdos;
	lcec_epos4_pin_t   *pins;
	lcec_epos4_param_t *params;
	int err;
	int debug = master->debug;

	/* initialize callbacks */
	slave->proc_read  = lcec_epos4_read;
	slave->proc_write = lcec_epos4_write;
	slave->sync_info  = lcec_epos4_syncs;

	/* alloc memory */
	if ((pins = hal_malloc(__core_hal_user, sizeof(lcec_epos4_pin_t))) == NULL) {
		rtapi_print_msg(RTAPI_MSG_ERR, LCEC_MSG_PFX "hal_malloc() for slave %s.%s failed\n", master->name, slave->name);
		return -EIO;
	}

	if ((params = hal_malloc(__core_hal_user, sizeof(lcec_epos4_param_t))) == NULL) {
		rtapi_print_msg(RTAPI_MSG_ERR, LCEC_MSG_PFX "hal_malloc() for slave %s.%s failed\n", master->name, slave->name);
		return -EIO;
	}

	pdos = kmalloc(sizeof(lcec_epos4_pdo_t), GFP_KERNEL);
	if (pdos == NULL)
		return -ENOMEM;

	hal_data = kmalloc(sizeof(lcec_epos4_data_t), GFP_KERNEL);
	if (hal_data == NULL)
		return -ENOMEM;
	memset(hal_data, 0, sizeof(lcec_epos4_data_t));

	hal_data->pdos   = pdos;
	hal_data->pins   = pins;
	hal_data->params = params;

	slave->hal_data = hal_data;

    if (!debug) {
        /* initialize PDO entries     position      vend.id     prod.code   index   sindx  offset                   bit pos */
		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x6040, 0x00, &pdos->ctrl_word,       NULL);
		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x6060, 0x00, &pdos->modes_oper,      NULL);
		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x6071, 0x00, &pdos->target_torque,   NULL);
		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x607A, 0x00, &pdos->target_position, NULL);
		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x60FF, 0x00, &pdos->target_velocity, NULL);

		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x6041, 0x00, &pdos->status_word,           NULL);
		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x6061, 0x00, &pdos->modes_oper_display,    NULL);
		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x6064, 0x00, &pdos->position_actual_value, NULL);
		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x606c, 0x00, &pdos->velocity_actual_value, NULL);
		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x6077, 0x00, &pdos->torque_actual_value,   NULL);
		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x603f, 0x00, &pdos->error_code,            NULL);
	}

    /* export pins */
    if ((err = lcec_pin_newf_list(pins, epso4_pins, lcec_module_name, master->name, slave->name)) != 0)
        return err;

    /* export params */
	if ((err = lcec_param_newf_list(params, epos4_params, lcec_module_name, master->name, slave->name)) != 0)
		return err;

	/* Variables initialization */
	hal_data->idle = true;
	params->pos_scale      = 1.0;
	params->velocity_scale = 1.0;
	params->torque_scale   = 1.0;
	params->modulo         = 0.0;

	return 0;
}
