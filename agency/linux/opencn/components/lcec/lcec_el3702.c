/********************************************************************
	*	Copyright (C) 2012 Sascha Ittner <sascha.ittner@modusoft.de>
	*	Copyright (C) 2015 Claudio lorini <claudio.lorini@iit.it>
	*	Copyright (C) 2021 Boris Rozanov <boris.rozanov@heig-vd.ch>
	*	Copyright (C) 2021 Xavier Soltner <xavier.soltner@heig-vd.ch>
	*
	*	This program is free software; you can redistribute it and/or modify
	*	it under the terms of the GNU General Public License as published by
	*	the Free Software Foundation; either version 2 of the License, or
	*	(at your option) any later version.
	*
	*	This program is distributed in the hope that it will be useful,
	*	but WITHOUT ANY WARRANTY; without even the implied warranty of
	*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	*	GNU General Public License for more details.
	*
	*	You should have received a copy of the GNU General Public License
	*	along with this program; if not, write to the Free Software
	*	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
	********************************************************************/

/*	Linuxcnc and Machinekit HAL driver for Beckhoff EL3702
	2-channel analog input terminal 16 bits with oversampling

		\details This terminal is used to sample analog voltage in the range of +/- 10[V].
		It stores values in 16 bits integer signed format.
		The values are acquired and stored internally in a slave buffer at the rate that can be higher than
		the EtherCAT frame rate, i.e. oversampling.
		There is 1 buffer per channel, and 2 channels per slave.
		The oversampling ratio is parametrized using the <dcConf> tag and Sync0/Sync1 entries in the EtherCAT configuration xml file.
		See further explanation and example of xml file in OpenCN documentation.
		Link to all OpenCN resources is provided below:
		https://opencn.heig-vd.ch/

		\note OS (OS=OverSampling) ratio is set to 10[-] using the method described above.
		In order to change the OS factor, one has to change the xml file.
		For the moment, one also has to adapt the corresponding c code, i. e. lcec_el3702.c and lcec_el3702.h

		Link to Beckhoff doc:
		https://www.beckhoff.com/en-en/products/i-o/ethercat-terminals/el3xxx-analog-input/el3702.html
*/

#include <opencn/uapi/lcec.h>

#include "lcec_priv.h"
#include "lcec_el3702.h"


/*	Master 0, Slave 1, "EL3702"
	*	Vendor ID:			0x00000002
	*	Product code:		0x0e763052
	*	Revision number:	0x00030000
*/

/*
SM0: PhysAddr 0x1000, ControlByte 0x20, Enable 1
	TxPDO 0x1b00 "Ch1 CycleCount"
		PDO entry 0x6800:01,	16 bit,	"Output"
	TxPDO 0x1a00 "Ch1 Sample 0"
		PDO entry 0x6000:01,	16 bit,	"Input"
	TxPDO 0x1a01 "Ch1 Sample 1"
		PDO entry 0x6010:01,	16 bit,	"Input"
	TxPDO 0x1a02 "Ch1 Sample 2"
		PDO entry 0x6020:01,	16 bit,	"Input"
	TxPDO 0x1a03 "Ch1 Sample 3"
		PDO entry 0x6030:01,	16 bit,	"Input"
	TxPDO 0x1a04 "Ch1 Sample 4"
		PDO entry 0x6040:01,	16 bit,	"Input"
	TxPDO 0x1a05 "Ch1 Sample 5"
		PDO entry 0x6050:01,	16 bit,	"Input"
	TxPDO 0x1a06 "Ch1 Sample 6"
		PDO entry 0x6060:01,	16 bit,	"Input"
	TxPDO 0x1a07 "Ch1 Sample 7"
		PDO entry 0x6070:01,	16 bit,	"Input"
	TxPDO 0x1a08 "Ch1 Sample 8"
		PDO entry 0x6080:01,	16 bit,	"Input"
	TxPDO 0x1a09 "Ch1 Sample 9"
		PDO entry 0x6090:01,	16 bit,	"Input"

SM1: PhysAddr 0x1400, ControlByte 0x20, Enable 1
	TxPDO 0x1b00 "Ch2 CycleCount"
		PDO entry 0x6800:02,	16 bit,	"Output"
	TxPDO 0x1a80 "Ch2 Sample 0"
		PDO entry 0x6000:02,	16 bit,	"Input"
	TxPDO 0x1a81 "Ch2 Sample 1"
		PDO entry 0x6010:02,	16 bit,	"Input"
	TxPDO 0x1a82 "Ch2 Sample 2"
		PDO entry 0x6020:02,	16 bit,	"Input"
	TxPDO 0x1a83 "Ch2 Sample 3"
		PDO entry 0x6030:02,	16 bit,	"Input"
	TxPDO 0x1a84 "Ch2 Sample 4"
		PDO entry 0x6040:02,	16 bit,	"Input"
	TxPDO 0x1a85 "Ch2 Sample 5"
		PDO entry 0x6050:02,	16 bit,	"Input"
	TxPDO 0x1a86 "Ch2 Sample 6"
		PDO entry 0x6060:02,	16 bit,	"Input"
	TxPDO 0x1a87 "Ch2 Sample 7"
		PDO entry 0x6070:02,	16 bit,	"Input"
	TxPDO 0x1a88 "Ch2 Sample 8"
		PDO entry 0x6080:02,	16 bit,	"Input"
	TxPDO 0x1a89 "Ch2 Sample 9"
		PDO entry 0x6090:02,	16 bit,	"Input"

SM2: PhysAddr 0x0998, Virtual
	TxPDO 0x1b10 "NextSync1Time"
		PDO entry 0x1d09:98,	32 bit,	"Output"
*/

ec_pdo_entry_info_t slave_el3702_pdo_entries[] = {
	{0x6800, 0x01, 16}, /* Ch1 CycleCount, one per channel, incremented with each record. Record = 10+1 values*/
	{0x6000, 0x01, 16}, /* Ch1 Value */
	{0x6010, 0x01, 16}, /* Ch1 Value */
	{0x6020, 0x01, 16}, /* Ch1 Value */
	{0x6030, 0x01, 16}, /* Ch1 Value */
	{0x6040, 0x01, 16}, /* Ch1 Value */
	{0x6050, 0x01, 16}, /* Ch1 Value */
	{0x6060, 0x01, 16}, /* Ch1 Value */
	{0x6070, 0x01, 16}, /* Ch1 Value */
	{0x6080, 0x01, 16}, /* Ch1 Value */
	{0x6090, 0x01, 16}, /* Ch1 Value */
	{0x6800, 0x02, 16}, /* Ch2 CycleCount */
	{0x6000, 0x02, 16}, /* Ch2 Value */
	{0x6010, 0x02, 16}, /* Ch2 Value */
	{0x6020, 0x02, 16}, /* Ch2 Value */
	{0x6030, 0x02, 16}, /* Ch2 Value */
	{0x6040, 0x02, 16}, /* Ch2 Value */
	{0x6050, 0x02, 16}, /* Ch2 Value */
	{0x6060, 0x02, 16}, /* Ch2 Value */
	{0x6070, 0x02, 16}, /* Ch2 Value */
	{0x6080, 0x02, 16}, /* Ch2 Value */
	{0x6090, 0x02, 16}, /* Ch2 Value */
};

ec_pdo_info_t slave_el3702_pdos[] = {
	{0x1b00, 1, slave_el3702_pdo_entries + 0}, /* Ch1 CycleCount */
	{0x1a00, 1, slave_el3702_pdo_entries + 1}, /* Ch1 Sample */
	{0x1a01, 1, slave_el3702_pdo_entries + 2}, /* Ch1 Sample */
	{0x1a02, 1, slave_el3702_pdo_entries + 3}, /* Ch1 Sample */
	{0x1a03, 1, slave_el3702_pdo_entries + 4}, /* Ch1 Sample */
	{0x1a04, 1, slave_el3702_pdo_entries + 5}, /* Ch1 Sample */
	{0x1a05, 1, slave_el3702_pdo_entries + 6}, /* Ch1 Sample */
	{0x1a06, 1, slave_el3702_pdo_entries + 7}, /* Ch1 Sample */
	{0x1a07, 1, slave_el3702_pdo_entries + 8}, /* Ch1 Sample */
	{0x1a08, 1, slave_el3702_pdo_entries + 9}, /* Ch1 Sample */
	{0x1a09, 1, slave_el3702_pdo_entries + 10}, /* Ch1 Sample */
	{0x1b01, 1, slave_el3702_pdo_entries + 11}, /* Ch2 CycleCount */
	{0x1a80, 1, slave_el3702_pdo_entries + 12}, /* Ch2 Sample */
	{0x1a81, 1, slave_el3702_pdo_entries + 13}, /* Ch2 Sample */
	{0x1a82, 1, slave_el3702_pdo_entries + 14}, /* Ch2 Sample */
	{0x1a83, 1, slave_el3702_pdo_entries + 15}, /* Ch2 Sample */
	{0x1a84, 1, slave_el3702_pdo_entries + 16}, /* Ch2 Sample */
	{0x1a85, 1, slave_el3702_pdo_entries + 17}, /* Ch2 Sample */
	{0x1a86, 1, slave_el3702_pdo_entries + 18}, /* Ch2 Sample */
	{0x1a87, 1, slave_el3702_pdo_entries + 19}, /* Ch2 Sample */
	{0x1a88, 1, slave_el3702_pdo_entries + 20}, /* Ch2 Sample */
	{0x1a89, 1, slave_el3702_pdo_entries + 21}, /* Ch2 Sample */
};

ec_sync_info_t slave_el3702_syncs[] = {
	{0, EC_DIR_INPUT, LCEC_EL3702_SAMPLES_PER_CHANNEL + 1, slave_el3702_pdos + 0, EC_WD_DISABLE},
	{1, EC_DIR_INPUT, LCEC_EL3702_SAMPLES_PER_CHANNEL + 1, slave_el3702_pdos + 11, EC_WD_DISABLE},
	{0xff}
};

/* data structure of one channel of the device */
typedef struct {
	
	/* data exposed as PIN */
	hal_u32_t *cycle_count;
	hal_float_t *in[LCEC_EL3702_SAMPLES_PER_CHANNEL];
	
	/* offsets to access data in EC PDOs */
	unsigned int cycle_count_offset;
	unsigned int in_offset[LCEC_EL3702_SAMPLES_PER_CHANNEL];

	/*Input pin to simulate signal aquisiton in debug mode */
	hal_float_t *in_simu[LCEC_EL3702_SAMPLES_PER_CHANNEL];

} lcec_el3702_chan_data_t;

/* complete data structure for EL3702 */
typedef struct {

	lcec_el3702_chan_data_t channel[LCEC_EL3702_CHANNELS];

} lcec_el3702_data_t;

/* In PINs names, "ain" stands for analog inputs */
/* The last %d is replaced with the channel number (0 or 1) */
static const lcec_pindesc_t slave_channel_pins[] = {
	{ HAL_U32, HAL_OUT, offsetof(lcec_el3702_chan_data_t, cycle_count), "%s.%s.%s.ain-%d-CycleCount" },
	{ HAL_FLOAT, HAL_OUT, offsetof(lcec_el3702_chan_data_t, in[0]), "%s.%s.%s.ain-%d-0-Volt" },
	{ HAL_FLOAT, HAL_OUT, offsetof(lcec_el3702_chan_data_t, in[1]), "%s.%s.%s.ain-%d-1-Volt" },
	{ HAL_FLOAT, HAL_OUT, offsetof(lcec_el3702_chan_data_t, in[2]), "%s.%s.%s.ain-%d-2-Volt" },
	{ HAL_FLOAT, HAL_OUT, offsetof(lcec_el3702_chan_data_t, in[3]), "%s.%s.%s.ain-%d-3-Volt" },
	{ HAL_FLOAT, HAL_OUT, offsetof(lcec_el3702_chan_data_t, in[4]), "%s.%s.%s.ain-%d-4-Volt" },
	{ HAL_FLOAT, HAL_OUT, offsetof(lcec_el3702_chan_data_t, in[5]), "%s.%s.%s.ain-%d-5-Volt" },
	{ HAL_FLOAT, HAL_OUT, offsetof(lcec_el3702_chan_data_t, in[6]), "%s.%s.%s.ain-%d-6-Volt" },
	{ HAL_FLOAT, HAL_OUT, offsetof(lcec_el3702_chan_data_t, in[7]), "%s.%s.%s.ain-%d-7-Volt" },
	{ HAL_FLOAT, HAL_OUT, offsetof(lcec_el3702_chan_data_t, in[8]), "%s.%s.%s.ain-%d-8-Volt" },
	{ HAL_FLOAT, HAL_OUT, offsetof(lcec_el3702_chan_data_t, in[9]), "%s.%s.%s.ain-%d-9-Volt" },
	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL },
};

static const lcec_pindesc_t slave_channel_pins_input_simulation[] = {
	{ HAL_FLOAT, HAL_IN, offsetof(lcec_el3702_chan_data_t, in_simu[0]), "%s.%s.%s.ain-%d-0-Volt-simu-input" },
	{ HAL_FLOAT, HAL_IN, offsetof(lcec_el3702_chan_data_t, in_simu[1]), "%s.%s.%s.ain-%d-1-Volt-simu-input" },
	{ HAL_FLOAT, HAL_IN, offsetof(lcec_el3702_chan_data_t, in_simu[2]), "%s.%s.%s.ain-%d-2-Volt-simu-input" },
	{ HAL_FLOAT, HAL_IN, offsetof(lcec_el3702_chan_data_t, in_simu[3]), "%s.%s.%s.ain-%d-3-Volt-simu-input" },
	{ HAL_FLOAT, HAL_IN, offsetof(lcec_el3702_chan_data_t, in_simu[4]), "%s.%s.%s.ain-%d-4-Volt-simu-input" },
	{ HAL_FLOAT, HAL_IN, offsetof(lcec_el3702_chan_data_t, in_simu[5]), "%s.%s.%s.ain-%d-5-Volt-simu-input" },
	{ HAL_FLOAT, HAL_IN, offsetof(lcec_el3702_chan_data_t, in_simu[6]), "%s.%s.%s.ain-%d-6-Volt-simu-input" },
	{ HAL_FLOAT, HAL_IN, offsetof(lcec_el3702_chan_data_t, in_simu[7]), "%s.%s.%s.ain-%d-7-Volt-simu-input" },
	{ HAL_FLOAT, HAL_IN, offsetof(lcec_el3702_chan_data_t, in_simu[8]), "%s.%s.%s.ain-%d-8-Volt-simu-input" },
	{ HAL_FLOAT, HAL_IN, offsetof(lcec_el3702_chan_data_t, in_simu[9]), "%s.%s.%s.ain-%d-9-Volt-simu-input" },
	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL },
};

/*	Callback executed by lcec main thread every EtherCAT period.
	Analog values are read from slave channels
	and are written to PINs
*/
void lcec_el3702_read(struct lcec_slave *slave, long period)
{
	lcec_master_t *master = slave->master;
	uint8_t *pd = master->process_data;
	lcec_el3702_data_t *hal_data = (lcec_el3702_data_t *) slave->hal_data;
	int i, j;
	lcec_el3702_chan_data_t *chan;
	hal_float_t v;
	int16_t n;
	int debug;

	/* get the DEBUG mode flag value */
	debug = master->debug;

	/* read all samples from each channel and write data to PINs */
	for (i =0 ; i < LCEC_EL3702_CHANNELS; i++) {

		chan = &hal_data->channel[i];

		if (!debug) {
			/* read CycleCount */
			*(chan->cycle_count) = (hal_u32_t)EC_READ_U16(&pd[chan->cycle_count_offset]);
		}

		/* read samples from one channel */
		for (j = 0; j < LCEC_EL3702_SAMPLES_PER_CHANNEL; j++) {
			/* read real inputs, or read dedicated PINs to simulate signal acquisition, when in debug mode*/
			if(!debug) {
				/* read one sample */
				n = EC_READ_S16(&pd[chan->in_offset[j]]);
				/* scaling number signed 16 bits -> volts */
				v = (hal_float_t)((LCEC_EL3702_VOLTAGE_FS/LCEC_EL3702_NUMBER_FS) * n);
			} else {
				v = *(chan->in_simu[j]);
			}
			/* write analog value in [V] to input PIN */
			*(chan->in[j]) = v;
		}
	}
}

/*	Initialization of slave read callback, slave hal data (lcec pdo structure, sync info),
	export of all HAL PINs for this slave */
int lcec_el3702_init(int comp_id, struct lcec_slave *slave, ec_pdo_entry_reg_t *pdo_entry_regs)
{
	lcec_master_t *master = slave->master;
	lcec_el3702_data_t *hal_data;
	int i, j;
	lcec_el3702_chan_data_t *chan;
	int err;
	int debug;

	/* get the DEBUG mode flag value */
	debug = master->debug;

	RTAPI_PRINT_MSG(RTAPI_MSG_DBG, "== lcec_el3702_init called================================\n");

	/* initialize callbacks */
	slave->proc_read = lcec_el3702_read;

	/* alloc hal memory */
	if ((hal_data = hal_malloc(__core_hal_user, sizeof(lcec_el3702_data_t))) == NULL) {
		rtapi_print_msg(RTAPI_MSG_ERR, LCEC_MSG_PFX "hal_malloc() for slave %s.%s failed\n", master->name, slave->name);
		return -EIO;
	}
	memset(hal_data, 0, sizeof(lcec_el3702_data_t));
	slave->hal_data = hal_data;

	/* initialize sync info */
	slave->sync_info = slave_el3702_syncs;


	for (i = 0; i < LCEC_EL3702_CHANNELS; i++) {

		chan = &hal_data->channel[i];

		/* export all output pins for one channel  */
		if ((err = lcec_pin_newf_list(chan, slave_channel_pins, lcec_module_name, master->name, slave->name, i)) != 0) {
			return err;
		}

		if(!debug) {

			/*	Initialize vid, pid, index and subindex entries in lcec pdo structure to be able to read from slave PDOs
				Initialize offsets in data structures for one channel */
			LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x6800, 0x01 + i, &chan->cycle_count_offset, NULL);

			for (j = 0; j < LCEC_EL3702_SAMPLES_PER_CHANNEL; j++) {				
					LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x6000 + (j << 4), 0x01 + i, &chan->in_offset[j], NULL);
			}

		} else {
			/* Export dedicated PINs for debug so we can simulate signal acquisition */
			if ((err = lcec_pin_newf_list(chan, slave_channel_pins_input_simulation, lcec_module_name, master->name, slave->name, i)) != 0) {
				return err;
			}
		}
	}

	return 0;
}
