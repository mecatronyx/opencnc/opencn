/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: mux.c
 *
 *
 * It is the kernel part of the "mux" OpenCN HAL component.
 *
 ********************************************************************/

#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>

#include <opencn/hal/hal.h>
#include <opencn/uapi/mux.h>

typedef union {
    hal_bit_t *b;
    hal_float_t *f;
    hal_u32_t *u;
    hal_s32_t *s;
} mux_pin_u;


typedef struct {
	struct list_head list;

	/* Mux selector PIN */
	hal_u32_t *selector_pin;

	/* Input / Outputs PINs vectors */
	mux_pin_u *pins_in;
	mux_pin_u *pins_out;

	/* Component ID */
	int comp_id;

	/* args */
	int vector_size;
	int vector_nr;
	int type;
	char name[HAL_NAME_LEN];

	/* Flags to enable / disable msg in RT function */
	bool log_error;

} mux_data_t;

/* Number of Muxes instances */
static int muxes_nr = 0;

LIST_HEAD(muxes);


/************************************************************************
 *                       REAL-TIME HAL FUNCTION                         *
 ************************************************************************/

static void mux_func(void *args, long period)
{
	mux_data_t *mux_data =  (mux_data_t *)args;
	int sel = *mux_data->selector_pin;
	int pos;
	int i;

	/* Check selector value */
	if (sel >= mux_data->vector_nr) {
		if (mux_data->log_error)
			printk("[%s] ERROR - selector value '%d' is not a valid (valid: 0 to %d)\n",
				mux_data->name, sel, mux_data->vector_nr - 1);

		 mux_data->log_error = false;

		return;
	}

	/* No error on 'sel' --> activate logs in case of error in next loop */
	mux_data->log_error = true;

	for (i = 0; i < mux_data->vector_size; i++) {
		pos = (sel * mux_data->vector_size) + i;
		switch(mux_data->type) {
		case HAL_BIT:
			*(mux_data->pins_out[i].b) = *(mux_data->pins_in[pos].b);
			break;
		case HAL_FLOAT:
			*(mux_data->pins_out[i].f) = *(mux_data->pins_in[pos].f);
			break;
		case HAL_S32:
			*(mux_data->pins_out[i].s) = *(mux_data->pins_in[pos].s);
			break;
		case HAL_U32:
			*(mux_data->pins_out[i].u) = *(mux_data->pins_in[pos].u);
			break;
		default:
			/* Nothing to do - 'type' value has been tested during initialization */
			break;
		}
	}
}


/************************************************************************
 *                       INIT AND EXIT CODE                             *
 ************************************************************************/

static int mux_create_pins(int type, hal_pin_dir_t dir, char *name, mux_pin_u *pin, int comp_id)
{
	int ret;

	switch(type) {
	case HAL_BIT:
		ret = hal_pin_bit_new(__core_hal_user, name, dir, &pin->b, comp_id);
		break;
	case HAL_FLOAT:
		ret = hal_pin_float_new(__core_hal_user, name, dir, &pin->f, comp_id);
		break;
	case HAL_S32:
		ret = hal_pin_s32_new(__core_hal_user, name, dir, &pin->s, comp_id);
		break;
	case HAL_U32:
		ret = hal_pin_u32_new(__core_hal_user, name, dir, &pin->u, comp_id);
		break;
	default:
		/* Nothing to do - 'type' value has been tested in user-space */
		break;
	}

	return ret;
}

static int mux_init(mux_data_t *mux_data, char *comp_name)
{
	char name[HAL_NAME_LEN];
	int i, j;
	int pos;

	/* Creation of the inputs vectors */
	for (i = 0; i < mux_data->vector_nr ; i++) {
		for (j = 0; j < mux_data->vector_size; j++) {
			sprintf(name, "%s.vector%d.input%d", comp_name, i, j);

			pos = (i * (mux_data->vector_size)) + j;
			mux_create_pins(mux_data->type, HAL_IN, name, &(mux_data->pins_in[pos]), mux_data->comp_id);
		}
	}

	/* Creation of the output vector */
	for (j = 0; j < mux_data->vector_size; j++) {
		sprintf(name, "%s.output%d", comp_name, j);
		mux_create_pins(mux_data->type, HAL_OUT, name, &(mux_data->pins_out[j]), mux_data->comp_id);
	}

	/* Creation of global PINs */
	sprintf(name, "%s.selector", comp_name);
	hal_pin_u32_new(__core_hal_user, name, HAL_IN, &mux_data->selector_pin, mux_data->comp_id);

	/* Export function */
	sprintf(name, "%s_func", comp_name);
	hal_export_funct(__core_hal_user, name, mux_func, mux_data, 1, 0, mux_data->comp_id);

	return 0;
}


static int mux_app_main(mux_connect_args_t *args)
{
	int ret = 0;
	char comp_name[HAL_NAME_LEN];
	mux_pin_u *pins_in;
	mux_pin_u *pins_out;
	int comp_id;
	mux_data_t *mux;

	/* Build Mux name  */
	if (! strcmp(args->name, MUX_DEFAULT_NAME))
		sprintf(comp_name, "%s.%d", MUX_COMP_NAME, muxes_nr);
	else
		sprintf(comp_name, "%s.%s", MUX_COMP_NAME, args->name);

	comp_id = hal_init(__core_hal_user, comp_name);
	if (comp_id < 0) {
		rtapi_print_msg(RTAPI_MSG_ERR, "[%s] Error: hal_init() failed\n", comp_name);
		return -EINVAL;
	}

	pins_in  = hal_malloc(__core_hal_user, sizeof(mux_pin_u) * args->vector_size * args->vector_nr);
	if (!pins_in) {
		printk("[%s] Error: Inputs PINs allocation failed\n", comp_name);
		ret = -EIO;
		goto fail;
	}

	pins_out = hal_malloc(__core_hal_user, sizeof(mux_pin_u) * args->vector_size);
	if (!pins_out) {
		printk("[%s] Error: Output PINs allocation failed\n", comp_name);
		ret = -EIO;
		goto fail;
	}

	mux = hal_malloc(__core_hal_user, sizeof(mux_data_t));
	if (!mux) {
		printk("[%s] Error: 'mux' allocation failed\n", comp_name);
		ret = -EIO;
		goto fail;
	}

	/* Input / Outputs PINs vector */
	mux->pins_in      = pins_in;
	mux->pins_out     = pins_out;
	mux->comp_id      = comp_id;
	mux->vector_size  = args->vector_size;
	mux->vector_nr    = args->vector_nr;
	mux->type         = args->type;
	mux->log_error    = true;
	strcpy(mux->name, comp_name);

	/* create pins pairs */
	ret = mux_init(mux, comp_name);
	if (ret != 0) {
		goto fail;
	}

	/* Store the new 'mux' in the muxes list */
	list_add_tail(&mux->list, &muxes);

	hal_ready(__core_hal_user, comp_id);

	/* All went well -> increment the number of 'mux' instances */
	muxes_nr++;

	return 0;

fail:
	hal_exit(__core_hal_user, comp_id);

	return ret;
}

/************************************************************************
 *                Char Device & file operation definitions              *
 ************************************************************************/

static int mux_open(struct inode *inode, struct file *file)
{
	return 0;
}

static int mux_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static long mux_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int rc = 0;
	mux_instance_nr_args_t data;

	switch (cmd) {
	case MUX_IOCTL_CONNECT:

		rc = mux_app_main((mux_connect_args_t *)arg);
		if (rc)
			printk("%s: failed to initialize...\n", __func__);

		break;

	case MUX_IOCTL_DISCONNECT:
		break;

	case MUX_IOCTL_INSTANCE_NR:
		data.instance_nr = muxes_nr;

 		if (copy_to_user((mux_instance_nr_args_t *)arg, &data, sizeof(mux_instance_nr_args_t)))
                return -EACCES;
		break;
	}

	return rc;
}

struct file_operations mux_fops = {
	.owner = THIS_MODULE,
	.open = mux_open,
	.release = mux_release,
	.unlocked_ioctl = mux_ioctl,
};

int mux_comp_init(void)
{
	printk("OpenCN: mux subsystem initialization.\n");

	hal_create_interface(MUX_COMP_NAME, &mux_fops);

	return 0;
}

late_initcall(mux_comp_init)
