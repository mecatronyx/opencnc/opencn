
#include <linux/types.h>

#include <asm/io.h>

#define virt_to_pfn(kaddr) (virt_to_phys((volatile void *) kaddr) >> PAGE_SHIFT)
#define pfn_to_virt(pfn) (phys_to_virt((phys_addr_t) (pfn << PAGE_SHIFT)))
    
