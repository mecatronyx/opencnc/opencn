***********************************************************
*                   General                             *
***********************************************************

This folder contain multiple file to test the spindle component.

The following script are to test the different functionnality : 
    - velocity.sh
    - atc.sh
    - cooling.sh
    - temperature.sh
It is adviced to directly use the test_spindle.sh script.

***********************************************************
*                   Full testing                             *
***********************************************************
To test every spindle combination, go to
cd /etc/opencn/validation/components/spindle/
Then load config : 
halcmd -f /etc/opencn/validation/components/spindle/spindle_test_8.hal


and one by one :
./test_spindle.sh 0 --std
./test_spindle.sh 1 --std --cooling
./test_spindle.sh 2 --std --temperature
./test_spindle.sh 3 --std --temperature --cooling
./test_spindle.sh 4 --atc
./test_spindle.sh 5 --atc --cooling
./test_spindle.sh 6 --atc --temperature
./test_spindle.sh 7 --atc --temperature --cooling

***********************************************************
*                   Other file                             *
***********************************************************
Load spindle_test_1.hal to test a single spindle instance in simulation.
Load spindle_test_8.hal to test a multiple spindle instance in simulation.
Load spindle_test_m5.hal to test the spindle on the M5 at HEIG-VD.

