#!/bin/bash

#################################################################################
# This file is part of a validation folder for the openCN spindle components    #
# This file test the behavior for the velocity option                           #
#                                                                               #
# Author: Xavier Soltner                                                        #
#                                                                               #
# Version: 1.0                                                                  #
# Date: 2024-11-14                                                              #
#                                                                               #
# This script DO test :                                                         #
#   - Final "target-velocity"                                                   #
#                                                                               #
# This scipt does NOT test :                                                    #
#   - Transition (eg. jump of velocity that may result in spindle error)        #
#   - fault state                                                               #
#                                                                               #
#################################################################################


function test_velocity()
{
    local name="$1" #name of the spindle
    local cur #current pin value
    local cur2 #current pin value
    local cur3 #current pin value

    echo "[spindle.$name][veloctiy] - Start Veloctiy test"

    # Velocity param
    halcmd setp spindle.$name.regulation.accel      10000
    halcmd setp spindle.$name.regulation.type       1
    halcmd setp spindle.$name.velocity.min          15000
    halcmd setp spindle.$name.velocity.max          60000
    halcmd setp spindle.$name.velocity.tolerance    1

    # Testing nominal
    halcmd setp spindle.$name.cmd-velocity 0
    halcmd setp spindle.$name.cmd-override 0
    halcmd setp spindle.$name.cmd-enable 1
    sleep 0.1
    cur=$(halcmd gets in-mode-csv-$name)
    if [ "$cur" == "0" ]; then
        echo "[spindle.$name][velocity]-#001- Spindle should be enabled !"
        return 1
    fi

    #Test air protection
    cur=$(halcmd getp spindle.$name.air-protection)
    if [ "$cur" == "0" ]; then
        echo "[spindle.$name][velocity]-#002- Air protection should be on !"
        return 1
    fi

    #Test velocity reached before 
    halcmd setp spindle.$name.cmd-velocity 30000
    sleep 1.5
    cur=$(halcmd getp spindle.$name.velocity-reached)
    cur2=$(halcmd getp spindle.$name.current-velocity)
    cur3=$(halcmd getp spindle.$name.velocity.tolerance)
    if [ "$cur" == "1" ]; then
        echo "[spindle.$name][velocity]-#003- velocity-reached pin should be false !"
        echo "[spindle.$name][velocity]-#003- velocity-reached=$cur / current-velocity=$cur2 / velocity.tolerance=$cur3"
        return 1
    fi
    sleep 2.0
    #Test current velocity
    cur=$(halcmd gets current-velocity-$name)
    if [ "$cur" != "30000" ]; then
        echo "[spindle.$name][velocity]-#004- Speed not at 30000 !"
        return 1
    fi
    #Test velocity reached after
    cur=$(halcmd getp spindle.$name.velocity-reached)
    if [ "$cur" == "0" ]; then
        echo "[spindle.$name][velocity]-#005- Velocity pin should be true !"
        return 1
    fi

    # Test override 
    halcmd setp spindle.$name.cmd-override 10
    sleep 1.0
    cur=$(halcmd gets current-velocity-$name)
    if [ "$cur" != "33000" ]; then
        echo "[spindle.$name][velocity]-#006- Speed not at 33000 !"
        return 1
    fi

    #Test overspeeding
    halcmd setp spindle.$name.cmd-override 200
    sleep 3.0
    cur=$(halcmd gets current-velocity-$name)
    if [ "$cur" != "33000" ]; then
        echo "[spindle.$name][velocity]-#007- Speed not at 33000 !"
        return 1
    fi

    #Test underspeeding
    halcmd setp spindle.$name.cmd-override 0
    halcmd setp spindle.$name.cmd-velocity 10000
    sleep 3.1
    halcmd setp spindle.$name.cmd-override 0
    halcmd setp spindle.$name.cmd-velocity 30000
    sleep 3.1
    cur=$(halcmd gets current-velocity-$name)
    if [ "$cur" != "30000" ]; then
        echo "[spindle.$name][velocity]-#008- Speed not at 30000 !"
        return 1
    fi

    #Test if can-turn pin is ok
    cur=$(halcmd getp spindle.$name.can-turn)
    if  [ "$cur" == "0" ]; then
        echo "[spindle.$name][atc][sens]-#009- Should be able to turn !"
        return 1
    fi

    #Setting back to "normal" for futher script
    halcmd setp spindle.$name.cmd-override 0
    halcmd setp spindle.$name.cmd-velocity 0
    halcmd setp spindle.$name.cmd-enable 0
    sleep 4
}


## --------- main program --------- ##

#Parameter input check
if [ $# != 1 ]; then
    echo "[spindle][velocity] Please specify spindle name"
    return 1
fi

name="$1" #name of the spindle

#testing
test_velocity $name
if [[ $? == 1 ]]; then
    return 1
else
    return 0
fi
