#!/bin/bash

#################################################################################################
# This file is part of a validation folder for the openCN spindle components                    #
# This file test all the behavior of the spindle                                                #
# It is assumed that the spindle is linked to a drive.                                          #
# The script is meant to test mainly the C code managing the spindle.                           #
# Therefore, it should not be used on a live spindle                                            #
#                                                                                               #
# Author: Xavier Soltner                                                                        #
#                                                                                               #
# Usage : ./test_spindle.sh <spindle_name> [--all] [--std] [--atc] [--temperature] [--cooling]  #
#                                                                                               #
# This script DO test :                                                                         #
#   - Final command velocity                                                                    #
#   - Configuration : std                                                                       #
#   - Configuration : atc                                                                       #
#   - Temperature option                                                                        #
#   - Cooling option                                                                            #
#                                                                                               #
# This scipt do NOT test :                                                                      #
#   - Transition (eg. jump of velocity that may result in spindle error)                        #
#   - Fault state                                                                               #
#                                                                                               #
# Version: 1.0                                                                                  #
# Date: 2024-11-14                                                                              #
#################################################################################################

## --------- Input Verification --------- ##

#Parameter input check
if [[ $# -lt 1 ]]; then
    echo "[spindle] Please specify spindle name"
    exit 1
fi

name="$1" #name of the spindle

#Check if the component exist 
cur=$(halcmd getp spindle.$name.cmd-enable)
if [ -z "$cur" ]; then
    echo "[spindle] Component spindle.$name not found"
    exit 1
fi

## --------- parsing option -------- ##
is_std=0
is_atc=0
has_temperature=0
has_cooling=0

shift 1
while [ True ]; do
if [ "$1" = "--all" ]; then
    is_std=1
    is_atc=1
    has_temperature=1
    has_cooling=1
    shift 1
elif [ "$1" = "--std" ]; then
    is_std=1
    shift 1
elif [ "$1" = "--atc" ]; then
    is_atc=1
    shift 1
elif [ "$1" = "--temperature" ]; then
    has_temperature=1
    shift 1
elif [ "$1" = "--cooling" ]; then
    has_cooling=1
    shift 1
else
    break
fi
done

echo "  is_std=$is_std"
echo "  is_atc=$is_atc"
echo "  has_temperature=$has_temperature"
echo "  has_cooling=$has_cooling"

## --------- Actual testing --------- ##

# atc
if [ $is_atc == 1 ]; then
    source atc.sh $name
    if [[ $? == 1 ]]; then
        echo "[spindle][atc] Test failed !"
        exit 1
    else
        echo "[spindle][atc] Test sucessful !"
    fi
fi

# Temperature
if [ $has_temperature == 1 ]; then
    source temperature.sh $name
    if [[ $? == 1 ]]; then
        echo "[spindle][temperature] Test failed !"
        exit 1
    else
        echo "[spindle][temperature] Test sucessful !"
    fi
fi

# Velocity is tested in all cases
if [ true ]; then
    source velocity.sh $name
    if [[ $? == 1 ]]; then
        echo "[spindle][velocity] Test failed !"
        exit 1
    else
        echo "[spindle][velocity] Test sucessful !"
    fi
fi

# Cooling
if [ $has_cooling == 1 ]; then
    source cooling.sh $name
    if [[ $? == 1 ]]; then
        echo "[spindle][cooling] Test failed !"
        exit 1
    else
        echo "[spindle][cooling] Test sucessful !"
    fi
fi

#If we are here, everything went well
echo "[spindle] All test succesful"
exit 0