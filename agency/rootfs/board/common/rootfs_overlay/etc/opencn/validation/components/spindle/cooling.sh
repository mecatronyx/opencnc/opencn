#!/bin/bash

#################################################################################
# This file is part of a validation folder for the openCN spindle components    #
# This file test the behavior for the "cooling" option.                         #
# Please note that the cooling option is also tested in the                     #
# temperature.sh script, if present.                                            #
#                                                                               #
# Author: Xavier Soltner                                                        #
#                                                                               #
# Version: 1.0                                                                  #
# Date: 2024-11-14                                                              #
#################################################################################





function test_cooling()
{
    local name="$1" #name of the spindle
    local cur #current pin value
    
    echo "[spindle.$name][cooling] - Start cooling test"
    
    #Nominal case
    halcmd setp spindle.$name.cmd-velocity 0
    halcmd setp spindle.$name.cmd-enable 1
    sleep 6.1
    cur=$(halcmd gets in-mode-csv-$name)
    if  [ "$cur" == "0" ]; then
        echo "[spindle.$name][cooling]-#001- Should be in CSV !"
        return 1
    fi

    cur=$(halcmd getp spindle.$name.cooling.enable)
    if  [ "$cur" == "0" ]; then
        echo "[spindle.$name][cooling]-#002- Should be on !"
        return 1
    fi

    halcmd setp spindle.$name.cmd-enable 0
    sleep 6.1
    cur=$(halcmd getp spindle.$name.cooling.enable)
    if  [ "$cur" == "1" ]; then
        echo "[spindle.$name][cooling]-#003- Should be off !"
        return 1
    fi

}

## --------- main program --------- ##

#Parameter input check
if [ $# != 1 ]; then
    echo "[spindle][cooling] Please specify spindle name"
    return 1
fi

name="$1" #name of the spindle

#Testing
test_cooling $name
if [[ $? == 1 ]]; then
    echo "[spindle.$name][cooling] Test failed !"
    return 1
else
    echo "[spindle.$name][cooling] Test sucessful !"
fi

return 0