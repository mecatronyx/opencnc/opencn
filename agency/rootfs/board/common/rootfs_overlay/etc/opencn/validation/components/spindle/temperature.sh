#!/bin/bash

#################################################################################
# This file is part of a validation folder for the openCN spindle components    #
# This file test the behavior for the temperature option                        #
#                                                                               #
# Author: Xavier Soltner                                                        #
#                                                                               #
# Version: 1.0                                                                  #
# Date: 2024-11-14                                                              #
#################################################################################


function test_temperature()
{
    local name="$1" #name of the spindle
    local cur #current pin value
    local has_cooling

    echo "[spindle.$name][temperature] - Start Temperature test"

    # Testing nominal
    # Temperature param
    halcmd setp spindle.$name.temp.min 20
    halcmd setp spindle.$name.temp.max 25
    halcmd setp spindle.$name.temp.max-vel-min-temp 30000

    halcmd setp spindle.$name.temp.actual 22
    halcmd setp spindle.$name.cmd-enable 1
    sleep 0.1
    
    cur=$(halcmd gets in-mode-csv-$name)
    if [ "$cur" == "0" ]; then
        echo "[spindle.$name][temperature]-#001- Spindle should be enabled !"
        return 1
    fi

    #Under temperature
    halcmd setp spindle.$name.temp.actual 19
    halcmd setp spindle.$name.cmd-enable 1
    sleep 0.1
    cur=$(halcmd gets in-mode-csv-$name)
    if [ "$cur" == "0" ]; then
        echo "[spindle.$name][temperature]-#002- Spindle should be enabled !"
        return 1
    fi

    #Test spindle max velocity under min-temperature
    halcmd setp spindle.$name.cmd-velocity 30000
    sleep 3.5
    cur=$(halcmd gets current-velocity-$name)
    if [ "$cur" != "30000" ]; then
        echo "[spindle.$name][temperature]-#003- Spindle should be at 30 000 !"
        return 1
    else
        echo "[spindle.$name][temperature]-#003- velocity=$cur"
    fi
    halcmd setp spindle.$name.cmd-velocity 31000
    sleep 3.5
    cur=$(halcmd gets current-velocity-$name)
    if [ "$cur" == "31000" ]; then
        echo "[spindle.$name][temperature]-#004- Spindle should NOT be at 31 000 !"
        return 1
    else
        echo "[spindle.$name][temperature]-#004- velocity=$cur"
    fi

    # TODO check velocity-reached pin

    #Over temperature
    halcmd setp spindle.$name.temp.actual 26
    halcmd setp spindle.$name.cmd-enable 1
    sleep 4.0
    cur=$(halcmd gets in-mode-csv-$name)
    if [ "$cur" == "1" ]; then
        echo "[spindle.$name][temperature]-#005- Spindle should NOT be enabled in-mode-csv-s=$cur !"
        return 1
    fi

    #Check if the cooling option is enabled
    cur=$(halcmd getp spindle.$name.cooling.enable)
    if [ -z "$cur" ]; then
        has_cooling=0
    else
        has_cooling=1
    fi

    if [ has_cooling ]; then
        #Check cooling if overtemperature
        cur=$(halcmd getp spindle.$name.cooling.enable)
        if [ "$cur" == "0" ]; then
            echo "[spindle.$name][temperature]-#006- Cooling should be enable in overtemperature !"
            return 1
        fi

        #Cooling should stop if stopped and not in overtemperature
        halcmd setp spindle.$name.temp.actual 22
        halcmd setp spindle.$name.cmd-enable 0
        sleep 0.1
        cur=$(halcmd getp spindle.$name.cooling.enable)
        if [ "$cur" == "1" ]; then
            echo "[spindle.$name][temperature]-#007- Cooling should be disabled when spinde is disabled and in temperature range !"
            return 1
        fi
    fi
}


## --------- main program --------- ##

#Parameter input check
if [ $# != 1 ]; then
    echo "[spindle][temperature] Please specify spindle name"
    return 1
fi

name="$1" #name of the spindle

test_temperature $name
if [[ $? == 1 ]]; then
    return 1
else
    return 0
fi

