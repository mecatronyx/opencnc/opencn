#!/bin/bash

#################################################################################
# This file is part of a validation folder for the openCN spindle components    #
# This file test the behavior for the "ATC" configuration                       #
#                                                                               #
# Author: Xavier Soltner                                                        #
#                                                                               #
# Version: 1.0                                                                  #
# Date: 2024-11-14                                                              #
#################################################################################





function test_sensor()
{
    local name="$1" #name of the spindle
    local cur #current pin value
    
    echo "[spindle.$name][atc][sens] - Start ATC sensor test"
    
    #Nominal case
    halcmd setp spindle.$name.cmd-override 0
    halcmd setp spindle.$name.cmd-velocity 0
    halcmd setp spindle.$name.atc.sens-0 1
    halcmd setp spindle.$name.atc.sens-1 1
    halcmd setp spindle.$name.cmd-enable 1
    sleep 1
    cur=$(halcmd gets in-mode-csv-$name)
    if  [ "$cur" == "0" ]; then
        echo "[spindle.$name][atc][sens]-#001- Should be in CSV !"
        return 1
    fi

    cur=$(halcmd getp spindle.$name.can-turn)
    if  [ "$cur" == "0" ]; then
        echo "[spindle.$name][atc][sens]-#002- Should turn !"
        return 1
    fi

    #Activate/Desactivate sensor in different scenario
    halcmd setp spindle.$name.atc.sens-1 0
    sleep 0.1
    cur=$(halcmd gets in-mode-csv-$name)
    if [ "$cur" == "1" ]; then
        echo "[spindle.$name][atc][sens]-#003- Should not be in CSV !"
        return 1
    fi
    
    cur=$(halcmd getp spindle.$name.can-turn)
    if  [ "$cur" == "1" ]; then
        echo "[spindle.$name][atc][sens]-#004- Should not turn !"
        return 1
    fi

    halcmd setp spindle.$name.atc.sens-1 1
    halcmd setp spindle.$name.cmd-enable 1
    sleep 0.1
    cur=$(halcmd gets in-mode-csv-$name)
    if [ "$cur" == "0" ]; then
        echo "[spindle.$name][atc][sens]-#005- Should be in CSV !"
        return 1
    fi


    halcmd setp spindle.$name.atc.sens-0 0
    sleep 0.1
    cur=$(halcmd gets in-mode-csv-$name)
    if [ "$cur" == "1" ]; then
        echo "[spindle.$name][atc][sens]-#006- Should not be in CSV !"
        return 1
    fi

    halcmd setp spindle.$name.atc.sens-0 1
    halcmd setp spindle.$name.cmd-enable 1
    sleep 0.1
    cur=$(halcmd gets in-mode-csv-$name)
    if [ "$cur" == "0" ]; then
        echo "[spindle.$name][atc][sens]-#007- Should be in CSV !"
        return 1
    fi

    return 0
}

function test_cone_cleaning_is_on()
{
    local cur
    cur=$(halcmd getp spindle.$name.atc.clean-cone)
    if [ "$cur" == "0" ]; then
        echo "[spindle.$name][atc][cone_cleaning] Cone cleaning should be ON !"
        return 1
    else
        echo "[spindle.$name][atc][cone_cleaning] spindle.$name.atc.clean-cone=$cur / Should be on" 
    fi
    return 0
}

function test_cone_cleaning_is_off()
{
    local cur
    cur=$(halcmd getp spindle.$name.atc.clean-cone)
    if [ "$cur" == "1" ]; then
        echo "[spindle.$name][atc][cone_cleaning] Cone cleaning should be OFF !"
        return 1
    else
        echo "[spindle.$name][atc][cone_cleaning] spindle.$name.atc.clean-cone=$cur / Should be off" 
    fi
    return 0
}

function test_locking()
{
    local cur

    echo "[spindle.$name][atc] - Start ATC locking/unlocking test"

    #Nominal case
    halcmd setp spindle.$name.cmd-enable 0
    halcmd setp spindle.$name.atc.sens-0 1
    halcmd setp spindle.$name.atc.sens-1 1
    sleep 6
    #Cone cleaning test
    test_cone_cleaning_is_off
    if [[ $? == 1 ]]; then
        echo "[spindle.$name][atc][locking]-#001- Cone cleaning should be OFF !"
        return 1
    fi
    halcmd setp spindle.$name.atc.cmd-free-tool 1
    sleep 0.1
    test_cone_cleaning_is_on
    if [[ $? == 1 ]]; then
        echo "[spindle.$name][atc][locking]-#002- Cone cleaning should be ON !"
        return 1
    fi
    #Tooling free test
    cur=$(halcmd getp spindle.$name.atc.free-tool)
    if [ "$cur" == "0" ];  then
        echo "[spindle.$name][atc][locking]-#003- Unlocking should be ON !"
        return 1
    fi

    # Test if spindle is enabled
    halcmd setp spindle.$name.atc.cmd-free-tool 0
    halcmd setp spindle.$name.cmd-enable 1
    halcmd setp spindle.$name.atc.cmd-free-tool 1
    sleep 0.1
    cur=$(halcmd getp spindle.$name.atc.free-tool)
    if [ "$cur" == "1" ]; then
        echo "[spindle.$name][atc][locking]-#004- Unlocking should be OFF !"
        return 1
    fi

    # Test if machine is active #TODO move to velocity test as its general
    halcmd setp spindle.$name.atc.cmd-free-tool 0
    halcmd setp spindle.$name.cmd-enable 1
    halcmd setp spindle.$name.machine-active 1
    halcmd setp spindle.$name.atc.cmd-free-tool 1
    sleep 0.1
    cur=$(halcmd getp spindle.$name.atc.free-tool)
    if [ "$cur" == "1" ]; then
        echo "[spindle.$name][atc][locking]-#005- Unlocking should be OFF !"
        return 1
    fi

    return 0
}


## --------- main program --------- ##

#Parameter input check
if [ $# != 1 ]; then
    echo "[spindle][atc] Please specify spindle name"
    return 1
fi

name="$1" #name of the spindle

test_sensor $name
if [[ $? == 1 ]]; then
    echo "[spindle.$name][atc][sens] Test failed !"
    return 1
else
    echo "[spindle.$name][atc][sens] Test sucessful !"
fi

test_locking $name
if [[ $? == 1 ]]; then
    echo "[spindle.$name][atc][locking] Test failed !"
    return 1
else
    echo "[spindle.$name][atc][locking] Test sucessful !"
fi

#Leave pin "Ok" for rest of testing
halcmd setp spindle.$name.atc.sens-0 1
halcmd setp spindle.$name.atc.sens-1 1
halcmd setp spindle.$name.machine-active 0

return 0