#!/bin/bash

cd /root

echo "Enabling RT logging echo"
/root/logfile -d

# rs274 interpreter
export INI_FILE_NAME=/root/micro5/micro5.ini

echo -e "Loading OpenCN configuration... "
/root/micro5/micro5_startup.sh
echo "OK"

echo "Starting OpenCN server"
/root/opencn-m5-server &

cd -
