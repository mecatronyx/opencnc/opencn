#!/bin/bash

HALCMD_CMD="addf getp gets load net setp sets show start stop"
HALCMD_OPTIONS="-f -h"
HALCMD_SHOW_OPTION="comp function param pin signal thread"

HALCMD_COMPONENTS_PATH="/etc/opencn/components"


function improved_ls()
{
  command ls "$@" | sed 's/\*//g'
}

function get_pins()
{
    local pins=$(halcmd list pin)
    echo $pins
}

function get_params()
{
    local params=$(halcmd list param)
    echo $params
}

function get_components()
{
    local components=$(improved_ls $HALCMD_COMPONENTS_PATH)
    echo "$components"
}

function get_functions()
{
    local functions=$(halcmd list funct)
    echo "$functions"
}

function get_signals()
{
    local signals=$(halcmd list signal)
    echo "$signals"
}

function get_threads()
{
    local threads=$(halcmd list thread)
    echo "$threads"
}

# addf func_name   thread_name
function do_addf()
{
    local cwords=${COMP_CWORD}

    if [ "$cwords" == "2" ]; then
        echo "$(get_functions)"
    elif [ "$cwords" == "3" ]; then
        echo "$(get_threads)"
    fi
}

# net sig_name PIN_1 [<= =>] PIN_2
function do_net()
{
    local cwords=${COMP_CWORD}

    if [ "$cwords" == "3" ]; then
        echo "$(get_pins)"
    elif [ "$cwords" == "4" ]; then
    #     echo "<= =>"
    # elif [ "$cwords" == "5" ]; then
        echo "$(get_pins)"
    fi
}


function do_show()
{
    local cwords=${COMP_CWORD}
    local prev=$(( cwords - 1 ))

    if [ "$cwords" == "2" ]; then
        echo $HALCMD_SHOW_OPTION

    elif [ "$cwords" == "3" ]; then
        case "${COMP_WORDS[$prev]}" in
            "function")
                echo $(get_functions)
                ;;
            "param")
                echo $(get_params)
                ;;
            "pin")
                echo "$(get_pins)"
                ;;
            "signal")
                echo "$(get_signals)"
                ;;
            "thread")
                echo "$(get_threads)"
                ;;
            *)
              ;;
        esac
    fi
}

_halcmd_completions ()
{
    local cwords=${COMP_CWORD}

    if [ ${COMP_CWORD} == "1" ]; then
        if [[ "${COMP_WORDS[1]}" == "-f" ]]; then
            COMPREPLY="-f"
        elif [[ "${COMP_WORDS[1]}" == "-h" ]]; then
            COMPREPLY="-h"
        else
            COMPREPLY=($(compgen -W "$HALCMD_CMD $HALCMD_OPTIONS" "${COMP_WORDS[1]}"))
        fi
    elif [ ${COMP_CWORD} -gt 1 ]; then

        case "${COMP_WORDS[1]}" in
            "-f")
                if [ "$cwords" == "2" ]; then
                    COMPREPLY=($(compgen -W "$(ls ${COMP_WORDS[2]}* 2>/dev/null)" "${COMP_WORDS[2]}"))
                fi
                ;;
            "-h")
                if [ "$cwords" == "2" ]; then
                    COMPREPLY=($(compgen -W "$HALCMD_CMD" "${COMP_WORDS[2]}"))
                fi
                ;;
            "addf")
                COMPREPLY=($(compgen -W "$(do_addf)" "${COMP_WORDS[$cwords]}"))
                ;;
            "setp") ;&
            "getp")
                if [ "$cwords" == "2" ]; then
                    COMPREPLY=($(compgen -W "$(get_pins) $(get_params)" "${COMP_WORDS[2]}"))
                fi
                ;;
            "sets") ;&
            "gets")
                if [ "$cwords" == "2" ]; then
                    COMPREPLY=($(compgen -W "$(get_signals)" "${COMP_WORDS[2]}"))
                fi
                ;;
            "load")
                # TODO How to deal with the components parameters ?
                if [ "$cwords" == "2" ]; then
                    COMPREPLY=($(compgen -W "$(get_components)" "${COMP_WORDS[2]}"))
                fi
                ;;
            "net")
                COMPREPLY=($(compgen -W "$(do_net)" "${COMP_WORDS[$cwords]}"))
                ;;
            "show")
                COMPREPLY=($(compgen -W "$(do_show)" "${COMP_WORDS[$cwords]}"))
                ;;
            "start") ;&
            "stop")  ;&
            *)
              ;;
        esac
    fi;
}

complete -F _halcmd_completions halcmd
