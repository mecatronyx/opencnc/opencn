#!/bin/bash

rpi4="Raspberry Pi 4 Model B"

# get the actual  board model - remove null byte
actual_model=$(cat /proc/device-tree/model | tr -d '\0')

# Update the network interface for rpi4 board only
if [ "$actual_model" = "$rpi4" ]; then
    # Note: The HAT used with the 'rpi4' does not support auto-negotiation.
    # Manually et the interface in full-duplex mode
    ifconfig eth0 down
    ifconfig eth1 down

    ethtool -s eth0 speed 10 duplex full
    ethtool -s eth1 speed 10 duplex full

    ifconfig eth0 up
    ifconfig eth1 up
fi
