#!/bin/bash
# Feedopt testing script


function pause()
{
 read -s -n 1 -p "Press any key to continue . . ."
 echo ""
}

# Logs messages in terminals
#/root/logfile -d

# == Setup configuration ==
echo "== Setup configuration"

cp /root/jmi/micro5.ini         /root/.
cp /root/jmi/tool.tbl           /root/.

# cp /root/jmi/011_anchor.ngc     /root/gcode.ngc
cp /root/jmi/012_anchor_5D.ngc /root/gcode.ngc
# cp /root/jmi/dome.ngc /root/gcode.ngc

export INI_FILE_NAME="/root/micro5.ini"

# == HAL setup
echo "load HAL file"
/root/halcmd -f feedopt.hal
sleep 1 # keep time to load and start HAL

# 1. Start Feedopt - "Sample Generation"
/root/halcmd setp feedopt.gen.start 1

sleep 5

# start recording
/root/halcmd setp sampler.0.enable 1

# Start Feedopt - "Sample Reading"
/root/halcmd setp feedopt.read.start 1

sleep 50

echo "Test terminated"