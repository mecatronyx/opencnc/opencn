#!/bin/bash

#################################################################################
# This file is part of a validation folder for the openCN connected to an EPOS4 #
# controller. It is the validation of CSP mode.                                 #
#                                                                               #
# Author: Sutterlet Bruno                                                       #
# From a script by: Miceli Jean-Pierre                                          #
#                                                                               #
# Version: 1.0                                                                  #
# Date: 2023-11-24                                                              #
#################################################################################

# Local Variables

# Functions

# Source functions
source "$(dirname "$0")/functions.sh"

#-- Main --#

function test_csp()
{
    local actual_pos
    local target_pos
    local relative_target_pos

    print_message -e "[epos4] [csp] - Start CSP test"

    # Test 3.00 : Check if the motor is in CSP mode after set
    print_message -e "[epos4] [csp] [3.00] - Set mux selector to 0"
    halcmd setp mux.target.selector 0

    print_message -e "[epos4] [csp] [3.00] - Homing"
    homing
    sleep 1

    # set drive inactive
    print_message -e "[epos4] [csp] [3.00] - Set drive inactive"
    halcmd setp lcec.0.EPOS4.set-mode-inactive 1

    # set target position to actual position
    print_message -e "[epos4] [csp] [3.00] - Set target position to actual position"
    halcmd setp simple_pg.joint0.cmd-pos $(halcmd gets actual_pos)

    # deasable simple position generator
    print_message -e "[epos4] [csp] [3.00] - Desable simple position generator"
    halcmd setp simple_pg.enable 0
    sleep 1

    # enable simple position generator
    print_message -e "[epos4] [csp] [3.00] - Enable simple position generator"
    halcmd setp simple_pg.enable 1
    sleep 1

    print_message -h "[epos4] [csp] [3.00] - Set CSP"
    if ! set_drive_csp; then
        print_message -e "[epos4] [csp] [3.00] - Failed to set Homing mode"
        echo -e "[epos4] ERROR - Test 3.00 failed"
        exit 1
    else
        print_message -e "[epos4] [csp] [3.00] - Test passed"
        print_message -e ""
    fi

    # Test 3.01 : Move the motor in CSP mode in positive roation, absolute position
    print_message -e "[epos4] [csp] [3.01] - Init absolute position"
    halcmd setp simple_pg.joint0.relative-pos 0

    print_message -e "[epos4] [csp] [3.01] - Set target position to 100"
    target_pos=100
    halcmd setp simple_pg.joint0.cmd-pos $target_pos

    wait_simple_pg --watch_dog 20

    actual_pos=$(halcmd gets actual_pos)
    if ! in_range $actual_pos $target_pos 0.005; then
        print_message -e "[epos4] [csp] [3.01] - Failed to moove"
        print_shit -e "[epos4] - DEBUG: actual position : $actual_pos"
        print_shit -e "[epos4] - DEBUG: target position : $target_pos"

        echo -e "[epos4] ERROR - Test 3.01 failed"
        exit 1
    else
        print_shit -e "[epos4] - DEBUG: actual position : $actual_pos"
        print_shit -e "[epos4] - DEBUG: target position : $target_pos"

        print_message -e "[epos4] [csp] [3.01] - Test passed"
        print_message -e ""
    fi


    # Test 3.02 : Move the motor in CSP mode in negative roation, absolute position
    print_message -e "[epos4] [csp] [3.02] - Init absolute position"
    halcmd setp simple_pg.joint0.relative-pos 0

    print_message -e "[epos4] [csp] [3.02] - Set target position to 0"
    target_pos=0
    halcmd setp simple_pg.joint0.cmd-pos $target_pos

    wait_simple_pg --watch_dog 20

    actual_pos=$(halcmd gets actual_pos)
    if ! in_range $actual_pos $target_pos 0.005; then
        print_message -e "[epos4] [csp] [3.02] - Failed to moove"
        print_shit -e "[epos4] - DEBUG: actual position : $actual_pos"
        print_shit -e "[epos4] - DEBUG: target position : $target_pos"

        echo -e "[epos4] ERROR - Test 3.02 failed"
        exit 1
    else
        print_shit -e "[epos4] - DEBUG: actual position : $actual_pos"
        print_shit -e "[epos4] - DEBUG: target position : $target_pos"

        print_message -e "[epos4] [csp] [3.02] - Test passed"
        print_message -e ""
    fi

    # Test 3.03 : Move the motor in CSP mode in positive roation, relative position
    print_message -e "[epos4] [csp] [3.03] - Init relative position"
    halcmd setp simple_pg.joint0.relative-pos 1

    print_message -e "[epos4] [csp] [3.03] - Set target position to 100"
    relative_target_pos=100
    halcmd setp simple_pg.joint0.cmd-pos $relative_target_pos
    actual_pos=$(halcmd gets actual_pos)
    target_pos=$(add_float "$relative_target_pos" "$actual_pos")

    wait_simple_pg --watch_dog 20

    actual_pos=$(halcmd gets actual_pos)
    if ! in_range $actual_pos $target_pos 0.005; then
        print_message -e "[epos4] [csp] [3.03] - Failed to moove"
        print_shit -e "[epos4] - DEBUG: actual position : $actual_pos"
        print_shit -e "[epos4] - DEBUG: target position : $target_pos"

        echo -e "[epos4] ERROR - Test 3.03 failed"
        exit 1
    else
        print_shit -e "[epos4] - DEBUG: actual position : $actual_pos"
        print_shit -e "[epos4] - DEBUG: target position : $target_pos"

        print_message -e "[epos4] [csp] [3.03] - Test passed"
        print_message -e ""
    fi

    # Test 3.04 : Move the motor in CSP mode in negative roation, relative position
    print_message -e "[epos4] [csp] [3.04] - Init relative position"
    halcmd setp simple_pg.joint0.relative-pos 1

    print_message -e "[epos4] [csp] [3.04] - Set target position to -100"
    relative_target_pos=-100
    halcmd setp simple_pg.joint0.cmd-pos $relative_target_pos
    actual_pos=$(halcmd gets actual_pos)
    target_pos=$(add_float "$relative_target_pos" "$actual_pos")

    wait_simple_pg --watch_dog 20

    actual_pos=$(halcmd gets actual_pos)
    if ! in_range $actual_pos $target_pos 0.005; then
        print_message -e "[epos4] [csp] [3.04] - Failed to moove"
        print_shit -e "[epos4] - DEBUG: actual position : $actual_pos"
        print_shit -e "[epos4] - DEBUG: target position : $target_pos"

        echo -e "[epos4] ERROR - Test 3.04 failed"
        exit 1
    else
        print_shit -e "[epos4] - DEBUG: actual position : $actual_pos"
        print_shit -e "[epos4] - DEBUG: target position : $target_pos"

        print_message -e "[epos4] [csp] [3.04] - Test passed"
        print_message -e ""
    fi

    # Test 3.05 : Stop motor durning movement
    print_message -e "[epos4] [csp] [3.05] - Init absolute position"
    halcmd setp simple_pg.joint0.relative-pos 1

    print_message -e "[epos4] [csp] [3.05] - Set target position to 200"
    target_pos=200
    halcmd setp simple_pg.joint0.cmd-pos $target_pos

    sleep 10

    print_message -e "[epos4] [csp] [3.05] - Stop motor"
    halcmd setp simple_pg.joint0.stop 1

    wait_simple_pg --watch_dog 5

    actual_pos=$(halcmd gets actual_pos)
    print_shit -e "[epos4] - DEBUG: actual position : $actual_pos"

    print_message -e "[epos4] [csp] - Desable simple pg"
    halcmd setp simple_pg.enable 0
    sleep 1

    print_message -e "[epos4] [csp] - Set mode inactive"
    set_drive_inactive


    echo -e "[epos4] [csp] - Test CSP passed successfully"
    return 0
}