# Validation documentation
This is the documentation of the validation for the implementation of the EPOS4 controler in openCN. This document provide the overview of all features tested and .

## Architecture of the HAl environment
OpenCN is based on an HAL architecture. It means that the communication with the Epos4 is done through HAL components. You can find an overview of HAL components [here](https://mecatronyx.gitlab.io/opencnc/opencn/components/components.html).

For this project, a simple architecture has been provided to test and control the Epos4. Here is a schema of the architecture:

![Schéma openCN components](./pictures/architecture.drawio.png)

This architecture is set up in /epos4/epos4.hal file.


## Architecture of the verification code

The validaion code would be too long to be in one file. So it is devided in multiple files like so :


```bash
validation
├── validation.sh
├── functions.sh
├── features
│   ├── ppm.sh
│   ├── pvm.sh
│   ├── csp.sh
│   ├── csv.sh
│   ├── cst.sh
│   ├── hm.sh
│   ├── (inactive_mode.sh) -> not implemented, useless
|   └── other_features.sh
│
└── Validation.md
```

The main file is `validation.sh`. It is the code that is called by the user to launch the validation. It is the file that will call all the other files.

`functions.sh` is the file that contains all the generique functions that are used by the other code's parts.

### Use of the validation code

To use the validation code, you need to launch the `validation.sh` file. You can do it by typing `./validation.sh` in the terminal. You can also add the option `-h` or `--help` to get the help message.

```bash
./validation.sh -h
```

This will display the help message:

```bash
Usage: test.sh [options]
Options:
  -init               Initialize the HAL file
  -nv, --non-verbal   Disable verbose mode
  -vv, --very-verbose Print a bunsh of shit
  -h, --help          Show this help message
```


### List of HAL componants
Here is a list of HAL components used for the project:

| Command | Description |
|----------|-------------|
| lcec | Ethernet communication with the Epos4 |
| mux | Multiplexer to select contrôl signal |
| simple_pg | Simple position generator |


## Overview
This is the overview of all the features that are available for validation by class.

### Modes

| Mode | Description |
| --- | --- |
| HM | Homing Mode |
| Inactive mode | Mode used to desable the motor |
| PPM | Profile Position Mode: mode used to control the motor in position with the intern regulator of the epos 4 |
| PVM | Profile Velocity Mode: mode used to control the motor in velocity with the intern regulator of the epos 4 |
| CSP | Cyclic Synchronous Position Mode: mode used to control the motor in position with the simple profile generator of openCN |
| CSV | Cyclic Synchronous Velocity Mode: mode used to control the motor in velocity with the simple profile generator of openCN |
| CST | Cyclic Synchronous Torque Mode: mode used to control the motor in torque with the simple profile generator of openCN |

### Other Features

| Feature | Description |
| --- | --- |
|Change of position scale| Change the position scale of the motor|
|Change of velocity scale| Change the velocity scale of the motor|
|Quick stop| Stop the motor quickly|
|Change of controler mode| Change the mode of the controler |
|Modulo position| Change the position of the motor to a modulo position |
|Error detection| Detect error in the communication with the controler |
|Error handling| Handle error in the communication with the controler |

### Classes
| Class | Number |
| --- | --- |
| Modes | 0.xx - 6.xx |
| Other Features | 7.xx - 9.xx |


### Features
| Feature | Number |
| --- | --- |
| HM  | 0.xx |
| PPM | 1.xx |
| PVM | 2.xx |
| CSP | 3.xx |
| CSV | 4.xx |
| CST | 5.xx |
| Inactive mode | 6.xx |
| Other features | 7.xx - 9.xx |


## Test Index

### HM

| Test | Description | Written | Tested | passed |
| ---- | ----------- | ------ | ------- | ------ |
| 0.00 | Check if the motor is in HM mode after seted | yes | yes | yes |
| 0.01 | Check if the motor is homed after homing | yes | yes | yes |

### PPM

| Test | Description | Written | Tested | passed |
| ---- | ----------- | ------ | ------- | ------ |
| 1.00 | Check if the motor is in PPM mode after seted | yes | yes | yes |
| 1.01 | Move the motor in PPM mode in positive roation, absolute position | yes | yes | yes |
| 1.02 | Move the motor in PPM mode in negative roation, absolute position | yes | yes | yes |
| 1.03 | Move the motor in PPM mode in positive roation, relative position | yes | yes | yes |
| 1.04 | Move the motor in PPM mode in negative roation, relative position | yes | yes | yes |
| 1.05 | Move the motor in PPM mode in absolute position with 'immediate change target mode' option | yes | yes | yes |
| 1.06 | Move the motor in PPM mode in relative position with 'immediate change target mode' option | yes | yes | yes |
| 1.07 | Stop motor durning movement | yes | yes | yes |
| 1.08 |Stop and restart directly the motor | yes | yes | yes |


### PVM

| Test | Description | Written | Tested | passed |
| ---- | ----------- | ------ | ------- | ------ |
| 2.00 | Check if the motor is in PVM mode after seted | yes | yes | yes |
| 2.01 | Move the motor in PVM mode in positive roation | yes | yes | yes |
| 2.02 | Move the motor in PVM mode in negative roation | yes | yes | yes |
| 2.03 | Change direction during movement| yes | yes | yes |

### CSP

| Test | Description | Written | Tested | passed |
| ---- | ----------- | ------ | ------- | ------ |
| 3.00 | Check if the motor is in CSP mode after seted | yes | yes | no |
| 3.01 | Move the motor in CSP mode in positive roation, absolute position | yes | yes | yes |
| 3.02 | Move the motor in CSP mode in negative roation, absolute position | yes | yes | no |
| 3.03 | Move the motor in CSP mode in positive roation, relative position | yes | yes | yes |
| 3.04 | Move the motor in CSP mode in negative roation, relative position | yes | yes | yes |
| 3.05 | Stop motor durning movement | yes | yes | yes |

### CSV

| Test | Description | Written | Tested | passed |
| ---- | ----------- | ------ | ------- | ------ |
| 4.00 | Check if the motor is in CSV mode after seted | yes | yes | yes |
| 4.01 | Move the motor in CSV mode in positive roation | yes | yes | yes |
| 4.02 | Move the motor in CSV mode in negative roation | yes | yes | yes |
| 4.03 | Stop motor durning movement | no | yes | no |
| - | Change velocity during movement | yes | yes | yes |

### CST

| Test | Description | Written | Tested | passed |
| ---- | ----------- | ------ | ------- | ------ |
| 5.00 | Check if the motor is in CST mode after seted | yes |  yes | yes |
| 5.01 | Move the motor in CST mode in positive roation | yes | yes | yes |
| 5.02 | Move the motor in CST mode in negative roation | yes | yes | yes |
| 5.03 | Stop motor durning movement | no | yes | no |
| - | Change torque during movement | no |

### Inactive mode

| Test | Description | Written | Tested | passed |
| ---- | ----------- | ------ | ------- | ------ |
| 6.00 | Check if the motor is in inactive mode after seted | no | yes | yes |

### Other Features

| Test | Description | Written | Tested | passed |
| ---- | ----------- | ------ | ------- | ------ |
| 7.00 | Change of position scale | no | yes | yes |
| 7.01 | Change of velocity scale | no | yes | yes |
| 7.02 | Quick stop | yes | yes | yes |
| 7.03 | Change of controler mode | no | yes | yes |
| 7.04 | Modulo position | yes | yes | yes |
| 7.05 | Error detection | yes | yes | yes |
| 7.06 | Error handling | yes | yes | yes |


**WARNING** : modulo does not work in relative position, it add the modulo value to the actual position instead of adding it to the target position.
