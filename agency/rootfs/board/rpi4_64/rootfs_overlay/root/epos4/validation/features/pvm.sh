#!/bin/bash

#################################################################################
# This file is part of a validation folder for the openCN connected to an EPOS4 #
# controller. It is the validation of PVM mode.                                 #
#                                                                               #
# Author: Sutterlet Bruno                                                       #
# From a script by: Miceli Jean-Pierre                                          #
#                                                                               #
# Version: 1.0                                                                  #
# Date: 2023-11-16                                                              #
#################################################################################

# Local Variables

# Functions

# Source functions
source "$(dirname "$0")/functions.sh"

#-- Main --#

function test_pvm()
{


    print_message -e "[epos4] [pvm] - Start PVM test"
    # Test 2.00 : Check if the motor is in pvm mode after set
    print_message -e "[epos4] [pvm] [2.01] - Set mux selector to 3"
    halcmd setp mux.target.selector 3

    print_message -e "[epos4] [pvm] [2.01] - Init target velocity to 0"
    halcmd setp mux.target.vector3.input0 0

    print_message -e "[epos4] [pvm] [2.00] - Set PVM"
    if ! set_drive_pvm; then
        print_message -e "[epos4] [pvm] [2.00] - Failed to set Homing mode"
        echo -e "[epos4] ERROR - Test 2.00 failed"
        exit 1
    else
        print_message -e "[epos4] [pvm] [2.00] - Test passed"
        print_message -e ""
    fi

    print_shit -e "[epos4] [pvm] - wait idle"
    wait_idle --watch_dog 15

    # Test 2.01 : Move the motor in PVM mode in positive rotation
    print_message -e "[epos4] [pvm] [2.01] - Set target velocity to 1000"
    halcmd setp mux.target.vector3.input0 1000
    sleep 10
    actual_velocity=$(halcmd gets actual_velocity)

    if ! in_range "$actual_velocity" 1000 15; then
        print_message -e "[epos4] [pvm] [2.01] - Failed to move the motor in PVM mode"
        print_shit -e "[epos4] [pvm] [2.01] - Actual velocity: $actual_velocity"
        print_shit -e "[epos4] [pvm] [2.01] - Expected velocity: 1000"

        print_message -e "[epos4] [pvm] [2.01] - Set target velocity to 0"
        halcmd setp mux.target.vector3.input0 0

        echo -e "[epos4] ERROR - Test 2.01 failed"
        exit 1
    else
        print_shit -e "[epos4] [pvm] [2.01] - Actual velocity: $actual_velocity"
        print_shit -e "[epos4] [pvm] [2.01] - Expected velocity: 1000"
        print_message -e "[epos4] [pvm] [2.01] - Set target velocity to 0"
        halcmd setp mux.target.vector3.input0 0

        print_message -e "[epos4] [pvm] [2.01] - Test passed"
        print_message -e ""
    fi

    print_shit -e "[epos4] [pvm] - wait idle"
    wait_idle --watch_dog 15

    # Test 2.02 : Move the motor in PVM mode in negative roation
    print_message -e "[epos4] [pvm] [2.02] - Set target velocity to -1000"
    halcmd setp mux.target.vector3.input0 -1000
    sleep 10

    actual_velocity=$(halcmd gets actual_velocity)
    if ! in_range "$actual_velocity" -1000 15; then
        print_message -e "[epos4] [pvm] [2.02] - Failed to move the motor in PVM mode"
        print_shit -e "[epos4] [pvm] [2.02] - Actual velocity: $actual_velocity"
        print_shit -e "[epos4] [pvm] [2.02] - Expected velocity: -1000"

        print_message -e "[epos4] [pvm] [2.02] - Set target velocity to 0"
        halcmd setp mux.target.vector3.input0 0

        echo -e "[epos4] ERROR - Test 2.02 failed"
        exit 1
    else
        print_shit -e "[epos4] [pvm] [2.02] - Actual velocity: $actual_velocity"
        print_shit -e "[epos4] [pvm] [2.02] - Expected velocity: -1000"
        print_message -e "[epos4] [pvm] [2.02] - Set target velocity to 0"
        halcmd setp mux.target.vector3.input0 0

        print_message -e "[epos4] [pvm] [2.02] - Test passed"
        print_message -e ""
    fi

    print_shit -e "[epos4] [pvm] - wait idle"
    wait_idle --watch_dog 15

    # Test 2.03 : Change direction during movement
    print_message -e "[epos4] [pvm] [2.03] - Set target velocity to 1500"
    halcmd setp mux.target.vector3.input0 1500
    sleep 10

    actual_velocity=$(halcmd gets actual_velocity)
    if ! in_range "$actual_velocity" 1500 15; then
        print_message -e "[epos4] [pvm] [2.03] - Failed to move the motor in PVM mode"
        print_shit -e "[epos4] [pvm] [2.03] - Actual velocity: $actual_velocity"
        print_shit -e "[epos4] [pvm] [2.03] - Expected velocity: 1500"

        print_message -e "[epos4] [pvm] [2.03] - Set target velocity to 0"
        halcmd setp mux.target.vector3.input0 0

        echo -e "[epos4] ERROR - Test 2.03 failed"
        exit 1
    else
        print_shit -e "[epos4] [pvm] [2.03] - Actual velocity: $actual_velocity"
        print_shit -e "[epos4] [pvm] [2.03] - Expected velocity: 1500"
    fi

    print_message -e "[epos4] [pvm] [2.03] - Set target velocity to -1500"
    halcmd setp mux.target.vector3.input0 -1500
    sleep 10

    actual_velocity=$(halcmd gets actual_velocity)
    if ! in_range "$actual_velocity" -1500 15; then
        print_message -e "[epos4] [pvm] [2.03] - Failed to move the motor in PVM mode"
        print_shit -e "[epos4] [pvm] [2.03] - Actual velocity: $actual_velocity"
        print_shit -e "[epos4] [pvm] [2.03] - Expected velocity: -1500"

        print_message -e "[epos4] [pvm] [2.03] - Set target velocity to 0"
        halcmd setp mux.target.vector3.input0 0

        echo -e "[epos4] ERROR - Test 2.03 failed"
        exit 1
    else
        print_shit -e "[epos4] [pvm] [2.03] - Actual velocity: $actual_velocity"
        print_shit -e "[epos4] [pvm] [2.03] - Expected velocity: -1500"
        print_message -e "[epos4] [pvm] [2.03] - Set target velocity to 0"
        halcmd setp mux.target.vector3.input0 0

        print_message -e "[epos4] [pvm] [2.03] - Test passed"
        print_message -e ""
    fi

    print_shit -e "[epos4] [pvm] - wait idle"
    wait_idle --watch_dog 15



    print_message -e "[epos4] [pvm] - Set mode inactive"
    sleep 1
    halcmd setp lcec.0.EPOS4.set-mode-inactive 1
    sleep 1

    print_message -e "[epos4] [pvm] - Test PVM passed successfully"

    return 0
}

