#!/bin/bash

#################################################################################
# This file is part of a validation folder for the openCN connected to an EPOS4 #
# controller. It is the validation of CSV mode.                                 #
#                                                                               #
# Author: Sutterlet Bruno                                                       #
#                                                                               #
# Version: 1.0                                                                  #
# Date: 2023-11-24                                                              #
#################################################################################

# Local Variables

# Functions

# Source functions
source "$(dirname "$0")/functions.sh"

#-- Main --#

function test_csv()
{
    local actual_vel
    local target_vel

    print_message -e "[epos4] [csv] - Start CSV test"

    # Test 4.00 : Check if the motor is in CSV mode after set
    print_message -e "[epos4] [csv] [4.00] - Set mux selector to 1"
    halcmd setp mux.target.selector 1

    print_message -e "[epos4] [csv] [4.00] - Homing"
    homing

    # enable simple position generator
    print_message -e "[epos4] [csv] [4.00] - Enable simple position generator"
    halcmd setp simple_pg.enable 1
    sleep 1

    print_message -h "[epos4] [csv] [4.00] - Set CSV"
    if ! set_drive_csv; then
        print_message -e "[epos4] [csv] [4.00] - Failed to set Homing mode"
        echo -e "[epos4] ERROR - Test 4.00 failed"
        exit 1
    else
        print_message -e "[epos4] [csv] [4.00] - Test passed"
        print_message -e ""
    fi

    # Test 4.01 : Move the motor in CSV mode in positive roation
    print_message -e "[epos4] [csv] [4.01] - Set target velocoty to 100"
    target_vel=100
    halcmd setp simple_pg.joint1.cmd-pos $target_vel

    sleep 15

    actual_vel=$(halcmd gets actual_velocity)
    if ! halcmd getp simple_pg.joint1.is_running; then
        print_message -e "[epos4] [csv] [4.01] - Failed to move the motor"
        print_shit -e "[epos4] [csv] - DEBUG: Actual velocity: $actual_vel"
        print_shit -e "[epos4] [csv] - DEBUG: Target velocity: $target_vel"

        echo -e "[epos4] ERROR - Test 4.01 failed"

        print_message -e "[epos4] [csv] [4.01] - Set motor inactive"
        set_drive_inactive
        exit 1
    else
        print_shit -e "[epos4] [csv] - DEBUG: Actual velocity: $actual_vel"
        print_shit -e "[epos4] [csv] - DEBUG: Target velocity: $target_vel"

        print_message -e "[epos4] [csv] [4.01] - Test passed"
        print_message -e ""
    fi

    print_message -e "[epos4] [csv] [4.01] - Set target velocoty to 0"
    target_vel=0
    halcmd setp simple_pg.joint1.cmd-pos $target_vel

    print_message -e "[epos4] [csv] [4.01] - Wait velocoty to 0"
    while true; do
        sleep 1
        print_message -n "."
        actual_vel=$(halcmd gets actual_velocity)
        if [ "$actual_vel" == "0" ]; then
            print_message -e "[epos4] [csv] [4.01] - Velocity = 0"
            break
        fi
    done

    print_message -e "[epos4] [csv] - Homing inter test"
    homing
    set_drive_csv

    # Test 4.02 Move the motor in CSV mode in negative roation
    print_message -e "[epos4] [csv] [4.02] - Set target velocoty to -100"
    target_vel=-100
    halcmd setp simple_pg.joint1.cmd-pos $target_vel

    sleep 15

    actual_vel=$(halcmd gets actual_velocity)
    if ! halcmd getp simple_pg.joint1.is_running; then
        print_message -e "[epos4] [csv] [4.02] - Failed to move the motor"
        print_shit -e "[epos4] [csv] - DEBUG: Actual velocity: $actual_vel"
        print_shit -e "[epos4] [csv] - DEBUG: Target velocity: $target_vel"

        echo -e "[epos4] ERROR - Test 4.02 failed"

        print_message -e "[epos4] [csv] [4.02] - Set motor inactive"
        set_drive_inactive
        exit 1
    else
        print_shit -e "[epos4] [csv] - DEBUG: Actual velocity: $actual_vel"
        print_shit -e "[epos4] [csv] - DEBUG: Target velocity: $target_vel"

        print_message -e "[epos4] [csv] [4.02] - Test passed"
        print_message -e ""
    fi

    print_message -e "[epos4] [csv] [4.02] - Set target velocoty to 0"
    target_vel=0
    halcmd setp simple_pg.joint1.cmd-pos $target_vel

    print_message -e "[epos4] [csv] [4.02] - Wait velocoty to 0"
    while true; do
        sleep 1
        print_message -n "."
        actual_vel=$(halcmd gets actual_velocity)
        if [ "$actual_vel" == "0" ]; then
            print_message -e "[epos4] [csv] [4.02] - Velocity = 0"
            break
        fi
    done


    print_message -e "[epos4] [csv] - Set motor inactive"
    set_drive_inactive

    echo -e "[epos4] [csv] - Test CSV passed successfully"
    return 0
}
