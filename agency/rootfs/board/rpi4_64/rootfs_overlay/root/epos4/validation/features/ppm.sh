#!/bin/bash

#################################################################################
# This file is part of a validation folder for the openCN connected to an EPOS4 #
# controller. It is the validation of PPM mode.                                 #
#                                                                               #
# Author: Sutterlet Bruno                                                       #
# From a script by: Miceli Jean-Pierre                                          #
#                                                                               #
# Version: 1.0                                                                  #
# Date: 2023-11-16                                                              #
#################################################################################

# Local Variables

# Functions

# Source functions
source "$(dirname "$0")/functions.sh"

# local functions
#moove $relative_pos $endless $immediate $target_pos
function moove_ppm()
{
    # set the drive in PPM mode
    halcmd setp mux.target.selector 3
    set_drive_ppm
    # initialize the options
    halcmd setp lcec.0.EPOS4.ppm.relative-pos     $1
    halcmd setp lcec.0.EPOS4.ppm.endless-movement $2
    halcmd setp lcec.0.EPOS4.ppm.immediate-change $3
    # set the target position
    halcmd setp mux.target.vector3.input0 $4
    # Set mode
    halcmd setp lcec.0.EPOS4.ppm.set-mode-ppm 1
    # start the movement
    halcmd setp lcec.0.EPOS4.ppm.start 1
}


# Main
function test_ppm()
{
    local actual_pos
    local target_pos
    local relative_target
    local NB_MOV

    print_message -e "--- Start PPM mode test ---\n"

    # test 1.00 : Check if the motor is in PPM mode after set
    print_message -e "[epos4] [ppm] [1.00] - Set mux selector to 3"
    halcmd setp mux.target.selector 3

    print_message -e "[epos4] [ppm] [1.00] - Set all PPM options to 0"
    halcmd setp lcec.0.EPOS4.ppm.relative-pos     0
    halcmd setp lcec.0.EPOS4.ppm.endless-movement 0
    halcmd setp lcec.0.EPOS4.ppm.immediate-change 0

    print_message -e "[epos4] [ppm] [1.00] - Set the drive in PPM mode and check the mode"

    print_message -e "[epos4] [ppm] - Test 1.00 success"

# ---------------------------------------------------------------------------------------


    print_shit -e "[epos4] [ppm] - Homing inter test"
    homing
    target_pos=0
    set_drive_ppm

# ---------------------------------------------------------------------------------------

    # test 1.01: 	Move the motor in PPM mode in positive roation, absolute position
    print_shit -n " - Set target : 100 r"
    print_message -n "."
    print_shit -e ""
    target_pos=100
    halcmd setp mux.target.vector3.input0  $target_pos

    print_message -e "[epos4] [ppm] [1.01] - Start motor"
    halcmd setp lcec.0.EPOS4.ppm.start 1


    print_message -e "[epos4] [ppm] [1.01] - Check if PPM procedure is active"
    idle=$(halcmd getp lcec.0.EPOS4.idle)
    if [ "$idle" == "1" ]; then
        echo  -e "[epos4] ERROR - drive is idle during ppm processing "
        exit 1
    fi

    print_message -e "[epos4] [ppm] [1.01] - Wait end of movement"
    wait_idle


    print_message -e "[epos4] [ppm] [1.01] - End of movement - check position"
    if ! in_range "$actual_pos" "$target_pos" 0.002; then
        echo -e ""
        echo -e "[epos4] WARNING - actual position '$actual_pos' is not in range of target position '$target_pos'"

    else
        print_shit -e "[epos4] [ppm] - Actual position '$actual_pos' is in range of target position '$target_pos'"
    fi

    print_message -e "[epos4] [ppm] - Test 1.01 success"

# ---------------------------------------------------------------------------------------

    print_shit -e "[epos4] [ppm] - Homing inter test"
    homing
    target_pos=0
    set_drive_ppm

#----------------------------------------------------------------------------------------------

    # test 1.02: Move the motor in PPM mode in negative roation, absolute position
    print_message -e "[epos4] [ppm] [1.02] - Set target : -100"
    target_pos=-100
    halcmd setp mux.target.vector3.input0  $target_pos

    print_message -e "[epos4] [ppm] [1.02] - Start motor"
    halcmd setp lcec.0.EPOS4.ppm.start 1

    sleep 1
    print_message -e "[epos4] [ppm] [1.02] - Check if PPM procedure is active"
    idle=$(halcmd getp lcec.0.EPOS4.idle)
    if [ "$idle" == "1" ]; then
        echo  -e "[epos4] ERROR - drive is idle during ppm processing "
        exit 1
    fi

    print_message -e "[epos4] [ppm] [1.02] - Wait end of movement"
    wait_idle

    print_message -e "[epos4] [ppm] [1.02] - End of movement - check position"
    check_position $target_pos


    print_message -e "[epos4] [ppm] - Test 1.02 success"

# ---------------------------------------------------------------------------------------

    print_shit -e "[epos4] [ppm] - Homing inter test"
    homing
    target_pos=0
    set_drive_ppm

#-----------------------------------------------------------------------------------------------------------

    # test 1.03 - 	Move the motor in PPM mode in positive roation, relative position
    print_message -e "[epos4] [ppm] [1.03] - Setup PPM options"
    halcmd setp lcec.0.EPOS4.ppm.relative-pos     1
    halcmd setp lcec.0.EPOS4.ppm.endless-movement 0
    halcmd setp lcec.0.EPOS4.ppm.immediate-change 0


    print_message -e "[epos4] [ppm] [1.03] - Set relative target : 50"
    relative_target=50
    halcmd setp mux.target.vector3.input0  $relative_target

    NB_MOV=4
    print_shit -n -e " - Movement done $NB_MOV times"
    for ((n = 1; n <= $NB_MOV; n++)); do
        target_pos=$((target_pos + relative_target))

        print_message -e "[epos4] [ppm] [1.03] - Start motor"
        halcmd setp lcec.0.EPOS4.ppm.start 1

        print_message -e "[epos4] [ppm] [1.03] - Wait end of movement"
        wait_idle

        print_message -e "[epos4] [ppm] [1.03] - End of movement $n / $NB_MOV - check position"
        actual_pos=$(halcmd gets actual_pos)
        if ! in_range "$actual_pos" "$target_pos" 0.002; then
            echo -e ""
            echo -e "[epos4] WARNING - actual position '$actual_pos' is not in range of target position '$target_pos'"

        else
            print_shit -e "[epos4] [ppm] [1.03] - Actual position '$actual_pos' is in range of target position '$target_pos'"
        fi

    done


    print_message -e "[epos4] [ppm] - Test 1.03 success \n"

# ---------------------------------------------------------------------------------------

    print_shit -e "[epos4] [ppm] - Homing inter test\n"
    homing
    target_pos=0
    set_drive_ppm

#-----------------------------------------------------------------------------------------------------------

    # test 1.04: Move the motor in PPM mode in negative roation, relative position
    print_shit -e "[epos4] [ppm] [1.04] - Setup PPM options"
    halcmd setp lcec.0.EPOS4.ppm.relative-pos     1
    halcmd setp lcec.0.EPOS4.ppm.endless-movement 0
    halcmd setp lcec.0.EPOS4.ppm.immediate-change 0


    print_shit -e "[epos4] [ppm] [1.04] - Set relative target : -50 "
    relative_target=-50
    halcmd setp mux.target.vector3.input0  $relative_target


    NB_MOV=4
    print_shit -n -e " - Movement done $NB_MOV times"
    for ((n = 1; n <= $NB_MOV; n++)); do
        target_pos=$((target_pos + relative_target))

        print_shit -e "[epos4] [ppm] [1.04] - Start motor"
        halcmd setp lcec.0.EPOS4.ppm.start 1

        print_shit -e "[epos4] [ppm] [1.04] - Wait end of movement"
        wait_idle

        print_shit -e "[epos4] [ppm] [1.04] - End of movement $n / $NB_MOV - check position"
        actual_pos=$(halcmd gets actual_pos)
        if ! in_range "$actual_pos" "$target_pos" 0.002; then
            echo -e ""
            echo -e "[epos4] WARNING - actual position '$actual_pos' is not in range of target position '$target_pos'"

        else
            print_shit -e "[epos4] [ppm] [1.04] - Actual position '$actual_pos' is in range of target position '$target_pos'"
        fi

    done


    print_message -e "[epos4] [ppm] - Test 1.04 success "

# ---------------------------------------------------------------------------------------

    print_shit -e "[epos4] [ppm] - Homing inter test"
    homing
    target_pos=0
    set_drive_ppm

#-----------------------------------------------------------------------------------------------------------

    # test 1.05: Move the motor in PPM mode in absolute position with 'immediate change target mode' option
    print_shit -e "[epos4] [ppm] [1.05] - Initialise absolute position "
    halcmd setp lcec.0.EPOS4.ppm.relative-pos     0
    halcmd setp lcec.0.EPOS4.ppm.endless-movement 0
    halcmd setp lcec.0.EPOS4.ppm.immediate-change 0

    print_shit -e "[epos4] [ppm] [1.05] - Set target in position 200"
    target_pos=200
    halcmd setp mux.target.vector3.input0  $target_pos

    print_shit -e "[epos4] [ppm] [1.05] - Start motor"
    halcmd setp lcec.0.EPOS4.ppm.start 1

    print_shit -e "[epos4] [ppm] [1.05] - Wait 1s"
    sleep 1

    print_shit -e "[epos4] [ppm] [1.05] - Set target in position 100"
    target_pos=100
    halcmd setp mux.target.vector3.input0  $target_pos

    print_shit -e "[epos4] [ppm] [1.05] - Start motor"
    halcmd setp lcec.0.EPOS4.ppm.start 1

    print_shit -e "[epos4] [ppm] [1.05] - Wait for the motor to stop and check if the motor as reached 200"
    wait_idle --reached 190

    sleep 1
    print_shit -e "[epos4] [ppm] [1.05] - End of movement - check position"
    actual_pos=$(halcmd gets actual_pos)
    if ! in_range "$actual_pos" "$target_pos" 0.002; then
        echo -e ""
        echo -e "[epos4] WARNING - actual position '$actual_pos' is not in range of target position '$target_pos'"

    else
        print_shit -e " - Actual position '$actual_pos' is in range of target position '$target_pos'"
    fi

    print_shit -e "[epos4] [ppm] [1.05]  - Homing"
    homing
    set_drive_ppm


    print_shit -e "[epos4] [ppm] [1.05] - Initialize immediate change"
    halcmd setp lcec.0.EPOS4.ppm.relative-pos     0
    halcmd setp lcec.0.EPOS4.ppm.endless-movement 0
    halcmd setp lcec.0.EPOS4.ppm.immediate-change 1

    print_shit -e "[epos4] [ppm] [1.05] - Set target in position 200"
    target_pos=200
    halcmd setp mux.target.vector3.input0  $target_pos

    print_shit -e "[epos4] [ppm] [1.05] - Start motor"
    halcmd setp lcec.0.EPOS4.ppm.start 1

    print_shit -e "[epos4] [ppm] [1.05] - Wait 1s"
    sleep 1

    print_shit -e "[epos4] [ppm] [1.05] - Set target in position 100"
    target_pos=100
    halcmd setp mux.target.vector3.input0  $target_pos

    print_shit -e "[epos4] [ppm] [1.05] - Start motor"
    halcmd setp lcec.0.EPOS4.ppm.start 1

    print_shit -e "[epos4] [ppm] [1.05] - Wait for the motor to stop and check if the motor as reached 200"
    wait_idle --reached 190


    print_shit -e "[epos4] [ppm] [1.05] - Check position"
    actual_pos=$(halcmd gets actual_pos)
    if ! in_range "$actual_pos" "$target_pos" 0.002; then
        echo -e ""
        echo -e "[epos4] WARNING - actual position '$actual_pos' is not in range of target position '$target_pos'"

    else
        print_shit -e "[epos4] [ppm] - Actual position '$actual_pos' is in range of target position '$target_pos'"
    fi

    print_message -e "[epos4] [ppm] - Test 1.05 success"

# ---------------------------------------------------------------------------------------

    print_shit -e "[epos4] [ppm] - Homing inter test"
    homing
    target_pos=0
    set_drive_ppm

#-----------------------------------------------------------------------------------------------------------

#     # test 1.06: Move the motor in PPM mode in relative position with 'immediate change target mode' option

    print_message -e "[epos4] [ppm] [1.06] - Initialise relative position "
    halcmd setp lcec.0.EPOS4.ppm.relative-pos     1
    halcmd setp lcec.0.EPOS4.ppm.endless-movement 0
    halcmd setp lcec.0.EPOS4.ppm.immediate-change 0

    print_message -e "[epos4] [ppm] [1.06] - Set target in position 200"
    relative_target=200
    actual_pos=$(halcmd gets actual_pos)
    target_pos="$relative_target"+"$actual_pos"
    halcmd setp mux.target.vector3.input0  $relative_target

    print_message -e "[epos4] [ppm] [1.06] - Start motor"
    halcmd setp lcec.0.EPOS4.ppm.start 1

    print_message -e "[epos4] [ppm] [1.06] - Wait 1s"
    sleep 1

    print_message -e "[epos4] [ppm] [1.06] - Set target in position 50"
    relative_target=50
    actual_pos=$(halcmd gets actual_pos)
    target_pos=$(awk "BEGIN {print $relative_target + $target_pos}")
    halcmd setp mux.target.vector3.input0  $relative_target

    print_message -e "[epos4] [ppm] [1.06] - Start motor"
    halcmd setp lcec.0.EPOS4.ppm.start 1

    print_message -e "[epos4] [ppm] [1.06] - Wait for the motor to stop and check if the motor as reached 250"
    wait_idle --reached 240

    print_message -e "[epos4] [ppm] [1.06]  - End of movement - check position"
    actual_pos=$(halcmd gets actual_pos)
    if ! in_range "$actual_pos" "$target_pos" 0.002; then
        echo -e ""
        echo -e "[epos4] WARNING - actual position '$actual_pos' is not in range of target position '$target_pos'"

    else
        print_shit -e " - Actual position '$actual_pos' is in range of target position '$target_pos'"
    fi

    print_message -e "[epos4] [ppm] [1.06] - Homing"
    homing
    target_pos=0
    set_drive_ppm


    print_message -e "[epos4] [ppm] [1.06] - Initialize immediate change"
    halcmd setp lcec.0.EPOS4.ppm.relative-pos     1
    halcmd setp lcec.0.EPOS4.ppm.endless-movement 0
    halcmd setp lcec.0.EPOS4.ppm.immediate-change 1

    print_message -e "[epos4] [ppm] [1.06] - Set target in position 200"
    relative_target=200
    actual_pos=$(halcmd gets actual_pos)
    target_pos=$(awk "BEGIN {print $relative_target + $actual_pos}")
    halcmd setp mux.target.vector3.input0  $relative_target

    print_message -e "[epos4] [ppm] [1.06] - Start motor"
    halcmd setp lcec.0.EPOS4.ppm.start 1

    print_message -e "[epos4] [ppm] [1.06] - Wait 1s"
    sleep 1

    print_message -e "[epos4] [ppm] [1.06] - Set target in position 50"
    relative_target=50
    actual_pos=$(halcmd gets actual_pos)
    target_pos=$(awk "BEGIN {print $relative_target + $actual_pos}")
    halcmd setp mux.target.vector3.input0  $relative_target

    print_message -e "[epos4] [ppm] [1.06] - Start motor"
    halcmd setp lcec.0.EPOS4.ppm.start 1

    print_message -e "[epos4] [ppm] [1.06] - Wait for the motor to stop and check if the motor as reached 200"
    wait_idle --reached 190


    print_message -e "[epos4] [ppm] [1.06] - Check position"
    actual_pos=$(halcmd gets actual_pos)
    if ! in_range "$actual_pos" "$target_pos" 0.25; then
        echo -e ""
        echo -e "[epos4] WARNING - actual position '$actual_pos' is not in range of target position '$target_pos'"

    else
        print_shit -e "[epos4] [ppm] - Actual position '$actual_pos' is in range of target position '$target_pos'"
    fi

    print_message -e "[epos4] [ppm] - Test 1.06 success \n"

# ---------------------------------------------------------------------------------------

    print_shit -e "[epos4] [ppm] - Homing inter test"
    homing
    set_drive_ppm

# ---------------------------------------------------------------------------------------

    # Test 1.07 : Stop motor durning movement
    print_message -n "[epos4] [ppm] [1.07] - Initialise PPM options to 0 "
    halcmd setp lcec.0.EPOS4.ppm.relative-pos     0
    halcmd setp lcec.0.EPOS4.ppm.endless-movement 0
    halcmd setp lcec.0.EPOS4.ppm.immediate-change 0

    print_message -e "[epos4] [ppm] [1.07] - Set target in position 200"
    target_pos=200
    halcmd setp mux.target.vector3.input0  $target_pos

    print_message -e "[epos4] [ppm] [1.07] - Start motor"
    halcmd setp lcec.0.EPOS4.ppm.start 1

    print_message -e "[epos4] [ppm] [1.07] - Wait 3s"
    sleep 3

    print_message -e "[epos4] [ppm] [1.07] - Stop motor"
    halcmd setp lcec.0.EPOS4.ppm.stop 1

    print_message -e "[epos4] [ppm] [1.07] - Wait 3s"
    sleep 3

    print_message -e "[epos4] [ppm] [1.07] - Check idle"
    if [ "$(halcmd getp lcec.0.EPOS4.idle)" == "1" ]; then
        print_message -e "[epos4] [ppm] - Test 1.07 success"
    else
        print_message -e "[epos4] [ppm] - Test 1.07 failed"
        set_drive_inactive
        exit 1
    fi

# ---------------------------------------------------------------------------------------

    print_shit -e "[epos4] [ppm] - Homing inter test\n"
    homing
    set_drive_ppm

# ---------------------------------------------------------------------------------------

    # Test 1.08 : Stop motor durning movement, direct restart
    print_message -n "[epos4] [ppm] [1.08] - Initialise PPM options to 0 "
    halcmd setp lcec.0.EPOS4.ppm.relative-pos     0
    halcmd setp lcec.0.EPOS4.ppm.endless-movement 0
    halcmd setp lcec.0.EPOS4.ppm.immediate-change 0

    print_message -e "[epos4] [ppm] [1.08] - Set target in position 200"
    target_pos=200
    halcmd setp mux.target.vector3.input0  $target_pos

    print_message -e "[epos4] [ppm] [1.08] - Start motor"
    halcmd setp lcec.0.EPOS4.ppm.start 1

    print_message -e "[epos4] [ppm] [1.08] - Wait 3s"
    sleep 3

    print_message -e "[epos4] [ppm] [1.08] - Stop motor"
    halcmd setp lcec.0.EPOS4.ppm.stop 1

    print_message -e "[epos4] [ppm] [1.08] - Restart motor"
    halcmd setp lcec.0.EPOS4.ppm.start 1

    print_message -e "[epos4] [ppm] [1.08] - Wait idle"
    wait_idle

    if ! in_range "$actual_pos" "$target_pos" 0.25; then
        echo -e ""
        echo -e "[epos4] ERROR - actual position '$actual_pos' is not in range of target position '$target_pos'"
        exit 1
    else
        print_shit -e "[epos4] [ppm] [1.08] - Actual position '$actual_pos' is in range of target position '$target_pos'"
    fi

    print_message -e "[epos4] [ppm] - Test 1.08 success"

    set_drive_inactive
    echo -e "[epos4] [ppm] - Test PPM finished successfully"
    return 0

}