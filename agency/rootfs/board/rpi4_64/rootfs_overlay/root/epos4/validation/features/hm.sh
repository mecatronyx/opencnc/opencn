#!/bin/bash

#################################################################################
# This file is part of a validation folder for the openCN connected to an EPOS4 #
# controller. It is the validation of Homing mode.                              #
#                                                                               #
# Author: Sutterlet Bruno                                                       #
# From a script by: Miceli Jean-Pierre                                          #
#                                                                               #
# Version: 1.0                                                                  #
# Date: 2023-11-22                                                              #
#################################################################################

# Local Variables

# Functions

# Source functions
source "$(dirname "$0")/functions.sh"

# local functions

#-- Main --#
function test_hm()
{

    print_message -e "[epos4] [hm] - Start Homing mode test"
    # Test 0.00 : Check if the motor is in Homing mode after set
    print_message -e "[epos4] [hm] [0.00] - Set Homing mode"
    if ! set_drive_hm; then
        print_message -e "[epos4] [hm] [0.00] - Failed to set Homing mode"
        echo -e "[epos4] ERROR - Test 0.00 failed"
        exit 1
    else
        print_message -e "[epos4] [hm] [0.00] - Test passed"
    fi


    # Test 0.01 : Check if the motor is in Homing mode before homing
    if [ "$(halcmd gets actual_pos | awk '{print $1}')" != "0" ]; then
        print_message -e "[epos4] [hm] [0.01] - Start homing"
        halcmd setp lcec.0.EPOS4.hm.start 1
        sleep 1
    else
        print_message -e "[epos4] [hm] [0.01] - Motor already homed"
        print_message -e "[epos4] [hm] [0.01] - Mooving motor to another position"

        print_shit -e "[epos4] [hm] [0.01] - set up mux input"
        halcmd setp mux.target.selector 3
        print_shit -e "[epos4] [hm] [0.01] - set mode ppm"
        set_drive_ppm
        print_shit -e "[epos4] [hm] [0.01] - set up PPM options"
        halcmd setp lcec.0.EPOS4.ppm.relative-pos     1
        halcmd setp lcec.0.EPOS4.ppm.endless-movement 0
        halcmd setp lcec.0.EPOS4.ppm.immediate-change 0
        print_shit -e "[epos4] [hm] [0.01] - set target to 100"
        halcmd setp mux.target.vector3.input0 100
        print_shit -e "[epos4] [hm] [0.01] - move"
        halcmd setp lcec.0.EPOS4.ppm.start 1
        sleep 1
        print_shit -e "[epos4] [hm] [0.01] - wait end of movement"
        wait_idle

        print_shit -e "[epos4] [hm] [0.01] - set HM back"
        set_drive_hm

        print_message -e "[epos4] [hm] [0.01] - Start homing"
        halcmd setp lcec.0.EPOS4.hm.start 1
        sleep 1

    fi

    print_shit -e "[epos4] - actual pos : $(halcmd gets actual_pos)"
    if [ "$(halcmd gets actual_pos)" -ne 0  ]; then
        print_message -e "[epos4] [hm] [0.01] - Failed to home motor: motor not at 0"
        echo -e "[epos4] ERROR - Test 0.01 failed"
        exit 1
    elif [ "$(halcmd getp lcec.0.EPOS4.homed)" == "0" ]; then
        print_message -e "[epos4] [hm] [0.01] - Failed to home motor: motor not homed"
        echo -e "[epos4] ERROR - Test 0.01 failed"
        exit 1
    else
        print_shit -e "[epos4] - DEBUG: hm start = $(halcmd getp lcec.0.EPOS4.hm.start)"
        print_shit -e "[epos4] - DEBUG: idle = $(halcmd getp lcec.0.EPOS4.idle)"
        print_shit -e "[epos4] - DEBUG: current pos = $(halcmd gets actual_pos)"

        print_message -e "[epos4] [hm] [0.01] - Test passed"
    fi

    print_message -e "[epos4] [hm] - set mode inactive"
    halcmd setp lcec.0.EPOS4.set-mode-inactive 1

    return 0
}