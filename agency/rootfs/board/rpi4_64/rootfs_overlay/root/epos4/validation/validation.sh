#!/bin/bash
################################################################################
# This is a test script designed to test the functionality of openCN paired    #
# with an EPOS4 controller. This is the main file of the validation folder.    #
#                                                                              #
# Author: Sutterlet Bruno                                                      #
# From a script by: Miceli Jean-Pierre                                         #
#                                                                              #
# Version: 1.0                                                                 #
# Date: 2023-11-16                                                             #
################################################################################

# Local Variables
export VERBOSE=true
export VERYVERBOSE=false


#-- Argument Handling --#
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -init)
            if halcmd -f /root/epos4/epos4.hal; then
                echo "HAL file loaded successfully."
            else
                echo "Failed to load HAL file. Check the file path or permissions."
                exit 1
            fi
            ;;
        -nv|--non-verbal)
            VERBOSE=false
            ;;
        -vv|--very-verbose)
            VERYVERBOSE=true
            ;;
        -h|--help)
            echo "Usage: validation.sh [options]"
            echo "Options:"
            echo "  -init               Initialize the HAL file
                                        WARNING: Use only once or it duplicates the HAL elements"
            echo "  -nv, --non-verbal   Disable verbose mode"
            echo "  -vv, --very-verbose Print a bunsh of shit"
            echo "  -h, --help          Show this help message"
            exit 0
            ;;
        *)
            echo "Unknown option: $1"
            exit 1
            ;;
    esac
    shift
done


#-- Main --#

# Print warning message
echo -e "ATTENTION: The motor will be moved during this test procedure"
echo -e "           Please make sure that the motor is not blocked"
echo -e ""

echo -e "This test procedure will take approximately 10 minutes to complete"

# Ask user to continue with Y/n (default: yes)
read -p "Continue? [Y/n] " -n 1 -r
echo -e ""

while [[ ! $REPLY =~ ^[Yy]$ ]] && [[ ! -z $REPLY ]] && [[ ! $REPLY =~ ^[Nn]$ ]]; do
    read -p "Continue? [Y/n] " -n 1 -r
    echo -e ""
done
if [[ $REPLY =~ ^[Nn]$ ]]; then
    echo -e "Test procedure aborted."
    exit 0
else
    echo -e "Test procedure started."
    echo -e ""
fi

# Initialize functions
source "$(dirname "$0")/functions.sh"
print_message -e "Functions initialized."
print_message -e

# HM mode
test_hm_mode() {
    read -p "Do you want to test HM mode? [Y/n] " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]] && [[ ! -z $REPLY ]] && [[ ! $REPLY =~ ^[Nn]$ ]]; then
        test_hm_mode
    fi

    if [[ $REPLY =~ ^[Nn]$ ]]; then
        echo -e "HM mode test skipped."
    else
        source "$(dirname "$0")/features/hm.sh"
        if ! test_hm; then
            echo -e "ERROR - HM mode test failed."
            exit 1
        fi
    fi
}

# PPM mode
test_ppm_mode() {
    read -p "Do you want to test PPM mode? [Y/n] " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]] && [[ ! -z $REPLY ]] && [[ ! $REPLY =~ ^[Nn]$ ]]; then
        test_ppm_mode
    fi

    if [[ $REPLY =~ ^[Nn]$ ]]; then
        echo -e "PPM mode test skipped."
    else
        source "$(dirname "$0")/features/ppm.sh"
        if ! test_ppm; then
            echo -e "ERROR - PPM mode test failed."
            exit 1
        fi
    fi
}

# PVM mode
test_pvm_mode() {
    read -p "Do you want to test PVM mode? [Y/n] " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]] && [[ ! -z $REPLY ]] && [[ ! $REPLY =~ ^[Nn]$ ]]; then
        test_pvm_mode
    fi

    if [[ $REPLY =~ ^[Nn]$ ]]; then
        echo -e "PVM mode test skipped."
    else
        source "$(dirname "$0")/features/pvm.sh"
        if ! test_pvm; then
            echo -e "ERROR - PVM mode test failed."
            exit 1
        fi
    fi
}

# CSP mode
test_csp_mode() {
    read -p "Do you want to test CSP mode? [Y/n] " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]] && [[ ! -z $REPLY ]] && [[ ! $REPLY =~ ^[Nn]$ ]]; then
        test_csp_mode
    fi

    if [[ $REPLY =~ ^[Nn]$ ]]; then
        echo -e "CSP mode test skipped."
    else
        source "$(dirname "$0")/features/csp.sh"
        if ! test_csp; then
            echo -e "ERROR - CSP mode test failed."
            exit 1
        fi
    fi
}

# CSV mode
test_csv_mode() {
    read -p "Do you want to test CSV mode? [Y/n] " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]] && [[ ! -z $REPLY ]] && [[ ! $REPLY =~ ^[Nn]$ ]]; then
        test_csv_mode
    fi

    if [[ $REPLY =~ ^[Nn]$ ]]; then
        echo -e "CSV mode test skipped."
    else
        source "$(dirname "$0")/features/csv.sh"
        if ! test_csv; then
            echo -e "ERROR - CSV mode test failed."
            exit 1
        fi
    fi
}

# CST mode
test_cst_mode() {
    read -p "Do you want to test CST mode? [Y/n] " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]] && [[ ! -z $REPLY ]] && [[ ! $REPLY =~ ^[Nn]$ ]]; then
        test_cst_mode
    fi

    if [[ $REPLY =~ ^[Nn]$ ]]; then
        echo -e "CST mode test skipped."
    else
        source "$(dirname "$0")/features/cst.sh"
        if ! test_cst; then
            echo -e "ERROR - CST mode test failed."
            exit 1
        fi
    fi
}

# other features
test_other_features() {
    read -p "Do you want to test other features? [Y/n] " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]] && [[ ! -z $REPLY ]] && [[ ! $REPLY =~ ^[Nn]$ ]]; then
        test_other_features
    fi

    if [[ $REPLY =~ ^[Nn]$ ]]; then
        echo -e "Other features test skipped."
    else
        source "$(dirname "$0")/features/other_features.sh"
        if ! test_other_feature; then
            echo -e "ERROR - Other features test failed."
            exit 1
        fi
    fi
}



# Call mode-specific test functions
test_hm_mode
test_ppm_mode
test_pvm_mode
test_csp_mode
test_csv_mode
#test_cst_mode
test_other_features

echo -e "Test procedure finished successfully."
