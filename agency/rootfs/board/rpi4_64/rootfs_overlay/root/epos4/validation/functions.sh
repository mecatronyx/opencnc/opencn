#!/bin/bash

#################################################################################
# This file is part of a validation folder for the openCN connected to an EPOS4 #
# controller. It is the generic function file for the validation folder.        #
# All the other files use the functions defined here.                           #
#                                                                               #
# Author: Sutterlet Bruno                                                       #
# From a script by: Miceli Jean-Pierre                                          #
#                                                                               #
# Version: 1.0                                                                  #
# Date: 2023-11-16                                                              #
#################################################################################


# -- Scripts constants/variables -- #

# mode related constant
INACTIVE_MODE=0
HM_MODE=1
PPM_MODE=2
PVM_MODE=3
CSP_MODE=4
CSV_MODE=5
CST_MODE=6

# -- Functions -- #

function print_shit() {
    local options=()

    # Parse options
    while [[ "$#" -gt 0 ]]; do
        case $1 in
            -n)
                options+=("-n")
                ;;
            -e)
                options+=("-e")
                ;;
            *)
                break
                ;;
        esac
        shift
    done

    if [ "$VERYVERBOSE" = true ]; then
        echo "${options[@]}" "$@"
    fi
}

function print_message() {
    local options=()

    # Parse options
    while [[ "$#" -gt 0 ]]; do
        case $1 in
            -n)
                options+=("-n")
                ;;
            -e)
                options+=("-e")
                ;;
            *)
                break
                ;;
        esac
        shift
    done

    if [ "$VERBOSE" = true ]; then
        echo "${options[@]}" "$@"
    fi
}

# Compare two floating-point numbers within a given tolerance
function in_range() {
    local value="$1"
    local target="$2"
    local range="$3"

    # Remove single quotes, if present
    value=$(echo "$value" | tr -d "'")
    target=$(echo "$target" | tr -d "'")

    local diff=$(awk -v v1="$value" -v v2="$target" 'BEGIN{print (v1 > v2) ? v1 - v2 : v2 - v1}')

    if (( $(echo "$diff <= $range" | bc) )); then
        return 0  # Inside the range
    else
        return 1  # Outside the range
    fi
}

function add_float() {
    local value="$1"
    local add="$2"

    echo $(awk "BEGIN { printf \"%.5f\", $value + $add }")
}

function check_position() {
    local actual_pos=$(halcmd gets actual_pos)
    local target_pos=$1

    if ! in_range "$actual_pos" "$target_pos" 0.002; then
        echo -e ""
        echo -e "[epos4] WARNING - actual position '$actual_pos' is not in range of target position '$target_pos'"

    else
        print_shit -e " - Actual position '$actual_pos' is in range of target position '$target_pos'"
    fi
}

function is_power_of_2()
{
    local num="$1"
    if [ "$num" -eq 0 ]; then
        return 1  # 0 is not a power of 2
    fi
    if [ "$((num & (num - 1)))" -eq 0 ]; then
        return 0  # num is a power of 2
    else
        return 1  # num is not a power of 2
    fi
}

function check_mode()
{
    local mask=$((1 << $1))

    # Retrieve the current state of various modes from the EPOS4 controller
    inactive=$(halcmd getp lcec.0.EPOS4.in-mode-inactive)
    hm=$(halcmd getp lcec.0.EPOS4.in-mode-hm)
    ppm=$(halcmd getp lcec.0.EPOS4.in-mode-ppm)
    pvm=$(halcmd getp lcec.0.EPOS4.in-mode-pvm)
    csp=$(halcmd getp lcec.0.EPOS4.in-mode-csp)
    csv=$(halcmd getp lcec.0.EPOS4.in-mode-csv)
    cst=$(halcmd getp lcec.0.EPOS4.in-mode-cst)

    # Create a bitmask representing the mode being checked
    mode=0
    mode=$(( mode | (inactive << INACTIVE_MODE)))
    mode=$(( mode | (hm  << HM_MODE)))
    mode=$(( mode | (ppm << PPM_MODE)))
    mode=$(( mode | (pvm << PVM_MODE)))
    mode=$(( mode | (csp << CSP_MODE)))
    mode=$(( mode | (csv << CSV_MODE)))
    mode=$(( mode | (cst << CST_MODE)))

    # Check if the wanted mode is well set
    if [ $((mode & mask)) -eq 0 ]; then
        echo "[epos4] ERROR - Wanted mode is not set"
        print_message " - mode: $mode - mask: $mask"
        return 1
    fi

    # Only one mode can be active!
    is_power_of_2 "$mode"
    if [ $? -ne 0 ]; then
        echo "[epos4] ERROR - Multiple modes set at the same time"
        return 1
    fi


    return 0
}

function set_drive_inactive()
{
    print_shit " - Set drive in 'INACTIVE' mode"
    halcmd setp lcec.0.EPOS4.set-mode-inactive 1
    sleep 0.1
    check_mode $INACTIVE_MODE
    if [ $? -ne 0 ]; then
        echo "[epos4] ERROR - drive not in 'INACTIVE' mode"
        exit 1
    fi

    return 0
}

function set_drive_hm()
{
    print_shit " - Set drive in 'HOMING' mode"
    halcmd setp lcec.0.EPOS4.set-mode-hm 1
    sleep 0.1
    check_mode $HM_MODE
    if [ $? -ne 0 ]; then
        echo "[epos4] ERROR - drive not in 'HOMING' mode"
        return 1
    fi

    return 0
}

function set_drive_ppm()
{
    print_shit " - Set drive in 'PPM' mode"
    halcmd setp lcec.0.EPOS4.set-mode-ppm 1
    sleep 0.1
    check_mode $PPM_MODE
    if [ $? -ne 0 ]; then
        echo "[epos4] ERROR - drive not in 'PPM' mode"
        return 1
    fi

    return 0
}

function set_drive_pvm()
{
    print_shit " - Set drive in 'PVM' mode"
    halcmd setp lcec.0.EPOS4.set-mode-pvm 1
    sleep 0.1
    check_mode $PVM_MODE
    if [ $? -ne 0 ]; then
        echo "[epos4] ERROR - drive not in 'PVM' mode"
        return 1
    fi

    return 0
}

function set_drive_cst()
{
    print_shit " - Set drive in 'CST' mode"
    halcmd setp lcec.0.EPOS4.set-mode-cst 1
    sleep 0.1
    check_mode $CST_MODE
    if [ $? -ne 0 ]; then
        echo "[epos4] ERROR - drive not in 'CST' mode"
        return 1
    fi

    return 0
}

function set_drive_csv()
{
    print_shit " - Set drive in 'CSV' mode"
    halcmd setp lcec.0.EPOS4.set-mode-csv 1
    sleep 0.1
    check_mode $CSV_MODE
    if [ $? -ne 0 ]; then
        echo "[epos4] ERROR - drive not in 'CSV' mode"
        return 1
    fi

    return 0
}

function set_drive_csp()
{
    print_shit " - Set drive in 'CSP' mode"
    halcmd setp lcec.0.EPOS4.set-mode-csp 1
    sleep 1
    check_mode $CSP_MODE
    if [ $? -ne 0 ]; then
        echo "[epos4] ERROR - drive not in 'CSP' mode"
        return 1
    fi

    return 0
}

function homing()
{
    set_drive_hm

    print_shit " - Start homing"

    halcmd setp lcec.0.EPOS4.hm.start 1

    sleep 1

    # check if is homed
    homed=$(halcmd getp lcec.0.EPOS4.homed)
    at0=$(halcmd gets actual_pos)

    if [ "$homed" == "1" ] && [ "$at0" == 0 ]; then
        print_shit " - Homing done"
        return 0
    else
        echo -e "ERROR - homing failed"
        return 1
    fi

}

wait_idle() {
    local watch_dog=false
    local watch_dog_time=
    local reached_pos=
    local reached=false
    local i=0

    # Parse command line options
    while [[ "$#" -gt 0 ]]; do
        case $1 in
            --watch_dog)
                watch_dog=true
                if [[ "$#" -ge 2 ]]; then
                    watch_dog_time=$2
                    shift 2
                else
                    echo "Error: --watch_dog option requires a time argument."
                    return 1
                fi
                ;;
            --reached)
                if [[ "$#" -ge 2 ]]; then
                    reached_pos=$2
                    shift 2
                else
                    echo "Error: --reached option requires a position argument."
                    return 1
                fi
                ;;
            *)
                echo "Unknown option: $1"
                return 1
                ;;
        esac
    done

    while true; do
        sleep 1
        print_message -n "."


        # Check if the --reached option is provided and compare positions
        if [ -n "$reached_pos" ]; then
            actual_pos=$(halcmd gets actual_pos)
            if [ "$(awk 'BEGIN {print ('$actual_pos' >= '$reached_pos')}')" -eq 1 ]; then
                reached=true
            fi
        fi

        idle=$(halcmd getp lcec.0.EPOS4.idle)
        if [ "$idle" == "1" ]; then
            print_message -e ""
            print_message -e "[epos4] Movement completed"
            actual_pos=$(halcmd gets actual_pos)
            print_message -e "[epos4] Actual position: '$actual_pos'"

            # Check if the --reached option is provided and compare positions
            if [ "$reached" == true ] && [ -n "$reached_pos" ]; then
                print_message -e "[epos4] Reached position: '$reached_pos'"
            elif [ "$reached" == false ] && [ -n "$reached_pos" ]; then
                print_message -e "[epos4] Didn't reached position: '$reached_pos'"
            fi

            # Check if the --watch_dog option is provided
            if [ "$watch_dog" == true ]; then
                if [ "$i" -ge "$watch_dog_time" ]; then
                    print_message -e "[epos4] Watch dog time reached"
                    return 1
                fi
                i=$((i+1))
            fi

            break
        fi
    done

    return 0
}

wait_simple_pg() {
    local watch_dog=false
    local watch_dog_time=
    local reached_pos=
    local reached=false
    local i=0

    # Parse command line options
    while [[ "$#" -gt 0 ]]; do
        case $1 in
            --watch_dog)
                watch_dog=true
                if [[ "$#" -ge 2 ]]; then
                    watch_dog_time=$2
                    shift 2
                else
                    echo "Error: --watch_dog option requires a time argument."
                    return 1
                fi
                ;;
            --reached)
                if [[ "$#" -ge 2 ]]; then
                    reached_pos=$2
                    shift 2
                else
                    echo "Error: --reached option requires a position argument."
                    return 1
                fi
                ;;
            *)
                echo "Unknown option: $1"
                return 1
                ;;
        esac
    done

    while true; do
        sleep 1
        print_message -n "."


        # Check if the --reached option is provided and compare positions
        if [ -n "$reached_pos" ]; then
            actual_pos=$(halcmd gets actual_pos)
            if [[ "$(awk 'BEGIN {print ('$actual_pos' >= '$reached_pos')}')" -eq 1 ]]; then
                reached=true
            fi
        fi

        idle=$(halcmd getp simple_pg.joint0.is_running || halcmd getp simple_pg.joint1.is_running || halcmd getp simple_pg.joint2.is_running)
        if [ "$idle" == "0" ]; then
            print_message -e ""
            print_message -e "[epos4] Movement completed"
            actual_pos=$(halcmd gets actual_pos)
            print_message -e "[epos4] Actual position: '$actual_pos'"

            # Check if the --reached option is provided and compare positions
            if [ "$reached" == true ] && [ -n "$reached_pos" ]; then
                print_message -e "[epos4] Reached position: '$reached_pos'"
            elif [ "$reached" == false ] && [ -n "$reached_pos" ]; then
                print_message -e "[epos4] Didn't reached position: '$reached_pos'"
            fi

            # Check if the --watch_dog option is provided
            if [ "$watch_dog" == true ]; then
                if [ "$i" -ge "$watch_dog_time" ]; then
                    print_message -e "[epos4] Watch dog time reached"
                    return 1
                fi
                i=$((i+1))
            fi

            break
        fi
    done

    return 0
}

