#!/bin/bash

#################################################################################
# This file is part of a validation folder for the openCN connected to an EPOS4 #
# controller. It is the validation of CST mode.                                 #
#                                                                               #
# Author: Sutterlet Bruno                                                       #
# From a script by: Miceli Jean-Pierre                                          #
#                                                                               #
# Version: 1.0                                                                  #
# Date: 2023-11-24                                                              #
#################################################################################

# Local Variables

# Functions

# Source functions
source "$(dirname "$0")/functions.sh"

#-- Main --#

function test_cst()
{
    local actual_pos
    local actual_torque
    local target_torque

    print_message -e "[epos4] [cst] - Start CST test"

    # Test 5.00 : Check if the motor is in CST mode after set
    print_message -e "[epos4] [cst] [5.00] - Set mux selector to 2"
    halcmd setp mux.target.selector 2

    print_message -e "[epos4] [cst] [5.00] - Homing"
    homing

    print_message -e "[epos4] [cst] [5.00] - Set target torque to 0"
    target_torque=0
    halcmd setp simple_pg.joint2.cmd-pos $target_torque

    # enable simple position generator
    print_message -e "[epos4] [cst] [5.00] - Enable simple position generator"
    halcmd setp simple_pg.enable 1
    sleep 1

    print_message -h "[epos4] [cst] [5.00] - Set CST"
    if ! set_drive_cst; then
        print_message -e "[epos4] [cst] [5.00] - Failed to set Homing mode"
        echo -e "[epos4] ERROR - Test 5.00 failed"
        exit 1
    else
        print_message -e "[epos4] [cst] [5.00] - Test passed"
        print_message -e ""
    fi

    # Test 5.01 : Move the motor in CST mode in positive roation
    print_message -e "[epos4] [cst] [5.01] - Set acceleration to 100"
    halcmd setp simple_pg.joint2.speed 100

    print_message -e "[epos4] [cst] [5.01] - Set target torque to 100"
    target_torque=100
    halcmd setp simple_pg.joint2.cmd-pos $target_torque

    print_message -e "[epos4] [cst] [5.01] - Wait for torque to be 100"
    wait_simple_pg

    print_message -e "[epos4] [cst] [5.01] - Set target torque to 0"
    target_torque=0
    halcmd setp simple_pg.joint2.cmd-pos $target_torque

    print_message -e "[epos4] [cst] [5.01] - Wait for torque to be 0"
    wait_simple_pg

    # Test 5.02 : Move the motor in CST mode in negative roation
    print_message -e "[epos4] [cst] [5.02] - Set target torque to -100"
    target_torque=-100
    halcmd setp simple_pg.joint2.cmd-pos $target_torque

    print_message -e "[epos4] [cst] [5.02] - Wait for torque to be -100"
    wait_simple_pg

    print_message -e "[epos4] [cst] [5.02] - Set target torque to 0"
    target_torque=0
    halcmd setp simple_pg.joint2.cmd-pos $target_torque

    print_message -e "[epos4] [cst] [5.02] - Wait for torque to be 0"
    wait_simple_pg



    print_message -e "[epos4] [cst] - Set motor inactive"
    set_drive_inactive

    echo -e "[epos4] [cst] - Test CST passed successfully"
    return 0
}