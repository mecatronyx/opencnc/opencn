#!/bin/bash

#################################################################################
# This file is part of a validation folder for the openCN connected to an EPOS4 #
# controller. It is the validation of all the untested features so far          #
#                                                                               #
# Author: Sutterlet Bruno                                                       #
# From a script by: Miceli Jean-Pierre                                          #
#                                                                               #
# Version: 1.0                                                                  #
# Date: 2023-11-16                                                              #
#################################################################################

# Local Variables

# Functions

# Source functions
source "$(dirname "$0")/functions.sh"

#-- Main --#

function test_other_feature()
{
    local is_error=0
    local is_idle=0
    local actual_pos=0.0

    print_message -e "[epos4] [other_feature] - Start other_feature test"

    # Test 7.02 : Quick stop
    print_message -e "[epos4] [other_feature] [7.02] - Set mux selector to 3"
    halcmd setp mux.target.selector 3

    print_message -e "[epos4] [other_feature] [7.02] - Init target position to 500"
    halcmd setp mux.target.vector3.input0 500

    print_message -e "[epos4] [other_feature] [7.02] - Init PPM options"
    halcmd setp lcec.0.EPOS4.ppm.relative-pos     0
    halcmd setp lcec.0.EPOS4.ppm.endless-movement 0
    halcmd setp lcec.0.EPOS4.ppm.immediate-change 0

    print_message -e "[epos4] [other_feature] [7.02] - Set PPM mode"
    set_drive_ppm

    print_message -e "[epos4] [other_feature] [7.02] - Start PPM"
    halcmd setp lcec.0.EPOS4.ppm.start 1

    print_message -e "[epos4] [other_feature] [7.02] - Wait 3s"
    sleep 3

    print_message -e "[epos4] [other_feature] [7.02] - Quick stop"
    halcmd setp lcec.0.EPOS4.quick-stop 1

    print_message -e "[epos4] [other_feature] [7.02] - Wait 3s"
    sleep 3

    is_idle=$(halcmd getp lcec.0.EPOS4.idle)
    is_error=$(halcmd getp lcec.0.EPOS4.in-fault)


    if [ "$is_error" == "0" ] && [ "$is_idle" == "1" ]; then
        print_message -e "[epos4] [other_feature] [7.02] - Test passed"
    else
        print_message -e "[epos4] [other_feature] [7.02] - Test failed"
        print_message -e "[epos4] [other_feature] [7.02] - is_error: $is_error"
        print_message -e "[epos4] [other_feature] [7.02] - is_idle: $is_idle"
        exit 1
    fi

    print_message -e "[epos4] [other_feature] [7.02] b - Init target position to 0"
    halcmd setp mux.target.vector3.input0 0

    print_message -e "[epos4] [other_feature] [7.02] b - Set PVM mode"
    set_drive_pvm

    print_message -e "[epos4] [other_feature] [7.02] b - Start PVM with -1500 rpm"
    halcmd setp mux.target.vector3.input0 -1500

    print_message -e "[epos4] [other_feature] [7.02] b - Wait 3s"
    sleep 3

    print_message -e "[epos4] [other_feature] [7.02] b - Quick stop"
    halcmd setp lcec.0.EPOS4.quick-stop 1

    print_message -e "[epos4] [other_feature] [7.02] b - Wait 3s"
    sleep 3

    is_idle=$(halcmd getp lcec.0.EPOS4.idle)
    is_error=$(halcmd getp lcec.0.EPOS4.in-fault)
    if [ "$is_error" == "0" ] && [ "$is_idle" == "1" ]; then
        print_message -e "[epos4] [other_feature] [7.02] b - Test passed"
    else
        print_message -e "[epos4] [other_feature] [7.02] b - Test failed"
        print_message -e "[epos4] [other_feature] [7.02] b - is_error: $is_error"
        print_message -e "[epos4] [other_feature] [7.02] b - is_idle: $is_idle"
        exit 1
    fi

    print_message -e "[epos4] [other_feature] [7.02] c - Set mux selector to 1"
    halcmd setp mux.target.selector 1

    print_message -e "[epos4] [other_feature] [7.02] c - Set CSV mode"
    set_drive_csv

    print_message -e "[epos4] [other_feature] [7.02] c - Enable simple profile generator"
    halcmd setp simple_pg.enable 1

    print_message -e "[epos4] [other_feature] [7.02] c - Set acceleration to 2000 rpm/s"
    halcmd setp simple_pg.joint1.speed 2000

    print_message -e "[epos4] [other_feature] [7.02] c - Start PVM with 1500 rpm"
    halcmd setp simple_pg.joint1.cmd-pos 1500

    print_message -e "[epos4] [other_feature] [7.02] c - Wait 3s"
    sleep 3

    print_message -e "[epos4] [other_feature] [7.02] c - Quick stop"
    halcmd setp lcec.0.EPOS4.quick-stop 1

    print_message -e "[epos4] [other_feature] [7.02] c - Wait 3s"
    sleep 3

    is_idle=$(halcmd getp lcec.0.EPOS4.idle)
    is_error=$(halcmd getp lcec.0.EPOS4.in-fault)
    if [ "$is_error" == "0" ] && [ "$is_idle" == "1" ]; then
        print_message -e "[epos4] [other_feature] [7.02] c - Test passed"
    else
        print_message -e "[epos4] [other_feature] [7.02] c - Test failed"
        print_message -e "[epos4] [other_feature] [7.02] c - is_error: $is_error"
        print_message -e "[epos4] [other_feature] [7.02] c - is_idle: $is_idle"
        exit 1
    fi

    print_message -e "[epos4] [other_feature] [7.02] c - REset acceleration to 300 rpm/s"
    halcmd setp simple_pg.joint1.speed 300

    print_message -e "[epos4] [other_feature] [7.02] c - Desable simple profile generator"
    halcmd setp simple_pg.enable 0

    # Test 7.04 - Modulo position
    print_message -e "[epos4] [other_feature] [7.04] - Homing"
    homing

    print_message -e "[epos4] [other_feature] [7.04] - Set modulo position to 1000"
    halcmd setp lcec.0.EPOS4.modulo 1000

    print_message -e "[epos4] [other_feature] [7.04] - Set PPM mode"
    set_drive_ppm

    print_message -e "[epos4] [other_feature] [7.04] - Set mux selector to 3"
    halcmd setp mux.target.selector 3

    print_message -e "[epos4] [other_feature] [7.04] - Start PPM to 1500"
    halcmd setp mux.target.vector3.input0 1500

    print_message -e "[epos4] [other_feature] [7.04] - Start PPM"
    halcmd setp lcec.0.EPOS4.ppm.start 1

    print_message -e "[epos4] [other_feature] [7.04] - Wait idle"
    wait_idle --watch_dog 30

    print_message -e "[epos4] [other_feature] [7.04] - Read position"
    actual_pos=$(halcmd gets actual_pos)
    if in_range $actual_pos 500 0.005; then
        print_message -e "[epos4] [other_feature] [7.04] - actual_pos: $actual_pos"
        print_message -e "[epos4] [other_feature] [7.04] - Test passed"
        print_message -e "[epos4] [other_feature] [7.04] - Set modulo position to 0"
    halcmd setp lcec.0.EPOS4.modulo 0
    else
        print_message -e "[epos4] [other_feature] [7.04] - Test failed"
        print_message -e "[epos4] [other_feature] [7.04] - actual_pos: $actual_pos"
        print_message -e "[epos4] [other_feature] [7.04] - Set modulo position to 0"
        halcmd setp lcec.0.EPOS4.modulo 0

        exit 1
    fi

    # Test 7.05 - Error detection
    print_message -e "[epos4] [other_feature] [7.05] - Homing"
    homing
    print_message -e "[epos4] [other_feature] [7.05] - Send CSP command of 0"
    halcmd setp mux.target.vector3.input0 0
    sleep 1


    print_message -e "[epos4] [other_feature] [7.05] - Set CSP mode"
    set_drive_csp

    print_message -e "[epos4] [other_feature] [7.05] - Set mux selector to 3"
    halcmd setp mux.target.selector 3

    print_message -e "[epos4] [other_feature] [7.05] - Send CSP command of 1500"
    halcmd setp mux.target.vector3.input0 15000
    sleep 1

    is_error=$(halcmd getp lcec.0.EPOS4.in-fault)
    if [ "$is_error" == "1" ]; then
        print_message -e "[epos4] [other_feature] [7.05] - Test passed"
    else
        print_message -e "[epos4] [other_feature] [7.02] - Quick stop"
        halcmd setp lcec.0.EPOS4.quick-stop 1
        sleep 3

        print_message -e "[epos4] [other_feature] [7.05] - Set mode inactive"
        set_drive_inactive

        print_message -e "[epos4] [other_feature] [7.05] - Test failed"
        print_message -e "[epos4] [other_feature] [7.05] - is_error: $is_error"
        exit 1
    fi

    # Test 7.06 - Error handling
    print_message -e "[epos4] [other_feature] [7.06] - Fault reset"
    halcmd setp lcec.0.EPOS4.fault-reset 1
    sleep 1

    print_message -e "[epos4] [other_feature] [7.06] - Check if drive is in fault"
    is_error=$(halcmd getp lcec.0.EPOS4.in-fault)
    if [ "$is_error" == "0" ]; then
        print_message -e "[epos4] [other_feature] [7.06] - Test passed"
    else
        print_message -e "[epos4] [other_feature] [7.06] - Test failed"
        print_message -e "[epos4] [other_feature] [7.06] - is_error: $is_error"
        exit 1
    fi

    print_message -e "[epos4] [other_feature] - Set mode inactive"
    set_drive_inactive

    print_message -e "[epos4] [other_feature] - Test passed successfully"
    return 0
}