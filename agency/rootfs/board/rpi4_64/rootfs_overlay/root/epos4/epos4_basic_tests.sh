#!/bin/bash

# Basic test of the epos4 drive. It is based on 'epos4.hal' config
#

# -- Scripts constants -- #

# mode related constant
INACTIVE_MODE=0
HM_MODE=1
PPM_MODE=2
PVM_MODE=3
CSP_MODE=4
CSV_MODE=5
CST_MODE=6

function is_power_of_2()
{
    local num="$1"
    if [ "$num" -eq 0 ]; then
        return 1  # 0 is not a power of 2
    fi
    if [ "$((num & (num - 1)))" -eq 0 ]; then
        return 0  # num is a power of 2
    else
        return 1  # num is not a power of 2
    fi
}

function check_mode()
{
    local mask=$((1 << $1))

    inactive=$(halcmd getp lcec.0.EPOS4.in-mode-inactive)
    hm=$(halcmd getp lcec.0.EPOS4.in-mode-hm)
    ppm=$(halcmd getp lcec.0.EPOS4.in-mode-ppm)
    pvm=$(halcmd getp lcec.0.EPOS4.in-mode-pvm)
    csp=$(halcmd getp lcec.0.EPOS4.in-mode-csp)
    csv=$(halcmd getp lcec.0.EPOS4.in-mode-csv)
    cst=$(halcmd getp lcec.0.EPOS4.in-mode-cst)

    mode=0
    mode=$(( mode | (inactive << INACTIVE_MODE)))
    mode=$(( mode | (hm  << HM_MODE)))
    mode=$(( mode | (ppm << PPM_MODE)))
    mode=$(( mode | (pvm << PVM_MODE)))
    mode=$(( mode | (csp << CSP_MODE)))
    mode=$(( mode | (csv << CSV_MODE)))
    mode=$(( mode | (cst << CST_MODE)))

    # Check if the wanted mode is well set
    if [ $((mode & mask)) -eq 0 ]; then
        echo "[epos4] ERROR - Wanted mode is not set"
        echo "[epos4] mode: $mode - mask: $mask"
        retrun 1
    fi

    # Only on mode can be active !
    is_power_of_2 "$mode"
    if [ $? -ne 0 ]; then
        echo "[epos4] ERROR - Multiple mode set at the same time"
        return 1
    fi

    return 0
}

function set_drive_inactive()
{
    echo -e "[epos4] Set drive in 'INACTIVE' mode"
    halcmd setp lcec.0.EPOS4.set-mode-inactive 1
    sleep 0.1
    check_mode $INACTIVE_MODE
    if [ $? -ne 0 ]; then
        echo "[epos4] ERROR - drive not in 'HOMING' mode"
        return 1
    fi

    return 0
}

function test_homing()
{
    echo -e "[epos4] == Starting 'HOMING' test procedure =="
    local failed=true

    echo -e "[epos4] Set the drive in homing mode"
    halcmd setp lcec.0.EPOS4.set-mode-hm 1
    sleep 0.1
    check_mode $HM_MODE
    if [ $? -ne 0 ]; then
        echo "[epos4] ERROR - drive not in 'HOMING' mode"
        return 1
    fi

    echo -e "[epos4] Start Homing process"
    halcmd setp lcec.0.EPOS4.hm.start 1

    echo -n "[epos4] Wait end of homing procedure"
    for ((i = 0; i < 10; i++)); do
        sleep 1
        echo -n "."

        homed=$(halcmd getp lcec.0.EPOS4.homed)
        if [ "$homed" == "1" ]; then
            # Drive is homed - check if the machine is idle or not
            idle=$(halcmd getp lcec.0.EPOS4.idle)
            if [ "$idle" == "0" ]; then
                echo -e ""
                echo "[epos4] ERROR - drive is 'homed' but the drive is not idle"
                return 1
            else
                echo -e ""
                echo -e "[epos4] homed procedure completed"
                failed=false
                break
            fi
        fi
    done
    echo -e ""

    if $failed; then
        echo "[epos4] ERROR - HOMING procedure never stopped !"
        return 1
    fi
}


function test_ppm()
{
    echo -e "[epos4] == Starting 'PPM' test procedure =="
    local failed=true

    echo -e "[epos4] Set target mux to external inputs"
    halcmd setp mux.target.selector 3

    halcmd setp lcec.0.EPOS4.ppm.immediate-change 0
    halcmd setp lcec.0.EPOS4.ppm.relative-pos     0

    echo -e "[epos4] Set the drive in ppm mode"
    halcmd setp lcec.0.EPOS4.set-mode-ppm 1
    sleep 0.1
    check_mode $PPM_MODE
    if [ $? -ne 0 ]; then
        echo -e "[epos4] ERROR - drive not in 'PPM' mode"
        return 1
    fi

    echo -e "[epos4] Start processing - target = 10"
    halcmd setp mux.target.vector3.input0  10
    halcmd setp lcec.0.EPOS4.ppm.start 1

    sleep 1
    echo -e "[epos4] Check if PPM procedure is active"
    idle=$(halcmd getp lcec.0.EPOS4.idle)
    if [ "$idle" == "1" ]; then
        echo  -e "[epos4] ERROR - drive is idle during ppm processing "
        return 1
    fi

    echo -n "[epos4] Wait end of movement"
    for ((i = 0; i < 30; i++)); do
        sleep 1
        echo -n "."

        idle=$(halcmd getp lcec.0.EPOS4.idle)
        if [ "$idle" == "1" ]; then
            echo -e ""
            echo -e "[epos4] movement completed"
            actual_pos=$(halcmd gets actual_pos)
            echo -e "[epos4] actual-pos: '$actual_pos' - target pos: 10"
            break
        fi
    done

    # check 'relative positions'
    echo -e "[epos4] Test 'relative position' mode"
    halcmd setp lcec.0.EPOS4.ppm.relative-pos 1
    halcmd setp mux.target.vector3.input0 5
    halcmd setp lcec.0.EPOS4.ppm.start 1

    echo -n "[epos4] Wait end of movement"
    for ((i = 0; i < 30; i++)); do
        sleep 1
        echo -n "."

        idle=$(halcmd getp lcec.0.EPOS4.idle)
        if [ "$idle" == "1" ]; then
            echo -e ""
            echo -e "[epos4] movement completed"
            actual_pos=$(halcmd gets actual_pos)
            echo -e "[epos4] actual-pos: '$actual_pos' - target pos: 15"
            break
        fi
    done

    # check 'immediate change target'

}


# function test_pvm()
# {

# }

# -- Main -- #

echo -e "## -- ----------------------------- -- ## "
echo -e "## -- Starting epo4 Tests procedure -- ## "
echo -e "## -- ----------------------------- -- ## "


# Setup environment
echo -e "[epos4] Load HAL file"
halcmd -f /root/epos4/epos4.hal

sleep 5

echo -e "[epos4] System just started. The mode should be 'inactive'"
check_mode $INACTIVE_MODE
if [ $? -ne 0 ]; then
    echo "[epos4] ERROR - Check if inactive mode failed"
    exit 1
fi

test_homing
if [ $? -ne 0 ]; then
    echo "[epos4] ERROR - HOMING test procedure failed"
    exit 1
fi

set_drive_inactive
if [ $? -ne 0 ]; then
    echo "[epos4] ERROR - Set drive in 'INACTIVE' mode failed"
    exit 1
fi

test_ppm
if [ $? -ne 0 ]; then
    echo "[epos4] ERROR - PPM test procedure failed"
    exit 1
fi

echo -e "## -- ------------------------- ## "
echo -e "## -- Completed successfully -- ## "
echo -e "## -- ------------------------- ## "
