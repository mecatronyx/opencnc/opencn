#!/bin/bash

#mount hdd on USB1 for sampler
echo -e "Mounting nvme0n1p1 for sampler"
mkdir -p /media/hdd
mount /dev/nvme0n1p1 /media/hdd

# Script to setup opencn environment for the Micro5  
echo -e "Loading OpenCN configuration... "
/usr/local/bin/halcmd -f /root/micro5/micro5.hal

echo "OK"

