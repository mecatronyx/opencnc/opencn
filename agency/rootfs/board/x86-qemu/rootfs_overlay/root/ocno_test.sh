#!/bin/bash
# OCNO testing script

## Helper functions ===========================================================

function print_msg_exit()
{
	echo $1
	exit -1
}


function ocno_is_inactive_mode()
{
	local res=$(/root/halcmd getp ocno.in-machine-inactive)
	if [ $res == 0 ]; then
		# echo "Machine is not in Inactive mode"
		return 1
	else
		# echo "Machine is in 'inactive' mode"
		return 0
	fi
}

function ocno_is_homing_mode()
{
	local res=$(/root/halcmd getp ocno.in-machine-homing)
	if [ $res == 0 ]; then
		# echo "Machine is not in Homing mode"
		return 1
	else
		# echo "Machine is in 'Homing' mode"
		return 0
	fi
}

function ocno_is_jog_mode()
{
	local res=$(/root/halcmd getp ocno.in-machine-jog)
	if [ $res == 0 ]; then
		# echo "Machine is not in Jog mode"
		return 1
	else
		# echo "Machine is in 'Jog' mode"
		return 0
	fi
}

function ocno_is_machining_mode()
{
	local res=$(/root/halcmd getp ocno.in-machine-machining)
	if [ $res == 0 ]; then
		# echo "Machine is not in Machining mode"
		return 1
	else
		# echo "Machine is in 'Machining' mode"
		return 0
	fi
}

function ocno_check_inactive_mode()
{
	ocno_is_inactive_mode  || print_msg_exit "[ERROR] Machine not in 'Inactive' mode"
	ocno_is_homing_mode    && print_msg_exit "[ERROR] 'Homing' mode is also set (only 'Inactive' mode expected) ! "
	ocno_is_jog_mode       && print_msg_exit "[ERROR] 'JOG' mode is also set (only 'Inactive' mode expected) ! "
	ocno_is_machining_mode && print_msg_exit "[ERROR] 'Machining' mode is also set (only 'Inactive' mode expected) ! "
}

function ocno_check_homing_mode()
{
	ocno_is_homing_mode    || print_msg_exit "[ERROR] Machine not in 'Homing' mode"
	ocno_is_inactive_mode  && print_msg_exit "[ERROR] 'Inactive' mode is also set (only 'Homing' mode expected) !"
	ocno_is_jog_mode       && print_msg_exit "[ERROR] 'JOG' mode is also set (only 'Homing' mode expected) !"
	ocno_is_machining_mode && print_msg_exit "[ERROR] 'Machining' mode is also set (only 'Homing' mode expected) !"
}

function ocno_check_jog_mode()
{
	ocno_is_jog_mode       || print_msg_exit "[ERROR] Machine not in 'JOG' mode"
	ocno_is_inactive_mode  && print_msg_exit "[ERROR] 'Inactive' mode is also set (only 'JOG' mode expected) !"
	ocno_is_homing_mode    && print_msg_exit "[ERROR] 'Homing' mode is also set (only 'JOG' mode expected) !"
	ocno_is_machining_mode && print_msg_exit "[ERROR] 'Machining' mode is also set (only 'JOG' mode expected) !"
}

function ocno_check_machining_mode()
{
	ocno_is_machining_mode || print_msg_exit "[ERROR] Machine not in 'Machining' mode"
	ocno_is_inactive_mode  && print_msg_exit "[ERROR] 'Inactive' mode is also set (only 'Machining' mode expected) !"
	ocno_is_homing_mode    && print_msg_exit "[ERROR] 'Homing' mode is also set (only 'Machining' mode expected) !"
	ocno_is_jog_mode       && print_msg_exit "[ERROR] 'JOG' mode is also set (only 'Machining' mode expected) !"
}

# Check axis 'homed' state - expected state passed as argument
function ocno_axis_homed_state_check()
{
	local homed=$(/root/halcmd gets homed)
	[ "$homed" != "$1" ] && print_msg_exit "[ERROR] 'homed' PIN is not equal to $1 (value: $homed"
}

function ocno_axis_position_check()
{
	local pos=$(/root/halcmd gets joint-pos)
	[ "$pos" != "$1" ] && print_msg_exit "[ERROR] Axis not at position $1 (current position: $pos)"
}

function ocno_jog_is_running()
{
	local res=$(/root/halcmd gets csp-running)
	if [ $res == 1 ]; then
		return 1
	else
		return 0
	fi
}


# Test to put the value in a variable
#ocno_is_inactive_mode
#if [ $? -eq 0 ]; then
#	echo "inactive"
#else
#	echo "not inactive"
#fi
#
## Test to put the value in a variable
#ocno_is_inactive_mode
#[ $? -eq 0 ] && echo "inactive" || echo "not inactive"
#
#ocno_is_inactive_mode && echo "inactive" || echo "not inactive"


# == Main Program =============================================================

# Make kernel logs in console - semi manual debug phase
# /root/logfile -d
# sleep 0.1

# Load Test HAL file
/root/halcmd -f ocno_dbg.hal
sleep 1

echo '==========================='
echo '== Starting OCNO testing =='
echo '==========================='
echo ''
## Inactive ##
# System just started - It has to be in 'Inactive' mode

echo "==> Machine just startup - check modes (expected to be in 'Inactive' mode)"
ocno_check_inactive_mode
echo "<== Machine in 'Inactive' mode - OK"
echo ''

# set in jog mode --> must stay in current mode (machine not homed yet)
echo "==> Try to set the Machine in 'JOG' mode"
echo "==> Expected to stay in 'Inactive' mode as machine is not homed yet"
/root/halcmd setp ocno.set-machine-jog 1
sleep 0.5
ocno_check_inactive_mode
echo "<== Machine in 'Inactive' mode - OK"
echo ''

## HOMING ##
echo "== Homing tests == "
echo ''
echo "==> Set the Machine in 'Homing' mode"
/root/halcmd setp ocno.set-machine-homing 1
sleep 0.5
ocno_check_homing_mode
echo "<== Machine in 'Homing' mode - OK"
echo ''

echo "==> Start Homing process"
/root/halcmd setp ocno.homing.start 1
sleep 5
echo "--> homing process running --> axis has to not be homed"
ocno_axis_homed_state_check 0
echo "<-- Axis not homed - OK"
sleep 10
echo "-- homing process completed --> axis has to be homed"
ocno_axis_homed_state_check 1
echo "<-- Axis homed - OK"
echo "<== Homing process Completed - OK"
echo ''

echo "==> Second Homing - Adding a 'clearing offset' of $clearing_offset"
clearing_offset=22.5
/root/halcmd setp ocno.homing.axisX.move-offset $clearing_offset
/root/halcmd setp ocno.homing.start 1
sleep 20
ocno_axis_homed_state_check 1
ocno_axis_position_check $clearing_offset
echo "<== Machine in 'Homed' and at $clearing_offset position - OK"
echo ''

## JOG ##
echo "== JOG tests == "
echo ''
echo "==> Set the Machine in 'JOG' mode"
/root/halcmd setp ocno.set-machine-jog 1
sleep 0.5
ocno_check_jog_mode
echo "<== Machine in 'JOG' mode - OK"
echo ''

jog_cmd_pos=100
echo "==> Start jog at position $jog_cmd_pos"
/root/halcmd setp ocno.jog.axisX.select 1
/root/halcmd setp ocno.jog.axisX.cmd-pos $jog_cmd_pos
/root/halcmd setp ocno.jog.start 1
sleep 0.1

while true
do
	# Display current axis position
	current_pos=$(/root/halcmd gets joint-pos)
	echo -ne " \r current position: ${current_pos}"

	ocno_jog_is_running
	if [ $? -eq 0 ]; then
		break
	fi
	sleep 0.2
done
echo "--> Jog completed - test current position"
ocno_axis_position_check $jog_cmd_pos
echo "<== JOG process Completed - OK"

## Fault generation ##
#echo "Fault case testing"
#/root/halcmd setp ocno.jog.axisX.active 1
#/root/halcmd setp ocno.jog.axisX.cmd-pos 1000
#sleep 1
#/root/halcmd setp plc.ocno_dbg.set-fault 1

echo '============================'
echo '== OCNO testing Completed =='
echo '============================'
echo ''
