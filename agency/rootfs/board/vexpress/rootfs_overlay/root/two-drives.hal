

# The threads period is in [us], contrary to the one defined for the
# Ethercat master in its xml, where it is in [ns]

load lcec cfg=two-drives.xml -d

load lcct
load feedopt

net feedopt-sampling-period lcec.0.TSD0.period => feedopt.sampling-period-ns

load streamer depth=200 cfg=fff
setp streamer.0.enable 0

load sampler --depth=200 --cfg=fffffffffffffffffffs

net feedopt-rt-active feedopt.rt-active => lcct.gcode.feedopt-rt-active
net feedopt-single-shot lcct.gcode.feedopt-single-shot => feedopt.rt-single-shot
net feedopt-ready feedopt.ready => lcct.gcode.feedopt-ready
net feedopt-rt-finished feedopt.rt-finished => lcct.gcode.feedopt-rt-finished
net feedopt-rt-underrun feedopt.rt-underrun => lcct.gcode.feedopt-rt-underrun
net feedopt-rt-start lcct.gcode.feedopt-rt-start => feedopt.rt-start
net feedopt-rt-reset lcct.gcode.feedopt-rt-reset => feedopt.opt-rt-reset
net feedopt-us-reset lcct.gcode.feedopt-us-reset => feedopt.opt-us-reset
net feedopt-rt-pause lcct.gcode.feedopt-rt-pause => feedopt.resampling.paused

net pos-cmd-{012} feedopt.sample-{012} => lcct.gcode.joint-pos-cmd-{012}
net start-homing-{0123} lcct.home.start-homing-{0123} => lcec.0.TSD{01}.do-homing-{01}
net stop-homing-{0123} lcct.home.stop-homing-{0123} => lcec.0.TSD{01}.do-stop-homing-{01}

net joint-pos-streamer-{012} streamer.0.pin.{012} => lcct.stream.joint-pos-streamer-{012}
net spindle-cmd-out lcct.spindle-cmd-out => lcec.0.TSD1.target-velocity-1
net spindle-cur-in lcec.0.TSD1.current-velocity-1 => lcct.spindle-cur-in
net spindle-feedopt-speed feedopt.spindle-target-speed => lcct.gcode.feedopt-spindle-speed

net joint-pos-cur-in-{0123} lcec.0.TSD{01}.current-position-{01} => lcct.joint-pos-cur-in-{0123}
net target-position-{0123} lcct.target-position-{0123} => lcec.0.TSD{01}.target-position-{01}

net in-mode-csp-{0123} lcec.0.TSD{01}.in-mode-csp-{01} => lcct.in-mode-csp-{0123}
net in-mode-csv-{0123} lcec.0.TSD{01}.in-mode-csv-{01} => lcct.in-mode-csv-{0123}
net in-mode-hm-{0123} lcec.0.TSD{01}.in-mode-hm-{01} => lcct.in-mode-hm-{0123}
net in-mode-inactive-{0123} lcec.0.TSD{01}.in-mode-inactive-{01} => lcct.in-mode-inactive-{0123}

net in-fault-{0123} lcec.0.TSD{01}.in-fault-{01} => lcct.in-fault-{0123}

net set-mode-csp-{0123} lcec.0.TSD{01}.set-mode-csp-{01} => lcct.set-mode-csp-{0123}
net set-mode-csv-{0123} lcec.0.TSD{01}.set-mode-csv-{01} => lcct.set-mode-csv-{0123}
net set-mode-hm-{0123} lcec.0.TSD{01}.set-mode-hm-{01} => lcct.set-mode-hm-{0123}
net set-mode-inactive-{0123} lcec.0.TSD{01}.set-mode-inactive-{01} => lcct.set-mode-inactive-{0123}

net fault-reset-{0123} lcct.fault-reset-{0123} => lcec.0.TSD{01}.fault-reset-{01}
net homed-{0123} lcec.0.TSD{01}.homed-{01} => lcct.home.homed-{0123}

net streamer-clock-out lcct.stream.streamer-clock => streamer.0.clock
net streamer-clock-mode-out lcct.stream.streamer-clock-mode => streamer.0.clock-mode
net streamer-enable lcct.stream.streamer-enable => streamer.0.enable
net streamer-empty streamer.0.empty => lcct.stream.streamer-empty

net sampler-enable lcct.sampler-enable-out => sampler.0.enable
net external-trigger lcct.external-trigger => lcec.0.TSD0.extra-rx-var1-0
net electrovalve lcct.electrovalve => lcec.0.TSD1.extra-rx-var1-0

net joint-pos-cur-in-{01} => sampler.0.pin.{01}
net joint-pos-cur-in-2 => sampler.0.pin.2
net pos-error-{01} lcec.0.TSD0.position-error-{01} => sampler.0.pin.{34}
net pos-error-2 lcec.0.TSD1.position-error-0 => sampler.0.pin.5
net actual-curr-{01} lcec.0.TSD0.extra-var0-{01} => sampler.0.pin.{67}
net actual-curr-2 lcec.0.TSD1.extra-var0-0 => sampler.0.pin.8
net desired-volt-0 lcec.0.TSD0.extra-var1-0 => sampler.0.pin.9
net desired-volt-1 lcec.0.TSD0.extra-var1-1 => sampler.0.pin.10
net desired-volt-2 lcec.0.TSD1.extra-var1-0 => sampler.0.pin.11
net desired-volt-3 lcec.0.TSD1.extra-var1-1 => sampler.0.pin.12
net path-interp-acc-0 lcec.0.TSD0.extra-var3-0 => sampler.0.pin.13
net path-interp-acc-1 lcec.0.TSD0.extra-var3-1 => sampler.0.pin.14
net path-interp-acc-2 lcec.0.TSD1.extra-var3-0 => sampler.0.pin.15
net path-interp-acc-3 lcec.0.TSD1.extra-var3-1 => sampler.0.pin.16
net spindle-cur-in => sampler.0.pin.17
net spindle-cmd-out => sampler.0.pin.18
net external-trigger => sampler.0.pin.19

addf streamer.0 lcec_thread.0
addf feedopt.update lcec_thread.0
addf lcct.update lcec_thread.0
addf sampler.0 lcec_thread.0

start
