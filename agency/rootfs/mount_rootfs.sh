#!/bin/bash
echo "-------------------mount rootfs ---------------"

if [ $# -ne 1 ]; then
        echo "Usage: ./mount_rootfs <board>"
	echo "Please provide the board name (vexpress, virt64, rpi4_64, rpi4, x86, x86-qemu)"
	exit 0
fi 
echo "Here: board is $1"
../../scripts/mount_cpio.sh $PWD/board/$1/rootfs.cpio


