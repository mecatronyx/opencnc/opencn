#!/bin/bash

if [ -z "$PLATFORM" ]; then
    if [ $# -ne 1 ]; then
        echo "Please provide the board name (vexpress, virt64, rpi4, rpi4_64, x86, x86-qemu, zcu106)"
        exit 0
    else
        PLATFORM=$1
    fi
fi

# Partition layout on the sdcard (vExpress for example):
# - Partition #1: 64 MB (u-boot, kernel, etc.)
# - Partition #2: Rest of available size (agency rootfs 1)

# Partition layout on a x86 system:
# - Partition #1: 8 GB

if [ "$PLATFORM" == "vexpress" -o "$PLATFORM" == "virt64" -o "$PLATFORM" == "x86-qemu" ]; then
    #create image first
    echo Creating sdcard.img.$PLATFORM ...

    dd_size=8G
    truncate -s $dd_size sdcard.img.$PLATFORM
    devname=$(sudo losetup --partscan --find --show sdcard.img.$PLATFORM)

    # Keep device name only without /dev/
    devname=${devname#"/dev/"}
fi

if [ "$PLATFORM" == "rpi4" -o "$PLATFORM" == "rpi4_64" -o "$PLATFORM" == "x86" -o "$PLATFORM" == "zcu106" -o "$PLATFORM" == "cm4_64" ]; then
    if [ -z "$devname" ]; then
        echo "Specify the MMC/USB device you want to deploy on (ex: sdb or mmcblk0 or other...)"
        read devname
    fi
fi

if [ "$PLATFORM" != "x86" -a "$PLATFORM" != "x86-qemu" ]; then
#create the partition layout this way
    (echo o; echo n; echo p; echo; echo; echo +64M; echo t; echo c; echo n; echo p; echo; echo; echo; echo w)   | sudo fdisk /dev/"$devname";
fi

if [ "$PLATFORM" == "x86" ]; then
  echo "Erasing partitions on "$devname""
  sudo dd if=/dev/zero of=/dev/"$devname" count=10k conv=notrunc || abort "Failed to erase "$devname""

  echo "Creating rootfs partition..."
  sudo parted -s /dev/"$devname" mklabel msdos mkpart primary ext4 1MiB 100%
fi

if [ "$PLATFORM" == "x86-qemu" ]; then
#create a unique partition this way
    (echo o; echo n; echo p; echo; echo; echo; echo; echo; echo w) | sudo fdisk /dev/"$devname";
fi

# Give a chance to the real SD-card to be sync'd
sleep 1s

if [[ "$devname" = *[0-9] ]]; then
    export devname="${devname}p"
fi

if [ "$PLATFORM" == "x86" -o "$PLATFORM" == "x86-qemu" ]; then
  sudo mke2fs -F -t ext4 /dev/"$devname"1
  sudo e2label /dev/"$devname"1 rootfs
fi

if [ "$PLATFORM" != "x86" -a "$PLATFORM" != "x86-qemu" ]; then
    sudo mkfs.vfat /dev/"$devname"1
    sudo mkfs.ext4 /dev/"$devname"2
    sudo fatlabel /dev/"$devname"1 BOOT
    sudo e2label /dev/"$devname"2 rootfs
fi

if [ "$PLATFORM" == "vexpress" -o "$PLATFORM" == "virt64" -o "$PLATFORM" == "x86" -o "$PLATFORM" == "x86-qemu" ]; then
    sudo losetup -D
fi
