#!/bin/bash

#Small script that aim to locally copy a file from /root

echo "Name of the file to recover in /root:"
read FILENAME


# auto-export all variables found in build.conf (POSIX compatible method)
set -o allexport
. "../build.conf"
set +o allexport

#Checking if a VM is running
if ps -A |grep -q qemu; then
    echo "A VM is running, please run 'sync' and 'shutdown' on it, aborting recover"
else
    #Recovering file
    if [ "$PLATFORM" == "x86-qemu" -o "$PLATFORM" == "x86" ]; then
        ./mount.sh 1 $PLATFORM
    else
        ./mount.sh 2 $PLATFORM
    fi

    #copy file locally
    sudo cp fs/root/$FILENAME $FILENAME

    # Check if the copy as gone well
    if [ $? ]; then
        ./umount.sh $PLATEFORM
        exit 1 
    fi

    #Set owner and group of the file to user
    sudo chown $USER:$USER $FILENAME
    
    ./umount.sh $PLATEFORM
    echo "Done"
fi

