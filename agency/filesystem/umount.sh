#!/bin/bash

if [ "$PLATFORM" == "" ]; then
    if [ "$1" == "" ]; then
        echo "PLATFORM must be defined (vexpress, virt64, rpi4, rpi4_64, x86, x86-qemu, zcu106)"
        echo "You can invoke mount.sh <partition_nr> <platform>"
        exit 0
    fi
    
    PLATFORM=$1
fi

sleep 1

sudo umount fs

# Let the filesystem be synchronized
sleep 1

if [ "$PLATFORM" == "vexpress" -o "$PLATFORM" == "virt64" -o "$PLATFORM" == "x86-qemu" ]; then
    sudo losetup -D
fi
