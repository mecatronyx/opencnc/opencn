/*
 * Copyright (C) 2014-2018 Daniel Rossier <daniel.rossier@heig-vd.ch>
 * Copyright (C) 2018 Baptiste Delporte <bonel@bonel.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#if 0
#define DEBUG
#endif

#include <memslot.h>
#include <memory.h>
#include <sched.h>
#include <keyhandler.h>
#include <domain.h>
#include <errno.h>
#include <types.h>

#include <asm/io.h>
#include <asm/percpu.h>

#include <asm/cacheflush.h>

#include <soo/uapi/debug.h>
#include <soo/uapi/soo.h>

#include <soo_migration.h>


/**
 * Return the descriptor of a domain (agency or ME).
 * A size of 0 means there is no ME in the slot.
 */
void get_dom_desc(unsigned int slotID, dom_desc_t *dom_desc) {

	/* Copy the content to the target desc */
	memcpy(dom_desc, &domains[slotID]->shared_info->dom_desc, sizeof(dom_desc_t));

}

/**
 * SOO hypercall processing.
 */
int do_soo_hypercall(soo_hyp_t *args) {
	int rc = 0;
	soo_hyp_t op;
	struct domain *dom;
	soo_hyp_dc_event_t *dc_event_args;

	/* Get argument from guest */
	memcpy(&op, args, sizeof(soo_hyp_t));

	/*
	 * Execute the hypercall
	 * The usage of args and returns depend on the hypercall itself.
	 * This has to be aligned with the guest which performs the hypercall.
	 */

	switch (op.cmd) {

	case AVZ_DC_SET:
		/*
		 * AVZ_DC_SET is used to assign a new dc_event number in the (target) domain shared info page.
		 * This has to be done atomically so that if there is still a "pending" value in the field,
		 * the hypercall must return with -BUSY; in this case, the caller has to busy-loop (using schedule preferably)
		 * until the field gets free, i.e. set to DC_NO_EVENT.
		 */
		dc_event_args = (soo_hyp_dc_event_t *) op.p_val1;

		dom = domains[dc_event_args->domID];
		BUG_ON(dom == NULL);

		/* The shared info page is set as non cacheable, i.e. if a CPU tries to update it, it becomes visible to other CPUs */
		if (atomic_cmpxchg(&dom->shared_info->dc_event, DC_NO_EVENT, dc_event_args->dc_event) != DC_NO_EVENT)
			rc = -EBUSY;

		break;

	default:
		printk("%s: Unrecognized hypercall: %d\n", __func__, op.cmd);
		BUG();
		break;
	}

	/* If all OK, copy updated structure to guest */
	memcpy(args, &op, sizeof(soo_hyp_t));

	flush_dcache_all();

	return rc;
}

int soo_activity_init(void) {

	DBG("Setting SOO avz up ...\n");

	return 0;
}

