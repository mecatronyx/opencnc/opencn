#!/bin/bash

# -- This script can be use to build the different components of OpenCN framework
# --
# -- NB: it does not provides option to build Host/GUI apps

# By default, no target is defined
target=''

# Define OpenCN items paths. It is defined relatively the path of this script
#   - 'build.sh' path is <OPENCN_HOME>/agency/build.sh

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
AGENCY=$SCRIPTPATH
ROOTFS=$SCRIPTPATH/rootfs
USR=$SCRIPTPATH/usr
QEMU=$SCRIPTPATH/../qemu
UBOOT=$SCRIPTPATH/../u-boot

# Setup some colors
RED='\033[0;31m'
NC='\033[0m' # No Color

# List of the supported targets
targets="rpi4_64 virt64 x86 x86-qemu cm4_64"

usage()
{
    echo "Usage: $0 [OPTIONS] "
    echo ""
    echo "Where OPTIONS are:"
    echo "  -a    build all components"
    echo "  -b    build u-boot and/or qemu depending on the target"
    echo "  -c    clean the selected components (no action if used alone)"
    echo "  -k    build linux kernel"
    echo "  -r    build rootfs (secondary)"
    echo "  -u    build user apps"
    echo "  -t    select the wanted target. See below for the supported targets are:"
    echo "        It has to be done at least once (creation of build.conf file)"
    echo ""
    echo " Supported targets: $targets"
    echo ""
    exit 1
}

check_target()
{
    for i in $targets; do
        if [ "$i" = "$1" ]; then
            # It is a valid target
            return
        fi
    done

    echo "ERROR: target '$1' is not supported. Valid targets are '$targets'"
    exit 1
}

set_config()
{
    if [ $1 ]; then
        check_target $1

        touch $AGENCY/build.conf
        echo "PLATFORM=$1" > $AGENCY/build.conf
        PLATFORM=$1
    elif [ ! -f "$AGENCY/build.conf" ]; then
        echo "ERROR: build.conf file does NOT exist, use -t option to select the wanted target"
        exit 1
    else
        echo "read target from the file"
        # auto-export all variables found in build.conf (POSIX compatible method)
        set -o allexport
        . $AGENCY/build.conf
        set +o allexport
    fi

    # The build process between 'rpi4_64' & 'cm4_64' is the same
    if [ "$PLATFORM" = "cm4_64" ]; then
        PLATFORM="rpi4_64"
    fi
}

build_uboot()
{
    echo "-- [ Build U-Boot ] -----------------------------------------"

    cd $UBOOT

    if [ "$2" = "y" ]; then
        echo "Clean"
        make distclean
    else
        echo "Config=$1"
        make $1 || exit 1
        make -j$(nproc)
    fi

    if [ $? -ne 0 ]; then
        printf "\n${RED}[ERROR] u-boot compilation failed !!${NC}\n"
        exit -1
    fi

    cd -
}

build_qemu()
{
    echo "-- [ Build qemu ] -------------------------------------------"

    cd $QEMU

    if [ "$2" = "y" ]; then
        make distclean
        cd -
        return
    fi

    if [ "$PLATFORM" = "virt64" ]; then
        ./configure --target-list=aarch64-softmmu --disable-attr --disable-werror --disable-docs || exit -1

    elif [ "$PLATFORM" = "x86-qemu" ]; then
        ./configure --target-list=x86_64-softmmu --enable-kvm || exit -1

    else
        echo "[ERROR] no qemu build for PLATFORM '$1'"
        return
    fi

    make -j$(nproc)

    if [ $? -ne 0 ]; then
        printf "\n${RED}[ERROR] qemu compilation failed !!${NC}\n"
        exit -1
    fi

    cd -
}

buid_kernel()
{
    echo "-- [ Build Linux kernel ] ----------------------------------"

    if [ "$1" = "y" ]; then
        echo "Clean"
        cd $AGENCY/linux
        make distclean
    else
        cd $AGENCY
        make
    fi

    if [ $? -ne 0 ]; then
        printf "\n${RED}[ERROR] kernel compilation failed !!${NC}\n"
        exit -1
    fi

    cd -
}

build_rootfs()
{
    echo "-- [ Build rootfs ] -----------------------------------------"

    cd $ROOTFS

    if [ "$2" = "y" ]; then
        echo "Clean"
        make distclean
    else
        echo "Config=$1_defconfig"
        make "$1_defconfig" || exit -1
        make source || exit -1
        make || exit -1
    fi

    cd -
}

build_usr()
{
    cd $USR

    echo "Build usr applications"

    if [ "$1" = "y" ]; then
        ./build.sh -c
    else
        ./build.sh
    fi

    cd -
}

while getopts "abckrut:" o; do
    case "$o" in
        a)
            buildBoot=y
            buildKernel=y
            buildRootfs=y
            buildUsr=y
            ;;
        b)
            buildBoot=y
            ;;
        c)
            clean=y
            ;;
        k)
            buildKernel=y
            ;;
        r)
            buildRootfs=y
            ;;
        u)
            buildUsr=y
            ;;
        t)
            target=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

if [ $OPTIND -eq 1 ]; then usage; fi

set_config $target

# Buid u-boot and/or qemu depending on the PLATFORM
if [ "$buildBoot" == "y" ]; then

    if [ "$PLATFORM" = "rpi4_64" ]; then
        build_uboot rpi4_64_defconfig $clean
    elif [ "$PLATFORM" = "virt64" ]; then
        build_uboot virt64_defconfig $clean
        build_qemu virt64 $clean
    elif [ "$PLATFORM" = "x86-qemu" ]; then
        build_qemu x86-qemu $clean
    fi
fi

if [ "$buildKernel" == "y" ]; then
    buid_kernel "$clean"
fi

if [ "$buildRootfs" == "y" ]; then
    build_rootfs "$PLATFORM" "$clean"
fi

if [ "$buildUsr" == "y" ]; then
    build_usr "$clean"
fi
