#!/bin/bash

SCRIPT_PATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
BUILD_DIR=$SCRIPT_PATH/build

# Create and initialize the associative array
declare -A install_paths=(
  ["bin"]="fs/usr/local/bin/"
  ["components"]="fs/etc/opencn/components"
)

# Deploy usr apps into the agency partition (second partition)
echo Deploying usr apps into the agency partition...
cd ../filesystem

if [ "$PLATFORM" = "x86-qemu" -o "$PLATFORM" = "x86" ]; then
./mount.sh 1
else
./mount.sh 2
fi

sudo cp -r ../usr/build/deploy/* fs/root/

# Loop over installation paths 
for folder in "${!install_paths[@]}"; do
	target_path=${install_paths[$folder]}

	sudo mkdir -p  $target_path
	sudo cp -r $BUILD_DIR/$folder/* $target_path

done

./umount.sh
cd -
