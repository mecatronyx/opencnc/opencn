#include <iostream>
#include <string>
#include <dirent.h>
#include <cerrno>
#include <vector>
#include <stdexcept>

#include "interp_return.hh"
#include "rs274ngc_interp.hh"
#include "ocn.h"

#define TEST_FILE       "test.ngc"
#define TEST_DIR        "test_files/"
#define OUT_DIR         "out_files/"
#define GCODE_EXT       ".ngc"
#define GCODE_EXT1      ".nc"

#define STDOUT_RESET    "\033[0m"
#define STDOUT_GREEN    "\033[32m"
#define STDOUT_RED      "\033[31m"
#define STDOUT_YELLOW   "\033[33m"
#define STDOUT_BOLD     "\033[1m"

void push(ocn::CurvStruct &curv)
{
    std::cout << __func__ << std::endl;
}

void find_file_in_directory(std::vector<std::string> &files)
{
    DIR *dir;
    struct dirent *diread;
    std::string file;

    if ((dir = opendir(TEST_DIR)) != nullptr)
    {
        while ((diread = readdir(dir)) != nullptr)
        {
            file = std::string(diread->d_name);

            if (file.find(GCODE_EXT) != std::string::npos)
                files.push_back(TEST_DIR + file);
            if (file.find(GCODE_EXT1) != std::string::npos)
                files.push_back(TEST_DIR + file);
        }
        closedir(dir);
    }
    else
    {
        std::string err = "opendir failed: " + std::string(strerror(errno));
        throw std::runtime_error(err);
    }
}

int read_file(std::string& err_str, std::string filepath = "")
{
    int err_r = 0, err_ex = 0, index = 0;
    std::string filename;
    Interp interp;
    interp.init();
    interp.set_queue_push_func(&push);
    err_str = "";

    if (filepath.empty()) 
    {
        filename = std::string(TEST_FILE);
    }
    else 
    {
        filename = std::string(filepath);
    }

    if (interp.open(filename.c_str()) != INTERP_OK) 
    {
        std::cout << "Failed to open file" << std::endl;
        return -1;
    }

    interp.set_loglevel(2);

    while (true)
    {
        err_r = interp.read();
        if (err_r == INTERP_ENDFILE)
            break;
        if ((err_r != INTERP_OK) && 
            (err_r != INTERP_EXECUTE_FINISH)) 
        {
            err_str = "Error reading line [" + std::to_string(interp.line()) + "] : " 
                    + interp.getSavedError();
            break;
        }

        err_ex = interp.execute();

        if ((err_ex != INTERP_OK) &&
            (err_ex != INTERP_EXIT) &&
            (err_ex != INTERP_EXECUTE_FINISH)) 
        {
            err_str = "Error executing line [" + std::to_string(interp.line()) + "] : " 
                    + interp.getSavedError();

            break;
        }
        else if (err_ex == INTERP_EXIT)
            break;
    }

    interp.close();

    if (err_r > INTERP_MIN_ERROR || err_ex > INTERP_MIN_ERROR)
    {
        return -1;
    }

    return 0;
}

int main(int argc, char const *argv[]) 
{   
    std::string err;
    std::vector<std::string> files;
    int count_errors = 0;

    find_file_in_directory(files);

    if (files.size() < 1)
    {
        std::cout << "No files found in directory " << TEST_DIR << std::endl;
        files.push_back(TEST_FILE);
    }

    for (int i = 0; i < files.size(); i++)
    {
        std::cout << "Reading file: " << files[i] << std::endl;

#ifdef VALUES_OUTPUT
        std::string out_file = files[i];
        out_file.erase(files[i].find(TEST_DIR), sizeof(TEST_DIR) - 1);
        out_file = OUT_DIR + out_file;
        set_output_file(out_file + ".out");
#endif
        if (read_file(err, files[i]) < 0)
        {
            std::cout << STDOUT_RED <<"Error: " << err << STDOUT_RESET << std::endl;
            count_errors++;
        }
        else
        {
            std::cout << STDOUT_GREEN << "No error" << STDOUT_RESET << std::endl;
        }

        std::cout << std::endl;
    }

    if (count_errors > 0)
        std::cout << STDOUT_RED; 
    else
        std::cout << STDOUT_GREEN;
        
    std::cout << STDOUT_BOLD << "Total errors: " << count_errors << STDOUT_RESET << std::endl;


    return 0;
}
