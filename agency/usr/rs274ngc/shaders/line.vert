#version 330 core
layout (location = 0) in vec3 aPos; // the position variable has attribute position 0
layout (location = 1) in vec3 aColor;
  
out vec4 vertexColor; // specify a color output to the fragment shader

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

out vec2 lineCenter;

uniform vec2 viewport; // width and height of the viewport

// https://vitaliburkov.wordpress.com/2016/09/17/simple-and-fast-high-quality-antialiased-lines-with-opengl/
void main()
{
    vec4 pp = P*V*M*vec4(aPos, 1.0); 
    gl_Position = pp;
    vertexColor = vec4(aColor, 1.0);
    lineCenter = 0.5*(pp.xy/pp.w + vec2(1,1))*viewport;
}
