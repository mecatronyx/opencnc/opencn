// SPDX-License-Identifier: GPL-2.0
/*
 *   rotational_axis_mode.hh - Singelton class for rotational axis mode
 *
 *   Copyright (C) 2022 Mattia Gallacchi <mattia.gallacchi@heig-vd.ch>
 */

#pragma once

#define AXIS_NUM 3

class RotationalAxis
{

public:
    enum AXIS
    {
        A,
        B,
        C
    };

    /**
     * @brief Get the Instance object
     *
     * @return Directives&
     */
    static RotationalAxis &GetInstance()
    {
        static RotationalAxis *instance = new RotationalAxis();
        return *instance;
    }

    /**
     * @brief Read a axis mode
     *
     * @param line Line to parse
     * @param counter position of the first character
     * @param AXIS axis to save the mode of
     */
    void read_mode(char *line, int *counter, AXIS axis);

    /**
     * @brief Get the status ON/OFF of a directive
     *
     * @param AXIS Axis to get the mode of
     * @return 0 for NONE, 1 for PLUS, 2 for MINUS
     */
    int get_mode(AXIS axis);

    void set_distance_mode(bool mode) { distance_mode = mode; };

    bool get_distance_mode() { return distance_mode; };

    void set_ijk_distance_mode(bool mode) { ijk_distance_mode = mode; };

    bool get_ijk_distance_mode() { return ijk_distance_mode; };

private:
    RotationalAxis() : distance_mode(false), ijk_distance_mode(false){};
    RotationalAxis(const RotationalAxis &) = delete;
    RotationalAxis &operator=(const RotationalAxis &) = delete;
    RotationalAxis(RotationalAxis &&) = delete;
    RotationalAxis &operator=(RotationalAxis &&) = delete;

    enum MODE
    {
        NONE = 0,
        M_PLUS,
        M_MINUS
    };

    struct axis_rot_mode_t
    {
        MODE a;
        MODE b;
        MODE c;
    };

    axis_rot_mode_t axis_mode = {NONE};

    bool distance_mode;
    bool ijk_distance_mode;

    const char axis_name[AXIS_NUM][1] = {'a', 'b', 'c'};
};
