#include "ocn.h"
#include "canon.hh"
#include "config.h"
#include <cmath>
#include <cstdarg>
#include <cstdio>
#include <matlab_headers.h>
#include <vector>

#include <fstream>
#include <iostream>
#include <bits/stdc++.h>

#include "directives.hh"
#include "rotational_axis_mode.hh"
#include "interp_internal.hh"

using namespace ocn;

inline void REMOVE_NAN( double &x ){ x = ( std::isnan( x ) ? 0.0 : x ); } 

namespace
{
	constexpr int NB_MAX_AXIS = 6;
	bool first_movement = true;
	double last_x = 0, last_y = 0, last_z = 0;
	double last_a = 0, last_b = 0, last_c = 0;
	double last_u = 0, last_v = 0, last_w = 0;
	double g92_offsets[CANON_AXIS_COUNT];
	double g5x_offsets[9 + 1][CANON_AXIS_COUNT];
	double feedrate = 0;
	double DEFAULT_FEEDRATE_G0 = 9000;
	double spindle_speed = 0;
	CANON_PLANE current_plane = CANON_PLANE_XY;

	CANON_MOTION_MODE motion_mode = CANON_CONTINUOUS;
	bool first_point_zero = true;
	bool second_point_zero = false;

	CurvStruct curve;
	bool curve_was_init = false;

	CANON_TOOL_TABLE tool_table[CANON_POCKETS_MAX] = {-1};
	int current_tool_pocket = 0;

	/* XYZ by default */
	int axis_mask = 0x7;
#ifdef VALUES_OUTPUT
	std::string out_file = "";
#endif
} // namespace
#ifdef VALUES_OUTPUT
void set_output_file(std::string filepath)
{
	std::ofstream ofs;
	out_file = filepath;

	if (!out_file.empty())
	{
		ofs.open(out_file, std::ios::trunc);
		ofs.close();
	}
}

void write_values(std::vector<coordinates_t> vect)
{
	std::ofstream ofs;

	if (!out_file.empty())
	{
		ofs.open(out_file, std::ios::app);
		for (auto &elem : vect)
		{
			for (int i = 0; i < 3; i++)
			{
				ofs << elem.current[i] << ',';
			}
		}
		ofs << std::endl;
		ofs.close();
	}
	
}
#endif
void check_curve_init()
{
	if (curve_was_init)
		return;
	curve_was_init = true;
	curve.Info.Type = CurveType_None;
}

const char *canon_plane_to_str(CANON_PLANE p)
{
	switch (p)
	{
	case CANON_PLANE_XY:
		return "XY";
	case CANON_PLANE_YZ:
		return "YZ";
	case CANON_PLANE_XZ:
		return "XZ";
	case CANON_PLANE_UV:
		return "UV";
	case CANON_PLANE_VW:
		return "VW";
	case CANON_PLANE_UW:
		return "UW";
	default:
		return "!! UNKNOWN !!";
	}
}

void push_curve(CurvStruct &c)
{
	if (first_movement)
	{
		/* Current implementation of matlab allow a maximum of 3 cartesian axes 
		 * and 3 rotary axes. U,V and W are not supported. */
		last_x = c.R1[0];
		last_y = c.R1[1];
		last_z = c.R1[2];
		last_a = c.R1[3];
		last_b = c.R1[4];
		last_c = c.R1[5];
		first_movement = false;
		return;
	}

	switch (c.Info.Type)
	{
	case CurveType_Line:
		FNLOG("%sLINE%s from %f,%f,%f to %f,%f,%f at F=%f", ASCII_RED, ASCII_RESET, c.R0[0],
			  c.R0[1], c.R0[2], c.R1[0], c.R1[1], c.R1[2], c.Info.FeedRate);
		last_x = c.R1[0];
		last_y = c.R1[1];
		last_z = c.R1[2];
		last_a = c.R1[3];
		last_b = c.R1[4];
		last_c = c.R1[5];
		break;
	case CurveType_Helix:
		FNLOG("%sHELIX%s from %f,%f,%f to %f,%f,%f evec=%f,%f,%f theta=%f pitch=%f "
			  "F=%f",
			  ASCII_RED, ASCII_RESET, c.R0[0], c.R0[1], c.R0[2], c.R1[0], c.R1[1], c.R1[2],
			  c.evec[0], c.evec[1], c.evec[2], c.theta, c.pitch, c.Info.FeedRate);
		last_x = c.R1[0];
		last_y = c.R1[1];
		last_z = c.R1[2];
		last_a = c.R1[3];
		last_b = c.R1[4];
		last_c = c.R1[5];
		break;
	default:
		FNLOG("!!! THIS SHOULD NEVER HAPPEN !!!");
		break;
	}

	c.Info.FeedRate /= 60.0; // Convert FeedRate into mm/s
	c.Info.SpindleSpeed = spindle_speed;

	if (first_point_zero && !second_point_zero)
	{
		c.Info.zspdmode = ZSpdMode_ZN;
	}
	else if (!first_point_zero && !second_point_zero)
	{
		c.Info.zspdmode = ZSpdMode_NN;
	}
	else if (!first_point_zero && second_point_zero)
	{
		c.Info.zspdmode = ZSpdMode_NZ;
	}
	else
	{
		c.Info.zspdmode = ZSpdMode_ZZ;
	}

	push_curv_struct(&c);
	first_point_zero 	= second_point_zero;
	second_point_zero	= false;
}

template <size_t N>
inline double dot(const double (&u) [N], const double (&v) [N])
{
	double res = 0;
	for( auto i = 0; i < N; i++ )
		res += u[i] * v[i];

	return res;
}

template <size_t N>
inline double norm(const double (&v) [N])
{	
	return sqrt( dot<N>( v, v ) );
}

template <size_t N>
inline double dist(const double (&u) [N], const double (&v) [N])
{	
	double res = 0;
	for( auto i = 0; i < N; i++ )
		res += ( u[i] - v[i] ) * ( u[i] - v[i] );
	return sqrt( res );
}

void DWELL(double)
{
	DUMMY_FN;
}
void SET_FEED_MODE(int, int)
{
	DUMMY_FN;
}
void MIST_ON()
{
	DUMMY_FN;
}
void MIST_OFF()
{
	DUMMY_FN;
}
void FLOOD_ON()
{
	DUMMY_FN;
}
void FLOOD_OFF()
{
	DUMMY_FN;
}
void START_SPINDLE_CLOCKWISE(int, int)
{
	DUMMY_FN;
}
void START_SPINDLE_COUNTERCLOCKWISE(int, int)
{
	DUMMY_FN;
}
void STOP_SPINDLE_TURNING(int)
{
	DUMMY_FN;
}
void SET_SPINDLE_MODE(int, double)
{
	DUMMY_FN;
}
void START_CHANGE()
{
	DUMMY_FN;
}
void ORIENT_SPINDLE(int, double, int)
{
	DUMMY_FN;
}
void WAIT_SPINDLE_ORIENT_COMPLETE(int, double)
{
	DUMMY_FN;
}

double GET_EXTERNAL_PROBE_POSITION_A()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_PROBE_POSITION_B()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_PROBE_POSITION_C()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_PROBE_POSITION_X()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_PROBE_POSITION_Y()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_PROBE_POSITION_Z()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_PROBE_POSITION_U()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_PROBE_POSITION_V()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_PROBE_POSITION_W()
{
	DUMMY_FN;
	return 0.0;
}

double GET_EXTERNAL_TOOL_LENGTH_AOFFSET()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_TOOL_LENGTH_BOFFSET()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_TOOL_LENGTH_COFFSET()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_TOOL_LENGTH_XOFFSET()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_TOOL_LENGTH_YOFFSET()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_TOOL_LENGTH_ZOFFSET()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_TOOL_LENGTH_UOFFSET()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_TOOL_LENGTH_VOFFSET()
{
	DUMMY_FN;
	return 0.0;
}
double GET_EXTERNAL_TOOL_LENGTH_WOFFSET()
{
	DUMMY_FN;
	return 0.0;
}

void START_SPEED_FEED_SYNCH(int, double, bool)
{
	DUMMY_FN;
}
double GET_EXTERNAL_LENGTH_UNITS() // units scaling from into mm
{
	return 1.0;
}
double GET_EXTERNAL_ANGLE_UNITS() // HGS : disable conversion
{
	return 1.0;
}
int GET_EXTERNAL_PROBE_TRIPPED_VALUE()
{
	return 0.0;
}

double GET_EXTERNAL_MOTION_CONTROL_TOLERANCE()
{
	return 0.0;
};
CANON_MOTION_MODE GET_EXTERNAL_MOTION_CONTROL_MODE()
{
	return 0;
}
void DISABLE_FEED_OVERRIDE()
{
	DUMMY_FN;
}
void ENABLE_FEED_OVERRIDE()
{
	DUMMY_FN;
}
void ENABLE_SPEED_OVERRIDE(int)
{
	DUMMY_FN;
}
void DISABLE_SPEED_OVERRIDE(int)
{
	DUMMY_FN;
}
void PROGRAM_STOP()
{
	DUMMY_FN;
}

void STOP_SPEED_FEED_SYNCH()
{
	DUMMY_FN;
}
int UNLOCK_ROTARY(int, int)
{
	return 0;
}
int GET_EXTERNAL_FEED_OVERRIDE_ENABLE()
{
	return 0;
}
void PALLET_SHUTTLE()
{
	DUMMY_FN;
}
void PROGRAM_END()
{
	DUMMY_FN;
}
int GET_EXTERNAL_SPINDLE_OVERRIDE_ENABLE(int)
{
	return 0;
}
void USE_TOOL_LENGTH_OFFSET(EmcPose offset)
{
	FNLOG("EmcPose offs : X%f, Y%f, Z%f, A%f, B%f, C%f", offset.tran.x, offset.tran.y,
		offset.tran.z, offset.a, offset.b, offset.c);

	tool_table[0].offset = offset;
}

int LOCK_ROTARY(int, int)
{
	return 0;
}
void RIGID_TAP(int, double, double, double, double)
{
	DUMMY_FN;
}
void FINISH()
{
	DUMMY_FN;
}
void OPTIONAL_PROGRAM_STOP()
{
	DUMMY_FN;
}
void SET_TOOL_TABLE_ENTRY(int pocket, int toolno, EmcPose offset, double diameter, double frontangle,
						  double backangle, int orientation)
{
	FNLOG("Modify tool %d", toolno);
	CANON_TOOL_TABLE &tbl = tool_table[pocket];
	tbl.toolno = toolno;
	tbl.offset = offset;
	tbl.diameter = diameter;
	tbl.frontangle = frontangle;
	tbl.backangle = backangle;
	tbl.orientation = orientation;

	if (tbl.toolno == tool_table[0].toolno)
	{
		tool_table[0] = tbl;
	}
}
void TURN_PROBE_ON()
{
	DUMMY_FN;
}
void TURN_PROBE_OFF()
{
	DUMMY_FN;
}
void ENABLE_ADAPTIVE_FEED()
{
	DUMMY_FN;
}
void DISABLE_ADAPTIVE_FEED()
{
	DUMMY_FN;
}
void STRAIGHT_PROBE(int, double, double, double, double, double, double, double, double, double,
					unsigned char)
{
	DUMMY_FN;
}
void ENABLE_FEED_HOLD()
{
	DUMMY_FN;
}
void DISABLE_FEED_HOLD()
{
	DUMMY_FN;
}

int GET_EXTERNAL_QUEUE_EMPTY()
{
	DUMMY_FN;
	return 1;
}
int GET_EXTERNAL_TC_REASON()
{
	DUMMY_FN;
	return 0;
}
int GET_EXTERNAL_DIGITAL_INPUT(int, int)
{
	DUMMY_FN;
	return 0;
}
double GET_EXTERNAL_ANALOG_INPUT(int, double)
{
	DUMMY_FN;
	return 0.0;
}
bool GET_BLOCK_DELETE()
{
	DUMMY_FN;
	return false;
}
void MESSAGE(char *)
{
	DUMMY_FN;
}
void CLEAR_MOTION_OUTPUT_BIT(int)
{
	DUMMY_FN;
}
void SET_AUX_OUTPUT_BIT(int)
{
	DUMMY_FN;
}
void CLEAR_AUX_OUTPUT_BIT(int)
{
	DUMMY_FN;
}
int WAIT(int, int, int, double)
{
	DUMMY_FN;
	return 0;
}
void SET_MOTION_OUTPUT_VALUE(int, double)
{
	DUMMY_FN;
}
void SET_AUX_OUTPUT_VALUE(int, double)
{
	DUMMY_FN;
}
void CHANGE_TOOL_NUMBER(int number)
{
	FNLOG("Change toolno. %d -> %d", current_tool_pocket, number);
	current_tool_pocket = number;
	tool_table[0] = tool_table[number]; // "load" the requested tool
}
void LOGOPEN(char *s)
{
	DUMMY_FN;
}
void LOGAPPEND(char *s)
{
	DUMMY_FN;
}
void LOGCLOSE()
{
	DUMMY_FN;
}
void LOG(char *)
{
	DUMMY_FN;
}
void SET_NAIVECAM_TOLERANCE(double)
{
	DUMMY_FN;
}
void SET_MOTION_OUTPUT_BIT(int)
{
	DUMMY_FN;
}

void NURBS_FEED(int, std::vector<CONTROL_POINT, std::allocator<CONTROL_POINT>>, unsigned int)
{
	DUMMY_FN;
}

/**************************************************************
 *************** PARTIALY IMPLEMENTED *************************
 **************************************************************/

/**
 * @brief Changes tool to be first in the tool table
 *
 * @param slot Index of the tool in tool table
 */
void CHANGE_TOOL(int slot)
{
	FNLOG("Tool table index %d", slot);

	tool_table[0] = tool_table[slot];
}

void SELECT_POCKET(int pocket, int tool)
{
/*See linuxcnc.org/docs/2.8/pdf/LinuxCNC_Developer_Manual.pdf#page=33 */
// #warning TO Implement if a auto tool changer is used
	FNLOG("NO OP");
}

int GET_EXTERNAL_LENGTH_UNIT_TYPE()
{
	FNLOG("return: %d", CANON_UNITS_MM);
	return CANON_UNITS_MM;
}
void USE_LENGTH_UNITS(int units)
{
	FNLOG("units: %d", units);
}

void SET_XY_ROTATION(double rot)
{
	FNLOG("rot: %f", rot);
}

void SET_FEED_REFERENCE(int ref)
{
	FNLOG("ref: %d", ref);
}

void READ_EXTERNAL_AXIS_MASK(const char *str)
{
	std::string token;
	std::stringstream sub(str);
	std::vector<std::string> tokens;
	axis_mask = 0;
	/* Separate axis entry in tokens*/
	while (std::getline(sub, token, '\x20'))
		tokens.push_back(token);

	for (auto &elem : tokens)
	{
		switch (elem[0])
		{
		case 'X':
			axis_mask |= (1 << 0);
			break;
		case 'Y':
			axis_mask |= (1 << 1);
			break;
		case 'Z':
			axis_mask |= (1 << 2);
			break;
		case 'A':
			axis_mask |= (1 << 3);
			break;
		case 'B':
			axis_mask |= (1 << 4);
			break;
		case 'C':
			axis_mask |= (1 << 5);
			break;
		case 'U':
			axis_mask |= (1 << 6);
			break;
		case 'V':
			axis_mask |= (1 << 7);
			break;
		case 'W':
			axis_mask |= (1 << 8);
			break;
		default:
			break;
		}
	}

	FNLOG("axis mask = %d", axis_mask);
}

int GET_EXTERNAL_AXIS_MASK()
{
	FNLOG("mask = %d", axis_mask);
	return axis_mask;
}

double GET_EXTERNAL_POSITION_A()
{
	FNLOG("pos = 0");
	return 0.0;
}
double GET_EXTERNAL_POSITION_B()
{
	FNLOG("pos = 0");
	return 0.0;
}
double GET_EXTERNAL_POSITION_C()
{
	FNLOG("pos = 0");
	return 0.0;
}
double GET_EXTERNAL_POSITION_X()
{
	FNLOG("pos = 0");
	return 0.0;
}
double GET_EXTERNAL_POSITION_Y()
{
	FNLOG("pos = 0");
	return 0.0;
}
double GET_EXTERNAL_POSITION_Z()
{
	FNLOG("pos = 0");
	return 0.0;
}
double GET_EXTERNAL_POSITION_U()
{
	FNLOG("pos = 0");
	return 0.0;
}
double GET_EXTERNAL_POSITION_V()
{
	FNLOG("pos = 0");
	return 0.0;
}
double GET_EXTERNAL_POSITION_W()
{
	FNLOG("pos = 0");
	return 0.0;
}

// Returns the system value for the carousel slot in which the tool
// currently in the spindle belongs. Return value zero means there is no
// tool in the spindle.
int GET_EXTERNAL_TOOL_SLOT()
{
	return tool_table[0].pocketno;
}

double GET_EXTERNAL_FEED_RATE()
{
	FNLOG("rate = 0.0");
	feedrate = 0.0;
	return feedrate;
}

int GET_EXTERNAL_FLOOD()
{
	FNLOG("flood = 0");
	return 0;
}
int GET_EXTERNAL_MIST()
{
	FNLOG("mist = 0");
	return 0;
}

int GET_EXTERNAL_PLANE()
{
	FNLOG("plane = %s (%d)", canon_plane_to_str(current_plane), current_plane);
	return current_plane;
}
int GET_EXTERNAL_SELECTED_TOOL_SLOT()
{
	return current_tool_pocket;
}
int GET_EXTERNAL_POCKETS_MAX()
{
	//	FNLOG("pockets = 1");
	return sizeof(tool_table) / sizeof(CANON_TOOL_TABLE);
}
int GET_EXTERNAL_ADAPTIVE_FEED_ENABLE()
{
	FNLOG("enabled = 0");
	return 0;
}

void SET_TRAVERSE_RATE( double rate )
{
	DEFAULT_FEEDRATE_G0 = rate;
}

double GET_EXTERNAL_TRAVERSE_RATE()
{
	FNLOG("rate = %f", DEFAULT_FEEDRATE_G0);
	return DEFAULT_FEEDRATE_G0;
}
double GET_EXTERNAL_SPEED(int n)
{
	FNLOG("n = %d, speed = 0", n);
	return 0.0;
}

CANON_DIRECTION GET_EXTERNAL_SPINDLE(int n)
{
	FNLOG("n = %d, dir = CANON_CLOCKWISE", n);
	return CANON_CLOCKWISE;
}

void GET_EXTERNAL_PARAMETER_FILE_NAME( char *out, int len)
{
	out[0] = '\0';
	FNLOG("filename = %s", out);
}

int GET_EXTERNAL_FEED_HOLD_ENABLE()
{
	FNLOG("hold = 1");
	return 1;
}

/* OPENCN */
extern void READ_EXTERNAL_TOOL_TABLE(const char *filepath)
{
	std::string line;
	std::string token;
	std::vector<std::string> tokens;
	std::ifstream ifs(filepath, std::ifstream::in);
	int tool_index = 0;
	bool tool_found = false, ignore = false;
	int line_index = 0;

	FNLOG("Reading tool table file %s", filepath);

	/* read the file line by line */
	while (std::getline(ifs, line))
	{
		std::stringstream sub(line);
		line_index++;
		/* Separate table entry in tokens*/
		while (std::getline(sub, token, '\x20'))
			tokens.push_back(token);

		CANON_TOOL_TABLE tool_entry;
		// Reset entry to default value
		tool_entry.diameter 	= 0.0;
		tool_entry.backangle 	= 0.0;
		tool_entry.frontangle 	= 0.0;
		ZERO_EMC_POSE(tool_entry.offset);
		tool_entry.pocketno 	= 0;
		tool_entry.toolno 		= -1;

		for (auto &elem : tokens)
		{
			/* Ignore comments */
			if (elem[0] == ';')
			{
				ignore = true;
				break;
			}

			switch (elem[0])
			{
			case 'T':
				tool_index++;
				tool_found = true;
				tool_entry.toolno = std::stoi(&elem[1]);
				break;
			case 'P':
				tool_entry.pocketno = std::stoi(&elem[1]);
				break;
			case 'X':
				tool_entry.offset.tran.x = std::stod(&elem[1]);
				break;
			case 'Y':
				tool_entry.offset.tran.y = std::stod(&elem[1]);
				break;
			case 'Z':
				tool_entry.offset.tran.z = std::stod(&elem[1]);
				break;
			case 'A':
				tool_entry.offset.a = std::stod(&elem[1]);
				break;
			case 'B':
				tool_entry.offset.b = std::stod(&elem[1]);
				break;
			case 'C':
				tool_entry.offset.c = std::stod(&elem[1]);
				break;
			case 'U':
				tool_entry.offset.u = std::stod(&elem[1]);
				break;
			case 'V':
				tool_entry.offset.v = std::stod(&elem[1]);
				break;
			case 'W':
				tool_entry.offset.w = std::stod(&elem[1]);
				break;
			case 'D':
				tool_entry.diameter = std::stod(&elem[1]);
				break;
			case 'I':
				tool_entry.frontangle = std::stod(&elem[1]);
				break;
			case 'J':
				tool_entry.backangle = std::stod(&elem[1]);
				break;
			case 'Q':
				tool_entry.orientation = std::stoi(&elem[1]);
				break;
			default:
				break;
			}
		}

		if (tool_found)
		{
			memcpy(&tool_table[tool_index], &tool_entry, sizeof(CANON_TOOL_TABLE));
			tool_found = false;
		}
		else
		{

			if (ignore)
				ignore = false;
			else
				FNLOG("Error in tool table entry %d : %s", line_index, line.c_str());
		}

		tokens.clear();
	}
}

// Returns the CANON_TOOL_TABLE structure associated with the tool
// in the given pocket
extern CANON_TOOL_TABLE GET_EXTERNAL_TOOL_TABLE(int pocket)
{
	CANON_TOOL_TABLE tbl;

	if (pocket < 0 || pocket > CANON_POCKETS_MAX)
	{
		tbl.diameter = 0.0;
		tbl.backangle = 0.0;
		tbl.frontangle = 0.0;
		ZERO_EMC_POSE(tbl.offset);
		tbl.pocketno = 0;
		tbl.toolno = -1;
	}
	else
	{
		tbl = tool_table[pocket];
	}

	return tbl;
}

int GET_EXTERNAL_TC_FAULT()
{
	FNLOG("fault-pin = %d", 0);
	return 0;
}

void SET_FEED_RATE(double rate)
{
	FNLOG("rate = %f", rate);
	feedrate = rate;
}

void check_values(coordinates_t& val)
{
	for(int i = 0; i < COORDINATES_NUM; i++)
	{
		if (std::isinf(val.current[i]) || std::isnan(val.current[i]))
		{
			val.current[i] = 0.0;
		}
	}
}

void check_offsets(offs_t& off)
{
	for(int i = 0; i < COORDINATES_NUM; i++)
	{
		if (std::isinf(off.current[i]) || std::isnan(off.current[i]))
		{
			off.current[i] = 0.0;
		}
	}
}

/* OPENCN */
/**
 * @brief
 *
 * @param lineno G-Code line number
 * @param x G-Code X value
 * @param y G-Code Y value
 * @param z G-Code Z value
 * @param a G-Code A value
 * @param b G-Code B value
 * @param c G-Code C value
 * @param u G-Code U value
 * @param v G-Code V value
 * @param w G-Code W value
 */
void straight_movement(int lineno, double x, double y, double z, double a, double b, double c,
					   double u, double v, double w)
{
	coordinates_t Pos = {{x, y, z}, {last_x, last_y, last_z}};
	coordinates_t Angles = {{a, b, c}, {last_a, last_b, last_c}};
	coordinates_t U = {{u, v, w}, {last_u, last_v, last_w}};
	offs_t Poffs = {tool_table[0].offset.tran.x, tool_table[0].offset.tran.y, tool_table[0].offset.tran.z};
	offs_t Aoffs = {tool_table[0].offset.a, tool_table[0].offset.b, tool_table[0].offset.c};
	offs_t Uoffs = {tool_table[0].offset.u, tool_table[0].offset.v, tool_table[0].offset.w};

	check_values(Pos);
	check_values(Angles);
	check_values(U);

	check_offsets(Poffs);
	check_offsets(Aoffs);
	check_offsets(Uoffs);

	FNLOG("lineno = %d, x = %f, y = %f, z = %f, a = %f, b = %f, c = %f, u = %f, v = %f, w = %f, feedrate = %f ", lineno, x, y, z, a, b, c, u, v, w, feedrate);
	
	check_curve_init();
#ifdef VALUES_OUTPUT
	write_values(std::vector<coordinates_t>{Pos, Angles, U});
#endif
#ifdef DEBUG_RS274
	Directives::GetInstance().print_all();
#endif

	if (first_movement)
	{
		FNLOG( "First Movement" );
		last_x = x;
		last_y = y;
		last_z = z;
		last_a = a;
		last_b = b;
		last_c = c;
		last_u = u;
		last_v = v;
		last_w = w;
		first_movement = false;
	}
	else if( dist( Pos.last, Pos.current ) > 1e-9 || dist( Angles.last, Angles.current ) > 1e-9 )
	{
		GcodeInfoStruct info;

		constrGcodeInfoStruct( ocn::CurveType::CurveType_Line, ZSpdMode_NN, Directives::GetInstance().get_directive_status(DIRECTIVES::TRAFO), 
		Directives::GetInstance().get_directive_status(DIRECTIVES::HSC), false, false, feedrate, spindle_speed, lineno, &info );

		double R0[NB_MAX_AXIS]; double R1[NB_MAX_AXIS];
		for ( int i = 0; i < 3; i++ ) 
		{ 
			R0[ i ] = Pos.last[ i ]; 
			R1[ i ] = Pos.current[ i ];
			R0[ 3 + i ] = Angles.last[ i ]; 
			R1[ 3 + i ] = Angles.current[ i ];
		}

		Axes axesOffset;
		constrAxesStruct( Poffs.current[0], Poffs.current[1], Poffs.current[2], Aoffs.current[0], 
		Aoffs.current[1], Aoffs.current[2], Uoffs.current[0], Uoffs.current[1], Uoffs.current[2], &axesOffset );
		Tool tool;
		constrToolStruct( tool_table[0].toolno, tool_table[0].pocketno, &axesOffset, tool_table[0].diameter, tool_table[0].frontangle, 
		tool_table[0].backangle, tool_table[0].orientation, &tool );

		constrLineStruct( &info, &tool, R0, &NB_MAX_AXIS, R1, &NB_MAX_AXIS, &curve );

		curve.Info.gcode_source_line = lineno;

#ifdef EXTERNAL_CALLBACK
		if (push_elem)
		{
			push_elem(curve);
		}
		else
		{
			FNLOG("Push elem not defined. DEMO MODE");
		}
#else
		push_curve(curve);
#endif
	}
	else
	{
		FNLOG("Points too close, ignored segment");
	}
}

void STRAIGHT_TRAVERSE(int lineno, double x, double y, double z, double a, double b, double c,
					   double u, double v, double w)
{
	auto old_feedrate = feedrate;
	feedrate = GET_EXTERNAL_TRAVERSE_RATE();
	REMOVE_NAN( x );
	REMOVE_NAN( y );
	REMOVE_NAN( z );
	REMOVE_NAN( a );
	REMOVE_NAN( b );
	REMOVE_NAN( c );
	REMOVE_NAN( u );
	REMOVE_NAN( v );
	REMOVE_NAN( w );
	straight_movement(lineno, x, y, z, a, b, c, u, v, w);
	feedrate = old_feedrate;
	FNLOG("Active tool T%d", tool_table[0].toolno);
}

void STRAIGHT_FEED(int lineno, double x, double y, double z, double a, double b, double c, double u,
				   double v, double w)
{
	REMOVE_NAN( x );
	REMOVE_NAN( y );
	REMOVE_NAN( z );
	REMOVE_NAN( a );
	REMOVE_NAN( b );
	REMOVE_NAN( c );
	REMOVE_NAN( u );
	REMOVE_NAN( v );
	REMOVE_NAN( w );
	straight_movement(lineno, x, y, z, a, b, c, u, v, w);
}

/*
 * first_end: end of arc in the first reference axis
 * second_end: end of arc in the second reference axis
 * first_axis: offset of the center of rotation in the first reference axis
 * second_axis: offset of the center of rotation in the second reference axis
 * rotation: clockwise/anticlockwise
 * axis_end_point: movement along the third axis, orthgonal to the first two,
 * can be 0
 */
void ARC_FEED(int lineno, double first_end, double second_end, double first_axis,
			  double second_axis, int rotation, double axis_end_point, double a, double b, double c,
			  double u, double v, double w, double pitch)
{
	REMOVE_NAN( a );
	REMOVE_NAN( b );
	REMOVE_NAN( c );
	REMOVE_NAN( u );
	REMOVE_NAN( v );
	REMOVE_NAN( w );

    coordinates_t Pos = {{0, 0, 0}, {last_x, last_y, last_z}};
	coordinates_t Angles = {{a, b, c}, {last_a, last_b, last_c}};
	coordinates_t U = {{u, v, w}, {last_u, last_v, last_w}};
	coordinates_t evec = {{0, 0, 0}, {0, 0, 0}};
	coordinates_t Center = {{0, 0, 0}, {0, 0, 0}};
	offs_t Poffs = {tool_table[0].offset.tran.x, tool_table[0].offset.tran.y, tool_table[0].offset.tran.z};
	offs_t Aoffs = {tool_table[0].offset.a, tool_table[0].offset.b, tool_table[0].offset.c};
	offs_t Uoffs = {tool_table[0].offset.u, tool_table[0].offset.v, tool_table[0].offset.w};

	check_offsets(Poffs);
	check_offsets(Aoffs);
	check_offsets(Uoffs);
	check_values(Pos);
	check_values(Angles);
	check_values(U);
	check_values(evec);
	check_values(Center);
	
	FNLOG("lineno = %d, first_end = %f, second_end = %f\n"
		  "first_axis = %f, second_axis = %f, rotation = %d, axis_end_point = %f",
		  lineno, first_end, second_end, first_axis, second_axis, rotation, axis_end_point);
    
#ifdef VALUES_OUTPUT
    write_values(std::vector<coordinates_t>{Pos, Angles, U});
#endif
	// rotation: number of turns in trigonometric conventions:
	// positive -> couter-clockwise
	// negative -> clockwise
	check_curve_init();

	if (rotation == 0)
	{
		rotation = 1;
	}

	switch (current_plane)
	{
	case CANON_PLANE_XY:
		evec.current[2] = 1.0;
		Pos.current[0] = first_end;
		Pos.current[1] = second_end;
		Pos.current[2] = axis_end_point;

		Center.current[0] = first_axis;
		Center.current[1] = second_axis;
		Center.current[2] = Pos.last[2];
		break;
	case CANON_PLANE_YZ:
		evec.current[0] = 1.0;
		Pos.current[0] = axis_end_point;
		Pos.current[1] = first_end;
		Pos.current[2] = second_end;

		Center.current[0] = Pos.last[0];
		Center.current[1] = first_axis;
		Center.current[2] = second_axis;
		break;
	case CANON_PLANE_XZ:
		evec.current[1] = 1.0;
		Pos.current[0] = second_end;
		Pos.current[1] = axis_end_point;
		Pos.current[2] = first_end;


		Center.current[0] = second_axis;
		Center.current[1] = Pos.last[1];
		Center.current[2] = first_axis;
		break;
	}

	GcodeInfoStruct info;

	constrGcodeInfoStruct( ocn::CurveType::CurveType_Line, ZSpdMode_NN, Directives::GetInstance().get_directive_status(DIRECTIVES::TRAFO), 
	Directives::GetInstance().get_directive_status(DIRECTIVES::HSC), false, false, feedrate, spindle_speed, lineno, &info );

	Axes axesOffset;
	constrAxesStruct( Poffs.current[0], Poffs.current[1], Poffs.current[2], Aoffs.current[0], 
	Aoffs.current[1], Aoffs.current[2], Uoffs.current[0], Uoffs.current[1], Uoffs.current[2], &axesOffset );
	Tool tool;
	constrToolStruct( tool_table[0].toolno, tool_table[0].pocketno, &axesOffset, tool_table[0].diameter, tool_table[0].frontangle, 
	tool_table[0].backangle, tool_table[0].orientation, &tool );

	constrHelixStructFromArcFeed( &info, &tool, Pos.last, Pos.current, Center.current, Angles.last, Angles.current, rotation, evec.current, &curve );

	curve.Info.FeedRate = feedrate;
	curve.Info.gcode_source_line = lineno;

#ifdef EXTERNAL_CALLBACK
	if (push_elem)
	{
		push_elem(curve);
	}
	else
	{
		FNLOG("Push elem not defined. DEMO MODE");
	}
#else
	push_curve(curve);
#endif
}

void SET_G92_OFFSET(double x, double y, double z, double a, double b, double c, double u, double v,
					double w)
{
	FNLOG("x = %lf, y = %lf, z = %lf, a = %lf, b = %lf, c = %lf, u = %lf, v = "
		  "%lf, w = %lf",
		  x, y, z, a, b, c, u, v, w);
	g92_offsets[CANON_AXIS_X] = x;
	g92_offsets[CANON_AXIS_Y] = y;
	g92_offsets[CANON_AXIS_Z] = z;
	g92_offsets[CANON_AXIS_A] = a;
	g92_offsets[CANON_AXIS_B] = b;
	g92_offsets[CANON_AXIS_C] = c;
	g92_offsets[CANON_AXIS_U] = u;
	g92_offsets[CANON_AXIS_V] = v;
	g92_offsets[CANON_AXIS_W] = w;
}

void SET_G5X_OFFSET(int origin, double x, double y, double z, double a, double b, double c,
					double u, double v, double w)
{
	FNLOG("origin = %d, x = %lf, y = %lf, z = %lf, a = %lf, b = %lf, c = %lf, u "
		  "= %lf, v = %lf, w "
		  "= %lf",
		  origin, x, y, z, a, b, c, u, v, w);
	g5x_offsets[origin][CANON_AXIS_X] = x;
	g5x_offsets[origin][CANON_AXIS_Y] = y;
	g5x_offsets[origin][CANON_AXIS_Z] = z;
	g5x_offsets[origin][CANON_AXIS_A] = a;
	g5x_offsets[origin][CANON_AXIS_B] = b;
	g5x_offsets[origin][CANON_AXIS_C] = c;
	g5x_offsets[origin][CANON_AXIS_U] = u;
	g5x_offsets[origin][CANON_AXIS_V] = v;
	g5x_offsets[origin][CANON_AXIS_W] = w;
}

void COMMENT(const char *str)
{
	FNLOG("str = %s", str);
}

void INIT_CANON()
{
	FNLOG("INIT_CANON");
	last_x = 0; last_y = 0; last_z = 0;
	last_a = 0; last_b = 0; last_c = 0;
	last_u = 0; last_v = 0; last_w = 0;
	
	first_movement 		= true;
	first_point_zero 	= true;
	second_point_zero 	= false;

// #warning This should later use "Home position" values from feedopt, settable in the GUI
}

void init_push_elem(std::function<void(ocn::CurvStruct &)> func)
{
	push_elem = func;
}

void SELECT_PLANE(int plane)
{
	FNLOG("plane = %s (%d)", canon_plane_to_str(plane), plane);
	current_plane = plane;
}

void SET_SPINDLE_SPEED(int spindle, double speed)
{
	FNLOG("spindle = %d, speed = %f", spindle, speed);
	spindle_speed = speed;
}

void SET_MOTION_CONTROL_MODE(int mode, double tolerance)
{
	FNLOG("=================================== mode = %d", mode);
	motion_mode = mode;
	if (mode == CANON_CONTINUOUS)
	{
		second_point_zero = false;
	}
	else
	{
		second_point_zero = true;
	}
}

/**************************************************************
 ******************* FULLY IMPLEMENTED ************************
 **************************************************************/

int _task = 1;
char _parameter_file_name[LINELEN] = {0};
USER_DEFINED_FUNCTION_TYPE USER_DEFINED_FUNCTION[USER_DEFINED_FUNCTION_NUM] = {0};
