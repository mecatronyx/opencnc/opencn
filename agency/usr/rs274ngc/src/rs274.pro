TEMPLATE = app
CONFIG += console link_pkgconfig precompile_header c++1z
CONFIG -= app_bundle
CONFIG -= qt
LANGUAGE = C++

PKGCONFIG = glfw3 clp gsl
PRECOMPILED_HEADER = pch.h
PRECOMPILED_DIR = pch

QMAKE_CXXFLAGS += -march=native

INCLUDEPATH += ../../../include ../../../matlab/common/src \
../../../ ../../../matlab/common generated imgui glad imgui/examples

DEFINES += USE_FULL_MATLAB_GEN DEBUG_RS274 IMGUI_IMPL_OPENGL_LOADER_GLAD
DEFINES += RS274_DEV
#DEFINES += MATLAB_MEX_FILE

SOURCES += ../../../matlab/common/src/functions.cpp \
../../../matlab/common/src/cpp_simplex.cpp\
../../../matlab/common/src/c_spline.c

SOURCES += main.cpp \
    camera.cpp \
    glad/glad.c \
    imgui/examples/imgui_impl_glfw.cpp \
    imgui/examples/imgui_impl_opengl3.cpp \
    imgui/imgui.cpp \
    imgui/imgui_demo.cpp \
    imgui/imgui_draw.cpp \
    imgui/imgui_widgets.cpp \
    inifile.cc \
    interp_arc.cc \
    interp_array.cc \
    interp_base.cc \
    interp_check.cc \
    interp_convert.cc \
    interp_cycles.cc \
    interp_execute.cc \
    interp_find.cc \
    interp_internal.cc \
    interp_inverse.cc \
    interp_namedparams.cc \
    interp_o_word.cc \
    interp_queue.cc \
    interp_read.cc \
    interp_remap.cc \
    interp_setup.cc \
    interp_write.cc \
    ocn.cc \
    pch.cpp \
    rs274ngc_pre.cc \
    shader.cpp \
    vertex_buffer.cpp

SOURCES += $$files(generated/*.cpp, false)

HEADERS += \
    camera.h \
    canon.hh \
    canon_position.hh \
    cmd_msg.hh \
    debugflags.h \
    emc.hh \
    emcmotcfg.h \
    emcpos.h \
    emctool.h \
    inifile.h \
    inifile.hh \
    interp_base.hh \
    interp_internal.hh \
    interp_queue.hh \
    interp_return.hh \
    kinematics.h \
    motion.h \
    motion_types.h \
    nml.hh \
    nml_oi.hh \
    nml_type.hh \
    nmlmsg.hh \
    ocn.h \
    posemath.h \
    rcs.hh \
    rs274ngc.hh \
    rs274ngc_interp.hh \
    rs274ngc_return.hh \
    shader.h \
    units.h \
    vertex_buffer.h

LIBS += -ldl

DISTFILES += \
    anchor.ngc \
    micro5_3axes_ebauche.ngc \
    ngc/line.ngc \
    shaders/base.frag \
    shaders/base.vert \
    shaders/line.frag \
    shaders/line.vert
