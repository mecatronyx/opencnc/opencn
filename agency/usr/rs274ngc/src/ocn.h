#pragma once

#include "config.h"
#include <feedopt.hpp>
#include <functional>

#define __FILENAME__ (strrchr("/" __FILE__, '/') + 1)

#define DIRECTIVE_ARG_SIZE      256
#define COORDINATES_NUM         3

/*
// #define DBG(msg, ...)                                                                                                  \
//     do {                                                                                                               \
//         fprintf(stderr, "%s:%d: ", __FILENAME__, __LINE__);                                                            \
//         fprintf(stderr, msg __VA_OPT__(, ) __VA_ARGS__);                                                               \
//     } while (0);
*/


#define UNIMPLEMENTED(expr)                                                                                            \
    do {                                                                                                               \
        fprintf(stderr, "[UNIMPLEMENTED] %s:%d: \n", __FILENAME__, __LINE__);                                          \
        fprintf(stderr, #expr);                                                                                        \
        fprintf(stderr, "----------------------\n");                                                                   \
    } while (0);

#define UNSUPPORTED(expr)                                                                                              \
    do {                                                                                                               \
        fprintf(stderr, "[UNSUPPORTED] %s:%d: \n", __FILENAME__, __LINE__);                                            \
        fprintf(stderr, #expr);                                                                                        \
        fprintf(stderr, "----------------------\n");                                                                   \
    } while (0);

void qc_reset();

static std::function<void(ocn::CurvStruct&)> push_elem;

void init_push_elem(std::function<void(ocn::CurvStruct&)> func);

// #define INCH_PER_MM (1.0 / 25.4)

// constexpr int CANON_POCKETS_MAX = 1;


struct coordinates_t    
{
    double current[COORDINATES_NUM];
    double last[COORDINATES_NUM];
};

struct offs_t{
    double current[COORDINATES_NUM];
};

#ifdef VALUES_OUTPUT
void set_output_file(std::string filepath = "");
#endif
