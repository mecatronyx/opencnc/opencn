// SPDX-License-Identifier: GPL-2.0
/*
*   directives.cc - Class Directives methods implementation
*
*   Copyright (C) 2022 Mattia Gallacchi <mattia.gallacchi@heig-vd.ch>
*/

#include <iostream>
#include <cstring>
#include <algorithm>

#include "directives.hh"
#include "interp_return.hh"

bool Directives::check_directives_exists(DIRECTIVES type)
{
    bool exist = false;

    for (auto &elem : directives)
    {
        if(elem.type == type) {
            exist = true;
        }
    }

    
#ifdef DEBUG_RS274
    if (!exist) {
        std::cout << "Directive type " << type << " is not supported!" << std::endl;
        std::cout << "Supported directives are : " << std::endl;

        for (auto &elem : directives) 
        {
            std::string capital = elem.name;
            std::transform(capital.begin(), capital.end(), capital.begin(), ::toupper);
            std::cout << capital << " : " << elem.type << std::endl;
        }
    }
#endif

    return exist;
}

bool Directives::get_directive_status(DIRECTIVES type)
{
    bool ret = false;

    if (!check_directives_exists(type)) {
        return ret;
    }

    for (auto &elem : directives) 
    {
        if (elem.type == type) {
            ret = elem.status;
            break;
        }
    }
    return ret;
}

bool Directives::get_directive_arguments(DIRECTIVES type, char *args, size_t size)
{
    bool ret = false;

    if (!check_directives_exists(type)) {
        return ret;
    }

    /* 
     * Matlab does not support the implementation below. If HSC args are to be used it needs
     * to be changed.
     */

#ifdef MEX_READGCODE
    args = "\0";
    ret = true;
#else
    for (auto &elem : directives)
    {
        if (elem.type == type) {
            if (!elem.arguments.empty()) {
                if (size > elem.arguments.length()) {
                    strcpy(args, elem.arguments.c_str());
                    ret = true;
                    break;
                }
                else {
#ifdef DEBUG_RS274
                    std::cout << "["<< __func__ << "] Args size too small. Need :" 
                            << elem.arguments.length() << ", got " << size << std::endl;
#endif                      
                }
            }
            else {
#ifdef DEBUG_RS274
                std::cout << "["<< __func__ << "] No args for directive "
                            << elem.name << std::endl;
#endif  
            }
        }
    }
#endif
    return ret;
}

int Directives::read_directive(char *line, int *counter)
{
    std::string dir(line);
    std::string args;
    size_t start_pos = 0;
    bool found_dir = false;

    *counter = (*counter + dir.length());

    for (auto &elem: directives) 
    {
        /* Search for the directive name */
        start_pos = dir.find(elem.name);

        /* Check if string is found and if the first character is just afte # */
        if (start_pos != std::string::npos && start_pos == 1) {

#ifdef DEBUG_RS274
            std::cout << "Found directive " << dir << std::endl;
#endif  
            found_dir = true;

            /* remove the directive and the # from the string */
            args = dir.substr(start_pos + elem.name.length());

            if (args.find(ACTIVE) != std::string::npos && 
                args.length() == std::string(ACTIVE).length()) {
                
                elem.status = true;
            }
            else if (args.find(INACTIVE) != std::string::npos && 
                    args.length() == std::string(INACTIVE).length()) {
                
                elem.status = false;
            }
            else {
                switch (elem.type) 
                {
                    case HSC:
                        elem.arguments = args;
                        break;
                    
                    default:
                        found_dir = false;
                        break;
                }

            }

            break;
        }
    }

    if (found_dir) {
        return INTERP_OK;
    }
    else {
        std::cout << "[" << __FILE__ << " : " << __func__ 
                    << "()] Unsupported directive : " << dir << std::endl; 
        return INTERP_ERROR ;
    }
}

void Directives::print_all()
{
    for (auto &elem : directives) 
    {
        std::cout << "Directive " << elem.name << " => status : " << elem.status
                    <<", arguments : " << elem.arguments << std::endl;
    }
}