// SPDX-License-Identifier: GPL-2.0
/*
*   rotational_axis_mode.hh - Singelton class for rotational axis mode
*
*   Copyright (C) 2022 Mattia Gallacchi <mattia.gallacchi@heig-vd.ch>
*/
#include <string>
#include <iostream>

#include "rotational_axis_mode.hh"

void RotationalAxis::read_mode(char *line, int *counter, AXIS axis)
{
    std::string str = std::string(line);
    MODE mode;

    str.erase(0, str.find(axis_name[axis]));

    switch(str[1])
    {
        case '+':
            mode = M_PLUS;
            break;
        case '-':
            mode = M_MINUS;
            break;
        default:
            mode = NONE;
    }

    switch(axis)
    {
        case A:
            axis_mode.a = mode;
            break;
        case B:
            axis_mode.b = mode;
            break;
        case C:
            axis_mode.c = mode;
            break;
    }
}

int RotationalAxis::get_mode(AXIS axis)
{
    MODE mode;
    switch(axis)
    {
        case A:
            mode = axis_mode.a;
            break;
        case B:
            mode = axis_mode.b;
            break;
        case C:
            mode = axis_mode.c;
            break;
    }

    return (int)mode;
}