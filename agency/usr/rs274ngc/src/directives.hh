// SPDX-License-Identifier: GPL-2.0
/*
*   directives.hh - Singelton class Directives definition
*
*   Copyright (C) 2022 Mattia Gallacchi <mattia.gallacchi@heig-vd.ch>
*/

#pragma once

#include <string>

#define DIRECTIVE_CHAR      '#'

/* Directives status*/
#define ACTIVE              "on"
#define INACTIVE            "off"

enum DIRECTIVES {TRAFO=0, HSC, TEST};

struct directives_t {
    std::string name;
    bool status;
    std::string arguments;
    DIRECTIVES type;
};

/* Supported directives */
static directives_t directives [] = {
    {"trafo", false, "", TRAFO},
    {"hsc", false, "", HSC}
};

/**
 * @brief This singelton class is used to read and store G-Code directives (#). 
 * Supported directives are :
 *  #TRAFO ON/OFF
 *  #HSC ON/OFF
 *  #HSC[<ARGS>] ==> partially supported it will not return an error
 *  #TEST ON/OFF ==> used for test pourpouses
 * 
 * To add a new directive you can simply add in the directives array a new 
 * struct. Name must be in lowercases. Add it also to the enum DIRECTIVES.
 */
class Directives {

    public:
        /**
         * @brief Get the Instance object
         * 
         * @return Directives& 
         */
        static Directives& GetInstance() 
        {
            static Directives* instance = new Directives();
            return *instance;
        }

        /**
         * @brief Read a directive
         * 
         * @param line Line to parse 
         * @param counter position of the first character
         * @return INTERP_OK, INTERP_ERROR if the directive is not part of the directive_str
         */
        int read_directive(char *line, int *counter);

        /**
         * @brief Get the status ON/OFF of a directive
         * 
         * @param dir Directive to get the value of
         * @return true 
         * @return false 
         */
        bool get_directive_status(DIRECTIVES type);

        /**
         * @brief Get the directive arguments object
         * 
         * @param dir Directives to get the arguments of
         * @param args strings in which the arguments will be saved
         * @return true 
         * @return false if a directives has no arguments
         */
        bool get_directive_arguments(DIRECTIVES type, char *args, size_t size);
        
        /**
         * @brief Print all directives current status
         * 
         */
        void print_all();

    private:

        Directives() {};
        Directives(const Directives&) = delete;
        Directives& operator=(const Directives&) = delete;
        Directives(Directives&&) = delete;
        Directives& operator=(Directives&&) = delete;

        /**
         * @brief Check if directives is defined in directives
         * 
         * @param type Directive
         * @return true 
         * @return false 
         */
        bool check_directives_exists(DIRECTIVES type);

};