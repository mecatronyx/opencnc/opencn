#!/bin/bash

function usage {
  echo "$0 [OPTIONS]"
  echo "  -c        Clean"
  echo "  -d        Debug build"
  echo "  -h        Print this help"
}

function cp_to_bin {
  [ -f $1 ] && echo "Copy $1" && cp $1 build-host/bin
}

function cp_to_python {
  [ -f $1 ] && echo "Copy $1" && cp $1 build-host/python
}


clean=n
debug=n

while getopts cdh option
  do
    case "${option}"
      in
        c) clean=y;;
        d) debug=y;;
        h) usage && exit 1;;
    esac
  done

SCRIPT=$(readlink -f $0)
SCRIPTPATH=`dirname $SCRIPT`

# Retrieve tool table from binary (for dev)
cp build-host/host/user_m5_gui/tool.tbl host/user_m5_gui/tool.tbl 

if [ $clean == y ]; then
  echo "Cleaning $SCRIPTPATH/build-host"
  rm -rf $SCRIPTPATH/build-host
  exit
fi

if [ $debug == y ]; then
  build_type="Debug"
else
  build_type="Release"
fi

echo "Starting $build_type build"
mkdir -p $SCRIPTPATH/build-host

cd $SCRIPTPATH/build-host
cmake -DCMAKE_BUILD_TYPE=$build_type -DOPENCN_BUILD_HOST=ON  ..

NRPROC=$((`cat /proc/cpuinfo | awk '/^processor/{print $3}' | wc -l` + 1))
make -j$NRPROC

cd -

# Copy generated binaries and python modules in dedicated folder
build_paths=(bin python)

# copy generated binaries to the 'bin' folder
for folder in "${build_paths[@]}"; do
  rm -rf $SCRIPTPATH/build-host/$folder
  mkdir -p $SCRIPTPATH/build-host/$folder
done

# usr-gui
cp_to_bin build-host/host/user_gui/user-gui
cp_to_bin build-host/host/user_m5_gui/user-m5-gui

# companion demo
cp_to_bin build-host/host/companion_demo/companion_demo

# Python module & the related lib
cp_to_python build-host/host/cmctl_client/python_module/opencn*\.so

# Tooledit 
cp_to_bin host/tooledit/tooledit.sh
mkdir -p build-host/tooledit && cp host/tooledit/tooledit.tcl build-host/tooledit