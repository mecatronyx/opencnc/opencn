#!/usr/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
TCL_PATH=$SCRIPTPATH/../tooledit/tooledit.tcl

help()
{
    echo "Use this tool to edit tooltables files"
    echo 
    echo "Usage: tooledit.sh [OPTION]... FILE..."
    echo "Syntax: [-n] FILE"
    echo "Options: "
    echo "h     Prints this help"
    echo "n     Create a new tooltable"
}

if [ $# -eq 0 ];
then
    echo "Error: missing argument" 
    help
    exit 0
elif [ $# -eq 1 ];
then
    tclsh $TCL_PATH $1
    exit 0
else

while getopts ":n" option; do
    case $option in
        h)
            help
            exit;;
        n)
            if [ $# -eq 1 ];
            then
                echo "Error: missing tooltable file"
                help
            else
                touch $2 && echo ";" > $2 && echo "T1" > $2
                tclsh $TCL_PATH $2
            fi
            exit;;
        \?)
            echo "Error: invalid option"
            help
            exit;;
    esac
done
fi