#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <pins.h>
#include <pins_transaction.h>

typedef enum {
	DIRECTION_FORWARD = 0,
	DIRECTION_BACWARD,
} direction_t;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(capnp::EzRpcClient *client, QWidget *parent = nullptr) ;
    ~MainWindow() noexcept;

 public slots:
 	void update_speed_motorA();
    void change_direction_motorA(bool checked);
    void emergency_stop_motorA(bool checked);

    void update_speed_motorB();
    void change_direction_motorB(bool checked);
    void emergency_stop_motorB(bool checked);

private:
    Ui::MainWindow *ui;
    capnp::EzRpcClient *_client;
    direction_t _directionA = DIRECTION_FORWARD;
    direction_t _directionB = DIRECTION_FORWARD;

    CMPinU32 speedAPin{"lcec.0.0.pwm-duty-cycle-0", _client};
    CMPinBit in1APin{"lcec.0.0.out-0", _client};
    CMPinBit in2APin{"lcec.0.0.out-1", _client};

    CMPinU32 speedBPin{"lcec.0.0.pwm-duty-cycle-1", _client};
    CMPinBit in1BPin{"lcec.0.0.out-2", _client};
    CMPinBit in2BPin{"lcec.0.0.out-3", _client};

    bool _isForward_motorA;
    bool _isForward_motorB;
};

#endif // MAINWINDOW_H
