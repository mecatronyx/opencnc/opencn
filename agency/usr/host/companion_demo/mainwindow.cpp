#include <iostream>

#include <QSlider>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(capnp::EzRpcClient* client, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
   _client(client)
{
    ui->setupUi(this);

    ui->stopButtonA->setCheckable(true);
    ui->stopButtonB->setCheckable(true);

    connect(ui->speedSliderA,   SIGNAL(sliderReleased()), this, SLOT(update_speed_motorA()));
    connect(ui->ForwardButtonA, SIGNAL(toggled(bool)),    this, SLOT(change_direction_motorA(bool)));
    connect(ui->stopButtonA,    SIGNAL(toggled(bool)),    this, SLOT(emergency_stop_motorA(bool)));

    connect(ui->speedSliderB,   SIGNAL(sliderReleased()), this, SLOT(update_speed_motorB()));
    connect(ui->ForwardButtonB, SIGNAL(toggled(bool)),    this, SLOT(change_direction_motorB(bool)));
    connect(ui->stopButtonB,    SIGNAL(toggled(bool)),    this, SLOT(emergency_stop_motorB(bool)));

    // Access is done directly by PINs - no PINs transactions
	speedAPin.setMode(CMPin::AccessMode::PIN);
	in1APin.setMode(CMPin::AccessMode::PIN);
	in2APin.setMode(CMPin::AccessMode::PIN);

	speedBPin.setMode(CMPin::AccessMode::PIN);
	in1BPin.setMode(CMPin::AccessMode::PIN);
	in2BPin.setMode(CMPin::AccessMode::PIN);

    // Set motor in startup state
    speedAPin = 0;
	in1APin   = true;
	in2APin   = false;
	_isForward_motorA = true;

	speedBPin = 0;
	in1BPin   = true;
	in2BPin   = false;
	_isForward_motorB = true;
}

MainWindow::~MainWindow() noexcept
{
	std::cout << "Application stopped " << std::endl;

	speedAPin = 0;
	speedBPin = 0;

    delete ui;
}

void MainWindow::update_speed_motorA()
{
	int speed = ui->speedSliderA->value();

	speedAPin = speed;
}

void MainWindow::change_direction_motorA(bool checked)
{
	bool forward  = checked;
	bool backward = ui->backwardButtonA->isChecked();

	// Sanity Check / debug
	if ((forward && backward) ||
		(!forward && !backward)) {
		std::cerr << "[ERROR] Both backward & forward option are checked !" << std::endl;
		QCoreApplication::exit(-1);
	}

	if (forward) {
		std::cout << "Change direction - FORWARD" << std::endl;
		in1APin = true;
		in2APin = false;

		_isForward_motorA = true;

	} else {
		std::cout << "Change direction - BACKWARD" << std::endl;
		in1APin = false;
		in2APin = true;

		_isForward_motorA = false;
	}
}

void MainWindow::emergency_stop_motorA(bool checked)
{
	if (checked) {
		std::cout << " Emergency STOP" << std::endl;
		in1APin = false;
		in2APin = false;

	} else {
		std::cout << " Normal State" << std::endl;
		if (_isForward_motorA) {
			in1APin = true;
			in2APin = false;
		} else {
			in1APin = false;
			in2APin = true;
		}
	}
}

void MainWindow::update_speed_motorB()
{
	int speed = ui->speedSliderB->value();

	std::cout << "New speed value: " << speed << std::endl;
	speedBPin = speed;
}

void MainWindow::change_direction_motorB(bool checked)
{
	bool forward  = checked;
	bool backward = ui->backwardButtonB->isChecked();

	// Sanity Check / debug
	if ((forward && backward) ||
		(!forward && !backward)) {
		std::cerr << "[ERROR] Both backward & forward option are checked !" << std::endl;
		QCoreApplication::exit(-1);
	}

	if (forward) {
		std::cout << "Change direction - FORWARD" << std::endl;
		in1BPin = true;
		in2BPin = false;

		_isForward_motorB = true;

	} else {
		std::cout << "Change direction - BACKWARD" << std::endl;
		in1BPin = false;
		in2BPin = true;

		_isForward_motorB = false;
	}
}

void MainWindow::emergency_stop_motorB(bool checked)
{
	if (checked) {
		std::cout << " Emergency STOP" << std::endl;
		in1BPin = false;
		in2BPin = false;

	} else {
		std::cout << " Normal State" << std::endl;
		if (_isForward_motorB) {
			in1BPin = true;
			in2BPin = false;
		} else {
			in1BPin = false;
			in2BPin = true;
		}
	}
}
