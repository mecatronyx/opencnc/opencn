#include "mainwindow.h"
#include <QCommandLineParser>
#include <QDebug>
#include <QApplication>
#include <QSettings>
#include <QDir>

#include <arpa/inet.h>
#include <capnp/ez-rpc.h>

#include "ipdialog.h"

#define DEFAULT_IP_ADDR     "192.168.53.15"
#define DEFAULT_IP_PORT     7002

static bool isValidIpAddress(QString ipAddress)
{
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ipAddress.toStdString().c_str(), &(sa.sin_addr));
    return result != 0;
}

static void showIPGui(QString &ip, uint &port)
{
    IPDialog d;

    d.setIP(ip);
    d.setPort(port);

    int result = d.exec();

    if (result != QDialog::Accepted)
        qFatal("[ERROR] User pressed 'cancel' - application stopped");

    ip = d.getIP();
    if (!isValidIpAddress(ip))
        qFatal("[ERROR] IP address is not valid");

    port = d.getPort();
}

static void getParameters(QApplication &a, QString &ip, uint &port)
{
    QCommandLineParser parser;
    parser.setApplicationDescription("OpenCN GUI");
    parser.addHelpOption();

    QCommandLineOption ipOption(QStringList() << "i" << "ip",
            QCoreApplication::translate("main", "target's IP address"),
            QCoreApplication::translate("main", "ip"));
    parser.addOption(ipOption);

    QCommandLineOption portOption(QStringList() << "p" << "port",
            QCoreApplication::translate("main", "target's IP port"),
            QCoreApplication::translate("main", "port"));
    parser.addOption(portOption);

    parser.process(a);

    if (parser.isSet(ipOption)) {

        // Get the IP value
        if (isValidIpAddress(parser.value(ipOption)))
            ip = parser.value(ipOption);
        else
            qFatal("[ERROR] IP address is not valid");

        // Get the port's value if set
        bool ok;
        if (parser.isSet(portOption)) {
            port = parser.value(portOption).toUInt(&ok, 10);
            if (!ok)
                qFatal("[ERROR] 'port' has to be a number ");
        }

    } else {
        // No IP set at cmd line - show GUI to set it
        showIPGui(ip, port);
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString  configFilePath = QDir::home().filePath("opencnConfig.conf");

	qDebug() << configFilePath;


    // Get application settings
    QSettings settings(configFilePath, QSettings::NativeFormat);
    settings.beginGroup("Comm_params");
    QString ip = settings.value("ip",   DEFAULT_IP_ADDR).toString();
    uint port  = settings.value("port", DEFAULT_IP_PORT).toUInt();

    getParameters(a, ip, port);

    // Save parameters
    settings.setValue("ip", ip);
    settings.setValue("port", port);

    capnp::EzRpcClient *client = new capnp::EzRpcClient(ip.toStdString(), port);

    MainWindow w(client);
    w.show();

    return a.exec();
}
