/* SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2021 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 */

#include "pins_transaction.h"


CMPinTransactions::CMPinTransactions(capnp::EzRpcClient* client) :
        _client(client), _cap{_client->getMain<CMCtlPins>()}
{
}

void CMPinTransactions::clear()
{
	_transactions.clear();
}

void CMPinTransactions::add(CMPin *pin, CMPinTransactionType transaction)
{
	// First, check is the new transaction is already present in the "vector"
	// It is expected that a PIN to be present only once in the vector
	for (const auto& trans : _transactions) {
		if (trans->_pin == pin)  {
			std::cout << "[CMPinTransactions] add() / transaction already in the 'transactions' vector" << std::endl;
			return;
		}
	}

	auto element = new CMPinTransactionsElement(pin, transaction);

	_transactions.push_back(element);
}

void CMPinTransactions::remove(CMPin *pin)
{
	int idx;
	bool found = false;

	for(size_t i = 0; i < _transactions.size(); i++) {
		if (_transactions[i]->_pin == pin)  {
			idx = i;
			found = true;
		}
	}

	if (found) {
		auto transaction = _transactions[idx];
		_transactions.erase(_transactions.begin()+idx);
		delete transaction;
	}
}

size_t CMPinTransactions::size()
{
	return _transactions.size();
}

void CMPinTransactions::execute()
{
	kj::WaitScope& waitScope = _client->getWaitScope();

	auto request = _cap.pinActionsRequest();

	auto pinActions = request.initActions(_transactions.size());

	for (size_t i = 0; i < _transactions.size(); i++) {
		pinActions[i].setName(_transactions[i]->_pin->name());

		if (_transactions[i]->_transaction == CMPinTransactionType::GET) {
			pinActions[i].setAction(CMCtlPins::PinAccess::GET_PIN);

		} else {
			pinActions[i].setAction(CMCtlPins::PinAccess::SET_PIN);

			auto val = pinActions[i].getValue().getValue();
			switch (_transactions[i]->_pin->type()) {
				case HAL_BIT:
					val.setB(*static_cast<CMPinBit *>(_transactions[i]->_pin));
					break;
				case HAL_FLOAT:
					val.setF(*static_cast<CMPinFloat *>(_transactions[i]->_pin));
					break;
				case HAL_S32:
					val.setS(*static_cast<CMPinI32 *>(_transactions[i]->_pin));
					break;
				case HAL_U32:
					val.setU( *static_cast<CMPinU32 *>(_transactions[i]->_pin));
					break;
			}
		}
	}

	auto promise = request.send();
	auto response = promise.wait(waitScope);

	auto pinResults = response.getResults();

	for(size_t i = 0; i < _transactions.size(); i++) {
		auto error = pinResults[i].getError();

		_transactions[i]->_pin->setError(error);

		if (_transactions[i]->_transaction == CMPinTransactionType::GET) {

			if (error == CMCTL_SUCCESS) {
				// Value updated only if the transaction succeeded

				auto val = pinResults[i].getValue().getValue();
				switch (_transactions[i]->_pin->type()) {
					case HAL_BIT:
						*static_cast<CMPinBit *>(_transactions[i]->_pin) = val.getB();
						break;
					case HAL_FLOAT:
						*static_cast<CMPinFloat *>(_transactions[i]->_pin) = val.getF();
						break;
					case HAL_S32:
						*static_cast<CMPinI32 *>(_transactions[i]->_pin) = val.getS();
						break;
					case HAL_U32:
						*static_cast<CMPinU32 *>(_transactions[i]->_pin) = val.getU();
						break;
				}
			}
		}
	}
}
