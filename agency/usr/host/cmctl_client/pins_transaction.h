/* SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2021 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 */

#ifndef PINS_TRANSACTIONS_H
#define PINS_TRANSACTIONS_H

#include <iostream>
#include <vector>
#include <capnp/ez-rpc.h>

#include "pins.h"

enum class CMPinTransactionType { GET, SET };

class CMPinTransactionsElement {

public:
    CMPinTransactionsElement(CMPin *pin, CMPinTransactionType transaction):
        _pin(pin), _transaction(transaction) {};

    CMPin *_pin;
    CMPinTransactionType _transaction;
};

class CMPinTransactions {

public:

    CMPinTransactions(capnp::EzRpcClient* client);

    void clear();

    void add(CMPin *pin, CMPinTransactionType transaction);

    void remove(CMPin *pin);

    void execute();

    size_t size();

protected:
    capnp::EzRpcClient* _client;
    CMCtlPins::Client _cap;

    std::vector<CMPinTransactionsElement *> _transactions;
};

#endif /* PINS_TRANSACTIONS_H */
