#include <pybind11/pybind11.h>
// #include <pybind11/operators.h>

#include <pins.h>
#include <pins_transaction.h>

namespace py = pybind11;

PYBIND11_MODULE(opencn, m) {
	py::class_<capnp::EzRpcClient>(m, "EzRpcClient")
        .def(py::init<std::string, uint>());

	py::class_<CMPin>(m, "CMPin")

        // .def(py::init<std::string, capnp::EzRpcClient*>())
		.def("name", &CMPin::name)
        .def("error", &CMPin::error)
        .def("setMode", &CMPin::setMode)
        .def("mode", &CMPin::mode);

    py::class_<CMPinI32, CMPin>(m, "CMPinI32")
        .def(py::init<std::string, capnp::EzRpcClient*>())
        .def("set", &CMPinI32::set)
        .def("get", &CMPinI32::get)
        .def( "__getattr__", [](CMPinI32& obj, std::string name) {if (name == "value") return  obj.operator int32_t(); else throw std::domain_error("wrong attribute");})
        .def("__setattr__", [](CMPinI32& obj, std::string name, int32_t value) { if (name == "value") obj.operator=(value);} );


    py::class_<CMPinU32, CMPin>(m, "CMPinU32")
        .def(py::init<std::string, capnp::EzRpcClient*>())
        .def("set", &CMPinU32::set)
        .def("get", &CMPinU32::get)
        .def( "__getattr__", [](CMPinU32& obj, std::string name) {if (name == "value") return  obj.operator uint32_t(); else throw std::domain_error("wrong attribute");})
        .def("__setattr__", [](CMPinU32& obj, std::string name, uint32_t value) { if (name == "value") obj.operator=(value);} );

    py::class_<CMPinBit, CMPin>(m, "CMPinBit")
        .def(py::init<std::string, capnp::EzRpcClient*>())
        .def("set", &CMPinBit::set)
        .def("get", &CMPinBit::get)
        .def( "__getattr__", [](CMPinBit& obj, std::string name) {if (name == "value") return  obj.operator bool(); else throw std::domain_error("wrong attribute");})
        .def("__setattr__", [](CMPinBit& obj, std::string name, bool value) { if (name == "value") obj.operator=(value);} );

    py::class_<CMPinFloat, CMPin>(m, "CMPinFloat")
        .def(py::init<std::string, capnp::EzRpcClient*>())
        .def("set", &CMPinFloat::set)
        .def("get", &CMPinFloat::get)
        .def( "__getattr__", [](CMPinFloat& obj, std::string name) {if (name == "value") return  obj.operator double(); else throw std::domain_error("wrong attribute");})
        .def("__setattr__", [](CMPinFloat& obj, std::string name, double value) { if (name == "value") obj.operator=(value);} );

	py::class_<CMPinTransactions>(m, "CMPinTransactions")
        .def(py::init<capnp::EzRpcClient*>())
        .def("add", &CMPinTransactions::add)
        .def("remove", &CMPinTransactions::remove)
        .def("clear", &CMPinTransactions::clear)
        .def("size", &CMPinTransactions::size)
        .def("execute", &CMPinTransactions::execute);
}
