/*
 * Copyright (C) 2021 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <iostream>
#include <string>
#include <chrono>
#include <thread>

#include <pins.h>
#include <pins_transaction.h>

#define DEFAULT_IP_PORT     7002

static void sleep_ms(int time)
{
	std::this_thread::sleep_for(std::chrono::milliseconds(time));
}

static void testPin(capnp::EzRpcClient *client)
{
	// PINs creation
	//   By default all PINs get/set values locally (no capnp call)
 	CMPinBit b_in{"loopback.0.b_in.3", client};
 	CMPinBit b_out{"loopback.0.b_out.3", client};
 	CMPinFloat f_in{"loopback.0.f_in.0", client};
 	CMPinFloat f_out{"loopback.0.f_out.0", client};
 	CMPinI32 s32_in{"loopback.0.s32_in.1", client};
 	CMPinI32 s32_out{"loopback.0.s32_out.1", client};
 	CMPinU32 u32_in{"loopback.0.u32_in.2", client};
 	CMPinU32 u32_out{"loopback.0.u32_out.2", client};

 	// Set u32_{in,out} PINS mode as PIN_VALUE
 	//		--> to get/set their values, a capnp call is performed
 	u32_in.setMode(CMPin::AccessMode::PIN);
 	u32_out.setMode(CMPin::AccessMode::PIN);

	std::cout << "PINs initial values" << std::endl <<
	        "  b_in:  "   <<  b_in << std::endl <<
	        "  b_out: "   <<  b_out << std::endl <<
	        "  f_in:  "   <<  f_in << std::endl <<
	        "  f_out: "   <<  f_out << std::endl <<
	        "  s32_in:  " <<  s32_in << std::endl <<
	        "  s32_out: " <<  s32_out << std::endl <<
	        "  u32_in:  " <<  u32_in << std::endl <<
	        "  u32_out: " <<  u32_out << std::endl <<
	        std::endl;

	b_in   = true;
	f_in   = 12.6;
	s32_in = -352;
	u32_in = 0xaaa;

	sleep_ms(500);

	std::cout << "PINs result values\n" <<
	        "  b_in:  "   <<  b_in << std::endl <<
	        "  b_out: "   <<  b_out << std::endl <<
	        "  f_in:  "   <<  f_in << std::endl <<
	        "  f_out: "   <<  f_out << std::endl <<
	        "  s32_in:  " <<  s32_in << std::endl <<
	        "  s32_out: " <<  s32_out << std::endl <<
	        "  u32_in:  " <<  u32_in << std::endl <<
	        "  u32_out: " <<  u32_out << std::endl <<
	        std::endl;

	// Force transaction with a call to 'set' methods
	b_in.set(true);
	f_in.set(12.6);
	s32_in.set(-352);
	u32_in.set( 0xaaa);

	sleep_ms(500);

	std::cout << "PINs result: in for transaction with a set().\n" <<
	        "  b_in:  "   <<  b_in << std::endl <<
	        "  b_out: "   <<  b_out << std::endl <<
	        "  f_in:  "   <<  f_in << std::endl <<
	        "  f_out: "   <<  f_out << std::endl <<
	        "  s32_in:  " <<  s32_in << std::endl <<
	        "  s32_out: " <<  s32_out << std::endl <<
	        "  u32_in:  " <<  u32_in << std::endl <<
	        "  u32_out: " <<  u32_out << std::endl <<
	        std::endl;

	std::cout << "Get PINs value with 'get' methods\n" <<
	        "  b_in:  "   <<  b_in.get() << std::endl <<
	        "  b_out: "   <<  b_out.get() << std::endl <<
	        "  f_in:  "   <<  f_in.get() << std::endl <<
	        "  f_out: "   <<  f_out.get() << std::endl <<
	        "  s32_in:  " <<  s32_in.get() << std::endl <<
	        "  s32_out: " <<  s32_out.get() << std::endl <<
	        "  u32_in:  " <<  u32_in.get() << std::endl <<
	        "  u32_out: " <<  u32_out.get() << std::endl <<
	        std::endl;

	// Reset PINs values & mode
	u32_in.setMode(CMPin::AccessMode::LOCAL);
 	u32_out.setMode(CMPin::AccessMode::LOCAL);

	b_in.set(false);
	f_in.set(0.0);
	s32_in.set(0);
	u32_in.set(0);

	sleep_ms(500);

	std::cout << "PINs value rested \n" <<
	        "  b_in:  "   <<  b_in.get() << std::endl <<
	        "  b_out: "   <<  b_out.get() << std::endl <<
	        "  f_in:  "   <<  f_in.get() << std::endl <<
	        "  f_out: "   <<  f_out.get() << std::endl <<
	        "  s32_in:  " <<  s32_in.get() << std::endl <<
	        "  s32_out: " <<  s32_out.get() << std::endl <<
	        "  u32_in:  " <<  u32_in.get() << std::endl <<
	        "  u32_out: " <<  u32_out.get() << std::endl <<
	        std::endl;

	// PINs transaction testing !
	CMPinTransactions setTransactions{client};
	CMPinTransactions getTransactions{client};

	setTransactions.add(&b_in,   CMPinTransactionType::SET);
	setTransactions.add(&f_in,   CMPinTransactionType::SET);
	setTransactions.add(&s32_in, CMPinTransactionType::SET);
	setTransactions.add(&u32_in, CMPinTransactionType::SET);

	getTransactions.add(&b_out,   CMPinTransactionType::GET);
	getTransactions.add(&f_out,   CMPinTransactionType::GET);
	getTransactions.add(&s32_out, CMPinTransactionType::GET);
	getTransactions.add(&u32_out, CMPinTransactionType::GET);

	b_in   = true;
	f_in   = 3.1415;
	s32_in = -1234;
	u32_in = 987654;

	setTransactions.execute();

	sleep_ms(500);

	std::cout << "Executed SET transactions\n" <<
	        "  b_in:  "   <<  b_in    << std::endl <<
	        "  b_out: "   <<  b_out   << std::endl <<
	        "  f_in:  "   <<  f_in    << std::endl <<
	        "  f_out: "   <<  f_out   << std::endl <<
	        "  s32_in:  " <<  s32_in  << std::endl <<
	        "  s32_out: " <<  s32_out << std::endl <<
	        "  u32_in:  " <<  u32_in  << std::endl <<
	        "  u32_out: " <<  u32_out << std::endl <<
	        std::endl;

	getTransactions.execute();

	std::cout << "Executed GET transactions\n" <<
	        "  b_in:  "   <<  b_in    << std::endl <<
	        "  b_out: "   <<  b_out   << std::endl <<
	        "  f_in:  "   <<  f_in    << std::endl <<
	        "  f_out: "   <<  f_out   << std::endl <<
	        "  s32_in:  " <<  s32_in  << std::endl <<
	        "  s32_out: " <<  s32_out << std::endl <<
	        "  u32_in:  " <<  u32_in  << std::endl <<
	        "  u32_out: " <<  u32_out << std::endl <<
	        std::endl;

	std::cout << "SET transactions size: " << setTransactions.size() << std::endl;

	// Try to add a PIN already in the Transaction
	setTransactions.add(&b_in, CMPinTransactionType::SET);
	std::cout << "Check SET transactions size (should be same as previous): "
	          << setTransactions.size() << std::endl;

	setTransactions.remove(&b_in);
	std::cout << "(After remove) SET transactions size : " << setTransactions.size() << std::endl;
}

static void show_usage(std::string name)
{
	std::cerr << "Usage: " << name << " IP [PORT]" << std::endl;
}

int main(int argc, char *argv[])
{
    unsigned int port;

	if ((argc < 2) || (argc > 3)) {
        show_usage(argv[0]);
        return 1;
    }

    std::string ip = argv[1];

    if (argc == 3)
    	port =  std::stoul(argv[2]);
    else
    	port = DEFAULT_IP_PORT;

    capnp::EzRpcClient *client = new capnp::EzRpcClient(ip, port);

	testPin(client);
}