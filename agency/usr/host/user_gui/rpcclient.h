#ifndef RPC_CLIENT_H
#define RPC_CLIENT_H

#include <string>
#include <fstream>
#include <iostream>
#include <experimental/optional>
#include <experimental/filesystem>

#include <capnp/ez-rpc.h>
#include <opencn/uapi/sampler.h>
#include "opencn_matlab_types.h"

#include "opencn-interface/opencn_interface.capnp.h"

template <typename T>
using Option = std::experimental::optional<T>;

#define CHUNK       1024*1024

typedef struct {
    double x;
    double y;
    double z;
} position_t;

typedef struct {
    bool x;
    bool y;
    bool z;
    bool spindle;
} currMode_t;

typedef enum {
    WRITE,
    READ
} file_ops_t;

struct Progress {
    int optimising_count;
    int optimising_progress;
    int resampling_progress;
};


class RpcClient {
public:
    RpcClient(capnp::EzRpcClient* client);

    Option<OpenCNServerInterface::CyclicData::Reader> getCyclicData();

    void setFeedoptCommitCfg(bool val);

    void setLcctSetMachineModeHoming(bool mode);
    void setLcctSetMachineModeStream(bool mode);
    void setLcctSetMachineModeJog(bool mode);
    void setLcctSetMachineModeInactive(bool mode);
    void setLcctSetMachineModeGCode(bool mode);

    void setStartHoming(bool mode);
    void setStopHoming(bool mode);

    void setHomePositionX(double position);
    void setHomePositionY(double position);
    void setHomePositionZ(double position);

    void setSpeedSpindle(double speed);
    void setActiveSpindle(bool mode);
    void setSpindleThreshold(int percent);

    void setJogX(bool mode);
    void setJogY(bool mode);
    void setJogZ(bool mode);

    void setRelJog(double value);
    void setPlusJog(bool plus);
    void setMinusJog(bool minus);

    void setAbsJog(double value);
    void setGoJog(bool mode);
    void setSpeedJog(double speed);
    void setStopJog(bool mode);

    void setOffset(double x, double y, double z, double c);

    void setStartStream(bool mode);
    void setStopStream(bool mode);
    void setPauseStream(bool mode);
    void setLoadStream(bool mode);

    void setGcodeStart(bool mode);
    void setGcodePause(bool mode);

    void setFaultReset(bool ready);

    void setFeedrateScale(double scale);
    void setFeedoptReset(bool reset);

    std::string readLog();

    void setFeedoptConfig(ocn::FeedoptConfig *cfg);
    ocn::FeedoptConfig getFeedoptConfig();

    // Toolpath channel/sample support
    bool toolpathStartChannel(int sampleRate);
    void toolpathStopChannel();
    int toolpathReadSamples(std::vector<sampler_sample_t>& samples);

    template <typename Function>
    int uploadFile(const std::string& srcFile, const std::string& destFile, Function progress_callback) {
        namespace fs = std::experimental::filesystem;
        uint8_t buf[CHUNK];
        try {
            auto srcSize = fs::file_size(srcFile); // this can throw

            int rc = sendFileParam(destFile.c_str(), srcSize, WRITE);
            if (rc != 0) {
                return rc;
            }

            size_t totalCount = 0;

            /* Read & send file data */
            std::ifstream file{srcFile, std::ios::binary};

            do {
                file.read((char *) buf, sizeof(buf));
                rc = sendFileData(buf, file.gcount());
                totalCount += file.gcount();
                std::cerr << "Upload: " << totalCount << "/" << srcSize << '\n';
                progress_callback(srcSize, totalCount);
            } while (file);

            return rc;
        }
        catch (fs::filesystem_error& e) {
            std::cerr << e.what() << '\n';
        }

        return 0;

    }


    bool pathExist(const std::string& path);
    int createFolder(std::string folderPath);
    void samplerNewFile();
    int getFileData(uint8_t *file_chunk);
    int sendFileParam(const char *fileName, uint32_t size, file_ops_t file_op);
    int sendFileData(uint8_t *data, size_t count);

private:
    capnp::EzRpcClient* _client;
    OpenCNServerInterface::Client cap;
};

#endif /* RPC_CLIENT_H */
