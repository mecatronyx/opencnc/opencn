#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QShortcut>
#include <QProgressDialog>
#include <QFontDatabase>

#include <cstdlib>
#include <cstring>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>

#include <thread>

#include "opencn-interface/opencn_interface.capnp.h"

#include "rpcclientqtintf.h"

#define MAX_MESSAGE_LEN 10000

using namespace std::chrono_literals;

enum TabIndex { Inactive, Homing, Jog, Stream, GCode};

MainWindow::MainWindow(RpcClient* client, QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow), _client(client)
{
	ui->setupUi(this);


    feedoptParam_w = new ParamsWindow(this);
	_timer = new QTimer(this);

	rpcClientQTIntf = new RpcClientQTIntf(_client);
	ui->openGLWidget_toolpath->setRpcClient(_client);


	targetEnvInit();

    {
        QSettings settings("opencn_gui.conf", QSettings::NativeFormat);
        ClientStreamerPath = settings.value("StreamFolder", QDir::homePath()+"/setpoints").toString();
        ClientGCodePath = settings.value("GcodeFolder", QDir::homePath()+"/gcode").toString();
        ClientLogsPath = settings.value("LogsFolder", QDir::homePath()+"/logs").toString();
        ClientTmpPath = settings.value("TmpFolder", QDir::homePath()+"/tmp").toString();

        bool autoDownloadLogs = settings.value("AutoDownloadLogs", false).toBool();

        ui->check_autodownloadlogs->setChecked(autoDownloadLogs);
    }

    _streamingFileDialog = new QFileDialog(this, "Select the streamer file", ClientStreamerPath, "*.txt");
    _gcodeFileDialog = new QFileDialog(this, "Select the ngc file", ClientGCodePath, "*.ngc");

	connect(ui->actionStreaming, &QAction::triggered, _streamingFileDialog, &QDialog::open);
	connect(_streamingFileDialog, &QFileDialog::fileSelected, this,
			&MainWindow::streamingFileChanged);

	connect(ui->actionG_code, &QAction::triggered, _gcodeFileDialog, &QDialog::open);
    connect(_gcodeFileDialog, &QFileDialog::fileSelected, this, &MainWindow::gcodeFileChanged);

    connect(ui->check_autodownloadlogs, &QCheckBox::stateChanged, [this](){
        QSettings settings("opencn_gui.conf", QSettings::NativeFormat);
        settings.setValue("AutoDownloadLogs", ui->check_autodownloadlogs->isChecked());
    });

	// Homing
	connect(ui->pushButton_startHoming, SIGNAL(pressed()), rpcClientQTIntf, SLOT(startHomingSlot()));
	connect(ui->pushButton_stopHoming, SIGNAL(pressed()), rpcClientQTIntf, SLOT(stopHomingSlot()));

	// Spindle
    connect(ui->doubleSpinBox_spindleSpeed_rpm, SIGNAL(valueChanged(double)), rpcClientQTIntf, SLOT(speedSpindleSlot(double)));
    connect(ui->check_spindleActive, SIGNAL(toggled(bool)), rpcClientQTIntf, SLOT(activeSpindleSlot(bool)));

	// Init spindle speed PIN at startup
    _client->setSpeedSpindle(ui->doubleSpinBox_spindleSpeed_rpm->value());

	// Init spindle state and air PINs values with checkboxes at startup
    _client->setActiveSpindle(ui->check_spindleActive->isChecked());

	// Jog axis select
	connect(ui->radioButton_XJog, SIGNAL(toggled(bool)), rpcClientQTIntf, SLOT(jogXSlot(bool)));
	connect(ui->radioButton_YJog, SIGNAL(toggled(bool)), rpcClientQTIntf, SLOT(jogYSlot(bool)));
	connect(ui->radioButton_ZJog, SIGNAL(toggled(bool)), rpcClientQTIntf, SLOT(jogZSlot(bool)));

	// Init Jog axis select PINs values with radiobuttons at startup
	_client->setJogX(ui->radioButton_XJog->isChecked());
	_client->setJogY(ui->radioButton_YJog->isChecked());
	_client->setJogZ(ui->radioButton_ZJog->isChecked());

	// Jog relative
	connect(ui->doubleSpinBox_incrJog_mm, SIGNAL(valueChanged(double)), rpcClientQTIntf, SLOT(relJogSlot(double)));
	connect(ui->pushButton_plusJog, SIGNAL(pressed()), rpcClientQTIntf, SLOT(plusJogSlot()));
	connect(ui->pushButton_minusJog, SIGNAL(pressed()), rpcClientQTIntf, SLOT(minusJogSlot()));

	// Init rel Jog incr speed PIN at startup
	_client->setRelJog(ui->doubleSpinBox_incrJog_mm->value());

	// JogAbs
	connect(ui->doubleSpinBox_absJog_mm, SIGNAL(valueChanged(double)), rpcClientQTIntf, SLOT(absJogSlot(double)));
	connect(ui->pushButton_go_absJog, SIGNAL(pressed()), rpcClientQTIntf, SLOT(goJogSlot()));

	// Init abs Jog incr PIN at startup
	_client->setAbsJog(ui->doubleSpinBox_absJog_mm->value());

	// Jog speed
	connect(ui->doubleSpinBox_speedJog_mm_min, SIGNAL(valueChanged(double)), rpcClientQTIntf, SLOT(speedJogSlot(double)));
	connect(ui->pushButton_stopJog, SIGNAL(pressed()), rpcClientQTIntf, SLOT(stopJogSlot()));

	// Init Jog speed PIN at startup
	_client->setSpeedJog(ui->doubleSpinBox_speedJog_mm_min->value());

    offset_changed(0);

    connect(ui->doubleSpinBox_xOffset, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_yOffset, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_zOffset, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_cOffset, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));

    connect(ui->doubleSpinBox_xCorrection, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_yCorrection, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_zCorrection, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_cCorrection, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));

    connect(ui->doubleSpinBox_xHome, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_yHome, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_zHome, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_cHome, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));

	// Fault Reset
	connect(ui->pushButton_fault, SIGNAL(pressed()), rpcClientQTIntf, SLOT(faultResetSlot()));

    ui->doubleSpinBox_currPosX->setValue(0);
    ui->doubleSpinBox_currPosY->setValue(0);
    ui->doubleSpinBox_currPosZ->setValue(0);

	connect(_timer, &QTimer::timeout, this, &MainWindow::timer_update);

	connect(ui->push_clearpath, SIGNAL(pressed()), ui->openGLWidget_toolpath->getPath(), SLOT(reset()));
    connect(ui->push_gcodeopen, &QPushButton::pressed, _gcodeFileDialog, &QDialog::open);
	connect(ui->push_streamopen, &QPushButton::pressed, _streamingFileDialog, &QDialog::open);

    connect(ui->push_gcodereset, SIGNAL(pressed()), rpcClientQTIntf, SLOT(feedoptResetSlot()));

	connect(this, SIGNAL(destroyed()), rpcClientQTIntf, SLOT(streamGcodeStopSlot()));

    connect(ui->spin_spindle_threshold, &QSpinBox::editingFinished, [this](){
        _client->setSpindleThreshold(ui->spin_spindle_threshold->value());
        QSettings settings("opencn_gui.conf", QSettings::NativeFormat);
        settings.setValue("SpindleThreshold", ui->spin_spindle_threshold->value());
    });

    // try to load values from opencn_gui.conf
    feedoptParam_w->loadConfig();

	// Feedopt
	ui->progress_resampling->setMaximum(0);
	ui->progress_optimising->setValue(0);
    ui->progress_samples->setMaximum(0);
	ui->progress_samples->setValue(0);

	auto* shortcut = new QShortcut(QKeySequence(Qt::Key_Space), this);
	connect(shortcut, SIGNAL(activated()), this, SLOT(on_space_pressed()));

    ui->slider_manual_override->setValue(100);

    {
        QSettings settings("opencn_gui.conf", QSettings::NativeFormat);
        int percent = settings.value("SpindleThreshold", SPINDLE_DEF_VEL_THRESH).toInt();

        ui->spin_spindle_threshold->setValue(percent);
        _client->setSpindleThreshold(percent);
    }

	_timer->setInterval(100);
	_timer->start();
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::targetEnvInit()
{
    if (!_client->pathExist(GCodePath.toStdString())) {
        _client->createFolder(GCodePath.toStdString());
	}

	if (!_client->pathExist(StreamerFilePath.toStdString())) {
		_client->createFolder(StreamerFilePath.toStdString());
 	}

 	if (!_client->pathExist(LogFilePath.toStdString())) {
		_client->createFolder(LogFilePath.toStdString());
	}
}

void MainWindow::streamingFileChanged(const QString &path)
{
    qDebug() << "streamingFileChanged";
	ui->line_streamerfile->setText(path);
	ui->openGLWidget_toolpath->getPart()->loadStreamingFile(path);

    loadStream();

	QSettings settings("opencn_gui.conf", QSettings::NativeFormat);
    settings.beginGroup("Paths");
	ClientStreamerPath = path.left(path.lastIndexOf("/"));
	settings.setValue("StreamFolder", ClientStreamerPath);
}

void MainWindow::gcodeFileChanged(const QString &path)
{
    QFile file{path};
    char line[240];
    char line_with_number[256];
    ui->text_gcode_listing->setStyleSheet("QListView::item { height: 20px; }");
    ui->text_gcode_listing->clear();
    ui->text_gcode_listing->setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
    int line_number = 1;
    if (file.open(QFile::ReadOnly | QFile::Text)) {
        int count = 0;
        while((count = file.readLine(line, sizeof(line))) >= 0) {
            line[count-1] = 0;
            snprintf(line_with_number, sizeof(line_with_number), "%d: %s", line_number, line);
            ui->text_gcode_listing->addItem(line_with_number);
            line_number++;
        }
    }

    ui->line_gcodesource->setText(path);
    ui->openGLWidget_toolpath->getPart()->loadGCodeFile(path);
    usleep(10000);

    rpcClientQTIntf->feedoptResetSlot();
//    while(_client->getFeedoptQueueSize() > 0) {
//        usleep(1000);
//    }

    QSettings settings("opencn_gui.conf", QSettings::NativeFormat);
    settings.beginGroup("Paths");
    ClientGCodePath = path.left(path.lastIndexOf("/"));
    settings.setValue("GcodeFolder", ClientGCodePath);

    std::this_thread::sleep_for(0.1s);
    commit();
}

void MainWindow::timer_update()
{
    _timer->setInterval(1000 / 30);

    const auto& maybe_data = _client->getCyclicData();
    if (!maybe_data) return;

    const auto& data = maybe_data.value();

    if (data.getGcodeFinished()) {
        if (ui->check_autodownloadlogs->isChecked()) {
            downloadLogs();
        }

        rpcClientQTIntf->feedoptResetSlot();
        std::this_thread::sleep_for(0.1s);
        commit();
    }


    if (data.getStreamFinished()) {
        if (ui->check_autodownloadlogs->isChecked()) {
            downloadLogs();
        }

        streamingFileChanged(ui->line_streamerfile->text());
    }

    ui->text_gcode_listing->setCurrentRow(data.getCurrentGCodeLine() - 1);

    _client->setFeedrateScale(ui->slider_manual_override->value()/100.0);
    ui->progress_auto_override->setValue(data.getFeedoptStepDt()*100);

    can_change_tab = canChangeTab(data);
	ui->push_setinactive->setEnabled(can_change_tab);
    ui->push_sethoming->setEnabled(can_change_tab);
    ui->push_setjog->setEnabled(can_change_tab);
    ui->push_setstream->setEnabled(can_change_tab);
    ui->push_setgcode->setEnabled(can_change_tab);


    ui->doubleSpinBox_currPosX->setValue(data.getCurrentPosition().getX());
    ui->doubleSpinBox_currPosY->setValue(data.getCurrentPosition().getY());
    ui->doubleSpinBox_currPosZ->setValue(data.getCurrentPosition().getZ());
    ui->doubleSpinBox_currVelSpindle->setValue(data.getSpindleVelocity()*60);

    const auto progress = data.getFeedoptProgress();

    ui->progress_optimising->setMaximum(progress.getOptimisingCount());
    ui->progress_optimising->setValue(progress.getOptimisingProgress());

    ui->progress_resampling->setMaximum(progress.getResamplingCount());
    ui->progress_resampling->setValue(progress.getResamplingProgress());

    ui->progress_samples->setMaximum(data.getFeedoptQueueMax());
    ui->progress_samples->setValue(data.getFeedoptQueueSize());

    const auto mode_csp = data.getAxisMode().getCsp();
    const auto mode_csv = data.getAxisMode().getCsv();
    const auto mode_homing = data.getAxisMode().getHoming();
    const auto mode_inactive = data.getAxisMode().getInactive();
    const auto mode_fault = data.getAxisMode().getFault();

    if (mode_csp.getX()) {
		ui->label_axisXMode->setText("<font color=#00AA00>CSP</font>");
    } else if (mode_csv.getX()) {
		ui->label_axisXMode->setText("<font color=#00AA00>CSV</font>");
    } else if (mode_homing.getX()) {
		ui->label_axisXMode->setText("<font color=#00AA00>HOMING</font>");
    } else if (mode_inactive.getX()) {
		ui->label_axisXMode->setText("<font color=#000000>INACTIVE</font>");
    } else if (mode_fault.getX()) {
		ui->label_axisXMode->setText("<font color=#FA0000>FAULT</font>");
	} else {
		ui->label_axisXMode->setText("<font color=#000000>UNKNOWN</font>");
	}

	// Display Y axis current Mode
    if (mode_csp.getY()) {
        ui->label_axisYMode->setText("<font color=#00AA00>CSP</font>");
    } else if (mode_csv.getY()) {
        ui->label_axisYMode->setText("<font color=#00AA00>CSV</font>");
    } else if (mode_homing.getY()) {
        ui->label_axisYMode->setText("<font color=#00AA00>HOMING</font>");
    } else if (mode_inactive.getY()) {
        ui->label_axisYMode->setText("<font color=#000000>INACTIVE</font>");
    } else if (mode_fault.getY()) {
        ui->label_axisYMode->setText("<font color=#FA0000>FAULT</font>");
    } else {
        ui->label_axisYMode->setText("<font color=#000000>UNKNOWN</font>");
    }

	// Display Z axis current Mode
    if (mode_csp.getZ()) {
        ui->label_axisZMode->setText("<font color=#00AA00>CSP</font>");
    } else if (mode_csv.getZ()) {
        ui->label_axisZMode->setText("<font color=#00AA00>CSV</font>");
    } else if (mode_homing.getZ()) {
        ui->label_axisZMode->setText("<font color=#00AA00>HOMING</font>");
    } else if (mode_inactive.getZ()) {
        ui->label_axisZMode->setText("<font color=#000000>INACTIVE</font>");
    } else if (mode_fault.getZ()) {
        ui->label_axisZMode->setText("<font color=#FA0000>FAULT</font>");
    } else {
        ui->label_axisZMode->setText("<font color=#000000>UNKNOWN</font>");
    }

	// Display Spindle current Mode
    if (mode_csp.getSpindle()) {
        ui->label_SpindleMode->setText("<font color=#00AA00>CSP</font>");
    } else if (mode_csv.getSpindle()) {
        ui->label_SpindleMode->setText("<font color=#00AA00>CSV</font>");
    } else if (mode_homing.getSpindle()) {
        ui->label_SpindleMode->setText("<font color=#00AA00>HOMING</font>");
    } else if (mode_inactive.getSpindle()) {
        ui->label_SpindleMode->setText("<font color=#000000>INACTIVE</font>");
    } else if (mode_fault.getSpindle()) {
        ui->label_SpindleMode->setText("<font color=#FA0000>FAULT</font>");
    } else {
        ui->label_SpindleMode->setText("<font color=#000000>UNKNOWN</font>");
    }

    if (data.getHomed()) {
		ui->label_ishomed->setText("<font color=#00AA00>HOMED</font>");
	} else {
		ui->label_ishomed->setText("<font color=#FA0000>NOT HOMED</font>");
	}

	while (true) {
		std::string message = _client->readLog();
		if (message.length() == 0)
			break;

		QString str_msg = QString::fromStdString(message);
		ui->listWidget_logs->addItem(str_msg.trimmed());
		ui->listWidget_logs->scrollToBottom();
	}

    ui->doubleSpinBox_spindleSpeed_rpm->setDisabled(false);
    ui->frame_Gcode->hide();

    if (data.getMachineMode().getInactive()) {
        ui->tabWidget->setCurrentIndex(TabIndex::Inactive);
    } else if (data.getMachineMode().getHoming()) {
		ui->tabWidget->setCurrentIndex(TabIndex::Homing);
    } else if (data.getMachineMode().getStream()) {
		ui->tabWidget->setCurrentIndex(TabIndex::Stream);
        ui->FIFO_streamer_bar->setValue(data.getStreamerFIFO());
    } else if (data.getMachineMode().getJog()) {
		ui->tabWidget->setCurrentIndex(TabIndex::Jog);
    } else if (data.getMachineMode().getGcode()) {
		ui->tabWidget->setCurrentIndex(TabIndex::GCode);
        ui->doubleSpinBox_spindleSpeed_rpm->setDisabled(true);
        ui->frame_Gcode->show();
		// ui->push_gcodecommit->setEnabled(_client->getGcodeFinished());
	}

    ui->tabWidget->setTabEnabled(TabIndex::Stream, data.getHomed());
    ui->tabWidget->setTabEnabled(TabIndex::GCode, data.getHomed());

    ui->pushButton_go_absJog->setEnabled(data.getHomed());

	/* ================== FEEDOPT ================== */

    ui->push_gcodestart->setEnabled(data.getFeedoptReady());

	ui->openGLWidget_toolpath->update();

    // ui->push_gcodeopen->setEnabled(!_client->getFeedoptUsActive() && !_client->getFeedoptRtActive());
}

void MainWindow::commit()
{
    _timer->stop();
    QString gcode_host_filepath = ui->line_gcodesource->text();

    qDebug() << "Uploading " << gcode_host_filepath;

    _client->uploadFile(gcode_host_filepath.toStdString(), "/root/gcode.ngc", [](auto, auto){});

    feedoptConfig = feedoptParam_w->getConfig();

	int filename_size[2] = {1, static_cast<int>(strlen("/root/gcode.ngc"))};
	ocn::ConfigSetSource(&feedoptConfig, "/root/gcode.ngc", filename_size);

    {
        QFile tmp_file{"header.txt"};
        if (!tmp_file.open(QFile::WriteOnly | QFile::Text)) {
            print_to_log("[GUI] Unable to create file: " + tmp_file.fileName());
            return;
        }

        QTextStream txt{&tmp_file};

        txt << "# File: "   << gcode_host_filepath << "\n"
            << "# Time: "   << QDateTime::currentDateTime().toString() << "\n"
            << "# NHorz: "  << feedoptConfig.NHorz << "\n"
            << "# NDiscr: " << feedoptConfig.NDiscr << "\n"
            << "# NBreak: " << feedoptConfig.NBreak << "\n"

            << "# LSplit [mm]: "    << feedoptConfig.LSplit << "\n"
            << "# CutOff [mm]: "    << feedoptConfig.CutOff << "\n"

            << "# amax X [mm/s^2]: " << feedoptConfig.amax[0] << "\n"
            << "# amax Y [mm/s^2]: " << feedoptConfig.amax[1] << "\n"
            << "# amax Z [mm/s^2]: " << feedoptConfig.amax[2] << "\n"

            << "# jmax X [mm/s^3]: " << feedoptConfig.jmax[0] << "\n"
            << "# jmax Y [mm/s^3]: " << feedoptConfig.jmax[1] << "\n"
            << "# jmax Z [mm/s^3]: " << feedoptConfig.jmax[2] << "\n";

    }

    qDebug() << "Uploading header";
    _client->uploadFile("header.txt", "/root/header.txt", [](auto, auto){});

    _client->setFeedoptConfig(&feedoptConfig);
	_client->setFeedoptCommitCfg(true);
	_timer->start();
}

void MainWindow::print_to_log(QString msg)
{
    qDebug() << msg;
	ui->listWidget_logs->addItem(msg.trimmed());
	ui->listWidget_logs->scrollToBottom();
}

void MainWindow::on_push_gcoderefresh_clicked()
{
    qDebug() << "Refresh";
	// save the same params to tmp file for writing to logfile
	feedoptConfig = _client->getFeedoptConfig();
    feedoptParam_w->setConfig(feedoptConfig);
}

void MainWindow::on_push_gcodepause_clicked()
{
    _client->setGcodePause(true);
}

void MainWindow::on_push_gcodestart_clicked()
{
    _client->samplerNewFile();
    _client->setGcodeStart(true);

}

void MainWindow::on_space_pressed()
{
	_client->setGcodePause(true);
	_client->setStopJog(true);
}

void MainWindow::on_push_streamstart_clicked()
{
    _client->samplerNewFile();
    _client->setStartStream(true);
}

void MainWindow::on_push_streamstop_clicked()
{
    _client->setStopStream(true);
}

void MainWindow::loadStream()
{

    if (!ui->line_streamerfile->text().isEmpty()) {
        qDebug() << "Uploading " << ui->line_streamerfile->text();
        _client->uploadFile(ui->line_streamerfile->text().toStdString(), "/root/stream.txt", [](auto, auto){});
        {
            QFile tmp_file{"header.txt"};
            if (!tmp_file.open(QFile::WriteOnly|QFile::Text)) {
                print_to_log("[GUI] Unable to create file: " + tmp_file.fileName());
                return;
            }

            QTextStream txt{&tmp_file};
            txt << "# File: " << ui->line_streamerfile->text() << "\n"
                << "# Time: " << QDateTime::currentDateTime().toString() << "\n";
        }

        qDebug() << "Uploading header";
        _client->uploadFile("./header.txt", "/root/header.txt", [](auto, auto){});
        _client->setLoadStream(true);
    } else {
        print_to_log("Please specify streamer filename");
    }
}

void MainWindow::downloadLogs()
{
    QString str_msg;
    uint8_t file_chunk[CHUNK];
    int nbytes = 0;

    QString prefix = "log";
    if (!ui->line_log_prefix->text().isEmpty()) {
        prefix = ui->line_log_prefix->text();
    }

    QString filename_source = "_";
    if (_client->getCyclicData()) {
        const auto& data = _client->getCyclicData().value();
        if (data.getMachineMode().getStream()) {
            const auto parts = ui->line_gcodesource->text().split(QRegExp(R"((\.\/))"), QString::SkipEmptyParts);
            if (parts.size() >= 2) {
                filename_source = "_" + parts[parts.size()-2] + "_";
            }
        }
        else if (data.getMachineMode().getGcode()) {
            const auto parts = ui->line_gcodesource->text().split(QRegExp(R"((\.|\/))"), QString::SkipEmptyParts);
            if (parts.size() >= 2) {
                filename_source = "_" + parts[parts.size()-2] + "_";
            }
        }
    }

    const QString filename_client = prefix + filename_source + QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss") + ".csv";
    QFile file{filename_client};

    if (!file.open(QFile::WriteOnly)) {
        print_to_log("[GUI]: Download failed - unable to create " + filename_client);
        return;
    }

    print_to_log("Downloading logs");

    QMessageBox box;
    box.setModal(true);
    box.setText("Downloading logs");
    box.show();

    if (_client->sendFileParam(TARGET_LOGS_FILENAME, 0, READ)) {
        print_to_log("[SERVER] Error reading file");
        return;
    }

    while (int len = _client->getFileData(file_chunk)) {
        nbytes += len;
        file.write((const char*)file_chunk, len);
    }

    print_to_log("Download finished");
    box.hide();
}

void MainWindow::on_push_setinactive_clicked()
{
    qDebug() << "Push Inactive";
    if (can_change_tab)
        _client->setLcctSetMachineModeInactive(true);
}


void MainWindow::on_push_sethoming_clicked()
{
    qDebug() << "Push Homing";
    if (can_change_tab)
        _client->setLcctSetMachineModeHoming(true);
}


void MainWindow::on_push_setjog_clicked()
{
    qDebug() << "Push Jog";
    if (can_change_tab)
        _client->setLcctSetMachineModeJog(true);
}


void MainWindow::on_push_setstream_clicked()
{
    qDebug() << "Push Stream";
    if (can_change_tab)
        _client->setLcctSetMachineModeStream(true);
}


void MainWindow::on_push_setgcode_clicked()
{
    qDebug() << "Push GCode";
    if (can_change_tab)
        _client->setLcctSetMachineModeGCode(true);
}

bool MainWindow::canChangeTab(const OpenCNServerInterface::CyclicData::Reader& data)
{
    switch (ui->tabWidget->currentIndex()) {
        case TabIndex::Jog:
            return data.getJogFinished();
        case TabIndex::Stream:
            return !data.getStreamRunning();
        case TabIndex::Homing:
            return data.getHomingFinished();
        case TabIndex::GCode:
            return !data.getGcodeRunning();
        case TabIndex::Inactive:
            return true;
    }
    return false;
}

void MainWindow::on_push_gcodeparams_clicked()
{
    feedoptParam_w->show();
}

void MainWindow::on_push_downloadlogs_clicked()
{
    downloadLogs();
}

void MainWindow::offset_changed(double)
{
    _client->setOffset(ui->doubleSpinBox_xOffset->value(),
                       ui->doubleSpinBox_yOffset->value(),
                       ui->doubleSpinBox_zOffset->value(),
                       ui->doubleSpinBox_cOffset->value());

    _client->setHomePositionX(ui->doubleSpinBox_xHome->value());
    _client->setHomePositionY(ui->doubleSpinBox_yHome->value());
    _client->setHomePositionZ(ui->doubleSpinBox_zHome->value());

    ui->openGLWidget_toolpath->getPart()->offsetXChange(ui->doubleSpinBox_xOffset->value());
    ui->openGLWidget_toolpath->getPart()->offsetYChange(ui->doubleSpinBox_yOffset->value());
    ui->openGLWidget_toolpath->getPart()->offsetZChange(ui->doubleSpinBox_zOffset->value());
    ui->openGLWidget_toolpath->getPart()->offsetThetaZChange(ui->doubleSpinBox_cOffset->value());
}

