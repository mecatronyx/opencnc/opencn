/*
 * Copyright (C) 2019 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <QDebug>

#include "rpcclientqtintf.h"

RpcClientQTIntf::RpcClientQTIntf(RpcClient *client) : _client(client)
{
    qDebug() << "RpcClientQTIntf done" << endl;
}

RpcClientQTIntf::~RpcClientQTIntf() {};

void RpcClientQTIntf::startHomingSlot()
{
    _client->setStartHoming(true);
}

void RpcClientQTIntf::stopHomingSlot()
{
    _client->setStopHoming(true);
}


void RpcClientQTIntf::speedSpindleSlot(double speed)
{
	_client->setSpeedSpindle(speed);
}

void RpcClientQTIntf::activeSpindleSlot(bool mode)
{
	_client->setActiveSpindle(mode);
}

void RpcClientQTIntf::jogXSlot(bool mode)
{
	_client->setJogX(mode);
}

void RpcClientQTIntf::jogYSlot(bool mode)
{
	_client->setJogY(mode);
}

void RpcClientQTIntf::jogZSlot(bool mode)
{
	_client->setJogZ(mode);
}

void RpcClientQTIntf::relJogSlot(double value)
{
	_client->setRelJog(value);
}

void RpcClientQTIntf::plusJogSlot()
{
	_client->setPlusJog(true);
}

void RpcClientQTIntf::minusJogSlot()
{
	_client->setMinusJog(true);
}

void RpcClientQTIntf::absJogSlot(double value)
{
	_client->setAbsJog(value);
}

void RpcClientQTIntf::goJogSlot()
{
	_client->setGoJog(true);
}

void RpcClientQTIntf::speedJogSlot(double value)
{
	_client->setSpeedJog(value);
}

void RpcClientQTIntf::stopJogSlot()
{
	_client->setStopJog(true);
}


void RpcClientQTIntf::gcodeStartSlot()
{
	_client->setGcodeStart(true);
}

void RpcClientQTIntf::faultResetSlot()
{
	_client->setFaultReset(true);
}

void RpcClientQTIntf::feedoptResetSlot()
{
	_client->setFeedoptReset(true);
}

void RpcClientQTIntf::streamGcodeStopSlot()
{
    _client->setStopStream(true);
    _client->setFeedoptReset(true);
}
