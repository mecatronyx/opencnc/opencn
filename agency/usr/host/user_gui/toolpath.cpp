#include "toolpath.h"

#include <GL/glu.h>
#include <QDebug>
#include <fcntl.h>

const unsigned int ToolPath::updateIntervalMs = 1000/60;
const unsigned int ToolPath::maxPoints = 4096*4;
const unsigned int ToolPath::sampleRate = 10;

ToolPath::ToolPath(QObject *parent)
    : QObject(parent), _channelstarted(false), _points(nullptr), _pointCount(0), _writeIdx(0), _readIdx(0),
      _overlapped(false), _client(nullptr)
{
    _updateTimer = new QTimer(this);

    connect(_updateTimer, &QTimer::timeout, this, &ToolPath::update);

    _points = new QVector3D[maxPoints];
}

ToolPath::~ToolPath()
{
    stop();
    delete[] _points;
}

void ToolPath::setRpcClient(RpcClient *client)
{
    _client = client;
}

bool ToolPath::start()
{
    int ret;

    if (_channelstarted) {
        qDebug() << "Toolpath channel already started.";
        return false;
    }

    ret = _client->toolpathStartChannel(sampleRate);
    if (!ret) {
        qDebug() << "toolpathStartChannel command failed";
        return false;
    }

    _channelstarted = true;

    _updateTimer->start(updateIntervalMs);

    return true;
}

void ToolPath::stop()
{
    if (_channelstarted) {
        _client->toolpathStopChannel();
        _channelstarted = false;
    }

    _updateTimer->stop();
}

void ToolPath::reset()
{
    _writeIdx = 0;
    _readIdx = 0;
    _overlapped = false;
}

QVector3D ToolPath::getLastPosition()
{
    unsigned int idx = _writeIdx == 0 ? maxPoints - 1 : _writeIdx - 1;
    return _points[idx];
}

void ToolPath::draw()
{
    if (_readIdx == _writeIdx) {
        // Empty
        return;
    }

    glPushMatrix();
    glTranslatef(0.0f, 0.00001f, 0.0f);
    glEnable(GL_COLOR_MATERIAL);
    glColor3f(1.0f, 0.0f, 0.0f);

    glLineWidth(2.0f);
    glBegin(GL_LINE_STRIP);

    unsigned int i = _readIdx;
    do {
        glVertex3f(_points[i].x(), -_points[i].y(), _points[i].z());

        i++;
        if (i >= maxPoints) {
            i = 0;
        }
    } while (i != _writeIdx);

    glEnd();

    glDisable(GL_COLOR_MATERIAL);
    glPopMatrix();
}

void ToolPath::update()
{
    static std::vector<sampler_sample_t> samples;

    _client->toolpathReadSamples(samples);

    if (!samples.empty()) {

        if (samples[0].n_pins < 3) {
            qDebug() << "Error format point: ";
        } else {
            for(auto& sample: samples) {
                _points[_writeIdx].setX(sample.pins[0].f / 1000.0f);
                _points[_writeIdx].setY(sample.pins[2].f / 1000.0f);
                _points[_writeIdx].setZ(-sample.pins[1].f / 1000.0f);


                _writeIdx++;

                if (_writeIdx == maxPoints) {
                    _writeIdx = 0;
                    _overlapped = true;
                }

                if (_overlapped) {
                    _readIdx = _writeIdx + 1;
                    _readIdx = _readIdx < maxPoints ? _readIdx : 0;
                }
            }
        }
    }

    if (!samples.empty()) {
        emit isUpdated();
    }
}
