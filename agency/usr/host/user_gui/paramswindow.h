#ifndef PARAMSWINDOW_H
#define PARAMSWINDOW_H

#include <QMainWindow>
#include "opencn_matlab_types.h"

namespace Ui {
class ParamsWindow;
}

class ParamsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ParamsWindow(QWidget *parent = nullptr);
    ~ParamsWindow();

    ocn::FeedoptConfig getConfig();
    void setConfig(ocn::FeedoptConfig cfg);

public slots:
    void saveConfig();
    void loadConfig();
    void loadFactoryConfig();

private:
    Ui::ParamsWindow *ui;
};

#endif // PARAMSWINDOW_H
