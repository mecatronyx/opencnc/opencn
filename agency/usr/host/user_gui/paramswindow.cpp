#include "paramswindow.h"
#include "ui_paramswindow.h"

#include <QDebug>
#include <QSettings>

ParamsWindow::ParamsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ParamsWindow)
{
    ui->setupUi(this);

    connect(ui->push_save, &QPushButton::clicked, this, &ParamsWindow::saveConfig);
    connect(ui->push_load, &QPushButton::clicked, this, &ParamsWindow::loadConfig);
    connect(ui->push_factory, &QPushButton::clicked, this, &ParamsWindow::loadFactoryConfig);
}

ParamsWindow::~ParamsWindow()
{
    delete ui;
}

ocn::FeedoptConfig ParamsWindow::getConfig()
{
    qDebug() << "ParamsWindow::getConfig";
    ocn::FeedoptConfig cfg{};

    cfg.NHorz = ui->spin_nhorz->value();
    cfg.NDiscr = ui->spin_ndiscr->value();
    cfg.NBreak = ui->spin_nbreak->value();

    cfg.LSplit = ui->spin_lsplit->value();
    cfg.CutOff = ui->spin_cutoff->value();

//    cfg.vmax = ui->spin_vmax->value()/60;

    cfg.amax[0] = ui->spin_amax_x->value();
    cfg.amax[1] = ui->spin_amax_y->value();
    cfg.amax[2] = ui->spin_amax_z->value();

    cfg.jmax[0] = ui->spin_jmax_x->value();
    cfg.jmax[1] = ui->spin_jmax_y->value();
    cfg.jmax[2] = ui->spin_jmax_z->value();

    return cfg;
}

void ParamsWindow::setConfig(ocn::FeedoptConfig cfg)
{
    qDebug() << "ParamsWindow::setConfig";

    ui->spin_nhorz->setValue(cfg.NHorz);
    ui->spin_ndiscr->setValue(cfg.NDiscr);
    ui->spin_nbreak->setValue(cfg.NBreak);

    ui->spin_lsplit->setValue(cfg.LSplit);
    ui->spin_cutoff->setValue(cfg.CutOff);

//    ui->spin_vmax->setValue(cfg.vmax);

    ui->spin_amax_x->setValue(cfg.amax[0]);
    ui->spin_amax_y->setValue(cfg.amax[1]);
    ui->spin_amax_z->setValue(cfg.amax[2]);

    ui->spin_jmax_x->setValue(cfg.jmax[0]);
    ui->spin_jmax_y->setValue(cfg.jmax[1]);
    ui->spin_jmax_z->setValue(cfg.jmax[2]);
}

void ParamsWindow::saveConfig()
{
    auto cfg = getConfig();

    QSettings settings("opencn_gui.conf", QSettings::NativeFormat);
    settings.beginGroup("OptimizationParams");
    settings.setValue("NHorz", cfg.NHorz);
    settings.setValue("NDiscr", cfg.NDiscr);
    settings.setValue("NBreak", cfg.NBreak);

    settings.setValue("LSplit", cfg.LSplit);
    settings.setValue("CutOff", cfg.CutOff);

    settings.setValue("amax0", cfg.amax[0]);
    settings.setValue("amax1", cfg.amax[1]);
    settings.setValue("amax2", cfg.amax[2]);

    settings.setValue("jmax0", cfg.jmax[0]);
    settings.setValue("jmax1", cfg.jmax[1]);
    settings.setValue("jmax2", cfg.jmax[2]);
}

void ParamsWindow::loadConfig()
{
    ocn::FeedoptConfig cfg;
    QSettings settings("opencn_gui.conf", QSettings::NativeFormat);
    settings.beginGroup("OptimizationParams");
    cfg.NHorz = settings.value("NHorz", 5).toInt();
    cfg.NDiscr = settings.value("NDiscr", 20).toInt();
    cfg.NBreak = settings.value("NBreak", 10).toInt();

    cfg.LSplit = settings.value("LSplit", 3.0).toDouble();
    cfg.CutOff = settings.value("CutOff", 0.1).toDouble();

    cfg.amax[0] = settings.value("amax0", 20000.0).toDouble();
    cfg.amax[1] = settings.value("amax1", 20000.0).toDouble();
    cfg.amax[2] = settings.value("amax2", 20000.0).toDouble();

    cfg.jmax[0] = settings.value("jmax0", 1500000.0).toDouble();
    cfg.jmax[1] = settings.value("jmax1", 1500000.0).toDouble();
    cfg.jmax[2] = settings.value("jmax2", 1500000.0).toDouble();

    setConfig(cfg);

}

void ParamsWindow::loadFactoryConfig()
{
    ocn::FeedoptConfig cfg;
    cfg.NHorz = 5;
    cfg.NDiscr = 20;
    cfg.NBreak = 10;

    cfg.LSplit = 3.0;
    cfg.CutOff = 0.1;

    cfg.amax[0] = 20000;
    cfg.amax[1] = 20000;
    cfg.amax[2] = 20000;

    cfg.jmax[0] = 1500000;
    cfg.jmax[1] = 1500000;
    cfg.jmax[2] = 1500000;

    setConfig(cfg);
}
