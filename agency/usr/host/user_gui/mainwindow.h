#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QTimer>
#include <QVBoxLayout>

#include <QFileDialog>
#include <QMessageBox>
#include <QThread>

#include "paramswindow.h"

#include "rpcclient.h"
#include "rpcclientqtintf.h"

#include "ConfigSetSource.h"

#define TARGET_USER_HOME    "/root/"
#define TARGET_GCODE_FOLDER_PATH TARGET_USER_HOME"gcode"
#define TARGET_STREAMER_FOLDER_PATH TARGET_USER_HOME"setpoints"
#define TARGET_LOGS_FOLDER_PATH TARGET_USER_HOME"logs"

#define TARGET_STREAMER_FILENAME "/root/stream.txt"
#define TARGET_LOGS_FILENAME "/root/log_sampler_0.csv"
#define TARGET_PARAMS_FILENAME TARGET_LOGS_FOLDER_PATH"/fopt_params.txt"

#define SPINDLE_DEF_VEL_THRESH 30

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(RpcClient* client, QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void streamingFileChanged(const QString &path);
    void gcodeFileChanged(const QString &path);
    void timer_update();

    void commit();

    void on_push_gcodepause_clicked();
    
    void on_push_gcodestart_clicked();

    void on_space_pressed();

    void on_push_streamstart_clicked ();

    void on_push_streamstop_clicked();

    void on_push_gcoderefresh_clicked();

    void on_push_setinactive_clicked();
    void on_push_sethoming_clicked();
    void on_push_setjog_clicked();
    void on_push_setstream_clicked();
    void on_push_setgcode_clicked();

    void on_push_gcodeparams_clicked();
    void on_push_downloadlogs_clicked();

    void offset_changed(double);

private:

    void print_to_log(QString msg);
    bool canChangeTab(const OpenCNServerInterface::CyclicData::Reader& data);

    ParamsWindow* feedoptParam_w;

    Ui::MainWindow *ui;
    QFileDialog *_streamingFileDialog;
    QFileDialog *_gcodeFileDialog;
    QTimer *_timer;
    RpcClient* _client;
    QString GCodePath = TARGET_GCODE_FOLDER_PATH;
    QString StreamerFilePath = TARGET_STREAMER_FOLDER_PATH;
    QString LogFilePath = TARGET_LOGS_FOLDER_PATH;
    QString ClientGCodePath;
    QString ClientStreamerPath;
    QString ClientLogsPath;
    QString ClientTmpPath;

    int file_close_delay = 0;
    bool can_change_tab = false;
    
    RpcClientQTIntf *rpcClientQTIntf;

    ocn::FeedoptConfig feedoptConfig;
    std::vector<ocn::CurvStruct> curv_structs;

    void targetEnvInit();
    void downloadLogs();
    void loadStream();

};

#endif // MAINWINDOW_H
