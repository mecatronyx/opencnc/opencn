#include "part.h"

#include <QDebug>
#include <QFile>
#include <cmath>

static const int FEEDOPT_MAX_IT = 15;
static const double xmin = -24e-3;
static const double xmax = 24e-3;
static const double ymin = -24e-3;
static const double ymax = 24e-3;

const int Part::SubDivisionCount = 10;
static const int STREAMING_SUBSAMPLE = 100;

Part::Part() {}

Part::~Part()
{

}

void Part::draw()
{

    if (!display_list_index) {
        display_list_index = glGenLists(1);
        glNewList(display_list_index, GL_COMPILE);
        glPushMatrix();
        glEnable(GL_COLOR_MATERIAL);

        glLineWidth(1.0f);

        glBegin(GL_LINES);

        int last_id = 0;
        GLfloat r = 1.0f, g = 1.0f, b = 1.0f;
        for (int i = 0; i < (int)_segmentPoints.size() - 1; i++) {
//            if (_segmentSourceId[i] != last_id) {
//                last_id = _segmentSourceId[i];
//                if (last_id % 2 == 1) {
//                    g = 0.0f;
//                    b = 1.0f;
//                } else {
//                    g = 1.0f;
//                    b = 0.0f;
//                }
//            }
            glColor3f(r,g,b);
            glVertex3f(_segmentPoints[i].x(), -_segmentPoints[i].y(), _segmentPoints[i].z());

            glColor3f(r,g,b);
            glVertex3f(_segmentPoints[i+1].x(), -_segmentPoints[i+1].y(), _segmentPoints[i+1].z());
        }

        glEnd();

        glDisable(GL_COLOR_MATERIAL);
        glPopMatrix();
        glEndList();
    } else {
        glPushMatrix();
        glTranslatef(offsetX, -offsetZ, offsetY);
        glRotated(offsetThetaZ, 0.0, 1.0, 0.0);
        glCallList(display_list_index);
        glPopMatrix();
    }

    glEnable(GL_COLOR_MATERIAL);
    glPushMatrix();
    glTranslatef(offsetX, 0, offsetY);
    if (_xmin + offsetX <= xmin ||
        _xmax + offsetX >= xmax ||
        _ymin + offsetY <= ymin ||
        _ymax + offsetY >= ymax) {
        glColor3f(1.0, 0.0, 0.0);
    } else {
        glColor3f(0.0, 0.1, 0.0);
    }

    glLineWidth(3.0f);
    glBegin(GL_LINE_LOOP);
        glVertex3f(_xmin, 0, _ymin);
        glVertex3f(_xmax, 0, _ymin);
        glVertex3f(_xmax, 0, _ymax);
        glVertex3f(_xmin, 0, _ymax);
    glEnd();
    glPopMatrix();
    glDisable(GL_COLOR_MATERIAL);
}

void Part::loadGCodeFile(const QString &filename)
{
    bool failed_opt = false;
    int it=0;
    ocn::FeedoptConfig cfg{};
    ocn::FeedoptDefaultConfig(&cfg);
    int filename_size[2] = {1, (int)filename.size()};

    ocn::ConfigSetSource(&cfg, filename.toStdString().c_str(), filename_size);
    cfg.Compressing.Skip = true;
    cfg.Cusp.Skip = true;
    cfg.Smoothing.Skip = true;
    
    ocn::initFeedoptPlan(cfg, &m_ctx);

    try
    {
        while(m_ctx.op != ocn::Fopt_Check) {
            it = it+1;
            bool optimized;
            ocn::CurvStruct opt_curv;
            ocn::FeedoptPlan(&m_ctx, &optimized, &opt_curv);
        
            #if 1 
            /* Avoid infinite loop and GUI crash */
            if ( it > FEEDOPT_MAX_IT){
                failed_opt = true;
                printf("[GUI] ERROR : Feedopt failed to optimize path on GUI, after %d iteration\n",it);
                break;
            } else {
                failed_opt = false;
            }
            #endif
        }
   } catch ( ocn::Error &e ) {
        (void) std::strncpy( m_ctx.errmsg.msg, e.what(), sizeof( m_ctx.errmsg.msg ) );
        m_ctx.errmsg.size	= strlen( m_ctx.errmsg.msg );
        m_ctx.errcode		= (ocn::FeedoptPlanError)m_ctx.op;
        printf( "[GUI] : [Errorcode : %d] %s\n",m_ctx.errcode, m_ctx.errmsg.msg );
        #warning [HGS] : Better to set errmsg in the status of the GUI
    }

    _gcodeStructs.clear();
    const size_t len = m_ctx.q_gcode.size();

    for (size_t i = 1; i <= len; i++) {
        ocn::CurvStruct tmp;
        m_ctx.q_gcode.get( (int) i, &tmp);
        _gcodeStructs.push_back(std::move(tmp));
    }

    loadCurvStruct(_gcodeStructs);
}

void Part::offsetXChange(double value)
{
    offsetX = value / 1000.0;
    emit pointsUpdated();
}
void Part::offsetYChange(double value)
{
    offsetY = -value / 1000.0;
    emit pointsUpdated();
}
void Part::offsetZChange(double value)
{
    offsetZ = value / 1000.0;
    emit pointsUpdated();
}

void Part::offsetThetaZChange(double value)
{
    offsetThetaZ = value;
    emit pointsUpdated();
}

void Part::loadCurvStruct(const std::vector<ocn::CurvStruct>& curvStruct)
{
    if (display_list_index) {
        glDeleteLists(display_list_index, 1);
        display_list_index = 0;
    }

    _segmentPoints.clear();
    _segmentPoints.reserve(curvStruct.size() * SubDivisionCount);

    _segmentSourceId.clear();
    _segmentSourceId.reserve(curvStruct.size() * SubDivisionCount);


    _xmin = std::numeric_limits<float>::max();
    _xmax = std::numeric_limits<float>::min();

    _ymin = std::numeric_limits<float>::max();
    _ymax = std::numeric_limits<float>::min();

    int source_id = 0;

    coder::array<double, 2U> temp_uVec;
    coder::array<double, 1U> r0D;
    ocn::coder::linspace( 0.0, 1.0, SubDivisionCount, temp_uVec );

    coder::array<double, 1U> uVec(temp_uVec);

    for(const auto& curv: curvStruct) {

        // printf("dataSize: %dx%d\n", dataSize[0], dataSize[1]);
        ocn::CurvStruct spline;
        m_ctx.q_spline.get(curv.sp_index, &spline);

		m_ctx.kin.set_tool_length( -curv.tool.offset.z );
        
	for (int j = 0; j < SubDivisionCount; j++) {

            evalPositionFromU(m_ctx.cfg,  &curv, &spline, uVec[j], r0D);

            auto x = static_cast<float>( r0D[0] / 1000.0);  //  x
            auto y = static_cast<float>( r0D[1] / 1000.0);  //  y
            auto z = static_cast<float>( r0D[2] / 1000.0);  //  z


            _segmentPoints.push_back(QVector3D(x, y, z));
            _xmin = std::min(x, _xmin);
            _xmax = std::max(x, _xmax);

            _ymin = std::min(y, _ymin);
            _ymax = std::max(y, _ymax);

            _zmin = std::min(z, _ymin);
            _zmax = std::max(z, _ymax);

            _segmentSourceId.push_back(source_id);
        }
        source_id++;
    }

    emit pointsUpdated();
}


bool Part::loadStreamingFile(const QString &path)
{
    if (display_list_index) {
        glDeleteLists(display_list_index, 1);
        display_list_index = 0;
    }
    QFile file(path);

    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Error reading " << path;
        return false;
    }

    QTextStream in(&file);

    // Count lines
    int lineCount = 0;
    while (!in.atEnd()) {
        QString line = in.readLine();
        lineCount++;
    }

    in.seek(0);

    _segmentPoints.clear();
    _segmentPoints.resize(lineCount/STREAMING_SUBSAMPLE);

    _xmin = std::numeric_limits<float>::max();
    _xmax = std::numeric_limits<float>::min();

    _ymin = std::numeric_limits<float>::max();
    _ymax = std::numeric_limits<float>::min();

    int i = 0;
    int k = 0;
    while (!in.atEnd() && i < 100000 && i < _segmentPoints.size()) {

        QString line = in.readLine();
        if (k % 100 != 0) {
            k++;
            continue;
        }
        QStringList list = line.split(" ", QString::SkipEmptyParts);

        if (list.size() != 3) {
            _segmentPoints.clear();
            return false;
        }

        _segmentPoints[i].setX(list[0].toFloat() / 1000.0f);
        _segmentPoints[i].setY(list[2].toFloat() / 1000.0f);
        _segmentPoints[i].setZ(-list[1].toFloat() / 1000.0f);

        _xmin = std::min(_segmentPoints[i].x(), _xmin);
        _xmax = std::max(_segmentPoints[i].x(), _xmax);

        _ymin = std::min(_segmentPoints[i].z(), _ymin);
        _ymax = std::max(_segmentPoints[i].z(), _ymax);

//        if (i > 0 && _segmentPoints[i].distanceToPoint(_segmentPoints[i-1]) > 1e-3) {
//            i++;
//        }
//        if (i == 0) i = 1;
        k++;
        i++;
    }

    emit pointsUpdated();

    return true;

}
