#include "vertex_buffer.h"

VertexBuffer::VertexBuffer()
{
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

VertexBuffer::~VertexBuffer()
{
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
}

void VertexBuffer::draw(GLenum mode, int first_index, int last_index)
{
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	if (dirty) {
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(),
					 GL_STATIC_DRAW);
		dirty = false;
	}

	glDrawArrays(mode, first_index, last_index - first_index);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void VertexBuffer::set(std::vector<Vertex> vertices)
{
	this->vertices = std::move(vertices);
	dirty = true;
}

void VertexBuffer::setColor(float r, float g, float b, int index_start, int count)
{

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	std::for_each(begin(vertices) + index_start, begin(vertices) + index_start + count,
				  [r, g, b](Vertex &v) {
					  v.r = r;
					  v.g = g;
					  v.b = b;
				  });
	glBufferSubData(GL_ARRAY_BUFFER, index_start * sizeof(Vertex), count * sizeof(Vertex),
					&vertices[index_start]);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

int VertexBuffer::size() const
{
	return vertices.size();
}
