#include <stdarg.h>
#include <stdio.h>
#include <sys/inotify.h>
#include <unistd.h>

#include <glad.h>

#include <GLFW/glfw3.h>

//#include <stb/stb_image.h>

#ifdef __cplusplus

#include <algorithm>
#include <atomic>
#include <optional>
#include <thread>
#include <vector>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <imgui.h>

#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

//#include <matlab_headers.h>

void c_assert_(const char *msg);

//#include <dependencies/blend2d/src/blend2d.h>

#endif // __cplusplus
