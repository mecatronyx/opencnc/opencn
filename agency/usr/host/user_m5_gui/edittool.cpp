#include "edittool.h"
#include "ui_edittool.h"

EditTool::EditTool(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::EditTool)
{
    ui->setupUi(this);

    /* Set role of button */
    QList<QAbstractButton *> button_list = ui->button_tool->buttons();
    ui->button_tool->addButton(button_list.at(0),QDialogButtonBox::AcceptRole);
    ui->button_tool->addButton(button_list.at(1),QDialogButtonBox::RejectRole);

    /* Connect button to function */
    connect(ui->button_tool, &QDialogButtonBox::accepted, this, &EditTool::saveTool);
    connect(ui->button_tool, &QDialogButtonBox::rejected, this, &EditTool::cancel);

}

EditTool::~EditTool()
{
    delete ui;
}

void EditTool::cancel()
{
    EditTool::close();
}

void EditTool::saveTool()
{
    QFile file(toolfp);

    /* Write to File */
    if (file.open(QIODevice::WriteOnly)){
        
        QTextStream outStream(&file);
        outStream << ui->text_tool->toPlainText();

        file.close();
        qDebug() << "Wrote to " << toolfp;
    } else {
        qDebug() << "ERROR : Impossible to write to " << toolfp;
    }

    /* Signal tool has been edited */
    emit toolTable_edited();

    EditTool::close();
}

void EditTool::loadTool(QString toolfilepath)
{
    toolfp = toolfilepath;
    QFile file(toolfp);
    QColor c;

    /* Check if file is accesible*/
    if (file.open(QIODevice::ReadOnly)){
        c.setRgb(255,255,255);
        ui->text_tool->setTextColor(c);

        /* Clean any content */
        ui->text_tool->setPlainText("");

        /* Fill text */
        while (!file.atEnd()) {
            QByteArray line = file.readLine();
            ui->text_tool->insertPlainText(QString(line));
        }

        file.close();     
    } else {
        /* Print error info if file is not found */
        c.setRgb(255,0,0);
        QString errmsg;
        ui->text_tool->setTextColor(c);
        errmsg.append("Error : Could not open tool table under :\n");
        errmsg.append(toolfp);
        ui->text_tool->setPlainText(errmsg);
    }



}