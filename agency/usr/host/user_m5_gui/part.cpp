
#include "part.h"

#include <QDebug>
#include <QFile>
#include <cmath>

bool failed_opt = false; /* Feedopt */
const int Part::SubDivisionCount = 10;
static const int STREAMING_SUBSAMPLE = 100;

Part::Part(MainWindow *parent, limits_t& limits)
{
    printf("Part constructor\n");

    xmin = limits.xMin / 1000.0f;
    xmax = limits.xMax / 1000.0f;
    ymin = limits.yMin / 1000.0f;
    ymax = limits.yMax / 1000.0f;
    zmin = limits.zMin / 1000.0f;
    zmax = limits.zMax / 1000.0f;
    /* Angle - in degree */
    bmin = limits.bMin;
    bmax = limits.bMax;
    cmin = -std::numeric_limits<float>::max();;
    cmax =  std::numeric_limits<float>::max();;

    /* Register mainwindow for log printing */
    mainWin = parent;
}

Part::~Part()
{

}

void Part::draw()
{

    /* Draw red cross if optimization failed */
    if (failed_opt){
        GLfloat r = 1.0f, g = 0.0f, b = 0.0f;
        glEnable(GL_COLOR_MATERIAL);
        glColor3f(r,g,b);
        glLineWidth(10.0f);
        glBegin(GL_LINE_LOOP);
            glVertex3f(-0.020,-0.020, 0);
            glVertex3f(+0.020,+0.020, 0);
        glEnd();
        glBegin(GL_LINE_LOOP);
            glVertex3f(-0.020,+0.020, 0);
            glVertex3f(+0.020,-0.020, 0);
        glEnd();
        glPopMatrix();
        return;
    }

    if (!display_list_index) {
        display_list_index = glGenLists(1);
        glNewList(display_list_index, GL_COMPILE);
        glPushMatrix();
        glEnable(GL_COLOR_MATERIAL);

        glLineWidth(1.0f);

        glBegin(GL_LINES);

        int last_id = 0;
        GLfloat r = 1.0f, g = 1.0f, b = 1.0f;
        for (int i = 0; i < (int)_segmentPoints.size() - 1; i++) {
            glColor3f(r,g,b);
            glVertex3f(_segmentPoints[i].x(), _segmentPoints[i].y(), _segmentPoints[i].z());

            glColor3f(r,g,b);
            glVertex3f(_segmentPoints[i+1].x(), _segmentPoints[i+1].y(), _segmentPoints[i+1].z());
        }

        glEnd();

        glDisable(GL_COLOR_MATERIAL);
        glPopMatrix();
        glEndList();
    } else {
        glPushMatrix();
        glTranslatef(offsetX, offsetY, offsetZ);
        glRotated(offsetThetaZ, 0.0, 0.0, 1.0);
        glCallList(display_list_index);
        glPopMatrix();
    }

    /* Color Red if GCode is out of zone, yellow otherwie */
    glEnable(GL_COLOR_MATERIAL);
    glPushMatrix();
    glTranslatef(offsetX, offsetY, 0);
    if (_xmin + offsetX <= xmin ||
        _xmax + offsetX >= xmax ||
        _ymin + offsetY <= ymin ||
        _ymax + offsetY >= ymax ||
        _zmin + offsetZ <= zmin ||
        _zmax + offsetZ >= zmax ||
        /*no offset*/
        _bmin           <= bmin ||
        _bmax           >= bmax ||
        _cmin           <= cmin ||
        _cmax           >= cmax ) {
        glColor3f(1.0, 0.0, 0.0);
    } else {
        glColor3f(0.0, 0.1, 0.0);
    }

    /* Draw GCode zone */
    glLineWidth(3.0f);
    //Bottom rectangle
    glBegin(GL_LINE_LOOP);
        glVertex3f(_xmin, _ymin, _zmin);
        glVertex3f(_xmax, _ymin, _zmin);
        glVertex3f(_xmax, _ymax, _zmin);
        glVertex3f(_xmin, _ymax, _zmin);
    glEnd();
    // Upper rectangle
    glBegin(GL_LINE_LOOP);
        glVertex3f(_xmin, _ymin, _zmax);
        glVertex3f(_xmax, _ymin, _zmax);
        glVertex3f(_xmax, _ymax, _zmax);
        glVertex3f(_xmin, _ymax, _zmax);
    glEnd();
    // Vertical edge
    glBegin(GL_LINES);
        glVertex3f(_xmin, _ymin, _zmax);
        glVertex3f(_xmin, _ymin, _zmin);

        glVertex3f(_xmin, _ymax, _zmax);
        glVertex3f(_xmin, _ymax, _zmin);

        glVertex3f(_xmax, _ymax, _zmax);
        glVertex3f(_xmax, _ymax, _zmin);

        glVertex3f(_xmax, _ymin, _zmax);
        glVertex3f(_xmax, _ymin, _zmin);
    glEnd();
    glPopMatrix();
    glDisable(GL_COLOR_MATERIAL);
}

void Part::loadGCodeFile(const QString &filename)
{
    int it=0;
    ocn::FeedoptConfig cfg{};
    ocn::FeedoptDefaultConfig(&cfg);
    int filename_size[2] = {1, (int)filename.size()};

    ocn::ConfigSetSource(&cfg, filename.toStdString().c_str(), filename_size);
    cfg.Compressing.Skip = true;
    cfg.Cusp.Skip = true;
    cfg.Smoothing.Skip = true;
    
    ocn::initFeedoptPlan(cfg, &m_ctx);

    try
    {
        while(m_ctx.op != ocn::Fopt_Check) {
            it = it+1;
            bool optimized;
            ocn::CurvStruct opt_curv;
            

            ocn::FeedoptPlan(&m_ctx, &optimized, &opt_curv);
            
            #if 1 // HGS : I don't think we need this any more.
            /* Avoid infinite loop and GUI crash */
            if ( it > FEEDOPT_MAX_IT){
                failed_opt = true;
                printf("[GUI] ERROR : Feedopt failed to optimize path on GUI, after %d iteration\n",it);
                break;
            } else {
                failed_opt = false;
            }
            #endif
        }

    } catch ( ocn::Error &e ) {
        (void) std::strncpy( m_ctx.errmsg.msg, e.what(), sizeof( m_ctx.errmsg.msg ) );
        m_ctx.errmsg.size	= strlen( m_ctx.errmsg.msg );
        m_ctx.errcode		= (ocn::FeedoptPlanError)m_ctx.op;
        printf( "[GUI] : [Errorcode : %d] %s\n",m_ctx.errcode, m_ctx.errmsg.msg ); // Temporary, to remove if working properly 
        QString err = QString("[GUI] : [Errorcode : %1] %2")
            .arg(m_ctx.errcode).arg(m_ctx.errmsg.msg);
        mainWin->print_to_log(err);
    }

    _gcodeStructs.clear();
    const size_t len = m_ctx.q_gcode.size();

    for (size_t i = 1; i <= len; i++) {
        ocn::CurvStruct tmp;
        m_ctx.q_gcode.get( (int) i, &tmp);
        _gcodeStructs.push_back(std::move(tmp));
    }

    loadCurvStruct(_gcodeStructs);
}

void Part::offsetXChange(double value)
{
    offsetX = value / 1000.0;
    emit pointsUpdated();
}
void Part::offsetYChange(double value)
{
    offsetY = -value / 1000.0;
    emit pointsUpdated();
}
void Part::offsetZChange(double value)
{
    offsetZ = value / 1000.0;
    emit pointsUpdated();
}

void Part::offsetThetaZChange(double value)
{
    offsetThetaZ = value;
    emit pointsUpdated();
}

void Part::loadCurvStruct(const std::vector<ocn::CurvStruct>& curvStruct)
{
    if (display_list_index) {
        glDeleteLists(display_list_index, 1);
        display_list_index = 0;
    }

    _segmentPoints.clear();
    _segmentPoints.reserve(curvStruct.size() * SubDivisionCount);

    _segmentSourceId.clear();
    _segmentSourceId.reserve(curvStruct.size() * SubDivisionCount);


    _xmin = std::numeric_limits<float>::max();
    _xmax = std::numeric_limits<float>::min();

    _ymin = std::numeric_limits<float>::max();
    _ymax = std::numeric_limits<float>::min();

    _zmin = std::numeric_limits<float>::max();
    _zmax = std::numeric_limits<float>::min();

    _bmin = std::numeric_limits<float>::max();
    _bmax = std::numeric_limits<float>::min();

    _cmin = std::numeric_limits<float>::max();
    _cmax = std::numeric_limits<float>::min();

    int source_id = 0;

    coder::array<double, 2U> temp_uVec;
    coder::array<double, 1U> r0D;
    
    ocn::coder::linspace( 0.0, 1.0, SubDivisionCount, temp_uVec );

    coder::array<double, 1U> uVec(temp_uVec);

    for(const auto& curv: curvStruct) {

        // printf("dataSize: %dx%d\n", dataSize[0], dataSize[1]);

        ocn::CurvStruct spline;
        m_ctx.q_spline.get(curv.sp_index, &spline);

        m_ctx.kin.set_tool_length( -curv.tool.offset.z );

        for (int j = 0; j < SubDivisionCount; j++) {

            evalPositionFromU(m_ctx.cfg,  &curv, &spline, uVec[j], r0D);

            if( !curv.Info.TRAFO ){
                ::coder::array<double, 2U> copy_r0D;
                copy_r0D.set_size( r0D.size(0), 1 );
                for( auto k = 0; k < r0D.size(0); k++)
                    copy_r0D[k] = r0D[k];
                    
                auto pos        = ::coder::array<double, 2U>( copy_r0D );

        		m_ctx.kin.set_tool_length( -curv.tool.offset.z );
	
                m_ctx.kin.r_relative( copy_r0D, pos );
				
                for( auto k = 0; k < m_ctx.cfg.NumberAxis; k++)
                	r0D[k] = pos[k];
            }

            auto x = static_cast<float>( r0D[0] / 1000.0);  //  x
            auto y = static_cast<float>( r0D[1] / 1000.0);  //  y
            auto z = static_cast<float>( r0D[2] / 1000.0);  //  z
            auto b = static_cast<float>( r0D[3]);           //  b
            auto c = static_cast<float>( r0D[4]);           //  c


            _segmentPoints.push_back(QVector3D(x, y, z));
            _xmin = std::min(x, _xmin);
            _xmax = std::max(x, _xmax);

            _ymin = std::min(y, _ymin);
            _ymax = std::max(y, _ymax);

            _zmin = std::min(z, _zmin);
            _zmax = std::max(z, _zmax);

            _bmin = std::min(b, _bmin);
            _bmax = std::max(b, _bmax);

            _cmin = std::min(c, _cmin);
            _cmax = std::max(c, _cmax);
            _segmentSourceId.push_back(source_id);
        }
        source_id++;
    }

    emit pointsUpdated();
}


bool Part::loadStreamingFile(const QString &path)
{
    if (display_list_index) {
        glDeleteLists(display_list_index, 1);
        display_list_index = 0;
    }
    QFile file(path);

    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Error reading " << path;
        return false;
    }

    QTextStream in(&file);

    // Count lines
    int lineCount = 0;
    while (!in.atEnd()) {
        QString line = in.readLine();
        lineCount++;
    }

    in.seek(0);

    _segmentPoints.clear();
    _segmentPoints.resize(lineCount/STREAMING_SUBSAMPLE);

    _xmin = std::numeric_limits<float>::max();
    _xmax = std::numeric_limits<float>::min();

    _ymin = std::numeric_limits<float>::max();
    _ymax = std::numeric_limits<float>::min();

    int i = 0;
    int k = 0;
    while (!in.atEnd() && i < 100000 && i < _segmentPoints.size()) {

        QString line = in.readLine();
        if (k % 100 != 0) {
            k++;
            continue;
        }
        QStringList list = line.split(" ", QString::SkipEmptyParts);

        if (list.size() != 3) {
            _segmentPoints.clear();
            return false;
        }

        _segmentPoints[i].setX(list[0].toFloat() / 1000.0f);
        _segmentPoints[i].setY(list[1].toFloat() / 1000.0f);
        _segmentPoints[i].setZ(list[2].toFloat() / 1000.0f);

        _xmin = std::min(_segmentPoints[i].x(), _xmin);
        _xmax = std::max(_segmentPoints[i].x(), _xmax);

        _ymin = std::min(_segmentPoints[i].y(), _ymin);
        _ymax = std::max(_segmentPoints[i].y(), _ymax);

//        if (i > 0 && _segmentPoints[i].distanceToPoint(_segmentPoints[i-1]) > 1e-3) {
//            i++;
//        }
//        if (i == 0) i = 1;
        k++;
        i++;
    }

    emit pointsUpdated();

    return true;

}
