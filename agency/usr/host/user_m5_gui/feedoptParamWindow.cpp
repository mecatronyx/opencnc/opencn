#include "feedoptParamWindow.h"
#include "ui_feedoptParamWindow.h"

#include <QDebug>
#include <QSettings>

FeedoptParamWindow::FeedoptParamWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FeedoptParamWindow)
{
    /* QT setup */
    ui->setupUi(this);

    ui->comboBox_FeedoptCfg->addItem("#0 Factory Default");
    for (int i = 1; i < FEEDOPT_MAX_CFG; i++)
    {
        ui->comboBox_FeedoptCfg->addItem("#" + QString::number(i) + " Default");
    }

    /* Setup settings, boxes value, list index etc.. */
    setup();

    /* Ok & Cancel Button */
    connect(ui->buttonBox_validate, &QDialogButtonBox::accepted, this, &FeedoptParamWindow::validateCfg);
    connect(ui->buttonBox_validate, &QDialogButtonBox::rejected, this, &FeedoptParamWindow::cancelCfg);

    /* Param window behavior function */
    connect(ui->comboBox_FeedoptCfg, SIGNAL(currentIndexChanged(int)), this, SLOT(tabChanged(int)));
    connect(ui->comboBox_FeedoptCfg, SIGNAL(editTextChanged(QString)), this, SLOT(handleNameChange(QString)));
    connect(ui->checkBox_editName, SIGNAL(stateChanged(int)), this, SLOT(setListEditable(int)));
    connect(ui->push_factory, &QPushButton::clicked, this, &FeedoptParamWindow::loadFactoryConfig);
}

/**
 * @def showEvent
 * @brief Catch QT function to show window and setup the UI accordingly to parameter
 */
void FeedoptParamWindow::showEvent(QShowEvent *ev)
{
    QMainWindow::showEvent(ev);
    setup();
}

FeedoptParamWindow::~FeedoptParamWindow()
{
    /* Store windows position */
    saveUIGeometry();
    delete ui;
}

/**********************************************************
 *                  PUBLIC
 * ********************************************************/

ocn::FeedoptConfig FeedoptParamWindow::getCurrentConfig()
{
    return getUIConfig();
}

/******************************************
 *              PRIVATE SLOTS
 * *****************************************/

/**
 * @def showEvent
 * @brief Setup the UI with the proper parameter values
 */
void FeedoptParamWindow::setup()
{
    /* Retrieve config */
    QSettings settings(guiConfFile, QSettings::NativeFormat);
    cfg.currIndex = settings.value("fopt/cfg/currIndex").toInt();
    
    /* Load & set stored config */
    for (int i = 0; i < FEEDOPT_MAX_CFG; i++)
    {
        cfg.paramFopt[i] = loadConfigIndex(i);
        ui->comboBox_FeedoptCfg->setItemText(i,cfg.cfgName[i]); // Set item name
    }

    ui->comboBox_FeedoptCfg->setCurrentIndex(cfg.currIndex); // Set proper index
    setUIConfig(cfg.paramFopt[cfg.currIndex]); // Set value of boxes

    /* Disable boxes for config 0->Factory settings */
    if (cfg.currIndex == 0){
        setEnableAllBoxes(false);
    } else {
        setEnableAllBoxes(true);
    }

    /* Restore position of window */
    setUIGeometry();
}

/**
 * @def validateCfg
 * @brief Handle validate option window : Store all settings when configuration is validated by user
 */
void FeedoptParamWindow::validateCfg()
{
    /* Store current config in struct */
    cfg.paramFopt[cfg.currIndex] = getUIConfig();

    /* Store all config in settings */
    saveAllConfig();

    /* Make feedopt recalculate path */
    emit feedoptRecalc();

    /* Store UI position */
    saveUIGeometry();

    /* Close UI */
    this->close();
}

/**
 * @def cancelCfg
 * @brief Handle user cancel option of windows
 */
void FeedoptParamWindow::cancelCfg()
{
    /* Store UI geometry/Pos and close UI */
    saveUIGeometry();
    this->close();
}

/**
 * @def tabChanged
 * @brief Handle the change of item, store data of previous one, display data of newIndex
 * @param newIndex : [int] The new index of the list QComboBox
 */
void FeedoptParamWindow::tabChanged(int newIndex)
{
    /* Store config of previous tab */
    cfg.paramFopt[cfg.currIndex] = getUIConfig();

    /* Set value of all UI boxes */
    setUIConfig(cfg.paramFopt[newIndex]);

    /* Disable boxes for config 0 */
    if (newIndex == 0){
        setEnableAllBoxes(false);
        ui->checkBox_editName->setCheckState(Qt::Unchecked);
    } else {
        setEnableAllBoxes(true);
    }

    /* Register previous newIndex */
    cfg.currIndex = newIndex;
}

/*********************************************************
 *                      PRIVATE FUNCTION
 * ********************************************************/

/**
 * @def getUIConfig
 * @brief Get the UI boxes parameter value and return a ocn::FeedoptConfig structure
 * @return fOptCfg : [ocn::FeedoptConfig] The feedopt configuration structure of the UI boxes
 */
ocn::FeedoptConfig FeedoptParamWindow::getUIConfig()
{
    ocn::FeedoptConfig fOptCfg{};

    /* Geometric Parameter */
    fOptCfg.NHorz = ui->spin_nhorz->value();
    fOptCfg.NDiscr = ui->spin_ndiscr->value();
    fOptCfg.NBreak = ui->spin_nbreak->value();

    fOptCfg.LSplit = ui->spin_lsplit->value();
    fOptCfg.CutOff = ui->spin_cutoff->value();    
    fOptCfg.SplitSpecialSpline = ui->checkBox_splitSpecialSpline->isChecked();

    /* Debug */
    fOptCfg.ENABLE_PRINT_MSG = ui->checkBox_floodUser->isChecked();

    /* Velocity-Accel-jerk of axis */
    fOptCfg.vmax[0] = ui->spin_vmax_x->value();
    fOptCfg.vmax[1] = ui->spin_vmax_y->value();
    fOptCfg.vmax[2] = ui->spin_vmax_z->value();
    //No A axis in this config
    fOptCfg.vmax[4] = ui->spin_vmax_b->value(); // B is 4th
    fOptCfg.vmax[5] = ui->spin_vmax_c->value(); // C is 5th

    fOptCfg.amax[0] = ui->spin_amax_x->value();
    fOptCfg.amax[1] = ui->spin_amax_y->value();
    fOptCfg.amax[2] = ui->spin_amax_z->value();
    //No A axis in this config
    fOptCfg.amax[4] = ui->spin_amax_b->value(); // B is 4th
    fOptCfg.amax[5] = ui->spin_amax_c->value(); // C is 5th

    fOptCfg.jmax[0] = ui->spin_jmax_x->value();
    fOptCfg.jmax[1] = ui->spin_jmax_y->value();
    fOptCfg.jmax[2] = ui->spin_jmax_z->value();
    //No A axis in this config
    fOptCfg.jmax[4] = ui->spin_jmax_b->value(); // B is 4th
    fOptCfg.jmax[5] = ui->spin_jmax_c->value(); // C is 5th

    return fOptCfg;
}

/**
 * @def setUIConfig
 * @brief Set the UI boxes to the values of feedopt parameter cfg
 * @param cfg: [ocn::FeedoptConfig] The configuration to set UI boxes to.
 */
void FeedoptParamWindow::setUIConfig(ocn::FeedoptConfig cfg)
{
    ui->spin_nhorz->setValue(cfg.NHorz);
    ui->spin_ndiscr->setValue(cfg.NDiscr);
    ui->spin_nbreak->setValue(cfg.NBreak);
    ui->spin_lsplit->setValue(cfg.LSplit);
    ui->spin_cutoff->setValue(cfg.CutOff);
    ui->checkBox_splitSpecialSpline->setChecked(cfg.SplitSpecialSpline);

    /* Debug */
    ui->checkBox_floodUser->setChecked(cfg.ENABLE_PRINT_MSG);

            /* Velocity */
    ui->spin_vmax_x->setValue(cfg.vmax[0]);
    ui->spin_vmax_y->setValue(cfg.vmax[1]);
    ui->spin_vmax_z->setValue(cfg.vmax[2]);
    //No A axis in this config 
    ui->spin_vmax_b->setValue(cfg.vmax[4]);
    ui->spin_vmax_c->setValue(cfg.vmax[5]);

            /* Acceleration */
    ui->spin_amax_x->setValue(cfg.amax[0]);
    ui->spin_amax_y->setValue(cfg.amax[1]);
    ui->spin_amax_z->setValue(cfg.amax[2]);
    //No A axis in this config 
    ui->spin_amax_b->setValue(cfg.amax[4]);
    ui->spin_amax_c->setValue(cfg.amax[5]);

            /* Jerk */
    ui->spin_jmax_x->setValue(cfg.jmax[0]);
    ui->spin_jmax_y->setValue(cfg.jmax[1]);
    ui->spin_jmax_z->setValue(cfg.jmax[2]);
    //No A axis in this config 
    ui->spin_jmax_b->setValue(cfg.jmax[4]);
    ui->spin_jmax_c->setValue(cfg.jmax[5]);
}


/**
 * @brief Store in settings all the feedopt configuration as well as UI parameter
 */
void FeedoptParamWindow::saveAllConfig()
{
    qDebug() << "FeedoptParamWindow::saveAllConfig()";

    ocn::FeedoptConfig fOptCfg;
    QString prefix;
    QSettings settings(guiConfFile, QSettings::NativeFormat);

    /* Iterate through all cfg index */
    for (int i = 0; i < FEEDOPT_MAX_CFG; i++)    {
        fOptCfg = cfg.paramFopt[i];
        prefix = FEEDOPT_CFG_SETTINGS + QString::number(i) + "/";

        settings.setValue(prefix + "name", cfg.cfgName[i]);
        settings.setValue(prefix + "NHorz", fOptCfg.NHorz);
        settings.setValue(prefix + "NDiscr", fOptCfg.NDiscr);
        settings.setValue(prefix + "NBreak", fOptCfg.NBreak);

        settings.setValue(prefix + "LSplit", fOptCfg.LSplit);
        settings.setValue(prefix + "CutOff", fOptCfg.CutOff);

        settings.setValue(prefix + "SplitSpecialSpline", fOptCfg.SplitSpecialSpline);
        settings.setValue(prefix + "FloodUser", fOptCfg.ENABLE_PRINT_MSG);

        settings.setValue(prefix + "vmax0", fOptCfg.vmax[0]);
        settings.setValue(prefix + "vmax1", fOptCfg.vmax[1]);
        settings.setValue(prefix + "vmax2", fOptCfg.vmax[2]);
        settings.setValue(prefix + "vmax3", fOptCfg.vmax[3]);
        settings.setValue(prefix + "vmax4", fOptCfg.vmax[4]);
        settings.setValue(prefix + "vmax5", fOptCfg.vmax[5]);

        settings.setValue(prefix + "amax0", fOptCfg.amax[0]);
        settings.setValue(prefix + "amax1", fOptCfg.amax[1]);
        settings.setValue(prefix + "amax2", fOptCfg.amax[2]);
        settings.setValue(prefix + "amax3", fOptCfg.amax[3]);
        settings.setValue(prefix + "amax4", fOptCfg.amax[4]);
        settings.setValue(prefix + "amax5", fOptCfg.amax[5]);

        settings.setValue(prefix + "jmax0", fOptCfg.jmax[0]);
        settings.setValue(prefix + "jmax1", fOptCfg.jmax[1]);
        settings.setValue(prefix + "jmax2", fOptCfg.jmax[2]);
        settings.setValue(prefix + "jmax3", fOptCfg.jmax[3]);
        settings.setValue(prefix + "jmax4", fOptCfg.jmax[4]);
        settings.setValue(prefix + "jmax5", fOptCfg.jmax[5]);
    }

    /* Store current Index */
    settings.setValue("fopt/cfg/currIndex",cfg.currIndex);

    /* Sync with file */
    settings.sync();
}


/**
 * @brief Load from the settings the value stored for a given configuration index. 
 * If the settings can not be found, set it to the factory default value.
 * @param index: [int] The index of the configuration to load.
 * @return A ocn::FeedoptConfig structure containing the parameter
 */
ocn::FeedoptConfig FeedoptParamWindow::loadConfigIndex(int index)
{
    ocn::FeedoptConfig fOptCfg;
    QString prefix = FEEDOPT_CFG_SETTINGS + QString::number(index) + "/";
    QSettings settings(guiConfFile, QSettings::NativeFormat);

    if(index==0){
        cfg.cfgName[index] = settings.value(prefix + "name", "#" + QString::number(index) + " Factory Default").toString();
    } else {
        cfg.cfgName[index] = settings.value(prefix + "name", "#" + QString::number(index) + " Default").toString();
    }
    
    fOptCfg.NHorz = settings.value(prefix + "NHorz", 5).toInt();
    fOptCfg.NDiscr = settings.value(prefix + "NDiscr", 20).toInt();
    fOptCfg.NBreak = settings.value(prefix + "NBreak", 10).toInt();

    fOptCfg.LSplit = settings.value(prefix + "LSplit", 3.0).toDouble();
    fOptCfg.CutOff = settings.value(prefix + "CutOff", 0.1).toDouble();
    fOptCfg.SplitSpecialSpline = settings.value(prefix + "SplitSpecialSpline", 0).toBool();
    
    fOptCfg.ENABLE_PRINT_MSG = settings.value(prefix + "FloodUser", 0).toBool();

    fOptCfg.vmax[0] = settings.value(prefix + "vmax0", 500.0).toDouble();
    fOptCfg.vmax[1] = settings.value(prefix + "vmax1", 500.0).toDouble();
    fOptCfg.vmax[2] = settings.value(prefix + "vmax2", 500.0).toDouble();
    fOptCfg.vmax[3] = settings.value(prefix + "vmax3", 0.0).toDouble();// A
    fOptCfg.vmax[4] = settings.value(prefix + "vmax4", 40.0).toDouble();// B
    fOptCfg.vmax[5] = settings.value(prefix + "vmax5", 50.0).toDouble();// C

    fOptCfg.amax[0] = settings.value(prefix + "amax0", 20000.0).toDouble();
    fOptCfg.amax[1] = settings.value(prefix + "amax1", 20000.0).toDouble();
    fOptCfg.amax[2] = settings.value(prefix + "amax2", 20000.0).toDouble();
    fOptCfg.amax[3] = settings.value(prefix + "amax3", 0.0).toDouble();// A
    fOptCfg.amax[4] = settings.value(prefix + "amax4", 200.0).toDouble();// B
    fOptCfg.amax[5] = settings.value(prefix + "amax5", 1000.0).toDouble();// C

    fOptCfg.jmax[0] = settings.value(prefix + "jmax0", 1500000.0).toDouble();
    fOptCfg.jmax[1] = settings.value(prefix + "jmax1", 1500000.0).toDouble();
    fOptCfg.jmax[2] = settings.value(prefix + "jmax2", 1500000.0).toDouble();
    fOptCfg.jmax[3] = settings.value(prefix + "jmax3", 0.0).toDouble();// A
    fOptCfg.jmax[4] = settings.value(prefix + "jmax4", 10000.0).toDouble();// B
    fOptCfg.jmax[5] = settings.value(prefix + "jmax5", 50000.0).toDouble();// C

    return fOptCfg;
}

/**
 * @brief Setup UI boxes to value of the factory parameter of Feedopt
 */
void FeedoptParamWindow::loadFactoryConfig()
{
    ocn::FeedoptConfig cfg;
    cfg.NHorz = 5;
    cfg.NDiscr = 20;
    cfg.NBreak = 10;

    cfg.LSplit = 3.0;
    cfg.CutOff = 0.1;
    cfg.SplitSpecialSpline = false;
    
    cfg.ENABLE_PRINT_MSG = false;

    cfg.vmax[0] = 500;
    cfg.vmax[1] = 500;
    cfg.vmax[2] = 500;
    cfg.vmax[3] = 0;
    cfg.vmax[4] = 40;
    cfg.vmax[5] = 50;

    cfg.amax[0] = 20000.0;
    cfg.amax[1] = 20000.0;
    cfg.amax[2] = 20000.0;
    cfg.amax[3] = 0.0;
    cfg.amax[4] = 200.0;
    cfg.amax[5] = 1000.0;

    cfg.jmax[0] = 1500000.0;
    cfg.jmax[1] = 1500000.0;
    cfg.jmax[2] = 1500000.0;
    cfg.jmax[3] = 0.0;
    cfg.jmax[4] = 10000.0;
    cfg.jmax[5] = 50000.0;

    setUIConfig(cfg);
}

/**
 * @brief Set the state of parameter boxes 
 * @param val: [bool] True->Enable the boxes / False->Disable the boxes
 */
void FeedoptParamWindow::setEnableAllBoxes(bool val)
{
    ui->spin_nhorz->setEnabled(val);
    ui->spin_ndiscr->setEnabled(val);
    ui->spin_nbreak->setEnabled(val);
    ui->spin_lsplit->setEnabled(val);
    ui->spin_cutoff->setEnabled(val);
    ui->checkBox_splitSpecialSpline->setEnabled(val);
    ui->checkBox_floodUser->setEnabled(val);

    ui->spin_vmax_x->setEnabled(val);
    ui->spin_vmax_y->setEnabled(val);
    ui->spin_vmax_z->setEnabled(val);
    ui->spin_vmax_b->setEnabled(val);
    ui->spin_vmax_c->setEnabled(val);
    ui->spin_amax_x->setEnabled(val);
    ui->spin_amax_y->setEnabled(val);
    ui->spin_amax_z->setEnabled(val);
    ui->spin_amax_b->setEnabled(val);
    ui->spin_amax_c->setEnabled(val);
    ui->spin_jmax_x->setEnabled(val);
    ui->spin_jmax_y->setEnabled(val);
    ui->spin_jmax_z->setEnabled(val);
    ui->spin_jmax_b->setEnabled(val);
    ui->spin_jmax_c->setEnabled(val);
}

/**
 * @brief Get in the settings the UI geometry and state, set the UI geometry accordingly
 */
void FeedoptParamWindow::setUIGeometry()
{
    /* Set UI geometry and state from settings */
    QSettings settings(guiConfFile, QSettings::NativeFormat);
    restoreGeometry(settings.value("UI/ParamWindow/geometry").toByteArray()); 
    restoreState(settings.value("UI/ParamWindow/windowState").toByteArray());
    return;
}

/**
 * @brief Store in the settings the UI geometry and state
 */
void FeedoptParamWindow::saveUIGeometry()
{
    /* Store UI position */
    QSettings settings(guiConfFile, QSettings::NativeFormat);
    settings.setValue("UI/ParamWindow/geometry", saveGeometry());
    settings.setValue("UI/ParamWindow/windowState", saveState());
    settings.sync();
    return;
}

/**
 * @brief Set if the item name is editable, except for the item #0 wich is Factory settings.
 * @param state: [int] True->Make item name editable / False->Make item name NOT editable
 */
void FeedoptParamWindow::setListEditable(int state)
{
    /* Item 0 not text editable */
    if ( ui->comboBox_FeedoptCfg->currentIndex() == 0) {
        ui->comboBox_FeedoptCfg->setEditable(false);    
    } else {
        ui->comboBox_FeedoptCfg->setEditable(state);
    }    
}

/**
 * @brief Handle the name change of an item
 * @param name: The last input name of the edited item
 */
void FeedoptParamWindow::handleNameChange(QString name)
{
    /* Store new name */
    cfg.cfgName[cfg.currIndex] = name;
}
