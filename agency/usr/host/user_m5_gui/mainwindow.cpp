#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QShortcut>
#include <QProgressDialog>
#include <QFontDatabase>

#include <limits>
#include <cstdlib>
#include <cstring>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>

#include <thread>

#include "opencn-interface/opencn_interface.capnp.h"

#include "rpcclientqtintf.h"

#define MAX_MESSAGE_LEN 10000

#define MACHINE_STATE_IDLE     0
#define MACHINE_STATE_RUNNING  1

using namespace std::chrono_literals;

enum TabIndex { Inactive, Homing, Jog, Stream, GCode};

MainWindow::MainWindow(RpcClient* client, QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow), _client(client)
{
	ui->setupUi(this);

    /* Add subwindow */
    feedoptParam_w = new FeedoptParamWindow(this);
    editTool_window = new EditTool(this);

	_timer = new QTimer(this);

	rpcClientQTIntf = new RpcClientQTIntf(_client);
	ui->openGLWidget_toolpath->setRpcClient(_client);
    ui->openGLWidget_toolpath->setParentWindow(this);


	targetEnvInit();

    {
        QSettings settings(guiConfFile, QSettings::NativeFormat);
        settings.beginGroup("Paths");
        ClientStreamerPath = settings.value("StreamFolder", QDir::homePath()+"/setpoints").toString();
        ClientGCodePath = settings.value("GcodeFolder", QDir::homePath()).toString();
        ClientLogsPath = settings.value("LogsFolder", QDir::homePath()+"/logs").toString();
        ClientTmpPath = settings.value("TmpFolder", QDir::homePath()+"/tmp").toString();

        bool autoDownloadLogs = settings.value("AutoDownloadLogs", false).toBool();

        ui->check_autodownloadlogs->setChecked(autoDownloadLogs);
    }

    _streamingFileDialog = new QFileDialog(this, "Select the streamer file", ClientStreamerPath, "*.txt");
    _gcodeFileDialog = new QFileDialog(this, "Select the ngc file", ClientGCodePath, "*.ngc");

	connect(ui->actionStreaming, &QAction::triggered, _streamingFileDialog, &QDialog::open);
	connect(_streamingFileDialog, &QFileDialog::fileSelected, this,
			&MainWindow::streamingFileChanged);

	connect(ui->actionG_code, &QAction::triggered, _gcodeFileDialog, &QDialog::open);
    connect(_gcodeFileDialog, &QFileDialog::fileSelected, this, &MainWindow::gcodeFileChanged);

    connect(ui->check_autodownloadlogs, &QCheckBox::stateChanged, [this](){
        QSettings settings(guiConfFile, QSettings::NativeFormat);
        settings.setValue("AutoDownloadLogs", ui->check_autodownloadlogs->isChecked());
    });

	// Homing
	connect(ui->pushButton_startHoming, SIGNAL(pressed()), rpcClientQTIntf, SLOT(startHomingSlot()));
	connect(ui->pushButton_stopHoming, SIGNAL(pressed()), rpcClientQTIntf, SLOT(stopHomingSlot()));

	// Spindle
    connect(ui->doubleSpinBox_spindleSpeed_rpm, SIGNAL(valueChanged(double)), rpcClientQTIntf, SLOT(speedSpindleSlot(double)));
    connect(ui->check_spindleActive, SIGNAL(toggled(bool)), rpcClientQTIntf, SLOT(activeSpindleSlot(bool)));

    connect(ui->check_freeTool,    SIGNAL(toggled(bool)), rpcClientQTIntf, SLOT(freeToolSlot(bool)));
    connect(ui->check_freePalette, SIGNAL(toggled(bool)), rpcClientQTIntf, SLOT(freePaletteSlot(bool)));

	// Init spindle speed PIN at startup
    _client->setSpeedSpindle(ui->doubleSpinBox_spindleSpeed_rpm->value());

	// Init spindle state and air PINs values with checkboxes at startup
    _client->setActiveSpindle(ui->check_spindleActive->isChecked());

	// Jog axis select
	connect(ui->radioButton_XJog, SIGNAL(toggled(bool)), rpcClientQTIntf, SLOT(jogXSlot(bool)));
	connect(ui->radioButton_YJog, SIGNAL(toggled(bool)), rpcClientQTIntf, SLOT(jogYSlot(bool)));
	connect(ui->radioButton_ZJog, SIGNAL(toggled(bool)), rpcClientQTIntf, SLOT(jogZSlot(bool)));
	connect(ui->radioButton_BJog, SIGNAL(toggled(bool)), rpcClientQTIntf, SLOT(jogBSlot(bool)));
	connect(ui->radioButton_CJog, SIGNAL(toggled(bool)), rpcClientQTIntf, SLOT(jogCSlot(bool)));

	// Jog relative
	connect(ui->doubleSpinBox_incrJog_mm, SIGNAL(valueChanged(double)), rpcClientQTIntf, SLOT(relJogSlot(double)));
	connect(ui->pushButton_plusJog, SIGNAL(pressed()), rpcClientQTIntf, SLOT(plusJogSlot()));
	connect(ui->pushButton_minusJog, SIGNAL(pressed()), rpcClientQTIntf, SLOT(minusJogSlot()));

	// JogAbs
	connect(ui->doubleSpinBox_absJog_mm, SIGNAL(valueChanged(double)), rpcClientQTIntf, SLOT(absJogSlot(double)));
	connect(ui->pushButton_go_absJog, SIGNAL(pressed()), rpcClientQTIntf, SLOT(goJogSlot()));

	// Jog speed
	connect(ui->doubleSpinBox_speedJog_mm_min, SIGNAL(valueChanged(double)), rpcClientQTIntf, SLOT(speedJogSlot(double)));
	connect(ui->pushButton_stopJog, SIGNAL(pressed()), rpcClientQTIntf, SLOT(stopJogSlot()));

	// Init Jog speed PIN at startup
	_client->setSpeedJog(ui->doubleSpinBox_speedJog_mm_min->value());

    offset_changed(0);

    connect(ui->doubleSpinBox_xOffset, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_yOffset, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_zOffset, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_cOffset, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));

    connect(ui->doubleSpinBox_xCorrection, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_yCorrection, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_zCorrection, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_cCorrection, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));

    connect(ui->doubleSpinBox_xHome, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_yHome, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_zHome, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));
    connect(ui->doubleSpinBox_cHome, SIGNAL(valueChanged(double)), this, SLOT(offset_changed(double)));

	// Fault Reset
	connect(ui->pushButton_fault, SIGNAL(pressed()), rpcClientQTIntf, SLOT(faultResetSlot()));

    ui->doubleSpinBox_currPosX->setValue(0);
    ui->doubleSpinBox_currPosY->setValue(0);
    ui->doubleSpinBox_currPosZ->setValue(0);
    ui->doubleSpinBox_currPosB->setValue(0);
    ui->doubleSpinBox_currPosC->setValue(0);

    double limitMax = std::numeric_limits<double>::max();

    ui->doubleSpinBox_currPosX->setRange(-limitMax, limitMax);
    ui->doubleSpinBox_currPosY->setRange(-limitMax, limitMax);
    ui->doubleSpinBox_currPosZ->setRange(-limitMax, limitMax);
    ui->doubleSpinBox_currPosB->setRange(-limitMax, limitMax);
    ui->doubleSpinBox_currPosC->setRange(-limitMax, limitMax);

	connect(_timer, &QTimer::timeout, this, &MainWindow::timer_update);

	connect(ui->push_clearpath, SIGNAL(pressed()), ui->openGLWidget_toolpath->getPath(), SLOT(reset()));
    connect(ui->push_gcodeopen, &QPushButton::pressed, _gcodeFileDialog, &QDialog::open);
	connect(ui->push_streamopen, &QPushButton::pressed, _streamingFileDialog, &QDialog::open);

    /* Feedopt Connection */
    connect(ui->push_gcodereset, SIGNAL(pressed()), rpcClientQTIntf, SLOT(feedoptResetSlot()));
    connect(feedoptParam_w, SIGNAL(feedoptRecalc()), this, SLOT(feedoptRecalc())); // Feedopt recalc on parameter edit
    connect(editTool_window, SIGNAL(toolTable_edited()),this, SLOT(feedoptRecalc())); // Feedopt recalc on tool table edit
    // Connect Bar at top of GUI
    connect(ui->actionFeedopt, &QAction::triggered, this, &MainWindow::show_paramFeedopt);

	connect(this, SIGNAL(destroyed()), rpcClientQTIntf, SLOT(streamGcodeStopSlot()));

    connect(ui->spin_spindle_threshold, &QSpinBox::editingFinished, [this](){
        _client->setSpindleThreshold(ui->spin_spindle_threshold->value());
        QSettings settings(guiConfFile, QSettings::NativeFormat);
        settings.setValue("SpindleThreshold", ui->spin_spindle_threshold->value());
    });
    // Get tool table path
    toolPath=getenv(RS274_INI);
    toolPath = toolPath.left(toolPath.lastIndexOf("/"));
    toolPath.append("/tool.tbl");

	// Feedopt
	ui->progress_resampling->setMaximum(0);
	ui->progress_optimising->setValue(0);
    ui->progress_samples->setMaximum(0);
	ui->progress_samples->setValue(0);

	auto* shortcut = new QShortcut(QKeySequence(Qt::Key_Space), this);
	connect(shortcut, SIGNAL(activated()), this, SLOT(on_space_pressed()));

    ui->slider_manual_override->setValue(100);

    {
        QSettings settings(guiConfFile, QSettings::NativeFormat);
        int percent = settings.value("SpindleThreshold", SPINDLE_DEF_VEL_THRESH).toInt();

        ui->spin_spindle_threshold->setValue(percent);
        _client->setSpindleThreshold(percent);
    }

    // Disable 'pause' button on G-Code mode - feature not implemented yet
    ui->push_gcodepause->setEnabled(false);
    ui->push_gcodepause->setToolTip("Not implemented");
    
    /* Axis load bar / factor 10 for more precise readout*/
    #warning "Setting of Axis load not dynamic"
    ui->horizontalSlider_xLoad->setRange(0,axisLoad.xLoadMax*10);
    ui->horizontalSlider_yLoad->setRange(0,axisLoad.yLoadMax*10);
    ui->horizontalSlider_zLoad->setRange(0,axisLoad.zLoadMax*10);
    ui->horizontalSlider_bLoad->setRange(0,axisLoad.bLoadMax*10);
    ui->horizontalSlider_cLoad->setRange(0,axisLoad.cLoadMax*10);
    ui->horizontalSlider_spindleLoad->setRange(0,axisLoad.sLoadMax*10);

    /* Spindle temperature display / factor 10 for more precise readout*/
    ui->horizontalSlider_spindle_temp->setRange(0.0,spindleTemp.maxTemp * 10);

	_timer->setInterval(100);
	_timer->start();

    /* Hack : QDoubleSpinbox is at an infinite value at startup, we force a value change to avoid crash */
    ui->doubleSpinBox_absJog_mm->setValue(1.0);
    ui->doubleSpinBox_absJog_mm->setValue(0.0);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::targetEnvInit()
{
    if (!_client->pathExist(GCodePath.toStdString())) {
        _client->createFolder(GCodePath.toStdString());
	}

	if (!_client->pathExist(StreamerFilePath.toStdString())) {
		_client->createFolder(StreamerFilePath.toStdString());
 	}

 	if (!_client->pathExist(LogFilePath.toStdString())) {
		_client->createFolder(LogFilePath.toStdString());
	}
}

void MainWindow::streamingFileChanged(const QString &path)
{
    qDebug() << "streamingFileChanged";
	ui->line_streamerfile->setText(path);
	ui->openGLWidget_toolpath->getPart()->loadStreamingFile(path);

    loadStream();

	QSettings settings(guiConfFile, QSettings::NativeFormat);
    settings.beginGroup("Paths");
	ClientStreamerPath = path.left(path.lastIndexOf("/"));
	settings.setValue("StreamFolder", ClientStreamerPath);
}

void MainWindow::gcodeFileChanged(const QString &path)
{
    QFile file{path};
    char line[240];
    char line_with_number[256];
    ui->text_gcode_listing->setStyleSheet("QListView::item { height: 20px; }");
    ui->text_gcode_listing->clear();
    ui->text_gcode_listing->setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
    int line_number = 1;
    if (file.open(QFile::ReadOnly | QFile::Text)) {
        int count = 0;
        while((count = file.readLine(line, sizeof(line))) >= 0) {
            line[count-1] = 0;
            snprintf(line_with_number, sizeof(line_with_number), "%d: %s", line_number, line);
            ui->text_gcode_listing->addItem(line_with_number);
            line_number++;
        }
    }

    ui->line_gcodesource->setText(path);
    ui->openGLWidget_toolpath->getPart()->loadGCodeFile(path);
    usleep(10000);

    rpcClientQTIntf->feedoptResetSlot();
#if 0
//    while(_client->getFeedoptQueueSize() > 0) {
//        usleep(1000);
//    }
#endif

    QSettings settings(guiConfFile, QSettings::NativeFormat);
    settings.beginGroup("Paths");
    ClientGCodePath = path.left(path.lastIndexOf("/"));
    settings.setValue("GcodeFolder", ClientGCodePath);

    std::this_thread::sleep_for(0.1s);
    commit();
}

void MainWindow::timer_update()
{
    _timer->setInterval(1000 / 30);

    const auto& maybe_data = _client->getCyclicData();
    if (!maybe_data) return;

    const auto& data = maybe_data.value();

    bool gcodeCompleted = false;
    bool gcodeProcessCompleted = false;
    static bool gcodeWasRunning = false;
    auto gcodeRunning     = data.getGcodeRunning();
    unsigned machineState = data.getMachineState();

    if (gcodeWasRunning && !gcodeRunning)
        gcodeCompleted = true;

    if (gcodeCompleted && (machineState == MACHINE_STATE_IDLE))
        gcodeProcessCompleted = true;

    if (!gcodeWasRunning)
        gcodeWasRunning  = gcodeRunning;

   if (gcodeProcessCompleted) {
        if (ui->check_autodownloadlogs->isChecked()) {
            downloadLogs();
        }

        gcodeWasRunning = false;

        rpcClientQTIntf->feedoptResetSlot();
        std::this_thread::sleep_for(0.1s);
        commit();
    }

#if 0 /* no support for the 'steamer' mode */
    if (data.getStreamFinished()) {
        if (ui->check_autodownloadlogs->isChecked()) {
            downloadLogs();
        }

        streamingFileChanged(ui->line_streamerfile->text());
    }
#endif

    ui->text_gcode_listing->setCurrentRow(data.getCurrentGCodeLine() - 1);

    _client->setFeedrateScale(ui->slider_manual_override->value()/100.0);
    ui->progress_auto_override->setValue(data.getFeedoptStepDt()*100);

    can_change_tab = canChangeTab(data);
	ui->push_setinactive->setEnabled(can_change_tab);
    ui->push_sethoming->setEnabled(can_change_tab);
    ui->push_setjog->setEnabled(can_change_tab);
    ui->push_setstream->setEnabled(can_change_tab);
    ui->push_setgcode->setEnabled(can_change_tab);

    ui->doubleSpinBox_currPosX->setValue(data.getCurrentPosition().getX());
    ui->doubleSpinBox_currPosY->setValue(data.getCurrentPosition().getY());
    ui->doubleSpinBox_currPosZ->setValue(data.getCurrentPosition().getZ());
    ui->doubleSpinBox_currPosB->setValue(data.getCurrentPosition().getB());
    ui->doubleSpinBox_currPosC->setValue(data.getCurrentPosition().getC());

    ui->doubleSpinBox_currVelSpindle->setValue(data.getSpindleVelocity());

    const auto progress = data.getFeedoptProgress();

    ui->progress_optimising->setMaximum(progress.getOptimisingCount());
    ui->progress_optimising->setValue(progress.getOptimisingProgress());

    ui->progress_resampling->setMaximum(progress.getResamplingCount());
    ui->progress_resampling->setValue(progress.getResamplingProgress());

    ui->progress_samples->setMaximum(data.getFeedoptQueueMax());
    ui->progress_samples->setValue(data.getFeedoptQueueSize());

    const auto mode_csp = data.getAxisMode().getCsp();
    const auto mode_csv = data.getAxisMode().getCsv();
    const auto mode_homing = data.getAxisMode().getHoming();
    const auto mode_inactive = data.getAxisMode().getInactive();
    const auto mode_fault = data.getAxisMode().getFault();

    if (mode_csp.getX()) {
		ui->label_axisXMode->setText("<font color=#00AA00>CSP</font>");
    } else if (mode_csv.getX()) {
		ui->label_axisXMode->setText("<font color=#00AA00>CSV</font>");
    } else if (mode_homing.getX()) {
		ui->label_axisXMode->setText("<font color=#00AA00>HOMING</font>");
    } else if (mode_inactive.getX()) {
		ui->label_axisXMode->setText("<font color=#000000>INACTIVE</font>");
    } else if (mode_fault.getX()) {
		ui->label_axisXMode->setText("<font color=#FA0000>FAULT</font>");
	} else {
		ui->label_axisXMode->setText("<font color=#000000>UNKNOWN</font>");
	}

	// Display Y axis current Mode
    if (mode_csp.getY()) {
        ui->label_axisYMode->setText("<font color=#00AA00>CSP</font>");
    } else if (mode_csv.getY()) {
        ui->label_axisYMode->setText("<font color=#00AA00>CSV</font>");
    } else if (mode_homing.getY()) {
        ui->label_axisYMode->setText("<font color=#00AA00>HOMING</font>");
    } else if (mode_inactive.getY()) {
        ui->label_axisYMode->setText("<font color=#000000>INACTIVE</font>");
    } else if (mode_fault.getY()) {
        ui->label_axisYMode->setText("<font color=#FA0000>FAULT</font>");
    } else {
        ui->label_axisYMode->setText("<font color=#000000>UNKNOWN</font>");
    }

	// Display Z axis current Mode
    if (mode_csp.getZ()) {
        ui->label_axisZMode->setText("<font color=#00AA00>CSP</font>");
    } else if (mode_csv.getZ()) {
        ui->label_axisZMode->setText("<font color=#00AA00>CSV</font>");
    } else if (mode_homing.getZ()) {
        ui->label_axisZMode->setText("<font color=#00AA00>HOMING</font>");
    } else if (mode_inactive.getZ()) {
        ui->label_axisZMode->setText("<font color=#000000>INACTIVE</font>");
    } else if (mode_fault.getZ()) {
        ui->label_axisZMode->setText("<font color=#FA0000>FAULT</font>");
    } else {
        ui->label_axisZMode->setText("<font color=#000000>UNKNOWN</font>");
    }

    // Display B axis current Mode
    if (mode_csp.getB()) {
        ui->label_axisBMode->setText("<font color=#00AA00>CSP</font>");
    } else if (mode_csv.getB()) {
        ui->label_axisBMode->setText("<font color=#00AA00>CSV</font>");
    } else if (mode_homing.getB()) {
        ui->label_axisBMode->setText("<font color=#00AA00>HOMING</font>");
    } else if (mode_inactive.getB()) {
        ui->label_axisBMode->setText("<font color=#000000>INACTIVE</font>");
    } else if (mode_fault.getB()) {
        ui->label_axisBMode->setText("<font color=#FA0000>FAULT</font>");
    } else {
        ui->label_axisBMode->setText("<font color=#000000>UNKNOWN</font>");
    }

    // Display C axis current Mode
    if (mode_csp.getC()) {
        ui->label_axisCMode->setText("<font color=#00AA00>CSP</font>");
    } else if (mode_csv.getC()) {
        ui->label_axisCMode->setText("<font color=#00AA00>CSV</font>");
    } else if (mode_homing.getC()) {
        ui->label_axisCMode->setText("<font color=#00AA00>HOMING</font>");
    } else if (mode_inactive.getC()) {
        ui->label_axisCMode->setText("<font color=#000000>INACTIVE</font>");
    } else if (mode_fault.getC()) {
        ui->label_axisCMode->setText("<font color=#FA0000>FAULT</font>");
    } else {
        ui->label_axisCMode->setText("<font color=#000000>UNKNOWN</font>");
    }



	// Display Spindle current Mode
    if (mode_csp.getSpindle()) {
        ui->label_SpindleMode->setText("<font color=#00AA00>CSP</font>");
    } else if (mode_csv.getSpindle()) {
        ui->label_SpindleMode->setText("<font color=#00AA00>CSV</font>");
    } else if (mode_homing.getSpindle()) {
        ui->label_SpindleMode->setText("<font color=#00AA00>HOMING</font>");
    } else if (mode_inactive.getSpindle()) {
        ui->label_SpindleMode->setText("<font color=#000000>INACTIVE</font>");
    } else if (mode_fault.getSpindle()) {
        ui->label_SpindleMode->setText("<font color=#FA0000>FAULT</font>");
    } else {
        ui->label_SpindleMode->setText("<font color=#000000>UNKNOWN</font>");
    }

    if (data.getHomed()) {
		ui->label_ishomed->setText("<font color=#00AA00>HOMED</font>");
	} else {
		ui->label_ishomed->setText("<font color=#FA0000>NOT HOMED</font>");
	}

	while (true) {
		std::string message = _client->readLog();
		if (message.length() == 0)
			break;

		QString str_msg = QString::fromStdString(message);
		ui->listWidget_logs->addItem(str_msg.trimmed());
		ui->listWidget_logs->scrollToBottom();
	}

    ui->doubleSpinBox_spindleSpeed_rpm->setDisabled(false);
    ui->frame_Gcode->hide();

    if (data.getMachineMode().getInactive()) {
        ui->tabWidget->setCurrentIndex(TabIndex::Inactive);
    } else if (data.getMachineMode().getHoming()) {
		ui->tabWidget->setCurrentIndex(TabIndex::Homing);
    } else if (data.getMachineMode().getStream()) {
		ui->tabWidget->setCurrentIndex(TabIndex::Stream);
        ui->FIFO_streamer_bar->setValue(data.getStreamerFIFO());
    } else if (data.getMachineMode().getJog()) {
		ui->tabWidget->setCurrentIndex(TabIndex::Jog);
    } else if (data.getMachineMode().getGcode()) {
		ui->tabWidget->setCurrentIndex(TabIndex::GCode);
        ui->doubleSpinBox_spindleSpeed_rpm->setDisabled(true);
        ui->frame_Gcode->show();
		// ui->push_gcodecommit->setEnabled(_client->getGcodeFinished());
	}

    ui->tabWidget->setTabEnabled(TabIndex::Stream, data.getHomed());
    ui->tabWidget->setTabEnabled(TabIndex::GCode, data.getHomed());

    ui->pushButton_go_absJog->setEnabled(data.getHomed());

    /* Axis load */
    ui->label_xLoad->setText(QString::number(qFabs(data.getXLoad()), 'f', 2) + "/" + QString::number(axisLoad.xLoadMax) + "A");
    ui->label_yLoad->setText(QString::number(qFabs(data.getYLoad()), 'f', 2) + "/" + QString::number(axisLoad.yLoadMax) + "A");
    ui->label_zLoad->setText(QString::number(qFabs(data.getZLoad()), 'f', 2) + "/" + QString::number(axisLoad.zLoadMax) + "A");
    ui->label_bLoad->setText(QString::number(qFabs(data.getBLoad()), 'f', 2) + "/" + QString::number(axisLoad.bLoadMax) + "A");
    ui->label_cLoad->setText(QString::number(qFabs(data.getCLoad()), 'f', 2) + "/" + QString::number(axisLoad.cLoadMax) + "A");
    ui->label_spindleLoad->setText(QString::number(qFabs(data.getSLoad()), 'f', 2) + "/" + QString::number(axisLoad.sLoadMax) + "A");

    ui->horizontalSlider_xLoad->setValue(qFabs(data.getXLoad()*10)); // x10 because slider is in [A/10] unit
    ui->horizontalSlider_yLoad->setValue(qFabs(data.getYLoad()*10)); // x10 because slider is in [A/10] unit
    ui->horizontalSlider_zLoad->setValue(qFabs(data.getZLoad()*10)); // x10 because slider is in [A/10] unit
    ui->horizontalSlider_bLoad->setValue(qFabs(data.getBLoad()*10)); // x10 because slider is in [A/10] unit
    ui->horizontalSlider_cLoad->setValue(qFabs(data.getCLoad()*10)); // x10 because slider is in [A/10] unit
    ui->horizontalSlider_spindleLoad->setValue(qFabs(data.getSLoad()*10)); // x10 because slider is in [A/10] unit

    /* Red label if over nominal current */
    if ( data.getXLoad() > axisLoad.xLoadNom ){
        ui->label_xLoad->setStyleSheet("QLabel { color : red; font-weight: bold;}");
    } else {
        ui->label_xLoad->setStyleSheet("QLabel { color : white; font-weight: normal;}");
    }   
    if ( data.getYLoad() > axisLoad.yLoadNom ){
        ui->label_yLoad->setStyleSheet("QLabel { color : red; font-weight: bold;}");
    } else {
        ui->label_yLoad->setStyleSheet("QLabel { color : white; font-weight: normal;}");
    }   
    if ( data.getZLoad() > axisLoad.zLoadNom ){
        ui->label_zLoad->setStyleSheet("QLabel { color : red; font-weight: bold;}");
    } else {
        ui->label_zLoad->setStyleSheet("QLabel { color : white; font-weight: normal;}");
    }   
    if ( data.getBLoad() > axisLoad.bLoadNom ){
        ui->label_bLoad->setStyleSheet("QLabel { color : red; font-weight: bold;}");
    } else {
        ui->label_bLoad->setStyleSheet("QLabel { color : white; font-weight: normal;}");
    }   
    if ( data.getCLoad() > axisLoad.cLoadNom ){
        ui->label_cLoad->setStyleSheet("QLabel { color : red; font-weight: bold;}");
    } else {
        ui->label_cLoad->setStyleSheet("QLabel { color : white; font-weight: normal;}");
    }   
    if ( data.getSLoad() > axisLoad.sLoadNom ){
        ui->label_spindleLoad->setStyleSheet("QLabel { color : red; font-weight: bold;}");
    } else {
        ui->label_spindleLoad->setStyleSheet("QLabel { color : white; font-weight: normal;}");
    }    


    /* Spindle temperature */
    ui->horizontalSlider_spindle_temp->setValue(data.getSpindleTemp()*10);
    ui->label_spindle_temp->setText(QString::number(data.getSpindleTemp(), 'f', 1) + "/" + QString::number(spindleTemp.maxTemp) + "°C");

    // Fancy stuff to color spindle temperature form blue to red color.
    QColor sTemp_cold = QColor(1, 204, 255);
    QColor sTemp_hot = QColor(255, 1, 1);
    QColor curr;
    curr.setRed(  data.getSpindleTemp() / spindleTemp.maxTemp * (sTemp_hot.red()-sTemp_cold.red())/sTemp_cold.red());
    curr.setBlue(  sTemp_cold.blue() - (data.getSpindleTemp() / spindleTemp.maxTemp * sTemp_cold.blue()));
    curr.setGreen(  sTemp_cold.green() - (data.getSpindleTemp() / spindleTemp.maxTemp * sTemp_cold.green()));
    QPalette sample_palette;
    sample_palette.setColor(QPalette::WindowText, curr);
    ui->label_spindle_temp->setPalette(sample_palette);

    /* Spindle override */
    {
        double spinOverride = _client->getSpindleOverride();
        QString txt;
        if (spinOverride >= 0.0){
            txt = "+" + QString::number(spinOverride) + "%";
        } else {
            txt = QString::number(spinOverride) + "%";
        }
        ui->label_spindle_override->setText(txt);
    }
    

	/* ================== FEEDOPT ================== */

    ui->push_gcodeopen->setEnabled(machineState == MACHINE_STATE_IDLE);
    ui->push_gcodestart->setEnabled(data.getFeedoptReady() && machineState == MACHINE_STATE_IDLE);

	ui->openGLWidget_toolpath->update();
}

void MainWindow::commit()
{
    _timer->stop();
    QString gcode_host_filepath = ui->line_gcodesource->text();

    /* Send tool.tbl */
    _client->uploadFile(toolPath.toStdString(), TARGET_TOOL_TABLE, [](auto, auto){});
    
     /* Send gcode */
    qDebug() << "Uploading " << gcode_host_filepath;
    _client->uploadFile(gcode_host_filepath.toStdString(), "/root/gcode.ngc", [](auto, auto){});

    feedoptConfig = feedoptParam_w->getCurrentConfig();

	// int filename_size[2] = {1, static_cast<int>(strlen("/root/gcode.ngc"))};
	// ocn::ConfigSetSource(&feedoptConfig, "/root/gcode.ngc", filename_size);

    {
        QFile tmp_file{"header.txt"};
        if (!tmp_file.open(QFile::WriteOnly | QFile::Text)) {
            print_to_log("[GUI] Unable to create file: " + tmp_file.fileName());
            return;
        }

        QTextStream txt{&tmp_file};

        txt << "# File: "   << gcode_host_filepath << "\n"
            << "# Time: "   << QDateTime::currentDateTime().toString() << "\n"
            << "# NHorz: "  << feedoptConfig.NHorz << "\n"
            << "# NDiscr: " << feedoptConfig.NDiscr << "\n"
            << "# NBreak: " << feedoptConfig.NBreak << "\n"

            << "# LSplit [mm]: "    << feedoptConfig.LSplit << "\n"
            << "# CutOff [mm]: "    << feedoptConfig.CutOff << "\n"
            << "# SplitSpecialSpline [-]: "    << feedoptConfig.SplitSpecialSpline << "\n"
            << "# vmax X [mm/s]: " << feedoptConfig.vmax[0] << "\n"
            << "# vmax Y [mm/s]: " << feedoptConfig.vmax[1] << "\n"
            << "# vmax Z [mm/s]: " << feedoptConfig.vmax[2] << "\n"
            << "# vmax A [mm/s]: " << feedoptConfig.vmax[3] << "\n"
            << "# vmax B [mm/s]: " << feedoptConfig.vmax[4] << "\n"
            << "# vmax C [mm/s]: " << feedoptConfig.vmax[5] << "\n"

            << "# amax X [mm/s^2]: " << feedoptConfig.amax[0] << "\n"
            << "# amax Y [mm/s^2]: " << feedoptConfig.amax[1] << "\n"
            << "# amax Z [mm/s^2]: " << feedoptConfig.amax[2] << "\n"
            << "# amax A [mm/s^2]: " << feedoptConfig.amax[3] << "\n"
            << "# amax B [mm/s^2]: " << feedoptConfig.amax[4] << "\n"
            << "# amax C [mm/s^2]: " << feedoptConfig.amax[5] << "\n"

            << "# jmax X [mm/s^3]: " << feedoptConfig.jmax[0] << "\n"
            << "# jmax Y [mm/s^3]: " << feedoptConfig.jmax[1] << "\n"
            << "# jmax Z [mm/s^3]: " << feedoptConfig.jmax[2] << "\n"
            << "# jmax A [mm/s^3]: " << feedoptConfig.jmax[3] << "\n"
            << "# jmax B [mm/s^3]: " << feedoptConfig.jmax[4] << "\n"
            << "# jmax C [mm/s^3]: " << feedoptConfig.jmax[5] << "\n";

    }

    qDebug() << "Uploading header";
    _client->uploadFile("header.txt", "/root/header.txt", [](auto, auto){});

    _client->setFeedoptConfig(&feedoptConfig);
	_client->setFeedoptCommitCfg(true);
	_timer->start();
}

void MainWindow::feedoptRecalc()
{
    /* Reset feedopt before, otherwise it crashes.. */
    rpcClientQTIntf->feedoptResetSlot();
    
    /* Pause otherwise it work 1 out of 2 time WTF */
    std::this_thread::sleep_for(0.1s);

    /* Commit the recalculation  */
    commit();

}

void MainWindow::print_to_log(QString msg)
{
    qDebug() << msg;
	ui->listWidget_logs->addItem(msg.trimmed());
	ui->listWidget_logs->scrollToBottom();
}

void MainWindow::on_push_gcoderefresh_clicked()
{
    qDebug() << "Refresh";
	// save the same params to tmp file for writing to logfile
	feedoptConfig = _client->getFeedoptConfig();
}

void MainWindow::on_push_gcodepause_clicked()
{
    _client->setGcodePause(true);
}

void MainWindow::on_push_gcodestart_clicked()
{
    _client->samplerNewFile();
    _client->setGcodeStart(true);

}

void MainWindow::on_space_pressed()
{
	_client->setGcodePause(true);
	_client->setStopJog(true);
}

void MainWindow::on_push_streamstart_clicked()
{
    _client->samplerNewFile();
    _client->setStartStream(true);
}

void MainWindow::on_push_streamstop_clicked()
{
    _client->setStopStream(true);
}

void MainWindow::loadStream()
{

    if (!ui->line_streamerfile->text().isEmpty()) {
        qDebug() << "Uploading " << ui->line_streamerfile->text();
        _client->uploadFile(ui->line_streamerfile->text().toStdString(), "/root/stream.txt", [](auto, auto){});
        {
            QFile tmp_file{"header.txt"};
            if (!tmp_file.open(QFile::WriteOnly|QFile::Text)) {
                print_to_log("[GUI] Unable to create file: " + tmp_file.fileName());
                return;
            }

            QTextStream txt{&tmp_file};
            txt << "# File: " << ui->line_streamerfile->text() << "\n"
                << "# Time: " << QDateTime::currentDateTime().toString() << "\n";
        }

        qDebug() << "Uploading header";
        _client->uploadFile("./header.txt", "/root/header.txt", [](auto, auto){});
        _client->setLoadStream(true);
    } else {
        print_to_log("Please specify streamer filename");
    }
}

void MainWindow::downloadLogs()
{
    QString str_msg;
    uint8_t file_chunk[CHUNK];
    int nbytes = 0;

    QString prefix = "log";
    if (!ui->line_log_prefix->text().isEmpty()) {
        prefix = ui->line_log_prefix->text();
    }

    QString filename_source = "_";
    if (_client->getCyclicData()) {
        const auto& data = _client->getCyclicData().value();
        if (data.getMachineMode().getStream()) {
            const auto parts = ui->line_gcodesource->text().split(QRegExp(R"((\.\/))"), QString::SkipEmptyParts);
            if (parts.size() >= 2) {
                filename_source = "_" + parts[parts.size()-2] + "_";
            }
        }
        else if (data.getMachineMode().getGcode()) {
            const auto parts = ui->line_gcodesource->text().split(QRegExp(R"((\.|\/))"), QString::SkipEmptyParts);
            if (parts.size() >= 2) {
                filename_source = "_" + parts[parts.size()-2] + "_";
            }
        }
    }

    const QString filename_client = prefix + filename_source + QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss") + ".csv";
    QFile file{filename_client};

    if (!file.open(QFile::WriteOnly)) {
        print_to_log("[GUI]: Download failed - unable to create " + filename_client);
        return;
    }

    print_to_log("Downloading logs");

    QMessageBox box;
    box.setModal(true);
    box.setText("Downloading logs");
    box.show();

    if (_client->sendFileParam(TARGET_LOGS_FILENAME, 0, READ)) {
        print_to_log("[SERVER] Error reading file");
        return;
    }

    while (int len = _client->getFileData(file_chunk)) {
        nbytes += len;
        file.write((const char*)file_chunk, len);
    }

    print_to_log("Download finished");
    box.hide();
}

void MainWindow::on_push_setinactive_clicked()
{
    qDebug() << "Push Inactive";
    if (can_change_tab)
        _client->setLcctSetMachineModeInactive(true);
}


void MainWindow::on_push_sethoming_clicked()
{
    qDebug() << "Push Homing";
    if (can_change_tab)
        _client->setLcctSetMachineModeHoming(true);
}


void MainWindow::on_push_setjog_clicked()
{
    qDebug() << "Push Jog";
    if (can_change_tab)
        _client->setLcctSetMachineModeJog(true);
}


void MainWindow::on_push_setstream_clicked()
{
    qDebug() << "Push Stream";
    if (can_change_tab)
        _client->setLcctSetMachineModeStream(true);
}


void MainWindow::on_push_setgcode_clicked()
{
    qDebug() << "Push GCode";
    if (can_change_tab)
        _client->setLcctSetMachineModeGCode(true);
}

void MainWindow::on_push_spin_over_m10_clicked()
{
    double spin_override = _client->getSpindleOverride();
    spin_override -= 10;
    _client->setSpindleOverride(spin_override);
}

void MainWindow::on_push_spin_over_m1_clicked()
{
    double spin_override = _client->getSpindleOverride();
    spin_override -= 1;
    _client->setSpindleOverride(spin_override);
}

void MainWindow::on_push_spin_over_p1_clicked()
{
    double spin_override = _client->getSpindleOverride();
    spin_override += 1;
    _client->setSpindleOverride(spin_override);
}

void MainWindow::on_push_spin_over_p10_clicked()
{
    double spin_override = _client->getSpindleOverride();
    spin_override += 10;
    _client->setSpindleOverride(spin_override);
}

bool MainWindow::canChangeTab(const OpenCNServerInterface::CyclicData::Reader& data)
{
    switch (ui->tabWidget->currentIndex()) {
        case TabIndex::Jog:
            return data.getJogFinished();
        case TabIndex::Stream:
            return !data.getStreamRunning();
        case TabIndex::Homing:
            return data.getHomingFinished();
        case TabIndex::GCode:
            return !data.getGcodeRunning();
        case TabIndex::Inactive:
            return true;
    }
    return false;
}

void MainWindow::show_paramFeedopt()
{
    feedoptParam_w->show();
}

void MainWindow::on_push_editTool_clicked()
{
    editTool_window->loadTool(toolPath);
    editTool_window->show();
}

void MainWindow::on_push_downloadlogs_clicked()
{
    downloadLogs();
}

void MainWindow::offset_changed(double)
{
    _client->setOffset(ui->doubleSpinBox_xOffset->value(),
                       ui->doubleSpinBox_yOffset->value(),
                       ui->doubleSpinBox_zOffset->value(),
                       ui->doubleSpinBox_cOffset->value());

    _client->setHomePositionX(ui->doubleSpinBox_xHome->value());
    _client->setHomePositionY(ui->doubleSpinBox_yHome->value());
    _client->setHomePositionZ(ui->doubleSpinBox_zHome->value());

    ui->openGLWidget_toolpath->getPart()->offsetXChange(ui->doubleSpinBox_xOffset->value());
    ui->openGLWidget_toolpath->getPart()->offsetYChange(ui->doubleSpinBox_yOffset->value());
    ui->openGLWidget_toolpath->getPart()->offsetZChange(ui->doubleSpinBox_zOffset->value());
    ui->openGLWidget_toolpath->getPart()->offsetThetaZChange(ui->doubleSpinBox_cOffset->value());
}

