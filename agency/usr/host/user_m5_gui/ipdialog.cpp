#include <QIntValidator>
#include <QDialogButtonBox>
#include <QtWidgets>

#include "ipdialog.h"
#include "ui_ipdialog.h"

IPDialog::IPDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::IPDialog)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok)->setDefault(true);

    // Add Constrains to 'ip' & 'port' lineEdit boxes
    ui->portEdit->setValidator(new QIntValidator(this));
}

IPDialog::~IPDialog()
{
    delete ui;
}

QString IPDialog::getIP()
{
    return ui->ipEdit->text();
}

uint IPDialog::getPort()
{
    return ui->portEdit->text().toUInt();
}

void IPDialog::setIP(QString ip)
{
	ui->ipEdit->setText(ip);
}

void IPDialog::setPort(uint port)
{
	ui->portEdit->setText(QString::number(port));
}
