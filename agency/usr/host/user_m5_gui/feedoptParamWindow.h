#ifndef PARAMSWINDOW_H
#define PARAMSWINDOW_H

#include <global.hpp>
#include "opencn_matlab_types.h"

#include <QMainWindow>
#include <QSettings>

#define FEEDOPT_MAX_CFG 10
#define FEEDOPT_CFG_SETTINGS "fopt/cfg/"

namespace Ui {
class FeedoptParamWindow;
}

typedef struct{
    int currIndex; // Curent tab index of selectable list
    ocn::FeedoptConfig paramFopt[FEEDOPT_MAX_CFG];
    QString cfgName[FEEDOPT_MAX_CFG];
} ParamCfg;



class FeedoptParamWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit FeedoptParamWindow(QWidget *parent = nullptr);
    ~FeedoptParamWindow();

    ocn::FeedoptConfig getCurrentConfig();

protected:
    void showEvent(QShowEvent *ev);

public slots:
    

private slots:
    void validateCfg();
    void cancelCfg();
    void tabChanged(int index);
    void setListEditable(int state);
    void handleNameChange(QString name);

signals:
    void feedoptRecalc();

private:
    Ui::FeedoptParamWindow *ui;
    void setup();
    ParamCfg cfg;
    
    QSettings settings;

    ocn::FeedoptConfig getUIConfig();


    ocn::FeedoptConfig loadConfigIndex(int index);
    void loadFactoryConfig();

    void saveAllConfig();
    ocn::FeedoptConfig getStoredConfigIndex(int index);

    /* UI manipulation */
    void setUIConfig(ocn::FeedoptConfig cfg);
    void setEnableAllBoxes(bool val);
    void setUIGeometry();
    void saveUIGeometry();
};

#endif // PARAMSWINDOW_H
