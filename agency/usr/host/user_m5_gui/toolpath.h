#ifndef TOOLPATH_H
#define TOOLPATH_H

#include <QObject>
#include <QTimer>
#include <QVector3D>

#include "rpcclient.h"

class ToolPath: public QObject
{
    Q_OBJECT

public:
    ToolPath(QObject *parent = Q_NULLPTR);
    ~ToolPath();

    void setRpcClient(RpcClient *client);

    bool start();
    void stop();


    QVector3D getLastPosition();
    void draw();

signals:
    void isUpdated();

private slots:
    void update();
    void reset();

private:
    QTimer *_updateTimer;
    RpcClient* _client;

    bool _channelstarted;

    QVector3D* _points;
    unsigned int _pointCount;
    unsigned int _writeIdx, _readIdx;
    bool _overlapped;

    static const unsigned int updateIntervalMs;
    static const unsigned int maxPoints;
    static const unsigned int sampleRate;
};

#endif // TOOLPATH_H
