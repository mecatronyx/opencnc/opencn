#ifndef EDITTOOL_H
#define EDITTOOL_H

#include <QMainWindow>
#include <QDebug>
#include <QFile>
namespace Ui {
class EditTool;
}

class EditTool : public QMainWindow
{
    Q_OBJECT

    QString toolfp;

public:
    explicit EditTool(QWidget *parent = nullptr);
    void loadTool(QString toolfilepath);
    ~EditTool();

public slots:
    
signals:
    void toolTable_edited();

private:
    Ui::EditTool *ui;

    void saveTool();
    void cancel();
};

#endif // EDITTOOL_H
