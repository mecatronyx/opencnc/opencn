#include "tool.h"

#include <GL/glu.h>

const float Tool::ToolRadius = 0.5e-3f;
const float Tool::ToolHeight = 0.01f;

Tool::Tool()
    : _position(0.0f, 0.0f, 0.0f)
{

}

void Tool::draw()
{
    glPushMatrix();

    glTranslatef(_position.x(), _position.y(), _position.z());

    glEnable(GL_BLEND);
    glDepthMask(GL_FALSE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);

    GLfloat cyan[] = {.0f, .5f, .5f, .85f};
    glMaterialfv(GL_FRONT, GL_DIFFUSE, cyan);
    GLfloat blue[] = {.0f, .0f, .2f, .85f};
    glMaterialfv(GL_FRONT, GL_AMBIENT, blue);
    GLfloat white[] = {1.f, 1.f, 1.f, .85f};
    glMaterialfv(GL_FRONT, GL_SPECULAR, white);

    GLUquadric* params = gluNewQuadric();

    gluQuadricDrawStyle(params, GLU_FILL);
    gluCylinder(params, static_cast<GLdouble>(ToolRadius), static_cast<GLdouble>(ToolRadius), static_cast<GLdouble>(ToolHeight), 20, 1);

    gluDeleteQuadric(params);

    /* Disk */
    glTranslatef(0.0f, 0.0f, ToolHeight);
    GLUquadric* params2 = gluNewQuadric();
    gluQuadricDrawStyle(params2, GLU_FILL);
    gluDisk(params2, 0.0, static_cast<GLdouble>(ToolRadius), 20, 1);
    gluDeleteQuadric(params2);

    glPopMatrix();
}

void Tool::setPosition(const QVector3D& pos)
{
    _position = pos;
}

void Tool::setPosition(float x, float y, float z)
{
    setPosition(QVector3D(x, y, z));
}
