#include "axes.h"

#include <GL/glu.h>

const float Axes::AxesLength = 0.02f;
const float Axes::AxesTextLength = 0.001f;
const float Axes::AxesTextSize = 0.005f;

Axes::Axes(limits_t& limits)
{
    xmin = limits.xMin / 1000.0f;
    xmax = limits.xMax / 1000.0f;
    ymin = limits.yMin / 1000.0f;
    ymax = limits.yMax / 1000.0f;
    zmin = limits.zMin / 1000.0f;
    zmax = limits.zMax / 1000.0f;
}


void Axes::draw()
{
    glEnable(GL_COLOR_MATERIAL);
    glLineWidth(1.0f);

    /* OpenCN X axis (X OpenGL axis) */
    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_LINES);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(AxesLength, 0.0f, 0.0f);
    glEnd();

    /* X letter */
    glPushMatrix();
    glTranslatef(AxesLength + AxesTextLength, AxesTextSize/2.0f, 0.0);
    glBegin(GL_LINES);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(AxesTextSize, -AxesTextSize, 0.0f);
        glVertex3f(AxesTextSize, 0.0f, 0.0f);
        glVertex3f(0.0f, -AxesTextSize, 0.0f);
    glEnd();
    glPopMatrix();

    /* OpenCN Y axis (Y OpenGL axis) */
    glColor3f(0.0f, 1.0f, 0.0f);
    glBegin(GL_LINES);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(0.0f, AxesLength, 0.0f);
    glEnd();

    /* Y letter */
    glPushMatrix();
    glTranslatef(-AxesTextSize/2.0f, (AxesLength + AxesTextLength), 0.0f);
    glBegin(GL_LINES);
        glVertex3f(AxesTextSize/2.0f, 0.0f, 0.0f);
        glVertex3f(AxesTextSize/2.0f, AxesTextSize/2.0f, 0.0f);
        glVertex3f(AxesTextSize/2.0f, AxesTextSize/2.0f, 0.0f);
        glVertex3f(0.0f, AxesTextSize, 0.0f );
        glVertex3f(AxesTextSize/2.0f, AxesTextSize/2.0f, 0.0f);
        glVertex3f(AxesTextSize, AxesTextSize, 0.0f);
    glEnd();
    glPopMatrix();

    /* OpenCN Z axis (Y OpenGL axis) */
    glColor3f(0.0f, 0.0f, 1.0f);
    glBegin(GL_LINES);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(0.0f, 0.0f, AxesLength);
    glEnd();

    /* Z letter */
    glPushMatrix();
    glTranslatef(-AxesTextSize/2.0f, 0.0f, AxesLength + AxesTextLength);
    glBegin(GL_LINES);
        glVertex3f(AxesTextSize, 0.0f, 0.0f);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glVertex3f(AxesTextSize, 0.0f, AxesTextSize);
        glVertex3f(AxesTextSize, 0.0f, AxesTextSize);
        glVertex3f(0.0f, 0.0f, AxesTextSize);
    glEnd();
    glPopMatrix();

    /* OpenCN workspace cube*/
    glColor3f(1.0f, 1.0f, 0.0f);

    // top face
    glBegin(GL_LINE_LOOP);
        glVertex3f(xmin, ymin, zmax);
        glVertex3f(xmin, ymax, zmax);
        glVertex3f(xmax, ymax, zmax);
        glVertex3f(xmax, ymin, zmax);
    glEnd();

    // bottom face
    glBegin(GL_LINE_LOOP);
        glVertex3f(xmin, ymin, zmin);
        glVertex3f(xmin, ymax, zmin);
        glVertex3f(xmax, ymax, zmin);
        glVertex3f(xmax, ymin, zmin);
    glEnd();

    // origin face
    glBegin(GL_LINE_LOOP);
        glVertex3f(xmin, ymin, 0);
        glVertex3f(xmin, ymax, 0);
        glVertex3f(xmax, ymax, 0);
        glVertex3f(xmax, ymin, 0);
    glEnd();

    // vertical edges
    glBegin(GL_LINES);
        glVertex3f(xmin, ymin, zmax);
        glVertex3f(xmin, ymin, zmin);

        glVertex3f(xmin, ymax, zmax);
        glVertex3f(xmin, ymax, zmin);

        glVertex3f(xmax, ymax, zmax);
        glVertex3f(xmax, ymax, zmin);

        glVertex3f(xmax, ymin, zmax);
        glVertex3f(xmax, ymin, zmin);
    glEnd();

    glDisable(GL_COLOR_MATERIAL);
}
