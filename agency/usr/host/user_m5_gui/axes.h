#ifndef AXES_H
#define AXES_H

#include "rpcclient.h"

class Axes
{
public:
    Axes(limits_t& limits);
    void draw();

private:
    static const float AxesLength;
    static const float AxesTextLength;
    static const float AxesTextSize;

    double xmin;
    double xmax;
    double ymin;
    double ymax;
    double zmin;
    double zmax;
};

#endif /* AXES_H */
