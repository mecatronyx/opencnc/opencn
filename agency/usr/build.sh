#!/bin/bash

function usage {
  echo "$0 [OPTIONS]"
  echo "  -c        Clean"
  echo "  -d        Debug build"
  echo "  -v        Verbose"
  echo "  -s        Single core"
  echo "  -h        Print this help"
}

function install_file_root {
    [ -f $1 ] && echo "Installing $1" && cp $1 build/deploy
  
#    if [ $? -ne 0 ]; then
#        printf "\n${RED}[ERROR] file '$1' does not exist !!${NC}\n"
#        exit -1
#    fi
}

function install_directory_root {
    [ -d $1 ] && echo "Installing $1" && cp -R $1 build/deploy
}

clean=n
debug=n
verbose=n
singlecore=n

while getopts cdhvs option
  do
    case "${option}"
      in
        c) clean=y;;
        d) debug=y;;
        v) verbose=y;;
        s) singlecore=y;;
        h) usage && exit 1;;
    esac
  done

SCRIPT=$(readlink -f $0)
SCRIPTPATH=`dirname $SCRIPT`

if [ $clean == y ]; then
  echo "Cleaning $SCRIPTPATH/build"
  rm -rf $SCRIPTPATH/build
  exit
fi

if [ $debug == y ]; then
  build_type="Debug"
else
  build_type="Release"
fi

echo "Starting $build_type build"
mkdir -p $SCRIPTPATH/build

cd $SCRIPTPATH/build
cmake -DCMAKE_BUILD_TYPE=$build_type -DCMAKE_TOOLCHAIN_FILE=../../rootfs/host/share/buildroot/toolchainfile.cmake -DCMAKE_NO_SYSTEM_FROM_IMPORTED=1 ..
if [ $singlecore == y ]; then
    NRPROC=1
else
    NRPROC=$((`cat /proc/cpuinfo | awk '/^processor/{print $3}' | wc -l` + 1))
fi
if [ $verbose == y ]; then
	make install VERBOSE=1 -j1 || exit -1
else
	make install -j$NRPROC || exit -1
fi
cd -

# Check if user has rust installed, and build term_ui if true
if [ $(command -v cargo) ]; then
cd target/term_ui
cargo rustc -- --cfg TargetOpenCN
cd -
fi

mkdir -p build/deploy/

# Core
install_file_root build/target/core/logfile

# Components
install_file_root build/target/components/curses_gui/curses_gui
# install_file_root build/target/components/feedopt/feedopt
# install_file_root build/target/components/lcct/lcct
# install_file_root build/target/components/lcec/lcec
install_directory_root target/components/lcec/example
# install_file_root build/target/components/loopback/loopback
# install_file_root build/target/components/sampler/sampler
install_file_root build/target/components/sampler/test_sampler_memory # to be moved to proof of concept? example?
install_file_root target/components/sampler/example/one-drive-sampler.hal
# install_file_root build/target/components/streamer/streamer
install_file_root target/components/streamer/example/SetPoints_fast.txt
install_file_root target/components/streamer/example/test_input.txt
install_file_root target/components/streamer/example/cmd.hal
install_file_root target/components/streamer/example/cmd_float.hal
# install_file_root build/target/components/threads/threads
# install_file_root build/target/components/trivkins/trivkins
install_file_root build/target/components/timing_tests/timing_tests
# install_file_root build/target/components/plc/plc
# install_file_root build/target/components/simple_pg/simple_pg
# install_file_root build/target/components/mux/mux
# install_file_root build/target/components/kinematic/kinematic
# install_file_root build/target/components/ocno/ocno

# Halcmd
# install_file_root build/target/halcmd/halcmd

# Proof_of_concept
install_file_root build/target/proof_of_concept/proof-of-concept
install_file_root build/target/proof_of_concept/cpu23_try
install_file_root build/target/proof_of_concept/trysample
install_file_root build/target/proof_of_concept/trylog
install_file_root build/target/proof_of_concept/trypin

install_file_root target/proof_of_concept/rate.hal

# Ethercat command line tool
# install_file_root build/target/ethercat_tool/ethercat

# opencn-server (original server, used by GUI)
install_file_root build/target/opencn_server/opencn-server
install_file_root build/target/opencn_m5_server/opencn-m5-server

# rpc-server
install_file_root build/target/cmctl_server/cmctl-server

# rust term_ui
install_file_root target/term_ui/target/debug/term_ui
