//********************************************************************************************
//                                                                                           *
// AB&T Tecnologie Informatiche - Ivrea Italy                                                *
// http://www.bausano.net                                                                    *
// https://www.ethercat.org/en/products/791FFAA126AD43859920EA64384AD4FD.htm                 *
//                                                                                           *  
//********************************************************************************************    
//                                                                                           *
// This software is distributed as an example, in the hope that it could be useful,          *
// WITHOUT ANY WARRANTY, even the implied warranty of FITNESS FOR A PARTICULAR PURPOSE       *
//                                                                                           *
//******************************************************************************************** 


//----- EasyCAT HAT application basic example for Raspberry ----------------------------------
//----- Derived from the example project TestEasyCAT.ino for the AB&T EasyCAT Arduino shield

#include <stdio.h>
#include <unistd.h>
#include <bcm2835.h>

#include "EasyCAT.h"									 // EasyCAT library to interface     

#define LOBYTE(x) ((unsigned char) ((x) & 0xff))
#define HIBYTE(x) ((unsigned char) ((x) >> 8 & 0xff))

#define OUT_G23 RPI_BPLUS_GPIO_J8_16   // OUTPUT 1

EasyCAT EASYCAT;                    // EasyCAT istantiation

                                    // The constructor allow us to choose the pin used for the EasyCAT HAT chip select 
                                    // Without any parameter pin 24 (CE0) will be used 
                                                                      
                                    // example:                                  
//EasyCAT EASYCAT(RPI_GPIO_P1_26);  // pin 26 (CE1) will be used as SPI chip select


                                    // Remember that the board must be modified to match the pin chosen


int main()
{
     char ledval = 0;
     char ledval_old = 0;
     char test = 0;
     unsigned int duty_cycle = 100;
     unsigned int duty_cycle_old = duty_cycle;

	
	 //---- initialize the EasyCAT board -----

     if (EASYCAT.Init() == true)						 // initialization
     {
       printf("inizialized\n");							 // succesfully completed
     }
     else											 	 // initialization failed   
     {							
       printf("inizialization failed\n");				 // the EasyCAT board was not recognized
	   return -1;
     }			
														 // In the main loop we must call ciclically the 
                                                         // EasyCAT task and our application
                                                         //
                                                         // This allows the bidirectional exachange of the data
                                                         // between the EtherCAT master and our application
                                                         //
                                                         // The EasyCAT cycle and the Master cycle are asynchronous
                                                         //     
		                                                 // The delay allows us to set the EasyCAT cycle time  
                                                         // according to the needs of our application
                                                         //
                                                         // For user interface applications a cycle time of 100mS,
                                                         // or even more, is appropriate, but, for data processing 
                                                         // applications, a faster cycle time may be required
                                                         //
                                                         // In this case we can also completely eliminate this
                                                         // delay in order to obtain the fastest possible response


    bcm2835_gpio_fsel(OUT_G23, BCM2835_GPIO_FSEL_OUTP);

    bcm2835_gpio_write(OUT_G23, 0x1);


    bcm2835_gpio_fsel(RPI_BPLUS_GPIO_J8_32, BCM2835_GPIO_FSEL_ALT0);
    /* 54MHz / 16 = 1.2MHz */
    bcm2835_pwm_set_clock(16);
    /* Allow to have 1/200 steps */
    bcm2835_pwm_set_range(0, 100);
    /* Balanced mode and Enable channel 0 */
    bcm2835_pwm_set_mode(0, 1, 1);
    /* Set it at approx 1/2 duty cycle */
    bcm2835_pwm_set_data(0, duty_cycle);


	while (1)
	{  
	 	  EASYCAT.MainTask();					// execute the EasyCAT task


        duty_cycle = EASYCAT.BufferOut.Byte[2];
        if (duty_cycle != duty_cycle_old) {
            printf("DUTY CYCLE CHANGED %d -> %d!\n", duty_cycle_old, duty_cycle);
            bcm2835_pwm_set_data(0, duty_cycle);
        }
        duty_cycle_old = duty_cycle;

        //   ledval = EASYCAT.BufferOut.Byte[0];

        //   ledval = EASYCAT.BufferOut.Byte[1];

        //   if (ledval != ledval_old) {
        //     //   printf("LED changed: %d\n", ledval);
        //       bcm2835_gpio_write(OUT_G23, ledval);
        //       EASYCAT.BufferIn.Byte[0] = test;
        //       test++;
        //   }
        //   EASYCAT.BufferIn.Byte[1] = ledval;
        //   ledval_old = ledval;

		   usleep(100000);					// delay of 100mS
	}
}