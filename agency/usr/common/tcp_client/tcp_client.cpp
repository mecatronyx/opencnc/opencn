/*
 * Copyright (C) 2021 Elieva Pignat <elieva.pignat@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <arpa/inet.h>
#include <iostream>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <unistd.h>
#include "tcp_client.hpp"

#define MAX_BUFFER_SIZE 1024

TcpClient::TcpClient(const char *apServerIP, unsigned int apPort)
{
    port = apPort;
    server_ip = apServerIP;
}

bool TcpClient::sendMessage(std::string message, bool sync_response,
    std::string *response)
{
    int socket_fd;
    struct sockaddr_in server_addr;
    char buf[MAX_BUFFER_SIZE + 1];
    int bytes_read;

    if (sync_response) {
        if (response == nullptr) {
            /*
            * If a response is expected, the caller needs to provide the
            * response pointer
            */
            return false;
        }
    }

    /* Create socket */
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd == -1) {
        std::cerr << "[TCP Client] Failed to create socket" << std::endl;
        return false;
    }
    bzero(&server_addr, sizeof(server_addr));

    /* Assign server IP address and PORT */
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(server_ip);
    server_addr.sin_port = htons(port);

    /* Connect the client socket to server socket */
    if (connect(socket_fd, (struct sockaddr*)&server_addr,
        sizeof(server_addr)) != 0) {
        std::cerr << "[TCP Client] Failed to connect to server" << std::endl;
        return false;
    }

    /* Send the message to the server */
    write(socket_fd, message.c_str(), strlen(message.c_str()) + 1);

    /* Receive synchronous response if expected */
    if (sync_response) {
        do {
            bytes_read = read(socket_fd, buf, MAX_BUFFER_SIZE);
            if (bytes_read > 0) {
                /*
                 * Add a null terminator to avoid the full buffer to be copied
                 * if a partially string message has been received
                 */
                buf[bytes_read] = '\0';
                *response += buf;
            } else {
                /*
                 * Reach end-of-file or an error occurred. The message may be
                 * ill-shaped.
                 */
                break;
            }
        } while (buf[bytes_read - 1] != '\0');
    }

    /* close the socket */
    close(socket_fd);

    return true;
}

