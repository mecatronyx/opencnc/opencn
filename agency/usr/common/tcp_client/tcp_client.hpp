/*
 * Copyright (C) 2021 Elieva Pignat <elieva.pignat@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef TCP_CLIENT_HPP
#define TCP_CLIENT_HPP

#include <string>

class TcpClient {

    public:
        /*!
         * \brief Constructor of the TCP client
         *
         * \param apServerIP Address IP of the server.
         * \param apPort Port on which the TCP server is listening for
         *      connection.
         */
        TcpClient(const char *apServerIP, unsigned int apPort);

        /*!
         * \brief Connect to the server and send message. If a response is
         *     expected, the client waits to receive the response after sending
         *     a request.
         *
         * \param message The message to be transmitted through this client
         * \param sync_response Indicate if the client needs to receive a
         *      response from the server.
         * \param [out] response Response sent from the server to client after
         *      the client message. Only valid if sync_response is True.
         * \return True if the call is successful, False otherwise.
         *      If the call is not successful, the message should be considered
         *      as not transmitted to the server.
         */
        bool sendMessage(std::string message, bool sync_response,
            std::string *response=nullptr);

    private:
        unsigned int port;
        const char *server_ip;
};

#endif /* TCP_CLIENT_HPP */

