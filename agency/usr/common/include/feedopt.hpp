#pragma once

#include <semaphore.h>
#include <vector>

#include <matlab_headers.h>

#if !defined(MATLAB_MEX_FILE)
#include <opencn/uapi/feedopt.h>
#include "opencn_matlab_types.h"
#include "constrGcodeInfoStruct.h"
#include "constrLineStruct.h"
#include "constrHelixStruct.h"
#include "constrHelixStructFromArcFeed.h"
#include "constrBaseSpline.h"
#include "constrAxesStruct.h"
#include "constrToolStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "initFeedoptPlan.h"
#include "bspline_copy.h"
#include "ResampleStateClass.h"
#include "ResampleState.h"
#include "resampleCurvOCN.h"
#include "evalPositionFromU.h"
#include "PrintCurvStruct.h"
#endif

#define MAX_SHARED_CURV_STRUCTS (1024 * 1024)

#if 0
#define OPENCN_FEEDOPT_DEBUG        1
#endif

struct SharedCurvStructs
{
    sem_t mutex;
    int generation;
    int curv_struct_count;
    ocn::CurvStruct curv_structs[MAX_SHARED_CURV_STRUCTS];
};

void push_curv_struct(const ocn::CurvStruct *curv);

#ifdef DEBUG_RS274

#define DUMMY_FN                                                                          \
    do                                                                                    \
    {                                                                                     \
        fprintf(stderr, "DUMMY_CALL to %s%s%s\n", ASCII_CYAN, __FUNCTION__, ASCII_RESET); \
    } while (0)
#define FNLOG(fmt, args...) \
    fprintf(stderr, "[%s%s%s] " fmt "\n", ASCII_GREEN, __FUNCTION__, ASCII_RESET, ##args)

#else

#define DUMMY_FN
#define FNLOG(...)


#if OPENCN_FEEDOPT_DEBUG
#define DEBUG_PRINT( MSG ) printf( "%s : %s\n", __FUNCTION__, MSG );
#else
#define DEBUG_PRINT( MSG ) 
#endif

#define BUG() do { force_print("BUG\n"); fflush(stdout); exit(-1); } while (0);
#endif

