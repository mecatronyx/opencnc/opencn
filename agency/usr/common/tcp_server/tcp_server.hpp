/*
 * Copyright (C) 2021 Elieva Pignat <elieva.pignat@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef TCP_SERVER_HPP
#define TCP_SERVER_HPP

#include <atomic>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string>

#define MAX_BUFFER_SIZE 256

/*!
 * This TCP server waits for client connections. Once a client is connected,
 * it reads the message sent by the client. Then it calls a callback function
 * provided by the creator of the object. It may respond to the message if
 * requested.
 */
class TcpServer {

    public:
        /*!
         * \brief Constructor of the TCP server
         *
         * \param apPort Port on which the TCP server is listening for
         *      connection.
         * \param apReceiveTcpCallback Callback function used when a packet
         *      has been received. The entire packet is transmitted through it.
         */
        TcpServer(unsigned int apPort,
            void (*apReceiveTcpCallback)(std::string message,
                bool &sync_response, std::string &response, void *data),
            void *apReceiveTcpCallbackData);

        /*!
         * \brief Waits on client request. When a request is received the
         *      callback function receiveTcp() is called.
         * \note: This function does not return except if an error occurred.
         * \note: setup() needs to be called before accept_client()
         *
         * \retval False if an error occurred.
         */
        bool start(void);

        /*! \brief Kill the server */
        void stop(void);

    private:
        /*!
         * \brief Create the socket and attach the socket to the port.
         * \return True if successful, False otherwise.
         */
        bool setup(void);

        /*
        * Thread function launches when a new connection with a client
        * is established
        */
        static void threadFn(int client_socket, TcpServer *server);
    private:
        int socket_fd;
        struct sockaddr_in address;
        int opt;
        int addrlen;
        unsigned int port;

        /*!
        * \brief Callback function to be called when a new message is received
        *       by the server.
        * \param message Message received by the server
        * \param [out] sync_response Indicate if the server should sent an
        *       synchronous response to the client (on the same socket)
        * \param [out] response Response to be sent to the client.
        *       This parameter is only valid if sync_response is True.
        * \param data This data is provided by the caller when it registered
        *       this callback function and it is always provided to it when the
        *       callback function is called.
        */
        void (*receiveTcp)(std::string message, bool &sync_response, std::string &response, void *data);
        void *receiveTcpData;

        std::atomic<bool> stopServer;
};

#endif /* TCP_SERVER_HPP */

