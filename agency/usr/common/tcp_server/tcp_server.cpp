/*
 * Copyright (C) 2021 Elieva Pignat <elieva.pignat@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <iostream>
#include <unistd.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string>
#include <thread>
#include "tcp_client.hpp"
#include "tcp_server.hpp"

#define TCP_SERVER_MAX_CLIENTS 5

TcpServer::TcpServer(unsigned int apPort,
    void (*apReceiveTcpCallback)(std::string message, bool &sync_response,
        std::string &response, void *data),
    void *apReceiveTcpCallbackData)
{
    stopServer = false;
    opt = 1;
    addrlen = sizeof(address);
    port = apPort;
    receiveTcp = apReceiveTcpCallback;
    receiveTcpData = apReceiveTcpCallbackData;
    socket_fd = 0;
}

bool TcpServer::setup(void)
{
    /* Creating socket file descriptor */
    if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
       std::cerr << "[TCP Server] Setup failed" << std::endl;
       return false;
    }

    /* Attaching socket to port */
    if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
        &opt, sizeof(opt))) {
        std::cerr << "[TCP Server] Setup failed" << std::endl;
        return false;
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    /* Bind socket to the address */
    if (bind(socket_fd, (struct sockaddr *)&address, addrlen) < 0) {
        std::cerr << "[TCP Server] Setup failed" << std::endl;
        return false;
    }
    if (listen(socket_fd, TCP_SERVER_MAX_CLIENTS) < 0) {
        std::cerr << "[TCP Server] Setup failed" << std::endl;
        return false;
    }

    return true;
}

bool TcpServer::start(void)
{
    int client_socket;

    if (!setup()) {
        std::cerr << "[TCP server] server setup failed" << std::endl;
        return false;
    }

    while (1) {
        /* Accept a client connection */
        if (stopServer) {
            return true;
        }
        if ((client_socket = accept(socket_fd, (struct sockaddr *)&address,
            (socklen_t*)&addrlen)) < 0) {
            continue;
        }
        if (stopServer) {
            return true;
        }
        /* Launch thread to receive the command */
        std::thread thread(threadFn, client_socket, this);
        thread.detach();
    }

    return true;
}

void TcpServer::threadFn(int client_socket, TcpServer *server)
{
    int bytes_read;
    std::string message;
    char buffer[MAX_BUFFER_SIZE + 1];
    bool sync_response;
    std::string response;

    /*
     * Receive message
     * \note As the message is a string, the null terminator indicates if the
     *      message has been received in full
     * \note The server and client are designed to send only one message per
     *      socket stream. Such that at this point only one message is
     *      expected to be received.
     */
    do {
        bytes_read = read(client_socket, buffer, MAX_BUFFER_SIZE);
        if (bytes_read > 0) {
            /*
             * Add a null terminator to copy only the bytes received
             */
            buffer[bytes_read] = '\0';
            message += buffer;
        } else {
            /*
             * Reached end-of-file or an error occurred. The message may be
             * ill-shaped
             */
            break;
        }
    } while (buffer[bytes_read - 1] != '\0');

    /* Call callback function */
    if (server->receiveTcp != nullptr) {
        server->receiveTcp(message, sync_response, response,
            server->receiveTcpData);
    } else {
        std::cerr << "[TCP Server] No callback function registered"
            << std::endl;
    }

    if (sync_response) {
        /* A response message is sent, if requested */
        write(client_socket, response.c_str(), response.size() + 1);
    }

    /* Close socket */
    close(client_socket);
}

void TcpServer::stop(void)
{
    TcpClient stopClient("127.0.0.1", port);
    stopServer = true;
    /*
     * This message allows the server to get out of the
     * accept() function
     */
    stopClient.sendMessage("Stop server", false);
}
