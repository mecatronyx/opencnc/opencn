#ifndef OCN_CRL_INCLUDE
#define OCN_CRL_INCLUDE

static inline double __ocn_sqrt(double value) {
	double ret;
	asm volatile ("vsqrt.f64 %P0, %P1" : "=t" (ret) : "t" (value));
	return ret;
}

#endif
