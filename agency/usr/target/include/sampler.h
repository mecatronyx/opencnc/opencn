/*
 * Copyright (C) 2014-2021 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *                         Elieva Pignat <elieva.pignat@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef SAMPLER_H
#define SAMPLER_H

#include <opencn/uapi/sampler.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>

#define SAMPLE_CHANNEL_FREQ_NR_PIN	"sampler.0.freq_nr"
#define SAMPLE_CHANNEL_RING_ENABLE_PIN	"sampler.0.ring_enable"

#define SAMPLE_CHANNEL_FREQ_NR		10

#define SAMPLER_SHMEM_NAME 			"sampler"
#define SAMPLER_SHMEM_KEY_CONFIG	60
#define SAMPLER_MSG_KEY             61
#define SAMPLER_SHMEM_PERM          0644

struct shmsg {
    long mtype;
    sampler_sample_t samples;
};

#endif /* SAMPLER_H */

