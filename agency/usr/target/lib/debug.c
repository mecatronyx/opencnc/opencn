/*
 * Copyright (C) 2019 HEIG-VD - Peter Lichard <peter.lichard@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <soo/uapi/debug.h>

#include <stdio.h>
#include <semaphore.h>

static int debug_indent_level = 0;

void debug_function_print(const char *function_name, int diff) {
    int i;

    if (diff > 0) {
        fprintf(stderr, "%s>%s", ASCII_GREEN, ASCII_RESET);
        i = 0;
    }
    if (diff < 0) {
        fprintf(stderr, "%s<%s", ASCII_RED, ASCII_RESET);
        i = 1;
    }
    for(; i < debug_indent_level; i++) {
        fprintf(stderr, "  ");
    }
    fprintf(stderr, "%s\n", function_name);
    debug_indent_level += diff;
}
