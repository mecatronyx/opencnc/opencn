/*
 * Copyright (C) 2019-2020 Daniel Rossier <daniel.rossier@heig-vd.ch>
 * Copyright (C) 2019-2020 Peter Lichard <peter.lichard@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <fcntl.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sampler.h>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>

#include <af.h>
#include <shr.h>

#include <linux/generated/asm-offsets.h>

#include <opencn/uapi/hal.h>                /* HAL public API decls */

#define AFLIB_SAMPLE_RING_NAME "/tmp/aflib-sample-ring"
#define AFLIB_SAMPLE_RING_MAX 1000
#define AFLIB_SAMPLE_RING_SIZE (AFLIB_SAMPLE_RING_MAX * 120)

struct _communicationChannel {
	struct shr *__shr;
	char shr_name[HAL_NAME_LEN];
	void (*close_channel)(communicationChannel_t *channel);
};

static void __close_channel(communicationChannel_t *channel);

static bool hal_open = false;
static int hal_fd = -1;
static rtapi_intptr_t *pin_list_ptr   = NULL;
static rtapi_intptr_t *param_list_ptr = NULL;
static rtapi_intptr_t *sig_list_ptr = NULL;
static rtapi_intptr_t *comp_list_ptr = NULL;
static void *shmem = NULL;
static unsigned long hal_shmem = 0;

#define SHMPTR(offset) ((void *)(shmem + offset))
#define SHMOFF_HAL(addr) ((unsigned long)addr - hal_shmem)

const char *hal_type_to_str(hal_type_t type)
{
	switch (type) {
	case HAL_BIT:
		return "HAL_BIT";
	case HAL_FLOAT:
		return "HAL_FLOAT";
	case HAL_S32:
		return "HAL_S32";
	case HAL_U32:
		return "HAL_U32";
	default:
		return "INVALID_HAL_TYPE";
	}
}

hal_pin_t *pin_find_by_name(const char *name) {
	int next;
	hal_pin_t *pin;
	hal_oldname_t *oldname;

	/* search pin list for 'name' */
	if (pin_list_ptr) {
		next = *pin_list_ptr;
	} else {
		next = 0;
	}

	while (next != 0) {
		pin = SHMPTR(next);

		if (strcmp(pin->name, name) == 0) {
			/* found a match */
			return pin;
		}
		if (pin->oldname != 0) {
			oldname = SHMPTR(pin->oldname);
			if (strcmp(oldname->name, name) == 0) {
				/* found a match */
				return pin;
			}
		}
		/* didn't find it yet, look at next one */
		next = pin->next_ptr;
	}
	/* if loop terminates, we reached end of list with no match */
	return 0;
}

bool pin_as_sig_linked(const char *name){
	hal_pin_t *pin;
	hal_sig_t *sig;

	pin = pin_find_by_name(name);

	/* Check if pin exist */
	if (!pin){
		return false;
	}

	/* Check if signal exist */
	if(pin->signal == 0){
		return false;
	}else{
		return true;
	}

}

char* pin_get_sig_name(const char *name){
	hal_pin_t *pin;
	hal_sig_t *sig;

	pin = pin_find_by_name(name);

	/* Check if pin exist */
	if (!pin){
		return "N/A";
	}

	/* Check if signal exist */
	if(pin->signal == 0){
		return "N/A";
	}

	sig = SHMPTR(pin->signal);

	return sig->name;
}

/*
 * Get the type of a specific pin
 */
hal_type_t pin_get_type(hal_pin_t *pin) {
	return pin->type;
}

/*
 * Get the value associated to a specific pin
 */
hal_data_u *pin_get_value(hal_pin_t *pin) {
	void **p_pin_addr, *p_item;
	void *p_pin_data;
	unsigned long offset;
	hal_data_u *p_pin_value, *p_value;

	if (pin->data_ptr_addr != 0) {
		p_pin_addr = SHMPTR(pin->data_ptr_addr);
		p_item = *p_pin_addr;

		offset = (unsigned long) SHMOFF_HAL(p_item);

		p_pin_data = (void *) SHMPTR(offset);
	} else {
        p_pin_data = &(pin->dummysig);
    }

	p_pin_value = (hal_data_u *) p_pin_data;

	return p_pin_value;
}

hal_bit_t *pin_get_bit_ptr(const char* name) {
	hal_pin_t *pin = pin_find_by_name(name);
	if (pin == NULL) {
		printf("[ERROR] Could not find pin named: %s name\n", name);
		return NULL;
	}

	if (pin->type != HAL_BIT) {
		printf("[ERROR][HAL] pin_get_bit_ptr: Wrong type, is %s\n", hal_type_to_str(pin->type));
		return NULL;
	}

	hal_data_u *value = pin_get_value(pin);
	if (value) {
		return &value->b;
	}
	else {
		return NULL;
	}
}

hal_s32_t *pin_get_s32_ptr(const char *name) {
	hal_pin_t *pin;
	hal_data_u *value;

	pin = pin_find_by_name(name);
	if (pin == NULL) {
		printf("[ERROR][HAL] Could not find pin named: %s name\n", name);
		return NULL;
	}

	if (pin->type != HAL_S32) {
		printf("[ERROR][HAL] pin_get_s32_ptr: Wrong type, is %s\n", hal_type_to_str(pin->type));
		return NULL;
	}

	value = pin_get_value(pin);
	if (value) {
		return &value->s;
	}
	else {
		return NULL;
	}
}

hal_u32_t *pin_get_u32_ptr(const char *name) {
    hal_pin_t *pin = pin_find_by_name(name);
	hal_data_u *value;
    if (pin == NULL) {
        printf("[ERROR][HAL] Could not find pin named: %s name\n", name);
        return NULL;
    }

    if (pin->type != HAL_U32) {
        printf("[ERROR][HAL] pin_get_u32_ptr: Wrong type, is %s\n", hal_type_to_str(pin->type));
        return NULL;
    }

    value = pin_get_value(pin);
    if (value) {
        return &value->u;
    }
    else {
        return NULL;
    }
}


hal_float_t *pin_get_float_ptr(const char* name) {
	hal_pin_t *pin;
	hal_data_u *value;

	pin = pin_find_by_name(name);
	if (pin == NULL) {
		printf("[ERROR] Could not find pin named: %s name\n", name);
		return NULL;
	}

	if (pin->type != HAL_FLOAT) {
		printf("[ERROR][HAL] pin_get_float_ptr: Wrong type, is %s\n", hal_type_to_str(pin->type));
		return NULL;
	}

	value = pin_get_value(pin);
	if (value) {
		return &value->f;
	}
	else {
		return NULL;
	}
}



/*
 * Create a new anonymous pin in the HAL.
 */
hal_pin_t *pin_new(const char *name, hal_type_t type, hal_pin_dir_t dir) {
	hal_anon_pin_args_t hal_anon_pin_args;
	int rc;

	hal_anon_pin_args.name = name;
	hal_anon_pin_args.type = type;
	hal_anon_pin_args.dir = dir;

	rc = ioctl(hal_fd, HAL_IOCTL_ANON_PIN_NEW, &hal_anon_pin_args);
	if (rc) {
		printf("!! pin_new failed with rc = %d\n", rc);
		return NULL;
	}

	/* Now returns the address to this pin. */
	return pin_find_by_name(name);
}

/*
 * Delete an anonymous pin from the HAL.
 */
void pin_delete(const char *name) {
	ioctl(hal_fd, HAL_IOCTL_ANON_PIN_DELETE, (unsigned long)name);
}



/***********************************************************************
 *                        PARAMS FUNCTIONS                             *
 ***********************************************************************/

hal_param_t *param_find_by_name(const char *name)
{
	int next;
	hal_param_t *param;
	hal_oldname_t *oldname;

	/* search parameter list for 'name' */
	if (param_list_ptr) {
		next = *param_list_ptr;
	} else {
		next = 0;
	}

	while (next != 0) {
		param = SHMPTR(next);

		if (strcmp(param->name, name) == 0) {
			/* found a match */
			return param;
		}
		if (param->oldname != 0) {
			oldname = SHMPTR(param->oldname);
			if (strcmp(oldname->name, name) == 0) {
				/* found a match */
				return param;
			}
		}
		/* didn't find it yet, look at next one */
		next = param->next_ptr;
	}
	/* if loop terminates, we reached end of list with no match */
	return 0;
}


/*
 * Get the value associated to a specific param
 */
hal_data_u *param_get_value(hal_param_t *param)
{
	void **p_param_addr, *p_item;
	void *p_param_data;
	unsigned long offset;
	hal_data_u *p_param_value, *p_value;

	if (param->data_ptr != 0) {
		p_param_data = (void *) SHMPTR(param->data_ptr);
	}

	p_param_value = (hal_data_u *) p_param_data;

	return p_param_value;
}

/***********************************************************************
 *                        COMPONENT FUNCTIONS                          *
 ***********************************************************************/

hal_comp_t *comp_find_by_name(const char *name)
{
	int next;
	hal_comp_t *comp;

	/* search component list for 'name' */
	if (comp_list_ptr) {
		next = *comp_list_ptr;
	} else {
		next = 0;
	}

	while (next != 0) {
		comp = SHMPTR(next);

		if (strcmp(comp->name, name) == 0) {
			/* found a match */
			return comp;
		}

		/* didn't find it yet, look at next one */
		next = comp->next_ptr;
	}

	/* if loop terminates, we reached end of list with no match */
	return NULL;

}

/***********************************************************************
 *                        CHANNEL FUNCTIONS                            *
 ***********************************************************************/

/*
 * Handling samples through a circular ring buffer
 */
int open_sample_channel(communicationChannel_t **channel, int flags) {

	*channel = malloc(sizeof(struct _communicationChannel));
	strcpy((*channel)->shr_name, AFLIB_SAMPLE_RING_NAME);

	/* First, init the ring */
	shr_init((*channel)->shr_name, AFLIB_SAMPLE_RING_SIZE, SHR_MLOCK | SHR_MAXMSGS_2 | SHR_KEEPEXIST | SHR_DROP,
									AFLIB_SAMPLE_RING_MAX);

	(*channel)->__shr = shr_open((*channel)->shr_name, ((flags == O_RDONLY) ? SHR_RDONLY : SHR_WRONLY) | SHR_NONBLOCK);

	(*channel)->close_channel = __close_channel;

	return (((*channel)->__shr == NULL) ? -1 : 0);
}

/*
 * Write the sample within the ring. Return 0 if the write operation has overwritten a sample.
 */
ssize_t write_sample(communicationChannel_t *channel, const sampler_sample_t *sample) {
	ssize_t ret;
	struct shr_stat cur_stat, stat;

	shr_stat(channel->__shr, &cur_stat, NULL);
	ret = shr_write(channel->__shr, (char *) sample->pins, sample->n_pins*sizeof(sampler_value_t));
	shr_stat(channel->__shr, &stat, NULL);

	/* Check if an unread message (sample) has been dropped following this write. */
	if (stat.md != cur_stat.md)
		return 0;

	return ret;
}

ssize_t read_sample(communicationChannel_t *channel, sampler_sample_t* sample) {
	ssize_t ret;

	ret = shr_read(channel->__shr, (char*)sample->pins, sizeof(sample->pins));
	sample->n_pins = ret/sizeof(sampler_value_t);

	return ret;
}

bool sample_channel_enable(bool enable) {
	hal_pin_t *sample_channel_enable_pin;
	hal_data_u *sample_channel_enable;

	/* If the sample_channel_enable_pin does not exit yet, it will be created. */
	sample_channel_enable_pin = pin_find_by_name(SAMPLE_CHANNEL_RING_ENABLE_PIN);
	if (!sample_channel_enable_pin) {
		sample_channel_enable_pin = pin_new(SAMPLE_CHANNEL_RING_ENABLE_PIN, HAL_BIT, HAL_IO);
		if (!sample_channel_enable_pin) {
			return false;
		}
	}

	sample_channel_enable = pin_get_value(sample_channel_enable_pin);
	sample_channel_enable->b = enable;

	return true;
}

bool sample_channel_enabled(void) {
	hal_pin_t *sample_channel_enable_pin;
	hal_data_u *sample_channel_enable;

	sample_channel_enable_pin = pin_find_by_name(SAMPLE_CHANNEL_RING_ENABLE_PIN);
	if (!sample_channel_enable_pin)
		return false;

	sample_channel_enable = pin_get_value(sample_channel_enable_pin);

	return sample_channel_enable->b;
}

bool sample_channel_set_freq(unsigned int freq) {
	hal_pin_t *sample_freq_nr_pin;
	hal_data_u *sample_freq_nr;

	sample_freq_nr_pin = pin_find_by_name(SAMPLE_CHANNEL_FREQ_NR_PIN);
	if (!sample_freq_nr_pin) {
		sample_freq_nr_pin = pin_new(SAMPLE_CHANNEL_FREQ_NR_PIN, HAL_U32, HAL_IO);
		if (!sample_freq_nr_pin) {
			return false;
		}
	}

	sample_freq_nr = pin_get_value(sample_freq_nr_pin);
	sample_freq_nr->u = freq;

	return true;
}

/*
 * Get the current freq. If the associated pin is not yet available, returns -1.
 */
int sample_channel_get_freq(void) {
	hal_pin_t *sample_freq_nr_pin;
	hal_data_u *sample_freq_nr;

	sample_freq_nr_pin = pin_find_by_name(SAMPLE_CHANNEL_FREQ_NR_PIN);
	if (!sample_freq_nr_pin)
		return -1;

	sample_freq_nr = pin_get_value(sample_freq_nr_pin);

	return sample_freq_nr->u;

}

static void __close_channel(communicationChannel_t *channel) {
	int ret;

	shr_close(channel->__shr);

	/*
	 * We do not remove the dir entry related to this pipe since it may
	 * be re-used by another process.
	 */
	free(channel);
}

/***************/

/*
 * rtapi_print_msg logging facility
 *
 * returns -1 if the pin does not exist.
 */

int rtapi_log_enable(bool enable) {
	hal_pin_t *rtapi_log_enable_pin;
	bool *rtapi_log_enable;

	rtapi_log_enable_pin = pin_find_by_name(HAL_RTAPI_LOG_PIN);

	if (!rtapi_log_enable_pin)
		return -1;

	rtapi_log_enable = (bool *) pin_get_value(rtapi_log_enable_pin);

	*rtapi_log_enable = enable;

    return 0;
}

int rtapi_log_read(char *msg, ssize_t maxlen) {
	int len;

	len = read(hal_fd, msg, maxlen);

	return len;
}

/***************/

void af_exit(void) {
	munlockall();
	close(hal_fd);
}

/*
 * Retrieve the pins information
 */
void __attribute__((constructor)) af_init(void) {

	if (hal_open)
		return;

	printf("%s: initializing...\n", __func__);

	/*
	 * Open the char device to access the kernel-side HAL.
	 */
	if (!hal_open) {
		hal_fd = open("/dev/opencn/hal/0", O_RDWR | O_SYNC);
		hal_open = true;
	}

	if (hal_fd >= 0) {

		/* Retrieve the virtual address of the core hal user shared memory.
		 * Useful to compute offset in order to access our local mapped shared memory.
		 */
		ioctl(hal_fd, HAL_IOCTL_GET_SHMEM_ADDR, &hal_shmem);

		/* In the hal, shmem is also used as a reference to hal_data_t structure. */

		shmem = mmap(NULL, HAL_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_LOCKED, hal_fd, 0);

		pin_list_ptr   = (rtapi_intptr_t *) (shmem + HAL_SHMEM_PINS);
		param_list_ptr = (rtapi_intptr_t *) (shmem + HAL_SHMEM_PARAMS);
		sig_list_ptr = (rtapi_intptr_t *) (shmem + HAL_SHMEM_SIGS);
		comp_list_ptr = (rtapi_intptr_t *) (shmem + HAL_SHMEM_COMP);

		printf("AFLIB initialization DONE\n");

	} else {
		pin_list_ptr = NULL;
		param_list_ptr = NULL;
		sig_list_ptr = NULL;
		comp_list_ptr = NULL;
	}
}
