/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2023 by Xavier Soltner <xaver.soltner@heig-vd.ch>
 *
 * file: spindle.cpp
 *
 *
 * It is the user-space part of the "spindle" OpenCN HAL component. 
 * The user-space checks the load parameters and sends it to the kernel part.
 * 
 * 
 ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <cxxopts.hpp>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <sys/ipc.h>
#include <sys/shm.h>

#include <af.h>
#include <cmdline.h>

#include <opencn/uapi/spindle.h>

#include <debug.h>

#define MAX_FILENAME_LEN 1000

int devfd;
int exitval = 1; /* program return code - 1 means error */
int ignore_sig = 0; /* used to flag critical regions */

spindle_connect_args_t args_spindle;

/***********************************************************************
 *                            HAL FUNC 	                              *
 ************************************************************************/
/* signal handler */
static void quit(int sig) {
	if (ignore_sig == 1) {
		return;
	}
}

static int spindle_connect(char *devname, spindle_connect_args_t *args_in)
{
    int ret;

    devfd = open(devname, O_RDWR);

    ret = ioctl(devfd, SPINDLE_IOCTL_CONNECT, args_in);
    if (ret != 0){
        printf("[%s] ERROR: Connect failed (%d)\n", devname, ret);
        BUG()
    }

    close(devfd);

    return ret;
}

void spindle_disconnect(char *devname) {

    devfd = open(devname, O_RDWR);

    ioctl(devfd, SPINDLE_IOCTL_DISCONNECT, args_spindle.instance_nr);

    close(devfd);
}

static int spindle_get_instance_nr(void)
{
    int fd;
    int ret;
    spindle_instance_nr_args_t args = { 0 };

    fd = open(SPINDLE_DEV_NAME, O_RDWR);

    ret = ioctl(fd, SPINDLE_IOCTL_INSTANCE_NR, &args);
    if (ret != 0) {
        printf("[SPINDLE] ERROR - Retrieve 'spindle' instance number failed \n");
        BUG()
    }

    close(fd);

    return args.instance_nr;
}

/***********************************************************************
 *                            PARSER FUNC                              *
 ************************************************************************/

void init_args (spindle_connect_args_t *args){
    args->cfg_id = 0;
    args->has_temperature = 0;
    args->has_cooling = 0;
    args->instance_nr=0;
}

/* Parse the input argument of the spindle using cxxopts */
std::pair<bool,spindle_connect_args_t> parse_spindle_input(int argc, char* argv[]){
    
    bool is_valid = true;
    spindle_connect_args_t ret;

    init_args(&ret);

    cxxopts::Options options(SPINDLE_COMP_NAME, "Spindle component");

    options.add_options()
        ("c,cfg", "(opt) Configuration as <char*>, std or atc", cxxopts::value<std::string>())
        ("n,name", "(opt) Name for spindle instance : spindle.<arg>", cxxopts::value<std::string>())
        ("C,cooling", "(opt) Provide a cooling pin, active when spindle is enabled", cxxopts::value<bool>()->default_value("false"))
        ("t,temperature", "(opt) Provide temperature monitoring", cxxopts::value<bool>()->default_value("false"))
        ;
    options.allow_unrecognised_options();

    try {
        /* Parse input */
        auto result = options.parse(argc, argv);
        auto unmatched = result.unmatched();

        /* Check if invalid argument is present */
        if (unmatched.size() != 0) {
            printf("[%s] Error - The following arguments are not valid: ", SPINDLE_COMP_NAME);
            for ( auto & element : unmatched)
                printf("'%s' ", element.c_str());
            printf("\n");
            is_valid = false;
        }
        /* Check if we have duplicate argument - only for cfg and name */
        if ( result.count("cfg") > 1 ){
            printf("[%s] Error - duplicate argument %s", SPINDLE_COMP_NAME,"cfg");
            is_valid = false;
        }
        if ( result.count("name") > 1 ){
            printf("[%s] Error - duplicate argument %s", SPINDLE_COMP_NAME,"name");
            is_valid = false;
        }

        /* Configuration ('c/cfg' option) */
        if (result.count("cfg") == 1) {
            std::string cfg_s = result["cfg"].as<std::string>();
            printf("[%s] cfg=%s\n",SPINDLE_COMP_NAME, cfg_s.c_str());

            if (cfg_s == "std"){
                ret.cfg_id = SPINDLE_CFG_STD;
            } else if (cfg_s == "atc"){
                ret.cfg_id = SPINDLE_CFG_ATC;
            } else {
                printf("[%s] Error - '%s' is unknow configuration\n",SPINDLE_COMP_NAME, cfg_s.c_str());
                ret.cfg_id = SPINDLE_CFG_UNKNOWN;
                is_valid = false;
            }
        } else {
            /* Standard config if cfg is unspecified*/
            ret.cfg_id = SPINDLE_CFG_STD;
        }

        /* Name of the spindle ('n/name' option)*/
        if (result.count("name") == 1) {
            std::string name_s = result["name"].as<std::string>();
            char* name = const_cast<char*>(name_s.c_str()); // We need char* for kernel side

            if(name_s.length() > SPINDLE_NAME_MAX_LENGTH){
                printf("[%s] Error : 'name' has length=%ld, max is SPINDLE_NAME_MAX_LENGTH=%d, aborting \n", SPINDLE_COMP_NAME, name_s.length(), SPINDLE_NAME_MAX_LENGTH);
                is_valid = false;
            } else {
                strcpy(ret.name,name);
                printf("[%s] name=%s\n",SPINDLE_COMP_NAME,ret.name);
            }            
        } else {
            /* Get instance number from kernel */
            ret.instance_nr = spindle_get_instance_nr();
            sprintf(ret.name,"%d",ret.instance_nr);
        }
        /* Cooling mode ('C/cooling' option)*/
        if (result.count("cooling") == 1) {
            ret.has_cooling = true;
            printf("[%s] cooling=%d\n",SPINDLE_COMP_NAME,ret.has_cooling);
        }
        /* Temperature mode ('t/temperature' option)*/
        if (result.count("temperature") == 1) {
            ret.has_temperature = true;
            printf("[%s] temperature=%d\n",SPINDLE_COMP_NAME,ret.has_temperature);
        }
    } catch (const cxxopts::exceptions::exception& e) {
    
        is_valid = false;
        printf("[%s] cxxpopts::exceptions : %s\n", SPINDLE_COMP_NAME, e.what());     
       
    }
    /* Print help function */
    if(!is_valid){
        printf("%s\n",options.help().c_str());
    }
    return std::make_pair(is_valid,ret);
    
}

/***********************************************************************
 *                            MAIN PROGRAM                              *
 ************************************************************************/

/*
 * Syntax: spindle [--cfg=<CONFIG>] [--cooling] [--temperature]
 */
int main(int argc, char **argv)
{
	int ret;
    bool args_is_valid = false;
    char comp_name[HAL_NAME_LEN + 1];

    /* Set return code to "fail", clear it later if all goes well */
    exitval = 1;
    /* Get input argument */
    std::pair<bool, spindle_connect_args_t> r = parse_spindle_input(argc,argv);
    args_is_valid = r.first;
    args_spindle = r.second;


	/* Check if input arg is valid*/
    if( !args_is_valid){
        exitval=-EINVAL;
        goto out;
    }
    
    /* Check if component name is duplicate */
    sprintf(comp_name, "%s.%s",SPINDLE_COMP_NAME,args_spindle.name);
    if (comp_find_by_name(comp_name) != NULL){
        fprintf(stderr, "[%s] ERROR: '%s' component already exist, specify another name, abort\n",  SPINDLE_COMP_NAME, comp_name);
        exitval=-EEXIST;
        goto out;
    }

    /* register signal handlers - if the process is killed
     we need to call hal_exit() to free the shared memory */
    signal(SIGINT, quit);
    signal(SIGTERM, quit);
//    signal(SIGPIPE, SIG_IGN);

    /* connect to the HAL */
    ignore_sig = 1;
    ret = spindle_connect(SPINDLE_DEV_NAME, &args_spindle);
    ignore_sig = 0;

    /* Check result */
    if (ret < 0) {
        fprintf(stderr, "[%s] ERROR: %s failed with ret = %d\n", __func__, SPINDLE_COMP_NAME, ret);
        goto out;
    }

    /* run was succesfull */
    exitval = 0;

out:

	fprintf(stderr, "[%s.%s] Exited with ret=%d\n", SPINDLE_COMP_NAME, args_spindle.name, exitval);

    /* Inform the parent (normally halcmd) that we are up and running. */
    kill(getppid(), SIGUSR1);

    return exitval;
}

