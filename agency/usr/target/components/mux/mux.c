/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: mux.c
 *
 * 
 * It is the user-space part of the "mux" OpenCN HAL component. The user-space
 * only checks the load parameters and sends it to the kernel part
 *
 ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <debug.h>
#include <cmdline.h>

#include <opencn/uapi/mux.h>

/* used to flag critical regions */
static int ignore_sig = 0;

static int mux_set_type(int *type, char *param, char *name)
{
    int ret = 0;

    if (!strcmp(param, "bit")) {
        *type = HAL_BIT;
    } else if (!strcmp(param, "float")) {
        *type = HAL_FLOAT;
    } else if (!strcmp(param, "signed")) {
        *type = HAL_S32;
    } else if (!strcmp(param, "unsigned")) {
        *type = HAL_U32;
    } else {
        fprintf(stderr, "[%s] ERROR: type '%s' is not supported\n", name, param);
        ret = -1;
    }

    return ret;
}


static int check_config(mux_connect_args_t *args, char *name)
{
    int ret = 0;

    if (args->vector_nr < MUX_DEFAULT_VECTOR_NR) {
        fprintf(stderr, "[%s] ERROR: 'vector_nr' has to be at least 1 !\n", name);
        ret = -1;
    }

    if (args->vector_size < MUX_DEFAULT_VECTOR_SIZE) {
        fprintf(stderr, "[%s] ERROR: 'vector_size' has to be at least 2 !\n", name);
        ret = -1;
    }

    return ret;
}

/* signal handler */
static void quit(int sig) {
    if (ignore_sig == 1) {
        return;
    }
    /* Inform the parent (normally halcmd) that we are up and running. */
    kill(getppid(), SIGUSR1);
    exit(0);
}


/* Connect to the kernel HAL. */
static int mux_connect(char *devname, mux_connect_args_t *args, char *name)
{
    int devfd;
    int ret = 0;

    devfd = open(devname, O_RDWR);

    ret = ioctl(devfd, MUX_IOCTL_CONNECT, args);
    if (ret != 0) {
        printf("[%s] ERROR: Connect failed (%d)\n", name, ret);
        BUG()
    }

    close(devfd);

    return ret;
}

static int mux_get_instance_nr(void)
{
    int fd;
    int ret;
    mux_instance_nr_args_t args = { 0 };

    fd = open(MUX_DEV_NAME, O_RDWR);

    ret = ioctl(fd, MUX_IOCTL_INSTANCE_NR, &args);
    if (ret != 0) {
        printf("[MUX] ERROR - Retrieve 'mux' instance number failed \n");
        BUG()
    }

     close(fd);

    return args.instance_nr;
}

 /*
 * Syntax: mux name= type=<HAL TYPE>  vector_nr=<> vector_size=<>
 */
int main(int argc, char **argv)
{
    int i;
    int ret = 0;
    char *params = NULL, *value = NULL;
    char *cp;
    char *type = NULL;
    char mux_name[HAL_NAME_LEN];

    mux_connect_args_t args;

    /* Set default parameters values */
    strcpy(args.name, MUX_DEFAULT_NAME);
    args.vector_nr   = MUX_DEFAULT_VECTOR_NR;
    args.vector_size = MUX_DEFAULT_VECTOR_SIZE;

    for (i = 1; i < argc; i++) {
        cp = argv[i];

        /* First parse the option with "=" */
        next_arg(argv[i], &params, &value);

        if (params) {
            if (!strcmp(params, "name")) {
                strcpy(args.name, value);
                continue ;
            }

            if (!strcmp(params, "type")) {
                type = value;
                continue ;
            }

            if (!strcmp(params, "vector_nr")) {
                args.vector_nr = atoi(value);
                continue ;
            }

            if (!strcmp(params, "vector_size")) {
                args.vector_size = atoi(value);
                continue ;
            }
        }
        if (*cp != '-') {
            fprintf(stderr, "[MUX] ERROR: unknown option '%s'\n", cp);
            goto out;
        }
    }

    /* Get the 'mux' name */
    if (!strcmp(args.name, MUX_DEFAULT_NAME))
        sprintf(mux_name, "%s.%d", MUX_COMP_NAME, mux_get_instance_nr());
    else
        sprintf(mux_name, "%s.%s", MUX_COMP_NAME, args.name);

    if (type == NULL) {
        fprintf(stderr, "[%s] ERROR: Please select a type !\n", mux_name);
        goto out;
    } else {
        ret = mux_set_type(&args.type, type, mux_name);
        if (ret)
            goto out;
    }

    /* Check parameters */
    ret = check_config(&args, mux_name);
    if (ret)
        goto out;

    /* register signal handlers - if the process is killed
     we need to call hal_exit() to free the shared memory */
    signal(SIGTERM, quit);

    /* connect to the HAL */
    ignore_sig = 1;
    ret = mux_connect(MUX_DEV_NAME, &args, mux_name);
    ignore_sig = 0;

    /* check result */
    if (ret < 0) {
        fprintf(stderr, "[%s] ERROR: mux_connect failed with ret = %d\n", mux_name, ret);
        goto out;
    }

out:
    /* Inform the parent (normally halcmd) that we are up and running. */
    kill(getppid(), SIGUSR1);

    return ret;
}
