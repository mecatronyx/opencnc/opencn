#include <assert.h>
#include <curses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <af.h>
#include <virtshare/opencn/feedopt.h>

static struct timeval tv0;

static hal_bit_t *fopt_rt_active_pin = NULL;
static hal_bit_t *fopt_us_active_pin = NULL;
static hal_float_t *target_x_pin = NULL;
static hal_float_t *target_y_pin = NULL;
static hal_u32_t *fopt_buffer_underrun_pin = NULL;
static hal_bit_t *fopt_opt_rt_reset_pin = NULL;
static hal_bit_t *fopt_opt_us_reset_pin = NULL;
static hal_u32_t *fopt_queue_size_pin = NULL;
static hal_float_t *fopt_opt_per_second_pin = NULL;

static feedopt_opt_config cfg;

static int feedopt_devfd = 0;

double msnow() {
	struct timeval tv;
	gettimeofday(&tv, NULL);

	return (tv.tv_sec - tv0.tv_sec) * 1000.0 + (tv.tv_usec - tv0.tv_usec) / 1000.0;
}

enum {
	P_TOGGLE_ACTIVE = 1, P_TOGGLE_INACTIVE, P_INPUT_FOCUS, P_INPUT_NORMAL
};

typedef enum {
	ELEM_TOGGLE, ELEM_INPUT_double, ELEM_OUTPUT_double, ELEM_INPUT_INT, ELEM_OUTPUT_INT
} ELEM_TYPE;

typedef struct {
	union {
		struct {
			hal_bit_t *active;
			int x;
			int y;
			const char *active_text;
			const char *inactive_text;
		} toggle;

		struct {
			int x;
			int y;
			const char *text;
			double *value;
			char tmp_value[16];
			int tmp_value_n;
		} input_double;

		struct {
			int x;
			int y;
			const char *text;
			int *value;
			char tmp_value[16];
			int tmp_value_n;
		} input_int;

		struct {
			int x;
			int y;
			const char *text;
			hal_float_t *value;
		} output_double;

		struct {
			int x;
			int y;
			const char *text;
			hal_u32_t *value;
		} output_int;
	};
	ELEM_TYPE type;
} gui_elem_t;

typedef void*(*gui_cb)(volatile void*);

// gui callback struct
typedef struct {
	gui_cb fn;
	volatile void* arg;
} gui_cb_def;

struct {
	gui_elem_t elems[20];
	int elem_count;
	gui_elem_t *focused;
	gui_elem_t *focus_key[255];
	gui_cb_def callback_key[255];
	WINDOW* main_window;
	WINDOW* log_window;
} gui;

void gui_init() {
	gui.elem_count = 0;
	gui.focused = NULL;
	memset(gui.focus_key, 0, sizeof(gui.focus_key));
	memset(gui.callback_key, 0, sizeof(gui.callback_key));
}

void gui_check_cb(int c) {
	if (c < 0 || c >= 255)
		return;
	if (gui.callback_key[c].fn != NULL) {
		gui.callback_key[c].fn(gui.callback_key[c].arg);
	}
}

void gui_add_focus_key(gui_elem_t *e, int c) {
	assert(c >= 0 && c < 255);
	gui.focus_key[c] = e;
}

void gui_add_callback_key(int c, gui_cb fn, volatile void *arg) {
	assert(c >= 0 && c < 255);
	gui.callback_key[c].fn = fn;
	gui.callback_key[c].arg = arg;
}

int gui_check_focus(int c) {
	if (c < 0 || c >= 255)
		return 0;
	gui_elem_t *e = gui.focus_key[c];
	if (e) {
		switch (e->type) {
			case ELEM_TOGGLE:
				*e->toggle.active = !*e->toggle.active;
				gui.focused = NULL;
				break;
			case ELEM_INPUT_double:
				gui.focused = e;
				break;
			case ELEM_INPUT_INT:
				gui.focused = e;
				break;
			default:
				gui.focused = NULL;
				break;
		}
	} else {
		gui.focused = NULL;
	}
	return e != NULL;
}

void gui_on_key_input_double(gui_elem_t *e, int c) {
	if (c == '\n') {
		char *end;
		double value = strtof(e->input_double.tmp_value, &end);
		if (end != e->input_double.tmp_value) {
			*e->input_double.value = value;
		}
		gui.focused = NULL;
	}
	if (c == 'q')
		gui.focused = NULL;
	if (((c >= '0' && c <= '9') || c == '.' || c == '-' || c == '+') && e->input_double.tmp_value_n < 16) {
		e->input_double.tmp_value[e->input_double.tmp_value_n++] = c;
	}
	if (c == 127 && e->input_double.tmp_value_n > 0) {
		e->input_double.tmp_value[--e->input_double.tmp_value_n] = 0;
	}
}

void gui_on_key_input_int(gui_elem_t *e, int c) {
	if (c == '\n') {
		char *end;
		double value = strtol(e->input_int.tmp_value, &end, 10);
		if (end != e->input_int.tmp_value) {
			*e->input_int.value = value;
		}
		gui.focused = NULL;
	}
	if (c == 'q')
		gui.focused = NULL;
	if (((c >= '0' && c <= '9') || c == '-' || c == '+') && e->input_int.tmp_value_n < 16) {
		e->input_int.tmp_value[e->input_int.tmp_value_n++] = c;
	}
	if (c == 127 && e->input_int.tmp_value_n > 0) {
		e->input_int.tmp_value[--e->input_int.tmp_value_n] = 0;
	}
}

gui_elem_t *gui_add_toggle(int y, int x, hal_bit_t *active, const char *active_text, const char *inactive_text) {
	assert(active && active_text && inactive_text);
	gui_elem_t *e = gui.elems + gui.elem_count++;
	e->toggle.x = x;
	e->toggle.y = y;
	e->toggle.active = active;
	e->toggle.active_text = active_text;
	e->toggle.inactive_text = inactive_text;
	e->type = ELEM_TOGGLE;
	return e;
}

gui_elem_t *gui_add_input_double(int y, int x, const char *text, double *value) {
	assert(value && text);
	gui_elem_t *e = gui.elems + gui.elem_count++;
	e->input_double.x = x;
	e->input_double.y = y;
	e->input_double.text = text;
	e->input_double.value = value;
	e->input_double.tmp_value_n = 0;
	memset(e->input_double.tmp_value, 0, sizeof(e->input_double.tmp_value));
	e->type = ELEM_INPUT_double;
	return e;
}

gui_elem_t *gui_add_input_int(int y, int x, const char *text, int *value) {
	assert(value && text);
	gui_elem_t *e = gui.elems + gui.elem_count++;
	e->input_int.x = x;
	e->input_int.y = y;
	e->input_int.text = text;
	e->input_int.value = value;
	e->input_int.tmp_value_n = 0;
	memset(e->input_int.tmp_value, 0, sizeof(e->input_int.tmp_value));
	e->type = ELEM_INPUT_INT;
	return e;
}

void gui_add_output_int(int y, int x, const char *text, hal_u32_t *value) {
	assert(text && value);
	gui_elem_t *e = gui.elems + gui.elem_count++;
	e->output_int.x = x;
	e->output_int.y = y;
	e->output_int.text = text;
	e->output_int.value = value;
	e->type = ELEM_OUTPUT_INT;
}

void gui_add_output_double(int y, int x, const char *text, hal_float_t *value) {
	assert(text && value);
	gui_elem_t *e = gui.elems + gui.elem_count++;
	e->output_double.x = x;
	e->output_double.y = y;
	e->output_double.text = text;
	e->output_double.value = value;
	e->type = ELEM_OUTPUT_double;
}

void gui_refresh() {
	for (int i = 0; i < gui.elem_count; i++) {
		gui_elem_t *e = gui.elems + i;
		switch (e->type) {
			case ELEM_TOGGLE: {
				if (*e->toggle.active) {
					wattron(gui.main_window, COLOR_PAIR(P_TOGGLE_ACTIVE));
					mvwaddstr(gui.main_window, e->toggle.y, e->toggle.x, e->toggle.active_text);
					attroff(COLOR_PAIR(P_TOGGLE_ACTIVE));
				} else {
					wattron(gui.main_window, COLOR_PAIR(P_TOGGLE_INACTIVE));
					mvwaddstr(gui.main_window, e->toggle.y, e->toggle.x, e->toggle.inactive_text);
					attroff(COLOR_PAIR(P_TOGGLE_INACTIVE));
				}
				break;
			}
			case ELEM_INPUT_double: {
				if (e == gui.focused) {
					wattron(gui.main_window, COLOR_PAIR(P_INPUT_FOCUS));
					mvwaddstr(gui.main_window, e->input_double.y, e->input_double.x, e->input_double.text);
					mvwprintw(gui.main_window, e->input_double.y,
													e->input_double.x
																					+ strlen(
																													e->input_double.text),
													" : %20s",
													e->input_double.tmp_value);
					attroff(COLOR_PAIR(P_INPUT_FOCUS));
				} else {
					wattron(gui.main_window, COLOR_PAIR(P_INPUT_NORMAL));
					mvwaddstr(gui.main_window, e->input_double.y, e->input_double.x, e->input_double.text);
					mvwprintw(gui.main_window, e->input_double.y,
													e->input_double.x
																					+ strlen(
																													e->input_double.text),
													" : %+20.3f",
													*e->input_double.value);
					attroff(COLOR_PAIR(P_INPUT_NORMAL));
				}

				break;
			}
			case ELEM_OUTPUT_double:
				wattron(gui.main_window, COLOR_PAIR(P_INPUT_NORMAL));
				mvwaddstr(gui.main_window, e->output_double.y, e->output_double.x, e->output_double.text);
				mvwprintw(gui.main_window, e->output_double.y, e->output_double.x + strlen(e->output_double.text),
												" : %+20.3f",
												*e->output_double.value);
				attroff(COLOR_PAIR(P_INPUT_NORMAL));
				break;

			case ELEM_OUTPUT_INT:
				wattron(gui.main_window, COLOR_PAIR(P_INPUT_NORMAL));
				mvwaddstr(gui.main_window, e->output_int.y, e->output_int.x, e->output_int.text);
				mvwprintw(gui.main_window, e->output_int.y, e->output_int.x + strlen(e->output_int.text),
												" : %+20d", *e->output_int.value);
				attroff(COLOR_PAIR(P_INPUT_NORMAL));
				break;
			case ELEM_INPUT_INT: {
				if (e == gui.focused) {
					wattron(gui.main_window, COLOR_PAIR(P_INPUT_FOCUS));
					mvwaddstr(gui.main_window, e->input_int.y, e->input_int.x, e->input_int.text);
					mvwprintw(gui.main_window, e->input_int.y, e->input_int.x + strlen(e->input_int.text),
													" : %20s",
													e->input_int.tmp_value);
					attroff(COLOR_PAIR(P_INPUT_FOCUS));
				} else {
					wattron(gui.main_window, COLOR_PAIR(P_INPUT_NORMAL));
					mvwaddstr(gui.main_window, e->input_int.y, e->input_int.x, e->input_int.text);
					mvwprintw(gui.main_window, e->input_int.y, e->input_int.x + strlen(e->input_int.text),
													" : %+20d",
													*e->input_int.value);
					attroff(COLOR_PAIR(P_INPUT_NORMAL));
				}

				break;
			}
		}
	}
}

void* toggle_hal_bit_pin(hal_bit_t* pin) {
	*pin = !*pin;
	return NULL;
}

void* cb_reset_op(volatile void* arg) {
	*fopt_us_active_pin = 0;
	*fopt_rt_active_pin = 0;
	*fopt_opt_rt_reset_pin = 1;
	*fopt_opt_us_reset_pin = 1;
	feedopt_devfd = open(FEEDOPT_DEV_NAME, O_RDWR);
	ioctl(feedopt_devfd, FEEDOPT_IOCTL_WRITE_CFG, &cfg);
	close(feedopt_devfd);
	return NULL;
}

void init_colors() {
	start_color();
	init_pair(P_TOGGLE_ACTIVE, COLOR_WHITE, COLOR_GREEN);
	init_pair(P_TOGGLE_INACTIVE, COLOR_WHITE, COLOR_BLACK);
	init_pair(P_INPUT_FOCUS, COLOR_WHITE, COLOR_BLUE);
	init_pair(P_INPUT_NORMAL, COLOR_WHITE, COLOR_BLACK);
}

void init_curses() {
	initscr();
	if (!has_colors()) {
		printf("Your terminal does not support colors!\n");
		endwin();
		exit(1);
	}
	/*int w, h;
	 getmaxyx(stdscr, h, w);*/
	gui.main_window = newwin(0, 0, 0, 0);
	/* gui.log_window = newwin(h/2, w/2, h/2, w/2); */

	idcok(stdscr, 0);
	idlok(stdscr, 0);
	gui_init();
	init_colors();
	cbreak();
	noecho();
	/* hide cursor */
	curs_set(0);
	timeout(100);
}

void init() {
	gettimeofday(&tv0, NULL);

	af_init();
	fopt_rt_active_pin = &pin_get_value(pin_find_by_name("feedopt.rt-active"))->b;
	fopt_us_active_pin = &pin_get_value(pin_find_by_name("feedopt.us-active"))->b;

	fopt_opt_rt_reset_pin = &pin_get_value(pin_find_by_name("feedopt.opt-rt-reset"))->b;
	fopt_opt_us_reset_pin = &pin_get_value(pin_find_by_name("feedopt.opt-us-reset"))->b;

	target_x_pin = &pin_get_value(pin_find_by_name("feedopt.sample-0"))->f;
	target_y_pin = &pin_get_value(pin_find_by_name("feedopt.sample-1"))->f;

	fopt_buffer_underrun_pin = &pin_get_value(pin_find_by_name("feedopt.buffer-underrun-count"))->u;
	fopt_queue_size_pin = &pin_get_value(pin_find_by_name("feedopt.queue-size"))->u;

	fopt_opt_per_second_pin = &pin_get_value(pin_find_by_name("feedopt.opt-per-second"))->f;
}

void deinit() {
	endwin();
}

int main() {
	init();
	printf("Press a key to continue, or Ctrl-C to abort\n");
	getchar();
	init_curses();
	int should_stop = 0;

	gui_add_toggle(0, 0, fopt_rt_active_pin, "(r) Feedopt-rt ON ", "(r) Feedopt-rt OFF");
	gui_add_toggle(1, 0, fopt_us_active_pin, "(u) Feedopt-us ON ", "(u) Feedopt-us OFF");

	gui_elem_t *gui_vmax = gui_add_input_double(2, 0, "(v) vmax", &cfg.vmax);
	gui_elem_t *gui_amax = gui_add_input_double(3, 0, "(a) amax", &cfg.amax);
	gui_elem_t *gui_jmax = gui_add_input_double(4, 0, "(j) jmax", &cfg.jmax);
	gui_elem_t *gui_nhorz = gui_add_input_int(5, 0, "(h) nhorz", &cfg.nhorz);
	gui_elem_t *gui_nbreak = gui_add_input_int(6, 0, "(b) nbreak", &cfg.nbreak);
	gui_elem_t *gui_ndiscr = gui_add_input_int(7, 0, "(d) ndiscr", &cfg.ndiscr);
	gui_elem_t *gui_spline_degree = gui_add_input_int(8, 0, "(s) spline degree", &cfg.spline_degree);

	int fopt_reset = *fopt_opt_rt_reset_pin || *fopt_opt_us_reset_pin;
	gui_add_toggle(10, 0, (hal_bit_t*) &fopt_reset, "(o) Reset Optimization", "(o) Reset Optimization");

	gui_add_output_double(0, 50, "Target X", target_x_pin);
	gui_add_output_double(1, 50, "Target Y", target_y_pin);
	gui_add_output_int(3, 50, "Queue", fopt_queue_size_pin);
	gui_add_output_int(4, 50, "Buffer Underrun", fopt_buffer_underrun_pin);
	gui_add_output_double(5, 50, "Opt/s", fopt_opt_per_second_pin);

	gui_add_callback_key('u', (gui_cb) toggle_hal_bit_pin, fopt_us_active_pin);
	gui_add_callback_key('r', (gui_cb) toggle_hal_bit_pin, fopt_rt_active_pin);
	gui_add_callback_key('o', cb_reset_op, NULL);

	gui_add_focus_key(gui_nhorz, 'h');
	gui_add_focus_key(gui_nbreak, 'b');
	gui_add_focus_key(gui_ndiscr, 'd');
	gui_add_focus_key(gui_spline_degree, 's');

	gui_add_focus_key(gui_vmax, 'v');
	gui_add_focus_key(gui_amax, 'a');
	gui_add_focus_key(gui_jmax, 'j');

	int last_c = 0;

	clear();

	while (!should_stop) {

		fopt_reset = *fopt_opt_us_reset_pin || *fopt_opt_rt_reset_pin;

		gui_refresh();
		mvprintw(20, 0, "c = %d", last_c);
		wrefresh(gui.main_window);
		int c = getch();
		last_c = c;
		if (!gui.focused) {
			if (!gui_check_focus(c)) {
				if (c == 'q') {
					should_stop = 1;
				}
				if (c == 'c') {
					wclear(gui.main_window);
				}
			}
			gui_check_cb(c);
		} else {
			switch (gui.focused->type) {
				case ELEM_INPUT_double:
					gui_on_key_input_double(gui.focused, c);
					break;
				case ELEM_INPUT_INT:
					gui_on_key_input_int(gui.focused, c);
					break;
				default:
					break;
			}
		}
	}
	deinit();
	return 0;
}
