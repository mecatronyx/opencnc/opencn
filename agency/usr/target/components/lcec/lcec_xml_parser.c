/********************************************************************
 *  Copyright (C) 2012 Sascha Ittner <sascha.ittner@modusoft.de>
 *  Copyright (C) 2019 Jean-Pierre Miceli Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <expat.h>

#include <opencn/uapi/lcec_conf.h>

#include "lcec_conf_priv.h"

static const lcec_conf_typelist_t slaveTypes[] = {
	/* bus coupler */
	{ "EK1100", lcecSlaveTypeEK1100 },

	/* digital in */
	{ "EL1252", lcecSlaveTypeEL1252 },

	{ "EL1002", lcecSlaveTypeEL1002 },
	{ "EL1004", lcecSlaveTypeEL1004 },
	{ "EL1008", lcecSlaveTypeEL1008 },
	{ "EL1012", lcecSlaveTypeEL1012 },
	{ "EL1014", lcecSlaveTypeEL1014 },
	{ "EL1018", lcecSlaveTypeEL1018 },
	{ "EL1024", lcecSlaveTypeEL1024 },
	{ "EL1034", lcecSlaveTypeEL1034 },
	{ "EL1084", lcecSlaveTypeEL1084 },
	{ "EL1088", lcecSlaveTypeEL1088 },
	{ "EL1094", lcecSlaveTypeEL1094 },
	{ "EL1098", lcecSlaveTypeEL1098 },
	{ "EL1104", lcecSlaveTypeEL1104 },
	{ "EL1114", lcecSlaveTypeEL1114 },
	{ "EL1124", lcecSlaveTypeEL1124 },
	{ "EL1134", lcecSlaveTypeEL1134 },
	{ "EL1144", lcecSlaveTypeEL1144 },
	{ "EL1808", lcecSlaveTypeEL1808 },
	{ "EL1809", lcecSlaveTypeEL1809 },
	{ "EL1819", lcecSlaveTypeEL1819 },

	/* digital out */
	{ "EL2252", lcecSlaveTypeEL2252 },

	{ "EL2002", lcecSlaveTypeEL2002 },
	{ "EL2004", lcecSlaveTypeEL2004 },
	{ "EL2008", lcecSlaveTypeEL2008 },
	{ "EL2022", lcecSlaveTypeEL2022 },
	{ "EL2024", lcecSlaveTypeEL2024 },
	{ "EL2032", lcecSlaveTypeEL2032 },
	{ "EL2034", lcecSlaveTypeEL2034 },
	{ "EL2042", lcecSlaveTypeEL2042 },
	{ "EL2084", lcecSlaveTypeEL2084 },
	{ "EL2088", lcecSlaveTypeEL2088 },
	{ "EL2124", lcecSlaveTypeEL2124 },
	{ "EL2612", lcecSlaveTypeEL2612 },
	{ "EL2622", lcecSlaveTypeEL2622 },
	{ "EL2634", lcecSlaveTypeEL2634 },
	{ "EL2808", lcecSlaveTypeEL2808 },
	{ "EL2798", lcecSlaveTypeEL2798 },
	{ "EL2809", lcecSlaveTypeEL2809 },

	{ "EP2008", lcecSlaveTypeEP2008 },
	{ "EP2028", lcecSlaveTypeEP2028 },
	{ "EP2809", lcecSlaveTypeEP2809 },

	/* analog in */
	{ "EL3702", lcecSlaveTypeEL3702 },

	/* Analog Input Terminals Pt100 */
	{ "EL3202", lcecSlaveTypeEL3202 },

	/* Triamec Drives */
	{ "TSD80E", lcecSlaveTypeTSD80E },

	/* Beckhoff Drives */
	{ "AX5100", lcecSlaveTypeAX5100 },
	{ "AX5200", lcecSlaveTypeAX5200 },
	{ "EL7411", lcecSlaveTypeEL7411 },

	/* Pneumatic valve control */
	{ "CTEU", lcecSlaveTypeCTEU } ,

	/* Maxon Drive */
	{ "EPOS4", lcecSlaveTypeEPOS4 },

	/* Companion slave */
	{ "COMPANION", lcecSlaveTypeCompanion },

	{ NULL }
};

lcec_master_t *first_master = NULL;
static lcec_master_t *last_master  = NULL;

static void parseMasterAttrs(lcec_conf_xml_inst_t *inst, int next, const char **attr)
{
	lcec_master_t *master = (lcec_master_t *)malloc(sizeof(lcec_master_t));

	while (*attr) {
		const char *name = *(attr++);
		const char *val = *(attr++);

		/* parse index */
		if (strcmp(name, "idx") == 0) {
			master->index = atoi(val);
			continue;
		}

		/* parse name */
		if (strcmp(name, "name") == 0) {
			strncpy(master->name, val, LCEC_CONF_STR_MAXLEN);
			master->name[LCEC_CONF_STR_MAXLEN - 1] = 0;
			continue;
		}

		/* parse appTimePeriod */
		if (strcmp(name, "appTimePeriod") == 0) {
			master->app_time_period = atol(val);
			continue;
		}

		/* handle error */
		fprintf(stderr, "%s: ERROR: Invalid master attribute %s\n", module_name, name);
		XML_StopParser(inst->parser, 0);
		return;
	}

	/* set default name */
	if (master->name[0] == 0) {
		snprintf(master->name, LCEC_CONF_STR_MAXLEN, "%d", master->index);
	}

	/* add master to list */
	LCEC_LIST_APPEND(first_master, last_master, master);
}

static void parseSlaveAttrs(lcec_conf_xml_inst_t *inst, int next, const char **attr)
{
	const lcec_conf_typelist_t *slaveType = NULL;
	lcec_slave_t *slave;
	bool type_found;

	slave = (lcec_slave_t *)malloc(sizeof(lcec_slave_t));
	memset(slave, 0, sizeof(lcec_slave_t));

	while (*attr) {
		const char *name = *(attr++);
		const char *val = *(attr++);

		if (strcmp(name, "type") == 0) {
			type_found = false;
			for (slaveType = slaveTypes; slaveType->name != NULL; slaveType++) {
				if (strcmp(val, slaveType->name) == 0) {
					slave->type = slaveType->type;
					strncpy(slave->type_name, slaveType->name, LCEC_CONF_STR_MAXLEN);
					slave->type_name[LCEC_CONF_STR_MAXLEN - 1] = 0;

					type_found = true;
					break;
				}
			}

			if (!type_found) {
				fprintf(stderr, "%s: ERROR: Invalid slave type %s\n", module_name, val);
				XML_StopParser(inst->parser, 0);
				return;
			}

			continue;
		}

		/* parse index */
		if (strcmp(name, "idx") == 0) {
			slave->index = atoi(val);
			continue;
		}

		/* parse name */
		if (strcmp(name, "name") == 0) {
			strncpy(slave->name, val, LCEC_CONF_STR_MAXLEN);
			slave->name[LCEC_CONF_STR_MAXLEN - 1] = 0;
			continue;
		}

		/* handle error */
		fprintf(stderr, "%s: ERROR: Invalid slave attribute %s\n", module_name, name);
		XML_StopParser(inst->parser, 0);
		return;
	}

	/* set default name */
	if (slave->name[0] == 0) {
		snprintf(slave->name, LCEC_CONF_STR_MAXLEN, "%d", slave->index);
	}

	slave->master = last_master;

	/* add slave to list */
	LCEC_LIST_APPEND(last_master->first_slave, last_master->last_slave, slave);
}

static int parseSyncCycle(uint32_t appTimePeriod, const char *nptr)
{
	/* chack for master period multiples */
	if (*nptr == '*') {
		nptr++;
		return atoi(nptr) * appTimePeriod;
	}

	/* custom value */
	return atoi(nptr);
}

static void parseDcConfAttrs(lcec_conf_xml_inst_t *inst, int next, const char **attr)
{
	lcec_slave_dc_t *slave_dc = (lcec_slave_dc_t *)malloc(sizeof(lcec_slave_dc_t));

	while (*attr) {
		const char *name = *(attr++);
		const char *val = *(attr++);

		/* parse assignActivate (hex value) */
		if (strcmp(name, "assignActivate") == 0) {
			slave_dc->assignActivate = strtol(val, NULL, 16);
			continue;
		}

		/* parse sync0Cycle */
		if (strcmp(name, "sync0Cycle") == 0) {
			slave_dc->sync0Cycle = parseSyncCycle(last_master->app_time_period, val);
			continue;
		}

		/* parse sync0Shift */
		if (strcmp(name, "sync0Shift") == 0) {
			slave_dc->	sync0Shift = atoi(val);
			continue;
		}

		/* parse sync1Cycle */
		if (strcmp(name, "sync1Cycle") == 0) {
			slave_dc->sync1Cycle = parseSyncCycle(last_master->app_time_period, val);
			continue;
		}

		/* parse sync1Shift */
		if (strcmp(name, "sync1Shift") == 0) {
			slave_dc->sync1Shift = atoi(val);
			continue;
		}

		/* handle error */
		fprintf(stderr, "%s: ERROR: Invalid dcConfig attribute %s\n", module_name, name);
		XML_StopParser(inst->parser, 0);
		return;
	}

	last_master->last_slave->dc_conf = slave_dc;
}

static void parseInitCmdsAttrs(lcec_conf_xml_inst_t *inst, int next, const char **attr)
{
	const char *filename = NULL;

	while (*attr) {
		const char *name = *(attr++);
		const char *val = *(attr++);

		/* parse filename */
		if (strcmp(name, "filename") == 0) {
			filename = val;
			continue;
		}

		/* handle error */
		fprintf(stderr, "%s: ERROR: Invalid syncManager attribute %s\n", module_name, name);
		XML_StopParser(inst->parser, 0);
		return;
	}

	/* filename is required */
	if (filename == NULL || *filename == 0) {
		fprintf(stderr, "%s: ERROR: initCmds has no filename attribute\n", module_name);
		XML_StopParser(inst->parser, 0);
		return;
	}

	/* try to parse initCmds */
	if (parseIcmds(last_master->last_slave, filename)) {
		XML_StopParser(inst->parser, 0);
		return;
	}
}

static const lcec_conf_xml_hanlder_t xml_states[] = {
	{ "masters",  lcecConfTypeNone,    lcecConfTypeMasters,  NULL,               NULL },
	{ "master",   lcecConfTypeMasters, lcecConfTypeMaster,   parseMasterAttrs,   NULL },
	{ "slave",    lcecConfTypeMaster,  lcecConfTypeSlave,    parseSlaveAttrs,    NULL },
	{ "dcConf",   lcecConfTypeSlave,   lcecConfTypeDcConf,   parseDcConfAttrs,   NULL },
	{ "initCmds", lcecConfTypeSlave,   lcecConfTypeInitCmds, parseInitCmdsAttrs, NULL },
	{ "NULL", -1, -1, NULL, NULL }
};

static void xml_start_handler(void *data, const char *el, const char **attr)
{
	lcec_conf_xml_inst_t *inst = (lcec_conf_xml_inst_t *) data;
	const lcec_conf_xml_hanlder_t *state;

	for (state = inst->states; state->el != NULL; state++) {
		if (inst->state == state->state_from && (strcmp(el, state->el) == 0)) {
			if (state->start_handler != NULL) {
				state->start_handler(inst, state->state_to, attr);
			}
			inst->state = state->state_to;
			return;
		}
	}

	fprintf(stderr, "%s: ERROR: unexpected node %s found\n", module_name, el);
	XML_StopParser(inst->parser, 0);
}

static void xml_end_handler(void *data, const char *el)
{
	lcec_conf_xml_inst_t *inst = (lcec_conf_xml_inst_t *) data;
	const lcec_conf_xml_hanlder_t *state;

	for (state = inst->states; state->el != NULL; state++) {
		if (inst->state == state->state_to && (strcmp(el, state->el) == 0)) {
			if (state->end_handler != NULL) {
				state->end_handler(inst, state->state_from);
			}
			inst->state = state->state_from;
			return;
		}
	}

	fprintf(stderr, "%s: ERROR: unexpected close tag %s found\n", module_name, el);
	XML_StopParser(inst->parser, 0);
}


int initXmlInst(lcec_conf_xml_inst_t *inst, const lcec_conf_xml_hanlder_t *states)
{
	/* create xml parser */
	inst->parser = XML_ParserCreate(NULL);
	if (inst->parser == NULL) {
		return 1;
	}

	/* setup data */
	inst->states = states;
	inst->state = 0;
	XML_SetUserData(inst->parser, inst);

	/* setup handlers */
	XML_SetElementHandler(inst->parser, xml_start_handler, xml_end_handler);

	return 0;
}

int lcec_parse_xml(char *filename, lcec_conf_xml_inst_t *xml_inst)
{
	int done;
	char buffer[LCEC_BUFFSIZE];
	FILE *file;

	/* open file */
	file = fopen(filename, "r");
	if (file == NULL) {
		fprintf(stderr, "%s: ERROR: unable to open config file %s\n", module_name, filename);
		goto parse_fail0;
	}

	/* create xml parser */
	memset(xml_inst, 0, sizeof(lcec_conf_xml_inst_t));
	if (initXmlInst((lcec_conf_xml_inst_t *) xml_inst, xml_states)) {
		fprintf(stderr, "%s: ERROR: Couldn't allocate memory for parser\n", module_name);
		goto parse_fail1;
	}

	/* initialize list */
	first_master = NULL;
	last_master  = NULL;

	for (done=0; !done;) { /* TODO replace with while loop */
		/* read block */
		int len = fread(buffer, 1, LCEC_BUFFSIZE, file);
		if (ferror(file)) {
			fprintf(stderr, "%s: ERROR: Couldn't read from file %s\n", module_name, filename);
			goto parse_fail2;
		}

		/* check for EOF */
		done = feof(file);

		/* parse current block */
		if (!XML_Parse(xml_inst->parser, buffer, len, done)) {
			fprintf(stderr, "%s: ERROR: Parse error at line %u: %s\n", module_name,
				      (unsigned int)XML_GetCurrentLineNumber(xml_inst->parser),
			XML_ErrorString(XML_GetErrorCode(xml_inst->parser)));
			goto parse_fail2;
		}
	}

	fclose(file);

	return 0;

parse_fail2:
	XML_ParserFree(xml_inst->parser);
parse_fail1:
	fclose(file);
parse_fail0:
	return -1;
}

void lcec_clear_config(void)
{
	lcec_master_t *master;
	lcec_master_t *m_next;
	lcec_slave_t *slave;
	lcec_slave_t *s_next;
	lcec_slave_sdoconf_t *sdo;
	lcec_slave_sdoconf_t *sdo_next;

	for (master = first_master; master != NULL; /*master = master->next*/) {
		m_next = master->next;

		for (slave = master->first_slave; slave != NULL; /* slave = slave->next */) {
			s_next = slave->next;

			if (slave->dc_conf) {
				free(slave->dc_conf);
			}
			if (slave->sdo_config) {
				for (sdo = slave->sdo_config; sdo != NULL;  /* sdo = sdo->next */) {
					sdo_next = sdo->next;
					free(sdo->data);
					free(sdo);

					sdo = sdo_next;
				}
			}
			free(slave);

			slave = s_next;
		}
		free(master);

		master = m_next;
	}
}

void print_parsed_cfg(void)
{
	lcec_master_t *master;
	lcec_slave_t *slave;
	lcec_slave_sdoconf_t *sdo;
	lcec_slave_idnconf_t *idn;
	int i;

	printf("Decoded configuration:\n");
	for (master = first_master; master != NULL; master = master->next) {
		printf("Master #%d\n", master->index);
		printf("  name: %s\n", master->name);

		printf("  app_time_period: %d\n", master->app_time_period);
		for (slave = master->first_slave; slave != NULL; slave = slave->next) {
			printf("  slave #%d\n", slave->index);
			printf("    type: %d\n", slave->type);
			printf("    type name: %s\n", slave->type_name);
			printf("    name: %s\n", slave->name);
			if (slave->dc_conf) {
				printf("    dc, assignActivate: %x\n", slave->dc_conf->assignActivate);
				printf("    dc, sync0Cycle: %d\n", slave->dc_conf->sync0Cycle);
				printf("    dc, sync0Shift: %d\n", slave->dc_conf->sync0Shift);
			} else {
				printf("    no dc config\n");
			}
			if (slave->sdo_config) {
				for (sdo = slave->sdo_config; sdo != NULL; sdo = sdo->next) {
					printf("    sdo, index: %d\n", sdo->index);
					printf("    sdo, subindex: %d\n", sdo->subindex);
					printf("    sdo, length: %lu\n", (long unsigned) sdo->length);
					printf("    sdo, data: ");
					for (i=0; i<sdo->length; i++)
						printf("%02x", sdo->data[i]);
					printf("\n\n");
				}
			} else {
				printf("    no sdo config\n");
			}

			if (slave->idn_config) {
				for (idn = slave->idn_config; idn != NULL; idn = idn->next) {
					printf("    idn,  drive: %d\n", idn->drive);
					printf("    idn,  idn: %d\n", idn->idn);
					printf("    idn,  state: %d\n", idn->state);
					printf("    idn,  length: %lu\n", (long unsigned) idn->length);
					printf("    idn,  data: ");
					for (i=0; i<idn->length; i++)
						printf("%02x", idn->data[i]);
					printf("\n\n");
				}
			} else {
				printf("    no idn config\n");
			}
		}
	}
}

