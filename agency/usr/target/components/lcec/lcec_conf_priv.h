/********************************************************************
 *  Copyright (C) 2012 Sascha Ittner <sascha.ittner@modusoft.de>
 *  Copyright (C) 2019 Jean-Pierre Miceli Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

#ifndef _LCEC_CONF_PRIV_H_
#define _LCEC_CONF_PRIV_H_

#include <stdbool.h>
#include <expat.h>

#include <soo/errno-base.h>

#define LCEC_BUFFSIZE 8192

typedef struct lcec_conf_xml_inst {
	XML_Parser parser;
	const struct lcec_conf_xml_hanlder *states;
	int state;
} lcec_conf_xml_inst_t;

typedef struct lcec_conf_xml_hanlder {
	const char *el;
	int state_from;
	int state_to;
	void (*start_handler)(struct lcec_conf_xml_inst *inst, int next, const char **attr);
	void (*end_handler)(struct lcec_conf_xml_inst *inst, int next);
} lcec_conf_xml_hanlder_t;

typedef struct {
	const char *name;
	lcec_slave_type_t type;
} lcec_conf_typelist_t;

extern char *module_name;
extern lcec_master_t *first_master;

int initXmlInst(lcec_conf_xml_inst_t *inst, const lcec_conf_xml_hanlder_t *states);

int lcec_parse_xml(char *filename, lcec_conf_xml_inst_t *state);
int parseIcmds(lcec_slave_t *slave, const char *filename);
void lcec_clear_config(void);
void print_parsed_cfg(void);

#endif /* _LCEC_CONF_PRIV_H_ */
