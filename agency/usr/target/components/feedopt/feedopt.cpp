/********************************************************************
 *  Copyright (C) 2019  Peter Lichard  <peter.lichard@heig-vd.ch>
 *  Copyright (C) 2022 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *  Copyright (C) 2022 Gabriel Catel Torres <arzur.cateltorres@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

#include <fcntl.h>
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>

#include <sys/ipc.h>
#include <sys/shm.h>

#include <cxxopts.hpp>

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>
#include <matlab/common/src/ocn_error.hpp>

extern "C"
{
#include <matlab/common/src/c_spline.h>
#include <af.h>
#include <sys/time.h>
}

/* If we want to use the kinematics for the micro5 until matlab will be fully able to 
 * switch of kinematics. Or we can call the lib kin but we need to select first the rigth kinematics. */
#include <kin_xyzbc_tt_inverse.h>
#include <setMachineAxisInConfig.h>
#include <ConfigSetSource.h>


#include "feedopt.hpp"

#include "rs274ngc/src/rs274ngc_interp.hh"

#include <cblas.h>
#include <af.hpp>

using namespace std::chrono_literals;
/**
 * @brief Low Pass filter used to change the override of the feedrate
 *
 */
class LowPass
{
public:
	explicit LowPass(double time_constant, double initial_value) noexcept {
		reset( time_constant, initial_value );
	 }

	void reset(double time_constant, double value)
	{
		m_time_constant = time_constant;
		m_value = value;
		a = exp( -value / time_constant );
	}

	void update( double value )
	{
		m_value = a * m_value + (1 - a) * value;
	}

	double get_m_value() const
	{
		return m_value;
	}

private:
	double m_time_constant;
	double m_value;
	double a;
};

static int exitval = 1;	   /* program return code - 1 means error */
static int ignore_sig = 0; /* used to flag critical regions */

static int devfd = 0;

static timeval tv_tic, tv_toc;

static void tic()
{
	gettimeofday(&tv_tic, nullptr);
}

static double toc()
{
	gettimeofday(&tv_toc, nullptr);
	return static_cast<double>(tv_toc.tv_sec - tv_tic.tv_sec) + static_cast<double>(tv_toc.tv_usec - tv_tic.tv_usec) / 1e6;
}

/* signal handler */
static sig_atomic_t stop;
static void quit(int /* sig*/)
{
	if (ignore_sig)
	{
		return;
	}
	stop = 1;
}

enum FEEDOPT_STATE
{
	INACTIVE,
	ACTIVE
};

namespace
{
	// PINs
	HalPinBool pin_active{"feedopt.gen.active"};
	HalPinBool pin_reset{"feedopt.reset"};
	HalPinBool pin_gen_start{"feedopt.gen.start"};
	HalPinU32 pin_queue_size{"feedopt.queue-size"};
	HalPinBool pin_read_active{"feedopt.read.active"};
	HalPinU32 pin_sampling_period_ns{"feedopt.sampling-period-ns"};

	HalPinI32 pin_optimising_progress{"feedopt.optimising.progress"};
	HalPinI32 pin_optimising_count{"feedopt.optimising.count"};
	HalPinI32 pin_resampling_progress{"feedopt.resampling.progress"};
	HalPinBool pin_resampling_pause{"feedopt.resampling.pause"};

	HalPinDouble pin_manual_override{"feedopt.resampling.manual-override"};

	// PARAMs
	std::vector<HalParamDouble>param_vmax;
	std::vector<HalParamDouble>param_amax;
	std::vector<HalParamDouble>param_jmax;

	HalParamU32 param_nhorz{"feedopt.nhorz"};
	HalParamU32 param_ndiscr{"feedopt.ndiscr"};
	HalParamU32 param_nbreak{"feedopt.nbreak"};
	HalParamDouble param_lsplit{"feedopt.lsplit"};
	HalParamDouble param_cut_off{"feedopt.cut_off"};
	HalParamDouble pin_auto_override{"feedopt.resampling.auto-override" };
	HalParamBool param_split_special_spline{"feedopt.split-special-spline"};

	HalParamBool param_debug_print{"feedopt.debug-print"};

	struct OptStruct {
		bool end_flag = false;
		ocn::CurvStruct curv;
	};

	struct Opt_t {
		real_T qk = 0;
		real_T qd_k = 0;
		real_T qdd_k = 0;
	} opt;

	std::vector<ocn::CurvStruct> opt_splines;
	std::vector<OptStruct> opt_curv_structs;

	ocn::BaseSplineStruct Bl;

	std::mutex opt_mutex;
	std::condition_variable opt_cv;

	std::mutex matlab_mutex;
	std::thread resample_thread;
	std::atomic_bool stop_resample{true};

	ocn::FeedoptContext ctx;

	ocn::FeedoptConfig config_mem;

	std::string axes;

	FEEDOPT_STATE state = FEEDOPT_STATE::INACTIVE;

	LowPass resample_dt{1e-2, 0.0};

	// Parameters used during Resampling
	::coder::array<double, 1U> gaussLegendreX, gaussLegendreW;

	std::string supported_axes 	= "xyzabc";
	std::string rotary_axes 	= "ABC";
} // namespace

void feedopt_resample_worker(double resample_period_ns);

/**
 * @brief Initialized coder array : Allocate memory + copy elements of the array
 *
 * @param out 	: Output array
 * @param in 	: Input array
 * @param size 	: Size of the input array use to allocate memory of out array
 */
inline void init1DCoderArray(::coder::array<double, 1U> &out, double *const in, int size)
{
	out.set_size(size);
	// Copy array memory
	for (auto i{0}; i < size; ++i)
	{
		out[i] = in[i];
	}
}

void reset_buttons()
{
	pin_reset = false;
	pin_gen_start = false;
}

void set_active()
{
	state = FEEDOPT_STATE::ACTIVE;
#if OPENCN_FEEDOPT_DEBUG
	printf("FEEDOPT: state = ACTIVE\n");
#endif
}

void set_inactive()
{
	state = FEEDOPT_STATE::INACTIVE;
#if OPENCN_FEEDOPT_DEBUG
	printf("FEEDOPT: state = INACTIVE\n");
#endif
}

void send_reset()
{
#if OPENCN_FEEDOPT_DEBUG
	printf("FEEDOPT: sending FEEDOPT_IOCTL_RESET\n");
#endif
	ioctl(devfd, FEEDOPT_IOCTL_RESET, 0);
#if OPENCN_FEEDOPT_DEBUG
	printf("FEEDOPT: completed FEEDOPT_IOCTL_RESET\n");
	opt_splines.clear();
	opt_curv_structs.clear();
#endif
}

void feedopt_state_inactive()
{
	pin_active = false;

	if (pin_reset)
	{
#if OPENCN_FEEDOPT_DEBUG
		printf("FEEDOPT(inactive): Received reset\n");
#endif
		stop_resample = true;

		// wait for the resampling thread to finish its  business before continuing
		if (resample_thread.joinable())
		{
			resample_thread.join();
		}
		send_reset();
		reset_buttons();
	}
	else if (pin_gen_start)
	{
		reset_buttons();

		// Set the parameter values
		config_mem.NHorz  = param_nhorz;
		config_mem.NDiscr = param_ndiscr;
		config_mem.NBreak = param_nbreak;
		config_mem.LSplit = param_lsplit;
		config_mem.CutOff = param_cut_off;
		config_mem.SplitSpecialSpline = param_split_special_spline;
		config_mem.ENABLE_PRINT_MSG = param_debug_print;
		
// Axes mask update, which is used to indicate which axes are enabled, is not
// working in current matlab code.
#if 0
		for (unsigned i = 0, j = 0; i < FEEDOPT_MAX_AXIS; i++) {
			if (config_mem.maskTot.data[i]) {
				config_mem.vmax[i] = param_vmax[j];
				config_mem.amax[i] = param_amax[j];
				config_mem.jmax[i] = param_jmax[j++];
			}
		}
#else
		if (axes.size() == 3) {
			for (unsigned i = 0; i < axes.size(); i++) {
				if (config_mem.maskTot.data[i]) {
					config_mem.vmax[i] = param_vmax[i];
					config_mem.amax[i] = param_amax[i];
					config_mem.jmax[i] = param_jmax[i];
				}
			}

		} else {
			for (unsigned i = 0, j = 0; i < FEEDOPT_MAX_AXIS; i++) {
				// Check what are the active axis
				if (config_mem.maskTot.data[i]) {
					config_mem.vmax[i] = param_vmax[j];
					config_mem.amax[i] = param_amax[j];
					config_mem.jmax[i] = param_jmax[j++];
				}
			}
		}
#endif

		// Initialize variable for resampling
		config_mem.dt = pin_sampling_period_ns * 1e-9;
		ocn::initFeedoptPlan(config_mem, &ctx);
		ocn::bspline_copy(&ctx.Bl, &Bl);

		init1DCoderArray(gaussLegendreX, config_mem.GaussLegendreX, sizeof(config_mem.GaussLegendreX) / sizeof(config_mem.GaussLegendreX[0]));
		init1DCoderArray(gaussLegendreW, config_mem.GaussLegendreW, sizeof(config_mem.GaussLegendreW) / sizeof(config_mem.GaussLegendreW[0]));

		opt_curv_structs.clear();
		opt_splines.clear();

		stop_resample = false;
		resample_thread = std::thread(feedopt_resample_worker, config_mem.dt );
		pthread_setname_np(resample_thread.native_handle(), "resample_thread");

		set_active();
	}
}

void feedopt_state_active()
{
	pin_active = true;
#if OPENCN_FEEDOPT_DEBUG
	static auto previous_op = ocn::Fopt_Init;
#endif

	if (pin_reset)
	{
		reset_buttons();
		set_inactive();
		stop_resample = true;

		// wait for the resampling thread to finish its  business before continuing
		if (resample_thread.joinable())
		{
			resample_thread.join();
		}

		send_reset();
		return;
	}

	if (ctx.op != ocn::Fopt_Finished)
	{
		ocn::CurvStruct opt_struct;
		ocn::CurvStruct opt_curve;
		ocn::CurvStruct opt_spline;
		bool optimized = false;

		try
		{
			matlab_mutex.lock();
			ocn::FeedoptPlan(&ctx, &optimized, &opt_struct); /* matlab entry point */
			matlab_mutex.unlock();
		} catch ( ocn::Error &e ){
			matlab_mutex.lock();
			(void) std::strncpy( ctx.errmsg.msg, e.what(), sizeof( ctx.errmsg.msg ) );
			ctx.errmsg.size	= strlen( ctx.errmsg.msg );
			ctx.errcode		= (ocn::FeedoptPlanError)ctx.op;
			printf( "[Errorcode : %d] %s\n",ctx.errcode, ctx.errmsg.msg );
			matlab_mutex.unlock();
		}

		// Push optimized curve in resampling queue --> it will handled by resampling thread
		if (optimized)
		{
			{
				std::lock_guard<std::mutex> lock(opt_mutex);
				ctx.q_opt.get(ctx.q_opt.size(), &opt_curve);

				opt_curv_structs.push_back({false, opt_curve});

				ctx.q_spline.get(opt_curve.sp_index, &opt_spline);
				opt_splines.push_back(opt_spline);

				pin_optimising_count = ctx.q_split.size();
				pin_optimising_progress = opt_curv_structs.size();
			}
			opt_cv.notify_one();
		}

		if (ctx.op == ocn::Fopt_Finished)
		{
			// If the optimisation is finished, push a last struct acting as an end flag
			{
				std::lock_guard<std::mutex> lock(opt_mutex);
				opt_curv_structs.push_back({true, {}});
				opt_splines.push_back({});
			}
			opt_cv.notify_one();
			set_inactive();
		}
	}
#if OPENCN_FEEDOPT_DEBUG
	if (previous_op != ctx.op) {
		switch (previous_op)
		{
		case ocn::Fopt_GCode:
			printf("FEEDOPT: Read %d structs from %s\n", ctx.q_gcode.size(), ctx.cfg.source);
			break;
		case ocn::Fopt_Compress:
			printf("FEEDOPT: Compress: %d\n", ctx.q_compress.size());
			break;
		case ocn::Fopt_Smooth:
			printf("FEEDOPT: Smooth: %d\n", ctx.q_smooth.size());
			break;
		case ocn::Fopt_Split:
			printf("FEEDOPT: Split: %d\n", ctx.q_split.size());
			break;
		case ocn::Fopt_Opt:
			printf("FEEDOPT: Opt: %d\n", ctx.q_opt.size());
			break;
		case ocn::Fopt_Finished:
			printf("FEEDOPT: finished: %d\n", ctx.q_opt.size());
			break;
		default:
			break;
		}

		previous_op = ctx.op;

		if (ctx.op == ocn::Fopt_Finished)
		{
			printf("FEEDOPT: Optimization finished\n");
		}
	}
#endif
}

/**
 * @brief Tries sending the sample to the kernel module until it succeeds or resample_stop = true
 * @param sample
 */
static void send_sample(feedopt_sample_t sample)
{
	while (write(devfd, &sample, sizeof(sample)) == PushStatus_TryAgain && !stop_resample)
	{
		sched_yield();
	}
}

static void adapt_auto_override()
{
	if (pin_read_active && pin_resampling_progress > pin_optimising_progress - 50 && pin_optimising_progress < pin_optimising_count)
	{
		pin_auto_override = 0.01;
	}
	else
	{
		pin_auto_override = 1.0;
	}

	if (pin_resampling_pause)
	{
		pin_auto_override = 0.0;
	}
}

void getPositionFromU(const ocn::FeedoptConfig &cfg, const ocn::CurvStruct *curv, const ocn::CurvStruct *spline,
					  const double u, double *pos)
{
	// Instantiate the input variable by using coder::array template
	static ::coder::array<double, 1U> r0D;
	static ::coder::array<double, 1U> kin_params;
	static bool initialization = true;

#if OPENCN_FEEDOPT_DEBUG
	//printf("Number of kin_params = %d", cfg.kin_params.size[0]);
	for(int i = 0; i < cfg.kin_params.size[0]; i++) {
		//printf("Param %d = %f\n", i, cfg.kin_params.data[i]);
	}
#endif

	if (initialization) {
		// Allocate initial memory for the array
		r0D.set_size(axes.size());
		kin_params.set_size( cfg.kin_params.size[0] );

		// Copy array memory
		for( auto i = 0; i < cfg.kin_params.size[0]; i++ ) {
			kin_params[i] = cfg.kin_params.data[i];
		}

		initialization = false;
	}

	ocn::evalPositionFromU(cfg, curv, spline, u, r0D);
	if( curv->Info.TRAFO ) { // Result is stored in pos vector
		// Warning - temp solution
		// Call to current loaded kinematic is expected to be implemented soon.
		// see issue 34 in opencn-matlab repository
		// link: https://gitlab.com/mecatronyx/opencnc/opencn-matlab/-/issues/34
		kin_params[5] 	= -curv->tool.offset.z;

		ocn::kin_xyzbc_tt_inverse( r0D, kin_params, pos );
	}

	// Copy array memory
	for (unsigned i = 0; i < axes.size(); i++) {
		if ( ! curv->Info.TRAFO ) // Result is stored in pos vector
			pos[i] = r0D[i];

		// Rotary axes - convert radian to degree
		//if (rotary_axes.find(toupper(axes[i])) != std::string::npos) {
		if (rotary_axes.find(axes[i]) != std::string::npos) {
			double deg  = (pos[i] * 180.0) / M_PI;
			pos[i] 		= deg;

			//printf("FEEDOPT: axis%c, convet to degree, rad=%5.3f deg=%5.3f (pos=%5.3f))\n", axes[i], rad, deg, pos[i]);

		} else {
			//printf("FEEDOPT: axis%c NOT ROTARY\n", axes[i]);
		}
	}
}

/* Resampler thread */
void feedopt_resample_worker(const double dt)
{
	const double dt_time_constant = dt * 10;
	resample_dt.reset( dt_time_constant, dt );

	size_t k = 0;
	OptStruct Curv;
	ocn::CurvStruct spline;

	ocn::ResampleStateClass resampleState{};

	matlab_mutex.lock();
	ocn::ResampleState(dt, config_mem.DefaultZeroStopCount, &resampleState);
	matlab_mutex.unlock();

	while (!stop_resample)
	{
		std::unique_lock<std::mutex> lk(opt_mutex);
		if (opt_cv.wait_for(lk, 1ms, [k]()
							{ return k < opt_curv_structs.size() && !stop_resample; }))
		{
			// wait_for returns true if the predicate has been satisfied
			Curv 	= opt_curv_structs.at(k);
			spline 	= opt_splines.at(k);
			// As soon as we get the two structs, we can unlock the mutex that the
			// optimization worker uses for protecting the two queues
			lk.unlock();

			// Check if the new struct is the end flag, if so, stop resampling and
			// send a last sample acting as the end flag
			if (Curv.end_flag) {
				feedopt_sample_t sample;
				sample.end_flag = 1;
				send_sample(sample);
				stop_resample = true;
				continue;
			}

			try {
				while (!resampleState.go_next && !stop_resample)
				{
					adapt_auto_override();
					resample_dt.update(pin_auto_override * pin_manual_override * dt);
						
					feedopt_sample_t sample;

					matlab_mutex.lock();
					ocn::resampleCurvOCN(&resampleState, &Bl, Curv.curv.Info.zspdmode, Curv.curv.Coeff, 
						Curv.curv.ConstJerk, (real_t) resample_dt.get_m_value(), gaussLegendreX, gaussLegendreW, config_mem.ENABLE_PRINT_MSG);
					matlab_mutex.unlock();

					sample.end_flag = 0;
					sample.index = k;
					sample.gcode_line = Curv.curv.Info.gcode_source_line;
					sample.spindle_speed = Curv.curv.Info.SpindleSpeed;

					if (!resampleState.isOutsideRange)
					{
						//matlab_mutex.lock();
							getPositionFromU( config_mem, &(Curv.curv), &spline, resampleState.u, sample.axis_position);
						//matlab_mutex.unlock();

						send_sample(sample);
					}
				}
			} catch ( ocn::Error &e ) {
				printf( "[ Error Feedopt ] : %s\n",e.what() );
			}
		
			k++;

			/*Reset Resamplestate but preserve resampleState.dt*/
			matlab_mutex.lock();
			ocn::ResampleState(resampleState.dt, config_mem.DefaultZeroStopCount, &resampleState);
			matlab_mutex.unlock();
		}
	}
#if OPENCN_FEEDOPT_DEBUG
	printf("FEEDOPT: Resampling stopped\n");
#endif
}

void feedopt_opt_worker()
{
	tic();
	while (!stop)
	{
		switch (state)
		{
		case FEEDOPT_STATE::INACTIVE:
			feedopt_state_inactive();
			usleep(100);
			break;
		case FEEDOPT_STATE::ACTIVE:
			feedopt_state_active();
			sched_yield();
			break;
		}
		double t = toc();
		if (t > 1.0)
		{
			// TODO: Bring back the OPT/sec metric
			tic();
		}
	}
}


/***********************************************************************
 *                            MAIN PROGRAM                              *
 ************************************************************************/

/*
 * Connect to the kernel HAL.
 * @mod_name: name of the component (unique)
 * @returns a comp_id (component ID).
 */
int feedopt_connect(const char *axes)
{
	feedopt_connect_args_t args = { 0 };
	int ret;

	devfd = open(FEEDOPT_DEV_NAME, O_RDWR);

	strcpy(args.axes, axes);

	ret = ioctl(devfd, FEEDOPT_IOCTL_CONNECT, &args);
	if (ret != 0) {
		printf("[%s] ioctl return an error (%d)\n", FEEDOPT_COMP_NAME, ret);
		BUG();
	}

	return ret;
}

void feedopt_disconnect(void)
{
	ioctl(devfd, FEEDOPT_IOCTL_DISCONNECT);
	close(devfd);
}

static int check_selected_axes(std::string &axes)
{
    /* Check that all 'axis' are valid */
    std::size_t found = axes.find_first_not_of(supported_axes);

    if (found != std::string::npos) {
        printf("[%s] Error - '%c' is not a invalid axis\n", FEEDOPT_COMP_NAME, axes[found]);
        return -1;
    }

    /* Check that each 'axis' is unique */
    std::string tmp = axes;
    std::sort(tmp.begin(), tmp.end());
    for (unsigned i = 0; i < (tmp.length() - 1); i++) {
        // if at any time, 2 adjacent elements become equal, return false
        if (tmp[i] == tmp[i + 1]) {
            printf("[%s] Error - '%c' is present multiple times\n", FEEDOPT_COMP_NAME, tmp[i]);
            return -1;
        }
    }

    return 0;
}

static void feedopt_set_affinity()
{
	cpu_set_t cpuset;

	CPU_ZERO(&cpuset);
	CPU_SET(2, &cpuset);
	CPU_SET(3, &cpuset);

	sched_setaffinity(getpid(), sizeof(cpuset), &cpuset);
}

static std::string feedopt_parse_args(int argc, char **argv)
{
	std::string axes;

	cxxopts::Options options(FEEDOPT_COMP_NAME, "Feedopt component");

	options.add_options()
		("a,axes", "Axes", cxxopts::value<std::string>())
	;

	options.allow_unrecognised_options();

	auto result    = options.parse(argc, argv);
	auto unmatched = result.unmatched();

	if (unmatched.size() != 0) {
		printf("[%s] Error - The following arguments are not valid: ", FEEDOPT_COMP_NAME);
		for ( auto & element : unmatched)
			printf("'%s' ", element.c_str());
		printf("\n");
	}

	if (result.count("axes") == 1) {
		axes = result["axes"].as<std::string>();
	} else {
		printf("[%s] Error - 'axes' parameter is missing or present multiple times\n", FEEDOPT_COMP_NAME);
	}

	return axes;
}

/*
 * Syntax: feedopt --axes=<AXES>
 */
int main(int argc, char **argv)
{
	int ret = 0;
	ocn::FeedoptConfig cfg{};
	int size_source[2] = { 0 };

#if 0 // Axes mask update is not working in matlab code
	bool mask[6] = { 0 };
#endif

	// Set the CPU affinity
	feedopt_set_affinity();

	signal(SIGTERM, quit);

	signal(SIGPIPE, SIG_IGN);

	// Get the axes from the parameters
	axes = feedopt_parse_args(argc, argv);
	if (axes.empty())
		goto out;

	ret = check_selected_axes(axes);
	if (ret < 0)
		goto out;

	for (auto & c: axes) c = toupper(c);

	/* connect to the HAL */
	ignore_sig = 1;
	ret = feedopt_connect(axes.c_str());
	ignore_sig = 0;

	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);

	/* check result */
	if (ret < 0) {
		fprintf(stderr, "ERROR: feedopt_connect failed with ret = %d\n", ret);
		exitval = 0;

		ignore_sig = 1;

		feedopt_disconnect();

		return exitval;
	}

	// Create axis PINs & Params variables
	for (unsigned i = 0; i < axes.size(); i++) {
		char vmax_name[HAL_NAME_LEN];
		char amax_name[HAL_NAME_LEN];
		char jmax_name[HAL_NAME_LEN];

		sprintf(vmax_name, "feedopt.axis%c.vmax", axes[i]);
		sprintf(amax_name, "feedopt.axis%c.amax", axes[i]);
		sprintf(jmax_name, "feedopt.axis%c.jmax", axes[i]);

		HalParamDouble vmax{vmax_name};
		HalParamDouble amax{amax_name};
		HalParamDouble jmax{jmax_name};

		param_vmax.push_back(vmax);
		param_amax.push_back(amax);
		param_jmax.push_back(jmax);
	}

	// Set the external PARAMs values with the default ones
	ocn::FeedoptDefaultConfig(&cfg);

#if 0 /*To disable some part of matlab algorithms*/
	cfg.Cusp.Skip			= true;
	cfg.Compressing.Skip 	= true;
	cfg.Smoothing.Skip 		= true;
#endif


// Axes mask update, which is used to indicate which axes are enabled, is not
// working in current matlab code.
#if 0
	for (unsigned i = 0; i < FEEDOPT_MAX_AXIS; i++) {
		if (axes.find(toupper(supported_axes[i])) != std::string::npos) {
			mask[i] = true;
		}
	}
	setMachineAxisInConfig(&cfg, mask);
#endif

	param_nhorz   = cfg.NHorz;
	param_ndiscr  = cfg.NDiscr;
	param_nbreak  = cfg.NBreak;
	param_lsplit  = cfg.LSplit;
	param_cut_off = cfg.CutOff;
	param_split_special_spline =  cfg.SplitSpecialSpline;
	param_debug_print = cfg.ENABLE_PRINT_MSG;

// Axis Mask not working - currently it is working with 'xyz' or 'xyzbc' modes
#if 0
	for (unsigned i = 0, j = 0; i < FEEDOPT_MAX_AXIS; i++) {
		// Check what are the active axis
		if (cfg.maskTot.data[i]) {
			param_vmax[j]   = cfg.vmax[i];
			param_amax[j]   = cfg.amax[i];
			param_jmax[j++] = cfg.jmax[i];
		}
	}
#else
	if (axes.size() == 3) {
		for (unsigned i = 0; i < axes.size(); i++) {
			param_vmax[i] = cfg.vmax[i];
			param_amax[i] = cfg.amax[i];
			param_jmax[i] = cfg.jmax[i];
		}
	} else {
		for (unsigned i = 0, j = 0; i < FEEDOPT_MAX_AXIS; i++) {
			// Check what are the active axis
			if (cfg.maskTot.data[i]) {
				param_vmax[j]   = cfg.vmax[i];
				param_amax[j]   = cfg.amax[i];
				param_jmax[j++] = cfg.jmax[i];
			}
		}
	}
#endif

	// Store the config
	config_mem = cfg;
	
	// Copy GCode file
	size_source[1] = sizeof( GCODE_FILE_PATH );
	ocn::ConfigSetSource( &config_mem, GCODE_FILE_PATH, size_source);

	reset_buttons();

	feedopt_opt_worker();

	/* run was successful */
	exitval = 0;
	ignore_sig = 1;

	feedopt_disconnect();

	return exitval;

out:
	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);

	return -1;
}
