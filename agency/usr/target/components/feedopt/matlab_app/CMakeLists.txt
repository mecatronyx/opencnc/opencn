# Use this CMakeLists to generate a stand alone application.
cmake_minimum_required(VERSION 2.8.11)

project(feedopt_app LANGUAGES C CXX)

#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/libs)
set( CURRENT_DIR ${CMAKE_CURRENT_SOURCE_DIR} )
add_definitions(-DROOT_WORKING_DIR=${CURRENT_DIR}/)
# Environnement variables
SET( OPENCN_ROOT_PATH  "../../../../../.." )

#SET( MAIN_NAME feedopt.cpp )
SET( MAIN_NAME feedopt.cpp )

SET( CURRENT_DIR ${OPENCN_ROOT_PATH}/agency/usr )
SET( opencn-usr_SOURCE_DIR ${OPENCN_ROOT_PATH}/agency/usr )
SET( OPENCN_PLATFORM "x86" )

set(OPENCN_MATLAB_PATH ${CURRENT_DIR}/matlab)
set(OPENCN_RS274_PATH ${OPENCN_ROOT_PATH}/agency/usr/rs274ngc/src )
set(OPENCN_MATLAB_GEN ${OPENCN_MATLAB_PATH}/${OPENCN_PLATFORM}/matlab/generated/Feedopt )

file(GLOB MATLAB_GEN_SRC ${OPENCN_MATLAB_GEN}/*.cpp)
file(GLOB RS274_SRC ${OPENCN_RS274_PATH}/*.cc)


message(MATLAB_GEN_SRC: ${OPENCN_RS274_PATH})


add_executable(${PROJECT_NAME}  ${MAIN_NAME}
${MATLAB_GEN_SRC}
${OPENCN_MATLAB_PATH}/common/src/ocn_error.cpp
${OPENCN_MATLAB_PATH}/common/src/c_spline.c
${OPENCN_MATLAB_PATH}/common/src/cpp_interp.cpp
${OPENCN_MATLAB_PATH}/common/src/functions.cpp
${OPENCN_MATLAB_PATH}/common/src/cpp_simplex.cpp
${OPENCN_MATLAB_PATH}/common/src/ocn_print.cpp
${RS274_SRC}
)

set(BLA_VENDOR Generic )


find_package(GSL REQUIRED)
find_package(BLAS REQUIRED)

set(CMAKE_CXX_FLAGS "-O0 -g")

target_link_libraries(${PROJECT_NAME}  ${BLAS_LIBRARIES} GSL::gsl ClpSolver Clp CoinUtils dl rt pthread)

set( FLAG_MEMORY_GUARD "-fsanitize=address -static-libasan" )

set_target_properties(${PROJECT_NAME}  PROPERTIES COMPILE_FLAGS "${_components_cflags} -DUSE_FULL_MATLAB_GEN -D_POSIX_C_SOURCE=199309L -DTRACY_NO_SYSTEM_TRACING")
set_target_properties(${PROJECT_NAME}  PROPERTIES LINK_FLAGS "${_components_ldflags}")
# -fsanitize=address -static-libasan"
target_include_directories(${PROJECT_NAME}  PRIVATE 
        ${OPENCN_ROOT_PATH}/agency/usr/common/include/
        ${opencn-usr_SOURCE_DIR}
#        ${opencn-usr_SOURCE_DIR}/components/feedopt
        ${OPENCN_RS274_PATH}
        ${OPENCN_MATLAB_PATH}/common/src
        ${OPENCN_MATLAB_PATH}/${OPENCN_PLATFORM}/matlab/generated/Feedopt
        ${OPENCN_MATLAB_PATH}/common
)

#set_property(SOURCE ${MATLAB_GEN_SRC} PROPERTY COMPILE_FLAGS -Wno-maybe-uninitialized)
