
#include <cstdio>
#include <cstdlib>

#include "ocn_error.hpp"
#include "common/include/feedopt.hpp"
#include "ConfigSetSource.h"

#include <iostream>

// clang-format off
#define STR(X) #X
/** @brief Stringify the variable contents*/
#define XSTR(X) STR(X)
// clang-format on

/* FUNCTIONS */

/**
 * @brief Do the evaluation of the position vector of the curve at a given u ( parametrisation of the curve )
 *
 * @param cfg
 * @param curv
 * @param spline
 * @param u
 * @param pos
 * @param size
 */
void getPositionFromU(ocn::FeedoptConfig *const cfg, ocn::CurvStruct *const curv, ocn::CurvStruct *const spline,
					  const double u, double pos[], const int size)
{
	// Instantiate the input variable by using coder::array template
	static ::coder::array<double, 1U> r0D;
	static bool initialization = true;

	if (initialization)
	{
		// Allocate initial memory for the array
		r0D.set_size(size);
		initialization = false;
	}

	ocn::evalPositionFromU(*cfg, curv, spline, u, r0D);

	// Copy array memory
	for (auto i{0}; i < size; ++i)
	{
		pos[i] = r0D[i];
	}
}

inline void initCoderArray(::coder::array<double, 1U> &out, double *const in, int size)
{
	out.set_size(size);
	// Copy array memory
	for (auto i{0}; i < size; ++i)
	{
		out[i] = in[i];
	}
}

/**
 * @brief Print current operation state in feedrate planning computation chain.
 *
 * @param operation
 */
inline void print_operation_mode(ocn::Fopt const operation)
{

	switch (operation)
	{
	case ocn::Fopt_GCode:
		printf("operation : Fopt_GCode start\n");
		break;

	case ocn::Fopt_Check:
		printf("operation : Fopt_Check start\n");
		break;

	case ocn::Fopt_Compress:
		printf("operation : Fopt_Compress start\n");
		break;

	case ocn::Fopt_Smooth:
		printf("operation : Fopt_Smooth start\n");
		break;

	case ocn::Fopt_Split:
		printf("operation : Fopt_Split start\n");
		break;

	case ocn::Fopt_Opt:
		printf("operation : Fopt_Opt start\n");
		break;
	}
}

/**
 * @brief Print queue size of the current operation
 *
 * @param operation
 */
inline void print_queue_length_of_current_op(ocn::FeedoptContext const& ctx, ocn::Fopt const operation)
{

	switch (operation)
	{
	case ocn::Fopt_GCode:
		break;

	case ocn::Fopt_Check:
		printf("Queue after Gcode : %d\n", ctx.q_gcode.size());
		break;

	case ocn::Fopt_Compress:
		printf("Queue after Check : %d\n", ctx.q_gcode.size());
		break;

	case ocn::Fopt_Smooth:
		printf("Queue after Compressing : %d\n", ctx.q_compress.size());
		break;

	case ocn::Fopt_Split:
		printf("Queue after Smoothing : %d\n", ctx.q_smooth.size());
		break;

	case ocn::Fopt_Opt:
		printf("Queue after Splitting : %d\n", ctx.q_split.size());
		break;
	}
}
/* MAIN PROGRAM */

int main(int, char **)
{
	ocn::FeedoptConfig cfg;
	ocn::FeedoptContext ctx;

	// Change here the GCode file
	char source_file[1024] = XSTR( ROOT_WORKING_DIR )"test.ngc";
	char csv_file[1024] = XSTR( ROOT_WORKING_DIR )"data.csv";

	// initialization of the configuration and context structures
	ocn::FeedoptDefaultConfig(&cfg);
	cfg.dt = 1e-3;
	cfg.amax[0] = 20000;
	cfg.amax[1] = 20000;
	cfg.amax[2] = 20000;
	//cfg.Compressing.Skip = false;
	//cfg.Cusp.Skip = false;

	ocn::initFeedoptPlan(cfg, &ctx);
	
	// Finite State Machine ( FSM )
	bool optimized = false;
	ocn::CurvStruct opt_struct;
	//std::memcpy(ctx.cfg.source, source_file, sizeof(ctx.cfg.source));

	int size_source[2] = { 0 };
	size_source[1] = sizeof( source_file );
	ocn::ConfigSetSource( &ctx.cfg, source_file, size_source);
	printf("FEEDOPT: Read %d structs from %s\n", ctx.q_gcode.size(), source_file);

	int i = 0;
	auto operation = ctx.op;

	print_operation_mode(operation);
	int NCurves = 0;

	try
	{
		while (ctx.op != ocn::Fopt_Finished) // Do the computation until the last operation
		{
			// For debugging
			if (operation != ctx.op)
			{
				operation = ctx.op;

				print_operation_mode(operation);
				print_queue_length_of_current_op(ctx, operation);
				NCurves = ctx.q_split.size();
			}
			// if( operation == ocn::Fopt_Check ){ break; }
			// Run the state machine
			ocn::FeedoptPlan(&ctx, &optimized, &opt_struct);

			// The status of the optimization
			if (optimized)
			{
				printf("[%d / %d]\n", ctx.k0, NCurves);
			}
		}
	} catch ( ocn::Error &e ){
		(void) std::strncpy( ctx.errmsg.msg, e.what(), sizeof( ctx.errmsg.msg ) );
		ctx.errmsg.size	= strlen( ctx.errmsg.msg );
		ctx.errcode		= (ocn::FeedoptPlanError)ctx.op;
		std::cout << "[Errorcode : " << ctx.errcode << "]" <<  e.what() << std::endl;
	}
	// Resampling
	const int size = 5;
	auto pos = new double[size];
	long int count{0};

	FILE *fid = fopen(csv_file, "w+");
	::coder::array<double, 1U> gaussLegendreX, gaussLegendreW;
	initCoderArray(gaussLegendreX, ctx.cfg.GaussLegendreX, sizeof(ctx.cfg.GaussLegendreX) / sizeof(ctx.cfg.GaussLegendreX[0]));
	initCoderArray(gaussLegendreW, ctx.cfg.GaussLegendreW, sizeof(ctx.cfg.GaussLegendreW) / sizeof(ctx.cfg.GaussLegendreW[0]));
	ocn::ResampleStateClass resampleState{};
	ocn::ResampleState(ctx.cfg.dt, ctx.cfg.DefaultZeroStopCount, &resampleState);

	for (auto i{1}; i <= NCurves; ++i)
	{
		ocn::CurvStruct curve;
		ctx.q_opt.get(i, &curve); // queue indexes are between 1 and N due to Matlab

		printf("[%d / %d]\n", i, NCurves);

		while (!resampleState.go_next)
		{
			ocn::resampleCurvOCN(&resampleState, &(ctx.Bl), curve.Info.zspdmode, curve.Coeff, curve.ConstJerk, ctx.cfg.dt, gaussLegendreX, gaussLegendreW, true);

			if (!resampleState.isOutsideRange)
			{
				ocn::CurvStruct spline;
				ctx.q_spline.get(curve.sp_index, &spline);
				getPositionFromU(&(ctx.cfg), &curve, &spline,
								 resampleState.u, pos, size);

				// printf("%ld %f %f %f %f %f\n", ++count, pos[0], pos[1], pos[2], pos[3], pos[4]); // csv
				fprintf(fid, "%f, %f, %f,%f, %f, %f, %f\n", pos[0], pos[1], pos[2], pos[3], pos[4], resampleState.u, resampleState.dt); // streamer
			}
		}
		ocn::ResampleState( resampleState.dt, ctx.cfg.DefaultZeroStopCount, &resampleState );

		printf("%ld %f %f %f %f %f\n", ++count, pos[0], pos[1], pos[2], pos[3], pos[4]);
	}

end_file:
	fclose(fid);

	delete[] pos;

	printf("success !\n");

	return EXIT_SUCCESS;
}
