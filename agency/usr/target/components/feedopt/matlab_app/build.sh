#!/bin/bash

help()
{
    echo "Allows to build matlab feedopt application"
    echo
    echo "Syntax: [-c|h|u|t]"
    echo "Options: "
    echo "c     Clean output dir"
    echo "d     Debug build"
    echo "h     Prints this help"
    echo "u     Build the userspace applications"
    echo "t     Configure cmake"
}

if [ $# -eq 0 ];
then
    echo "Error: missing argument" 
    help
    exit 0
else

while getopts ":hdcuit" option; do
    case $option in
        h)
            help
            exit;;

        d)
            cmake -DDEBUG_BUILD=ON -S . -B build
            cmake --build build/
            cp build/*_app .
            exit;;
        u)
            cmake -DDEBUG_BUILD=OFF -S . -B build
            cmake --build build/
            cp build/*_app .
            exit;;

        c)
            rm -rf build
            exit;;

        t)
            cmake -S . -B build
            exit;;
        \?)
            echo "Error: invalid option"
            help
            exit;;
    esac
done
fi
