/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: ocno.cpp
 *
 *
 * It is the user-space part of the "ocno" OpenCN HAL component. The user-space
 * only checks the load parameters and sends it to the kernel part
 *
 * The user-space part also read the current-joint-pos  PINs and write them to
 * the "sample" channel. This channel can be read from the OpenCN server, for
 * example, to send the current position to the HOST
 *
 ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <algorithm>

#include <opencn/uapi/kinematic.h>

#include <debug.h>

#define OCNO_AXIS_NOT_USED  -1

extern "C" {
    #include <cmdline.h>
}

#include <opencn/uapi/ocno.h>

/* used to flag critical regions */
static int ignore_sig = 0;

/* Sample channel */
static communicationChannel_t *sample_channel = nullptr;

/* signal handler */
static void quit(int sig) {
    if (ignore_sig == 1) {
        return;
    }
    /* Inform the parent (normally halcmd) that we are up and running. */
    kill(getppid(), SIGUSR1);
    exit(0);
}

static int check_selected_axes(std::string &axes)
{
    /* Check that all 'axis' are valid */
    std::size_t found = axes.find_first_not_of("xyzabcuvw");

    if (found != std::string::npos) {
        printf("[%s] Error - '%c' is not a invalid axis\n", OCNO_COMP_NAME, axes[found]);
        return -1;
    }

    /* TODO - check order or do a custom short.... */
    /* Not implemented yes */

    /* Check that each 'axis' is unique */
    std::string tmp = axes;
    std::sort(tmp.begin(), tmp.end());
    for (unsigned i = 0; i < (tmp.length() - 1); i++) {
        // if at any time, 2 adjacent elements become equal, return false
        if (tmp[i] == tmp[i + 1]) {
            printf("[%s] Error - '%c' is present multiple times\n", OCNO_COMP_NAME, tmp[i]);
            return -1;
        }
    }

    return 0;
}

/* Connect to the kernel HAL. */
static int ocno_connect(ocno_connect_args_t *args)
{
    int devfd;
    int ret = 0;

    devfd = open(OCNO_DEV_NAME, O_RDWR);

    ret = ioctl(devfd, OCNO_IOCTL_CONNECT, args);
    if (ret != 0) {
        printf("[%s] ioctl return an error (%d)\n", OCNO_COMP_NAME, ret);
        BUG()
    }

    close(devfd);

    return ret;
}

static void ocno_set_axis_ref_pos(int *axis_pos_idx, std::string axes)
{
    int tmp_pos[KIN_MAX_JOINTS];
    int axis_nr = axes.length();
    int i, j;

    for (i = 0; i < axis_nr ; i++)
        tmp_pos[i] = OCNO_AXIS_NOT_USED;

    for (i = 0; i < axis_nr ; i++) {
        switch (axes[i]) {
            case 'X':
                tmp_pos[0] = i;
                break;
            case 'Y':
                tmp_pos[1] = i;
                break;
            case 'Z':
                tmp_pos[2] = i;
                break;
            case 'A':
                tmp_pos[3] = i;
                break;
            case 'B':
                tmp_pos[4] = i;
                break;
            case 'C':
                tmp_pos[5] = i;
                break;
            default:
                printf("ERROR\n");

        }
    }

    for (i = 0, j = 0; i < KIN_MAX_JOINTS; i++) {
        if (tmp_pos[i] != OCNO_AXIS_NOT_USED)
            axis_pos_idx[j++] = tmp_pos[i];
    }
}

static void read_samples(std::string axes)
{
    sampler_sample_t sample;
    char cur_pos_pin[HAL_NAME_LEN];
    int n_pins = axes.length();

    sample_channel_set_freq(1);
    open_sample_channel(&sample_channel, O_WRONLY);

    int *axis_pos_idx = nullptr;
    int pos;
    double joint[KIN_MAX_JOINTS] = { 0 };
    double piece[KIN_MAX_JOINTS] = { 0 };

    sample.n_pins = n_pins;

    axis_pos_idx = (int *)malloc(sizeof(int) * n_pins);
    if (!axis_pos_idx) {
        printf("[%s] ERROR - malloc failed on axis_pos_idx\n", OCNO_COMP_NAME);
        exit(0);
    }

    ocno_set_axis_ref_pos(axis_pos_idx, axes);

    /* Read "current-joint-pos" PINs and write them in the sample channel */
    while (true) {
        if (sample_channel_enabled()) {
            for (int i = 0; i < n_pins; i++ ) {
                // Read join position
                pos = axis_pos_idx[i];

                std::sprintf(cur_pos_pin, "%s.axis%c.current-joint-pos", OCNO_COMP_NAME, axes[pos]);

                joint[i] = pin_get_value(pin_find_by_name(cur_pos_pin))->f;
            }
            /* Compute machine piece position */
            kin_forward( piece, joint );

            for (int i = 0; i < n_pins; i++ ) {
                sample.pins[i].f    = piece[i];
                sample.pins[i].type = HAL_FLOAT;
            }

            write_sample(sample_channel, &sample);
        }
        /* We want approximately 60 samples/s */
        usleep(1000000/60);
    }
}

/*
 * Syntax: ocno axes=<HAL TYPE>
 */
int main(int argc, char *argv[])
{
    int ret = 0;
    std::string axes;
    std::string arg;

    ocno_connect_args_t args = { 0 };

    std::string option = "axes="; /* TODO - do it better :-) */

     for ( int i = 1; i < argc; ++i) {
        arg = argv[i];

        if (arg.find(option) == 0) {
            axes = arg.substr(option.size());
            continue;
        }
     }

    ret = check_selected_axes(axes);
    if (ret < 0)
        goto out;

    for (auto & c: axes) c = toupper(c);

    strcpy(args.axes, axes.c_str());

    /* register signal handlers - if the process is killed
     we need to call hal_exit() to free the shared memory */
    signal(SIGTERM, quit);

    /* connect to the HAL */
    ignore_sig = 1;
    ret = ocno_connect(&args);
    ignore_sig = 0;

    /* Inform the parent (normally halcmd) that we are up and running. */
    kill(getppid(), SIGUSR1);

    /* check result */
    if (ret < 0) {
        fprintf(stderr, "ERROR: ocno_connect failed with ret = %d\n", ret);
        goto out;
    }

    /* infinite loop to get current axes position */
    read_samples(axes);

out:

    return ret;
}
