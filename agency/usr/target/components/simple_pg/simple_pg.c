/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: simple_pg.c
 *
 *
 * It is the user-space part of the "simple_pg" OpenCN HAL component. The user-space
 * only checks the load parameters and sends it to the kernel part
 *
 ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <debug.h>
#include <cmdline.h>

#include <opencn/uapi/simple_pg.h>

/* used to flag critical regions */
static int ignore_sig = 0;

/* signal handler */
static void quit(int sig) {
	if (ignore_sig == 1) {
		return;
	}

	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);
	exit(0);
}

/* Connect to the kernel HAL. */
static int simple_pg_connect(int joint_nr)
{
	int devfd;
	int ret = 0;

	simple_pg_connect_args_t args = { 0 };

	devfd = open(SIMPLE_PG_DEV_NAME, O_RDWR);

	args.joint_nr = joint_nr;

	ret = ioctl(devfd, SIMPLE_PG_IOCTL_CONNECT, &args);
	if (ret != 0) {
		printf("ioctl return an error (%d)\n", ret);
		BUG()
	}

	return ret;
}

/*
 * Syntax: simple_pg joint_nr=<JOINT NR>
 */
int main(int argc, char **argv)
{
	int i;
	int ret = 0;
	int joint_nr = 0;
	char *params = NULL, *value = NULL;
	char *cp;

	for (i = 1; i < argc; i++) {
		cp = argv[i];

		/* First parse the option with "=" */
		next_arg(argv[i], &params, &value);

		if (params) {
			if (!strcmp(params, "joint_nr")) {
				joint_nr = atoi(value);
				continue ;
			}
		}

		if (*cp != '-') {
			fprintf(stderr, "[%s] ERROR: unknown option '%s'\n", SIMPLE_PG_COMP_NAME, cp);
			goto out;
		}
	}

	/* register signal handlers - if the process is killed
	 we need to call hal_exit() to free the shared memory */
	signal(SIGTERM, quit);

	/* connect to the HAL */
	ignore_sig = 1;
	ret = simple_pg_connect(joint_nr);
	ignore_sig = 0;

	/* check result */
	if (ret < 0) {
		fprintf(stderr, "[%s] ERROR: simple_pg_connect failed with ret = %d\n", SIMPLE_PG_COMP_NAME, ret);
		goto out;
	}

out:
	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);

	return ret;
}

