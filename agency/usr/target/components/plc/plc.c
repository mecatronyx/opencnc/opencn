/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: plc.c
 *
 *
 * It is the user-space part of the "plc" OpenCN HAL component. The user-space
 * only checks the load parameters and sends it to the kernel part
 *
 ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <debug.h>
#include <cmdline.h>

#include <opencn/uapi/plc.h>

static plc_cfg_t supported_cfg[] = {
	{ PLC_CFG_MICRO3, "micro3" },
	{ PLC_CFG_MICRO5, "micro5" },
	{ PLC_CFG_COREXY, "corexy" },
	{ PLC_CFG_DEMONSTRATOR, "demonstrator" },

	{ PLC_CFG_OCNO_DBG, "ocno_dbg" },

	{PLC_CFG_END, "end"},
};

/* used to flag critical regions */
static int ignore_sig = 0;

static void show_help(void)
{
	int i;

	printf("Usage: ./halcmd load %s [OPTIONS]\n", PLC_COMP_NAME);

    printf("Where OPTIONS are:\n");
    printf("  -h    Display this help message\n");
    printf("  cfg=  Select the wanted configuration. See below for the supported targets\n");
    printf("\n");
    printf(" Supported configurations: ");

	for (i = 0; i < PLC_CFG_END; i++)
		printf("'%s' ", supported_cfg[i].cfg_name);
    printf("\n");
}

/* signal handler */
static void quit(int sig) {
	if (ignore_sig == 1) {
		return;
	}
	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);
	exit(0);
}

/* Check if the value of 'cfg' parameter is valid
 *
 *	return -1 if the 'cfg' is not valid or the cfg_id
 */
static int check_selected_config(char *cfg)
{
	int i;

	/* checks if the 'cfg' parameter was used */
	if (cfg == NULL) {
		fprintf(stderr, "ERROR: No configuration passed  (cfg parameter)\n");
		return -1;
	}

	/* checks if 'cfg' is valid */
	for (i = 0; i < PLC_CFG_END; i++) {
		if (!strcmp(cfg, supported_cfg[i].cfg_name))
			return supported_cfg[i].cfg_id;
	}

	/* cfg does not correspond to a valid configuration */
	fprintf(stderr, "ERROR: Configuration '%s' is not valid\n", cfg);
	return -1;

}

/* Connect to the kernel HAL. */
static int plc_connect(char *devname, char *name, int id)
{
	int devfd;
	int ret = 0;
	plc_connect_args_t args = { 0 };

	devfd = open(PLC_DEV_NAME, O_RDWR);

	strcpy(args.name, name);
	args.id = id;

	ret = ioctl(devfd, PLC_IOCTL_CONNECT, &args);
	if (ret != 0) {
		printf("ioctl return an error (%d)\n", ret);
		BUG()
	}

	return ret;
}

/*
 * Syntax: plc cfg=<WANTED CFG>
 */
int main(int argc, char **argv)
{
	int i;
	int ret = 0;
	int cfg_id;
	char *cfg = NULL;
	char *params = NULL, *value = NULL;
	char *cp;

	for (i = 1; i < argc; i++) {
		cp = argv[i];

		/* First parse the option with "=" */
		next_arg(argv[i], &params, &value);

		if (params) {
			if (!strcmp(params, "cfg")) {
				cfg = value;
				continue ;
			}
		}

		if (*cp != '-') {
			fprintf(stderr, "ERROR: unknown option '%s'\n", cp);
			show_help();
			goto out;
		}

		switch (*(++cp)) {
		case 'h':
			show_help();
			goto out;

		default:
			fprintf(stderr, "ERROR: unknown option '%s'\n", cp);
			show_help();
			goto out;
		}
	}

	cfg_id = check_selected_config(cfg);
	if (cfg_id < 0) {
		ret = -1;
		goto out;
	}

	/* register signal handlers - if the process is killed
	 we need to call hal_exit() to free the shared memory */
	signal(SIGTERM, quit);

	/* connect to the HAL */
	ignore_sig = 1;
	ret = plc_connect(PLC_DEV_NAME, cfg, cfg_id);
	ignore_sig = 0;


	/* check result */
	if (ret < 0) {
		fprintf(stderr, "ERROR: plc_connect failed with ret = %d\n", ret);
		goto out;
	}

out:
	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);

	return ret;
}


