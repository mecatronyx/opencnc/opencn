/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: libkin.c
 *
 *
 * User-space "kinematics" library.
 *      * Add support for 'kin_forward' and 'kin_inverse' calls.
 *
 ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <opencn/uapi/kinematic.h>

/* kinematic forward */
void kin_forward(double *world, const double *joint)
{
    int devfd;
    int ret = 0;
    kin_lib_args_t args = {0};

    devfd = open(KIN_DEV_NAME, O_RDWR);

    memcpy(args.joint, joint, sizeof(joint) * KIN_MAX_JOINTS);

    ret = ioctl(devfd, KIN_IOCTL_FORWARD, &args);
    if (ret != 0) {
        printf("[%s] ERROR - forward return an error (%d)\n", KIN_COMP_NAME, ret);
    }

    memcpy(world, args.world, sizeof(world) *KIN_MAX_JOINTS);

    close(devfd);
}

/* kinematic inverse */
void kin_inverse(const double *world, double *joint)
{
    int devfd;
    int ret = 0;
    kin_lib_args_t args = {0};

    devfd = open(KIN_DEV_NAME, O_RDWR);

    memcpy(args.world, world, sizeof(world) * KIN_MAX_JOINTS);

    ret = ioctl(devfd, KIN_IOCTL_INVERSE, &args);
    if (ret != 0) {
        printf("[%s] ERROR - inverse return an error (%d)\n", KIN_COMP_NAME, ret);
    }

    memcpy(joint, args.joint, sizeof(joint) * KIN_MAX_JOINTS);

    close(devfd);
}
