/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2022 by Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * file: kinematics.c
 *
 *
 * It is the user-space part of the "kinematics" OpenCN HAL component. The user-space
 * only checks the load parameters and sends it to the kernel part
 *
 ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <ini.h>

#include <debug.h>
#include <cmdline.h>

#include <opencn/uapi/kinematic.h>

#define KIN_PARAM_SECTION_NAME   "Parameters"

static kin_type_t supported_kins[] = {
    { KIN_TYPE_TRIVIAL, "trivial" },
    { KIN_TYPE_COREXY,  "corexy" },
    { KIN_TYPE_XYZBC,   "xyzbc" },

    {KIN_TYPE_END, "end"},
};

/* used to flag critical regions */
static int ignore_sig = 0;


/* signal handler */
static void quit(int sig) {
    if (ignore_sig == 1) {
        return;
    }

    /* Inform the parent (normally halcmd) that we are up and running. */
    kill(getppid(), SIGUSR1);
    exit(0);
}

static int get_param_index(const char *name)
{
    char index_str[3];
    int  index;
    char prefix[10];

    /* Check if the name is correct */
    memcpy(prefix, name, 5);
    prefix[5] = '\0';

    if (strcmp(prefix, "param")) {
        printf("[%s] ERROR - param name '%s' is not correct\n", KIN_COMP_NAME, name);
        return -1;
    }

    /* Extract parameter index */
    strcpy(index_str, name+5);
    index = atoi(index_str);

    return index;
}


static int handler(void* user, const char* section, const char* name, const char* value)
{
    double *params = (double *)user;
    int index;

    // Check the section - only section 'Parameters' is supported !
    if (strcmp(section, KIN_PARAM_SECTION_NAME)) {
        printf("[%s] ERROR - section '%s' not supported\n", KIN_COMP_NAME, section);
        return 0;
    }

    index = get_param_index(name);
    if (index >= 0)
        params[index] = atof(value);

    return 1;
}


static int read_parameters(char *filename, double *params)
{
    int ret = 0;

    if (ini_parse(filename, handler, params) < 0) {
        printf("[%s] ERROR - Can't load '%s' file\n", KIN_COMP_NAME, filename);
        return 1;
    }

    return ret;
}



/* Check if the value of 'type' parameter is valid
 *
 *  return -1 if the 'type' is not valid or the type_id
 */
static int check_selected_type(char *type)
{
    int i;

    /* checks if 'cfg' is valid */
    for (i = 0; i < KIN_TYPE_END; i++) {
        if (!strcmp(type, supported_kins[i].type_name))
            return supported_kins[i].type_id;
    }

    /* 'type' does not correspond to a valid configuration */
    fprintf(stderr, "[%s] ERROR: kinematic type '%s' is not supported\n", KIN_COMP_NAME, type);
    return -1;
}

/* Connect to the kernel HAL. */
static int kin_connect(kin_connect_args_t *args)
{
    int devfd;
    int ret = 0;

    devfd = open(KIN_DEV_NAME, O_RDWR);

    ret = ioctl(devfd, KIN_IOCTL_CONNECT, args);
    if (ret != 0) {
        printf("[%s] ERROR - ioctl return an error (%d)\n", KIN_COMP_NAME, ret);
        BUG()
    }

    close(devfd);

    return ret;
}

/*
 * Syntax: kinematic type=<TYPE> params=<FILE WITH PARAMS> -p
 */
int main(int argc, char **argv)
{
    int i;
    int ret = 0;
    char *params = NULL, *value = NULL;
    char *cp;
    char *params_file = NULL;
    char *type = NULL;
    kin_connect_args_t args = { 0 };

    for (i = 1; i < argc; i++) {
        cp = argv[i];

        /* First parse the option with "=" */
        next_arg(argv[i], &params, &value);

        if (params) {
            if (!strcmp(params, "type")) {
                type = value;
                continue ;
            }

            if (!strcmp(params, "params")) {
                params_file = value;
                continue ;
            }
        }

        if (*cp != '-') {
            break;
        }

        switch (*(++cp)) {
        case 'p':
            args.enable_pin = 1;
            break;

        default:
            fprintf(stderr, "[%s] ERROR - unknown option '%s'\n", KIN_COMP_NAME, cp);
            goto out;
        }
    }

    if (type) {
        ret = check_selected_type(type);
        if (ret < 0)
            goto out;

        args.type = ret;
    }

    /* Read the params values  */
    if (params_file) {
        ret = read_parameters(params_file, args.params);
        if (ret)
            goto out;
    }

    /* register signal handlers - if the process is killed
     we need to call hal_exit() to free the shared memory */
    signal(SIGTERM, quit);

    /* connect to the HAL */
    ignore_sig = 1;
    ret = kin_connect(&args);
    ignore_sig = 0;

    /* check result */
    if (ret < 0) {
        fprintf(stderr, "[%s] ERROR: connection failed with ret = %d\n", KIN_COMP_NAME, ret);
        goto out;
    }

out:
    /* Inform the parent (normally halcmd) that we are up and running. */
    kill(getppid(), SIGUSR1);

    return ret;
}
