add_executable(sampler sampler.cpp)

# Sampler
target_link_libraries(sampler opencn)
set_target_properties(sampler PROPERTIES COMPILE_FLAGS ${_components_cflags})
set_target_properties(sampler PROPERTIES LINK_FLAGS ${_components_ldflags})
target_include_directories(sampler PRIVATE ${_components_include_directories})

install(TARGETS sampler DESTINATION ${COMPONENTS_INSTALL_PATH})

# Build sampler example
option(WITH_SAMPLER_EXAMPLE "Build sampler example" ON)
if (WITH_SAMPLER_EXAMPLE)
    add_subdirectory(example)
endif()
