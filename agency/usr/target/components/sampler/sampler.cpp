/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2023 by Xavier Soltner <xaver.soltner@heig-vd.ch>
 *
 * file: sampler.cpp
 *
 *
 * It is the user-space part of the "sampler" OpenCN HAL component. 
 * The user-space checks the load parameters and sends it to the kernel part
 * Once the kernel part is loaded, it is in charge of sending the pin data to
 * the userspace spart that log it into a file (default mode). In the --memory
 * mode, the sampler copy the the data into a shmsg.
 * 
 ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <cxxopts.hpp>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <sys/ipc.h>
#include <sys/shm.h>

#include <sampler.h>
#include <af.h>
#include <cmdline.h>

#include <opencn/uapi/sampler.h>

#include <debug.h>

#define MAX_FILENAME_LEN 1000
#define SAMPLER_SIGNAL_TIMEOUT 1.000

int devfd;
int exitval = 1; /* program return code - 1 means error */
int ignore_sig = 0; /* used to flag critical regions */
unsigned int msgid; // memory mode
sig_atomic_t stop_worker;

sampler_connect_args_t args_samp;
sampler_sample_t *s_sample;
int nsample = -1;

/* All mode */
hal_pin_t *pin_overruns = NULL;

/* File mode */
std::string logF;
FILE *fd = NULL;
hal_pin_t *pin_new_file = NULL;

/* Memory mode */
struct shmsg queue_message;

/***********************************************************************
 *                            HAL FUNC 	                              *
 ************************************************************************/
/* signal handler */
static void quit(int sig) {
	if (ignore_sig == 1) {
		return;
	}
	stop_worker = 1;
}

static int sampler_connect(char *devname, sampler_connect_args_t *args_in)
{
    int ret;

    devfd = open(devname, O_RDWR);

    ret = ioctl(devfd, SAMPLER_IOCTL_CONNECT, args_in);
    if (ret != 0){
		printf("[%s] ERROR: Connect failed (%d)\n", devname, ret);
        BUG()
	}

    return ret;
}

void sampler_disconnect(void) {

    ioctl(devfd, SAMPLER_IOCTL_DISCONNECT, args_samp.instance_nr);

    close(devfd);
}

static int sampler_get_instance_nr(void)
{
    int fd;
    int ret;
    sampler_instance_nr_args_t args = { 0 };

    fd = open(SAMPLER_DEV_NAME, O_RDWR);

    ret = ioctl(fd, SAMPLER_IOCTL_INSTANCE_NR, &args);
    if (ret != 0) {
        printf("[%s] ERROR - Retrieve 'sampler' instance number failed \n", SAMPLER_COMP_NAME);
        BUG()
    }

     close(fd);

    return args.instance_nr;
}

/***********************************************************************
 *                            PARSER FUNC                              *
 ************************************************************************/

void init_args (sampler_connect_args_t *args){
	args->depth = 0;
    args->memory = 0;
	args->channel = 0;
	args->tag = 0;
	args->nb_pins = 0;
    args->timeusrloop = 0;
    strcpy(args->path, SAMPLER_DEF_LOGPATH);
    args->nb_samples = -1; /* -1 mean forever*/
	sprintf(args->name, "%s", SAMPLER_DEFAULT_NAME);
}

/* Check if the character is allowed for the recorded pin in 'cfg' parameter */
bool is_valid_char_cfg(char c){
    switch (c) {
        case 'b' : //HAL_BOOL
        case 'B' :
            return true;
        case 's' : //HAL_S32
        case 'S' :
            return true;
        case 'u' : //HAL_U32
        case 'U' :
            return true;
        case 'f' : //HAL_FLOAT
        case 'F' :
            return true;
        default : 
            printf("[%s] Error: '%c' is an invalid 'cfg' parameter, only 'b'/'s'/'u'/'f' is allowed, aborting\n",SAMPLER_COMP_NAME,c);
            return false;
    }

}

/* Parse the input argument of the sampler using cxxopts */
std::pair<bool,sampler_connect_args_t> parse_sampler_input(int argc, char* argv[]){
    
    bool is_valid = true;
    int nb_pins = 0;
    sampler_connect_args_t ret;

    init_args(&ret);

    cxxopts::Options options(SAMPLER_COMP_NAME, "Sampler component");

    options.add_options()
        ("c,cfg", "Configuration as <char*>, allowed character is 'b'/'s'/'u'/'f'", cxxopts::value<std::string>())
        ("d,depth", "(opt) Depth of buffer", cxxopts::value<int>()->default_value(SAMPLER_DEFAULT_DEPTH))
        ("m,memory", "(opt) Use RAM instead of file", cxxopts::value<bool>())
        ("n,name", "(opt) Name for sampler instance : sampler.<arg>", cxxopts::value<std::string>())
        ("t,tag", "(opt) Put sample number on first column", cxxopts::value<bool>())
        ("N,number", "(opt) Maximal number of samples that are read from the pins.If not set, the sampler continuously read the pins.", cxxopts::value<long int>())
        ("T,timeusrloop", "(opt)  FILE MODE ONLY - Time the loop for the userspace part of sampler to write to file.", cxxopts::value<bool>())
        ("p,path", "(opt)  FILE MODE ONLY - Specify an absolute path on where to store the log.", cxxopts::value<std::string>())
        ;

    options.allow_unrecognised_options();

    try {

        /* Parse input */
        auto result = options.parse(argc, argv);
        auto unmatched = result.unmatched();
    
        /* Check if invalid argument is present */
        if (unmatched.size() != 0) {
            printf("[%s] Error - The following arguments are not valid: ", SAMPLER_COMP_NAME);
            for ( auto & element : unmatched)
                printf("'%s' ", element.c_str());
            printf("\n");
            is_valid = false;
        }

        /* Check if we have duplicate argument */
        if ( result.count("cfg") > 1 ){
            printf("[%s] Error - duplicate argument %s", SAMPLER_COMP_NAME,"cfg");
            is_valid = false;
        }
        if ( result.count("depth") > 1 ){
            printf("[%s] Error - duplicate argument %s", SAMPLER_COMP_NAME,"depth");
            is_valid = false;
        }
        if ( result.count("memory") > 1 ){
            printf("[%s] Error - duplicate argument %s", SAMPLER_COMP_NAME,"memory");
            is_valid = false;
        }
        if ( result.count("name") > 1 ){
            printf("[%s] Error - duplicate argument %s", SAMPLER_COMP_NAME,"name");
            is_valid = false;
        }
        if ( result.count("tag") > 1 ){
            printf("[%s] Error - duplicate argument %s", SAMPLER_COMP_NAME,"tag");
            is_valid = false;
        }
        if ( result.count("number") > 1 ){
            printf("[%s] Error - duplicate argument %s", SAMPLER_COMP_NAME,"number");
            is_valid = false;
        }
        if ( result.count("timeusrloop") > 1 ){
            printf("[%s] Error - duplicate argument %s", SAMPLER_COMP_NAME,"timeusrloop");
            is_valid = false;
        }
        if ( result.count("path") > 1 ){
            printf("[%s] Error - duplicate argument %s", SAMPLER_COMP_NAME,"path");
            is_valid = false;
        }

        /* Get instance number from kernel */
        ret.instance_nr = sampler_get_instance_nr();

        /* Name of the sampler ('n/name' option)*/
        if (result.count("name") == 1) {
            std::string name_s = result["name"].as<std::string>();
            char* name = const_cast<char*>(name_s.c_str()); // We need char* for kernel side

            if(name_s.length() > SAMPLER_NAME_MAX_LENGTH){
                printf("[%s] Error : 'name' has length=%ld, max is SAMPLER_NAME_MAX_LENGTH=%d, aborting \n", SAMPLER_COMP_NAME, name_s.length(), SAMPLER_NAME_MAX_LENGTH);
                is_valid = false;
            } else {
                strcpy(ret.name,name);
                printf("[%s.%s] name=%s\n",SAMPLER_COMP_NAME, ret.name, ret.name);
            }            
        } else {
            sprintf(ret.name,"%d",ret.instance_nr);
        }

        /* Depth of buffer ('d/depth' option)*/
        if (result.count("depth") == 1) {
            ret.depth = result["depth"].as<int>();
            printf("[%s.%s] depth=%d\n",SAMPLER_COMP_NAME, ret.name, ret.depth);
        } else {
            printf("[%s.%s] Warn - 'depth' parameter is missing, setting default depth at %s\n", SAMPLER_COMP_NAME, ret.name, SAMPLER_DEFAULT_DEPTH);
            ret.depth = std::stoi(SAMPLER_DEFAULT_DEPTH);
        }

        /* Configuration ('c/cfg' option) */
        if (result.count("cfg") == 1) {
            std::string cfg_s = result["cfg"].as<std::string>();
            char* cfg = const_cast<char*>(cfg_s.c_str()); // We need char* for kernel side

            printf("[%s.%s] cfg=%s\n",SAMPLER_COMP_NAME, ret.name, cfg_s.c_str());

            /* Check valid type */
            for(char* c = cfg; *c; ++c) {
                nb_pins += 1;
                if( !is_valid_char_cfg(*c))
                    is_valid = false;
            }

            /* Check size of cfg*/
            if(cfg_s.length() == 0) {
                printf("[%s.%s] Error : 'cfg' is empty, aborting \n", SAMPLER_COMP_NAME, ret.name);
                is_valid = false;
            } else if(cfg_s.length() > SAMPLER_MAX_PINS){
                printf("[%s.%s] Error : 'cfg' has length=%ld, max is SAMPLER_MAX_PINS=%d, aborting \n", SAMPLER_COMP_NAME, ret.name, cfg_s.length(), SAMPLER_MAX_PINS);
                is_valid = false;
            } else {
                /* Store parameter */
                ret.nb_pins = nb_pins;
                strcpy(ret.cfg,cfg);
            }      
            
        } else {
            printf("[%s.%s] Error - 'cfg' parameter is missing, aborting\n", SAMPLER_COMP_NAME, ret.name);
            is_valid = false;
        }

        /* Memory mode ('m/memory' option)*/
        if (result.count("memory") == 1) {
            ret.memory = true;
            printf("[%s.%s] memory=%d\n",SAMPLER_COMP_NAME, ret.name, ret.memory);
        }

        /* Check if the instance number is below SAMPLER_MAX_INSTANCE */
        if (ret.instance_nr >= SAMPLER_MAX_INSTANCE){
            is_valid = false;
            printf("[%s.%s] Error : The maximum number of %d sampler instance is loaded, aborting \n", SAMPLER_COMP_NAME, ret.name, SAMPLER_MAX_INSTANCE);
        }

        /* Tag ('t/tag' option)*/
        if (result.count("tag") == 1) {
            ret.tag = true;
            printf("[%s.%s] tag=%d\n",SAMPLER_COMP_NAME, ret.name, ret.tag);
        }

        /* Number of sample ('N/number' option)*/
        if (result.count("number") == 1) {
            ret.nb_samples = result["number"].as<long int>();
            if(ret.nb_samples < 1){ //Check we take at least 1 sample
                is_valid = false;
                printf("[%s.%s] Error : 'number'=%ld < 1, at least one sample need to be taken, aborting \n", SAMPLER_COMP_NAME, ret.name, ret.nb_samples);
            } else {
                printf("[%s.%s] numberOfSample=%ld\n",SAMPLER_COMP_NAME, ret.name, ret.nb_samples);
            }
        }

        /* Time user loop ? ('T/timeusrloop' option)*/
        if (result.count("timeusrloop") == 1) {
            ret.timeusrloop = true;
            printf("[%s.%s] timeusrloop=%d\n",SAMPLER_COMP_NAME, ret.name,ret.timeusrloop);
        }

        /* Path (p/path) option */
        if (result.count("path") == 1) {
            std::string path_s = result["path"].as<std::string>();
            char* path = const_cast<char*>(path_s.c_str()); // We need char* for kernel side

            printf("[%s.%s] path=%s\n",SAMPLER_COMP_NAME, ret.name, path_s.c_str());

            /* Check size of path*/
            if(path_s.length() > SAMPLER_PATH_MAX) {
                printf("[%s.%s] Error : 'path' lenght is %ld, max is SAMPLER_PATH_MAX=%d \n", SAMPLER_COMP_NAME, ret.name, path_s.length(),SAMPLER_PATH_MAX);
                is_valid = false;
            } else {
                /* Store parameter */
                strcpy(ret.path, path);
            }      
            
        }

    } catch (const cxxopts::exceptions::exception& e) {
        is_valid = false;
        printf("[%s] cxxpopts::exceptions : %s\n", SAMPLER_COMP_NAME, e.what());
    }

    /* Print help function*/
    if(!is_valid){
        printf("%s\n",options.help().c_str());
    }

    return std::make_pair(is_valid,ret);
    
}

/***********************************************************************
 *                            WORKER FUNC                              *
 ************************************************************************/

int sampler_worker_write_file(void){

    int ret_n_sample;
    time_t start_signame, currTime_signame; //Timeout on pin signal retrival
    clock_t start_loopTime, end_loopTime; // mesure userspace loop time

    /* Mesure loop time */
    start_loopTime = clock();

    /* Open file and write header on new file request */
    if (pin_get_value(pin_new_file)->b) {

        fclose(fd);
        fd = fopen(logF.c_str(), "w");

        /* Add sampler component header */
        std::time_t time = std::time(nullptr);
        fprintf(fd, "# Generated file by 'sampler.%s' component on %s", args_samp.name,std::asctime(std::gmtime(&time)));

        /* Add optionnal header */
        FILE *header_fd = fopen("/root/header.txt", "r");
        if (header_fd) {
            struct stat stats;
            stat("/root/header.txt", &stats);
            char *header_content = (char *) malloc(stats.st_size);
            if (header_content) {
                fread(header_content, stats.st_size, 1, header_fd);
                fwrite(header_content, stats.st_size, 1, fd);
                free(header_content);
            } else {
                fprintf(stderr,
                    "[%s.%s] Failed to alloc memory for header copy\n", SAMPLER_COMP_NAME, args_samp.name);
            }
            fclose(header_fd);
        } else {
            fprintf(stderr, "[%s.%s] WARN : No optional header available\n", SAMPLER_COMP_NAME, args_samp.name);
        }

        /* Add header if sample is tagged */
        if(args_samp.tag){
            fprintf(fd,"num ");
        }
        
        /* Add header with signal name */
        std::time(&start_signame);
        std::time(&currTime_signame);
        for (int i = 0; i < args_samp.nb_pins; i++) {
            std::string pin_n = "sampler.";
            pin_n += args_samp.name;
            pin_n += ".pin." + std::to_string(i);
            while(!pin_as_sig_linked(pin_n.c_str()) && SAMPLER_SIGNAL_TIMEOUT > difftime(currTime_signame,start_signame)){
                std::time(&currTime_signame);
                usleep(100);
            }

            fprintf(fd,"%s",pin_get_sig_name(pin_n.c_str()));
            /* Add space if not last pins */
            if ( i == args_samp.nb_pins - 1)
                fprintf(fd," ");
        }
        /* Add header if we time user loop writer */
        if (args_samp.timeusrloop) {
            fprintf(fd,"sampler-usr-loop-time");
        }

        fprintf(fd,"\n");

        /* Reset pin */
        pin_get_value(pin_overruns)->s = 0;
        pin_get_value(pin_new_file)->b = 0;
#if OPENCN_SAMPLER_DEBUG
        fprintf(stderr, "[%s.%s] Ack new-file\n", SAMPLER_COMP_NAME, args_samp.name);
#endif

        if (!fd) {
            fprintf(stderr, "[%s.%s] failed to open log file\n", SAMPLER_COMP_NAME, args_samp.name);
            exitval = -1;
            return -1;
        }

        /* Reset nsample*/
        nsample = args_samp.nb_samples;
        
        /* Flush to file */
        fflush(fd);
    }

    /* Check if number of sampled pin is done */
    if (nsample == 0){
        return 0;
    }

    /* WARNING: read here is non-blocking 
    * Retrieve sample and write to file
    */
    ret_n_sample = read(devfd, s_sample, args_samp.instance_nr);
    if( ret_n_sample > 0 ) {
        /* Iterate through sample */
        for (int i = 0; i < ret_n_sample; i++)
        {   
            /* Add tag */
            if (args_samp.tag)
                fprintf(fd, "%ld ", s_sample[i].n_tag);
            /* Iterate through pin */
            for (int j = 0; j < args_samp.nb_pins; j++) {
                switch (s_sample[i].pins[j].type) {
                case HAL_FLOAT:
                    fprintf(fd, "%f", s_sample[i].pins[j].f);
                    break;
                case HAL_S32:
                    fprintf(fd, "%d", s_sample[i].pins[j].s);
                    break;
                case HAL_U32:
                    fprintf(fd, "%u", s_sample[i].pins[j].u);
                    break;
                case HAL_BIT:
                    fprintf(fd, "%d", s_sample[i].pins[j].b);
                    break;
                default:
                    fprintf(fd, "%s", "<UNHANDLED>");
                    break;
                }
                /* Add space - except for last pin */
                if ( j != args_samp.nb_pins)
                    fprintf(fd, " ");                
            }

            /* Check if we log user loop time */
            if (args_samp.timeusrloop) {
                /* Check if it's the last sample of the packet */
                if( i == ret_n_sample-1){
                    /* Do nothing, as we put timing of this sequence after the fflush */
                } else {
                    fprintf(fd, "N/A\n");
                }
            } else {
                fprintf(fd, "\n");
            }
        }

        /* Update number of sample to record, if necessary */
        if (nsample > 0) {
            nsample--;
        }
        
        /* Flush to file */
        fflush(fd);

        /* Add userspace timing, if requested */
        if (args_samp.timeusrloop) {
            end_loopTime = clock();
            double diff = ((double) (end_loopTime - start_loopTime)) / CLOCKS_PER_SEC;
            fprintf(fd, "%f\n", diff);
        }
    }
    return 0;
}


int sampler_worker_write_memory(void){
    int ret_n_sample;

    /* Give a chance to the others threads */
    usleep(1);

    /* Check if number of sampled pin is done */
    if (nsample == 0){
        return 0;
    }

    /* Write data in memory using message queue */
    ret_n_sample = read(devfd, s_sample, args_samp.instance_nr);
    if( ret_n_sample > 0 ) {
        /* Iterate through sample */
        for (int i = 0; i < ret_n_sample; i++) {
            queue_message.mtype = 1;
            memcpy(&queue_message.samples, &s_sample[i], sizeof(s_sample[i]));
            if (msgsnd(msgid, &queue_message, sizeof(queue_message.samples), IPC_NOWAIT) < 0) {
                //fprintf(stderr, "[%s.%s] Cannot add message to queue\n", SAMPLER_COMP_NAME, args_samp.name);
                return 0;
            }
        }
    }

    return 0;
}

/***********************************************************************
 *                            MAIN PROGRAM                              *
 ************************************************************************/

/*
 * Syntax: sampler --cfg=<char*> [--depth=<int>] [--memory] [--name=<char*>] [--tag] [--timeusrloop] [--path=<char*]
 */
int main(int argc, char **argv)
{
	int ret;
    bool args_is_valid = false;
    char comp_name[HAL_NAME_LEN + 1];
    cpu_set_t cpuset;

    // Set CPU affinity
	CPU_ZERO(&cpuset);
	CPU_SET(2, &cpuset);
	CPU_SET(3, &cpuset);

	sched_setaffinity(getpid(), sizeof(cpuset), &cpuset);

    /* Set return code to "fail", clear it later if all goes well */
    exitval = 1;
    stop_worker = 0;

    /* Get input argument */
    std::pair<bool, sampler_connect_args_t> r = parse_sampler_input(argc,argv);
    args_is_valid = r.first;
    args_samp = r.second;
    args_samp.channel = 0; //should be dynamic ?

	/* Check if input arg is valid*/
    if( !args_is_valid){
        exitval=-EINVAL;
        goto out;
    } else {
        /* Set global var for worker */
        s_sample = new sampler_sample_t[args_samp.depth];
    }

    /* Check if component name is duplicate */
    sprintf(comp_name, "%s.%s",SAMPLER_COMP_NAME,args_samp.name);
    if (comp_find_by_name(comp_name) != NULL){
        fprintf(stderr, "[%s] ERROR: '%s' component already exist, specify another name, abort\n",  SAMPLER_COMP_NAME, comp_name);
        exitval=-EEXIST;
        goto out;
    }

    /* register signal handlers - if the process is killed
     we need to call hal_exit() to free the shared memory */
    signal(SIGTERM, quit);
    signal(SIGPIPE, SIG_IGN);

    /* connect to the HAL */
    ignore_sig = 1;
    ret = sampler_connect(SAMPLER_DEV_NAME, &args_samp);
    ignore_sig = 0;
    /* Inform the parent (normally halcmd) that we are up and running. */
    kill(getppid(), SIGUSR1);

    /* Check result */
    if (ret < 0) {
        fprintf(stderr, "[%s] ERROR: %s failed with ret = %d\n", __func__, SAMPLER_COMP_NAME, ret);
        goto out;
    }

    /* Set global var for specific mode */
	if (!args_samp.memory) {

		/* Get pin & params*/
        std::string baseN = SAMPLER_COMP_NAME;
        baseN.append(".");
        baseN.append(args_samp.name);
        std::string newFile = baseN;
        newFile.append(".new-file");
        std::string overruns = baseN;
        overruns.append(".overruns");
        pin_new_file = pin_find_by_name(newFile.c_str());
        pin_overruns = pin_find_by_name(overruns.c_str());
        //fprintf(stderr, "[%s.%s] overruns=%s newFile=%s\n",SAMPLER_COMP_NAME,args_samp.name,newFile.c_str(),overruns.c_str());
        
        /* Get Log file */
        logF = args_samp.path;
        if (logF.back() != '/')
            logF.append("/");
        
        logF.append("log_sampler_");
        logF.append(args_samp.name);
        logF.append(".csv");
        fd = fopen(logF.c_str(), "w");

        fprintf(stderr, "[%s.%s] Logfile path=%s\n",SAMPLER_COMP_NAME, args_samp.name, logF.c_str());
        if (!fd) {
            fprintf(stderr, "[%s.%s] Failed to open log file\n",SAMPLER_COMP_NAME, args_samp.name);
            exitval = -1;
            goto out;
        }
    } else {
        /* Get shared memory ID */
        msgid = msgget(SAMPLER_MSG_KEY, SAMPLER_SHMEM_PERM | IPC_CREAT);
        if (msgid == -1) {
            fprintf(stderr, "[%s.%s] Failed to create message queue\n",SAMPLER_COMP_NAME, args_samp.name);
            exitval = -2;
            goto out;
        }
    }

	/* Worker */
	while (!stop_worker) {
        if (!args_samp.memory) { 
            /* File mode */
            if (sampler_worker_write_file() < 0 ){
                goto out;
            }
        } else { 
            /* Memory mode */
            if (sampler_worker_write_memory() < 0 ){
                goto out;
            }
        }
        /* Realese CPU */
        sched_yield();
    }
    
    /* run was succesfull */
    exitval = 0;

out:

	fprintf(stderr, "[%s.%s] Exited with ret=%d\n", SAMPLER_COMP_NAME, args_samp.name, exitval);

    /* Free sample structre */
    delete [] s_sample;

	/* Close file */
	if (fd != NULL) {
        fclose(fd);
        fd = NULL;
    }

    /* Free specific for memory mode */
    if (args_samp.memory) {
        /* Remove message queue */
        if (msgctl(msgid, IPC_RMID, NULL) == -1) {
            fprintf(stderr, "[%s.%s] Cannot remove message queue", SAMPLER_COMP_NAME, args_samp.name);
            exitval = -2;
        }
    }

    ignore_sig = 1;

    sampler_disconnect();

	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);

    return exitval;
}

