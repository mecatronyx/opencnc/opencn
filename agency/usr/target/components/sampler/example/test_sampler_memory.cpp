/********************************************************************
 * SPDX-License-Identifier: GPL-2.0
 * Copyright (C) 2023 by Xavier Soltner <xavier.soltner@heig-vd.ch>
 *
 * file: test_sampler_memory.c
 *
 * This is a simple program to test the memory mode of the sampler.
 * It retrieve the sample from the memory and simply print it in 
 * the console.
 * This file can be used as an example to use the memory mode in
 * another part of OpenCN.
 * WARNING : Print can become quite heavy
 * 
 * Source/Doc :
 * https://mecatronyx.gitlab.io/opencnc/opencn/components/sampler.html
 ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sampler.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <errno.h>

int main(int argc, char *argv[]) {

    struct shmsg queue_message;
    int status;
    
    /* Get the ID of the shared memory segment */
    int msgid = msgget(SAMPLER_MSG_KEY, SAMPLER_SHMEM_PERM);
    if (msgid == -1) {
        printf("[test_sampler_memory.cpp] Failed to get shared memory segment\n");
        return -1;
    }

    printf("[test_sampler_memory.cpp] Waiting for messages..\n");
    
    while(true) {
        
        /* Read the next queue message */
        status = msgrcv(msgid, &queue_message,
            sizeof(queue_message.samples), 0 , MSG_NOERROR);

        if (status == ENOMSG) {
            /* No message available in the queue */
            //printf("[test_sampler_memory.cpp] No message available in the queue\n");
            continue;
        } else if (status < 0) {
            /* An error occured with the queue */
            //printf("[test_sampler_memory.cpp] An error occured with the queue\n");
            continue;
        }

        /* Print samples */  
        printf("%ld ",queue_message.samples.n_tag);
        for (int i = 0; i < queue_message.samples.n_pins; i++)
        {
            switch (queue_message.samples.pins[i].type){
            case HAL_FLOAT:
                printf("%f ", queue_message.samples.pins[i].f);
                break;
            case HAL_S32:
                printf("%d ", queue_message.samples.pins[i].s);
                break;
            case HAL_U32:
                printf("%u ", queue_message.samples.pins[i].u);
                break;
            case HAL_BIT:
                printf("%d ", queue_message.samples.pins[i].b);
                break;
            default:
                printf("%s ", "<UNHANDLED>");
                break;
            }
        }
        printf("\n");
    }
}
