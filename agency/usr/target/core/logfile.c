/*
 * Copyright (C) 2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/ioctl.h>

#include <opencn/uapi/opencn.h>

void usage(void) {
	printf("\nUsage is:\n\n");
	printf("  logfile -e : enabling logging\n");
	printf("  logfile -d : disabling logging\n");

	exit(-1);
}

int main(int argc, char **argv) {

	int fd, ret;
	char cmdline[80];
	char c;

	printf("-- logfile utility --\n\n");
	printf("Enabling RT logging in the file %s\n", OPENCN_LOGFILE_NAME);
	printf("\n");

	if (argc != 2)
		usage();

	fd = open(OPENCN_CORE_DEV_NAME, O_RDWR);
	if (fd < 0) {
		printf("Failed to create %s\n", OPENCN_LOGFILE_NAME);
		exit(-1);
	}

	c = getopt(argc, argv, "ed");

	switch (c) {

	case 'e':
		printf("OpenCN logging into %s ...\n", OPENCN_LOGFILE_NAME);

		ioctl(fd, OPENCN_IOCTL_LOGFILE_ON, 0);
		break;

	case 'd':
		printf("OpenCN logging shutdown\n");

		ioctl(fd, OPENCN_IOCTL_LOGFILE_OFF, 0);

		/* Move the log file into its destination */
		printf("OpenCN logging: moving /var/log/opencn.log to %s\n", OPENCN_LOGFILE_NAME);
		sprintf(cmdline, "mv /var/log/opencn.log %s\n", OPENCN_LOGFILE_NAME);

		ret = system(cmdline);
		if (ret < 0)
			perror("OpenCN logging");
		break;

	default:
		usage();
	}

	return 0;
}
