add_executable(logfile logfile.c)

set_target_properties(logfile PROPERTIES COMPILE_FLAGS "-pedantic -Wall -Wno-long-long -Wno-variadic-macros")
set_target_properties(logfile PROPERTIES LINK_FLAGS "-lc")
