/*
 * Copyright (C) 2019 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *                    Kevin Joly <kevin.joly@heig-vd.ch>
 *                    Peter Lichard <peter.lichard@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef OPENCN_SERVER_INTERFACE_IMPL_HPP
#define OPENCN_SERVER_INTERFACE_IMPL_HPP

#include "opencn-interface/opencn_interface.capnp.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#include "sampler.h"
#include "af.h"
#include "feedopt.hpp"

#include <af.hpp>

class OpenCNServerInterfaceImpl: public OpenCNServerInterface::Server {
    public:
        OpenCNServerInterfaceImpl();

    protected:
        ::kj::Promise<void> setFeedoptCommitCfg(SetFeedoptCommitCfgContext context) override;
        ::kj::Promise<void> getCyclicData(GetCyclicDataContext context) override;

        ::kj::Promise<void> setLcctSetMachineModeHoming(SetLcctSetMachineModeHomingContext context) override;
        ::kj::Promise<void> setLcctSetMachineModeJog(SetLcctSetMachineModeJogContext context) override;
        ::kj::Promise<void> setLcctSetMachineModeInactive(SetLcctSetMachineModeInactiveContext context) override;
        ::kj::Promise<void> setLcctSetMachineModeGCode(SetLcctSetMachineModeGCodeContext context) override;

        ::kj::Promise<void> setStartHoming(SetStartHomingContext context) override;
        ::kj::Promise<void> setStopHoming(SetStopHomingContext context) override;

        ::kj::Promise<void> setHomePositionX(SetHomePositionXContext context) override;
        ::kj::Promise<void> setHomePositionY(SetHomePositionYContext context) override;
        ::kj::Promise<void> setHomePositionZ(SetHomePositionZContext context) override;

        ::kj::Promise<void> setSpeedSpindle(SetSpeedSpindleContext context) override;
        ::kj::Promise<void> setActiveSpindle(SetActiveSpindleContext context) override;
        ::kj::Promise<void> setSpindleThreshold(SetSpindleThresholdContext context) override;

        ::kj::Promise<void> setJogX(SetJogXContext context) override;
        ::kj::Promise<void> setJogY(SetJogYContext context) override;
        ::kj::Promise<void> setJogZ(SetJogZContext context) override;

        ::kj::Promise<void> setRelJog(SetRelJogContext context) override;
        ::kj::Promise<void> setPlusJog(SetPlusJogContext context) override;
        ::kj::Promise<void> setMinusJog(SetMinusJogContext context) override;

        ::kj::Promise<void> setAbsJog(SetAbsJogContext context) override;
        ::kj::Promise<void> setGoJog(SetGoJogContext context) override;
        ::kj::Promise<void> setSpeedJog(SetSpeedJogContext context) override;
        ::kj::Promise<void> setStopJog(SetStopJogContext context) override;

        ::kj::Promise<void> setOffset(SetOffsetContext context) override;

        ::kj::Promise<void> setGcodeStart(SetGcodeStartContext context) override;
        ::kj::Promise<void> setGcodePause(SetGcodePauseContext context) override;

        ::kj::Promise<void> setFaultReset(SetFaultResetContext context) override;

        ::kj::Promise<void> setFeedrateScale(SetFeedrateScaleContext context) override;
        ::kj::Promise<void> setFeedoptReset(SetFeedoptResetContext context) override;

        ::kj::Promise<void> readLog(ReadLogContext context) override;

        ::kj::Promise<void> setFeedoptConfig(SetFeedoptConfigContext context) override;
        ::kj::Promise<void> getFeedoptConfig(GetFeedoptConfigContext context) override;

        ::kj::Promise<void> toolpathStartChannel(ToolpathStartChannelContext context) override;
        ::kj::Promise<void> toolpathStopChannel(ToolpathStopChannelContext context) override;
        ::kj::Promise<void> toolpathReadSamples(ToolpathReadSamplesContext context) override;

        ::kj::Promise<void> sendFileParam(SendFileParamContext context) override;
        ::kj::Promise<void> sendFileData(SendFileDataContext context) override;
        ::kj::Promise<void> pathExist(PathExistContext context) override;
        ::kj::Promise<void> createFolder(CreateFolderContext context) override;
        ::kj::Promise<void> samplerNewFile(SamplerNewFileContext context) override;
        ::kj::Promise<void> getFileData(GetFileDataContext context) override;

        ::kj::Promise<void> startJog(StartJogContext context) override;

        ::kj::Promise<void> setFreeTool(SetFreeToolContext context);
        ::kj::Promise<void> setFreePalette(SetFreePaletteContext context);

        ::kj::Promise<void> getMachineLimits(GetMachineLimitsContext context);

        ::kj::Promise<void> getSpindleOverride(GetSpindleOverrideContext context);
        ::kj::Promise<void> setSpindleOverride(SetSpindleOverrideContext context);

    private:

        std::string _axes = "XYZBC";

        HalPinI32 _pinFeedoptOptimisingProgress{"feedopt.optimising.progress"};
        HalPinI32 _pinFeedoptOptimisingCount{"feedopt.optimising.count"};
        HalPinI32 _pinFeedoptResamplingProgress{"feedopt.resampling.progress"};

        HalPinDouble _pinFeedoptResamplingManualOverride{"feedopt.resampling.manual-override"};
        HalParamDouble _pinFeedoptResamplingAutoOverride{"feedopt.resampling.auto-override"};

        HalPinBool _pinMachiningRunning{"ocno.machining.running"};

        HalPinBool _pinSamplerNewFile{"sampler.0.new-file"};

        HalPinI32 _pinFeedoptLine{"feedopt.resampling.gcodeline"};

        /* Load on axis */
        HalPinDouble _pinLoadX{"lcec.0.TSDXB.extra-var0-0"};
        HalPinDouble _pinLoadY{"lcec.0.TSDYC.extra-var0-0"};
        HalPinDouble _pinLoadZ{"lcec.0.TSDZS.extra-var0-0"};
        HalPinDouble _pinLoadB{"lcec.0.TSDXB.extra-var0-1"};
        HalPinDouble _pinLoadC{"lcec.0.TSDYC.extra-var0-1"};
        HalPinDouble _pinLoadS{"lcec.0.TSDZS.extra-var0-1"};

        /* Spindle Temperature */
        HalPinDouble _pinSpindleTemperature{"lcec.0.EL3202.chan1.temperature"};

        HalPinBool _pinFeedoptGenStartCfg{"feedopt.gen.start"};
        HalPinI32 _pinFeedoptQueueSize{"feedopt.sample.number"};

        HalPinBool _pinSetMachineModeHoming{"ocno.set-machine-homing"};
        HalPinBool _pinSetMachineModeJog{"ocno.set-machine-jog"};
        HalPinBool _pinSetMachineModeInactive{"ocno.set-machine-inactive"};
        HalPinBool _pinSetMachineModeMachining{"ocno.set-machine-machining"};

        HalPinBool _pinInModeHoming{"ocno.in-machine-homing"};
        HalPinBool _pinInModeJog{"ocno.in-machine-jog"};
        HalPinBool _pinInModeInactive{"ocno.in-machine-inactive"};
        HalPinBool _pinInModeGCode{"ocno.in-machine-machining"};

        HalPinBool _pinHomingProcessing{"ocno.homing.processing"};
        HalPinBool _pinJogRunning{"ocno.jog.running"};
        HalPinBool _pinMachiningFinished{"ocno.machine-state"};
        HalPinU32  _pinMachineState{"ocno.machine-state"};

        HalPinBool _pinStartHoming{"ocno.homing.start"};
        HalPinBool _pinStopHoming{"ocno.homing.stop"};

        HalPinDouble _pinHomePositionX{"ocno.homing.axisX.move-offset"};
        HalPinDouble _pinHomePositionY{"ocno.homing.axisY.move-offset"};
        HalPinDouble _pinHomePositionZ{"ocno.homing.axisZ.move-offset"};

        HalPinDouble _pinSpeedSpindle{"ocno.jog.target-spindle-speed"};
        HalPinDouble _pinSpindleOverride{"spindle.0.cmd-override"};
        HalPinBool _pinActiveSpindle{"ocno.spindle.request"};
        HalParamDouble _paramSpindleThreshold{"spindle.0.velocity.tolerance"};

        HalPinBool _pinJogStart{"ocno.jog.start"};
        HalPinBool _pinJogStop{"ocno.jog.stop"};

        HalParamDouble _paramJogSpeed0{"simple_pg.joint0.speed"};
        HalParamDouble _paramJogSpeed1{"simple_pg.joint1.speed"};
        HalParamDouble _paramJogSpeed2{"simple_pg.joint2.speed"};
        HalParamDouble _paramJogSpeed3{"simple_pg.joint3.speed"};
        HalParamDouble _paramJogSpeed4{"simple_pg.joint4.speed"};

        // HalPinDouble _pinOffsetX{"ocno.machining.world-offset-X"};
        // HalPinDouble _pinOffsetY{"ocno.machining.world-offset-Y"};
        // HalPinDouble _pinOffsetZ{"ocno.machining.world-offset-Z"};
        // HalPinDouble _pinOffsetC{"lcct.spinbox-offset-ThetaZ"};

        HalPinBool _pinMachiningStart{"ocno.machining.start"};
        HalPinBool _pinMachiningPause{"ocno.machining.pause"};
        HalPinBool _pinFeedoptReady{"feedopt.ready"};

        HalPinBool _pinFaultReset{"ocno.fault-reset"};

        HalPinDouble _pinCurrPosX{"ocno.axisX.current-joint-pos"};
        HalPinDouble _pinCurrPosY{"ocno.axisY.current-joint-pos"};
        HalPinDouble _pinCurrPosZ{"ocno.axisZ.current-joint-pos"};
        HalPinDouble _pinCurrPosB{"ocno.axisB.current-joint-pos"};
        HalPinDouble _pinCurrPosC{"ocno.axisC.current-joint-pos"};

        HalPinDouble _pinCurrVelSpindle{"lcec.0.TSDZS.current-velocity-1"};

        HalPinBool _pinCurrModeInactiveX{"ocno.axisX.in-mode-inactive"};
        HalPinBool _pinCurrModeInactiveY{"ocno.axisY.in-mode-inactive"};
        HalPinBool _pinCurrModeInactiveZ{"ocno.axisZ.in-mode-inactive"};
        HalPinBool _pinCurrModeInactiveB{"ocno.axisB.in-mode-inactive"};
        HalPinBool _pinCurrModeInactiveC{"ocno.axisC.in-mode-inactive"};
        HalPinBool _pinCurrModeInactiveSpindle{"lcec.0.TSDZS.in-mode-inactive-1"};

        // HalPinBool _pinCurrModeInactiveSpindle{"lcct.in-mode-inactive-3"};

        HalPinBool _pinCurrModeFaultX{"ocno.axisX.in-fault"};
        HalPinBool _pinCurrModeFaultY{"ocno.axisY.in-fault"};
        HalPinBool _pinCurrModeFaultZ{"ocno.axisZ.in-fault"};
        HalPinBool _pinCurrModeFaultB{"ocno.axisB.in-fault"};
        HalPinBool _pinCurrModeFaultC{"ocno.axisC.in-fault"};
        HalPinBool _pinCurrModeFaultSpindle{"lcec.0.TSDZS.in-fault-1"};

        // HalPinBool _pinCurrModeFaultSpindle{"lcct.in-fault-3"};

        HalPinBool _pinCurrModeHomingX{"ocno.axisX.in-mode-homing"};
        HalPinBool _pinCurrModeHomingY{"ocno.axisY.in-mode-homing"};
        HalPinBool _pinCurrModeHomingZ{"ocno.axisZ.in-mode-homing"};
        HalPinBool _pinCurrModeHomingB{"ocno.axisB.in-mode-homing"};
        HalPinBool _pinCurrModeHomingC{"ocno.axisC.in-mode-homing"};
        // HalPinBool _pinCurrModeHomingSpindle{"lcct.in-mode-hm-3"};

        HalPinBool _pinCurrModeCSPX{"ocno.axisX.in-mode-csp"};
        HalPinBool _pinCurrModeCSPY{"ocno.axisY.in-mode-csp"};
        HalPinBool _pinCurrModeCSPZ{"ocno.axisZ.in-mode-csp"};
        HalPinBool _pinCurrModeCSPB{"ocno.axisB.in-mode-csp"};
        HalPinBool _pinCurrModeCSPC{"ocno.axisC.in-mode-csp"};
        HalPinBool _pinCurrModeCSVSpindle{"lcec.0.TSDZS.in-mode-csv-1"};
        // HalPinBool _pinCurrModeCSPSpindle{"lcct.in-mode-csp-3"};

        // HalPinBool _pinCurrModeCSVX{"lcct.in-mode-csv-0"};
        // HalPinBool _pinCurrModeCSVY{"lcct.in-mode-csv-1"};
        // HalPinBool _pinCurrModeCSVZ{"lcct.in-mode-csv-2"};
        // HalPinBool _pinCurrModeCSVSpindle{"lcct.in-mode-csv-3"};

        HalPinBool _pinIsHomed{"ocno.machine-homed"};
        HalPinBool _pinFeedoptReset{"feedopt.reset"};
        HalPinBool _pinFeedoptGenActive{"feedopt.gen.active"};
        HalPinBool _pinFeedoptReadActive{"feedopt.read.active"};

        HalPinBool _pinFreeTool{"spindle.0.atc.cmd-free-tool"};
        HalPinBool _pinFreePalette{"plc.micro5.cmd_free_palette"};

        communicationChannel_t *toolpathChannel;
};

#endif // OPENCN_SERVER_INTERFACE_IMPL_HPP
