/*
 * Copyright (C) 2019 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *                    Kevin Joly <kevin.joly@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "opencnserverinterfaceimpl.hpp"

#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <string>
#include <fcntl.h>
#include <cstdlib>

#include <thread>
#include <chrono>

using namespace std::chrono_literals;

#include <capnp/message.h>

#define MAX_MESSAGE_LEN 10000
#define MAX_FILE_PATH_LEN   200
#define MAX_FILENAME_LEN 	1000
#define TARGET_LOGS_FOLDER_PATH "/root/logs/"

OpenCNServerInterfaceImpl::OpenCNServerInterfaceImpl()
{
    rtapi_log_enable(true);

    // setup shared memory
//    config_key = ftok(FEEDOPT_SHMEM_NAME, FEEDOPT_SHMEM_KEY_CONFIG);
//    config_shmid = shmget(config_key, sizeof(ocn::FeedoptConfig), 0666 | IPC_CREAT);
//
//    config_mem = static_cast<ocn::FeedoptConfig *>(shmat(config_shmid, nullptr, 0));
//    if ((long)config_mem == -1) {
//        std::cerr << "Failed to get config_mem" << std::endl;
//        config_mem = nullptr;
//    }
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setFeedoptCommitCfg(SetFeedoptCommitCfgContext context)
{
    _pinFeedoptGenStartCfg = context.getParams().getCommit();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::getCyclicData(GetCyclicDataContext context)
{
    auto data = context.getResults().initData();
    data.setFeedoptStepDt(_pinFeedoptResamplingAutoOverride);
    data.setFeedoptQueueSize(_pinFeedoptQueueSize);
	data.setFeedoptQueueMax(FEEDOPT_RT_QUEUE_SIZE);
    
    auto progress = data.initFeedoptProgress();
    progress.setCompressingCount(0);
    progress.setCompressingProgress(0);

    progress.setSmoothingCount(0);
    progress.setSmoothingProgress(0);

    progress.setSplittingCount(0);
    progress.setSplittingProgress(0);

    progress.setOptimisingCount(_pinFeedoptOptimisingCount);
    progress.setOptimisingProgress(_pinFeedoptOptimisingProgress);

    progress.setResamplingCount(_pinFeedoptOptimisingCount);
    progress.setResamplingProgress(_pinFeedoptResamplingProgress);

    data.setHomingFinished(!_pinHomingProcessing);

    data.setJogFinished(!_pinJogRunning);

    // data.setGcodeFinished(_pinMachiningFinished);
    data.setMachineState(_pinMachineState);

    data.setGcodeRunning(_pinMachiningRunning);

    auto position = data.initCurrentPosition();
    position.setX(_pinCurrPosX);
    position.setY(_pinCurrPosY);
    position.setZ(_pinCurrPosZ);
    position.setB(_pinCurrPosB);
    position.setC(_pinCurrPosC);

   data.setSpindleVelocity(_pinCurrVelSpindle);

    {
        auto mode = data.initAxisMode();
        
        {
            auto mode_inactive = mode.initInactive();
            mode_inactive.setX(_pinCurrModeInactiveX);
            mode_inactive.setY(_pinCurrModeInactiveY);
            mode_inactive.setZ(_pinCurrModeInactiveZ);
            mode_inactive.setB(_pinCurrModeInactiveB);
            mode_inactive.setC(_pinCurrModeInactiveC);
            mode_inactive.setSpindle(_pinCurrModeInactiveSpindle);
        }

        {
            auto mode_fault = mode.initFault();
            mode_fault.setX(_pinCurrModeFaultX);
            mode_fault.setY(_pinCurrModeFaultY);
            mode_fault.setZ(_pinCurrModeFaultZ);
            mode_fault.setB(_pinCurrModeFaultB);
            mode_fault.setC(_pinCurrModeFaultC);
            mode_fault.setSpindle(_pinCurrModeFaultSpindle);
        }

        {
            auto mode_homing = mode.initHoming();
            mode_homing.setX(_pinCurrModeHomingX);
            mode_homing.setY(_pinCurrModeHomingY);
            mode_homing.setZ(_pinCurrModeHomingZ);
            mode_homing.setB(_pinCurrModeHomingB);
            mode_homing.setC(_pinCurrModeHomingC);
//            mode_homing.setSpindle(_pinCurrModeHomingSpindle);
        }

        {
            auto mode_csp = mode.initCsp();
            mode_csp.setX(_pinCurrModeCSPX);
            mode_csp.setY(_pinCurrModeCSPY);
            mode_csp.setZ(_pinCurrModeCSPZ);
            mode_csp.setB(_pinCurrModeCSPB);
            mode_csp.setC(_pinCurrModeCSPC);
//            mode_csp.setSpindle(_pinCurrModeCSPSpindle);
        }

        {
            auto mode_csv = mode.initCsv();
//            mode_csv.setX(_pinCurrModeCSVX);
//            mode_csv.setY(_pinCurrModeCSVY);
//            mode_csv.setZ(_pinCurrModeCSVZ);
            mode_csv.setSpindle(_pinCurrModeCSVSpindle);
        }
    }

    data.setHomed(_pinIsHomed);

    data.setFeedoptUsActive(_pinFeedoptGenActive);
    data.setFeedoptRtActive(_pinFeedoptReadActive);
    data.setFeedoptReady(_pinFeedoptReady);
    {
        auto mode = data.initMachineMode();

        mode.setHoming(_pinInModeHoming);
        // mode.setStream(_pinInModeStream);
        mode.setJog(_pinInModeJog);
        mode.setInactive(_pinInModeInactive);
        mode.setGcode(_pinInModeGCode);
    }

    data.setCurrentGCodeLine(_pinFeedoptLine);

    /* Load of all axis */
    data.setXLoad(_pinLoadX);
    data.setYLoad(_pinLoadY);
    data.setZLoad(_pinLoadZ);
    data.setBLoad(_pinLoadB);
    data.setCLoad(_pinLoadC);
    data.setSLoad(_pinLoadS);

    /* Temperature of spindle */
    data.setSpindleTemp(_pinSpindleTemperature);

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::getSpindleOverride(GetSpindleOverrideContext context)
{
    context.getResults().setResult(_pinSpindleOverride);

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setSpindleOverride(SetSpindleOverrideContext context)
{
    _pinSpindleOverride = context.getParams().getOverride();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setLcctSetMachineModeHoming(SetLcctSetMachineModeHomingContext context)
{
   _pinSetMachineModeHoming = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setLcctSetMachineModeJog(SetLcctSetMachineModeJogContext context)
{
    _pinSetMachineModeJog = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setLcctSetMachineModeInactive(SetLcctSetMachineModeInactiveContext context)
{
    _pinSetMachineModeInactive = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setLcctSetMachineModeGCode(SetLcctSetMachineModeGCodeContext context)
{
    _pinSetMachineModeMachining = context.getParams().getMode();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setStartHoming(SetStartHomingContext context)
{
    _pinStartHoming = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setStopHoming(SetStopHomingContext context)
{
    _pinStopHoming = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setHomePositionX(SetHomePositionXContext context)
{
    _pinHomePositionX = context.getParams().getPosition();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setHomePositionY(SetHomePositionYContext context)
{
    _pinHomePositionY = context.getParams().getPosition();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setHomePositionZ(SetHomePositionZContext context)
{
    _pinHomePositionZ = context.getParams().getPosition();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setSpeedSpindle(SetSpeedSpindleContext context)
{
   _pinSpeedSpindle = context.getParams().getSpeed();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setActiveSpindle(SetActiveSpindleContext context)
{
   _pinActiveSpindle = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setSpindleThreshold(SetSpindleThresholdContext context)
{
   _paramSpindleThreshold = context.getParams().getPercent();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setJogX(SetJogXContext context)
{

    printf("[WARNING] %s called - it should not happen\n", __func__);

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setJogY(SetJogYContext context)
{
    printf("[WARNING] %s called - it should not happen\n", __func__);

    return kj::READY_NOW;

}

::kj::Promise<void> OpenCNServerInterfaceImpl::setJogZ(SetJogZContext context)
{
    printf("[WARNING] %s called - it should not happen\n", __func__);

    return kj::READY_NOW;

}
::kj::Promise<void> OpenCNServerInterfaceImpl::setRelJog(SetRelJogContext context)
{
    printf("[WARNING] %s called - it should not happen\n", __func__);

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setPlusJog(SetPlusJogContext context)
{
    printf("[WARNING] %s called - it should not happen\n", __func__);

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setMinusJog(SetMinusJogContext context)
{
    printf("[WARNING] %s called - it should not happen\n", __func__);

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setAbsJog(SetAbsJogContext context)
{
    printf("[WARNING] %s called - it should not happen\n", __func__);

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setGoJog(SetGoJogContext context)
{
    printf("[WARNING] %s called - it should not happen\n", __func__);

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setSpeedJog(SetSpeedJogContext context)
{
    _paramJogSpeed0 = context.getParams().getSpeed();
    _paramJogSpeed1 = context.getParams().getSpeed();
    _paramJogSpeed2 = context.getParams().getSpeed();
    _paramJogSpeed3 = context.getParams().getSpeed();
    _paramJogSpeed4 = context.getParams().getSpeed();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setStopJog(SetStopJogContext context)
{
   _pinJogStop = context.getParams().getMode();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setOffset(SetOffsetContext context)
{
//    _pinOffsetX = context.getParams().getX();
//    _pinOffsetY = context.getParams().getY();
//    _pinOffsetZ = context.getParams().getZ();
//    _pinOffsetC = context.getParams().getC();
    printf("[WARNING] %s called - support of Offset is not implemented !\n", __func__);

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setGcodeStart(SetGcodeStartContext context)
{
    _pinMachiningStart = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setGcodePause(SetGcodePauseContext context)
{
    _pinMachiningPause = context.getParams().getMode();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setFaultReset(SetFaultResetContext context)
{
    _pinFaultReset = context.getParams().getReset();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setFeedrateScale(SetFeedrateScaleContext context)
{
	_pinFeedoptResamplingManualOverride = context.getParams().getScale();
    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setFeedoptReset(SetFeedoptResetContext context)
{
   _pinFeedoptReset = context.getParams().getReset();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::readLog(ReadLogContext context)
{
    char message[MAX_MESSAGE_LEN];
    message[0] = 0;

    rtapi_log_read(message, MAX_MESSAGE_LEN);
    context.getResults().setMessage(message);

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setFeedoptConfig(SetFeedoptConfigContext context)
{
    // TODO - improve the way to deal with multiple axes - number not know...
    HalParamDouble _param_vmax_X{"feedopt.axisX.vmax"};
    HalParamDouble _param_vmax_Y{"feedopt.axisY.vmax"};
    HalParamDouble _param_vmax_Z{"feedopt.axisZ.vmax"};
    HalParamDouble _param_vmax_B{"feedopt.axisB.vmax"};
    HalParamDouble _param_vmax_C{"feedopt.axisC.vmax"};
    
    HalParamDouble _param_amax_X{"feedopt.axisX.amax"};
    HalParamDouble _param_amax_Y{"feedopt.axisY.amax"};
    HalParamDouble _param_amax_Z{"feedopt.axisZ.amax"};
    HalParamDouble _param_amax_B{"feedopt.axisB.amax"};
    HalParamDouble _param_amax_C{"feedopt.axisC.amax"};
    
    HalParamDouble _param_jmax_X{"feedopt.axisX.jmax"};
    HalParamDouble _param_jmax_Y{"feedopt.axisY.jmax"};
    HalParamDouble _param_jmax_Z{"feedopt.axisZ.jmax"};
    HalParamDouble _param_jmax_B{"feedopt.axisB.jmax"};
    HalParamDouble _param_jmax_C{"feedopt.axisC.jmax"};
    
    HalParamU32    _param_nhorz{"feedopt.nhorz"};
    HalParamU32    _param_ndiscr{"feedopt.ndiscr"};
    HalParamU32    _param_nbreak{"feedopt.nbreak"};
    HalParamDouble _param_lsplit{"feedopt.lsplit"};
    HalParamDouble _param_cut_off{"feedopt.cut_off"};
    HalParamBool _param_split_special_spline{"feedopt.split-special-spline"};
    HalParamBool _param_debug_print{"feedopt.debug-print"};

    _param_vmax_X = context.getParams().getConfig().getVmaxX();
    _param_vmax_Y = context.getParams().getConfig().getVmaxY();
    _param_vmax_Z = context.getParams().getConfig().getVmaxZ();
    _param_vmax_B = context.getParams().getConfig().getVmaxB();
    _param_vmax_C = context.getParams().getConfig().getVmaxC();

    _param_amax_X = context.getParams().getConfig().getAmaxX();
    _param_amax_Y = context.getParams().getConfig().getAmaxY();
    _param_amax_Z = context.getParams().getConfig().getAmaxZ();
    _param_amax_B = context.getParams().getConfig().getAmaxB();
    _param_amax_C = context.getParams().getConfig().getAmaxC();

    _param_jmax_X = context.getParams().getConfig().getJmaxX();
    _param_jmax_Y = context.getParams().getConfig().getJmaxY();
    _param_jmax_Z = context.getParams().getConfig().getJmaxZ();
    _param_jmax_B = context.getParams().getConfig().getJmaxB();
    _param_jmax_C = context.getParams().getConfig().getJmaxC();

    _param_nhorz   = context.getParams().getConfig().getNHorz();
    _param_ndiscr  = context.getParams().getConfig().getNDiscr();
    _param_nbreak  = context.getParams().getConfig().getNBreak();
    _param_lsplit  = context.getParams().getConfig().getLSplit();
    _param_cut_off = context.getParams().getConfig().getCutOff();
    _param_split_special_spline = context.getParams().getConfig().getSplitSpecialSpine();

    _param_debug_print = context.getParams().getConfig().getDebugPrint();

    // TODO - remove the source parameter in Captain Proto
    //strncpy(config_mem->source, context.getParams().getConfig().getSource().cStr(), sizeof(config_mem->source));

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::getFeedoptConfig(GetFeedoptConfigContext context)
{
    // TODO - improve the way to deal with multiple axes - number not know...
    HalParamDouble _param_vmax_X{"feedopt.axisX.vmax"};
    HalParamDouble _param_vmax_Y{"feedopt.axisY.vmax"};
    HalParamDouble _param_vmax_Z{"feedopt.axisZ.vmax"};
    HalParamDouble _param_vmax_B{"feedopt.axisB.vmax"};
    HalParamDouble _param_vmax_C{"feedopt.axisC.vmax"};
    
    HalParamDouble _param_amax_X{"feedopt.axisX.amax"};
    HalParamDouble _param_amax_Y{"feedopt.axisY.amax"};
    HalParamDouble _param_amax_Z{"feedopt.axisZ.amax"};
    HalParamDouble _param_amax_B{"feedopt.axisB.amax"};
    HalParamDouble _param_amax_C{"feedopt.axisC.amax"};
    
    HalParamDouble _param_jmax_X{"feedopt.axisX.jmax"};
    HalParamDouble _param_jmax_Y{"feedopt.axisY.jmax"};
    HalParamDouble _param_jmax_Z{"feedopt.axisZ.jmax"};
    HalParamDouble _param_jmax_B{"feedopt.axisB.jmax"};
    HalParamDouble _param_jmax_C{"feedopt.axisC.jmax"};
    
    HalParamU32     _param_nhorz{"feedopt.nhorz"};
    HalParamU32     _param_ndiscr{"feedopt.ndiscr"};
    HalParamU32     _param_nbreak{"feedopt.nbreak"};
    HalParamDouble  _param_lsplit{"feedopt.lsplit"};
    HalParamDouble  _param_cut_off{"feedopt.cut_off"};
    HalParamBool    _param_split_special_spline{"feedopt.split-special-spline"};
    HalParamBool    _param_debug_print{"feedopt.debug-print"};

    context.getResults().getConfig().setVmaxX(_param_vmax_X);
    context.getResults().getConfig().setVmaxY(_param_vmax_Y);
    context.getResults().getConfig().setVmaxZ(_param_vmax_Z);
    context.getResults().getConfig().setVmaxB(_param_vmax_B);
    context.getResults().getConfig().setVmaxC(_param_vmax_C);

    context.getResults().getConfig().setAmaxX(_param_amax_X);
    context.getResults().getConfig().setAmaxY(_param_amax_Y);
    context.getResults().getConfig().setAmaxZ(_param_amax_Z);
    context.getResults().getConfig().setAmaxB(_param_amax_B);
    context.getResults().getConfig().setAmaxC(_param_amax_C);

    context.getResults().getConfig().setJmaxX(_param_jmax_X);
    context.getResults().getConfig().setJmaxY(_param_jmax_Y);
    context.getResults().getConfig().setJmaxZ(_param_jmax_Z);
    context.getResults().getConfig().setJmaxB(_param_jmax_B);
    context.getResults().getConfig().setJmaxC(_param_jmax_C);

    context.getResults().getConfig().setNHorz(_param_nhorz);
    context.getResults().getConfig().setNDiscr(_param_ndiscr);
    context.getResults().getConfig().setNBreak(_param_nbreak);
    context.getResults().getConfig().setLSplit(_param_lsplit);
    context.getResults().getConfig().setCutOff(_param_cut_off);
    context.getResults().getConfig().setSplitSpecialSpine(_param_split_special_spline);

    context.getResults().getConfig().setDebugPrint(_param_debug_print);

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::toolpathStartChannel(ToolpathStartChannelContext context)
{
    int sampleRate;
    int result = true;

    sampleRate = context.getParams().getSampleRate();

    if (open_sample_channel(&toolpathChannel, O_RDONLY)) {
       toolpathChannel = nullptr;
       result = false;
    }

    if (!sample_channel_set_freq(sampleRate))
        result = false;

    if (!sample_channel_enable(true)) {
        close_channel(toolpathChannel);
        toolpathChannel = nullptr;
        result = false;
    }

    context.getResults().setResult(result);

    return kj::READY_NOW;
}

 ::kj::Promise<void> OpenCNServerInterfaceImpl::toolpathStopChannel(ToolpathStopChannelContext context)
{
    if (sample_channel_enabled()) {
        sample_channel_enable(false);
    }

    if (toolpathChannel) {
        close_channel(toolpathChannel);
        toolpathChannel = nullptr;
    }

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::toolpathReadSamples(ToolpathReadSamplesContext context)
{
    static std::vector<sampler_sample_t> tmp_samples;
    sampler_sample_t tmp_sample;

    tmp_samples.clear();
    while(read_sample(toolpathChannel, &tmp_sample) > 0) {


        tmp_samples.push_back(tmp_sample);
    }

//    for(auto sample: tmp_samples) {
//        printf("[DEBUG] samples (%d) - ", sample.n_pins);
//        for(int k = 0; k < 3; k++)
//            printf(" %f (%d) ", sample.pins[k].f, sample.pins[k].type);
//        printf("\n");
//
//    }

    auto samples = context.getResults().initSamples(tmp_samples.size());
    int i = 0;

    for(auto sample: tmp_samples) {
        auto one_sample = samples[i];
        auto values = one_sample.initValues(sample.n_pins);
        for(int k = 0; k < sample.n_pins; k++) {
            switch(sample.pins[k].type) {
            case HAL_FLOAT:
                values[k].getValue().setF(sample.pins[k].f);
                break;
            case HAL_U32:
                values[k].getValue().setU(sample.pins[k].u);
                break;
            case HAL_S32:
                values[k].getValue().setS(sample.pins[k].s);
                break;
            case HAL_BIT:
                values[k].getValue().setB(sample.pins[k].b);
                break;
            }
        }
        ++i;
    }

    return kj::READY_NOW;
}


namespace {
FILE *destFile;
uint32_t fileSize;
uint32_t count;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::sendFileParam(SendFileParamContext context)
{

    switch (context.getParams().getFileOp())
    {
        case WRITE:
            destFile = fopen(context.getParams().getFileName().cStr(),"wb");
            if (!destFile) {
                std::cerr << "[SERVER] Error Opening file for writing" << std::endl;
                context.getResults().setResult(-1);
            } else {
                fileSize = context.getParams().getSize();
                context.getResults().setResult(0);
                count = 0;
            }
            break;
        case READ:
            destFile = fopen(context.getParams().getFileName().cStr(),"rb");
            if (!destFile) {
                std::cerr << "[SERVER] Error Opening file for reading" << std::endl;
                context.getResults().setResult(-1);
            } else {
                context.getResults().setResult(0);
                count = 0;
            }
            break;
    }

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::sendFileData(SendFileDataContext context)
{
    size_t writeNr;
    auto data = context.getParams().getData();

    writeNr = fwrite(data.begin(), 1, data.size() , destFile);
    if (writeNr != data.size()) {
        std::cerr << "write data in file failed" << std::endl;
        context.getResults().setResult(-1);
    } else {
        count += writeNr;
        context.getResults().setResult(0);

        if (count == fileSize) {
            // File transfer completed
            fclose(destFile);
		}
    }

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::getFileData(GetFileDataContext context)
{

    uint8_t buf[CHUNK];

    int nread;

    /* Reads binary data chunk from previously opened file */
    nread = fread(buf, 1, sizeof(buf), destFile);

    /* send file data chunk to client */
    context.getResults().setData(capnp::Data::Reader(buf, nread));
    // context.getResults().setRetval(nread);

    if ( nread == 0 )
        fclose(destFile);

    return kj::READY_NOW;

}

::kj::Promise<void> OpenCNServerInterfaceImpl::samplerNewFile(SamplerNewFileContext context)
{
    _pinSamplerNewFile = true;
#if OPENCN_SERVER_DEBUG
    std::cerr << "[SERVER] Waiting for new-file ack" << '\n';
#endif
    // The sampler will set this pin back to false when it is done
    while (_pinSamplerNewFile) {
        std::this_thread::sleep_for(1ms);
    }

#if OPENCN_SERVER_DEBUG
    std::cerr << "[SERVER] Received new-file ack" << '\n';
#endif

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::pathExist(PathExistContext context)
{
    struct stat st;
    bool exist;
    char path[100];

    memcpy(path, context.getParams().getPath().cStr(), context.getParams().getPath().size());
    path[context.getParams().getPath().size()] = 0;

    if (!stat(path, &st))
        exist = true;
    else
        exist = false;

    context.getResults().setResult(exist);

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::createFolder(CreateFolderContext context)
{
    bool ok;

    if (mkdir(context.getParams().getFolderPath().cStr(), 0777) == 0)
        ok = true;
    else
        ok = false;

    context.getResults().setResult(ok);

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::startJog(StartJogContext context)
{
    uint8_t axis;
    double  target;
    bool    relative;
    char relativePinName[100];
    char selectPinName[100];
    char cmdPosPinName[100];

    axis     = context.getParams().getAxis();
    target   = context.getParams().getTarget();
    relative = context.getParams().getRelative();

    sprintf(relativePinName, "ocno.jog.axis%c.relative-pos", _axes[axis]);
    sprintf(selectPinName, "ocno.jog.axis%c.select",       _axes[axis]);
    sprintf(cmdPosPinName, "ocno.jog.axis%c.cmd-pos",      _axes[axis]);
    
    HalPinBool   relativePin{relativePinName};
    HalPinBool   selectPin{selectPinName};
    HalPinDouble cmdPosPin{cmdPosPinName};

    selectPin   = true;
    relativePin = relative;
    cmdPosPin   = target;

    _pinJogStart = true;


    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setFreeTool(SetFreeToolContext context)
{
   _pinFreeTool = context.getParams().getState();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setFreePalette(SetFreePaletteContext context)
{
   _pinFreePalette = context.getParams().getState();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::getMachineLimits(GetMachineLimitsContext context)
{
    auto limits = context.getResults();

    limits.getX().setMin(param_get_value(param_find_by_name("ocno.axisX.limit-min"))->f);
    limits.getX().setMax(param_get_value(param_find_by_name("ocno.axisX.limit-max"))->f);
    limits.getY().setMin(param_get_value(param_find_by_name("ocno.axisY.limit-min"))->f);
    limits.getY().setMax(param_get_value(param_find_by_name("ocno.axisY.limit-max"))->f);
    limits.getZ().setMin(param_get_value(param_find_by_name("ocno.axisZ.limit-min"))->f);
    limits.getZ().setMax(param_get_value(param_find_by_name("ocno.axisZ.limit-max"))->f);
    limits.getB().setMin(param_get_value(param_find_by_name("ocno.axisB.limit-min"))->f);
    limits.getB().setMax(param_get_value(param_find_by_name("ocno.axisB.limit-max"))->f);
    limits.getC().setMin(param_get_value(param_find_by_name("ocno.axisC.limit-max"))->f);
    limits.getC().setMax(param_get_value(param_find_by_name("ocno.axisC.limit-min"))->f);

    return kj::READY_NOW;
}

// Generic code to set/get PINs values
//    hal_pin_t *pin;
//    hal_data_u *data;
//
//    pin = pin_find_by_name(pin_name);
//    if (!pin) {
//        // Error case
//    }
//
//    data = pin_get_value(pin);
//    data->b = value;


