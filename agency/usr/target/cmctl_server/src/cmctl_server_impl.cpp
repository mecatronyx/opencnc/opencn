/*
 * Copyright (C) 2021 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <iostream>
#include <sys/types.h>

#include "af.h"

#include "cmctl_server_impl.hpp"

CMCtlImpl::CMCtlImpl()
{
}

template<typename T>
cmctl_error_t CMCtlImpl::setPinRaw(const char *name, const T& value)
{
	cmctl_error_t error = CMCTL_SUCCESS;

	hal_pin_t *pin = pin_find_by_name(name);
	if (!pin) {
		std::cerr << "PIN named '" << name << "' not found" << std::endl;
		return CMCTL_PIN_NOT_EXIST;
	}

	hal_type_t type = pin_get_type(pin);
	switch(type) {
		case HAL_BIT:
			pin_get_value(pin)->b = value.getB();
			break;
		case HAL_FLOAT:
			pin_get_value(pin)->f = value.getF();;
			break;
		case HAL_S32:
			pin_get_value(pin)->s = value.getS();;
			break;
		case HAL_U32:
			pin_get_value(pin)->u = value.getU();;
			break;
		default:
			std::cerr << "PIN type '" << type << "' is not supported" << std::endl;
			error = CMCTL_PIN_TYPE_ERROR;
	}

	return error;
}

template<typename T>
cmctl_error_t CMCtlImpl::getPinRaw(const char *name, T&& result)
{
	cmctl_error_t error = CMCTL_SUCCESS;

	hal_pin_t *pin = pin_find_by_name(name);
	if (!pin) {
		std::cerr << "PIN named '" << name << "' not found" << std::endl;
		return CMCTL_PIN_NOT_EXIST;
	}

	hal_type_t type = pin_get_type(pin);
	switch(type) {
		case HAL_BIT:
			result.setB(pin_get_value(pin)->b);
			break;
		case HAL_FLOAT:
			result.setF(pin_get_value(pin)->f);
			break;
		case HAL_S32:
			result.setS(pin_get_value(pin)->s);
			break;
		case HAL_U32:
			result.setU(pin_get_value(pin)->u);
			break;
		default:
			std::cerr << "PIN type '" << type << "' is not supported" << std::endl;
			error = CMCTL_PIN_TYPE_ERROR;
	}

	return error;
}

::kj::Promise<void> CMCtlImpl::setPin(SetPinContext context)
{
	const char *name = context.getParams().getName().cStr();

	auto error = setPinRaw(name, context.getParams().getValue().getValue());

	context.getResults().setError(error);

    return kj::READY_NOW;
}

::kj::Promise<void> CMCtlImpl::getPin(GetPinContext context)
{
	const char *name = context.getParams().getName().cStr();
	auto &&value = context.getResults().getValue().getValue();

	cmctl_error_t err = getPinRaw(name, value);

	context.getResults().setError(err);

    return kj::READY_NOW;
}

::kj::Promise<void> CMCtlImpl::pinActions(PinActionsContext context)
{
	auto actions = context.getParams().getActions(); // Use iterator instead of for loop
	auto results = context.getResults().initResults(actions.size());
	cmctl_error_t err;

	for(size_t i = 0; i < actions.size(); i++) {
		auto action = actions[i];
		auto result = results[i];
		auto &&value = result.getValue().getValue();
		auto &&error = result.getError();

		const char *name = action.getName().cStr();

		switch (action.getAction()) {
			case CMCtlPins::PinAccess::GET_PIN:
				error = getPinRaw(name, value);
				break;

			case CMCtlPins::PinAccess::SET_PIN:
				error = setPinRaw(name, action.getValue().getValue());
				break;

			default:
				printf(" Unsupported action '%d'\n", action.getAction());
				break;
		}
	}

    return kj::READY_NOW;
}
