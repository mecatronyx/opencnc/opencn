/*
 * Copyright (C) 2019 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef CMCTL_SERVER_IMPL_HPP
#define CMCTL_SERVER_IMPL_HPP

#include <string>

#include "opencn-interface/opencn_interface.capnp.h"

#include "cmctl.h"

class CMCtlImpl: public CMCtlPins::Server {

    public:
        CMCtlImpl();

    protected:
        ::kj::Promise<void> setPin(SetPinContext context) override;
        ::kj::Promise<void> getPin(GetPinContext context) override;

        ::kj::Promise<void> pinActions(PinActionsContext context) override;

        template<typename T>
        cmctl_error_t setPinRaw(const char *name, const T& value);

        template<typename T>
        cmctl_error_t getPinRaw(const char *name,  T&& value);
};

#endif /* CMCTL_SERVER_IMPL_HPP */
