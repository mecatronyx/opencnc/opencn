/*
 * Copyright (C) 2019 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *                    Kevin Joly <kevin.joly@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <cstdlib>

#include <capnp/ez-rpc.h>

#include "opencnserverinterfaceimpl.hpp"

#define OPENCN_SERVER_PORT 7001

int main(int argc, char* argv[])
{
    capnp::EzRpcServer rpcServer(kj::heap<OpenCNServerInterfaceImpl>(),
                                        "0.0.0.0",
                                        OPENCN_SERVER_PORT);
    kj::WaitScope& waitScope = rpcServer.getWaitScope();
    kj::NEVER_DONE.wait(waitScope);

    return EXIT_SUCCESS;
}
