/*
 * Copyright (C) 2019 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *                    Kevin Joly <kevin.joly@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "opencnserverinterfaceimpl.hpp"

#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <string>
#include <fcntl.h>
#include <cstdlib>

#include <thread>
#include <chrono>

using namespace std::chrono_literals;

#include <capnp/message.h>

#define MAX_MESSAGE_LEN 10000
#define MAX_FILE_PATH_LEN   200
#define MAX_FILENAME_LEN 	1000
#define TARGET_LOGS_FOLDER_PATH "/root/logs/"

OpenCNServerInterfaceImpl::OpenCNServerInterfaceImpl()
{
    rtapi_log_enable(true);

    // setup shared memory
//    config_key = ftok(FEEDOPT_SHMEM_NAME, FEEDOPT_SHMEM_KEY_CONFIG);
//    config_shmid = shmget(config_key, sizeof(ocn::FeedoptConfig), 0666 | IPC_CREAT);
//
//    config_mem = static_cast<ocn::FeedoptConfig *>(shmat(config_shmid, nullptr, 0));
//    if ((long)config_mem == -1) {
//        std::cerr << "Failed to get config_mem" << std::endl;
//        config_mem = nullptr;
//    }
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setFeedoptCommitCfg(SetFeedoptCommitCfgContext context)
{
    _pinFeedoptGenStartCfg = context.getParams().getCommit();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::getCyclicData(GetCyclicDataContext context)
{
    auto data = context.getResults().initData();
    data.setFeedoptStepDt(_pinFeedoptResamplingAutoOverride);
    data.setFeedoptQueueSize(_pinFeedoptQueueSize);
	data.setFeedoptQueueMax(FEEDOPT_RT_QUEUE_SIZE);
    
    auto progress = data.initFeedoptProgress();
    progress.setCompressingCount(0);
    progress.setCompressingProgress(0);

    progress.setSmoothingCount(0);
    progress.setSmoothingProgress(0);

    progress.setSplittingCount(0);
    progress.setSplittingProgress(0);

    progress.setOptimisingCount(_pinFeedoptOptimisingCount);
    progress.setOptimisingProgress(_pinFeedoptOptimisingProgress);

    progress.setResamplingCount(_pinFeedoptOptimisingCount);
    progress.setResamplingProgress(_pinFeedoptResamplingProgress);

    data.setHomingFinished(_pinHomingFinished);
    _pinHomingFinished = false;

    data.setStreamFinished(_pinStreamFinished);
    _pinStreamFinished = false;

    data.setStreamRunning(_pinStreamRunning);
    data.setJogFinished(_pinJogFinished);
    _pinJogFinished = false;

    data.setGcodeFinished(_pinGcodeFinished);
    _pinGcodeFinished = false;

    data.setGcodeRunning(_pinGcodeRunning);
    
    auto position = data.initCurrentPosition();
    position.setX(_pinCurrPosX);
    position.setY(_pinCurrPosY);
    position.setZ(_pinCurrPosZ);

    data.setSpindleVelocity(_pinCurrVelSpindle);

    {
        auto mode = data.initAxisMode();
        
        {
            auto mode_inactive = mode.initInactive();
            mode_inactive.setX(_pinCurrModeInactiveX);
            mode_inactive.setY(_pinCurrModeInactiveY);
            mode_inactive.setZ(_pinCurrModeInactiveZ);
            mode_inactive.setSpindle(_pinCurrModeInactiveSpindle);
        }

        {
            auto mode_fault = mode.initFault();
            mode_fault.setX(_pinCurrModeFaultX);
            mode_fault.setY(_pinCurrModeFaultY);
            mode_fault.setZ(_pinCurrModeFaultZ);
            mode_fault.setSpindle(_pinCurrModeFaultSpindle);
        }

        {
            auto mode_homing = mode.initHoming();
            mode_homing.setX(_pinCurrModeHomingX);
            mode_homing.setY(_pinCurrModeHomingY);
            mode_homing.setZ(_pinCurrModeHomingZ);
            mode_homing.setSpindle(_pinCurrModeHomingSpindle);
        }

        {
            auto mode_csp = mode.initCsp();
            mode_csp.setX(_pinCurrModeCSPX);
            mode_csp.setY(_pinCurrModeCSPY);
            mode_csp.setZ(_pinCurrModeCSPZ);
            mode_csp.setSpindle(_pinCurrModeCSPSpindle);
        }

        {
            auto mode_csv = mode.initCsv();
            mode_csv.setX(_pinCurrModeCSVX);
            mode_csv.setY(_pinCurrModeCSVY);
            mode_csv.setZ(_pinCurrModeCSVZ);
            mode_csv.setSpindle(_pinCurrModeCSVSpindle);
        }
    }

    data.setHomed(_pinIsHomed);
    data.setFeedoptUsActive(_pinFeedoptUsActive);
    data.setFeedoptRtActive(_pinFeedoptRtActive);
    data.setFeedoptReady(_pinFeedoptReady);
    data.setStreamerFIFO(_pinFIFOStream);

    {
        auto mode = data.initMachineMode();

        mode.setHoming(_pinInModeHoming);
        mode.setStream(_pinInModeStream);
        mode.setJog(_pinInModeJog);
        mode.setInactive(_pinInModeInactive);
        mode.setGcode(_pinInModeGCode);
    }

    data.setCurrentGCodeLine(_pinFeedoptLine);

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setLcctSetMachineModeHoming(SetLcctSetMachineModeHomingContext context)
{
   _pinLcctSetMachineModeHoming = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setLcctSetMachineModeStream(SetLcctSetMachineModeStreamContext context)
{
    _pinLcctSetMachineModeStream = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setLcctSetMachineModeJog(SetLcctSetMachineModeJogContext context)
{
    _pinLcctSetMachineModeJog = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setLcctSetMachineModeInactive(SetLcctSetMachineModeInactiveContext context)
{
    _pinLcctSetMachineModeInactive = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setLcctSetMachineModeGCode(SetLcctSetMachineModeGCodeContext context)
{
    _pinLcctSetMachineModeGCode = context.getParams().getMode();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setStartHoming(SetStartHomingContext context)
{
    _pinStartHoming = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setStopHoming(SetStopHomingContext context)
{
    _pinStopHoming = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setHomePositionX(SetHomePositionXContext context)
{
    _pinHomePositionX = context.getParams().getPosition();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setHomePositionY(SetHomePositionYContext context)
{
    _pinHomePositionY = context.getParams().getPosition();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setHomePositionZ(SetHomePositionZContext context)
{
    _pinHomePositionZ = context.getParams().getPosition();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setSpeedSpindle(SetSpeedSpindleContext context)
{
    _pinSpeedSpindle = context.getParams().getSpeed();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setActiveSpindle(SetActiveSpindleContext context)
{
    _pinActiveSpindle = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setSpindleThreshold(SetSpindleThresholdContext context)
{
    _pinSpindleThreshold = context.getParams().getPercent()*0.01;

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setJogX(SetJogXContext context)
{
    _pinJogX = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setJogY(SetJogYContext context)
{
    _pinJogY = context.getParams().getMode();

    return kj::READY_NOW;

}

::kj::Promise<void> OpenCNServerInterfaceImpl::setJogZ(SetJogZContext context)
{
    _pinJogZ = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setRelJog(SetRelJogContext context)
{
    _pintRelJog = context.getParams().getValue();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setPlusJog(SetPlusJogContext context)
{
    _pintPlusJog = context.getParams().getPlus();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setMinusJog(SetMinusJogContext context)
{
    _pintMinusJog = context.getParams().getMinus();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setAbsJog(SetAbsJogContext context)
{
    _pintAbsJog = context.getParams().getValue();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setGoJog(SetGoJogContext context)
{
    _pintGoJog = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setSpeedJog(SetSpeedJogContext context)
{
    _pintSpeedJog = context.getParams().getSpeed();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setStopJog(SetStopJogContext context)
{
    _pintStopJog = context.getParams().getMode();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setOffset(SetOffsetContext context)
{
    _pinOffsetX = context.getParams().getX();
    _pinOffsetY = context.getParams().getY();
    _pinOffsetZ = context.getParams().getZ();
    _pinOffsetC = context.getParams().getC();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setStartStream(SetStartStreamContext context)
{
    _pinStartStream = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setStopStream(SetStopStreamContext context)
{
    _pinStopStream = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setPauseStream(SetPauseStreamContext context)
{
    _pinPauseStream = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setLoadStream(SetLoadStreamContext context)
{
    _pinLoadStream = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setGcodeStart(SetGcodeStartContext context)
{
    _pinGcodeStart = context.getParams().getMode();

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setGcodePause(SetGcodePauseContext context)
{
    _pinGcodePause = context.getParams().getMode();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setFaultReset(SetFaultResetContext context)
{
    _pinFaultReset = context.getParams().getReset();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::setFeedrateScale(SetFeedrateScaleContext context)
{
	_pinFeedoptResamplingManualOverride = context.getParams().getScale();
    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setFeedoptReset(SetFeedoptResetContext context)
{
    _pinFeedoptReset = context.getParams().getReset();

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::readLog(ReadLogContext context)
{
    char message[MAX_MESSAGE_LEN];
    message[0] = 0;

    rtapi_log_read(message, MAX_MESSAGE_LEN);
    context.getResults().setMessage(message);

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::setFeedoptConfig(SetFeedoptConfigContext context)
{
    // TODO - improve the way to deal with multiple axes - number not know...
    HalParamDouble _param_amax_X{"feedopt.axisX.amax"};
    HalParamDouble _param_amax_Y{"feedopt.axisY.amax"};
    HalParamDouble _param_amax_Z{"feedopt.axisZ.amax"};
    HalParamDouble _param_jmax_X{"feedopt.axisX.jmax"};
    HalParamDouble _param_jmax_Y{"feedopt.axisY.jmax"};
    HalParamDouble _param_jmax_Z{"feedopt.axisZ.jmax"};
    HalParamU32    _param_nhorz{"feedopt.nhorz"};
    HalParamU32    _param_ndiscr{"feedopt.ndiscr"};
    HalParamU32    _param_nbreak{"feedopt.nbreak"};
    HalParamDouble _param_lsplit{"feedopt.lsplit"};
    HalParamDouble _param_cut_off{"feedopt.cut_off"};

    _param_nhorz   = context.getParams().getConfig().getNHorz();
    _param_ndiscr  = context.getParams().getConfig().getNDiscr();
    _param_nbreak  = context.getParams().getConfig().getNBreak();
    _param_lsplit  = context.getParams().getConfig().getLSplit();
    _param_cut_off = context.getParams().getConfig().getCutOff();

    _param_amax_X =context.getParams().getConfig().getAmaxX();
    _param_amax_Y =context.getParams().getConfig().getAmaxY();
    _param_amax_Z =context.getParams().getConfig().getAmaxZ();

    _param_jmax_X = context.getParams().getConfig().getJmaxX();
    _param_jmax_Y = context.getParams().getConfig().getJmaxY();
    _param_jmax_Z = context.getParams().getConfig().getJmaxZ();

    // TODO - remove the source parameter in Captain Proto
    //strncpy(config_mem->source, context.getParams().getConfig().getSource().cStr(), sizeof(config_mem->source));

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::getFeedoptConfig(GetFeedoptConfigContext context)
{
    // TODO - improve the way to deal with multiple axes - number not know...
    HalParamDouble _param_amax_X{"feedopt.axisX.amax"};
    HalParamDouble _param_amax_Y{"feedopt.axisY.amax"};
    HalParamDouble _param_amax_Z{"feedopt.axisZ.amax"};
    HalParamDouble _param_jmax_X{"feedopt.axisX.jmax"};
    HalParamDouble _param_jmax_Y{"feedopt.axisY.jmax"};
    HalParamDouble _param_jmax_Z{"feedopt.axisZ.jmax"};
    HalParamU32    _param_nhorz{"feedopt.nhorz"};
    HalParamU32    _param_ndiscr{"feedopt.ndiscr"};
    HalParamU32    _param_nbreak{"feedopt.nbreak"};
    HalParamDouble _param_lsplit{"feedopt.lsplit"};
    HalParamDouble _param_cut_off{"feedopt.cut_off"};

    context.getResults().getConfig().setNHorz(_param_nhorz);
    context.getResults().getConfig().setNDiscr(_param_ndiscr);
    context.getResults().getConfig().setNBreak(_param_nbreak);
    context.getResults().getConfig().setLSplit(_param_lsplit);
    context.getResults().getConfig().setCutOff(_param_cut_off);

    context.getResults().getConfig().setAmaxX(_param_amax_X);
    context.getResults().getConfig().setAmaxY(_param_amax_Y);
    context.getResults().getConfig().setAmaxZ(_param_amax_Z);

    context.getResults().getConfig().setJmaxX(_param_jmax_X);
    context.getResults().getConfig().setJmaxY(_param_jmax_Y);
    context.getResults().getConfig().setJmaxZ(_param_jmax_Z);

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::toolpathStartChannel(ToolpathStartChannelContext context)
{
    int sampleRate;
    int result = true;

    sampleRate = context.getParams().getSampleRate();

    if (open_sample_channel(&toolpathChannel, O_RDONLY)) {
       toolpathChannel = nullptr;
       result = false;
    }

    if (!sample_channel_set_freq(sampleRate))
        result = false;

    if (!sample_channel_enable(true)) {
        close_channel(toolpathChannel);
        toolpathChannel = nullptr;
        result = false;
    }

    context.getResults().setResult(result);

    return kj::READY_NOW;
}

 ::kj::Promise<void> OpenCNServerInterfaceImpl::toolpathStopChannel(ToolpathStopChannelContext context)
{
    if (sample_channel_enabled()) {
        sample_channel_enable(false);
    }

    if (toolpathChannel) {
        close_channel(toolpathChannel);
        toolpathChannel = nullptr;
    }

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::toolpathReadSamples(ToolpathReadSamplesContext context)
{
    static std::vector<sampler_sample_t> tmp_samples;
    sampler_sample_t tmp_sample;

    tmp_samples.clear();
    while(read_sample(toolpathChannel, &tmp_sample) > 0) {
        tmp_samples.push_back(tmp_sample);
    }


    auto samples = context.getResults().initSamples(tmp_samples.size());
    int i = 0;

    for(auto sample: tmp_samples) {
        auto one_sample = samples[i];
        auto values = one_sample.initValues(sample.n_pins);
        for(int k = 0; k < sample.n_pins; k++) {
            switch(sample.pins[k].type) {
            case HAL_FLOAT:
                values[k].getValue().setF(sample.pins[k].f);
                break;
            case HAL_U32:
                values[k].getValue().setU(sample.pins[k].u);
                break;
            case HAL_S32:
                values[k].getValue().setS(sample.pins[k].s);
                break;
            case HAL_BIT:
                values[k].getValue().setB(sample.pins[k].b);
                break;
            }
        }
        ++i;
    }

    return kj::READY_NOW;
}


namespace {
FILE *destFile;
uint32_t fileSize;
uint32_t count;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::sendFileParam(SendFileParamContext context)
{

    switch (context.getParams().getFileOp())
    {
        case WRITE:
            destFile = fopen(context.getParams().getFileName().cStr(),"wb");
            if (!destFile) {
                std::cerr << "[SERVER] Error Opening file for writing" << std::endl;
                context.getResults().setResult(-1);
            } else {
                fileSize = context.getParams().getSize();
                context.getResults().setResult(0);
                count = 0;
            }
            break;
        case READ:
            destFile = fopen(context.getParams().getFileName().cStr(),"rb");
            if (!destFile) {
                std::cerr << "[SERVER] Error Opening file for reading" << std::endl;
                context.getResults().setResult(-1);
            } else {
                context.getResults().setResult(0);
                count = 0;
            }
            break;
    }

    return kj::READY_NOW;
}


::kj::Promise<void> OpenCNServerInterfaceImpl::sendFileData(SendFileDataContext context)
{
    size_t writeNr;
    auto data = context.getParams().getData();

    writeNr = fwrite(data.begin(), 1, data.size() , destFile);
    if (writeNr != data.size()) {
        std::cerr << "write data in file failed" << std::endl;
        context.getResults().setResult(-1);
    } else {
        count += writeNr;
        context.getResults().setResult(0);

        if (count == fileSize) {
            // File transfer completed
            fclose(destFile);
		}
    }

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::getFileData(GetFileDataContext context)
{

    uint8_t buf[CHUNK];

    int nread;

    /* Reads binary data chunk from previously opened file */
    nread = fread(buf, 1, sizeof(buf), destFile);

    /* send file data chunk to client */
    context.getResults().setData(capnp::Data::Reader(buf, nread));
    // context.getResults().setRetval(nread);

    if ( nread == 0 )
        fclose(destFile);

    return kj::READY_NOW;

}

::kj::Promise<void> OpenCNServerInterfaceImpl::samplerNewFile(SamplerNewFileContext context)
{
    _pinSamplerNewFile = true;
#if OPENCN_SERVER_DEBUG
    std::cerr << "[SERVER] Waiting for new-file ack" << '\n';
#endif
    // The sampler will set this pin back to false when it is done
    while (_pinSamplerNewFile) {
        std::this_thread::sleep_for(1ms);
    }

#if OPENCN_SERVER_DEBUG
    std::cerr << "[SERVER] Received new-file ack" << '\n';
#endif

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::pathExist(PathExistContext context)
{
    struct stat st;
    bool exist;
    char path[100];

    memcpy(path, context.getParams().getPath().cStr(), context.getParams().getPath().size());
    path[context.getParams().getPath().size()] = 0;

    if (!stat(path, &st))
        exist = true;
    else
        exist = false;

    context.getResults().setResult(exist);

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNServerInterfaceImpl::createFolder(CreateFolderContext context)
{
    bool ok;

    if (mkdir(context.getParams().getFolderPath().cStr(), 0777) == 0)
        ok = true;
    else
        ok = false;

    context.getResults().setResult(ok);

    return kj::READY_NOW;
}


// Generic code to set/get PINs values
//    hal_pin_t *pin;
//    hal_data_u *data;
//
//    pin = pin_find_by_name(pin_name);
//    if (!pin) {
//        // Error case
//    }
//
//    data = pin_get_value(pin);
//    data->b = value;


